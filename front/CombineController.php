<?php

class CombineController extends \BaseController {
	private $allTrack, $skip, $take;
	private $need, $orderBy, $order, $optionOrder;
	private $error;

	private $query;

	private $trackID, $trackIDArray, $userID, $userIDArray, $option, $count, $keyword;

	/* Ordering by value */
	private $author, $judul, $genre, $public, $tags, $type, $like, $play, 
			$shared, $buy, $commentSum, $rating, $original;

	/* Ordering by limit (ex: duration from 0 - 30 s) */
	private $duration, $price, $size, $production, $release, $update, $create;
	
	/* Construct Function */
	/*
		memasukkan tampilan yang akan digunakan dan apa yang dibutuhkan
	*/
	public function __construct($trackType = 'all', $skip = 0, $take = false){
		$this->error = array();
		$allTrack = $this->getAlltrack($trackType);
		$this->allTrack = $allTrack;
		$this->skip = $skip;
		$this->take = $take;
	}

	public function getAlltrack($trackType){
		switch ($trackType) {
			case 'song':
				$return = 1;
				break;
			
			case 'track':
				$return = 0;
				break;

			case 'cover':
				$return = 2;
				break;

			case 'collaborate':
				$return = 3;
				break;

			default:
				$return = 4;
				break;
		}
		return $return;
	}

	public function setPublic($public){
		$this->public = $public;
	}	

	public function setTrackID($trackID){
		if(is_array($trackID)){
			$this->trackIDArray = $trackID;
		}
		else{
			$this->trackID = $trackID;
		}
	}

	public function setUserID($userID, $option){
		if(is_array($userID))
			$this->userIDArray = $userID;
		else
			$this->userID = $userID;

		if($option){
			$this->option = $option;
		}
	}

	public function setType($type){
		$this->type = $type;
	}

	public function setSearch($keyword){
		$this->keyword = $keyword;
	}

	public function setGenre($genre){
		if($genre == 1) $return = 'Classical';
		elseif($genre == 2) $return = 'Jazz';
		elseif($genre == 3) $return = 'Gospel';
		elseif($genre == 4) $return = 'Blues';
		elseif($genre == 5) $return = 'Funk';
		elseif($genre == 6) $return = 'Rock';
		elseif($genre == 7) $return = 'Metal';
		elseif($genre == 8) $return = 'Electronic';
		elseif($genre == 9) $return = 'Reggae';
		elseif($genre == 10) $return = 'SKA';
		elseif($genre == 11) $return = 'Hip-Hop';
		elseif($genre == 12) $return = 'RnB';
		elseif($genre == 13) $return = 'Pop';
		elseif($genre == 14) $return = 'Latin';
		elseif($genre == 15) $return = 'Country';
		elseif($genre == 16) $return = 'Dangdut';
		elseif($genre == 17) $return = 'Traditional';
		elseif($genre == 18) $return = 'Dance';
		elseif($genre == 19) $return = 'Religi';
		else $return = 0;

		if($return == 0){
			$this->genre = $return;
		}
	}


	public function setNeed($whatINeed){
		$need = [
			'trackID' => false,
			'userID' => false,
			'author' => false,
			'folder' => false,
			'judul' => false,
			'picture' => false,
			'sample' => false,
			'full' => false,
			'duration' => false,
			'price' => false,
			'genre' => false,
			'public' => false,
			'tags' => false,
			'size' => false,
			'production' => false,
			'release' => false,
			'update' => false,
			'create' => false,
			'type' => false,
			'like' => false,
			'play' => false,
			'shared' => false,
			'buy' => false,
			'commentSum' => false,
			'commentAll' => false,
			'rating' => false,
			'original' => false,
			'wishlist' => false,
			'statusLike' => false,
			'instrument' => false,
			'collaborate' => false
		];

		if($whatINeed == 'all'){
			foreach ($need as $key => $value) {
				$need[$key] = true;
			}	
		}
		else{
			foreach ($whatINeed as $key => $value) {
				if($value)
					$need[$key] = true;
			}
		}
		$this->need = $need;
	}

	public function getCount(){
		return $this->count;
	}

	public function setOrderBy($orderBy, $optionOrder){
		$order = 0;

		if($orderBy == 'author')		$orderBy = 'user_data.username';
		elseif($orderBy == 'judul')		$orderBy = 'track_details.name';
		elseif($orderBy == 'duration')	$orderBy = 'track_details.duration';
		elseif($orderBy == 'price')		$orderBy = 'track_details.is_free';
		elseif($orderBy == 'genre')		$orderBy = 'track_details.genre'; //bisa g diisi
		elseif($orderBy == 'public')	$orderBy = 'track_details.is_public';
		elseif($orderBy == 'tags')		$orderBy = 'track_details.tags'; //bisa g diisi
		elseif($orderBy == 'size')		$orderBy = 'track_details.size';
		elseif($orderBy == 'production')$orderBy = 'track_details.year_production'; //bisa g diisi
		elseif($orderBy == 'release')	$orderBy = 'track_details.year_release'; //bisa g diisi
		elseif($orderBy == 'update')	$orderBy = 'track_details.updated_at';
		elseif($orderBy == 'create')	$orderBy = 'track_details.created_at';
		elseif($orderBy == 'type')		$orderBy = 'track.track_type_id';
		elseif($orderBy == 'like')		$orderBy = 'track_details.liked';
		elseif($orderBy == 'play')		$orderBy = 'track_details.play';
		elseif($orderBy == 'shared')	$orderBy = 'track_details.shared';
		elseif($orderBy == 'buy')		$orderBy = 'track_details.buy';
		elseif($orderBy == 'commentSum')$orderBy = 'track_details.comment';
		elseif($orderBy == 'rating')	$orderBy = 'track_details.rating';
		elseif($orderBy == 'original')	$orderBy = 'track_details.original';
		elseif($orderBy == 'instrument')$orderBy = 'track_details.instrument_name';
		else
			{$orderBy = 'default'; $order = 0;}

		$this->orderBy = $orderBy;
		$this->order = $order;
		$this->optionOrder = $optionOrder;
	}

	public function setData(){
		$allTrack 		= $this->allTrack;
		$skip 			= $this->skip; 
		$take 			= $this->take;
		$orderBy 		= $this->orderBy;
		$optionOrder	= $this->optionOrder;

		$query = DB::table('track');

		// mendapatkan data track = 0, song = 1, cover song = 2, collaborate song = 3 
		if($allTrack != 4)
			$query->where('track_type_id', '=', $allTrack);

		if($allTrack == 3)
			$query->join('collaborate_detail', function($join){
				$join->on('collaborate_detail.id', '=', 'track.collaborate_details_id');
			});	

		// mendapatkan details track
		$query->join('track_details', function($join){
			$join->on('track_details.id', '=', 'track.track_details_id');
		});

		//mendapatkan user details
		$query->join('user_data', function($join){
			$join->on('track.track_id_create', '=', 'user_data.id');
		});

		//handle order by yang kosong
		if($orderBy == 'genre' || $orderBy == 'tags' || $orderBy == 'year_production' || $orderBy == 'year_release'){
			$query->where($orderBy, '!=', '');
		}

		//jika track id di declare (artinya nyari details berdasaran track id)
		if(!empty($this->trackID)){
			$query->where('track.track_details_id', '=', $this->trackID);
		}
		elseif (!empty($this->trackIDArray)) {
			$query->whereIn('track.track_details_id', $this->trackIDArray);
		}

		//jika user id di declare (artinya nyari track berdasaran user id)
		if(!empty($this->userID) && $this->option){
			$query->where('track.track_id_create', '=', $this->userID);
		}
		elseif (!empty($this->userIDArray) && $this->option) {
			$query->whereIn('track.track_id_create', $this->userIDArray);
		}

		//ambil track dengan genre yang ditentukan
		if(!empty($this->genre)){
			$query->where('track_details.genre','=', $this->genre);
		}
		elseif (!empty($this->genreArray)) {
			$query->whereIn('track_details.genre', $this->genre);
		}

		//get public track or all
		if($this->public != 'all'){
			if(!empty($this->public))
				$query->where("track_details.is_public", '=', $this->public);
			else
				$query->where("track_details.is_public", '=', 0);
		}
		elseif (empty($this->public)) {
			$query->where("track_details.is_public", '=', 0);
		}
		// return $query->get();
		
		if(!empty($this->keyword)){
			$query->where('track_details.name', 'like', '%'.$this->keyword.'%');
		}

		//Ordering
		if($orderBy != 'default'){
			$query->orderBy($orderBy, $optionOrder);
		}

		//Limiting
		$count = $query->count(); 
		$this->count = $count;
		if($take != false || $take === 0){
			$query->skip($skip)->take($take);
		}

		//Set Query
		$this->query = $query;
		return 1;
	}

	public function getData(){
		$need 	= $this->need;
		$query	= $this->query;
		// echo sizeof($query); die();
	
		if($need['trackID']) 		$query->addSelect('track.track_details_id as trackID');
		if($need['userID'])			$query->addSelect('track.track_id_create as userID');
		if($need['judul'])			$query->addSelect('track_details.name as judul');
		if($need['folder'])			$query->addSelect('user_data.track_folderHash');
		if($need['picture'])		$query->addSelect('track_details.hashPicture as picture');
		if($need['sample'])			$query->addSelect('track_details.hashSample as sample');
		if($need['full'])			$query->addSelect('track_details.hashName as full');
		if($need['duration'])		$query->addSelect('track_details.duration as duration');
		if($need['price'])			$query->addSelect('track_details.is_free as price');
		if($need['genre'])			$query->addSelect('track_details.genre as genre');
		if($need['public'])			$query->addSelect('track_details.is_public as public');
		if($need['tags'])			$query->addSelect('track_details.tags as tags');
		if($need['size'])			$query->addSelect('track_details.size as size');
		if($need['production'])		$query->addSelect('track_details.year_production as production');
		if($need['release'])		$query->addSelect('track_details.year_release as release');
		if($need['update'])			$query->addSelect('track_details.updated_at as update');
		if($need['create'])			$query->addSelect('track_details.created_at as create');
		if($need['type'])			$query->addSelect('track.track_type_id as trackType');
		if($need['like'])			$query->addSelect('track_details.liked as like');
		if($need['play'])			$query->addSelect('track_details.play as play');
		if($need['shared'])			$query->addSelect('track_details.shared as shared');
		if($need['buy'])			$query->addSelect('track_details.buy as buy');
		if($need['rating'])			$query->addSelect('track_details.rating as rating');
		if($need['commentSum'])		$query->addSelect('track_details.comment as commentSum');
		if($need['original'])		$query->addSelect('track_details.original as original');
		if($need['instrument'])		$query->addSelect('track_details.instrument_name as instrument');
		if($need['collaborate'])	$query->addSelect('collaborate_detail.collaborate_name as collaborateName');

		if($need['author']){
			// $query->addSelect('user_data.name as name');
			$query->addSelect('user_data.username as username');
			// $query->addSelect('user_data.nickname as nickname');
			// $query->addSelect('user_data.email as email');
		}

		// echo $this->trackID; die();

		$data = $query->get();
		$data = json_decode(json_encode($data), true);
		// return $data;

		if(sizeof($data) == 0){
			$this->error[] = 0;
		}

		$wishlistController = new WishlistController;
		$trackCommentController = new TrackCommentController;
		$encryptController = new EncryptController;
		$trackBankController = new trackBankController;

		foreach ($data as $key => $value) {
			$data[$key]['trackID'] = $encryptController->encryptValue($data[$key]['trackID'], 1, 3);
			// $data[$key]['userID'] = $encryptController->encryptValue($data[$key]['userID'], 1, 1);
			if($need['wishlist']){
				if(!empty($this->userID)){
					$data[$key]['wishlist'] = $wishlistController->
												getCheckWishList(
													$this->userID,
													$value['trackID']);
				}
				else{
					if(!empty($this->userIDArray))
						$this->error[] = 'not for bunch of user';
					else
						$this->error[] = 'please set user id';
				}
			}
			if($need['commentAll'])
				$data[$key]['commentAll'] = $trackCommentController->
												retrieveCommentData(
													$value['trackID']);
            if($need['statusLike']){	
				if(!empty($this->userID)){
			
						$data[$key]['statusLike'] = $trackBankController->
													checkDataLike(
														$this->userID,
														$value['trackID']);
				} 
				else{
					if(!empty($this->userIDArray))
						$this->error[] = 'not for bunch of user';
					else
						$this->error[] = 'please set user id';
				}
            }												
		}

		if(sizeof($this->error) != 0){
			return $this->error;
		}

		return $data;
	}
}

