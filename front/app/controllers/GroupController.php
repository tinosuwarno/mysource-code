<?php
use Carbon\Carbon;

class GroupController extends \BaseController {

	public function createGroup(){
		return View::make('pages.creategroup');
	}

	public function prosesCreateGroup(){
		$data = Session::get('data');
		// print_r($data);
		// print_r(Input::all());

		
	}

	public function getGroup(){
		// return Input::all();
		$input = Input::all();
		$session = Session::get('data');

		$skip = $input['skip'];
		$take = $input['take'];
		$userID = $session['id'];
		$apiKey = $session['apiKey'];

		$data = [
			'skip' => $skip,
			'take' => $take,
			'userID' => $userID,
			'apiKey' => $apiKey,
		];

		$myGuzzle = new myGuzzle('getMyGroup');
		$myGuzzle->setHeader($session['apiKey']);
		$response = $myGuzzle->formParams($data, 'post');
		return $response;
	}

	public function latestpost($groupID){
		$data_group = $this->dataGroup($groupID);
		$group = $data_group['data'];
		 
		$session = Session::get('data');

		$invite_status = '0';
		$block = '0';
		$member_data = $this->anggotaMember($groupID, $invite_status, $block);

		if(count($member_data) != 0){
			$member = $member_data['data'];
		}else{
			$member = '';
		}

		if($data_group == 123){
			return View::make('pages.errors.404');

		}else{
			$data = [
				'id' => $groupID,
				'apiKey' => $session['apiKey']
			];

			$myGuzzle = new myGuzzle('groupprofile');
			$myGuzzle->setHeader($session['apiKey']);
			$data = $myGuzzle->formParams($data, 'post');

			$data1 = [
				'group_id' => $groupID,
				'apiKey' => $session['apiKey']
			];

			$myGuzzle = new myGuzzle('getpostgrup');
			$myGuzzle->setHeader($session['apiKey']);
			$data1 = $myGuzzle->formParams($data1, 'post');

			$data2 = [
				'user_id' => $session['id'],
				'apiKey' => $session['apiKey']
			];

			$myGuzzle = new myGuzzle('getsongforpost');
			$myGuzzle->setHeader($session['apiKey']);
			$data2 = $myGuzzle->formParams($data2, 'post');

			if (!empty($data1['data'])) {
				foreach ($data1['data'] as $key) {
					$created_at = $key['created_at'];
	          	    $ago[]= Carbon::createFromTimeStamp(strtotime($created_at))->diffForHumans();
	          	    $post_id[] = $key['id'];
				}
				
				$post_id = $post_id;
				$session = Session::get('data');
				$user_id = $session['id'];
				
				$data3 = [
					'post_id' => $post_id,
					'user_id' => $user_id,
					'apiKey' => $session['apiKey']
				];
				$myGuzzle = new myGuzzle('checklikepost');
				$myGuzzle->setHeader($session['apiKey']);
				$data3 = $myGuzzle->formParams($data3, 'post');

				$data4 = [
					'user_id' => $session['id'],
					'post_id' => $post_id,
					'apiKey' => $session['apiKey']
				];
				$myGuzzle = new myGuzzle('readallcommentsNew');
				$myGuzzle->setHeader($session['apiKey']);
				$data4 = $myGuzzle->formParams($data4, 'post');

				foreach ($data4['data123'] as $val) {
					$comment_id[] = $val['comment_id'];
				}

				foreach ($data3 as $key) {
					$idk[] = $key['group_post_id'];
				}
				if (empty($idk)) {
					$list = array();
					// print_r($data4);
					return View::make('group.user-timeline-group-post', compact('data', 'data1', 'data2', 'data4', 'list', 'group', 'groupID', 'ago', 'member'));
				}else{
					// print_r($data4['replycomment']);
					$list = array_combine($idk, array_values($idk));
					return View::make('group.user-timeline-group-post', compact('data', 'data1', 'data2', 'data4', 'list', 'group', 'groupID', 'ago', 'member'));
				}
			}else{
				$list = array();
				return View::make('group.user-timeline-group-post', compact('data', 'data1', 'data2', 'list', 'group', 'groupID', 'member'));
			}
		}
	}

	public function checklikepost(){
		$session = Session::get('data');
		$user_id = $session['id'];
		$post_id = Input::get('post_id');
			$data3 = [
				'post_id' => $post_id,
				'user_id' => $user_id,
				'apiKey' => $session['apiKey']
			];
			$myGuzzle = new myGuzzle('checklikepostmore');
			$myGuzzle->setHeader($session['apiKey']);
			$data3 = $myGuzzle->formParams($data3, 'post');
		return $data3;
	}

	public function viewGroup(){
          $session = Session::get('data');
		$data2 = [
				'user_id' => $session['id'],
				'apiKey' => $session['apiKey']
			];

			$myGuzzle = new myGuzzle('getsongforpost');
			$myGuzzle->setHeader($session['apiKey']);
			$data2 = $myGuzzle->formParams($data2, 'post');
		// return View::make('post.user-profile-wall')->with('data2', $data2);
		 return View::make('group-page')->with('data2', $data2);
	}

	public function groupSettingView($groupID){
		$data_group = $this->dataGroup($groupID);
		$group = $data_group['data'];

		$invite_status = '0';
		$block = '0';
		$member_data = $this->anggotaMember($groupID, $invite_status, $block);

		if(count($member_data) != 0){
			$member =  $member_data['data'];
		}else{
			$member = '';
		}
		
		if($data_group == 123){
			return View::make('pages.errors.404');

		}else if($group[0]['checkAdmin'] == 'admin'){
			$session = Session::get('data');
			$data = [
				'id' => $groupID,
				'apiKey' => $session['apiKey']
			];

			$myGuzzle = new myGuzzle('groupprofile');
			$myGuzzle->setHeader($session['apiKey']);
			$data = $myGuzzle->formParams($data, 'post');
			
			return View::make('group.group-setting', compact('groupID', 'data', 'group', 'member'));
		}else{
			return View::make('pages.errors.404');
		}
		
	}

	public function viewGroupMember($groupID){
		$session = Session::get('data');
		// return $session;
		$data_friend = $this->MyFriendList($groupID);
		if(!empty($data_friend['data'])){
			$friend =  $data_friend['data'];
		}else{
			$friend = '';
		}
		
		$data_group = $this->dataGroup($groupID);
		$group = $data_group['data'];

		$invite_status = '0';
		$block = '0';
		$member_data = $this->anggotaMember($groupID, $invite_status, $block);

		if(count($member_data) != 0){
			$member = $member_data['data'];
		}else{
			$member = '';
		}
		
		if($data_group == 123){
			return View::make('pages.errors.404');

		}else{
			$session = Session::get('data');
			$data = [
				'id' => $groupID,
				'apiKey' => $session['apiKey']
			];

			$myGuzzle = new myGuzzle('groupprofile');
			$myGuzzle->setHeader($session['apiKey']);
			$data = $myGuzzle->formParams($data, 'post');
			
			return View::make('group.user-timeline-groups-member', compact('groupID', 'friend', 'data', 'group', 'member'));
		}
	}

	public function anggotaMember($group_id, $invite_status, $block){
		$data = Session::get('data');
		$userID = $data['id'];
		$apiKey = $data['apiKey'];

		$myApp = App::make('serverLocation'); 
	    $server = $myApp->serverLocation ; //variable server
	    $link = 'anggotaMember'; // variable route yang dituju pada cms
	    $route =  $server.$link;

	   	$client = new GuzzleHttp\Client(['verify' => false]);
	        $response = $client->request('post', $route, array(
	            'form_params' => array(
	                'apiKey' => $apiKey,
	                'userID' => $userID,
	                'groupID' => $group_id,
	                'invite_status' => $invite_status,
	            ),
	            'headers' => array(
	                'X-Authorization' => $apiKey
	            )
	        ));
	     $return = $response->getBody();
	     $return = json_decode($return, true);
	     return $return;

	}

	public function allevent($groupID){
		$AllBackTrack = $this->getBackTrackEvent();
		$backTrack = $AllBackTrack['data'];
		$AllSong = $this->getSongEvent();
		$song = $AllSong['data'];
		
		$data_group = $this->dataGroup($groupID);
		$group = $data_group['data'];
		$genre = $this->getGenreTag();

		$invite_status = '0';
		$block = '0';
		$member_data = $this->anggotaMember($groupID, $invite_status, $block);

		if(count($member_data) != 0){
			$member = $member_data['data'];
		}else{
			$member = '';
		}
		
		$session = Session::get('data');

		if($data_group == 123){
			return View::make('pages.errors.404');

		}else{
			$data = [
				'id' => $groupID,
				'apiKey' => $session['apiKey']
			];

			$myGuzzle = new myGuzzle('groupprofile');
			$myGuzzle->setHeader($session['apiKey']);
			$data = $myGuzzle->formParams($data, 'post');

			$data1 = [
				'group_id' => $groupID,
				'apiKey' => $session['apiKey']
			];

			$myGuzzle = new myGuzzle('getpostgrup');
			$myGuzzle->setHeader($session['apiKey']);
			$data1 = $myGuzzle->formParams($data1, 'post');
			// print_r($data1);
			// return View::make('group.user-timeline-group-post')->with('data', $data);
			return View::make('group.user-timeline-group-events', compact('data', 'data1', 'group', 'groupID', 'genre', 'backTrack', 'song', 'member'));
		}
	}

	public function createGroupProsess(){
		// return Input::all();
	$data = Session::get('data');
    $user_id = $data['id'];

   function compress_image($source_url, $destination_url, $quality) {
		  $info = getimagesize($source_url);
		 
		  if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url);
		  elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url);
		  elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);
		 
		  //save it
		  
		  //$destination_url='public/upload_image/' . . '/';
		  imagejpeg($image, $destination_url, $quality);
		 
		  //return destination file url
		  return $destination_url;
		}
    $GroupName = Input::get('groupname');
    if(empty(Input::get('Description'))){
    	$Description = '';
    }else{
        $Description=Input::get('Description');
    }
   
    //======================untuk profile group=============================
    if(!empty(Input::file('file-1'))){
   
    	$input = array('image' => Input::file('file-1'));
		   $rules = array(
		   	    'image' => 'image'
		   	 );
		   $validator = Validator::make($input, $rules);
		   if($validator->fails()){
		   	echo '<script language="javascript">';
	        echo 'alert("Sorry, Group Avatar Harus Image");history.go(-1);';
	        echo '</script>';
		   }
		   else{
			    $PhotoGroup=Input::file('file-1');
				$nameReal = $PhotoGroup->getClientOriginalName();
				
			    $imageHashName = preg_replace('/[^\p{L}\p{N}\s]/u', '', Hash::make($nameReal.$data['username']. time()));
				$publicPathImage300x300 = public_path(). "/img/photo-group/".$imageHashName."_300x300.jpg";

		        try {
		        	require_once(app_path() . '/library/SimpleImage.php');
				 	$img = new abeautifulsite\SimpleImage($PhotoGroup);
				   	$img->thumbnail(300, 300, 'center')->save($publicPathImage300x300);
				   	$url_photoGroup = "img/photo-group/".$imageHashName."_300x300.jpg";
				   	// return $url;
				} catch(Exception $e) {
				    $this->errorImage[] = "gambar tidak terpotong";
				}
		   }		
    }else{
    	$url_photoGroup = '';
    }
    //======================untuk background group=============================
   	if(!empty(Input::file('file-2'))){
    
   		$input = array('image' => Input::file('file-2'));
		   $rules = array(
		   	    'image' => 'image'
		   	 );
		   $validator = Validator::make($input, $rules);
		   if($validator->fails()){
		   	$dest_photo = public_path().'/img/photo-group/';
		   	unlink(realpath($dest_photo) . DIRECTORY_SEPARATOR . $imageHashName."_300x300.jpg");
		   	echo '<script language="javascript">';
	        echo 'alert("Sorry, Group Cover Harus Image");history.go(-1);';
	        echo '</script>';
		   }
		   else{
		   		$BackgroundGroup=Input::file('file-2');
				$source_bg = $BackgroundGroup;
				$dest_bg = public_path().'/img/background_group';
				$nameReal_bg = $BackgroundGroup->getClientOriginalName();
				$ext_bg = pathinfo($dest_bg."/".$nameReal_bg, PATHINFO_EXTENSION);
				$rand_bg = rand(5, 150);
				$namehash_bg = md5($nameReal_bg.$rand_bg).".".$ext_bg;

				$d_bg = compress_image($source_bg, $dest_bg.'/'.$namehash_bg, 50);

			  	$url_bg = 'img/background_group/'.$namehash_bg;
	       }		  	
   }else{
   	    $url_bg = '';
   }
   $SettingGroup=Input::get('optionsRadios');		
    
    $myApp = App::make('serverLocation'); 
    $server = $myApp->serverLocation ; //variable server
    $link = 'createGroupProsess'; // variable route yang dituju pada cms
    $route =  $server.$link;

   	$client = new GuzzleHttp\Client(['verify' => false]);
        $response = $client->request('post', $route, array(
            'form_params' => array(
                'apiKey' => $data['apiKey'],
                'userID' => $user_id,
                'GroupName' => $GroupName,
                'Description' => $Description,
                'PhotoGroup' =>$url_photoGroup,
                'BackgroundGroup' => $url_bg,
                'SettingGroup' => $SettingGroup
            ),
            'headers' => array(
                'X-Authorization' => $data['apiKey']
            )
        ));
     $return = $response->getBody();
    
    $link = "group/";
    $link2 ="member/";
    $url=$link.$link2.$return;
    return Redirect::to($url);
}

	public function groupSettingProses(){
	  $data = Session::get('data');
      $user_id = $data['id'];


	   function compress_image($source_url, $destination_url, $quality) {
			  $info = getimagesize($source_url);
			 
			  if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url);
			  elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url);
			  elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);
			 
			  //save it
			  
			  //$destination_url='public/upload_image/' . . '/';
			  imagejpeg($image, $destination_url, $quality);
			 
			  //return destination file url
			  return $destination_url;
			}
	//===================== photo group ======================//		
	   if(empty(Input::file('file-input1')) ){
	   	    if(empty(Input::get('picture'))){
	   	    	$url_photoGroup = '';
	   	    }else{
	   	        $url_photoGroup = Input::get('picture');
	   	    }
	   }else{
		   $input = array('image' => Input::file('file-input1'));
		   $rules = array(
		   	    'image' => 'image'
		   	 );
		   $validator = Validator::make($input, $rules);
		   if($validator->fails()){
		   	echo '<script language="javascript">';
	        echo 'alert("Group Avatar Harus Image");history.go(-1);';
	        echo '</script>';
		   }
		   else{
		   	  	$PhotoGroup = Input::file('file-input1');
				$nameReal = $PhotoGroup->getClientOriginalName();
	
		   	  	$imageHashName = preg_replace('/[^\p{L}\p{N}\s]/u', '', Hash::make($nameReal.$data['username']. time()));
				$publicPathImage300x300 = public_path(). "/img/photo-group/".$imageHashName."_300x300.jpg";

                try {
		        	require_once(app_path() . '/library/SimpleImage.php');
				 	$img = new abeautifulsite\SimpleImage($PhotoGroup);
				   	$img->thumbnail(300, 300, 'center')->save($publicPathImage300x300);
				   	$url_photoGroup = "img/photo-group/".$imageHashName."_300x300.jpg";
				   	// return $url_photoGroup;
				} catch(Exception $e) {
				    $this->errorImage[] = "gambar tidak terpotong";
				}

            }
	   }

	//===================== bacground group ======================//		
	   if(empty(Input::file('file-input2') ) ){
	   	    if(empty(Input::get('bg_picture'))){
	   	    	$url_bg = '';
	   	    }else{
	   	        $url_bg = Input::get('bg_picture');
	   	    }
	   }else{
		   $input = array('image' => Input::file('file-input2'));
		   $rules = array(
		   	    'image' => 'image'
		   	 );
		   $validator = Validator::make($input, $rules);
		   if($validator->fails()){
		   	  if(!empty(Input::file('file-input1')) ){
			   	$dest_photo = public_path().'/img/photo-group/';
			   	if(file_exists(realpath($dest_photo) . DIRECTORY_SEPARATOR . $imageHashName."_300x300.jpg")){
			   	     unlink(realpath($dest_photo) . DIRECTORY_SEPARATOR . $imageHashName."_300x300.jpg");
			    }
			  } 

		   	echo '<script language="javascript">';
	        echo 'alert("Group Cover Harus Image");history.go(-1);';
	        echo '</script>';
		   }
		   else{
		   	  	$BackgroundGroup = Input::file('file-input2');
		   	  	$source_bg = $BackgroundGroup;
				$dest_bg = public_path().'/img/background_group/';
				$nameReal_bg = $BackgroundGroup->getClientOriginalName();
				$ext_bg = pathinfo($dest_bg."/".$nameReal_bg, PATHINFO_EXTENSION);
				$rand_bg = rand(5, 150);
				$namehash_bg = md5($nameReal_bg.$rand_bg).".".$ext_bg;

				$d_bg = compress_image($source_bg, $dest_bg.'/'.$namehash_bg, 50);

			  	$url_bg = 'img/background_group/'.$namehash_bg;
   
		   	}  
	    }


  //=================== untuk description ========================//
     if(empty(Input::get('Description')) ) {
     	 if(empty(Input::get('old_description'))){
     	 	$description = '';
     	 }else{
     	    $description = Input::get('old_description');
     	 }
     }else{
     	$description = Input::get('Description');
     }
  //=============== untuk cek admin =========================//
	  if(empty(Input::get('member_id')) ){
	  	  $admin = Input::get('old_admin');
	  }else{
	  	  $admin = Input::get('member_id');
	  }   

     $GroupName = Input::get('groupname');	  
     $groupID = Input::get('groupID'); 
        $myApp = App::make('serverLocation'); 
	    $server = $myApp->serverLocation ; //variable server
	    $link = 'groupSettingProses'; // variable route yang dituju pada cms
	    $route =  $server.$link;

	   	$client = new GuzzleHttp\Client(['verify' => false]);
	        $response = $client->request('post', $route, array(
	            'form_params' => array(
	                'apiKey' => $data['apiKey'],
	                'userID' => $user_id,
	                'GroupName' => $GroupName,
	                'Description' => $description,
	                'PhotoGroup' =>$url_photoGroup,
	                'BackgroundGroup' => $url_bg,
	                'groupID' => $groupID,
	                'admin' => $admin,
	            ),
	            'headers' => array(
	                'X-Authorization' => $data['apiKey']
	            )
	        ));
	     $return = $response->getBody();
	     $return = json_decode($return, true);
	     // return $return;
	     $new_picture = $return['picture'];
	     $new_bg_picture = $return['bg_picture'];
	     if(!empty(Input::get('picture'))){
		     if(Input::get('picture') != $return['picture']){
		     	$photo = Input::get('picture');
				$picture = explode("/", $photo);
				$dest_photo = public_path().'/img/photo-group/';
				$picture_name = $picture[2];
			  	if(file_exists(realpath($dest_photo) . DIRECTORY_SEPARATOR . $picture_name)){
	                unlink(realpath($dest_photo) . DIRECTORY_SEPARATOR . $picture_name);
	             }
		     
		     }

		 }
        if(!empty(Input::get('bg_picture'))){
		     if(Input::get('bg_picture') != $return['bg_picture']){
		     	$bg_photo = Input::get('bg_picture');
				$bg_picture = explode("/", $bg_photo);
				$dest_bg_photo = public_path().'/img/background_group/';
				$bg_picture_name = $bg_picture[2];
	            if(file_exists(realpath($dest_bg_photo) . DIRECTORY_SEPARATOR . $bg_picture_name)){
		       	    unlink(realpath($dest_bg_photo) . DIRECTORY_SEPARATOR . $bg_picture_name);
		         }
		   
		     }
		 }    
	    $link = "group/";
        $link2 ="member/";
        $url=$link.$link2.$groupID;
        return Redirect::to($url);
	}

	public function dataSelectNewAdmin(){
		$session = Session::get('data');
		$userID = $session['id'];
		$apiKey = $session['apiKey'];
      
		$groupID = Input::get('groupID');
		$invite_status = '0';
		$block = '0';
		$member_data = $this->anggotaMember($groupID, $invite_status, $block);

		if(count($member_data) != 0){
			$member =  $member_data['data'];
		}else{
			$member = '';
		}
        $return = [
           'data' => $member,
        ];
	return $return;
	}

	public function dataGroup($group_id){
		$session = Session::get('data');
		$userID = $session['id'];
		$apiKey = $session['apiKey'];
		$username = $session['username'];
        // $group_id = 'CE6F0DA41C31C4873FC85A533C9E8C96';
        // $input = Input::all();
        // $group_id = $input['groupID'];
		
		$myApp = App::make('serverLocation'); 
	    $server = $myApp->serverLocation ; //variable server
	    $link = 'dataGroup'; // variable route yang dituju pada cms
	    $route =  $server.$link;

	   	$client = new GuzzleHttp\Client(['verify' => false]);
	        $response = $client->request('post', $route, array(
	            'form_params' => array(
	                'apiKey' => $apiKey,
	                'userID' => $userID,
	                'groupID' => $group_id,
	                'username' => $username,
	            ),
	            'headers' => array(
	                'X-Authorization' => $apiKey
	            )
	        ));
	     $return = $response->getBody();
	     $return = json_decode($return, true);
	     return $return;
	     
	}

	public function yourGroup($userID){
		$session = Session::get('data');
		$userID_mine = $session['id'];
		$apiKey = $session['apiKey'];
        $invite_status = Input::get('invite_status');
		$take = Input::get('take');
		$skip = Input::get('skip');
		
		$myApp = App::make('serverLocation'); 
	    $server = $myApp->serverLocation ; //variable server
	    $link = 'yourGroup'; // variable route yang dituju pada cms
	    $route =  $server.$link;

	   	$client = new GuzzleHttp\Client(['verify' => false]);
	        $response = $client->request('post', $route, array(
	            'form_params' => array(
	                'apiKey' => $apiKey,
	                'userID' => $userID,
	                'userID_mine' => $userID_mine,
	                'invite_status' => $invite_status,
	                'take' => $take,
	                'skip' => $skip,
	            ),
	            'headers' => array(
	                'X-Authorization' => $apiKey
	            )
	        ));
	     $return = $response->getBody();
	     $return = json_decode($return, true);
	     return $return;
	     
	}

	public function memberGroup($group_id){
		$session = Session::get('data');
        $userID = $session['id'];
        $apiKey = $session['apiKey'];
        $input = Input::all();
        $invite_status = $input['invite_status'];
        $skip = $input['skip'];
        $take = $input['take'];

        // $group_id = 'CE6F0DA41C31C4873FC85A533C9E8C96';
        $myApp = App::make('serverLocation'); 
	    $server = $myApp->serverLocation ; //variable server
	    $link = 'memberGroup'; // variable route yang dituju pada cms
	    $route =  $server.$link;

	   	$client = new GuzzleHttp\Client(['verify' => false]);
	        $response = $client->request('post', $route, array(
	            'form_params' => array(
	                'apiKey' => $apiKey,
	                'userID' => $userID,
	                'group_id' => $group_id,
	                'invite_status' => $invite_status,
	                'skip' => $skip,
	                'take' => $take,
	            ),
	            'headers' => array(
	                'X-Authorization' => $apiKey
	            )
	        ));
	     $return = $response->getBody();
	     $return = json_decode($return, true);
	     return $return;
	}

	public function MyFriendList($group_id){
		$session = Session::get('data');
        $userID = $session['id'];
        $apiKey = $session['apiKey'];

        // $group_id = 'CE6F0DA41C31C4873FC85A533C9E8C96';
        $myApp = App::make('serverLocation'); 
	    $server = $myApp->serverLocation ; //variable server
	    $link = 'MyFriendList'; // variable route yang dituju pada cms
	    $route =  $server.$link;

	   	$client = new GuzzleHttp\Client(['verify' => false]);
	        $response = $client->request('post', $route, array(
	            'form_params' => array(
	                'apiKey' => $apiKey,
	                'userID' => $userID,
	                'group_id' => $group_id,
	            ),
	            'headers' => array(
	                'X-Authorization' => $apiKey
	            )
	        ));
	     $return = $response->getBody();
	     $return = json_decode($return, true);
	     
	     return $return;

	}

	public function inviteGroupProses(){
		// return Input::all();
	    $session = Session::get('data');
        $userID = $session['id'];
        $apiKey = $session['apiKey'];
        $friend_id = Input::get('friend_id');
        $group_id = Input::get('group_id');
       // return $group_id;

       $myApp = App::make('serverLocation'); 
	    $server = $myApp->serverLocation ; //variable server
	    $link = 'inviteGroupProses'; // variable route yang dituju pada cms
	    $route =  $server.$link;
        
        $client = new GuzzleHttp\Client(['verify' => false]);
        $response = $client->request('post', $route, array(
            'form_params' =>array(
                'apiKey' => $apiKey,
                'userID' => $userID,
                'friendID' => $friend_id,
                'groupID' => $group_id,
                
             ),
            'headers' => array(
                'X-Authorization' => $apiKey
            )
	     ));
       
        $return = $response->getBody();
        // $return = json_decode($return, true);
        // return $return;
        $link = "group/";
        $link2 ="member/";
        $url=$link.$link2.$group_id;
        return Redirect::to($url);
	}

	public function adminLeaveGroup(){
		$session = Session::get('data');
		$userID = $session['id'];
		$apiKey = $session['apiKey'];

		$groupID = Input::get('groupID');
		$memberID = Input::get('memberID');
		// return $groupID.' '.$memberID;

		$myApp = App::make('serverLocation'); 
	    $server = $myApp->serverLocation ; //variable server
	    $link = 'leaveGroup'; // variable route yang dituju pada cms
	    $route =  $server.$link;

	   	$client = new GuzzleHttp\Client(['verify' => false]);
	        $response = $client->request('post', $route, array(
	            'form_params' => array(
	                'apiKey' => $apiKey,
	                'userID' => $userID,
	                'groupID' => $groupID,
	                'memberID' => $memberID,
	            ),
	            'headers' => array(
	                'X-Authorization' => $apiKey
	            )
	        ));
	     $return = $response->getBody();
	     $return = json_decode($return, true);
	     return $return;
	}

	public function adminLeaveGroupTanpaMember(){
		$session = Session::get('data');
		$userID = $session['id'];
		$apiKey = $session['apiKey'];

		$input = Input::all();
        $group_id = $input['groupID'];
        $photo = $input['picture'];
        $bg_photo = $input['bg_picture'];
		// return $groupID;
		if(empty($photo) && empty($bg_photo)) {
                $myApp = App::make('serverLocation'); 
			    $server = $myApp->serverLocation ; //variable server
			    $link = 'leaveGroup'; // variable route yang dituju pada cms
			    $route =  $server.$link;

			   	$client = new GuzzleHttp\Client(['verify' => false]);
			        $response = $client->request('post', $route, array(
			            'form_params' => array(
			                'apiKey' => $apiKey,
			                'userID' => $userID,
			                'groupID' => $group_id,
			            ),
			            'headers' => array(
			                'X-Authorization' => $apiKey
			            )
			        ));
			     $return = $response->getBody();
			     $return = json_decode($return, true);
			     return $return;
       	    }
       	    else if(empty($photo) && !empty($bg_photo)){
		        $bg_picture = explode("/", $bg_photo);
		        $dest_bg_photo = public_path().'/img/background_group/';
		        $bg_picture_name = $bg_picture[2];
		       	unlink(realpath($dest_bg_photo) . DIRECTORY_SEPARATOR . $bg_picture_name);

               	$myApp = App::make('serverLocation'); 
			    $server = $myApp->serverLocation ; //variable server
			    $link = 'leaveGroup'; // variable route yang dituju pada cms
			    $route =  $server.$link;

			   	$client = new GuzzleHttp\Client(['verify' => false]);
			        $response = $client->request('post', $route, array(
			            'form_params' => array(
			                'apiKey' => $apiKey,
			                'userID' => $userID,
			                'groupID' => $group_id,
			            ),
			            'headers' => array(
			                'X-Authorization' => $apiKey
			            )
			        ));
			     $return = $response->getBody();
			     $return = json_decode($return, true);
			     return $return;
       	    }else if(empty($bg_photo) && !empty($photo)){
       	    	$picture = explode("/", $photo);
			    $dest_photo = public_path().'/img/photo-group/'; 
		        $picture_name = $picture[2];
		       	unlink(realpath($dest_photo) . DIRECTORY_SEPARATOR . $picture_name);

                $myApp = App::make('serverLocation'); 
			    $server = $myApp->serverLocation ; //variable server
			    $link = 'leaveGroup'; // variable route yang dituju pada cms
			    $route =  $server.$link;

			   	$client = new GuzzleHttp\Client(['verify' => false]);
			        $response = $client->request('post', $route, array(
			            'form_params' => array(
			                'apiKey' => $apiKey,
			                'userID' => $userID,
			                'groupID' => $group_id,
			            ),
			            'headers' => array(
			                'X-Authorization' => $apiKey
			            )
			        ));
			     $return = $response->getBody();
			     $return = json_decode($return, true);
			     return $return;
       	    }
            else{
	            $picture = explode("/", $photo);
		        $bg_picture = explode("/", $bg_photo);

			    $dest_photo = public_path().'/img/photo-group/';
		        $dest_bg_photo = public_path().'/img/background_group/';
			    
		        $picture_name = $picture[2];
		        $bg_picture_name = $bg_picture[2];

		       	unlink(realpath($dest_photo) . DIRECTORY_SEPARATOR . $picture_name);
		       	unlink(realpath($dest_bg_photo) . DIRECTORY_SEPARATOR . $bg_picture_name);
		       	$myApp = App::make('serverLocation'); 
			    $server = $myApp->serverLocation ; //variable server
			    $link = 'leaveGroup'; // variable route yang dituju pada cms
			    $route =  $server.$link;

			   	$client = new GuzzleHttp\Client(['verify' => false]);
			        $response = $client->request('post', $route, array(
			            'form_params' => array(
			                'apiKey' => $apiKey,
			                'userID' => $userID,
			                'groupID' => $group_id,
			            ),
			            'headers' => array(
			                'X-Authorization' => $apiKey
			            )
			        ));
			     $return = $response->getBody();
			     $return = json_decode($return, true);
			     return $return;
		    }
	}

	public function memberLeaveGroup(){
		$session = Session::get('data');
		$userID = $session['id'];
		$apiKey = $session['apiKey'];

		$groupID = Input::get('groupID');
		$myApp = App::make('serverLocation'); 
	    $server = $myApp->serverLocation ; //variable server
	    $link = 'memberLeaveGroup'; // variable route yang dituju pada cms
	    $route =  $server.$link;

	   	$client = new GuzzleHttp\Client(['verify' => false]);
	        $response = $client->request('post', $route, array(
	            'form_params' => array(
	                'apiKey' => $apiKey,
	                'userID' => $userID,
	                'groupID' => $groupID,
	            ),
	            'headers' => array(
	                'X-Authorization' => $apiKey
	            )
	        ));
	     $return = $response->getBody();
	     $return = json_decode($return, true);
	     return $return;
	} 

	
	public function confirmGroup(){
        $session = Session::get('data');
	    $userID = $session['id'];
	    $apiKey = $session['apiKey'];
	    $input = Input::all();
	    $group_id = $input['groupID'];

	    // $group_id = 'CE6F0DA41C31C4873FC85A533C9E8C96';
	    $myApp = App::make('serverLocation'); 
	    $server = $myApp->serverLocation ; //variable server
	    $link = 'confirmGroup'; // variable route yang dituju pada cms
	    $route =  $server.$link;

	   	$client = new GuzzleHttp\Client(['verify' => false]);
	        $response = $client->request('post', $route, array(
	            'form_params' => array(
	                'apiKey' => $apiKey,
	                'userID' => $userID,
	                'groupID' => $group_id,
	            ),
	            'headers' => array(
	                'X-Authorization' => $apiKey
	            )
	        ));
	     $return = $response->getBody();
	     // $return = json_decode($return, true);
	     return $return;
	}

	public function notInterestGroup(){
        $session = Session::get('data');
	    $userID = $session['id'];
	    $apiKey = $session['apiKey'];
	    $input = Input::all();
	    $group_id = $input['groupID'];
	    $invite_status = $input['invite_status'];

	    // $group_id = 'CE6F0DA41C31C4873FC85A533C9E8C96';
	    // $invite_status = '1';
	    $myApp = App::make('serverLocation'); 
	    $server = $myApp->serverLocation ; //variable server
	    $link = 'notInterestGroup'; // variable route yang dituju pada cms
	    $route =  $server.$link;

	   	$client = new GuzzleHttp\Client(['verify' => false]);
	        $response = $client->request('post', $route, array(
	            'form_params' => array(
	                'apiKey' => $apiKey,
	                'userID' => $userID,
	                'groupID' => $group_id,
	                'invite_status' => $invite_status,
	            ),
	            'headers' => array(
	                'X-Authorization' => $apiKey
	            )
	        ));
	     $return = $response->getBody();
	     $return = json_decode($return, true);
	     return $return;
	}

	public function joinRequestGroup(){
        $session = Session::get('data');
	    $userID = $session['id'];
	    $apiKey = $session['apiKey'];
	    $input = Input::all();
	    $group_id = $input['groupID'];

	    $myApp = App::make('serverLocation'); 
	    $server = $myApp->serverLocation ; //variable server
	    $link = 'joinRequestGroup'; // variable route yang dituju pada cms
	    $route =  $server.$link;

	   	$client = new GuzzleHttp\Client(['verify' => false]);
	        $response = $client->request('post', $route, array(
	            'form_params' => array(
	                'apiKey' => $apiKey,
	                'userID' => $userID,
	                'groupID' => $group_id,
	            ),
	            'headers' => array(
	                'X-Authorization' => $apiKey
	            )
	        ));
	     $return = $response->getBody();
	     $return = json_decode($return, true);
	     return $return;
	}


	public function adminAcceptRequest(){
		$session = Session::get('data');
	    $userID = $session['id'];
	    $apiKey = $session['apiKey'];

	    // $input = Input::all();
	    $memberID = Input::get('memberID');
	    $group_id = Input::get('groupID');
	    // $memberID = $input['memberID'];
	    // $group_id = $input['groupID'];
	    $myApp = App::make('serverLocation'); 
	    $server = $myApp->serverLocation ; //variable server
	    $link = 'adminAcceptRequest'; // variable route yang dituju pada cms
	    $route =  $server.$link;

	   	$client = new GuzzleHttp\Client(['verify' => false]);
	        $response = $client->request('post', $route, array(
	            'form_params' => array(
	                'apiKey' => $apiKey,
	                'userID' => $userID,
	                'groupID' => $group_id,
	                'memberID' => $memberID,
	            ),
	            'headers' => array(
	                'X-Authorization' => $apiKey
	            )
	        ));
	     $return = $response->getBody();
	     // $return = json_decode($return, true);
	     return $return;

	}

	public function adminNotAcceptRequest(){
		$session = Session::get('data');
	    $userID = $session['id'];
	    $apiKey = $session['apiKey'];

	    $input = Input::all();
	    $memberID = $input['memberID'];
	    $group_id = $input['groupID'];
	    $invite_status = $input['invite_status'];
	    // $group_id = "B5AD9A5E6379F272C1BC1B036DC8D29F";
	    // $memberID = "1FADB9D5C650EADA9ACFAC1EF2C4813D";

	    
	    $myApp = App::make('serverLocation'); 
	    $server = $myApp->serverLocation ; //variable server
	    $link = 'adminNotAcceptRequest'; // variable route yang dituju pada cms
	    $route =  $server.$link;

	   	$client = new GuzzleHttp\Client(['verify' => false]);
	        $response = $client->request('post', $route, array(
	            'form_params' => array(
	                'apiKey' => $apiKey,
	                'userID' => $userID,
	                'groupID' => $group_id,
	                'memberID' => $memberID,
	                'invite_status' => $invite_status,
	            ),
	            'headers' => array(
	                'X-Authorization' => $apiKey
	            )
	        ));
	     $return = $response->getBody();
	     // $return = json_decode($return, true);
	     return $return;
	}

	public function addToFriend($friend_id){
      $session = Session::get('data');
	    $userID = $session['id'];
	    $apiKey = $session['apiKey'];
	    // $friend_id = '6029AD9D187C4F247DD9BD32EE965EFF';
	    $myApp = App::make('serverLocation'); 
	    $server = $myApp->serverLocation ; //variable server
	    $link = 'addToFriend'; // variable route yang dituju pada cms
	    $route =  $server.$link;

	   	$client = new GuzzleHttp\Client(['verify' => false]);
	        $response = $client->request('post', $route, array(
	            'form_params' => array(
	                'apiKey' => $apiKey,
	                'userID' => $userID,
	                'friendID' => $friend_id,
	            ),
	            'headers' => array(
	                'X-Authorization' => $apiKey
	            )
	        ));
	     $return = $response->getBody();
	     // $return = json_decode($return, true);
	     return $return;
	}

	public function ConfrimFriendGroupMember($friend_id){
		$session = Session::get('data');
		$userID = $session['id'];
		$apiKey = $session['apiKey'];

		// $friendID = Input::get('friendID');

		$myApp = App::make('serverLocation'); 
	    $server = $myApp->serverLocation ; //variable server
	    $link = 'confirmFriend'; // variable route yang dituju pada cms
	    $route =  $server.$link;

	   	$client = new GuzzleHttp\Client(['verify' => false]);
	        $response = $client->request('post', $route, array(
	            'form_params' => array(
	                'apiKey' => $apiKey,
	                'user_id' => $userID,
	                'user_Confirm' => $friend_id,
	            ),
	            'headers' => array(
	                'X-Authorization' => $apiKey
	            )
	        ));
	     $return = $response->getBody();
	     // $return = json_decode($return, true);
	     return $return;
	}

	public function ShortBiodataInfo($userID){
      $session = Session::get('data');
      // $userID = $session['id'];
      $apiKey = $session['apiKey'];
      $myApp = App::make('serverLocation'); 
	    $server = $myApp->serverLocation ; //variable server
	    $link = 'getmybioinfo'; // variable route yang dituju pada cms
	    $route =  $server.$link;

	   	$client = new GuzzleHttp\Client(['verify' => false]);
	        $response = $client->request('post', $route, array(
	            'form_params' => array(
	                'apiKey' => $apiKey,
	                'id' => $userID,
	             
	            ),
	            'headers' => array(
	                'X-Authorization' => $apiKey
	            )
	        ));
	     $return = $response->getBody();
	     $return = json_decode($return, true);
	     $data = [
	       'data' => $return,
	     ];
	     return $data;
  }


	public function friendNotMember(){
		$session = Session::get('data');
        $userID = $session['id'];
        $apiKey = $session['apiKey'];

        $myApp = App::make('serverLocation'); 
	    $server = $myApp->serverLocation ; //variable server
	    $link = 'friendNotMember'; // variable route yang dituju pada cms
	    $route =  $server.$link;
        
        $client = new GuzzleHttp\Client(['verify' => false]);
        $response = $client->request('post', $route, array(
            'form_params' =>array(
                'apiKey' => $apiKey,
                'userID' => $userID,
                
             ),
            'headers' => array(
                'X-Authorization' => $apiKey
            )
	     ));
       
        $return = $response->getBody();
        // $return = json_decode($return, true);
        return $return;
	}

	public function loadevent(){
		$group_id= Input::get('group_id');
		$session = Session::get('data');
		$data = [
			'group_id' => $group_id,
			'apiKey' => $session['apiKey'],
			'userID' => $session['id']
		];

		$myGuzzle = new myGuzzle('loadevent');
		$myGuzzle->setHeader($session['apiKey']);
		$return = $myGuzzle->formParams($data, 'post');
		return $return;
	}

	public function submitpostgroup(){
		$text = Input::get('text');
		$group_id = Input::get('group_id');
		$user_id = Input::get('user_id');
		$track_id = Input::get('track_id');
		$session = Session::get('data');

		$data1 = [
			'user_id' => $user_id,
			'text' => $text,
			'group_id' => $group_id,
			'track_id' => $track_id,
			'apiKey' => $session['apiKey']
		];

		$myGuzzle = new myGuzzle('submitpostgroup');
		$myGuzzle->setHeader($session['apiKey']);
		$return = $myGuzzle->formParams($data1, 'post');

		$data = [
			'id' => $user_id,
			'apiKey' => $session['apiKey']
		];

		$myGuzzle = new myGuzzle('myprofile/$id');
		$myGuzzle->setHeader($session['apiKey']);
		$data = $myGuzzle->formParams($data, 'post');
		$username = $data['username'];
		$profile_image = $data['profile_image'];

		// $array = array('text' => $text, 'user_id' => $user_id, 'username' => $username, 'profile_image'=> $profile_image, 'post_id'=> $return, 'track_id'=> $track_id);
		// print_r($data);
		return $return;
	}

	public function komenpostgrup(){
		$user_id = Input::get('user_id');
		$post_id = Input::get('post_id');
		$komen = Input::get('komen');
		$session = Session::get('data');

		$data1 = [
			'user_id' => $user_id,
			'post_id' => $post_id,
			'komen' => $komen,
			'apiKey' => $session['apiKey']
		];
		$myGuzzle = new myGuzzle('komenpostgrup');
		$myGuzzle->setHeader($session['apiKey']);
		$data1 = $myGuzzle->formParams($data1, 'post');

		$data = [
			'id' => $user_id,
			'apiKey' => $session['apiKey']
		];

		$myGuzzle = new myGuzzle('myprofile/$id');
		$myGuzzle->setHeader($session['apiKey']);
		$data = $myGuzzle->formParams($data, 'post');
		$username = $data['username'];
		$profile_image = $data['profile_image'];

		$array = array('komen' => $komen, 'user_id' => $user_id, 'username' => $username, 'profile_image'=> $profile_image, 'created_at'=> $data1['created_at'], 'comment_id'=>$data1['id']);
		// print_r($data);
		// if ($data1==1) {
			return $array;
		// }else return 0;
	}

	public function loadmoregruppost(){
		$skip = Input::get('skip');
		$group_id = Input::get('group_id');
		$user_id = Input::get('user_id');
		$session = Session::get('data');

		$data1 = [
			'group_id' => $group_id,
			'user_id' => $user_id,
			'skip' => $skip,
			'apiKey' => $session['apiKey']
		];

		$myGuzzle = new myGuzzle('getmorepostgrup');
		$myGuzzle->setHeader($session['apiKey']);
		$data1 = $myGuzzle->formParams($data1, 'post');

		return $data1;
	}


	public function readallcomments(){
		$post_id = Input::get('id');
		$session = Session::get('data');
		$data = [
			'post_id' => $post_id,
			'apiKey' => $session['apiKey']
		];
		$myGuzzle = new myGuzzle('readallcomments');
		$myGuzzle->setHeader($session['apiKey']);
		$data = $myGuzzle->formParams($data, 'post');
		return $data;
	}

	public function checklike(){
		$comment_id = Input::get('comment_id');
		$session = Session::get('data');
		$user_id = $session['id'];
		
		$data = [
			'comment_id' => $comment_id,
			'user_id' => $user_id,
			'apiKey' => $session['apiKey']
		];
		$myGuzzle = new myGuzzle('checklike');
		$myGuzzle->setHeader($session['apiKey']);
		$data = $myGuzzle->formParams($data, 'post');
		return $data;
	}

	public function addevent(){
		// return Input::all();
		$trackID = Input::get('trackID')[0];
		$title = Input::get('title');
		$desc = Input::get('Description');
		$start = Input::get('start');
		$end = Input::get('end');
		$group_id = Input::get('group_id');
		$user_id = Input::get('user_id');
		$genre = Input::get('genre');
		if(!empty($genre)){
			$count_genre = count($genre);
			for( $i = 0; $i < $count_genre; $i ++){
				$genres[] = $genre[$i];
			}
			$genres_implode = implode(",",$genres);
        }else{
        	$genres_implode = '';
        }

		if(!empty(Input::file('file-event'))){
   
    	$input = array('image' => Input::file('file-event'));
		   $rules = array(
		   	    'image' => 'image'
		   	 );
		   $validator = Validator::make($input, $rules);
		   if($validator->fails()){
		   	echo '<script language="javascript">';
	        echo 'alert("Sorry, Event Picture Harus Image");history.go(-1);';
	        echo '</script>';
		   }
		   else{
			    $PhotoEvent=Input::file('file-event');
				$nameReal = $PhotoEvent->getClientOriginalName();
				
			    $imageHashName = preg_replace('/[^\p{L}\p{N}\s]/u', '', Hash::make($nameReal.$user_id. time()));
				$publicPathImage475x475 = public_path(). "/img/photo-event/".$imageHashName."_475x475.png";

		        try {
		        	require_once(app_path() . '/library/SimpleImage.php');
				 	$img = new abeautifulsite\SimpleImage($PhotoEvent);
				   	$img->thumbnail(475, 475, 'center')->save($publicPathImage475x475);
				   	$url_photoEvent = "img/photo-event/".$imageHashName."_475x475.png";
				   	// return $url;
				} catch(Exception $e) {
				    $this->errorImage[] = "gambar tidak terpotong";
				}
		   }		
	    }else{
	    	$url_photoEvent = '';
	    }

		$datetime =  explode( 'T', $start );
		$date = $datetime[0];
		$time = $datetime[1];
		$datetimestart = $date. " " .$time;

		$datetime2 =  explode( 'T', $end );
		$date2 = $datetime2[0];
		$time2 = $datetime2[1];
		$datetimeend = $date2. " " .$time2;
        
		$eventType = Input::get('optionsEventType');
		$session = Session::get('data');
		$data = [
			'group_id' => $group_id,
			'user_id' => $user_id,
			'title' =>$title,
			'start_time' => $datetimestart,
			'end_time' => $datetimeend,
			'description' => $desc,
			'apiKey' => $session['apiKey'],
			'url_photoEvent' => $url_photoEvent,
			'trackID' => $trackID,
			'eventType' => $eventType,
			'genres' => $genres_implode,
		];
		$myGuzzle = new myGuzzle('addevent');
		$myGuzzle->setHeader($session['apiKey']);
		$data = $myGuzzle->formParams($data, 'post');
	
		if ($data==1) {
			return Redirect::to('group/event/'.$group_id);
		}else echo "error";
	}

	public function getGenreTag(){
		$session = Session::get('data');
		$apiKey = $session['apiKey'];
		$userID = $session['id'];

		$myApp = App::make('serverLocation'); 
	    $server = $myApp->serverLocation ; //variable server
	    $link = 'getGenreTag'; // variable route yang dituju pada cms
	    $route =  $server.$link;

	   	$client = new GuzzleHttp\Client(['verify' => false]);
	        $response = $client->request('post', $route, array(
	            'form_params' => array(
	                'apiKey' => $apiKey,
	                'userID' => $userID,
	            ),
	            'headers' => array(
	                'X-Authorization' => $apiKey
	            )
	        ));
	     $return = $response->getBody();
	     $return = json_decode($return, true);
	     return $return;
	}

	public function gettrackproperty()
	{
		$track_id = Input::get('track_id');
		$session = Session::get('data');
		$data = [
			'track_id' => $track_id,
			'apiKey' => $session['apiKey']
			];
		$myGuzzle = new myGuzzle('gettrackproperty');
		$myGuzzle->setHeader($session['apiKey']);
		$data = $myGuzzle->formParams($data, 'post');
		return $data;
	}

	public function friendlistGroup($user_id){
	  $session = Session::get('data');
	    $apiKey = $session['apiKey'];
	    // $user_id = $session['id'];
	    $myApp = App::make('serverLocation'); 
	    $server = $myApp->serverLocation ; //variable server
	    $link = 'getFriend'; // variable route yang dituju pada cms
	    $route =  $server.$link;

	   	$client = new GuzzleHttp\Client(['verify' => false]);
	        $response = $client->request('post', $route, array(
	            'form_params' => array(
	                'apiKey' => $apiKey,
	                'userID' => $user_id,
	                'take' => 4,
	                'skip' => 0,
	                'controller' => 'getFriend',
	              
	            ),
	            'headers' => array(
	                'X-Authorization' => $apiKey
	            )
	        ));
	     $return = $response->getBody();
	     $return = json_decode($return, true);
	     return $return['confirmed'];
	}

	public function setCoverGroup($y){
		$session = Session::get('data');
	    $apiKey = $session['apiKey'];
	    $userID = $session['id'];

	    $groupID = Input::get('groupID');
	    // $groupID = '287317F73FE1F7F46A2D4CA0903E07BD';
	    // $y = 52;

	    $myApp = App::make('serverLocation'); 
	    $server = $myApp->serverLocation ; //variable server
	    $link = 'setCoverGroup'; // variable route yang dituju pada cms
	    $route =  $server.$link;

	   	$client = new GuzzleHttp\Client(['verify' => false]);
	        $response = $client->request('post', $route, array(
	            'form_params' => array(
	                'apiKey' => $apiKey,
	                'userID' => $userID,
	                'y_axisbg' => $y,
	                'groupID' => $groupID,
	              
	            ),
	            'headers' => array(
	                'X-Authorization' => $apiKey
	            )
	        ));
	     $return = $response->getBody();
	     // $return = json_decode($return, true);
	     
	     return $return;

	}

	public function clickLikeGroupEventSave(){
		$session = Session::get('data');
		$apiKey = $session['apiKey'];
		$userID = $session['id'];
		$event_id = Input::get('eventID');
		// $event_id = 14;

		$myApp = App::make('serverLocation'); 
	    $server = $myApp->serverLocation ; //variable server
	    $link = 'clickLikeGroupEventSave'; // variable route yang dituju pada cms
	    $route =  $server.$link;

	   	$client = new GuzzleHttp\Client(['verify' => false]);
	        $response = $client->request('post', $route, array(
	            'form_params' => array(
	                'apiKey' => $apiKey,
	                'userID' => $userID,
	                'eventID' => $event_id,
	            ),
	            'headers' => array(
	                'X-Authorization' => $apiKey
	            )
	        ));
	     $return = $response->getBody();
	     $return = json_decode($return, true);
	     
	     return $return;

	}

	public function submitkomenreplygrup(){
		// $user_id = Input::get('user_id');
		$post_id = Input::get('post_id');
		$comment = Input::get('komen');
		$comment_id = Input::get('comment_id');

		$session = Session::get('data');
		$data = [
			'post_id' => $post_id,
			'user_id' => $session['id'],
			'comment' =>$comment,
			'comment_id' => $comment_id,
			'apiKey' => $session['apiKey']
		];
		$myGuzzle = new myGuzzle('submitkomenreplygrup');
		$myGuzzle->setHeader($session['apiKey']);
		$data = $myGuzzle->formParams($data, 'post');
		return $data;
	}

	public function groupEventParticipate(){
       $session = Session::get('data');
       $userID = $session['id'];
       $apiKey = $session['apiKey'];
       $groupID = Input::get('groupID');
       $groupEventID = Input::get('groupEventID');
       $status = Input::get('statusEventParticipate');
       // $groupID = "693BB0F12E4F4499759A30DB64BF48EB";
       // $groupEventID = "16";
       // $status = '0';

       $myApp = App::make('serverLocation'); 
	   $server = $myApp->serverLocation ; //variable server
	   $link = 'groupEventParticipate'; // variable route yang dituju pada cms
	   $route =  $server.$link;

	   	$client = new GuzzleHttp\Client(['verify' => false]);
	        $response = $client->request('post', $route, array(
	            'form_params' => array(
	                'apiKey' => $apiKey,
	                'userID' => $userID,
	                'groupID' => $groupID,
	                'groupEventID' => $groupEventID,
	                'status' => $status,
	            ),
	            'headers' => array(
	                'X-Authorization' => $apiKey
	            )
	        ));
	    $return = $response->getBody();
	    $return = json_decode($return, true);
	    return $return;
	}

	public function getBackTrackEvent(){
	  $session = Session::get('data');
      $userID = $session['id'];
      $apiKey = $session['apiKey'];
      $skip = 0;
      $take = false;
      $genreCode = 0;
      $orderBy = 'create';
      $optionOrder = 'desc';
      $trackType = 'karaoke';

      $myApp = App::make('serverLocation'); 
      $server = $myApp->serverLocation ; //variable server
      $link = 'SongNewRelease'; // variable route yang dituju pada cms
      $route =  $server ."".$link;
     
      $client = new GuzzleHttp\Client(['verify' => false]);
      $response = $client->request('post', $route, array(
            'form_params' => array(
                'userID'  => $userID,
                'skip'    => $skip,
                'take'    => $take,
                'orderBy' => $orderBy,
                'genreCode' => $genreCode,
                'trackType' => $trackType,
                'optionOrder' => $optionOrder
            ),
            'headers' => array(
                'X-Authorization' => $apiKey
            )
        ));
      $return = $response->getBody();
      $return = json_decode($return, true);
      
      return $return;
	}

	public function getSongEvent(){
	  $session = Session::get('data');
      $userID = $session['id'];
      $apiKey = $session['apiKey'];
      $skip = 0;
      $take = false;
      $genreCode = 0;
      $orderBy = 'create';
      $optionOrder = 'desc';
      $trackType = 'song';

      $myApp = App::make('serverLocation'); 
      $server = $myApp->serverLocation ; //variable server
      $link = 'SongNewRelease'; // variable route yang dituju pada cms
      $route =  $server ."".$link;
     
      $client = new GuzzleHttp\Client(['verify' => false]);
      $response = $client->request('post', $route, array(
            'form_params' => array(
                'userID'  => $userID,
                'skip'    => $skip,
                'take'    => $take,
                'orderBy' => $orderBy,
                'genreCode' => $genreCode,
                'trackType' => $trackType,
                'optionOrder' => $optionOrder
            ),
            'headers' => array(
                'X-Authorization' => $apiKey
            )
        ));
      $return = $response->getBody();
      $return = json_decode($return, true);
      
      return $return;
	}

	public function viewGroupSinglePost($group_post_id,$groupID){	
		$data_group = $this->dataGroup($groupID);
		$group = $data_group['data'];
		
		$dataPost = $this->dataSinglePostGroup($group_post_id,$groupID);
		$usernamePost = $dataPost[0]['usernamePost'];

		if($data_group == 123){
			return View::make('pages.errors.404');

		}else{
			$session = Session::get('data');
			$data = [
				'id' => $groupID,
				'apiKey' => $session['apiKey']
			];

			$myGuzzle = new myGuzzle('groupprofile');
			$myGuzzle->setHeader($session['apiKey']);
			$data = $myGuzzle->formParams($data, 'post');

			return View::make('group.singlePost', compact('groupID', 'data', 'group', 'usernamePost'));
		}
	}

	public function dataSinglePostGroup($group_post_id,$groupID){
		$session = Session::get('data'); 
		$userID = $session['id'];
		$apiKey = $session['apiKey'];
		
	    $myApp = App::make('serverLocation'); 

	    $server = $myApp->serverLocation ; //variable server
	    $link = 'singlePostGroup'; // variable route yang dituju pada cms
	    $route =  $server.$link;
	     
	    $client = new GuzzleHttp\Client(['verify' => false]);
	    $response = $client->request('post', $route, array(
	           'form_params' => array(
	                'userID'  => $userID,
	                'apiKey'    => $apiKey,
	                'group_post_id' => $group_post_id,
	                'group_id' => $groupID,            
	            ),
	            'headers' => array(
	                'X-Authorization' => $apiKey
	            )
	        ));
	    $return = $response->getBody();
	    $return = json_decode($return, true);
	     
        return $return;
	}

	public function singlePostGroup(){
		$session = Session::get('data'); 
		$userID = $session['id'];
		$apiKey = $session['apiKey'];
		$group_post_id = Input::get('groupPostID');
		$group_id = Input::get('groupID');
		// echo $group_post_id.'-'.$group_id;
		// die;
		// $group_post_id = 156;
		// $group_id = "B68336F711FE7B3E400CB7ED6C689265";

        
	    $myApp = App::make('serverLocation'); 
	    $server = $myApp->serverLocation ; //variable server
	    $link = 'singlePostGroup'; // variable route yang dituju pada cms
	    $route =  $server.$link;
	     
	    $client = new GuzzleHttp\Client(['verify' => false]);
	    $response = $client->request('post', $route, array(
	           'form_params' => array(
	                'userID'  => $userID,
	                'apiKey'    => $apiKey,
	                'group_post_id' => $group_post_id,
	                'group_id' => $group_id,            
	            ),
	            'headers' => array(
	                'X-Authorization' => $apiKey
	            )
	        ));
	    $return = $response->getBody();
	    $return = json_decode($return, true);
	     
        return $return;

	} 

	public function readAllCommentGroupPost(){
		$session = Session::get('data');
		$userID = $session['id'];
		$apiKey = $session['apiKey'];
		$group_post_id = Input::get('groupPostID');

		$myApp = App::make('serverLocation'); 
	    $server = $myApp->serverLocation ; //variable server
	    $link = 'readAllCommentGroupPost'; // variable route yang dituju pada cms
	    $route =  $server.$link;
	     
	    $client = new GuzzleHttp\Client(['verify' => false]);
	    $response = $client->request('post', $route, array(
	           'form_params' => array(
	                'userID'  => $userID,
	                'apiKey'    => $apiKey,
	                'group_post_id' => $group_post_id,           
	            ),
	            'headers' => array(
	                'X-Authorization' => $apiKey
	            )
	        ));
	    $return = $response->getBody();
	    $return = json_decode($return, true);
	     
        return $return;
	}

	public function hideAllCommentGroupPost(){
		$session = Session::get('data');
		$userID = $session['id'];
		$apiKey = $session['apiKey'];
		$group_post_id = Input::get('groupPostID');

        $myApp = App::make('serverLocation'); 
	    $server = $myApp->serverLocation ; //variable server
	    $link = 'hideAllCommentGroupPost'; // variable route yang dituju pada cms
	    $route =  $server.$link;
	     
	    $client = new GuzzleHttp\Client(['verify' => false]);
	    $response = $client->request('post', $route, array(
	           'form_params' => array(
	                'userID'  => $userID,
	                'apiKey'    => $apiKey,
	                'group_post_id' => $group_post_id,           
	            ),
	            'headers' => array(
	                'X-Authorization' => $apiKey
	            )
	        ));
	    $return = $response->getBody();
	    $return = json_decode($return, true);
	     
        return $return;
	}

	public function seeAllCommentReplayGroup(){
		$session = Session::get('data');
		$userID = $session['id'];
		$apiKey = $session['apiKey'];
		$group_post_comment_id = Input::get('group_post_comment_id');

		$myApp = App::make('serverLocation'); 
	    $server = $myApp->serverLocation ; //variable server
	    $link = 'seeAllCommentReplayGroup'; // variable route yang dituju pada cms
	    $route =  $server.$link;
	     
	    $client = new GuzzleHttp\Client(['verify' => false]);
	    $response = $client->request('post', $route, array(
	           'form_params' => array(
	                'userID'  => $userID,
	                'apiKey'    => $apiKey,
	                'group_post_comment_id' => $group_post_comment_id,           
	            ),
	            'headers' => array(
	                'X-Authorization' => $apiKey
	            )
	        ));
	    $return = $response->getBody();
	    $return = json_decode($return, true);
	     
        return $return;
	}

	public function groupPostLikeProses(){
		$session = Session::get('data');
		$userID = $session['id'];
		$apiKey = $session['apiKey'];
		$group_post_id = Input::get('group_post_id');
		$like_total = Input::get('like_total');

		$myApp = App::make('serverLocation'); 
	    $server = $myApp->serverLocation ; //variable server
	    $link = 'groupPostLikeProses'; // variable route yang dituju pada cms
	    $route =  $server.$link;
	     
	    $client = new GuzzleHttp\Client(['verify' => false]);
	    $response = $client->request('post', $route, array(
	           'form_params' => array(
	                'userID'  => $userID,
	                'apiKey'    => $apiKey,
	                'group_post_id' => $group_post_id,  
	                'like_total' => $like_total,         
	            ),
	            'headers' => array(
	                'X-Authorization' => $apiKey
	            )
	        ));
	    $return = $response->getBody();
	    // $return = json_decode($return, true);
	     
        return $return;

	}

	public function groupCommentPostLikeProses(){
		$session = Session::get('data');
        $userID = $session['id'];
        $apiKey = $session['apiKey'];
        $group_post_comment_id = Input::get('group_post_comment_id');

        $myApp = App::make('serverLocation'); 
	    $server = $myApp->serverLocation ; //variable server
	    $link = 'groupCommentPostLikeProses'; // variable route yang dituju pada cms
	    $route =  $server.$link;
	     
	    $client = new GuzzleHttp\Client(['verify' => false]);
	    $response = $client->request('post', $route, array(
	           'form_params' => array(
	                'userID'  => $userID,
	                'apiKey'    => $apiKey,
	                'group_post_comment_id' => $group_post_comment_id,       
	            ),
	            'headers' => array(
	                'X-Authorization' => $apiKey
	            )
	        ));
	    $return = $response->getBody();
	    // $return = json_decode($return, true);
	     
        return $return;
	}

	function groupCommentPostUnlikeProses(){
        $session = Session::get('data');
        $userID = $session['id'];
        $apiKey = $session['apiKey'];
        $group_post_comment_id = Input::get('group_post_comment_id');

        $myApp = App::make('serverLocation'); 
	    $server = $myApp->serverLocation ; //variable server
	    $link = 'groupCommentPostUnlikeProses'; // variable route yang dituju pada cms
	    $route =  $server.$link;
	     
	    $client = new GuzzleHttp\Client(['verify' => false]);
	    $response = $client->request('post', $route, array(
	           'form_params' => array(
	                'userID'  => $userID,
	                'apiKey'    => $apiKey,
	                'group_post_comment_id' => $group_post_comment_id,       
	            ),
	            'headers' => array(
	                'X-Authorization' => $apiKey
	            )
	        ));
	    $return = $response->getBody();
	    // $return = json_decode($return, true);
	     
        return $return;
	}

	public function sharedMyGroupProfile(){
		$group_name = Input::get('group_name');
		$group_description = Input::get('group_description');
		$group_picture = Input::get('group_picture');
		$group_id = Input::get('group_id');

		$session = Session::get('data');
		$userID = $session['id'];
		$apiKey = $session['apiKey'];
		$username = $session['username'];

		$myApp = App::make('serverLocation'); 
	    $server = $myApp->serverLocation ; //variable server
	    $link = 'sharedMyGroupProfile'; // variable route yang dituju pada cms
	    $route =  $server.$link;
	     
	    $client = new GuzzleHttp\Client(['verify' => false]);
	    $response = $client->request('post', $route, array(
	           'form_params' => array(
	                'userID'  => $userID,
	                'apiKey'    => $apiKey,
	                'group_name' => $group_name,    
	                'group_description' => $group_description,
	                'group_picture' => $group_picture,   
	                'group_id' => $group_id,
	                'myUsername' => $username,
	            ),
	            'headers' => array(
	                'X-Authorization' => $apiKey
	            )
	        ));
	    $return = $response->getBody();
	    // $return = json_decode($return, true);
	     
        return $return;
	}


}
