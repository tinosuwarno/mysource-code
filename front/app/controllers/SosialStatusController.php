<?php

class SosialStatusController extends \BaseController {

    //--------------------------------- FUNGSI FOLLOW ------------------------------//

    public function ConfirmMyFriend($id){
        // return $id;
        $session = Session::get('data');
        // return $session;

        $data = [
            'apiKey'        => $session['apiKey'],
            'user_id'       => $session['id'],
            'user_Confirm'  => $id
        ];

        $myGuzzle = new myGuzzle('confirmFriend');
        $myGuzzle->setHeader($session['apiKey']);
        $response = $myGuzzle->formParams($data, 'post');
        return $response;
    }

    public function UnFriendMyFriend($id){
        // return $id;
        $session = Session::get('data');
        // return $session;

        $data = [
            'apiKey' => $session['apiKey'],
            'user_id' => $session['id'],
            'user_unFriend' => $id
        ];

        $myGuzzle = new myGuzzle('unFriend');
        $myGuzzle->setHeader($session['apiKey']);
        $response = $myGuzzle->formParams($data, 'post');
        return $response;
        
    }

    public function FriendMyFriend($id){
        // return $id;
        $session = Session::get('data');
        // return $session;
        
        $data = [
            'apiKey' => $session['apiKey'],
            'user_id' => $session['id'],
            'user_Friend' => $id
        ];

        $myGuzzle = new myGuzzle('addFriend');
        $myGuzzle->setHeader($session['apiKey']);
        $response = $myGuzzle->formParams($data, 'post');
        return $response;
    }

    //------------------------------------------------------------------------------//

    //--------------------------------- FUNGSI FOLLOW ------------------------------//

	public function ConfirmMyFollower($id){
		// return $id;
        $session = Session::get('data');
        // return $session;

        $data = [
            'apiKey' => $session['apiKey'],
            'user_id' => $session['id'],
            'user_confirm' => $id
        ];

        $myGuzzle = new myGuzzle('ConfirmFollow');
        $myGuzzle->setHeader($session['apiKey']);
        $response = $myGuzzle->formParams($data, 'post');
        return $response;
	}

	public function UnFollowMyFollowing($id){
        // return $id;
        $session = Session::get('data');

        $data = [
            'apiKey' => $session['apiKey'],
            'user_id' => $session['id'],
            'user_unFollow' => $id
        ];

        $myGuzzle = new myGuzzle('unFollow');
        $myGuzzle->setHeader($session['apiKey']);
        $response = $myGuzzle->formParams($data, 'post');
        return $response;
	}

	public function FollowOtherPeople($id){
        // return $id;
        $session = Session::get('data');
        // return $session;
        
        $data = [
            'apiKey' => $session['apiKey'],
            'user_id' => $session['id'],
            'user_Follow' => $id
        ];

        $myGuzzle = new myGuzzle('Follow');
        $myGuzzle->setHeader($session['apiKey']);
        $response = $myGuzzle->formParams($data, 'post');
        return $response;
	}

    //------------------------------------------------------------------------------//
}
