<?php

class EventController extends \BaseController {


    public function landingEvent($id){
      $session = Session::get('data');
      $userID = $session['id'];
      $apiKey = $session['apiKey'];
    
      $skip = 0;
      $take = 6;
      // $orderBy = 'created_at';
      $optionOrder = 'desc';

      if ($id == 'new-event') {
        $orderBy = 'created_at';
        $category = 1;

        $data = [
          'userID' => $userID,
          'skip' => $skip,
          'take' => $take,
          'orderBy' => $orderBy,
          'optionOrder' => $optionOrder,
        ];

      $myGuzzle = new myGuzzle('event/LandingNewEvent');
      $myGuzzle->setHeader($apiKey);
      $return = $myGuzzle->formParams($data, 'post');
      
      $winner = $this->eventWinner();
      return View::make('event.landing-event')->with('category', $category)->with('return', $return)->with('winner', $winner);

      }else if($id =='most-popular') {
        $orderBy = 'liked';
        $category = 2;
      }else if($id == 'running-event'){
        $orderBy = 'created_at';
        $category = 3;

        $myApp = App::make('serverLocation'); 
        $server = $myApp->serverLocation ; //variable server
        $link = 'explore/EventExplore'; // variable route yang dituju pada cms
        $route =  $server.$link;
       
        $client = new GuzzleHttp\Client(['verify' => false]);
        $response = $client->request('post', $route, array(
              'form_params' => array(
                  'userID'  => $userID,
                  'skip'    => $skip,
                  'take'    => $take,
                  'orderBy' => $orderBy,
                  'optionOrder' => $optionOrder
              ),
              'headers' => array(
                  'X-Authorization' => $apiKey
              )
          ));
        $return = $response->getBody();
        $return = json_decode($return, true);
        
        $winner = $this->eventWinner();
      
        return View::make('event.landing-event')->with('category', $category)->with('return', $return)->with('winner', $winner);
      }
      else{
        return View::make('pages.errors.404');
      }
      
      $myApp = App::make('serverLocation'); 
      $server = $myApp->serverLocation ; //variable server
      $link = 'event/LandingEvent'; // variable route yang dituju pada cms
      $route =  $server.$link;
     
      $client = new GuzzleHttp\Client(['verify' => false]);
      $response = $client->request('post', $route, array(
            'form_params' => array(
                'userID'  => $userID,
                'skip'    => $skip,
                'take'    => $take,
                'orderBy' => $orderBy,
                'optionOrder' => $optionOrder
            ),
            'headers' => array(
                'X-Authorization' => $apiKey
            )
        ));
      $return = $response->getBody();
      $return = json_decode($return, true);


      // return $return[0]['data'];

      // if ($id == 'most-popular' && $return[0]['count']!= 0) {
      //   usort($return[0]['data'], function($a, $b) {
      //       return $b['peserta'] - $a['peserta'];
      //   });

      //   //return $return[0]['data'];
      // }

      // return $return;
      
      $winner = $this->eventWinner();
      
      return View::make('event.landing-event')->with('category', $category)->with('return', $return)->with('winner', $winner);
    }

    public function loadMore(){
      // return Input::all();
      $input = Input::all();

      $session = Session::get('data');
      $userID = $session['id'];
      $apiKey = $session['apiKey'];

      $data = [
        'userID'  => $userID,
        'skip'    => $input['skip'],
        'take'    => $input['take'],
        'orderBy' => $input['orderBy'],
        'optionOrder' => $input['optionOrder'],
        'apiKey' => $apiKey
      ];

      if($input['type'] == 1)
        if($input['category'] == 1 ){
           $myGuzzle = new myGuzzle('event/LandingNewEvent');
        }else if($input['category'] == 2){
            $myGuzzle = new myGuzzle('event/LandingEvent');
         }else if($input['category'] == 3){
            $myGuzzle = new myGuzzle('explore/EventController');
         }
        
      elseif($input['type'] == 2)
        $myGuzzle = new myGuzzle('event/winner');
      $myGuzzle->setHeader($session['apiKey']);
      $return = $myGuzzle->formParams($data, 'post');
      
      if($input['type'] == 1){
        $content = $this->contentEvent($return, $input);
        $count = $return[0]['count'];
      }
      elseif($input['type'] == 2){
        $content = $this->contentEventWinner($return, $input);
        $count = $return['count'];
      }

      $data = [
        'content' => $content,
        'skip' => $input['skip'] + $input['take'],
        'count' => $count
      ];

      return $data;
    }

    public function contentEventWinner($response, $input){
      $data = $response['data'];
      $count = $response['count'];
      $skip = $input['skip'];
      $take = $input['take'];

      $content = '';
      foreach ($data as $key => $value) {
        $content .= "
        <div class='col-md-6 gj-song-wrap'>
          <div class='gj-song-card'>
            <div class='gj-song-cover'>
              <img src='".asset($value['trackPic'])."' class='img-responsive'>
              <div class='gj-song-play'><i class='fa fa-play-circle-o fa-3x'></i></div>
              <div class='gj-song-btn text-center'><i class='fa fa-magic'></i><i class='gritjam-icons gritjam-icon-add-playlist'></i></div>
            </div>
            <div class='gj-song-desc'>
              <div class='gj-song-story'>
                <h1 class='gj-song-title'>".$value['trackTitle']."</h1>
                <h5 class='gj-song-username'><i class='fa fa-user'></i> ".$value['username']."</h5>
                <p class='gj-song-contributor'>Event: ".$value['eventTitle']."</p>
                <ul class='gj-song-author'>
                  <li>Original Song Title: ".$value['originalTrack']['name']."</li>
                  <li>Genre: ".$value['originalTrack']['genre']."</li>
                  <li>Original From: ".$value['originalTrack']['username']."</li>
                </ul>
              </div>
              <div class='gj-song-action'>
                <!-- ===<p><i class='fa fa-exclamation-circle' style='opacity:.5;'></i> <a href=''>Report/feedback</a></p>=== -->
                <div class='btn-report-track-song'><a class='btn-report-ts'><i class='fa fa-exclamation-triangle btn'></i></a><span>report</span></div>
                <div class='gj-played'>
                  <p>".$value['trackPlay']." plays</p>
                </div>
              </div>
              <div class='warp-stat-card'>
                <div class='stat-song-track'>
                  <div class='warp-ico-stat'>
                    <div class='ico-stat'><a href='javascript:void(0)' onclick='clickLike();'><i class='fa fa-heart like-single-list-song'></i></a></div>
                    <div class='num-stat'>".$value['trackLiked']."</div>
                  </div>
                </div>
                <div class='stat-song-track'>
                  <div class='warp-ico-stat'>
                    <div class='ico-stat'><a class='btn-share-ts'><i class='fa fa-mail-forward forward-single-list-song'></i></a></div>
                    <div class='num-stat'>".$value['trackShared']."</div>
                  </div>
                </div>
                <div class='stat-song-track'>
                  <div class='warp-ico-stat'>
                    <div class='ico-stat'><a href=''><i class='fa fa-magic spice-single-list-song'></i></a></div>
                    <div class='num-stat'>".$value['trackSpice']."</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <hr>
        </div>";
      }

      if($skip + $take < $count){
        $content .= '<div class="warp-btn-loadmore-list" id="loadEventMore'.$input['type'].'">
          <a class="btn-loadmore-list" onclick="loadMoreEvent('.$input['type'].','.$input['category'].')">
          <i class="fa fa-plus-circle"></i>
          <span class="text-load-more-list">LOAD MORE</span>
          </a>
        </div>';
      }
      return $content;
    }

    public function contentEvent($response, $input){
      $data = $response[0]['data'];
      $count = $response[0]['count'];
      $skip = $input['skip'];
      $take = $input['take'];

      $content = "";
      foreach ($data as $key => $value) {
        $statusMemberEventParticipatePrint = "
          <div class='btn-in-stat' id='groupEventParticipant".$value['event_id']."'>".
            "<input type='button' onclick='sudahParticipate();' class='btn btn-line-stat col-btn-ex' value='participated'>".
          "</div>";
        $statusMemberEventNotParticipatePrint = "
          <div class='btn-in-stat' id='groupEventParticipant".$value['event_id']."'>".
            "<button type='button' onclick='groupEventParticipant(".'"'.$value['groupID'].'"'.", ".'"'.$value['event_id'].'"'.", ".'"'.$value['event_title'].'"'.",".'"1"'.");' class='btn btn-line-stat col-btn-ex'>Participate</button>".
          "</div>";
        $statusMemberGroupJoin = "
          <div class='btn-in-stat' id='groupEventParticipant".$value['event_id']."'>".
            "<button type='button' onclick='harusJoinGroup(".'"'.$value['groupID'].'"'.", ".'"'.$value['event_id'].'"'.", ".'"'.$value['event_title'].'"'.",".'"'.$value['group_name'].'"'.");' class='btn btn-line-stat col-btn-ex'>Participate</button>".
          "</div>";        
        $statusOther = "<div class='btn-in-stat'></div>";    

        $content .= "
        <div class='col-md-6 gj-song-wrap'>
          <div class='gj-song-card'>
            <div class='gj-song-cover'>";
              if($value['picture_event']=='' || $value['picture_event']==null)
                $content .= "<img src='".asset('img/album/album-song_475x475.jpg')."' class='img-responsive'>";
              else
                $content .= "<img src='".asset($value['picture_event'])."' class='img-responsive'>";
$content .= "</div>
            <div class='gj-song-desc'>
              <div class='gj-song-story'>
                <h1 class='gj-song-title'><a href='event/".$value['event_id']."'>".$value['event_title']."</a></h1>
              </div>
              <div class='gj-text-desc'>
                <p class='revs'>".$value['description_event']."</p>
                <p>participant(s): ".$value['peserta']."</p>
              </div>
            </div>
            <div class='warp-stat-card top-stat-card'>
              <div class='stat-song-track' id='groupEventLiked".$value['event_id']."'>
                <div class='warp-ico-stat'>
                  <div class='ico-stat'>
                    <a href='javascript:void(0)'"; if($value['likedGroupEvent']==1){ $content .= "onclick='udahLikePost();'"; } else { $content .= "onclick='clickLikeGroupEvent(".$value['event_id'].",".$value['liked'].");'"; } $content .= ">";
          $content .= "<i class='fa fa-heart like-single-list-song'"; if($value['likedGroupEvent']==1){ $content .= "style='color:red;'";} $content .= "></i>";
        $content .= "</a>
                  </div>
                  <div class='num-stat'>".$value['liked']."</div>
                </div>
              </div>
              <div class='stat-song-track'>
                <div id='sharedEvent".$value['event_id']."' class='warp-ico-stat'>
                  <div class='ico-stat'><a onclick='sharedEventToSocial(".$value['event_id'].",".$value['shared'].");' style='cursor:pointer;'><i class='fa fa-mail-forward forward-single-list-song'></i></a></div>
                  <div class='num-stat'>".$value['shared']."</div>
                </div>
              </div>

              <span id='eventTitle".$value['event_id']."' style='display:none;'>".$value['event_title']."</span>
              <span id='eventDescription".$value['event_id']."' style='display:none;'>".$value['description_event']."</span>
              <span id='eventPicture".$value['event_id']."' style='display:none;'>".$value['picture_event']."</span>

              <div class='btn-in-stat' id='groupEventParticipant".$value['event_id']."'>
                <span id='trackID-groupEvent".$value['event_id']."' style='display:none;'>".$value['trackID']."</span>
                <span id='judulTrack-groupEvent".$value['event_id']."' style='display:none;'>".$value['judulTrack']."</span>
                <span id='instrumentTrack-groupEvent".$value['event_id']."' style='display:none;'>".$value['instrument']."</span>
                <span id='creatorTrack-groupEvent".$value['event_id']."' style='display:none;'>".$value['creatorTrack']."</span>
                <span id='priceTrack-groupEvent".$value['event_id']."' style='display:none;'>".$value['price']."</span>";
                 $time = time();
                 $tanggal = (date("Y-m-d"." "."H-m-s",$time));

                  if($value['checkadmin'] != 'admin'){
                    if($tanggal < $value['end_time']){
                          if($value['group_type'] === 1 || $value['group_type'] === '1'){  // check group public
                            if($value['checkStatusMember'] === 'member'){
                                if($value['event_type'] === 1 || $value['event_type'] === '1'){    //check event public
                                     if($value['statusMemberEventParticipate'] === 1 || $value['statusMemberEventParticipate'] === '1'){
                                          $content .= $statusMemberEventParticipatePrint;
                                     }else{
                                          $content .= $statusMemberEventNotParticipatePrint;
                                     }
                                }else{
                                     if($value['statusMemberEventParticipate'] === 1 || $value['statusMemberEventParticipate'] === '1'){
                                          $content .= $statusMemberEventParticipatePrint;
                                     }else{
                                         $content .= $statusMemberEventNotParticipatePrint;
                                     }
                                }
                            }else{
                                if($value['event_type'] === 1 || $value['event_type'] === '1'){
                                     if($value['statusMemberEventParticipate'] === 1 || $value['statusMemberEventParticipate'] === '1'){
                                         
                                          $content .= $statusMemberEventParticipatePrint;
                                     }else{
                                          $content .= $statusMemberEventNotParticipatePrint;
                                     }
                                }else{
                                     if($value['statusMemberEventParticipate'] === 1 || $value['statusMemberEventParticipate'] === '1'){
                                          $content .= $statusMemberEventParticipatePrint;
                                     }else{
                                          $content .= $statusOther;
                                     }
                                }
                            }
                       }else{
                            if($value['checkStatusMember'] === 'member'){
                                 if($value['event_type'] === 1 || $value['event_type'] === '1'){
                                      if($value['statusMemberEventParticipate'] === 1 || $value['statusMemberEventParticipate'] === '1'){
                                           $content .= $statusMemberEventParticipatePrint;
                                      }else{
                                           $content .= $statusMemberEventNotParticipatePrint;
                                      } 
                                 }else{
                                      if($value['statusMemberEventParticipate'] === 1 || $value['statusMemberEventParticipate'] === '1'){
                                           $content .= $statusMemberEventParticipatePrint;
                                      }else{
                                           $content .= $statusMemberEventNotParticipatePrint;
                                      }
                                 }
                            }else{
                                if($value['event_type'] === 1 || $value['event_type'] === '1'){
                                     if($value['statusMemberEventParticipate'] === 1 || $value['statusMemberEventParticipate'] === '1'){
                                          $content .= $statusMemberEventParticipatePrint;
                                     }else{
                                          $content .= $statusMemberGroupJoin;
                                     }
                                }else{
                                     if($value['statusMemberEventParticipate'] === 1 || $value['statusMemberEventParticipate'] === '1'){
                                          $content .= $statusMemberEventParticipatePrint;
                                     }else{
                                          $content .= $statusOther;
                                     }
                                }
                            }
                  
                       }
                    }else{
                      $content .= $statusOther;
                    }    
                  }else{
                     $content .= $statusOther;
                  } 
    $content .= "</div>
            </div>
            <div class='gj-song-revision'>
              <div class='name-group-post'>
                <i class='fa fa-group'></i><a href='' class='pru'><strong>".$value['group_name']."</strong></a>
              </div>
            </div>
          </div>
          <hr>
        </div>";
      }

      if($skip + $take < $count){
        $content .= '<div class="warp-btn-loadmore-list" id="loadEventMore'.$input['type'].'">
          <a class="btn-loadmore-list" onclick="loadMoreEvent('.$input['type'].','.$input['category'].')">
          <i class="fa fa-plus-circle"></i>
          <span class="text-load-more-list">LOAD MORE</span>
          </a>
        </div>';
      }

      return $content;
    }

    public function eventWinner(){
      $session = Session::get('data');
      $userID = $session['id'];
      $apiKey = $session['apiKey'];

      $myApp = App::make('serverLocation'); 
      $server = $myApp->serverLocation ; //variable server
      $link = 'event/winner'; // variable route yang dituju pada cms
      $route =  $server.$link;

      if (!null == (Input::get('skip'))) {
        $skip = Input::get('skip');
      } else {
        $skip = 0;
      }

      if (!null == (Input::get('take'))) {
        $take = Input::get('take');
      } else {
        $take = 6;
      }
     
      $client = new GuzzleHttp\Client(['verify' => false]);
      $response = $client->request('post', $route, array(
            'form_params' => array(
                'userID'  => $userID,
                'skip'    => $skip,
                'take'    => $take,
                'orderBy' => 'created_at',
                'optionOrder' => 'DESC',
                'apiKey'  => $apiKey

            ),
            'headers' => array(
                'X-Authorization' => $apiKey
            )
        ));
      $return = $response->getBody();
      $return = json_decode($return, true);
      return $return;
    }

    public function loadEvent(){
      $session = Session::get('data');
      $userID = $session['id'];
      $apiKey = $session['apiKey'];
    
      $skip = 0;
      $take = 6;
      $orderBy = 'created_at';
      $optionOrder = 'DESC';

      
      $skip = Input::get('skip');
      
      $myApp = App::make('serverLocation'); 
      $server = $myApp->serverLocation ; //variable server
      $link = 'event/EventExplore'; // variable route yang dituju pada cms
      $route =  $server.$link;
     
      $client = new GuzzleHttp\Client(['verify' => false]);
      $response = $client->request('post', $route, array(
            'form_params' => array(
                'userID'  => $userID,
                'skip'    => $skip,
                'take'    => $take,
                'orderBy' => $orderBy,
                'optionOrder' => $optionOrder
            ),
            'headers' => array(
                'X-Authorization' => $apiKey
            )
        ));
      $return = $response->getBody();
      $return = json_decode($return, true);
      
      return $return;
    }

    public function prosesJoinGroupEvent(){
        $groupID = trim(Input::get('groupID'));
        $groupEventID = trim(Input::get('groupEventID'));
        $eventTitle = trim(Input::get('eventTitle'));
        $trackID = trim(Input::get('trackID'));
        $judulTrack = trim(Input::get('judulTrack'));
        $creatorTrack = trim(Input::get('creatorTrack'));

        $session = Session::get('data');
        $userID = $session['id'];
        $username = $session['username'];
        $apiKey = $session['apiKey'];
        
        $recordController = new RecordController();
        $projectName = $recordController->createDefaultProjectName($username, $judulTrack, $creatorTrack);
        $projectDescription = "this is project from eevnt"+$eventTitle;
        
        
        $myApp = App::make('serverLocation'); 
        $server = $myApp->serverLocation ; //variable server
        $link = 'event/checkMyProject'; // variable route yang dituju pada cms
        $route =  $server.$link;
       
        $client = new GuzzleHttp\Client(['verify' => false]);
        $response = $client->request('post', $route, array(
              'form_params' => array(
                  'userID'  => $userID,
              ),
              'headers' => array(
                  'X-Authorization' => $apiKey
              )
          ));
        $return = $response->getBody();
        $return = json_decode($return, true);
        
        if($return == 1 || $return == '1' ){
            $myApp = App::make('serverLocation'); 
            $server = $myApp->serverLocation ; //variable server
            $link = 'buyTrack'; // variable route yang dituju pada cms
            $route =  $server.$link;
           
            $client = new GuzzleHttp\Client(['verify' => false]);
            $response = $client->request('post', $route, array(
                  'form_params' => array(
                      'userID'  => $userID,
                      'trackID' => $trackID,
                      'projectName' => $projectName,
                      'trackType' => 'karaoke',
                      'apiKey' => $apiKey,
                      'event_id' => $groupEventID
                  ),
                  'headers' => array(
                      'X-Authorization' => $apiKey
                  )
              ));
            $returnProject = $response->getBody();
            $returnProject = json_decode($returnProject, true);
            if($returnProject['status'] == 1 || $return == '1'){

                $myApp = App::make('serverLocation'); 
                $server = $myApp->serverLocation ; //variable server
                $link = 'groupEventParticipate'; // variable route yang dituju pada cms
                $route =  $server.$link;
               
                $client = new GuzzleHttp\Client(['verify' => false]);
                $response = $client->request('post', $route, array(
                      'form_params' => array(
                          'userID'  => $userID,
                          'groupID' => $groupID,
                          'status' => 1,
                          'groupEventID' => $groupEventID,
                          'apiKey' => $apiKey,
                      ),
                      'headers' => array(
                          'X-Authorization' => $apiKey
                      )
                  ));

                return $returnProject;     // sukses buy track.
            }else if($return == -1 || $return == '-1'){
                return -1; //'gak cukup duit';
            }else{
                return 2; //'error';
            }

        }else{
           return  0; //'kebanyakan project';
        }
    }

    public function detailsEvent($event_id){
       $cek_event_id = $this->checkEventID($event_id);
       $cek_group = $this->checkStatusGroupEventID($event_id);
       $dataEvent = $this->getDataEvent($event_id);
       $end_time = $dataEvent['end_time'];
       
       if($cek_event_id == 1 || $cek_event_id == '1'){
           if($cek_group == 1 || $cek_group == '1'){
               return View::make('event.detailsEvent')->with('end_time', $end_time);
           }else{
               return View::make('pages.errors.404');
           }
            
         }else{
             return View::make('pages.errors.404');
         }    
    }

    public function getDataEvent($event_id){
      $session = Session::get('data');
      $userID = $session['id'];
      $apiKey = $session['apiKey'];
      $data = [
         'event_id' => $event_id,
       ];

      $myGuzzle = new myGuzzle('event/getDataEvent');
      $myGuzzle->setHeader($apiKey);
      $return = $myGuzzle->formParams($data, 'post');
      return $return;
    }

    public function checkEventID($event_id){
        $session = Session::get('data');
        $userID  = $session['id'];
        $apiKey = $session['apiKey'];

        $myApp = App::make('serverLocation'); 
        $server = $myApp->serverLocation ; //variable server
        $link = 'event/checkEventID'; // variable route yang dituju pada cms
        $route =  $server.$link;
       
        $client = new GuzzleHttp\Client(['verify' => false]);
        $response = $client->request('post', $route, array(
              'form_params' => array(
                  'userID'  => $userID,
                  'event_id' => $event_id,
              ),
              'headers' => array(
                  'X-Authorization' => $apiKey
              )
          ));
        $return = $response->getBody();
        $return = json_decode($return, true);
        return $return;
    }

    public function checkStatusGroupEventID($event_id){
        $session = Session::get('data');
        $userID  = $session['id'];
        $apiKey = $session['apiKey'];

        $myApp = App::make('serverLocation'); 
        $server = $myApp->serverLocation ; //variable server
        $link = 'event/checkStatusGroupEventID'; // variable route yang dituju pada cms
        $route =  $server.$link;
       
        $client = new GuzzleHttp\Client(['verify' => false]);
        $response = $client->request('post', $route, array(
              'form_params' => array(
                  'userID'  => $userID,
                  'event_id' => $event_id,
              ),
              'headers' => array(
                  'X-Authorization' => $apiKey
              )
          ));
        $return = $response->getBody();
        $return = json_decode($return, true);
        return $return;
    }

    public function eventDetailsDescription(){
        $session = Session::get('data');
        $userID  = $session['id'];
        $apiKey = $session['apiKey'];

        $event_id = Input::get('event_id');

        $myApp = App::make('serverLocation'); 
        $server = $myApp->serverLocation ; //variable server
        $link = 'event/eventDetailsDescription'; // variable route yang dituju pada cms
        $route =  $server.$link;
       
        $client = new GuzzleHttp\Client(['verify' => false]);
        $response = $client->request('post', $route, array(
              'form_params' => array(
                  'userID'  => $userID,
                  'event_id' => $event_id,
              ),
              'headers' => array(
                  'X-Authorization' => $apiKey
              )
          ));
        $return = $response->getBody();
        $return = json_decode($return, true);
        return $return;
    }

    public function SongEventRelease(){
        $session = Session::get('data');
        $userID  = $session['id'];
        $apiKey = $session['apiKey'];

        $event_id = Input::get('event_id');
        $skip = Input::get('skip');
        $take = Input::get('take');

        $myApp = App::make('serverLocation'); 
        $server = $myApp->serverLocation ; //variable server
        $link = 'event/SongEventRelease'; // variable route yang dituju pada cms
        $route =  $server.$link;
       
        $client = new GuzzleHttp\Client(['verify' => false]);
        $response = $client->request('post', $route, array(
              'form_params' => array(
                  'userID'  => $userID,
                  'event_id' => $event_id,
                  'skip' => $skip,
                  'take' => $take,
              ),
              'headers' => array(
                  'X-Authorization' => $apiKey
              )
          ));
        $return = $response->getBody();
        $return = json_decode($return, true);
        return $return;
    }

    public function winnerGroupEvent(){
        $session = Session::get('data');
        $userID  = $session['id'];
        $apiKey = $session['apiKey'];

        $event_id = Input::get('event_id');

        $myApp = App::make('serverLocation'); 
        $server = $myApp->serverLocation ; //variable server
        $link = 'event/winnerGroupEvent'; // variable route yang dituju pada cms
        $route =  $server.$link;
       
        $client = new GuzzleHttp\Client(['verify' => false]);
        $response = $client->request('post', $route, array(
              'form_params' => array(
                  'userID'  => $userID,
                  'event_id' => $event_id,
              ),
              'headers' => array(
                  'X-Authorization' => $apiKey
              )
          ));
        $return = $response->getBody();
        $return = json_decode($return, true);
        return $return;
    }

    public function submitEventWinner(){
        $session = Session::get('data');
        $userID  = $session['id'];
        $apiKey = $session['apiKey'];

        $event_id = Input::get('event_id');
        $trackID = Input::get('trackID');

        $myApp = App::make('serverLocation'); 
        $server = $myApp->serverLocation ; //variable server
        $link = 'event/submitEventWinner'; // variable route yang dituju pada cms
        $route =  $server.$link;
       
        $client = new GuzzleHttp\Client(['verify' => false]);
        $response = $client->request('post', $route, array(
              'form_params' => array(
                  'userID'  => $userID,
                  'event_id' => $event_id,
                  'trackID' => $trackID,
              ),
              'headers' => array(
                  'X-Authorization' => $apiKey
              )
          ));
        $return = $response->getBody();
        $return = json_decode($return, true);
        return $return;
    }

    public function sendEmailToEventWinner(){
        $session = Session::get('data');
        $userID = $session['id'];
        $apiKey = $session['apiKey'];

        $email = Input::get('email');
        $username_winner = Input::get('username_winner');
        $username_admin = Input::get('username_admin');
        $text_area = Input::get('text_area');
        $event_title = Input::get('event_title');

        $myApp = App::make('serverLocation'); 
        $server = $myApp->serverLocation ; //variable server
        $link = 'event/sendEmailToEventWinner'; // variable route yang dituju pada cms
        $route =  $server.$link;
       
        $client = new GuzzleHttp\Client(['verify' => false]);
        $response = $client->request('post', $route, array(
              'form_params' => array(
                  'userID'  => $userID,
                  'email' => $email,
                  'username_winner' => $username_winner,
                  'username_admin' => $username_admin,
                  'text_area' => $text_area,
                  'event_title' => $event_title,
              ),
              'headers' => array(
                  'X-Authorization' => $apiKey
              )
          ));
        $return = $response->getBody();
        $return = json_decode($return, true);
        return $return;
    }

    public function participatedEventNotReleaseSong(){
        $session = Session::get('data');
        $userID = $session['id'];
        $apiKey = $session['apiKey'];

        $event_id = Input::get('event_id');
        $skip = Input::get('skip');
        $take = Input::get('take');

        $myApp = App::make('serverLocation'); 
        $server = $myApp->serverLocation ; //variable server
        $link = 'event/participatedEventNotReleaseSong'; // variable route yang dituju pada cms
        $route =  $server.$link;
       
        $client = new GuzzleHttp\Client(['verify' => false]);
        $response = $client->request('post', $route, array(
              'form_params' => array(
                  'userID'  => $userID,
                  'event_id' => $event_id,
                  'skip' => $skip,
                  'take' => $take,
              ),
              'headers' => array(
                  'X-Authorization' => $apiKey
              )
          ));
        $return = $response->getBody();
        $return = json_decode($return, true);
        return $return;
    }

    public function hitungSharedEvent(){
       $session = Session::get('data');
       $userID = $session['id'];
       $apiKey = $session['apiKey'];

       $event_id = Input::get('eventID');

       $myApp = App::make('serverLocation');
       $server = $myApp->serverLocation;
       $link = 'event/hitungSharedEvent';
       $route = $server.$link;

       $client = new GuzzleHttp\Client(['verify' => false]);
       $response = $client->request('post', $route, array(
              'form_params' => array(
                  'userID'  => $userID,
                  'event_id' => $event_id,
              ),
              'headers' => array(
                  'X-Authorization' => $apiKey
              )
          ));
        $return = $response->getBody();
        $return = json_decode($return, true);
        return $return;
    } 

}
