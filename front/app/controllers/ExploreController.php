<?php

class ExploreController extends \BaseController {

	public function explorer(){
		return View::make('pages.explore');
	}

	public function friend(){
		$session = Session::get('data');
		$userID = $session['id'];
    $type = Input::get('type');

    if(empty($type)) return -1;

    if(empty($userID)){
			$data = [
        'type' => $type
      ];

      $guzzle = new myGuzzle('explore/getFriendExplore1');
      $return = $guzzle->formParams($data, 'post');
			// return $return;

      if($type == 2) return -1;
		}
		else{
			$session = Session::get('data');
	    $userID = $session['id'];
	    $apiKey = $session['apiKey'];
		    
	    $data = [
				    'userID' 	=> $userID,
		        'apiKey'	=> $apiKey,
            'type'    => $type
			];
			
			$guzzle = new myGuzzle('explore/getFriendExplore2');
			$guzzle->setHeader($apiKey);
			$return = $guzzle->formParams($data, 'post');
			// return $return;
      if($return == -2) return -2;
		}

		$content = $this->friendContent($return);
		return $content;
	}

	public function friendContent($data){
		$content = '';
		foreach ($data as $key => $value) {
$content .= "<div class='item warp-disc-user'>
              <div class='disc-user-header'>";

                if(empty($value['bg_image'])){
                	$content .= "<div class='disc-header-bg' style='background-image:url(img/blur-bg-profile/bg-profile-3.jpg);'>";
                }
                else{
                	$content .= "<div class='disc-header-bg' style='background-image:url(".$value['bg_image'].");'>";
                }

    $content .= "</div>
    			<div class='tile-card-bg'></div>
                <div class='disc-user-profile'>
                  <div class='img-disc-user'>";
                    if(empty($value['profile_image'])){
                    	$content .= "<img src='".asset('img/pp.png')."' class='img-responsive img-circle'>";
                    }
                    else{
                    	$content .= "<img src='".asset($value['profile_image'])."' class='img-responsive img-circle'>";
                    }
      $content .= "</div>
                  <div class='desc-disc-user'>
                    <div class='disc-user-name'><a data-pjax='yes' id='pjax' href='/".$value['username']."'>".$value['name']."</a></div>
                    <div class='disc-user-username'><a data-pjax='yes' id='pjax' href='/".$value['username']."'>".$value['username']."</a></div>
                    <div class='warp-btn-friend-list'>";

      if (isset($value['isFriend']) && $value['isFriend'] == 1) {
        $content .= "<button id='".$value['userID']."' type='button' class='btn btn-add-f-list col-btn-ex unFriend'><i class='fa fa-check'></i>Friend</button>";
      } else if(isset($value['isFriend']) && $value['isFriend'] == 0){
        $content .= "<button id='".$value['userID']."' type='button' class='btn btn-add-f-list col-btn-ex'><i class='fa fa-check'></i>Request sent</button>";
      } else{
        $content .= "<button id='".$value['userID']."' type='button' class='btn btn-add-f-list col-btn-ex addFriend'><i class='fa fa-user-plus'></i>Add Friend</button>";
      }

      $content .= "</div>
                  </div>
                </div>
              </div>
              <div class='disc-user-body'>
                <div class='aside-card-section'>
                  <div class='aside-card-header'><strong>About me</strong></div>
                  <div class='aside-card-body'>
                    <p>";
                    if(empty($value['biography'])){
                    	$content .= "Biography not set yet";
                    }
                    else{
                    	$content .= $value['biography'];
                    }
        $content .= "</p>
                  </div>
                </div>
                <div class='aside-card-section'>
                  <div class='aside-card-header'><strong>My Talent</strong></div>
                  <div class='aside-card-body'>
                    <p>";
                    if(empty($value['instrument'])){
                    	$content .= "Talent not set yet";
                    }
                    else{
                    	$content .= $value['instrument'];
                    }
    	$content .= "</p>
                  </div>
                </div>
                <div class='aside-card-section'>
                  <div class='aside-card-header'><strong>Genre</strong></div>
                  <div class='aside-card-body'>
                    <p>";
                    if(empty($value['genre'])){
                    	$content .= "Genre not set yet";
                    }
                    else{
                    	$content .= $value['genre'];
                    }
    	$content .= "</p>
                  </div>
                </div>
                <div class='card-user-more'><a href='/".$value['username']."'>more about</a></div>
              </div>
            </div>";
		}
		return $content;
	}

  public function EventExplore(){
      $session = Session::get('data');
      $userID = $session['id'];
      $apiKey = $session['apiKey'];
    
      $skip = Input::get('skip');
      $take = Input::get('take');
      $orderBy = Input::get('orderBy');
      $optionOrder = Input::get('optionOrder');

      if(!empty($userID) || $userID != ''){
        $myApp = App::make('serverLocation'); 
        $server = $myApp->serverLocation ; //variable server
        $link = 'explore/EventExplore'; // variable route yang dituju pada cms
        $route =  $server.$link;
       
        $client = new GuzzleHttp\Client(['verify' => false]);
        $response = $client->request('post', $route, array(
              'form_params' => array(
                  'userID'  => $userID,
                  'skip'    => $skip,
                  'take'    => $take,
                  'orderBy' => $orderBy,
                  'optionOrder' => $optionOrder
              ),
              'headers' => array(
                  'X-Authorization' => $apiKey
              )
          ));
        $return = $response->getBody();
        $return = json_decode($return, true);
        return $return;
      }else{
        $myApp = App::make('serverLocation'); 
        $server = $myApp->serverLocation ; //variable server
        $link = 'explore/EventExplore2'; // variable route yang dituju pada cms
        $route =  $server.$link;
       
        $client = new GuzzleHttp\Client(['verify' => false]);
        $response = $client->request('post', $route, array(
              'form_params' => array(
                  'skip'    => $skip,
                  'take'    => $take,
                  'orderBy' => $orderBy,
                  'optionOrder' => $optionOrder
              )
          ));
        $return = $response->getBody();
        $return = json_decode($return, true);
        return $return;
      }     
  }

   public function invitationExplore(){
       $session = Session::get('data');
       $userID = $session['id'];
       $apiKey = $session['apiKey'];
       
       $skip = Input::get('skip');
       $take = Input::get('take');
       $orderBy = Input::get('orderBy');
       $optionOrder = Input::get('optionOrder');

       if(!empty($userID) || $userID != ''){
            $myApp = App::make('serverLocation'); 
            $server = $myApp->serverLocation ; //variable server
            $link = 'explore/invitationExplore'; // variable route yang dituju pada cms
            $route =  $server.$link;
           
            $client = new GuzzleHttp\Client(['verify' => false]);
            $response = $client->request('post', $route, array(
                  'form_params' => array(
                      'userID'  => $userID,
                      'skip'    => $skip,
                      'take'    => $take,
                      'orderBy' => $orderBy,
                      'optionOrder' => $optionOrder
                  ),
                  'headers' => array(
                      'X-Authorization' => $apiKey
                  )
              ));
            $return = $response->getBody();
            $return = json_decode($return, true);
            return $return;
       }else{
            $myApp = App::make('serverLocation'); 
            $server = $myApp->serverLocation ; //variable server
            $link = 'explore/invitationExplore2'; // variable route yang dituju pada cms
            $route =  $server.$link;
           
            $client = new GuzzleHttp\Client(['verify' => false]);
            $response = $client->request('post', $route, array(
                  'form_params' => array(
                      'skip'    => $skip,
                      'take'    => $take,
                      'orderBy' => $orderBy,
                      'optionOrder' => $optionOrder
                  )
              ));
            $return = $response->getBody();
            $return = json_decode($return, true);
            return $return;
       }
   }

}
