<?php


class AuthController extends \BaseController {

    public function completeActivation($activation_id){

        $myApp = App::make('serverLocation'); 
        $server = $myApp->serverLocation ; //variable server
        $link = 'completeMyActivation'; // variable route yang dituju pada cms
        $route =  $server.$link;

        $client = new GuzzleHttp\Client(['verify' => false]);
        // $response = $client->request('post', 'localhost:8080/completeMyActivation', array(
        $response = $client->request('post', $route, array(
            'form_params' => array(
                'activation_id' => $activation_id
            )
        ));
        // echo $response->getBody();
        $return = $response->getBody();
        if($return == 1){
            return Redirect::to('/');
        }
        else{
            //activation not complete / not activated
            return 0;
        }
    }

    public function RegisterMeProses(){
        // print_r(Input::all());
        $rules = array(
            'email'         => 'required',
            'password'      => 'required',
            'username'      => 'required',
            'first_name'    => 'required',
            'last_name'     => 'required',
            'address'       => 'required',
            'province'      => 'required',
            'city'          => 'required'
        );

        $validator = Validator::make(Input::all(), $rules);

        if($validator->fails()){
            return 'sdadadad';
        }
        else{
            $realpath        = Input::file('photo')->getRealPath();
            $extention       = Input::file('photo')->getClientOriginalExtension();
            $filename        = rand(10000,99999) . "." . $extention;
            $destinationpath = 'public/upload_image';

            try {
                Input::file('photo') -> move($destinationpath, $filename);
            }
            catch (Exception $e){
                Session::flash('message_error', 'The File is not saved, please try again.');
                return Redirect::to('upload/addFile')->withErrors($e)->withInput(Input::all());
            }

            $filenow = $destinationpath . "/" . $filename;
            echo $filenow;

            $myApp  = App::make('serverLocation'); 
            $server = $myApp->serverLocation ; //variable server
            $link   = 'registerMyData'; // variable route yang dituju pada cms
            $route  =  $server.$link;
            
            $client = new GuzzleHttp\Client(['verify' => false]);
            // $response = $client->request('post', 'localhost:8080/registerMyData', [
            $response = $client->request('post', $route, [    
                'form_params' => [
                    'email'         => Input::get('email'),
                    'password'      => Input::get('password'),
                    'username'      => Input::get('username'),
                    'firstname'     => Input::get('first_name'),
                    'lastname'      => Input::get('last_name'),
                    'address'       => Input::get('address'),
                    'province'      => Input::get('province'),
                    'city'          => Input::get('city'),
                    'profile_image' => $filenow
                ]
            ]);
            // echo $response->getBody();
            $activation_id = $response->getBody();
            $activation_id = json_decode($activation_id);
            // $activation_id = "2y10rLVTyvvwPiY4oYxUNvQFyOJU5eznzyWg8J6HnUVbtv92aEFKg56";
            if($activation_id != '-1'){
                $data = array(
                    'firstname'     =>'Fadhlan', 
                    'activation_id' => $activation_id
                );
                // echo $activation_id;
                Mail::queue('emails.auth.reminder', $data, function($message){
                    $message->to(
                        'fadhlan.fari@gmail.com', 
                        'Fadhlan'
                        .' '.
                        'Rizal')->subject('Welcome to Gritjam.com');
                });
                Session::flash('message_success', 'Email Sent');
                return Redirect::to('/');
            }

            return 'Data tidak ke save wkwk';
        }
    }

    public function doLoginGoogle_front(){
        try {
            OAuth::login('google', function($user, $details){
                $email            = $details->email;
                $nickname         = $details->nickname;
                $name             = $details->full_name;
                $profile_image    = $details->avatar;

                $data = User::where('email', '=', $email)->first();
                if(sizeof($data)==0){
                    $user->email            = $email;
                    $user->nickname         = $nickname;
                    $user->name             = $name;
                    $user->profile_image    = $profile_image;
                    $user->save();
                }

                $myApp = App::make('serverLocation'); 
                $server = $myApp->serverLocation ; //variable server
                $link = 'sosial_media/loginBro'; // variable route yang dituju pada cms
                $route =  $server.$link;

                $client = new GuzzleHttp\Client(['verify' => false]);
                // $response = $client->request('POST', 'localhost:8080/sosial_media/loginBro', array(
                $response = $client->request('post', $route, array(
                    'form_params' => array(
                        'email'         => $details->email,
                        'nickname'      => $details->nickname,
                        'name'          => $details->full_name,
                        'profile_image' => $details->avatar
                    )
                ));
                // echo $response->getBody();
                // die();
                $return = $response->getBody();
                $return = json_decode($return, true);
                Session::put('apiKey', $return);
            });
        } catch (ApplicationRejectedException $e) {
            // User rejected application
        } catch (InvalidAuthorizationCodeException $e) {
            // Authorization was attempted with invalid
            // code,likely forgery attempt
        }
        //get apiKey
        $apiKey = Session::pull('apiKey');
        // print_r($apiKey); 
        $myApp = App::make('serverLocation'); 
        $server = $myApp->serverLocation ; //variable server
        $link = 'getUserDataBroo'; // variable route yang dituju pada cms
        $route =  $server.$link;    

        $client = new GuzzleHttp\Client(['verify' => false]);
        // $response = $client->request('post', 'localhost:8080/getUserDataBroo', array(
        $response = $client->request('post', $route, array(
            'form_params' => array(
                'apiKey' => $apiKey['apiKey']
            ),
            'headers' => array(
                'X-Authorization' => $apiKey['apiKey']
            )
        ));
        $return = $response->getBody();
        // echo $return;
        $return = json_decode($return, true);
        // print_r($return);

        Session::put('data', $return);
        return Redirect::to('myWall');
    }

    public function doLoginFacebook_front(){     
        try {
            OAuth::login('facebook', function($user, $details){
                $user->email            = $details->email;
                $user->nickname         = $details->nickname;
                $user->name             = $details->full_name;
                $user->profile_image    = $details->avatar;
                $user->save();

                $myApp = App::make('serverLocation'); 
                $server = $myApp->serverLocation ; //variable server
                $link = 'sosial_media/loginBro'; // variable route yang dituju pada cms
                $route =  $server.$link;    

                $client = new GuzzleHttp\Client(['verify' => false]);
                // $response = $client->request('POST', 'localhost:8080/sosial_media/loginBro', array(
                $response = $client->request('post', $route, array(
                    'form_params' => array(
                        'email'         => $details->email,
                        'nickname'      => $details->nickname,
                        'name'          => $details->full_name,
                        'profile_image' => $details->avatar
                    )
                ));
                // echo $response->getBody(); die();
                $return = $response->getBody();
                $return = json_decode($return, true);
                Session::put('apiKey', $return);
            });
        } catch (ApplicationRejectedException $e) {
            // User rejected application
        } catch (InvalidAuthorizationCodeException $e) {
            // Authorization was attempted with invalid
            // code,likely forgery attempt
        }
        //get apiKey
        $apiKey = Session::pull('apiKey');

        $myApp = App::make('serverLocation'); 
        $server = $myApp->serverLocation ; //variable server
        $link = 'getUserDataBroo'; // variable route yang dituju pada cms
        $route =  $server.$link; 

        $client = new GuzzleHttp\Client(['verify' => false]);
        // $response = $client->request('post', 'localhost:8080/getUserDataBroo', array(
        $response = $client->request('post', $route, array(
            'form_params' => array(
                'apiKey' => $apiKey['apiKey']
            ),
            'headers' => array(
                'X-Authorization' => $apiKey['apiKey']
            )
        ));

        // echo $response->getBody();
        $return = $response->getBody();
        $return = json_decode($return, true);

        Session::put('data', $return);
        return Redirect::to('myWall');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function doLogin(){
        $email = Input::get('inputEmail');
        $password = Input::get('inputPassword');
        // echo "email = " . $email ."<br>";
        // echo "password = " . $password . "<br>";

        $rules = array(
            'inputEmail' => 'required',
            'inputPassword' => 'required'
        );

        $validator = Validator::make(Input::all(), $rules);

        if($validator->fails()){
            return $password;
        }
        else{

            // $encryptController = new EncryptController();
            // $passwordEncrypt = $encryptController->encryptValue($password, 1, 3);
            // $emailEncrypt = $encryptController->encryptValue($email, 1, 2);

            $data = array(
                'inputEmail' => $email, 
                'inputPassword' => $password
            );

            $guzzle = new myGuzzle('doLogin_front');
            $return = $guzzle->formParams($data, 'post');
            // print_r($return);
            // die();

            if($return == 0){
                // Gagal Login
                //give alert
                return Redirect::to('login')->with('message_errorSign', '1');
            }
            else if($return == -1){
                // Tidak Ada user dengan email seperti itu
                //give alert
                return Redirect::to('login')->with('message_errorSign', '1');
            }

            Session::put('data', $return);
            return Redirect::to('myWall');
        }
    }

    public function doLogout(){
        $apiKey = Input::get('apiKey');
        // $id = Input::get('id');
        $myApp = App::make('serverLocation'); 
        $server = $myApp->serverLocation ; //variable server
        $link = 'doLogout_front'; // variable route yang dituju pada cms
        $route =  $server.$link; 

        $client = new GuzzleHttp\Client(['verify' => false]);

        try{
            // $response = $client->request('post','localhost:8080/doLogout_front', array(
            $response = $client->request('post', $route, array(  
                'form_params' => array(
                    'apiKey' => $apiKey
                    // 'id' => $id
                ),
                'headers' => array(
                    'X-Authorization' => $apiKey
                )
            ));
        }
        catch (RequestException $e) {
            echo $e->getRequest();
            if ($e->hasResponse()) {
                echo $e->getResponse();
            }
        }
        $return = $response->getBody();
        $return = json_decode($return, true);
        // echo $response->getBody();
        if($return['response']['http_code'] == 200){
            Session::flush();
            // Session::forget('lol');
            // Session::forget('test');
            return Redirect::to('/');
        }
        else{
            Session::flash('message_error', 'Can\'t Logout');
            return Redirect::to('home');
        }
    }

}
