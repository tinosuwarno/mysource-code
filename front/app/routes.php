<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/



//Explore
Route::get('explore', 'ExploreController@explorer');
Route::post('explorer/friend','ExploreController@friend');
Route::post('EventExplore', 'ExploreController@EventExplore');
Route::post('invitationExplore', 'ExploreController@invitationExplore');


Route::group(array('before' => 'authUser'), function(){
    //EVENT
    Route::get('events/{id}', 'EventController@landingEvent');
    Route::post('loadMoreEvent', 'EventController@loadMore');

    Route::post('prosesJoinGroupEvent', 'EventController@prosesJoinGroupEvent');
    Route::get('event/{event_id}', 'EventController@detailsEvent');
    Route::post('eventDetailsDescription', 'EventController@eventDetailsDescription');
    Route::post('SongEventRelease', 'EventController@SongEventRelease');
    Route::post('winnerGroupEvent', 'EventController@winnerGroupEvent');
    Route::post('submitEventWinner', 'EventController@submitEventWinner');
    Route::post('sendEmailToEventWinner', 'EventController@sendEmailToEventWinner');
    Route::post('participatedEventNotReleaseSong', 'EventController@participatedEventNotReleaseSong');
    Route::post('hitungSharedEvent', 'EventController@hitungSharedEvent');

    
    
});