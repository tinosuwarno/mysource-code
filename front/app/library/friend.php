<?php
// to do 
// handle modal adding song / track to status
// handle event

function getContent_friendWall($username){
    $session = Session::get('data');
    $skip = 0;
    $take = 1;

    $data = [
        'userID' => $session['id'],
        'apiKey' => $session['apiKey'],
        'username' => $username,
        'skip' => $skip,
        'take' => $take
    ];

    $myGuzzle = new myGuzzle('getFriendAbout');
    $myGuzzle->setHeader($session['apiKey']);
    $return = $myGuzzle->formParams($data, 'post');

    $data = $return['data'];
    $count = $return['count'];
    if($count != 0){
        foreach ($data as $key => $value) {
            printPost_friendWall($value, $session, $key);
        }
        $skip = $skip + $take;
        if($skip < $count)
            setLoadMore_friendWall($skip, $take);
    }
    else{
        echo "<div class='nf-item-wall'>
                <p>Don't Have any Post Yet</p>
            </div>";
    }
}

function setContent_friendWall($content, $counter, $type){
    if(strlen($content) <= 144){
        return "<p>".$content."</p>";
    }
    else{
        $newContent = "<p>" . substr($content, 0, 144);
        if($type == 1){
            $newContent .= "<span id='moreComment".$counter."'>...</span></p>
            <div id='moreContentComment".$counter."'";
        }
        else{
            $newContent .= "<span id='morePost".$counter."'>...</span></p>
                <div id='moreContentPost".$counter."'";
        }
        $strlength = strlen($content);
        $contentLeft = substr($content, 144, $strlength);
        $newContent .= " class='tb-more'>
                    <a style='cursor: pointer' onclick='seeMoreDesc_friendWall(".$type.",".$counter.",\"".$contentLeft."\")'>more</a>
                </div>";
        return $newContent;
    }
}

function setGenre_friendWall($data){
    $genre = $data['genre'];
    if(empty($data['subgenre']) || $data['subgenre'] == null) return $genre;
    else return $genre . " - " . $data['subgenre'];
}

function time_elapsed_string_friendWall($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}

function printComments_friendWall($comments){
    $count = $comments['commentCount'];
    
    if($count == 0) return '';

    $comment = $comments['commentAll'];
    $content = "
    	<div class='tb-comment'>
			<div class='hr'></div>
            <div class='tb-pp'>
                <a href=''><img src='".$comment[0]['profPic']."' class='img-responsive img-rounded-full'></a>
            </div>
            <div class='tb-info'>
                <h3><a href=''>".$comment[0]['name']."</a></h3>
            </div>
            <div class='tb-time'>
                <p>". time_elapsed_string_friendWall($comment[0]['createdComment']) ."</p>
            </div>
            <div class='tb-caption'>
                ".setContent_friendWall($comment[0]['commentContent'], 0, 1)."
                <div class='tb-like text-right'>
                    ";
    $likeID = $comment[0]['commentsID'] . '0';
    if($comment[0]['liked'] == false){
        $content .= "<a id='like".$likeID."' style='cursor: pointer' onclick='likeComment_friendWall(".$comment[0]['commentsID'].",1, ".$likeID.");'>Like</a>";
    }
    else{
        $content .= "<a id='unlike".$likeID."' style='cursor: pointer' onclick='likeComment_friendWall(".$comment[0]['commentsID'].",2, ".$likeID.");'>unLike</a>";
    }
    $content .= "<a href=''>Reply</a>
                </div>
            </div>
		</div>
    ";
    return $content;

}

function printPost_friendWall($data, $session, $counter){
    if(sizeof($data) == 0) return false;
    echo "  <div class='timeline-box'>
                <hr class='tb-hr'>
                <div class='tb-header'>
                    <div class='tb-pp'>
                        <a href=''><img src='". $data['profPic'] ."' class='img-responsive img-rounded-full'></a>
                    </div>
                    <div class='tb-info'>
                        <h3><a href=''>". $data['name'] ."</a></h3>
                        <h1><a href='' class='btn-single-post-tl'>". $data['trackDetails']['judul'] ."</a></h1>

                    </div>
                    <div class='tb-time'>
                        <p>". time_elapsed_string_friendWall($data['createdPost']) ."</p>
                    </div>
                </div>
                <div class='tb-content'>
                    <div class='tb-img'>
                        <img src='". getPicture($data['trackDetails']['picture'])."' class='img-responsive img-full-responsive'>
                    </div>
                    <div class='warp-entry-com'>
                        <div class='tb-bname'>
                            <h2>".$data['trackDetails']['username']."</h2>
                            <h5>".setGenre_friendWall($data['trackDetails'])."</h5>
                        </div>
                        <div class='tb-caption'>
                            ".setContent_friendWall($data['postCont'], $counter, 2)."
                        </div>
                        <div class='tb-action'>
                            <ul>
                                <li><a href='#'><i class='fa fa-heart'></i></a><span>".$data['trackDetails']['like']."</span></li>
                                <li><a href='#'><i class='fa fa-comments'></i></a><span>".$data['trackDetails']['commentSum']."</span></li>
                                <li><a href='#'><i class='fa fa-mail-forward'></i></a><span>".$data['trackDetails']['shared']."</span></li>
                                <li><a href='#'><i class='fa fa-music'></i></a><span>".$data['trackDetails']['buy']."</span></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class='warp-all-comment-post' id='moreComment_friendWallHere".$counter."'>"; if($data['comments']['commentCount'] > 1) { $comments = json_encode($data['comments']); echo "
                    <div id='moreComments_friendWall".$counter."' style='cursor: pointer' class='tb-more-comment ext-left'>
                        <a class='more-txt' id='moreComments_friendWall".$counter."' onclick='moreComment_friendWall(".$comments.",".$counter.")'>read more comments</a>
                    </div>"; } echo printComments_friendWall($data['comments']) . "
                </div>
                <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sxs-12'>
                    <div id='formComment".$counter."' class='row'>
                        <div class='hr'></div>
                        <form id='createComment".$counter."' onsubmit='return submitCommentFunction_friendWall(".$counter.");' class='warp-comment-post'>
                            <div class='col-lg-1 col-md-1 col-sm-1 col-xs-1 col-sxs-2 padten'>
                                <div class='warp-user-comment-post'>
                                    <img id='imageProf' src='".$session['profile_image']."' class='img-responsive img-rounded-full'>
                                </div>
                            </div>
                            <input type='hidden' name='postID' value='".$data['postID']."'></input>
                            <div class='col-lg-9 col-md-9 col-sm-9 col-xs-9 col-sxs-7 pad-xs-phone'>
                                <textarea name='commentContent' class='form-control' placeholder='Write Comments ...'></textarea>
                            </div>
                            <div class='col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 related-md-desktop'>
                                <button type='submit' class='btn btn-comment-post'>Comment</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>";
}

function setLoadMore_friendWall($skip){
    echo "<div id='loadMore_friendWallPost' class='col-xs-12'>
            <div  class='warp-btn-loadmore-list'>
                <a onclick='getMorePost_friendWall(". $skip .")' class='btn-loadmore-list'>
                    <i class='fa fa-plus-circle'></i>
                    <span class='text-load-more-list'>LOAD MORE</span>
                </a> 
             </div>
        </div>";
}
?>