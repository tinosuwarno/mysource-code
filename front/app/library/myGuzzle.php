<?php

class myGuzzle{
	protected $controllerLocation;
	protected $serverLocation;
	private $headers, $apiKey;
	
	public function __construct($controllerLocation){
		$this->setServerLocation();
		$this->controllerLocation = $controllerLocation;
	}

	public function setServerLocation(){
		$myApp = App::make('serverLocation');
		$this->serverLocation = $myApp->serverLocation;
	}

	public function setHeader($apiKey){
		$this->headers = [
			'X-Authorization' => $apiKey
		];
	}
	
	public function getControllerLocation(){
		return $this->controllerLocation;
	}

	public function formParams($dataToPass, $request){
		// return $dataToPass;
		// return $request;
		// return $this->controllerLocation;
		$client = new GuzzleHttp\Client(['verify' => false]);
		$controllerLocation = $this->serverLocation . $this->controllerLocation;
		// echo $controllerLocation; die();
		
		$data = $this->sendRequestData($dataToPass, $request);
		if($data == -1) return -1;
		$response = $client->request($request, $controllerLocation, $data);

		$return = $response->getBody();
		$return = json_decode($return, true);
		return $return;
	}

	private function sendRequestData($data, $request){
		$dataToPass = array();
		switch ($request) {
			case 'post':
			case 'POST':
				if(!empty($data) || $data == false){
					$dataToPass = [
						'form_params' => $data
					];
				}
				break;

			case 'get':
			case 'GET':
				if(!empty($data) || $data == false){
					$dataToPass = [
						'query' => $data
					];
				}
				break;
			case 'upload':
				if(!empty($data) || $data == false){
					return -1;
				}
				$dataToPass = [
					'multipart' => $data
				];
				break;
			default:
				return -1;
		}

		if(!empty($this->headers))
			$dataToPass['headers'] = $this->headers;

		$dataToPass['timeout'] = 900000;
		$dataToPass['verify']  = false;
		return $dataToPass;
	}

	protected function changeData($data, $multiPart){
		foreach ($data as $key => $value) {
			$multiPart[] = [
				'name' => $key,
				'contents' => (string)$value
			];
		}
		return $multiPart;
	}

	protected function changeDataFiles($data, $multiPart){
		$i = 0;
		foreach ($data as $key => $value) {
			$multiPart[] = [
				'name' => $key,
				'contents' => fopen($value->getRealPath(), 'r')
			];
			$multiPart[] = [
				'name' => 'extension' . $i,
				'contents' => $value->getClientOriginalExtension()
			];
			$multiPart[] = [
				'name' => 'filesize' . $i,
				'contents' => (string)$value->getSize()
			];
			$i++;
		}
		return $multiPart;
	}


	public function multiPart($dataToPass, $request){
		
		$multiPart = array();
		$multiPart = $this->changeData($dataToPass[0]['namesANDcontents'], $multiPart);
		$multiPart = $this->changeDataFiles($dataToPass[1]['namesANDcontentsFiles'], $multiPart);
		// print_r($multiPart);
		
		$client = new GuzzleHttp\Client();
		$controllerLocation = $this->serverLocation . $this->controllerLocation;
		$data = $this->sendRequestData($multiPart, 'upload');
		if($data == -1) return -1;
		$response = $client->request($request, $controllerLocation, $data);
		$return = $response->getBody();
		$return = json_decode($return, true);

		return $return;
	}
}