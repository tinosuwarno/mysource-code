<?php

function time_elapsed_string_myWall($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}

function formatBytes($bytes, $precision = 2) { 
    $units = array('Bi', 'KiB', 'MiB', 'GiB', 'TiB'); 

    $bytes = max($bytes, 0); 
    $pow = floor(($bytes ? log($bytes) : 0) / log(1024)); 
    $pow = min($pow, count($units) - 1); 

    // Uncomment one of the following alternatives
    // $bytes /= pow(1024, $pow);
    $bytes /= (1 << (10 * $pow)); 

    return round($bytes, $precision) . ' ' . $units[$pow]; 
} 

function getGenre(){
    $session = Session::get('data');
    $data = [
        'userID' => $session['id'],
        'apiKey' => $session['apiKey']
    ];
    
    $myGuzzle = new myGuzzle('getGenre');
    $myGuzzle->setHeader($session['apiKey']);
    $return = $myGuzzle->formParams($data, 'post');

    $content = '';
    foreach ($return as $key => $value) {
        $content .= "<div class='item'><a onclick='getGenreDemo(".$value['genreID'].")' style='cursor: pointer'>".$value['genreName']."</a></div>";
    }

    return $content;
}

?>