<?php
function getPicture($picture){
    if(empty($picture) || $picture == null) return "img/album/album-02_475x300.jpg";
    else return $picture;
}

function getDuration($duration){
    $sec_num = 100; // don't forget the second param
    $hours   = floor($sec_num / 3600);
    $minutes = floor(($sec_num - ($hours * 3600)) / 60);
    $seconds = $sec_num - ($hours * 3600) - ($minutes * 60);

    if ($hours   < 10) {$hours   = $hours;}
    if ($minutes < 10) {$minutes = $minutes;}
    if ($seconds < 10) {$seconds = $seconds;}
    $time    = $minutes.'m:'.$seconds.'s';
    return $time;
}

function getPrice($price){
    switch ($price) {
        case 0:
            $return = 'Free';
            break;
        case 1:
            $return = 'Rp.1000,-';
            break;
        case 2:
            $return = 'Rp.2000,-';
            break;
        case 3:
            $return = 'Rp.3000,-';
            break;
        default:
            $return = 'Rp.9999,-';
            break;
    }
    return $return;
}

?>