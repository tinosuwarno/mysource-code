<?php
	function getSearch($keyword){
		if(!empty($keyword)){
			$session = Session::get('data');
			$data = [
				'userID' => $session['id'],
				'apiKey' => $session['apiKey'],
				'keyword' => $keyword
			];

			$guzzle = new myGuzzle('getSearchMarket');
			$guzzle->setHeader($session['apiKey']);
			$return = $guzzle->formParams($data, 'post');
			return $return;
		}
	}

	function searchContent($data){
		$content = "";
		foreach ($data as $key => $value) {
			$content .= "<div class='panel panel-default'>";
			$content .= setContent($value['song'][0], 1, $key); //song
			$content .= "<div id='collapse".$key."' class='panel-collapse collapse'>
	        					<div class='panel-body'>
	            					<div class='list-song-striped'>";
			foreach ($value['track'] as $keys => $values) {
				$content .= setContent($values, 2, $keys);//track
			}
			$content .=			"</div>
			        			</div>
			    			</div>";
			$content .= "</div>";
		}
		return $content;
	}

	function setContent($details, $type, $key){
		$content = "<div class='li-ac-src-track'>
	        <div class='btn-li-ac-src-track'><a href=''><i class='fa fa-play-circle-o fa-2x play-single-list-song'></i></a></div>
	        <div class='tittle-li-ac-src-track'><span>".$details['judul']."</span></div>";
	        
	        if($details['original']){
	        	$content .= "<div class='original-li-ac-src-track'>
	        					<span class=' hint--left hint--info' data-hint='Original'>
	        						<i class='fa fa-bookmark'></i>
	        					</span>
	        				</div>";
	        }
	        
	    $content .= "<div class='author-li-ac-src-track'>
	    		<span>".$details['username']."</span>
	    	</div>
	        <div class='genre-li-ac-src-track'>
	        	<span>".$details['genre']."</span>
	        </div>
	        <div class='track-li-ac-src-track'>
	        	<span>"; 
	        		if($type == 2)
	        			$content .= $details['instrument'];
    	$content .= "</span>
	        </div>
	        <div class='time-li-ac-src-track'><span>".getDuration($details['duration'])."</span></div>
	        <div class='rating-li-ac-src-track'><input type='text' class='kv-fa rating-loading' value='".$details['rating']."' dir='ltr' data-size='xs' title=''></div>
	        <div class='harga-li-ac-src-track'><span>".getPrice($details['price'])."</span></div>
	        <div class='buy-li-ac-src-track'>
	            <a href='' class=' hint--left hint--info' data-hint='Buy Item'><i class='fa fa-plus-square-o buy-single-list-song'></i></a>
	        </div>";
	    	if($type == 1){
	    		$content .= "<div class='more-li-ac-src-track'>
		            <span class='hint--left hint--info' data-hint='Expand'>
	                    <a class='accordion-toggle more-list-song' data-toggle='collapse' data-parent='#accordion' href='#collapse".$key."' ><i class='fa fa-chevron-right'></i></a>
	                </span>
		        </div>";
	    	}
	    $content .= "</div>";
        return $content;
	}
?>