@extends('layouts.default-base')
{{-- overwrite title --}}
@section('title')
Gritjam - Group
@overwrite

@section('styles')
  
@stop

{{-- content --}}
@section('contents')

<?php
    $session = Session::get('data');
    $id = $session['id'];
    
    $data = [
        'id' => $id
    ];
    
  $myApp = App::make('serverLocation'); 
  $server = $myApp->serverLocation ; //

?>
  
<!-- ---------- SECTION MARKET MUSIC ----------------- --- -->
<div class="block-white"></div>
<div class="block-white visible-sm hidden-lg hidden-xs hidden-md"></div>
<div class="block-white visible-sm hidden-lg hidden-xs hidden-md"></div>
<?php $session = Session::get('data');  ?>
@include('layouts.bg-setting');
                    <?php
                        $return = Session::get('data');
                        $id = $return['id'];
                        $apiKey = $return['apiKey'];
                    ?>
<input type="hidden" id="sessiongroup" value="<?php echo $id; ?>"></input>
<div class="container-fluid">
	<div class="row">
            <div class="md-w-warp">
                <div class="col-lg-12">
                    <div class="menu text-center menu-wall motion-animate">
                        <div class="hr"></div>
                        	<div class="warp-btn-song-noaffix hidden-lg hidden-md">
                            	<a id="btnEvent-affixLeft" class="btn-song-affix" href="#sidrLeft"><i class="fa fa-bars"></i></a>
                            </div>
                        <ul class="action-menu-profile">
                        	<li>
                                <a id='pjax' data-pjax='yes' href='{{url("about-me")}}'>About</a>
                            </li>
                            <li>
                                <a id='pjax' data-pjax='yes' href='{{url("myWall")}}'>My wall</a>
                            </li>
                            <li>
                                <a data-pjax='yes' id='pjax' href="{{ url('profile/following', $parameters = [$id], $secure = null); }}">Timeline</a>
                            </li>
                            <li class="active">
                                <a data-pjax='yes' id='pjax' href="{{ url('group') }}">Groups</a>
                            </li>
                            <li>
                                <a data-pjax='yes' id='pjax' href="{{ url('myProject') }}">Project</a>
                            </li>
                        </ul>
        					<div class="warp-btn-event-noaffix hidden-lg hidden-md">
                            	<a id="btnEvent-affix2" class="btn-event-affix" href="#sidr"><i class="fa fa-calendar"></i></a>
                            </div>
                        <div class="hr"></div>
                    </div>
                </div>
                <div class="clearfix"></div>
                
                <div class="warp-all-wall">
                    <!-- <div class="col-lg-3 col-md-3 col-sm-12">
                    <div id="sidrLeft" class="sec-side-left-wall fullwidth">
                    <div class="row">
                    <div id="fixLeftSideBar" class="left-side-wall">
                    <div class="col-lg-12">
                    	<div class="row">
                        	<div class="warp-btn-back-side-left hidden-lg hidden-md">
                            	<a class="btn-back-side-left" href="#"><i class="fa fa-arrow-left"></i></a>
                            </div>
                            <div class="warp-side-l-menu">
                            	<ul>
                                	<li><a href="#">Group Member</a></li>
                                    <li><a href="#">About Group</a></li>
                                    <li><a href="#">Group Stuff</a></li>
                                    <li><a href="#">Setting</a></li>
                                    <li><a href="#eventGroup" class="smooths">Group Event</a></li>
                                    <li><a href="#">Latest Post</a></li>
                                    <li><a href="#">Top Engage</a></li>
                                    <li><a href="#">Hot Topics</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    </div>  
                    </div>
                    </div>
                    </div> -->

                     <div class="col-lg-3 col-md-3 col-sm-12">
                    <div id="sidrLeft" class="sec-side-left-wall fullwidth">
                        <div class="row">
                            <div id="fixLeftSideBar" class="left-side-wall">
                                <div class="col-lg-12">
                                    <div class="row">
                                        <div class="warp-btn-back-side-left hidden-lg hidden-md">
                                            <a class="btn-back-side-left" href="#"><i class="fa fa-arrow-left"></i></a>
                                        </div>
                                        
                                        <!-- =========================================================== SONGS WIDGET LEFT ============================================================ -->
                                        <div class="warp-side-left-wall">
                                            <div class="warp-title-side-left-wall">
                                                <h4>Song</h4>
                                                <a href="#" class="btn-detail-sosmed pull-right"><i class="fa fa-bars"></i><span>see all</span></a>
                                            </div>
                                            <div class="warp-item-side-left">
                                                <div class="warp-item-side-left" id="ownSong">
                                                    <div class="col-xs-push-5 col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 lazyloader-list" id='loadingGroupLeft1'>
                                                        <img src="{{asset('/img/loader-more.gif')}}" class="img-full-responsive e-centering width-loader">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                         <div class="warp-side-left-wall">
                                            <div class="warp-title-side-left-wall">
                                                <h4>Colaboration</h4>
                                                <i class="fa fa-pencil btn-edit-own-song"></i>
                                            </div>
                                            <div class="warp-item-side-left-np">
                                                <div class="warp-item-side-left" id='collaborateSong'>
                                                    <div class="col-xs-push-5 col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 lazyloader-list" id='loadingGroupLeft3'>
                                                        <img src="{{asset('/img/loader-more.gif')}}" class="img-full-responsive e-centering width-loader">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- cover song dan own song di merge ke tab song -->
                                        <div class="warp-side-left-wall" style="display: none;">
                                            <div class="warp-title-side-left-wall">
                                                <h4>Cover Song</h4>
                                            </div>
                                            <div class="warp-item-side-left">
                                                <div class="warp-item-side-left" id='coverSong'>
                                                    <div class="col-xs-push-5 col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 lazyloader-list" id='loadingGroupLeft2'>
                                                        <img src="{{asset('/img/loader-more.gif')}}" class="img-full-responsive e-centering width-loader">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                         <!-- =========================================================== FRIENDS WIDGET LEFT ============================================================ -->
                                        <div class="warp-side-left-wall" id="friendlist1">
                                            <div class="warp-title-side-left-wall">
                                                <h4>Friends</h4>
                                                <a id='pjax' data-pjax='yes' href='{{url("friend")}}' class="btn-detail-sosmed pull-right group-menu-sideBarLeft"><i class="fa fa-bars"></i><span>see all</span></a>
                                            </div>
                                            <div class="warp-item-side-left" id='friendlist'>
                                                <div class="col-xs-push-5 col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 lazyloader-list" id='loadingGroup4'>
                                                        <img src="{{asset('/img/loader-more.gif')}}" class="img-full-responsive e-centering width-loader">
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                        <!-- ======================================================== END FRIENDS WIDGET LEFT ============================================================ -->
                                        
                                        <!-- ======================================================== GROUPS WIDGET LEFT ================================================================= -->
                                       <!--  <div class="warp-side-left-wall">
                                            <div class="warp-title-side-left-wall">
                                                <h4>Groups</h4>
                                                <a href="#" class="btn-detail-sosmed pull-right"><i class="fa fa-bars"></i><span>detail</span></a>
                                            </div>

                                            <div class="warp-item-side-left">
                                                <div class="col-xs-push-5 col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 lazyloader-list" id='loadingMyWallGroup'>
                                                    <img src="/img/loader-more.gif" class="img-full-responsive e-centering width-loader">
                                                </div>
                                            </div>
                                        </div> -->
                                        <!-- ======================================================== END GROUPS WIDGET LEFT ================================================================= -->


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    
                    <div class="col-lg-6 col-md-6 col-sm-12 padten-sxs">
                    <div class="warp-over-timeline">
                    	<div class="timeline-box">
                        	<div class="warp-tittle-group">
                            	<h4>My Group</h4>
                                <div class="warp-btn-r-t"><button class="btn col-btn c-f-btn crt-group">Create New Group</button></div>
                            </div>
                            
                            <div class="warp-member-group" id="listMyGroup">
                                <!-- ------------ JIKA MASIH KOSONG ----------- -->
                                <!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="nf-item-wall-track">
                                        <p>There is no Group You Have not Joined</p>
                                    </div>
                                </div> -->
                                <div class='col-xs-push-5 col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 lazyloader-list' id='loadingGroup1'>
                                    <img src="{{asset('/img/loader-more.gif')}}" class='img-full-responsive e-centering'>
                                </div>
                                <div class="clearfix" id="clearfix-myGroup"></div>
                                <span id='totalCountYourGroup' style='display:none;'></span>
                                <!-- ------------ END JIKA MASIH KOSONG ----------- --> 
                            </div>
                               
                        </div>
                        <div class="timeline-box">
                            <div class="warp-tittle-group">
                                <h4>Group Invitation</h4>
                                
                            </div>
                            
                            <div class="warp-member-group">
                               
                                <div class='col-xs-push-5 col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 lazyloader-list' id='loadingGroup2'>
                                    <img src="{{asset('/img/loader-more.gif')}}" class='img-full-responsive e-centering'>
                                </div>
                                <div class="clearfix" id="clearfix-groupInvitation"></div>
                                <span id='totalCountYourGroupPending' style='display:none;'></span>
                               
                            </div>
                               
                        </div>
                        <div class="timeline-box">
                            <div class="warp-tittle-group">
                                <h4>Group Request</h4>
                                
                            </div>
                            
                            <div class="warp-member-group">
                               
                                <div class='col-xs-push-5 col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 lazyloader-list' id='loadingGroup3'>
                                    <img src="{{asset('/img/loader-more.gif')}}" class='img-full-responsive e-centering'>
                                </div>
                                <div class="clearfix" id="clearfix-groupRequest"></div>
                                <span id='totalCountYourGroupRequest' style='display:none;'></span>
                               
                            </div>
                               
                        </div>
                    </div>
                    </div>
                    
                    <div class="col-lg-3 col-md-3 col-sm-12">
                    <div id="sidrRight" class="sec-side-right-wall fullwidth">
                    	<div class="row">
                   		<div id="fixRightSideBar" class="right-side-bar">
                        	<div class="col-lg-12">
                            <div class="warp-btn-back-side-right hidden-lg hidden-md">
                            	<a class="btn-back-side-right" href="#"><i class="fa fa-arrow-left"></i></a>
                            </div>
                            </div>
                        	<!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="warp-cal-event-wall">
                            	<div class="warp-btn-add-evt-cal">
                                	<a class="btn-add-evt-cal"><i class="fa fa-plus-circle"></i></a>
                                </div>
                                <div class="warp-evt-month-year">
                                	<div class="evt-month-year"><span class="month">APRIL</span><span class="year">2016</span></div>
                                    <div class="clearfix"></div>
                                    <div class="evt-date"><a class="btn-date-evt">24</a></div>
                                </div>
                                <div class="warp-evt-name-wall">
                                	<a class="btn-det-evt"><h3>Hell Fest 2016</h3></a>
                                </div>
                                <div class="warp-evt-place-time">
                                	<div class="place-evt">
                                    	Parkir Hall Senayan
                                    </div>
                                    <div class="time-evt">
                                    	<span class="start">09:00 AM</span>
                                        <span class="finish"> 23:00 PM</span>
                                    </div>
                                </div>
                            </div>
                            </div> -->
                           
                            <div class="warp-side-left-wall">
                                <div class="warp-title-side-left-wall">
                                    <h4>My Event</h4>
                                    <!-- <a href="#" class="btn-detail-sosmed pull-right"><i class="fa fa-bars"></i><span>detail</span></a> -->
                                </div>
                                <div class="warp-item-side-left" id="printloadingGroupEventawal">
                                        <div class="col-xs-push-5 col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 lazyloader-list" id='loadingGroupEventawal'>
                                          <img src="{{asset('/img/loader-more.gif')}}" class="img-full-responsive e-centering width-loader">
                                        </div>
                                        <input id="skipevent" type="hidden" value="0"></input>
                                        <input id="session" type="hidden" value="{{$id}}"></input>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sxs-12">
                                <div class="warp-ads-right">
                                    <div class="ads-box-right hidden-xs hidden-lg hidden-sm">
                                        <a href=""><img src="{{asset('img/ads/ads_200x200.jpg')}}"></a>
                                    </div>
                                    <div class="side-ads-box e-centering hidden-md">
                                        <a href=""><img src="{{asset('img/ads/ads_300x250.jpg')}}" class="e-centering"></a>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sxs-12">
                            	<div class="warp-list-song-rec-wall">
                                	<div class="tittle-rec-wall">
                                    	<h5> Recommended Today</h5>
                                    </div>
                                    <ul>
                                        <li>
                                        	<div class="warp-btn-buy-rec-wall">
                                            	<a href="" class="btn-buy-rec-wall"><i class="fa fa-plus-square-o"></i><span>Buy This Item</span></a>
                                            </div>
                                            <div class="item-rec-song">
                                            <div class="pl-img-popup">
                                                <img src="{{asset('img/album/album-06.jpg')}}" class="img-responsive">
                                            </div>
                                            <div class="rec-tittle-name-popup">
                                                <div class="rec-tittle-popup">Nama Band Max 15 Ch</div>
                                                <div class="rec-name-popup">Judul Lagu Max 15 Ch</div>
                                            </div>
                                            <div class="rec-stat-popup">
                                                <div class="rec-stat-like-popup">
                                                    <a><i class="fa fa-heart like-single-list-song"></i></a>
                                                    <span>120</span>
                                                </div>
                                                <div class="rec-stat-forward-popup">
                                                    <a><i class="fa fa-mail-forward forward-single-list-song"></i></a>
                                                    <span>56</span>
                                                </div>
                                                <div class="rec-stat-forward-popup">
                                                    <a><i class="fa fa-plus-square plus-single-list-song"></i></a>
                                                    <span>120K</span>
                                                </div>
                                            </div>
                                            </div>
                                        </li>
                                        <li>
                                        	<div class="warp-btn-buy-rec-wall">
                                            	<a href="" class="btn-buy-rec-wall"><i class="fa fa-plus-square-o"></i><span>Buy This Item</span></a>
                                            </div>
                                            <div class="item-rec-song">
                                            <div class="pl-img-popup">
                                                <img src="{{asset('img/album/album-05.jpg')}}" class="img-responsive">
                                            </div>
                                            <div class="rec-tittle-name-popup">
                                                <div class="rec-tittle-popup">Nama Band Max 15 Ch</div>
                                                <div class="rec-name-popup">Judul Lagu Max 15 Ch</div>
                                            </div>

                                            <div class="rec-stat-popup">
                                                <div class="rec-stat-like-popup">
                                                    <a><i class="fa fa-heart like-single-list-song"></i></a>
                                                    <span>120</span>
                                                </div>
                                                <div class="rec-stat-forward-popup">
                                                    <a><i class="fa fa-mail-forward forward-single-list-song"></i></a>
                                                    <span>56</span>
                                                </div>
                                                <div class="rec-stat-forward-popup">
                                                    <a><i class="fa fa-plus-square plus-single-list-song"></i></a>
                                                    <span>120K</span>
                                                </div>
                                            </div>
                                            </div>
                                        </li>
                                        <li>
                                        	<div class="warp-btn-buy-rec-wall">
                                            	<a href="" class="btn-buy-rec-wall"><i class="fa fa-plus-square-o"></i><span>Buy This Item</span></a>
                                            </div>
                                            <div class="item-rec-song">
                                            <div class="pl-img-popup">
                                                <img src="{{asset('img/album/album-04.jpg')}}" class="img-responsive">
                                            </div>
                                            <div class="rec-tittle-name-popup">
                                                <div class="rec-tittle-popup">Nama Band Max 15 Ch</div>
                                                <div class="rec-name-popup">Judul Lagu Max 15 Ch</div>
                                            </div>
                                            <div class="rec-stat-popup">
                                                <div class="rec-stat-like-popup">
                                                    <a><i class="fa fa-heart like-single-list-song"></i></a>
                                                    <span>120</span>
                                                </div>
                                                <div class="rec-stat-forward-popup">
                                                    <a><i class="fa fa-mail-forward forward-single-list-song"></i></a>
                                                    <span>56</span>
                                                </div>
                                                <div class="rec-stat-forward-popup">
                                                    <a><i class="fa fa-plus-square plus-single-list-song"></i></a>
                                                    <span>120K</span>
                                                </div>
                                            </div>
                                            </div>
                                        </li>
                                        <li>
                                        	<div class="warp-btn-buy-rec-wall">
                                            	<a href="" class="btn-buy-rec-wall"><i class="fa fa-plus-square-o"></i><span>Buy This Item</span></a>
                                            </div>
                                            <div class="item-rec-song">
                                            <div class="pl-img-popup">
                                                <img src="{{asset('img/album/album-03.jpg')}}" class="img-responsive">
                                            </div>
                                            <div class="rec-tittle-name-popup">
                                                <div class="rec-tittle-popup">Nama Band Max 15 Ch</div>
                                                <div class="rec-name-popup">Judul Lagu Max 15 Ch</div>
                                            </div>
                                            <div class="rec-stat-popup">
                                                <div class="rec-stat-like-popup">
                                                    <a><i class="fa fa-heart like-single-list-song"></i></a>
                                                    <span>120</span>
                                                </div>
                                                <div class="rec-stat-forward-popup">
                                                    <a><i class="fa fa-mail-forward forward-single-list-song"></i></a>
                                                    <span>56</span>
                                                </div>
                                                <div class="rec-stat-forward-popup">
                                                    <a><i class="fa fa-plus-square plus-single-list-song"></i></a>
                                                    <span>120K</span>
                                                </div>
                                            </div>
                                            </div>
                                        </li>
                                        <li>
                                        	<div class="warp-btn-buy-rec-wall">
                                            	<a href="" class="btn-buy-rec-wall"><i class="fa fa-plus-square-o"></i><span>Buy This Item</span></a>
                                            </div>
                                            <div class="item-rec-song">
                                            <div class="pl-img-popup">
                                                <img src="{{asset('img/album/album-02.jpg')}}" class="img-responsive">
                                            </div>
                                            <div class="rec-tittle-name-popup">
                                                <div class="rec-tittle-popup">Nama Band Max 15 Ch</div>
                                                <div class="rec-name-popup">Judul Lagu Max 15 Ch</div>
                                            </div>
                                            <div class="rec-stat-popup">
                                                <div class="rec-stat-like-popup">
                                                    <a><i class="fa fa-heart like-single-list-song"></i></a>
                                                    <span>120</span>
                                                </div>
                                                <div class="rec-stat-forward-popup">
                                                    <a><i class="fa fa-mail-forward forward-single-list-song"></i></a>
                                                    <span>56</span>
                                                </div>
                                                <div class="rec-stat-forward-popup">
                                                    <a><i class="fa fa-plus-square plus-single-list-song"></i></a>
                                                    <span>120K</span>
                                                </div>
                                            </div>
                                            </div>
                                        </li>
                                        <li>
                                        	<div class="warp-btn-buy-rec-wall">
                                            	<a href="" class="btn-buy-rec-wall"><i class="fa fa-plus-square-o"></i><span>Buy This Item</span></a>
                                            </div>
                                            <div class="item-rec-song">
                                            <div class="pl-img-popup">
                                                <img src="{{asset('img/album/album-01.jpg')}}" class="img-responsive">
                                            </div>
                                            <div class="rec-tittle-name-popup">
                                                <div class="rec-tittle-popup">Nama Band Max 15 Ch</div>
                                                <div class="rec-name-popup">Judul Lagu Max 15 Ch</div>
                                            </div>
                                            <div class="rec-stat-popup">
                                                <div class="rec-stat-like-popup">
                                                    <a><i class="fa fa-heart like-single-list-song"></i></a>
                                                    <span>120</span>
                                                </div>
                                                <div class="rec-stat-forward-popup">
                                                    <a><i class="fa fa-mail-forward forward-single-list-song"></i></a>
                                                    <span>56</span>
                                                </div>
                                                <div class="rec-stat-forward-popup">
                                                    <a><i class="fa fa-plus-square plus-single-list-song"></i></a>
                                                    <span>120K</span>
                                                </div>
                                            </div>
                                            </div>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </div> -->
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sxs-12">
                                <div class="warp-ads-right">
                                    <div class="ads-box-right hidden-xs hidden-lg hidden-sm">
                                        <a href=""><img src="{{asset('img/ads/ads_200x200.jpg')}}"></a>
                                    </div>
                                    <div class="side-ads-box e-centering hidden-md">
                                        <a href=""><img src="{{asset('img/ads/ads_300x250.jpg')}}" class="e-centering"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    	</div>
                    </div>
                    </div>
                    
                </div><!-- ================ WARP 3 BAGIAN ================ -->
                
            </div>
    </div>
</div>

<div class="block-white"></div>
<!-- ---------- END MARKET MUSIC ------------------ -->


<!-- ============================= MODAL CREATE GROUP ============================================= --> 
	<div id="createGroup" tabindex="-1" role="dialog" aria-hidden="true" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          
          <div class="modal-body nopadding">
          <div class="row">
          	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sxs-12">
            	<div class="warp-crt-add-playlist"> 
                	<div class="modal-header color-gj-popup-report">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Create Group</h4>
                    </div>
                    <div class="warp-15">
                    	<form class="bs-example form-horizontal" name="formCreateGroup" method="post" action='createGroupProsess' enctype="multipart/form-data">
                        <!-- <form class="bs-example form-horizontal" name="formCreateGroup" id="formCreateGroup"> -->
                            <div class="form-group">
                              <label class="col-lg-3 control-label">Group Name</label>
                              <div class="col-lg-9">
                                <input type="text" class="form-control" placeholder="Group Name" name="groupname" id="groupname" required><span class="req-w">*</span>
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="col-lg-3 control-label">Description</label>
                              <div class="col-lg-9">
                                <textarea class="txt-area-off" name="Description" id="Description"></textarea>
                              </div>
                            </div>
                            <div class="l-hr"></div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Insert Group Avatar</label>
                                <div class="col-lg-9">
                                    <img class="img-responsive img-edit-up img-border-gj img-profile-user" id="preview1">
                                    <input type="file" name="file-1" id="file-1" class="inputfile" onchange="document.getElementById('preview1').src = window.URL.createObjectURL(this.files[0])" data-multiple-caption="{count} files selected" multiple accept="image/*">
                                    <label for="file-1"> <span>Choose a file</span></label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Insert Group Cover</label>
                                <div class="col-lg-9">
                                    <img class="img-responsive img-edit-up img-border-gj" id="previewBgGroup">
                                    <input type="file" name="file-2" id="file-2" class="inputfile" onchange="document.getElementById('previewBgGroup').src = window.URL.createObjectURL(this.files[0])" data-multiple-caption="{count} files selected" multiple accept="image/*">
                                    <label for="file-2"> <span>Choose a file</span></label>
                                </div>
                            </div>
                            <div class="l-hr"></div>
                            <div class="form-group">
                            	<label class="col-lg-3 control-label">Setting Group</label>
                                <div class="col-lg-9" id="Setting">
                                    <div class="radio gj-checks">
                                      <label>
                                        <input type="radio" name="optionsRadios" id="publicGroup" value="1" checked="checked">
                                        <i></i>Public
                                      </label>
                                    </div> 
                                    <div class="radio gj-checks">
                                      <label>
                                        <input type="radio" name="optionsRadios" id="privateGroup" value="0">
                                        <i></i>Private
                                      </label>
                                    </div>
                            	</div>
                            </div>
                            <div class="l-hr"></div>
                            <div class="form-group">
                              <div class="col-lg-offset-3 col-lg-9">
                              <button class="btn bgc-btn c-f-btn crt-group" onchange="UploadCreate" id="MyCreateGroup">Create Group</button>
                              	<!-- <a class="btn bgc-btn c-f-btn crt-group" onchange="UploadCreate" id="MyCreateGroup">Create Group</a> -->
                              </div>
                            </div>
                       </form>
                       <!-- {{ Form::close() }} -->
                    </div>
                </div>
      		</div>
          </div>
          </div>
        </div>
      </div>
  </div>
<!-- ============================= END MODAL CREATE GROUP =========================================== -->
<!-- ============================  MODAL REJECT GROUP ========================= -->
<div id="modalRejectGroupInvite" class="vh-center modal fade">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header color-gj-popup-report">
       <button type="button" class="close close-button-not-interest-group-member-from-page-group" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Reject Group </h4>
      </div>
      <div class="modal-body">
      <span id="data-groupname-confirm-reject" style="display: none;"></span> 
         <span id="data-groupid-cancel" style="display: none;"></span>
         <span id="data-groupid-confirm-reject" style="display: none;"></span>
        <div class="row">
          <div class="col-sm-12">
            <p>Are you sure to confirm group <strong>"<span id="data-groupname-confirm-1-reject"></span>"</strong> ?</p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <span class="loader-process" style="display: none;margin-right: 5px;"><img src="{{asset('img/loader-24.gif')}}" /></span>
        <a href="#" class="btn-s-gj bgc-btn close-button-not-interest-group-member-from-page-group" data-dismiss="modal" style="margin-right: 5px;">Cancel</a>
        <a href="#" class="btn-s-gj bgc-btn process-not-interest-group-member-from-page-group" id="button-cancel-invitation-member-from-page-group">Yes</a>
      </div>
    </div>
  </div>
</div>

<!-- ===================== MODAL USER CANCEL REQUEST ========================-->
<div id="modalCancelRequest" class="vh-center modal fade">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header color-gj-popup-report">
       <button type="button" class="close close-button-not-interest-group-from-user" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Cancel Your Request </h4>
      </div>
      <div class="modal-body">
         <span id="data-groupid-cancel" style="display: none;"></span>
        <div class="row">
          <div class="col-sm-12">
            <p>Are you sure to cancel request in group <strong>"<span id="data-groupname-cancel"></span>"</strong> ?</p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <span class="loader-process" style="display: none;margin-right: 5px;"><img src="{{asset('img/loader-24.gif')}}" /></span>
        <a href="#" class="btn-s-gj bgc-btn close-button-not-interest-group-from-user" data-dismiss="modal" style="margin-right: 5px;">Cancel</a>
        <a href="#" class="btn-s-gj bgc-btn process-not-interest-group-from-user" id="button-not-interest-group-from-user">Yes</a>
      </div>
    </div>
  </div>
</div>
<!-- ============== MODAL ADMIN LEAVE GROUP JIKA ADA MEMBER LAIN =====================-->  
<div id="modalAdminLeaveGroupAdaMemberLain" tabindex="-1" role="dialog" aria-hidden="true" class="modal vh-center fade">
  <div class="modal-dialog modal-sm" style="width: 400px">
    <div class="modal-content">
      <div class="modal-body nopadding">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sxs-12">
            <div class="warp-crt-add-playlist">
              <div class="modal-header color-gj-popup-report">
                <button type="button" class="close close-button" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Leave Group </h4>
              </div>
              <div class="warp-15">
                <div>Are you sure to leave group : <strong><span id="data-groupname-adminLeave"></span></strong> ?</div>
                <div>Please,select new admin</div>
                <br>
                <div>
                  <select data-placeholder="Select New Admin..." class="chosen-select" tabindex="4" style="display:block;" name="new_admin_member_id" id="new_admin_member_id">
                  </select>
                </div>
                <span id="data-groupid-adminLeave" style="display: none;"></span>
                <span id="data-picture-adminLeave" style="display: none;"></span>
                <span id="data-bg-picture-adminLeave" style="display: none;"></span>
                <br>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class='modal-footer'>
        <span class='loader-process' style='display: none;'><img src='{{asset('img/loader-24.gif')}}' /></span>
        <a href="#" class="btn-s-gj bgc-btn close-button" data-dismiss="modal">Cancel</a>
        <a href="#" class="btn-s-gj bgc-btn process-admin-leave-group-select-newadmin" id="admin-leave-select-newAdmin">YES</a>
      </div>
    </div>
  </div>
</div>                    
@stop
   
