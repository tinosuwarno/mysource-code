@extends('layouts.default-base')
{{-- overwrite title --}}
@section('title')
Gritjam - Group Members
@overwrite
@section('styles')
@stop
{{-- content --}}

@section('contents')
<?php
  $session = Session::get('data');
  $id = $session['id'];

  $myApp = App::make('baseUrl'); 
  $baseUrl = $myApp->baseUrl ; 
  ?>
<!-- ---------- SECTION MARKET MUSIC ----------------- --- -->
<div class="block-white"></div>
<div class="block-white visible-sm hidden-lg hidden-xs hidden-md"></div>
<div class="block-white visible-sm hidden-lg hidden-xs hidden-md"></div>

@include('layouts.bg-group-setting')
<div class="container-fluid">
  <div class="row">
    <div class="md-w-warp">
      <div class="col-lg-12">
        <div class="menu text-center menu-wall motion-animate">
          <div class="hr"></div>
          <div class="warp-btn-song-noaffix hidden-lg hidden-md">
            <a id="btnEvent-affixLeft" class="btn-song-affix" href="#sidrLeft"><i class="fa fa-bars"></i></a>
          </div>
          <ul class="action-menu-profile">
            <li>
              <a id='pjax' data-pjax='yes' href='{{url("about-me")}}'>About</a>
            </li>
            <li>
              <a id='pjax' data-pjax='yes' href='{{url("myWall")}}'>My wall</a>
            </li>
            <li>
              <a data-pjax='yes' id='pjax' href="{{ url('profile/following', $parameters = [$id], $secure = null); }}">Timeline</a>
            </li>
            <li class="active">
              <a data-pjax='yes' id='pjax' href="{{ url('group') }}">Groups</a>
            </li>
            <li>
              <a data-pjax='yes' id='pjax' href="{{ url('myProject') }}">Project</a>
            </li>
          </ul>
          <div class="warp-btn-event-noaffix hidden-lg hidden-md">
            <a id="btnEvent-affix2" class="btn-event-affix" href="#sidr"><i class="fa fa-calendar"></i></a>
          </div>
          <div class="hr"></div>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="warp-all-wall">
      @include('group.group-menu')
        <div class="col-lg-6 col-md-6 col-sm-12 padten-sxs">
          <div class="warp-over-timeline">
            <div class="timeline-box">
              <div id="memberGroup" class="warp-tittle-group">
                <h4>Group Member</h4>
                <?php
                  if($group[0]['checkAdmin'] == 'not admin'){
                          
                      }else{
                         echo "<div class='warp-btn-r-t'><button class='btn col-btn c-f-btn invite-friend'>Invite Friend to Group</button></div>";
                      }
                  ?> 
                <div class="pull-right sum-member" id="loadingGroupMember2"></div>
              </div>
              <div class="warp-member-group" id="groupmember3">
                <div class='col-xs-push-5 col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 lazyloader-list' id='loadingGroupMember3'>
                  <img src="{{asset('img/loader-more.gif')}}" class='img-full-responsive e-centering'>
                </div>
                <div class="clearfix" id="clearfix-groupmember3"></div>
                <div class="col-xs-12">
                </div>
              </div>
            </div>
            <div class="timeline-box">
              <div id="memberGroup" class="warp-tittle-group">
                <h4>Pending Member Invitation</h4>
                <div class="pull-right sum-member" id="loadingGroupMember4"></div>
              </div>
              <div class="warp-member-group" id="groupmember5">
                <div class='col-xs-push-5 col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 lazyloader-list' id='loadingGroupMember5'>
                  <img src="{{asset('img/loader-more.gif')}}" class='img-full-responsive e-centering'>
                </div>
                <div class="clearfix" id="clearfix-groupmember5"></div>
                <div class="col-xs-12">
                </div>
              </div>
            </div>
            <div class="timeline-box">
              <div id="memberGroup" class="warp-tittle-group">
                <h4>Group Member Request</h4>
                <div class="pull-right sum-member" id="loadingGroupMember6"></div>
              </div>
              <div class="warp-member-group" id="groupmember7">
                <div class='col-xs-push-5 col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 lazyloader-list' id='loadingGroupMember7'>
                  <img src="{{asset('img/loader-more.gif')}}" class='img-full-responsive e-centering'>
                </div>
                <div class="clearfix" id="clearfix-groupmember7"></div>
                <div class="col-xs-12">
                </div>
              </div>
            </div>
            <!-- <div id="eventGroup" class="warp-tittle-group">
              <h4>Event</h4>
                <div class="set-evt pull-right">
                  <a class="btn-add-evt-g col-btn">Add Event<i class="fa fa-plus"></i></a>
                    <a class="btn-set-evt-g col-btn"><i class="fa fa-gear"></i></a>
                </div>
              </div>
              
              
              <input type="hidden" id="group_id" value="<?php echo $groupID; ?>"></input>
              <div id="printevent"></div>
              <div class="col-xs-push-5 col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 lazyloader-list" style="display: block;" id="loadevent">
                <img src="{{asset('img/loader-more.gif')}}" class="img-full-responsive e-centering width-loader">
              </div>  -->
            <!-- <div class="warp-evt-group">
              <div class="tb-header">
                       <div class="tb-pp">
                           <a href=""><img src="{{asset('img/timeline/1.jpg')}}" class="img-responsive img-rounded-full"></a>
                       </div>
                       <div class="tb-info">
                           <h3><a href="">Utama</a></h3>
                       </div>
                       <div class="tb-time">
                           <p>35m</p>
                       </div>
                   </div>
                   <div class="tb-content">
                       <div class="tb-img evt-g">
                           <img src="{{asset('img/album/album-02_475x300.jpg')}}" class="img-responsive img-full-responsive">
                       </div>
                       <div class="tb-bname evt-t">
                        <h2>Event Title 1</h2>
                        <h5>Place Event</h5>
                         <h5>25 December 2016</h5>
                        <div class="tag-evt">
                          <a href="">#ezListening</a>
                             <a href="">#modernJazz</a>
                         </div>
                       </div>
                       <div class="tb-desc evt-d">
                           <div class="desc-evt-g">
                               <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                               tempor incididunt ut labore et dolore magna aliqua.</p>
                           </div>
                           <div class="tb-more">
                                <a href="more">more</a> 
                           </div>
                      </div>
                   </div>
                   <div class="warp-stat-card in-tl">
                    <div class="add-pl-track-song"></div>
                    <div class="stat-song-track"></div>
                    <div class="stat-song-track">
                      <div class="warp-ico-stat">
                          <div class="ico-stat"><a href="javascript:void(0)" onclick="clickLike();"><i class="fa fa-heart like-single-list-song"></i></a></div>
                          <div class="num-stat">31</div>
                      </div>
                    </div>
                    <div class="stat-song-track">
                      <div class="warp-ico-stat">
                          <div class="ico-stat"><a class="btn-share-ts"><i class="fa fa-mail-forward forward-single-list-song"></i></a></div>
                          <div class="num-stat">9999K</div>
                      </div>
                    </div>
                   </div>
                   <div class="warp-all-comment-post">
                       <div class="tb-more-comment ext-left">
                               <a href="#" class="more-txt">read more comments</a>
                       </div>
                   
                       <div class="tb-comment">
                             <div class="hr"></div> 
                           
                           <div class="tb-pp">
                               <a href=""><img src="{{asset('img/timeline/4.jpg')}}" class="img-responsive img-rounded-full"></a>
                           </div>
                           <div class="tb-info">
                               <h3><a href="">Iyutt</a></h3>
                           </div>
                           <div class="tb-time">
                               <p>30m</p>
                           </div>
                           <div class="tb-caption">
                               <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                   tempor incididunt ut labore et dolore magna aliqua. Consectetur adipisicing elit, sed do eiusmod
                                   tempor incididunt ut labore et dolore magna aliqua.
                               </p>
                               <a href="#" class="more-txt">more</a>
                               <div class="tb-like text-right">
                                   <a href="">Like</a>
                                   <a href="">Reply</a>
                               </div>
                           </div>
                       </div>
                   </div>
                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sxs-12">
                   <div class="row">  
                   <div class="hr"></div> 
                     <form class="warp-comment-post">
                         <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 col-sxs-2 padten">
                         <div class="warp-user-comment-post">
                             <img src="{{asset('img/avatar/avatar-1.jpg')}}" class="img-responsive img-rounded-full">
                         </div>
                         </div>
                         <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 col-sxs-7 pad-xs-phone">
                             <textarea class="form-control" placeholder="Write Comments ..."></textarea>
                         </div>
                         <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 related-md-desktop">
                             <button type="submit" class="btn btn-comment-post">Comment</button>
                         </div>
                     </form>
                   </div>
                   </div>
              </div>   -->
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-12">
        <div id="sidrRight" class="sec-side-right-wall fullwidth">
          <div class="row">
            <div id="fixRightSideBar" class="right-side-bar">
              <div class="col-lg-12">
                <div class="warp-btn-back-side-right hidden-lg hidden-md">
                  <a class="btn-back-side-right" href="#"><i class="fa fa-arrow-left"></i></a>
                </div>
              </div>

              <div class="warp-side-left-wall">
                    <div class="warp-title-side-left-wall">
                        <h4>My Event</h4>
                        <a href="#" class="btn-detail-sosmed pull-right"><i class="fa fa-bars"></i><span>detail</span></a>
                    </div>
                    <div class="warp-item-side-left" id="printloadingGroupEvent">
                        <div class="col-xs-push-5 col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 lazyloader-list" id='loadingGroupEvent'>
                          <img src="{{asset('/img/loader-more.gif')}}" class="img-full-responsive e-centering width-loader">
                        </div>
                        <input id="skipevent" type="hidden" value="0">
                        <input id="session" type="hidden" value="{{$id}}">
                    </div>
                </div>

              <!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="warp-cal-event-wall">
                  <div class="warp-btn-add-evt-cal">
                    <a class="btn-add-evt-cal"><i class="fa fa-plus-circle"></i></a>
                  </div>
                  <div class="warp-evt-month-year">
                    <div class="evt-month-year"><span class="month">APRIL</span><span class="year">2016</span></div>
                    <div class="clearfix"></div>
                    <div class="evt-date"><a class="btn-date-evt">24</a></div>
                  </div>
                  <div class="warp-evt-name-wall">
                    <a class="btn-det-evt">
                      <h3>Hell Fest 2016</h3>
                    </a>
                  </div>
                  <div class="warp-evt-place-time">
                    <div class="place-evt">
                      Parkir Hall Senayan
                    </div>
                    <div class="time-evt">
                      <span class="start">09:00 AM</span>
                      <span class="finish"> 23:00 PM</span>
                    </div>
                  </div>
                </div>
              </div> -->
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sxs-12">
                <div class="warp-ads-right">
                  <div class="ads-box-right hidden-xs hidden-lg hidden-sm">
                    <a href=""><img src="{{asset('img/ads/ads_200x200.jpg')}}"></a>
                  </div>
                  <div class="side-ads-box e-centering hidden-md">
                    <a href=""><img src="{{asset('img/ads/ads_300x250.jpg')}}" class="e-centering"></a>
                  </div>
                </div>
              </div>
            <!--   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sxs-12">
                <div class="warp-list-song-rec-wall">
                  <div class="tittle-rec-wall">
                    <h5> Recommended Today</h5>
                  </div>
                  <ul>
                    <li>
                      <div class="warp-btn-buy-rec-wall">
                        <a href="" class="btn-buy-rec-wall"><i class="fa fa-plus-square-o"></i><span>Buy This Item</span></a>
                      </div>
                      <div class="item-rec-song">
                        <div class="pl-img-popup">
                          <img src="{{asset('img/album/album-06.jpg')}}" class="img-responsive">
                        </div>
                        <div class="rec-tittle-name-popup">
                          <div class="rec-tittle-popup">Nama Band Max 15 Ch</div>
                          <div class="rec-name-popup">Judul Lagu Max 15 Ch</div>
                        </div>
                        <div class="rec-stat-popup">
                          <div class="rec-stat-like-popup">
                            <a><i class="fa fa-heart like-single-list-song"></i></a>
                            <span>120</span>
                          </div>
                          <div class="rec-stat-forward-popup">
                            <a><i class="fa fa-mail-forward forward-single-list-song"></i></a>
                            <span>56</span>
                          </div>
                          <div class="rec-stat-forward-popup">
                            <a><i class="fa fa-plus-square plus-single-list-song"></i></a>
                            <span>120K</span>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li>
                      <div class="warp-btn-buy-rec-wall">
                        <a href="" class="btn-buy-rec-wall"><i class="fa fa-plus-square-o"></i><span>Buy This Item</span></a>
                      </div>
                      <div class="item-rec-song">
                        <div class="pl-img-popup">
                          <img src="{{asset('img/album/album-05.jpg')}}" class="img-responsive">
                        </div>
                        <div class="rec-tittle-name-popup">
                          <div class="rec-tittle-popup">Nama Band Max 15 Ch</div>
                          <div class="rec-name-popup">Judul Lagu Max 15 Ch</div>
                        </div>
                        <div class="rec-stat-popup">
                          <div class="rec-stat-like-popup">
                            <a><i class="fa fa-heart like-single-list-song"></i></a>
                            <span>120</span>
                          </div>
                          <div class="rec-stat-forward-popup">
                            <a><i class="fa fa-mail-forward forward-single-list-song"></i></a>
                            <span>56</span>
                          </div>
                          <div class="rec-stat-forward-popup">
                            <a><i class="fa fa-plus-square plus-single-list-song"></i></a>
                            <span>120K</span>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li>
                      <div class="warp-btn-buy-rec-wall">
                        <a href="" class="btn-buy-rec-wall"><i class="fa fa-plus-square-o"></i><span>Buy This Item</span></a>
                      </div>
                      <div class="item-rec-song">
                        <div class="pl-img-popup">
                          <img src="{{asset('img/album/album-04.jpg')}}" class="img-responsive">
                        </div>
                        <div class="rec-tittle-name-popup">
                          <div class="rec-tittle-popup">Nama Band Max 15 Ch</div>
                          <div class="rec-name-popup">Judul Lagu Max 15 Ch</div>
                        </div>
                        <div class="rec-stat-popup">
                          <div class="rec-stat-like-popup">
                            <a><i class="fa fa-heart like-single-list-song"></i></a>
                            <span>120</span>
                          </div>
                          <div class="rec-stat-forward-popup">
                            <a><i class="fa fa-mail-forward forward-single-list-song"></i></a>
                            <span>56</span>
                          </div>
                          <div class="rec-stat-forward-popup">
                            <a><i class="fa fa-plus-square plus-single-list-song"></i></a>
                            <span>120K</span>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li>
                      <div class="warp-btn-buy-rec-wall">
                        <a href="" class="btn-buy-rec-wall"><i class="fa fa-plus-square-o"></i><span>Buy This Item</span></a>
                      </div>
                      <div class="item-rec-song">
                        <div class="pl-img-popup">
                          <img src="{{asset('img/album/album-03.jpg')}}" class="img-responsive">
                        </div>
                        <div class="rec-tittle-name-popup">
                          <div class="rec-tittle-popup">Nama Band Max 15 Ch</div>
                          <div class="rec-name-popup">Judul Lagu Max 15 Ch</div>
                        </div>
                        <div class="rec-stat-popup">
                          <div class="rec-stat-like-popup">
                            <a><i class="fa fa-heart like-single-list-song"></i></a>
                            <span>120</span>
                          </div>
                          <div class="rec-stat-forward-popup">
                            <a><i class="fa fa-mail-forward forward-single-list-song"></i></a>
                            <span>56</span>
                          </div>
                          <div class="rec-stat-forward-popup">
                            <a><i class="fa fa-plus-square plus-single-list-song"></i></a>
                            <span>120K</span>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li>
                      <div class="warp-btn-buy-rec-wall">
                        <a href="" class="btn-buy-rec-wall"><i class="fa fa-plus-square-o"></i><span>Buy This Item</span></a>
                      </div>
                      <div class="item-rec-song">
                        <div class="pl-img-popup">
                          <img src="{{asset('img/album/album-02.jpg')}}" class="img-responsive">
                        </div>
                        <div class="rec-tittle-name-popup">
                          <div class="rec-tittle-popup">Nama Band Max 15 Ch</div>
                          <div class="rec-name-popup">Judul Lagu Max 15 Ch</div>
                        </div>
                        <div class="rec-stat-popup">
                          <div class="rec-stat-like-popup">
                            <a><i class="fa fa-heart like-single-list-song"></i></a>
                            <span>120</span>
                          </div>
                          <div class="rec-stat-forward-popup">
                            <a><i class="fa fa-mail-forward forward-single-list-song"></i></a>
                            <span>56</span>
                          </div>
                          <div class="rec-stat-forward-popup">
                            <a><i class="fa fa-plus-square plus-single-list-song"></i></a>
                            <span>120K</span>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li>
                      <div class="warp-btn-buy-rec-wall">
                        <a href="" class="btn-buy-rec-wall"><i class="fa fa-plus-square-o"></i><span>Buy This Item</span></a>
                      </div>
                      <div class="item-rec-song">
                        <div class="pl-img-popup">
                          <img src="{{asset('img/album/album-01.jpg')}}" class="img-responsive">
                        </div>
                        <div class="rec-tittle-name-popup">
                          <div class="rec-tittle-popup">Nama Band Max 15 Ch</div>
                          <div class="rec-name-popup">Judul Lagu Max 15 Ch</div>
                        </div>
                        <div class="rec-stat-popup">
                          <div class="rec-stat-like-popup">
                            <a><i class="fa fa-heart like-single-list-song"></i></a>
                            <span>120</span>
                          </div>
                          <div class="rec-stat-forward-popup">
                            <a><i class="fa fa-mail-forward forward-single-list-song"></i></a>
                            <span>56</span>
                          </div>
                          <div class="rec-stat-forward-popup">
                            <a><i class="fa fa-plus-square plus-single-list-song"></i></a>
                            <span>120K</span>
                          </div>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div> -->
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sxs-12">
                <div class="warp-ads-right">
                  <div class="ads-box-right hidden-xs hidden-lg hidden-sm">
                    <a href=""><img src="{{asset('img/ads/ads_200x200.jpg')}}"></a>
                  </div>
                  <div class="side-ads-box e-centering hidden-md">
                    <a href=""><img src="{{asset('img/ads/ads_300x250.jpg')}}" class="e-centering"></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- ================ WARP 3 BAGIAN ================ -->
  </div>
</div>
<div class="block-white"></div>
<!-- ---------- END MARKET MUSIC ------------------ -->
<!-- ============================= MODAL CREATE INVITE FRIEND TO GROUP ============================================= --> 
<div id="inviteFriend" tabindex="-1" role="dialog" aria-hidden="true" class="modal vh-center fade" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body nopadding">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sxs-12">
            <div class="warp-crt-add-playlist">
              <div class="modal-header color-gj-popup-report">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Invite Friends to Group</h4>
              </div>
              <div class="warp-15">
                @if(!empty($friend))
                <form class="bs-example form-horizontal" method="post" action="/inviteGroupProses" enctype="multipart/form-data">
                  <?php
                    echo "<input type='hidden' name='group_id' value='{$groupID}'>";
                    ?>
                  <div class="form-group">
                    <label class="col-lg-12 pull-left">Invite Friends</label>
                    <div class="col-lg-12">
                      <!-- @if(!empty($friend)) -->
                      <select data-placeholder="Choose Your Friends..." class="chosen-select" multiple tabindex="4" style="display:block;" name="friend_id[]">
                        @foreach ($friend as $key => $value)
                        <option value="{{$value['friend_id']}}" id="{{$value['friend_id']}}" name="{{$value['friend_id']}}">{{$value['friend_username']}}</option>
                        @endforeach
                      </select>
                      <!--   @else
                        <select data-placeholder="Anda Belum Memiliki teman..." class="chosen-select" multiple tabindex="4" style="display:block;" name="friend_id[]">
                        
                        </select>
                        @endif    -->
                    </div>
                  </div>
                  <div class="l-hr"></div>
                  <div class="form-group">
                    <div class="col-lg-12">
                      <!-- @if(!empty($friend)) -->
                      <button class="btn bgc-btn c-f-btn crt-group">Invite</button>
                      <!-- @endif -->
                    </div>
                  </div>
                </form>
                @else
                <div class="form-group">             
                  <label class="col-lg-12 pull-left">You don't have a friend or all your friends are already a member in this group <a href='{{$baseUrl}}friend'>Get Friends</a></label>
                </div>
                @endif   
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- ============================= END MODAL CREATE INVITE FRIEND GROUP =========================================== -->   

<!-- ============================= MODAL REJECT GROUP MEMBER PENDING =========================================== -->   
<div id="modalRejectGroupMemberPending" class="vh-center modal fade">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header color-gj-popup-report">
       <button type="button" class="close close-button-reject-group-member-pending" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Reject Invitation</h4>
      </div>
      <div class="modal-body">
         <span id="data-groupid-reject-confirm" style="display: none;"></span>
         <span id="data-username-reject-confirm" style="display: none;"></span>
         <span id="data-userid-reject-confirm" style="display: none;"></span>
        <div class="row">
          <div class="col-sm-12">
            <p>Are you sure to reject invitation from group <strong>"<span id="data-groupname-reject-confirm"></span>"</strong> ?</p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <span class="loader-process" style="display: none;margin-right: 5px;"><img src="{{asset('img/loader-24.gif')}}" /></span>
        <a href="#" class="btn-s-gj bgc-btn close-button-reject-group-member-pending" data-dismiss="modal" style="margin-right: 5px;">Cancel</a>
        <a href="#" class="btn-s-gj bgc-btn process-not-interest-group-member-reject" id="button-reject-group-member-pending">Yes</a>
      </div>
    </div>
  </div>
</div>
<!-- ============================= MODAL CANCEL INVITE ============================================= --> 
<div id="AdminCancelInvitation" class="vh-center modal fade">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header color-gj-popup-report">
       <button type="button" class="close close-button-admin-cancel-invitation" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Cancel Invitation</h4>
      </div>
      <div class="modal-body">
           <span id="data-groupid-cancel-invite" style="display: none;"></span>
           <span id="data-memberid-cancel-invite" style="display: none;"></span>
        <div class="row">
          <div class="col-sm-12">
            <p>Are you sure Cancel Invite <strong><span id="data-username-cancel-invite"></span></strong> in group <strong>"<span id="data-groupname-cancel-invite"></span>"</strong> ?</p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <span class="loader-process" style="display: none;margin-right: 5px;"><img src="{{asset('img/loader-24.gif')}}" /></span>
        <a href="#" class="btn-s-gj bgc-btn close-button-admin-cancel-invitation" data-dismiss="modal" style="margin-right: 5px;">Cancel</a>
        <a href="#" class="btn-s-gj bgc-btn proses-admin-cancel-invite" id="button-admin-cancel-invitation">Accept</a>
      </div>
    </div>
  </div>
</div>
<!-- ============================= MODAL REQUEST JOIN GROUP ============================================= --> 
<div id="AdminConfirmRequest" class="vh-center modal fade">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header color-gj-popup-report">
       <button type="button" class="close close-button-admin-confirm-request" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Confirm Request to Group</h4>
      </div>
      <div class="modal-body">
          <span id="data-groupid-request" style="display: none;"></span>
          <span id="data-memberid-request" style="display: none;"></span>
        <div class="row">
          <div class="col-sm-12">
            <p><strong><span id="data-username-request"></span></strong> has requested join to group <strong>"<span id="data-groupname-request"></span>"</strong> ?</p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <span class="loader-process" style="display: none;margin-right: 5px;"><img src="{{asset('img/loader-24.gif')}}" /></span>
        <a href="#" class="btn-s-gj bgc-btn close-button-admin-confirm-request" data-dismiss="modal" style="margin-right: 5px;">Cancel</a>
        <a href="#" class="btn-s-gj bgc-btn proses-admin-confirm-request" id="button-admin-confirm-request">Accept</a>
      </div>
    </div>
  </div>
</div>
<!-- ============================= MODAL REJECT REQUEST JOIN GROUP ============================================= --> 
<div id="AdminRejectRequest" class="vh-center modal fade">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header color-gj-popup-report">
       <button type="button" class="close close-button-admin-reject-request" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Reject Request</h4>
      </div>
      <div class="modal-body">
           <span id="data-groupid-request-reject" style="display: none;"></span>
           <span id="data-memberid-request-reject" style="display: none;"></span>
           <span id="data-username-request-reject" style="display: none;"></span>
        <div class="row">
          <div class="col-sm-12">
            <p>Are you sure reject requested<strong><span id="data-username-request-reject"></span></strong>to join in group <strong>"<span id="data-groupname-request-reject"></span>"</strong> ?</p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <span class="loader-process" style="display: none;margin-right: 5px;"><img src="{{asset('img/loader-24.gif')}}" /></span>
        <a href="#" class="btn-s-gj bgc-btn close-button-admin-reject-request" data-dismiss="modal" style="margin-right: 5px;">Cancel</a>
        <a href="#" class="btn-s-gj bgc-btn proses-admin-cancel-request" id="button-admin-reject-request">Accept</a>
      </div>
    </div>
  </div>
</div>
@stop