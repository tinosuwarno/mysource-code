@extends('layouts.default-base')
{{-- overwrite title --}}
@section('title')

Gritjam - Group Post
@overwrite

@section('styles')
  
@stop

{{-- content --}}
@section('contents')
<?php
    $session = Session::get('data');
    $id = $session['id'];
    $profile_image = $session['profile_image'];

    $myApp = App::make('baseUrl'); 
    $baseUrl = $myApp->baseUrl ; 
?>
  
  
<!-- ---------- SECTION MARKET MUSIC ----------------- --- -->
<div class="block-white"></div>
<div class="block-white visible-sm hidden-lg hidden-xs hidden-md"></div>
<div class="block-white visible-sm hidden-lg hidden-xs hidden-md"></div>

@include('layouts.bg-group-setting')

<div class="container-fluid">
  <div class="row">
            <div class="md-w-warp">
                <div class="col-lg-12">
                    <div class="menu text-center menu-wall motion-animate">
                        <div class="hr"></div>
                          <div class="warp-btn-song-noaffix hidden-lg hidden-md">
                              <a id="btnEvent-affixLeft" class="btn-song-affix" href="#sidrLeft"><i class="fa fa-bars"></i></a>
                            </div>
                        <ul class="action-menu-profile">
                            <li>
                                <a id='pjax' data-pjax='yes' href='{{url("about-me")}}'>About</a>
                            </li>
                            <li>
                                <a id='pjax' data-pjax='yes' href='{{url("myWall")}}'>My wall</a>
                            </li>
                            <li>
                                <a data-pjax='yes' id='pjax' href="{{ url('profile/following', $parameters = [$id], $secure = null); }}">Timeline</a>
                            </li>
                            <li class="active">
                                <a data-pjax='yes' id='pjax' href="{{ url('group') }}">Groups</a>
                            </li>
                            <li>
                                <a data-pjax='yes' id='pjax' href="{{ url('myProject') }}">Project</a>
                            </li>
                        </ul>
                  <div class="warp-btn-event-noaffix hidden-lg hidden-md">
                              <a id="btnEvent-affix2" class="btn-event-affix" href="#sidr"><i class="fa fa-calendar"></i></a>
                            </div>
                        <div class="hr"></div>
                    </div>
                </div>
                <div class="clearfix"></div>
                
                <div class="warp-all-wall">
                    @include('group.group-menu')
                    
                    <div class="col-lg-6 col-md-6 col-sm-12 padten-sxs">
                    <div class="warp-over-timeline">
                      <div class="warp-post-wall">
                          <ul class="menu-post-wall">
                              <li class="dropdown"><a href="#" class="btn-post-music-wall dropdown-toggle" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-music"></i>Add Your Music</a>
                                  <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenuLink">
                                      <li class="submenu-music"><a tabindex="-1" href="#" class="postMusic-bank">Released Songs</a></li>
                                      <li class="submenu-music"><a tabindex="-1" href="#" class="postMusic-dir">From Karaoke</a></li>
                                    </ul>
                                </li>
                                <!-- <li><a class="btn-post-status-wall"><i class="fa fa-microphone"></i>Go Karaoke</a></li> -->
                            </ul>
                           
                            @if($group[0]['checkJoined'] == 'joined') 
                                <span id='group_name-sharedGroupProfile' style='display:none;'>{{$group[0]['group_name']}}</span>
                                <span id='group_picture-sharedGroupProfile' style='display:none;'>{{$group[0]['picture']}}</span>
                                <span id='group_description-sharedGroupProfile' style='display:none;'>{{$group[0]['description']}}</span>
                                <span id='group_id-sharedGroupProfile' style='display:none;'>{{$groupID}}</span>
                                <div class="warp-btn-r-t"><button style='margin-top:12px;' class="btn col-btn c-f-btn crt-group"><i class='fa fa-mail-forward forward-single-list-song'> Share Group Profile</i></button></div>
                            @endif
                            <div class="warp-song-selected" id="printselectsong"></div>
                            <div class="warp-form-wall">
                             <form id="postgroup" onsubmit="return submitpostgroup();" method="get" class="tb-form-input-2">
                                          <textarea id="fieldpost" name="text" form="postgroup" class="text-wall form-control" placeholder="Type Something ..."></textarea>
                                          <input id="group_id" name="group_id" type="hidden" value="<?php echo $data['id']; ?>"></input>
                                          <input id="user_id" name="user_id" type="hidden" value="<?php echo $id; ?>"></input>
                                          <input id="track_id" name="track_id" type="hidden"></input>
                                          <!-- <textarea class="text-wall form-control" placeholder="Whats Your Status ..."></textarea> -->
                                          <div class="warp-option-wall">
                                            <ul>
                                              <!-- <li><a class="btn-add-friend-wall"><i class="fa fa-user-plus"></i></a></li>
                                                <li><a class="btn-share-wall"><i class="fa fa-share"></i></a></li>
                                                <li><a class="btn-marker-wall"><i class="fa fa-map-marker"></i></a></li> -->
                                            </ul>
                                          </div>
                                          <div class="warp-btn-compose-wall">
                                                   <input id="buttonsubmitpostgrup" type="submit" value="Post" class="btn-compose-wall">
                                                   <!-- <select class="select-place-wall">
                                                      <option>Public</option>
                                                      <option>Private</option>
                                                    </select> -->
                                          </div>
                                </form>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="block-white"></div>
                              <!-- ------------ JIKA MASIH KOSONG ----------- -->
                                    <!-- <div class="nf-item-wall">
                                      <p>You Don't Have any Post Yet</p>
                                    </div> -->
                                    <!-- ------------ END JIKA MASIH KOSONG ----------- -->
                        <div class="col-xs-push-5 col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 lazyloader-list" style="display: none;margin-bottom: 30px;" id="loaderpost">
                                <img src="{{asset('img/loader-more.gif')}}" class="img-full-responsive e-centering width-loader">
                            </div> 
                            <input type="hidden" id="skip" value="0">
                          <div id="printpostgrup"></div>
                         @if(empty($data1['data']))
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padten">
                                    <div class="nf-item-wall">
                                        <p>You Don't Have any Post Yet</p>
                                    </div>
                                    </div>

                            <input type='hidden' value='1' id='hidebuttongrup'>
                          
                         @else
                         
                         @foreach ($data1['data'] as $key)
                          <?php if ($key['track_id']=="0") {?>
                          <div class="timeline-box">
                              <hr class="tb-hr">  
                                <div class="tb-header">
                                      <div class="tb-pp">
                                          <a href=""><img src=<?php
                                                              if(!empty($key['profile_image'])){
                                                                  $url = asset($key["profile_image"]);
                                                                  echo "\"" . $url . "\"";
                                                              }
                                                              else{
                                                                  $url = asset('/img/avatar/avatar-300x300.png');
                                                                  echo $url;
                                                              }
                                                              ?> class="img-responsive img-rounded-full"></a>
                                      </div>
                                      <div class="tb-info">
                                          <h1><a href="/{{$key['username']}}" class="btn-single-post-tl">{{$key['username']}}</a></h1>
                                          <!-- <h1><a href="" class="btn-single-post-tl">Project/Album/Song title</a></h1> -->
                                      </div>
                                      <div class="tb-time">
                                      <?php $created_at = $key['created_at'];
                                                $ago= Carbon::createFromTimeStamp(strtotime($created_at))->diffForHumans(); 
                                                ?>
                                          <p>{{ $ago }}</p>
                                      </div>
                                  </div>
                                  <div class="tb-content">
                                      <!-- <div class="tb-img">
                                          <img src="/img/album/album-02_475x300.jpg" class="img-responsive img-full-responsive">
                                      </div> -->
                                      <div class="warp-entry-com">
                                          <!-- <div class="tb-bname">
                                              <h2>Artist/Band name</h2>
                                              <h5>Genre</h5>
                                          </div> -->
                                          <div class="tb-caption">
                                              <p>
                                                {{$key['caption']}}
                                                <a id="pjax" data-pjax="yes" href="{{$baseUrl}}group/singlepost/{{$key['id']}}/{{$groupID}}" class='more-txt'>more</a>
                                              </p>
                                             
                                          </div>
                                          <!-- <div class="tb-more">
                                               <a href="more">more</a> 
                                          </div> -->
                                     </div>
                                     <!-- <div class="play-post-ctn">
                                            <div><a class="btn-play"><i class="fa fa-play-circle-o"></i></a></div>
                                            <div class="warp-player-ctn">
                                                <div class="seek-bar-player-ctn" style="width:100%;">
                                                    <div class="progres-bar-player" style="width:24%;"></div>
                                                </div>
                                                <div class="bar-duration-ctn">01:25</div>
                                            </div>
                                     </div> -->
                                     <div class="warp-stat-card in-tl">
                                      <!-- <div class="add-pl-track-song">
                                          <a class="btn-add-pl-track-song"><i class="gritjam-icons gritjam-icon-add-playlist"></i></a>
                                      </div> -->
                                      <div class="stat-song-track">
                                          <!-- <div class="warp-ico-stat">
                                              <div class="ico-stat"><a href="javascript:void(0)" onclick="clickLike();"><i class="fa fa-heart like-single-list-song"></i></a></div>
                                              <div class="num-stat">31</div>
                                          </div> -->
                                        </div>
                                        <div class="stat-song-track">
                                          <div class="warp-ico-stat">
                                              <div class="ico-stat">
                                                <a href="javascript:void(0);" onclick="shareSinglePost({{$key['id']}})" class="btn-share-ts"><i class="fa fa-mail-forward forward-single-list-song"></i></a>
                                              </div>
                                              <div class="num-stat">0</div>
                                          </div>
                                        </div>
                                      <div class="stat-song-track">
                                          <div class="warp-ico-stat">
                                              <div class="ico-stat">
                                              @if(!array_key_exists($key['id'],$list))
                                              <a id="likePostGroup-{{$key['id']}}" onclick="likePostGroup({{$key['id']}},1,{{$key['id']}})"><i class="fa fa-heart like-single-list-song"></i></a>
                                              @else
                                              <a id="unlikePostGroup-{{$key['id']}}" onclick="likePostGroup({{$key['id']}},2,{{$key['id']}})"><i style="color: red;" class="fa fa-heart like-single-list-song"></i></a>
                                              @endif
                                              </div>
                                              <div id="num-stat-{{$key['id']}}" class="num-stat">{{$key['liked']}}</div>
                                          </div>
                                        </div>
                                      </div>
                                  </div>
                                  <div class="warp-all-comment-post">
                                      <div class="tb-more-comment ext-left">
                                      @if($data1['list'][$key['id']]==0)
                                              <p></p>
                                      @else
                                              <div class='tot-comment'>
                                            <a style='cursor:pointer' id="readallcomments-{{$key['id']}}" onclick="readallcommentsNew({{$key['id']}}, {{$data1['list'][$key['id']]}})">
                                              <i class='fa fa-comment'></i>Read {{$data1['list'][$key['id']]}} Comments
                                            </a>
                                              <!-- <a id="readallcomments-{{$key['id']}}" onclick="readallcomments({{$key['id']}})" class="more-txt">read more comments</a> -->
                                              <a id="hideallcomments-{{$key['id']}}" onclick="hideallcomments({{$key['id']}},{{$data1['list'][$key['id']]}})" class="more-txt" style="display: none;"><i class='fa fa-comment'></i>Hide comments</a>  
                                            </div>
                                      @endif
                                      </div>
                                          
                                      <div id="printallcomments-{{$key['id']}}"></div>
                                      <div id="loaderkomenall-{{$key['id']}}" style="display: none;" class="col-xs-push-5 col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 lazyloader-list">
                                            <img src="/img/loader-more.gif" class="img-full-responsive e-centering width-loader">
                                          </div> 
                                  <?php  
                                    $x = 0;
                                    $itung = 1;
                                    foreach ($data4['data123'] as $value) {
                                      if ($value['group_post_id']==$key['id']) {
                                  ?>
                                  <?php 
                                    if ($data1['list'][$key['id']]<=3) {
                                      echo "<div name=comment-" . $key['id'] . " class='tb-comment totkomengrup-".$key['id']."'>";
                                    }else{
                                      if ($itung <= 3) {
                                       echo "<div name=comment-" . $key['id'] . " class='tb-comment totkomengrup-".$key['id']."'>";
                                      }else{
                                       echo "<div style='display: none;' name=comment-" . $key['id'] . " class='tb-comment totkomengrup-".$key['id']."'>";
                                      }
                                    }
                                    $itung++;
                                  ?><!-- 
                                      <div class="tb-comment totkomengrup-{{$value['comment_id']}}" style="display: none;"> -->
                                            <div class="hr-comment"></div>  
                                          
                                          <div class="tb-pp">
                                              <a href=""><img src=<?php
                                                              if(!empty($value['profile_image'])){
                                                                  $url = asset($value["profile_image"]);
                                                                  echo "\"" . $url . "\"";
                                                              }
                                                              else{
                                                                  $url = asset('/img/avatar/avatar-300x300.png');
                                                                  echo $url;
                                                              }
                                                              ?> class="img-responsive img-rounded-full"></a>
                                          </div>
                                          <div class="tb-text-comment">
                                          <div class="tb-info">
                                              <h3><a href="/{{$value['username']}}">{{$value['username']}}</a></h3>
                                          </div>
                                          <div class="tb-time">
                                              <p>{{$value['created_at']}}</p>
                                          </div>
                                          <div class="tb-caption">
                                              <p>{{$value['comment']}}
                                              </p>
                                              
                                              <div class="tb-like text-right">
                                              <?php  
                                                  if ($value['liked']==1) {
                                              ?>
                                                  <a id="like{{$value['comment_id']}}" style='cursor: pointer' onclick="likeCommentGroup({{$value['comment_id']}},2,{{$value['comment_id']}});">Unlike</a>
                                              <?php
                                                  }else{
                                              ?>
                                                  <a id="like{{$value['comment_id']}}" style='cursor: pointer' onclick="likeCommentGroup({{$value['comment_id']}},1,{{$value['comment_id']}});">Like</a>
                                              <?php
                                                  }
                                              ?>
                                                  <a class="replycommentgrup" id="replycommentgrup-{{$value['comment_id']}}" onclick="replycommentgrup({{$key['id']}},{{$value['comment_id']}})" style="display: inline;">Reply</a>
                                              </div>
                                          </div>
                                          </div>
                                          
                                          <div id="grup-comment-in-comment-{{$value['comment_id']}}" class="comment-in-comment">
                                          
                                            <?php $count = count($data4['replycomment'][$x]); 
                                                  if ($count==0) {
                                                    echo "";
                                                  }else{
                                            ?>
                                            <a id="seeSubKomenGroup-{{$value['comment_id']}}" href="javascript:toggleAndChangeReplyGroup({{$value['comment_id']}});" class="btn-expand-comment">See all comment</a>
                                              <div class="num-comment"><span>{{$count}}</span> Comment hidden</div>
                                            <?php } ?>
                                            <div style="display: none;" id="subKomen_1-{{$value['comment_id']}}" class="warp-sub-comment sub-comment">
                                            <?php 
                                              foreach ($data4['replycomment'][$x] as $val) {
                                                if ($val['comment_id']==$value['comment_id']) {
                                            ?>
                                              <div class="tb-comment">
                                                  <div class="hr-comment"></div>  
                                              <div class="tb-pp"><a href=""><img src=<?php
                                                              if(!empty($val['profile_image'])){
                                                                  $url = asset($val["profile_image"]);
                                                                  echo "\"".$url."\"";
                                                              }
                                                              else{
                                                                  $url = asset('/img/avatar/avatar-300x300.png');
                                                                  echo $url;
                                                              }
                                                              ?>></a></div>
                                              <div class="tb-text-comment">
                                                  <div class="tb-info">
                                                      <h3><a href="">{{$val['username']}}</a></h3>
                                                  </div>
                                                  <div class="tb-time">
                                                      <?php $created_at = $val['created_at'];
                                                            $agokomen= Carbon::createFromTimeStamp(strtotime($created_at))->diffForHumans(); 
                                                      ?>
                                                      <p style="margin-bottom: 0; font-size: 11px; color: #ccc; line-height: 9px;">{{$agokomen}}</p>
                                                  </div>
                                                  <div class="tb-caption"> 
                                                      <p>{{$val['contents']}}<a href="#" class="more-txt"></a></p>
                                                      <div class="tb-like text-right"><a href=""></a><a href=""></a></div>
                                                  </div>
                                              </div>
                                              </div>
                                              <?php 
                                                }
                                              }
                                              ?>
                                              </div>
                                              <div name="textareagrup-{{$value['comment_id']}}" id="textareagrup-{{$value['comment_id']}}"></div>
                                      </div>
                                      
                                      </div>
                                      <?php      
                                          }
                                          $x++;
                                        }
                                        
                                      ?>
                                  </div>
                                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sxs-12">
                                  <div class="row"> 
                                  <div class="hr"></div>  
                                    <form id="komengrup-{{$key['id']}}" onsubmit="return komengrup({{$key['id']}})" class="warp-comment-post">
                                      <input name="user_id" type="hidden" value="<?php echo $id; ?>"></input>
                                      <input name="post_id" type="hidden" value="<?php echo $key['id']; ?>"></input>
                                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 col-sxs-2 padten">
                                        <div class="warp-user-comment-post">
                                            <img  id="profpicgrup" src=<?php
                                                              if(!empty($key['profile_image'])){
                                                                  $url = asset($key['profile_image']);
                                                                  echo "\"".$url."\"";
                                                              }
                                                              else{
                                                                  $url = asset('/img/avatar/avatar-300x300.png');
                                                                  echo $url;
                                                              }
                                                              ?> class="img-responsive img-rounded-full">
                                        </div>
                                        </div>
                                        <div id="append-{{$key['id']}}" class="col-lg-8 col-md-8 col-sm-8 col-xs-8 col-sxs-6 pad-xs-phone">
                                            <textarea id="fieldkomen-{{$key['id']}}" name="komen" form="komengrup-{{$key['id']}}" class="form-control" placeholder="Write Comments ..."></textarea>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 col-sxs-3 tl-related-dev">
                                            <input style="margin-right: 5px;" type="submit" value="Comment" class="btn btn-comment-tl">
                                            <input type="reset" value="cancel" style="display: none;" id="buttoncancelcomment-{{$key['id']}}" onclick="cancelcommentreplygrup({{$key['id']}})" class="btn btn-comment-tl">
                                        </div>
                                    </form>
                                  </div>
                                  </div>
                          </div>    
                                                              <?php
                                                              if(!empty($key['profile_image'])){
                                                                  $url1 = $key['profile_image'];
                                                              }
                                                              else{
                                                                  $url1 = '/img/avatar/avatar-300x300.png';
                                                              }
                                                              ?>
                                <input id="profile_image" name="profile_image" type="hidden" value="<?php echo $url1; ?>"></input>  
                            
                                <?php }else{ 
                                  ?>
                            
                            <div class="timeline-box">
                              <hr class="tb-hr">  
                                <div class="tb-header">
                                      <div class="tb-pp">
                                          <a href=""><img src=<?php
                                                              if(!empty($key['profile_image'])){
                                                                  $url = asset($key["profile_image"]);
                                                                  echo "\"" . $url . "\"";
                                                              }
                                                              else{
                                                                  $url = asset('/img/avatar/avatar-300x300.png');
                                                                  echo $url;
                                                              }
                                                              ?> class="img-responsive img-rounded-full"></a>
                                      </div>
                                      <div class="tb-info">
                                          <h1><a href="/{{$key['username']}}" class="btn-single-post-tl">{{$key['username']}}</a></h1>
                                          
                                      </div>
                                      <div class="tb-time">
                                          <?php $created_at = $key['created_at'];
                                                $ago= Carbon::createFromTimeStamp(strtotime($created_at))->diffForHumans(); 
                                                ?>
                                          <p>{{ $ago }}</p>
                                      </div>
                                  </div>
                                  <div class="tb-content">
                                      <div class="tb-img-post-song">
                                          <img src=<?php
                                                              if(!empty($key['hashPicture'])){
                                                                  $url = asset($key["hashPicture"]);
                                                                  echo "\"" . $url . "\"";
                                                              }
                                                              else{
                                                                  $url = asset('/img/album/album-song_475x475.jpg');
                                                                  echo $url;
                                                              }
                                                              ?> class="img-responsive img-full-responsive">
                                          <?php 
                                          $print_trackID = $key['track_details_id'] ;?>
                                          <a style='cursor: pointer;' class="gj-btn-play"><i onclick='playMusic("{{$print_trackID}}");' class="fa fa-play-circle-o fa-4x"></i></a>
                                      </div>
                                      <div class='warp-info-post-song'>
                                      <div class='gj-post-song-title'><a href=''>{{$key['name']}}</a></div>
                                        <div class='gj-post-song-author'>{{$key['artist']}}</div>
                                        <div class='gj-post-song-genre'>{{$key['genre']}}</div>
                                        <div class='gj-post-song-contribut'>
                                            <span><a href='#'></a></span>
                                        </div>
                                        <div class='gj-post-song-desc'>
                                              <p id='more " . $key['id'] . "'>{{$key['caption']}}
                                                  <a id="pjax" data-pjax="yes" href="{{$baseUrl}}group/singlepost/{{$key['id']}}/{{$groupID}}" class='more-txt'>more</a>
                                              </p>
                                      </div>
                                    </div>
                                     <!-- <div class="play-post-ctn">
                                            <div><a class="btn-play"><i class="fa fa-play-circle-o"></i></a></div>
                                            <div class="warp-player-ctn">
                                                <div class="seek-bar-player-ctn" style="width:100%;">
                                                    <div class="progres-bar-player" style="width:24%;"></div>
                                                </div>
                                                <div class="bar-duration-ctn">01:25</div>
                                            </div>
                                     </div> -->
                                     <div class="warp-stat-card in-tl">
                                      <!-- <div class="add-pl-track-song">
                                          <a class="btn-add-pl-track-song"><i class="gritjam-icons gritjam-icon-add-playlist"></i></a>
                                      </div> -->
                                      <div class="stat-song-track">
                                          <!-- <div class="warp-ico-stat">
                                              <div class="ico-stat"><a href="javascript:void(0)" onclick="clickLike();"><i class="fa fa-heart like-single-list-song"></i></a></div>
                                              <div class="num-stat">31</div>
                                          </div> -->
                                        </div>
                                        <div class="stat-song-track">
                                          <div class="warp-ico-stat">
                                              <div class="ico-stat">
                                                  <a href="javascript:void(0);" onclick="shareSinglePost({{$key['id']}})" class="btn-share-ts"><i class="fa fa-mail-forward forward-single-list-song"></i></a>
                                              </div>
                                              <div class="num-stat">0</div>
                                          </div>
                                        </div>
                                      <div class="stat-song-track">
                                          <div class="warp-ico-stat">
                                            <div class="ico-stat">
                                          @if(!array_key_exists($key['id'],$list))
                                              <a id="likePostGroup-{{$key['id']}}" onclick="likePostGroup({{$key['id']}},1,{{$key['id']}})"><i class="fa fa-heart like-single-list-song"></i></a>
                                          @else
                                              <a id="unlikePostGroup-{{$key['id']}}" onclick="likePostGroup({{$key['id']}},2,{{$key['id']}})"><i style="color: red;" class="fa fa-heart like-single-list-song"></i></a>
                                          @endif
                                              </div>
                                              <div id="num-stat-{{$key['id']}}" class="num-stat">{{$key['liked']}}</div>
                                          </div>
                                        </div>
                                      </div>
                                  </div>
                                  <div class="warp-all-comment-post">
                                      <div class="tb-more-comment ext-left">
                                            @if($data1['list'][$key['id']]==0)
                                              <p></p>
                                            @else
                                            <div class='tot-comment'>
                                            <a style='cursor:pointer' id="readallcomments-{{$key['id']}}" onclick="readallcommentsNew({{$key['id']}}, {{$data1['list'][$key['id']]}})">
                                              <i class='fa fa-comment'></i>Read {{$data1['list'][$key['id']]}} Comments
                                            </a>
                                              <a id="hideallcomments-{{$key['id']}}" onclick="hideallcomments({{$key['id']}},{{$data1['list'][$key['id']]}})" class="more-txt" style="display: none;"><i class='fa fa-comment'></i>Hide comments</a>  
                                            </div>
                                            @endif
                                      </div>
                                          
                                      <div id="printallcomments-{{$key['id']}}"></div>
                                      <div id="loaderkomenall-{{$key['id']}}" style="display: none;" class="col-xs-push-5 col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 lazyloader-list">
                                            <img src="/img/loader-more.gif" class="img-full-responsive e-centering width-loader">
                                          </div> 
                                      <?php  
                                    $x = 0;
                                    $itung = 1;
                                    foreach ($data4['data123'] as $value) {
                                      if ($value['group_post_id']==$key['id']) {
                                      ?>
                                      <?php 
                                        if ($data1['list'][$key['id']]<=3) {
                                          echo "<div name=comment-" . $key['id'] . " class='tb-comment totkomengrup-".$key['id']."'>";
                                        }else{
                                          if ($itung <= 3) {
                                           echo "<div name=comment-" . $key['id'] . " class='tb-comment totkomengrup-".$key['id']."'>";
                                          }else{
                                           echo "<div style='display: none;' name=comment-" . $key['id'] . " class='tb-comment totkomengrup-".$key['id']."'>";
                                          }
                                        }
                                        $itung++;
                                      ?>
                                            <div class="hr-comment"></div>  
                                          
                                          <div class="tb-pp">
                                              <a href=""><img src=<?php
                                                              if(!empty($value['profile_image'])){
                                                                  $url = asset($value["profile_image"]);
                                                                  echo "\"".$url."\"";
                                                              }
                                                              else{
                                                                  $url = asset('/img/avatar/avatar-300x300.png');
                                                                  echo $url;
                                                              }
                                                              ?> class="img-responsive img-rounded-full"></a>
                                          </div>
                                          <div class="tb-text-comment">
                                          <div class="tb-info">
                                              <h3><a href="/{{$value['username']}}">{{$value['username']}}</a></h3>
                                          </div>
                                          <div class="tb-time">
                                              <p>{{$value['created_at']}}</p>
                                          </div>
                                          <div class="tb-caption">
                                              <p>{{$value['comment']}}
                                              </p>
                                              
                                              <div class="tb-like text-right">
                                                  <?php  
                                                  if ($value['liked']==1) {
                                                  ?>
                                                      <a id="unlike{{$value['comment_id']}}" style='cursor: pointer' onclick="likeCommentGroup({{$value['comment_id']}},2,{{$value['comment_id']}});">Unlike</a>
                                                  <?php
                                                      }else{
                                                  ?>
                                                      <a id="like{{$value['comment_id']}}" style='cursor: pointer' onclick="likeCommentGroup({{$value['comment_id']}},1,{{$value['comment_id']}});">Like</a>
                                                  <?php
                                                      }
                                                  ?>
                                                  
                                                  <a class="replycommentgrup" id="replycommentgrup-{{$value['comment_id']}}" onclick="replycommentgrup({{$key['id']}},{{$value['comment_id']}})" style="display: inline;">Reply</a>
                                              </div>
                                          </div>
                                          </div>
                                          
                                         <div id="grup-comment-in-comment-{{$value['comment_id']}}" class="comment-in-comment">
                                         
                                            
                                            <?php $count = count($data4['replycomment'][$x]); 
                                                  if ($count==0) {
                                                    echo "";
                                                  }else{
                                            ?>
                                            <a id="seeSubKomenGroup-{{$value['comment_id']}}" href="javascript:toggleAndChangeReplyGroup({{$value['comment_id']}});" class="btn-expand-comment">See all comment</a>
                                              <div class="num-comment"><span>{{$count}}</span> Comment hidden</div>
                                            <?php } ?>
                                            <div style="display: none;" id="subKomen_1-{{$value['comment_id']}}" class="warp-sub-comment sub-comment">
                                            <?php 
                                              foreach ($data4['replycomment'][$x] as $val) {
                                                if ($val['comment_id']==$value['comment_id']) {
                                            ?>
                                              <div class="tb-comment">
                                                  <div class="hr-comment"></div>  
                                              <div class="tb-pp"><a href=""><img src=<?php
                                                              if(!empty($val['profile_image'])){
                                                                  $url = asset($val["profile_image"]);
                                                                  echo "\"".$url."\"";
                                                              }
                                                              else{
                                                                  $url = asset('/img/avatar/avatar-300x300.png');
                                                                  echo $url;
                                                              }
                                                              ?>></a></div>
                                              <div class="tb-text-comment">
                                                  <div class="tb-info">
                                                      <h3><a href="">{{$val['username']}}</a></h3>
                                                  </div>
                                                  <div class="tb-time">
                                                  <?php $created_at = $val['created_at'];
                                                        $agokomen= Carbon::createFromTimeStamp(strtotime($created_at))->diffForHumans(); 
                                                  ?>
                                                      <p style="margin-bottom: 0; font-size: 11px; color: #ccc; line-height: 9px;">{{$agokomen}}</p>
                                                  </div>
                                                  <div class="tb-caption"> 
                                                      <p>{{$val['contents']}}<a href="#" class="more-txt"></a></p>
                                                      <div class="tb-like text-right"><a href=""></a><a href=""></a></div>
                                                  </div>
                                              </div>
                                              </div>
                                              <?php      
                                                  }
                                                }
                                              ?>
                                              </div>
                                              <div name="textareagrup-{{$value['comment_id']}}" id="textareagrup-{{$value['comment_id']}}"></div>
                                      </div>
                                      
                                      </div>
                                      <?php      
                                          }
                                          $x++;
                                        }
                                      ?>
                                  </div>
                                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sxs-12">
                                  <div class="row"> 
                                  <div class="hr"></div>  
                                    <form id="komengrup-{{$key['id']}}" onsubmit="return komengrup({{$key['id']}})" class="warp-comment-post">
                                      <input name="user_id" type="hidden" value="<?php echo $id; ?>"></input>
                                      <input name="post_id" type="hidden" value="<?php echo $key['id']; ?>"></input>
                                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 col-sxs-2 padten">
                                        <div class="warp-user-comment-post">
                                            <img id="profpicgrup" src=<?php
                                                              if(!empty($key['profile_image'])){
                                                                  $url = asset($key['profile_image']);
                                                                  echo "\"" . $url . "\"";
                                                              }
                                                              else{
                                                                  $url = asset('/img/avatar/avatar-300x300.png');
                                                                  echo $url;
                                                              }
                                                              ?> class="img-responsive img-rounded-full">
                                        </div>
                                        </div>
                                        <div id="append-{{$key['id']}}" class="col-lg-8 col-md-8 col-sm-8 col-xs-8 col-sxs-6 pad-xs-phone">
                                            <textarea id="fieldkomen-{{$key['id']}}" name="komen" form="komengrup-{{$key['id']}}" class="form-control" placeholder="Write Comments ..."></textarea>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 col-sxs-3 tl-related-dev">
                                            <input style="margin-right: 5px;" type="submit" value="Comment" class="btn btn-comment-tl">
                                            <input type="reset" value="cancel" style="display: none;" id="buttoncancelcomment-{{$key['id']}}" onclick="cancelcommentreplygrup({{$key['id']}})" class="btn btn-comment-tl">
                                        </div>
                                    </form>
                                  </div>
                                  </div>
                          </div>    
                                                              <?php
                                                              if(!empty($key['profile_image'])){
                                                                  $url = $key["profile_image"];
                                                              }
                                                              else{
                                                                  $url = '/img/avatar/avatar-300x300.png';
                                                              }
                                                              ?>
                                <input id="profile_image" name="profile_image" type="hidden" value="<?php echo $url; ?>"></input>  
                                <?php } ?>    
                          @endforeach
                          <div id="printpostgrupmore"></div>
                          <div class="col-xs-12">
                                <div class="warp-btn-loadmore-list">
                                    <a id="buttonloadmore" onclick="loadmoregrup()" class="btn-loadmore-list">
                                        <i class="fa fa-plus-circle"></i>
                                        <span class="text-load-more-list">LOAD MORE</span>
                                    </a> 
                                    
                                    
                                    <div id="loadermorepost" style="display: none;" class="col-xs-push-5 col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 lazyloader-list">
                                        <img src="/img/loader-more.gif" class="img-full-responsive e-centering width-loader">
                                    </div> 
                                  
                                    
                                 </div>
                            </div>
                          @endif
                          
                          <!-- ======================= START TIMELINE ====================== -->
                            
                    </div>
                    </div>
                    
                    <div class="col-lg-3 col-md-3 col-sm-12">
                    <div id="sidrRight" class="sec-side-right-wall fullwidth">
                      <div class="row">
                      <div id="fixRightSideBar" class="right-side-bar">
                          <div class="col-lg-12">
                            <div class="warp-btn-back-side-right hidden-lg hidden-md">
                              <a class="btn-back-side-right" href="#"><i class="fa fa-arrow-left"></i></a>
                            </div>
                            </div>
                          <!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="warp-cal-event-wall">
                              <div class="warp-btn-add-evt-cal">
                                  <a class="btn-add-evt-cal"><i class="fa fa-plus-circle"></i></a>
                                </div>
                                <div class="warp-evt-month-year">
                                  <div class="evt-month-year"><span class="month">APRIL</span><span class="year">2016</span></div>
                                    <div class="clearfix"></div>
                                    <div class="evt-date"><a class="btn-date-evt">24</a></div>
                                </div>
                                <div class="warp-evt-name-wall">
                                  <a class="btn-det-evt"><h3>Hell Fest 2016</h3></a>
                                </div>
                                <div class="warp-evt-place-time">
                                  <div class="place-evt">
                                      Parkir Hall Senayan
                                    </div>
                                    <div class="time-evt">
                                      <span class="start">09:00 AM</span>
                                        <span class="finish"> 23:00 PM</span>
                                    </div>
                                </div>
                            </div>
                            </div> -->
                        
                            <div class="warp-side-left-wall">
                                <div class="warp-title-side-left-wall">
                                    <h4>My Event</h4>
                                    <!-- <a href="#" class="btn-detail-sosmed pull-right"><i class="fa fa-bars"></i><span>detail</span></a> -->
                                </div>
                                <div class="warp-item-side-left" id="printloadingGroupEvent">
                                        <div class="col-xs-push-5 col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 lazyloader-list" id='loadingGroupEvent'>
                                          <img src="{{asset('/img/loader-more.gif')}}" class="img-full-responsive e-centering width-loader">
                                        </div>
                                        <input id="skipevent" type="hidden" value="0"></input>
                                        <input id="session" type="hidden" value="{{$id}}"></input>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sxs-12">
                                <div class="warp-ads-right">
                                    <div class="ads-box-right hidden-xs hidden-lg hidden-sm">
                                        <a href=""><img src="/img/ads/ads_200x200.jpg"></a>
                                    </div>
                                    <div class="side-ads-box e-centering hidden-md">
                                        <a href=""><img src="/img/ads/ads_300x250.jpg" class="e-centering"></a>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sxs-12">
                              <div class="warp-list-song-rec-wall">
                                  <div class="tittle-rec-wall">
                                      <h5> Recommended Today</h5>
                                    </div>
                                    <ul>
                                        <li>
                                          <div class="warp-btn-buy-rec-wall">
                                              <a href="" class="btn-buy-rec-wall"><i class="fa fa-plus-square-o"></i><span>Buy This Item</span></a>
                                            </div>
                                            <div class="item-rec-song">
                                            <div class="pl-img-popup">
                                                <img src="/img/album/album-06.jpg" class="img-responsive">
                                            </div>
                                            <div class="rec-tittle-name-popup">
                                                <div class="rec-tittle-popup">Nama Band Max 15 Ch</div>
                                                <div class="rec-name-popup">Judul Lagu Max 15 Ch</div>
                                            </div>
                                            <div class="rec-stat-popup">
                                                <div class="rec-stat-like-popup">
                                                    <a><i class="fa fa-heart like-single-list-song"></i></a>
                                                    <span>120</span>
                                                </div>
                                                <div class="rec-stat-forward-popup">
                                                    <a><i class="fa fa-mail-forward forward-single-list-song"></i></a>
                                                    <span>56</span>
                                                </div>
                                                <div class="rec-stat-forward-popup">
                                                    <a><i class="fa fa-plus-square plus-single-list-song"></i></a>
                                                    <span>120K</span>
                                                </div>
                                            </div>
                                            </div>
                                        </li>
                                        <li>
                                          <div class="warp-btn-buy-rec-wall">
                                              <a href="" class="btn-buy-rec-wall"><i class="fa fa-plus-square-o"></i><span>Buy This Item</span></a>
                                            </div>
                                            <div class="item-rec-song">
                                            <div class="pl-img-popup">
                                                <img src="/img/album/album-05.jpg" class="img-responsive">
                                            </div>
                                            <div class="rec-tittle-name-popup">
                                                <div class="rec-tittle-popup">Nama Band Max 15 Ch</div>
                                                <div class="rec-name-popup">Judul Lagu Max 15 Ch</div>
                                            </div>

                                            <div class="rec-stat-popup">
                                                <div class="rec-stat-like-popup">
                                                    <a><i class="fa fa-heart like-single-list-song"></i></a>
                                                    <span>120</span>
                                                </div>
                                                <div class="rec-stat-forward-popup">
                                                    <a><i class="fa fa-mail-forward forward-single-list-song"></i></a>
                                                    <span>56</span>
                                                </div>
                                                <div class="rec-stat-forward-popup">
                                                    <a><i class="fa fa-plus-square plus-single-list-song"></i></a>
                                                    <span>120K</span>
                                                </div>
                                            </div>
                                            </div>
                                        </li>
                                        <li>
                                          <div class="warp-btn-buy-rec-wall">
                                              <a href="" class="btn-buy-rec-wall"><i class="fa fa-plus-square-o"></i><span>Buy This Item</span></a>
                                            </div>
                                            <div class="item-rec-song">
                                            <div class="pl-img-popup">
                                                <img src="/img/album/album-04.jpg" class="img-responsive">
                                            </div>
                                            <div class="rec-tittle-name-popup">
                                                <div class="rec-tittle-popup">Nama Band Max 15 Ch</div>
                                                <div class="rec-name-popup">Judul Lagu Max 15 Ch</div>
                                            </div>
                                            <div class="rec-stat-popup">
                                                <div class="rec-stat-like-popup">
                                                    <a><i class="fa fa-heart like-single-list-song"></i></a>
                                                    <span>120</span>
                                                </div>
                                                <div class="rec-stat-forward-popup">
                                                    <a><i class="fa fa-mail-forward forward-single-list-song"></i></a>
                                                    <span>56</span>
                                                </div>
                                                <div class="rec-stat-forward-popup">
                                                    <a><i class="fa fa-plus-square plus-single-list-song"></i></a>
                                                    <span>120K</span>
                                                </div>
                                            </div>
                                            </div>
                                        </li>
                                        <li>
                                          <div class="warp-btn-buy-rec-wall">
                                              <a href="" class="btn-buy-rec-wall"><i class="fa fa-plus-square-o"></i><span>Buy This Item</span></a>
                                            </div>
                                            <div class="item-rec-song">
                                            <div class="pl-img-popup">
                                                <img src="/img/album/album-03.jpg" class="img-responsive">
                                            </div>
                                            <div class="rec-tittle-name-popup">
                                                <div class="rec-tittle-popup">Nama Band Max 15 Ch</div>
                                                <div class="rec-name-popup">Judul Lagu Max 15 Ch</div>
                                            </div>
                                            <div class="rec-stat-popup">
                                                <div class="rec-stat-like-popup">
                                                    <a><i class="fa fa-heart like-single-list-song"></i></a>
                                                    <span>120</span>
                                                </div>
                                                <div class="rec-stat-forward-popup">
                                                    <a><i class="fa fa-mail-forward forward-single-list-song"></i></a>
                                                    <span>56</span>
                                                </div>
                                                <div class="rec-stat-forward-popup">
                                                    <a><i class="fa fa-plus-square plus-single-list-song"></i></a>
                                                    <span>120K</span>
                                                </div>
                                            </div>
                                            </div>
                                        </li>
                                        <li>
                                          <div class="warp-btn-buy-rec-wall">
                                              <a href="" class="btn-buy-rec-wall"><i class="fa fa-plus-square-o"></i><span>Buy This Item</span></a>
                                            </div>
                                            <div class="item-rec-song">
                                            <div class="pl-img-popup">
                                                <img src="/img/album/album-02.jpg" class="img-responsive">
                                            </div>
                                            <div class="rec-tittle-name-popup">
                                                <div class="rec-tittle-popup">Nama Band Max 15 Ch</div>
                                                <div class="rec-name-popup">Judul Lagu Max 15 Ch</div>
                                            </div>
                                            <div class="rec-stat-popup">
                                                <div class="rec-stat-like-popup">
                                                    <a><i class="fa fa-heart like-single-list-song"></i></a>
                                                    <span>120</span>
                                                </div>
                                                <div class="rec-stat-forward-popup">
                                                    <a><i class="fa fa-mail-forward forward-single-list-song"></i></a>
                                                    <span>56</span>
                                                </div>
                                                <div class="rec-stat-forward-popup">
                                                    <a><i class="fa fa-plus-square plus-single-list-song"></i></a>
                                                    <span>120K</span>
                                                </div>
                                            </div>
                                            </div>
                                        </li>
                                        <li>
                                          <div class="warp-btn-buy-rec-wall">
                                              <a href="" class="btn-buy-rec-wall"><i class="fa fa-plus-square-o"></i><span>Buy This Item</span></a>
                                            </div>
                                            <div class="item-rec-song">
                                            <div class="pl-img-popup">
                                                <img src="/img/album/album-01.jpg" class="img-responsive">
                                            </div>
                                            <div class="rec-tittle-name-popup">
                                                <div class="rec-tittle-popup">Nama Band Max 15 Ch</div>
                                                <div class="rec-name-popup">Judul Lagu Max 15 Ch</div>
                                            </div>
                                            <div class="rec-stat-popup">
                                                <div class="rec-stat-like-popup">
                                                    <a><i class="fa fa-heart like-single-list-song"></i></a>
                                                    <span>120</span>
                                                </div>
                                                <div class="rec-stat-forward-popup">
                                                    <a><i class="fa fa-mail-forward forward-single-list-song"></i></a>
                                                    <span>56</span>
                                                </div>
                                                <div class="rec-stat-forward-popup">
                                                    <a><i class="fa fa-plus-square plus-single-list-song"></i></a>
                                                    <span>120K</span>
                                                </div>
                                            </div>
                                            </div>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </div> -->
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sxs-12">
                                <div class="warp-ads-right">
                                    <div class="ads-box-right hidden-xs hidden-lg hidden-sm">
                                        <a href=""><img src="/img/ads/ads_200x200.jpg"></a>
                                    </div>
                                    <div class="side-ads-box e-centering hidden-md">
                                        <a href=""><img src="/img/ads/ads_300x250.jpg" class="e-centering"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div>
                    </div>
                    </div>
                    
                </div><!-- ================ WARP 3 BAGIAN ================ -->
                
            </div>
    </div>
</div>

<div class="block-white"></div>
<!-- ---------- END MARKET MUSIC ------------------ -->

<!-- ============================= MODAL POST MUSIC IN TIMELINE ============================================= --> 
  <div id="postMusicBank" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" class="vh-center modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          
          <div class="modal-body nopadding">
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sxs-12">
              <div class="warp-crt-add-playlist"> 
                  <div class="modal-header color-gj-popup-report">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Post Music</h4>
                    </div>
                    <div class="warp-15">
                      <form class="form-horizontal">
                          <div class="form-group" id="toggleshow">
                              <label class="col-lg-12">Select Your Music</label>
                              <div class="col-lg-12">
                                  <div class="warp-list-song-post">
                                    <ul style="min-height: 230px; max-height: 420px; overflow-x: hidden; overflow-y: auto; padding: 0;">
                                    @foreach ($data2 as $track)
                                    <?php if ($track['hashPicture']==null) {
                                      $pic = 1;
                                    }else $pic =  $track['hashPicture']; ?>
                                      <li onclick="selectsong('<?php echo $pic; ?>', '<?php echo $track['name']; ?>', '<?php echo $track['username']; ?>', '<?php echo $track['id']; ?>')" class="warp-post-song">
                                          <a href="#" class="btn-select-post-song hover-select"></a>
                                            <div class="sm-list-song">
                                            <div class="gj-song-wrap">
                                                <div class="gj-song-card colab">
                                                    <div class="gj-song-cover">
                                                        <img src=<?php
                                                                  if(!empty($track['hashPicture'])){
                                                                      $url = asset($track['hashPicture']);
                                                                      echo "\"" . $url . "\"";
                                                                  }
                                                                  else{
                                                                      $url = asset('/img/album/album-song_475x475.jpg');
                                                                      echo $url;
                                                                  }
                                                                  ?> class="img-responsive">
                                                    </div>
                                                    <div class="gj-song-desc">
                                                        <div class="gj-song-story">
                                                            <h1 class="gj-song-title"><a href=""><?php echo $track['name']; ?></a></h1>
                                                            <h5 class="gj-song-username"><i class="fa fa-user"><?php echo "  ". $track['username']; ?></i></h5>
                                                            <!-- <p class="gj-song-contributor">Contributor1, contributor 2, contributor3</p> -->
                                                        </div>
                                                        
                                                        <div class="warp-stat-card">
                                                            <div class="stat-song-track">
                                                                <div class="warp-ico-stat">
                                                                    <div class="ico-stat"><a class="disable" href="javascript:void(0)" onclick="clickLike();"><i class="fa fa-heart like-single-list-song"></i></a></div>
                                                                    <div class="num-stat"><?php echo $track['liked']; ?></div>
                                                                </div>
                                                            </div>
                                                            <div class="stat-song-track">
                                                                <div class="warp-ico-stat">
                                                                    <div class="ico-stat"><a class="btn-share-ts disable"><i class="fa fa-mail-forward forward-single-list-song"></i></a></div>
                                                                    <div class="num-stat"><?php echo $track['shared']; ?></div>
                                                                </div>
                                                            </div>
                                                            <div class="stat-song-track">
                                                                <div class="warp-ico-stat">
                                                                    <div class="ico-stat"><a class="disable" href=""><i class="fa fa-magic spice-single-list-song"></i></a></div>
                                                                    <div class="num-stat"><?php echo $track['totalSpice']; ?></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                <hr>
                                            </div>
                                            </div>
                                        </li>
                                      @endforeach        
                                    </ul>
                                </div>
                                </div>
                            </div>
                            <!-- ================================ WARP Selected Song Put Here ========================================== -->
                              <div class="warp-selected-song-post">
                                <div href="#" class="btn-select-post-song selected"></div>
                                      <!-- ============================ CONTENT Selected Song Put Here ========================================== -->
                                   
                                            <!-- ======================== ENDCONTENT Selected Song Put Here ========================================== -->
                              </div>
                            <!-- ============================ End WARP Selected Song Put Here ========================================== -->
                            <div class="l-hr"></div>
                            
                       </form>
                    </div>
                </div>
          </div>
          </div>
          </div>
        </div>
      </div>
  </div>
<!-- ============================= END MODAL POST MUSIC IN TIMELINE  =========================================== -->

@stop
