<div class="col-lg-3 col-md-3 col-sm-12">
  <div id="sidrLeft" class="sec-side-left-wall fullwidth">
    <div class="row">
      <div id="fixLeftSideBar" class="left-side-wall">
        <div class="col-lg-12">
          <div class="row">
            <div class="warp-btn-back-side-left hidden-lg hidden-md">
              <a class="btn-back-side-left" href="#"><i class="fa fa-arrow-left"></i></a>
            </div>
            <div class="warp-side-l-menu">
              <ul>
                <li><a class='group-menu-sideBarLeft' data-pjax='yes' id='pjax' href="{{ url('group/latestpost', $parameters = [$groupID], $secure = null); }}">Latest Post</a></li>   
                <li><a class='group-menu-sideBarLeft' data-pjax='yes' id='pjax' href="{{ url('group/event', $parameters = [$groupID], $secure = null); }}">Group Event</a></li>
                <li><a class='group-menu-sideBarLeft' id='pjax' data-pjax='yes' href="{{ url('group/member', $parameters = [$groupID], $secure = null); }}">Group Member</a></li>
                <li><a class='group-menu-sideBarLeft' href="#">About Group</a></li>
                <li><a class='group-menu-sideBarLeft' href="#">Group Stuff</a></li>
                @if($group[0]['checkAdmin'] == 'admin')
                <li><a class='group-menu-sideBarLeft' id='pjax' data-pjax='yes' href="{{ url('group/setting', $parameters = [$groupID], $secure = null); }}">Setting</a></li>
                @endif
                <!-- <li><a href="#">Top Engage</a></li>
                  <li><a href="#">Hot Topics</a></li> -->
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>