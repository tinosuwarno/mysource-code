@extends('layouts.default-base')
{{-- overwrite title --}}
@section('title')
Gritjam - GroupMember
@overwrite
@section('styles')
@stop
{{-- content --}}
@section('contents')
<?php
  $session = Session::get('data');
  $id = $session['id'];
  ?>
<!-- ---------- SECTION MARKET MUSIC ----------------- --- -->
<div class="block-white"></div>
<div class="block-white visible-sm hidden-lg hidden-xs hidden-md"></div>
<div class="block-white visible-sm hidden-lg hidden-xs hidden-md"></div>
@include('layouts.bg-group-setting')
<div class="container-fluid">
  <div class="row">
    <div class="md-w-warp">
      <div class="col-lg-12">
        <div class="menu text-center menu-wall motion-animate">
          <div class="hr"></div>
          <div class="warp-btn-song-noaffix hidden-lg hidden-md">
            <a id="btnEvent-affixLeft" class="btn-song-affix" href="#sidrLeft"><i class="fa fa-bars"></i></a>
          </div>
          <ul class="action-menu-profile">
            <li>
              <a id='pjax' data-pjax='yes' href='{{url("about-me")}}'>About</a>
            </li>
            <li>
              <a id='pjax' data-pjax='yes' href='{{url("myWall")}}'>My wall</a>
            </li>
            <li>
              <a data-pjax='yes' id='pjax' href="{{ url('profile/following', $parameters = [$id], $secure = null); }}">Timeline</a>
            </li>
            <li class="active">
              <a data-pjax='yes' id='pjax' href="{{ url('group') }}">Groups</a>
            </li>
            <li>
              <a data-pjax='yes' id='pjax' href="{{ url('myProject') }}">Project</a>
            </li>
          </ul>
          <div class="warp-btn-event-noaffix hidden-lg hidden-md">
            <a id="btnEvent-affix2" class="btn-event-affix" href="#sidr"><i class="fa fa-calendar"></i></a>
          </div>
          <div class="hr"></div>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="warp-all-wall">
        @include('group.group-menu')
        <div class="col-lg-6 col-md-6 col-sm-12 padten-sxs">
          <div class="warp-over-timeline">
            <div class="timeline-box">
              <div id="memberGroup" class="warp-tittle-group">
                <h4>Setting Group</h4>
              </div>
              <form  name="formSettingGroup" method="post" action='/groupSettingProses' enctype="multipart/form-data">
                <div class="warp-edit-form-up">
                  <div class="single-form-up">
                    <div class="row">
                      <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12"><label class="label-edit-form">Group Name</label></div>
                      <div class="col-lg-6 col-md-7 col-sm-8"><input class="form-control input-edit-form-up" type="text"  maxlength="50" name="groupname" id="groupname" value="{{$group[0]['group_name']}}" required></div>
                    </div>
                  </div>
                  <div class="single-form-up">
                    <div class="row">
                      <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12"><label class="label-edit-form">Description</label></div>
                      <div class="col-lg-6 col-md-7 col-sm-8"><textarea class="form-control textarea-edit-form-up" maxlength="5000" rows="6" name="Description" id="Description">{{$group[0]['description']}}</textarea></div>
                    </div>
                  </div>
                  <div class="single-form-up">
                    <input type="hidden" name="groupID" value="{{$group[0]['groupID']}}">
                    <input type="hidden" name="picture" value="{{$group[0]['picture']}}">
                    <input type="hidden" name="bg_picture" value="{{$group[0]['bg_picture']}}">
                    <input type="hidden" name="old_description" value="{{$group[0]['description']}}">
                    <input type="hidden" name='old_admin' value="{{$group[0]['admin']}}" >
                    <div class="single-form-up">
                      <div class="row">
                        <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12"><label class="label-edit-form">Insert Group Avatar</label></div>
                        <div class="col-lg-6 col-md-7 col-sm-8">
                          @if(empty($group[0]['picture'] ) )
                          <img src='' class='img-responsive img-edit-up img-border-gj img-profile-user' id='preview1'>
                          @else
                          <img src='{{asset($group[0]['picture'])}}' class='img-responsive img-edit-up img-border-gj img-profile-user' id='preview1'>
                          @endif
                          <input type="file" name="file-input1" id="file-input1" class="inputfile" onchange="document.getElementById('preview1').src = window.URL.createObjectURL(this.files[0])" data-multiple-caption="{count} files selected" multiple accept="image/*">
                          <label for="file-input1"> <span>Choose a picture</span></label>
                        </div>
                      </div>
                    </div>
                    <div class="single-form-up">
                      <div class="row">
                        <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12"><label class="label-edit-form">Insert Group Cover</label></div>
                        <div class="col-lg-6 col-md-7 col-sm-8">
                          @if(empty($group[0]['bg_picture'] ) )
                          <img src='' class="mg-responsive img-edit-up img-border-gj" id="previewBgGroupSetting" width="317px" height="218px">
                          @else
                          <img src="{{asset($group[0]['bg_picture'])}}" class="img-responsive img-edit-up img-border-gj" id='previewBgGroupSetting'>
                          @endif  
                          <input type="file" name="file-input2" id="file-input2" class="inputfile" onchange="document.getElementById('previewBgGroupSetting').src = window.URL.createObjectURL(this.files[0])" data-multiple-caption="{count} files selected" multiple accept="image/*">
                          <label for="file-input2"> <span>Choose a picture</span></label>
                        </div>
                      </div>
                    </div>
                    <div class="single-form-up">
                      <div class="row">
                        <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12"><label class="label-edit-form">Select New Admin</label></div>
                        <div class="col-lg-6 col-md-7 col-sm-8">
                        @if($group['0']['checkingEventAktiveAndEventWinner'] == 0 ||  $group[0]['checkingEventAktiveAndEventWinner'] == '0')
                          <select data-placeholder="Select New Admin..." class="chosen-select" tabindex="4" style="display:block;" name="member_id">
                            @if(!empty($member))
                            <option value="">-- select new admin --</option>
                            @foreach ($member as $key => $value)
                            <option value="{{$value['memberID']}}" id="{{$value['memberID']}}" name="{{$value['memberID']}}">{{$value['username']}}
                            </option>
                            @endforeach
                            @else
                            <option value=""></option>
                            <option> Sorry, not Found other member in Group </option>
                            @endif 
                            <option value=""></option>
                          </select>
                        @else
                          <p>Sorry, You can not select a new admin, there running event or event that no winner
                          </p>
                        @endif
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-3 col-lg-1 col-md-2 col-sm-2 col-xs-6">
                        <input type="submit" class="btn btn-sign-in btn-gj" value="Save">
                      </div>
                      <!--  <div class="col-lg-1 col-md-2 col-sm-2 col-xs-6">
                        <input type="submit" class="btn btn-cancel btn-gj" value="Cancel">
                        </div> -->
                    </div>
                  </div>
              </form>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12">
                    <div id="sidrRight" class="sec-side-right-wall fullwidth">
                      <div class="row">
                      <div id="fixRightSideBar" class="right-side-bar">
                          <div class="col-lg-12">
                            <div class="warp-btn-back-side-right hidden-lg hidden-md">
                              <a class="btn-back-side-right" href="#"><i class="fa fa-arrow-left"></i></a>
                            </div>
                            </div>
                          <!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="warp-cal-event-wall">
                              <div class="warp-btn-add-evt-cal">
                                  <a class="btn-add-evt-cal"><i class="fa fa-plus-circle"></i></a>
                                </div>
                                <div class="warp-evt-month-year">
                                  <div class="evt-month-year"><span class="month">APRIL</span><span class="year">2016</span></div>
                                    <div class="clearfix"></div>
                                    <div class="evt-date"><a class="btn-date-evt">24</a></div>
                                </div>
                                <div class="warp-evt-name-wall">
                                  <a class="btn-det-evt"><h3>Hell Fest 2016</h3></a>
                                </div>
                                <div class="warp-evt-place-time">
                                  <div class="place-evt">
                                      Parkir Hall Senayan
                                    </div>
                                    <div class="time-evt">
                                      <span class="start">09:00 AM</span>
                                        <span class="finish"> 23:00 PM</span>
                                    </div>
                                </div>
                            </div>
                            </div> -->
                        
                            <div class="warp-side-left-wall">
                                <div class="warp-title-side-left-wall">
                                    <h4>My Event</h4>
                                    <!-- <a href="#" class="btn-detail-sosmed pull-right"><i class="fa fa-bars"></i><span>detail</span></a> -->
                                </div>
                                <div class="warp-item-side-left" id="printloadingGroupEvent">
                                        <div class="col-xs-push-5 col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 lazyloader-list" id='loadingGroupEvent'>
                                          <img src="{{asset('/img/loader-more.gif')}}" class="img-full-responsive e-centering width-loader">
                                        </div>
                                        <input id="skipevent" type="hidden" value="0"></input>
                                        <input id="session" type="hidden" value="{{$id}}"></input>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sxs-12">
                                <div class="warp-ads-right">
                                    <div class="ads-box-right hidden-xs hidden-lg hidden-sm">
                                        <a href=""><img src="/img/ads/ads_200x200.jpg"></a>
                                    </div>
                                    <div class="side-ads-box e-centering hidden-md">
                                        <a href=""><img src="/img/ads/ads_300x250.jpg" class="e-centering"></a>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sxs-12">
                              <div class="warp-list-song-rec-wall">
                                  <div class="tittle-rec-wall">
                                      <h5> Recommended Today</h5>
                                    </div>
                                    <ul>
                                        <li>
                                          <div class="warp-btn-buy-rec-wall">
                                              <a href="" class="btn-buy-rec-wall"><i class="fa fa-plus-square-o"></i><span>Buy This Item</span></a>
                                            </div>
                                            <div class="item-rec-song">
                                            <div class="pl-img-popup">
                                                <img src="/img/album/album-06.jpg" class="img-responsive">
                                            </div>
                                            <div class="rec-tittle-name-popup">
                                                <div class="rec-tittle-popup">Nama Band Max 15 Ch</div>
                                                <div class="rec-name-popup">Judul Lagu Max 15 Ch</div>
                                            </div>
                                            <div class="rec-stat-popup">
                                                <div class="rec-stat-like-popup">
                                                    <a><i class="fa fa-heart like-single-list-song"></i></a>
                                                    <span>120</span>
                                                </div>
                                                <div class="rec-stat-forward-popup">
                                                    <a><i class="fa fa-mail-forward forward-single-list-song"></i></a>
                                                    <span>56</span>
                                                </div>
                                                <div class="rec-stat-forward-popup">
                                                    <a><i class="fa fa-plus-square plus-single-list-song"></i></a>
                                                    <span>120K</span>
                                                </div>
                                            </div>
                                            </div>
                                        </li>
                                        <li>
                                          <div class="warp-btn-buy-rec-wall">
                                              <a href="" class="btn-buy-rec-wall"><i class="fa fa-plus-square-o"></i><span>Buy This Item</span></a>
                                            </div>
                                            <div class="item-rec-song">
                                            <div class="pl-img-popup">
                                                <img src="/img/album/album-05.jpg" class="img-responsive">
                                            </div>
                                            <div class="rec-tittle-name-popup">
                                                <div class="rec-tittle-popup">Nama Band Max 15 Ch</div>
                                                <div class="rec-name-popup">Judul Lagu Max 15 Ch</div>
                                            </div>

                                            <div class="rec-stat-popup">
                                                <div class="rec-stat-like-popup">
                                                    <a><i class="fa fa-heart like-single-list-song"></i></a>
                                                    <span>120</span>
                                                </div>
                                                <div class="rec-stat-forward-popup">
                                                    <a><i class="fa fa-mail-forward forward-single-list-song"></i></a>
                                                    <span>56</span>
                                                </div>
                                                <div class="rec-stat-forward-popup">
                                                    <a><i class="fa fa-plus-square plus-single-list-song"></i></a>
                                                    <span>120K</span>
                                                </div>
                                            </div>
                                            </div>
                                        </li>
                                        <li>
                                          <div class="warp-btn-buy-rec-wall">
                                              <a href="" class="btn-buy-rec-wall"><i class="fa fa-plus-square-o"></i><span>Buy This Item</span></a>
                                            </div>
                                            <div class="item-rec-song">
                                            <div class="pl-img-popup">
                                                <img src="/img/album/album-04.jpg" class="img-responsive">
                                            </div>
                                            <div class="rec-tittle-name-popup">
                                                <div class="rec-tittle-popup">Nama Band Max 15 Ch</div>
                                                <div class="rec-name-popup">Judul Lagu Max 15 Ch</div>
                                            </div>
                                            <div class="rec-stat-popup">
                                                <div class="rec-stat-like-popup">
                                                    <a><i class="fa fa-heart like-single-list-song"></i></a>
                                                    <span>120</span>
                                                </div>
                                                <div class="rec-stat-forward-popup">
                                                    <a><i class="fa fa-mail-forward forward-single-list-song"></i></a>
                                                    <span>56</span>
                                                </div>
                                                <div class="rec-stat-forward-popup">
                                                    <a><i class="fa fa-plus-square plus-single-list-song"></i></a>
                                                    <span>120K</span>
                                                </div>
                                            </div>
                                            </div>
                                        </li>
                                        <li>
                                          <div class="warp-btn-buy-rec-wall">
                                              <a href="" class="btn-buy-rec-wall"><i class="fa fa-plus-square-o"></i><span>Buy This Item</span></a>
                                            </div>
                                            <div class="item-rec-song">
                                            <div class="pl-img-popup">
                                                <img src="/img/album/album-03.jpg" class="img-responsive">
                                            </div>
                                            <div class="rec-tittle-name-popup">
                                                <div class="rec-tittle-popup">Nama Band Max 15 Ch</div>
                                                <div class="rec-name-popup">Judul Lagu Max 15 Ch</div>
                                            </div>
                                            <div class="rec-stat-popup">
                                                <div class="rec-stat-like-popup">
                                                    <a><i class="fa fa-heart like-single-list-song"></i></a>
                                                    <span>120</span>
                                                </div>
                                                <div class="rec-stat-forward-popup">
                                                    <a><i class="fa fa-mail-forward forward-single-list-song"></i></a>
                                                    <span>56</span>
                                                </div>
                                                <div class="rec-stat-forward-popup">
                                                    <a><i class="fa fa-plus-square plus-single-list-song"></i></a>
                                                    <span>120K</span>
                                                </div>
                                            </div>
                                            </div>
                                        </li>
                                        <li>
                                          <div class="warp-btn-buy-rec-wall">
                                              <a href="" class="btn-buy-rec-wall"><i class="fa fa-plus-square-o"></i><span>Buy This Item</span></a>
                                            </div>
                                            <div class="item-rec-song">
                                            <div class="pl-img-popup">
                                                <img src="/img/album/album-02.jpg" class="img-responsive">
                                            </div>
                                            <div class="rec-tittle-name-popup">
                                                <div class="rec-tittle-popup">Nama Band Max 15 Ch</div>
                                                <div class="rec-name-popup">Judul Lagu Max 15 Ch</div>
                                            </div>
                                            <div class="rec-stat-popup">
                                                <div class="rec-stat-like-popup">
                                                    <a><i class="fa fa-heart like-single-list-song"></i></a>
                                                    <span>120</span>
                                                </div>
                                                <div class="rec-stat-forward-popup">
                                                    <a><i class="fa fa-mail-forward forward-single-list-song"></i></a>
                                                    <span>56</span>
                                                </div>
                                                <div class="rec-stat-forward-popup">
                                                    <a><i class="fa fa-plus-square plus-single-list-song"></i></a>
                                                    <span>120K</span>
                                                </div>
                                            </div>
                                            </div>
                                        </li>
                                        <li>
                                          <div class="warp-btn-buy-rec-wall">
                                              <a href="" class="btn-buy-rec-wall"><i class="fa fa-plus-square-o"></i><span>Buy This Item</span></a>
                                            </div>
                                            <div class="item-rec-song">
                                            <div class="pl-img-popup">
                                                <img src="/img/album/album-01.jpg" class="img-responsive">
                                            </div>
                                            <div class="rec-tittle-name-popup">
                                                <div class="rec-tittle-popup">Nama Band Max 15 Ch</div>
                                                <div class="rec-name-popup">Judul Lagu Max 15 Ch</div>
                                            </div>
                                            <div class="rec-stat-popup">
                                                <div class="rec-stat-like-popup">
                                                    <a><i class="fa fa-heart like-single-list-song"></i></a>
                                                    <span>120</span>
                                                </div>
                                                <div class="rec-stat-forward-popup">
                                                    <a><i class="fa fa-mail-forward forward-single-list-song"></i></a>
                                                    <span>56</span>
                                                </div>
                                                <div class="rec-stat-forward-popup">
                                                    <a><i class="fa fa-plus-square plus-single-list-song"></i></a>
                                                    <span>120K</span>
                                                </div>
                                            </div>
                                            </div>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </div> -->
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sxs-12">
                                <div class="warp-ads-right">
                                    <div class="ads-box-right hidden-xs hidden-lg hidden-sm">
                                        <a href=""><img src="/img/ads/ads_200x200.jpg"></a>
                                    </div>
                                    <div class="side-ads-box e-centering hidden-md">
                                        <a href=""><img src="/img/ads/ads_300x250.jpg" class="e-centering"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div>
                    </div>
                    </div>
                    
                </div><!-- ================ WARP 3 BAGIAN ================ -->
    </div>
  </div>
</div>
<div class="block-white"></div>
<!-- ---------- END MARKET MUSIC ------------------ -->
@stop