@extends('layouts.default-base')
{{-- overwrite title --}}
@section('title')
Gritjam - Group Single Post
@overwrite
@section('styles')
@stop
{{-- content --}}

@section('contents')
<?php
  $session = Session::get('data');
  $id = $session['id'];
  ?>
<!-- ---------- SECTION MARKET MUSIC ----------------- --- -->
<div class="block-white"></div>
<div class="block-white visible-sm hidden-lg hidden-xs hidden-md"></div>
<div class="block-white visible-sm hidden-lg hidden-xs hidden-md"></div>
<span id="username-single" style="display: none;">{{$usernamePost}}</span>
@include('layouts.bg-group-setting')
<div class="container-fluid">
  <div class="row">
    <div class="md-w-warp">
      <div class="col-lg-12">
        <div class="menu text-center menu-wall motion-animate">
          <div class="hr"></div>
          <div class="warp-btn-song-noaffix hidden-lg hidden-md">
            <a id="btnEvent-affixLeft" class="btn-song-affix" href="#sidrLeft"><i class="fa fa-bars"></i></a>
          </div>
          <ul class="action-menu-profile">
            <li>
              <a id='pjax' data-pjax='yes' href='{{url("about-me")}}'>About</a>
            </li>
            <li>
              <a id='pjax' data-pjax='yes' href='{{url("myWall")}}'>My wall</a>
            </li>
            <li>
              <a data-pjax='yes' id='pjax' href="{{ url('profile/following', $parameters = [$id], $secure = null); }}">Timeline</a>
            </li>
            <li class="active">
              <a data-pjax='yes' id='pjax' href="{{ url('group') }}">Groups</a>
            </li>
            <li>
              <a data-pjax='yes' id='pjax' href="{{ url('myProject') }}">Project</a>
            </li>
          </ul>
          <div class="warp-btn-event-noaffix hidden-lg hidden-md">
            <a id="btnEvent-affix2" class="btn-event-affix" href="#sidr"><i class="fa fa-calendar"></i></a>
          </div>
          <div class="hr"></div>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="warp-all-wall">
      @include('group.group-menu')
        <div class="col-lg-6 col-md-6 col-sm-12 padten-sxs">
          <div class="warp-over-timeline">
          <!-- single post test -->
          <div class='col-xs-push-5 col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 lazyloader-list' id='loadingSinglePostGroup'>
                <img src="{{asset('img/loader-more.gif')}}" class='img-full-responsive e-centering width-loader'>
            </div>
          

        </div>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-12">
        <div id="sidrRight" class="sec-side-right-wall fullwidth">
          <div class="row">
            <div id="fixRightSideBar" class="right-side-bar">
              <div class="col-lg-12">
                <div class="warp-btn-back-side-right hidden-lg hidden-md">
                  <a class="btn-back-side-right" href="#"><i class="fa fa-arrow-left"></i></a>
                </div>
              </div>
        
               <div class="warp-side-left-wall">
                    <div class="warp-title-side-left-wall">
                        <h4>My Event</h4>
                        <a href="#" class="btn-detail-sosmed pull-right"><i class="fa fa-bars"></i><span>detail</span></a>
                    </div>
                    <div class="warp-item-side-left" id="printloadingSingleEvent">
                        <div class="col-xs-push-5 col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 lazyloader-list" id='loadingSingleEvent'>
                          <img src="{{asset('/img/loader-more.gif')}}" class="img-full-responsive e-centering width-loader">
                        </div>
                        <input id="skipevent" type="hidden" value="0">
                    </div>
                </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sxs-12">
                <div class="warp-ads-right">
                  <div class="ads-box-right hidden-xs hidden-lg hidden-sm">
                    <a href=""><img src="{{asset('img/ads/ads_200x200.jpg')}}"></a>
                  </div>
                  <div class="side-ads-box e-centering hidden-md">
                    <a href=""><img src="{{asset('img/ads/ads_300x250.jpg')}}" class="e-centering"></a>
                  </div>
                </div>
              </div>
             
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sxs-12">
                <div class="warp-ads-right">
                  <div class="ads-box-right hidden-xs hidden-lg hidden-sm">
                    <a href=""><img src="{{asset('img/ads/ads_200x200.jpg')}}"></a>
                  </div>
                  <div class="side-ads-box e-centering hidden-md">
                    <a href=""><img src="{{asset('img/ads/ads_300x250.jpg')}}" class="e-centering"></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- ================ WARP 3 BAGIAN ================ -->
  </div>
</div>
</div>
<div class="block-white"></div>
<!-- ---------- END MARKET MUSIC ------------------ -->

@stop