<link href="{{asset('js/jwplayer/skins/gjaudioplayer.css')}}" rel='stylesheet' type='text/css'>
<script src="{{asset('js/jwplayer/jwplayer7.js')}}"></script>
<!-- <script src="{{asset('js/jwplayer/jwplayer.js')}}"></script> -->
<script> jwplayer.key="1I4Td5GO5Vv3qqMfm954PkUhpgbS8bxwlmO20N0Pcx4="; </script>
<script>
    function playMusic(trackID){
        playTheMusic('/playlist/getPlayTrack', {'trackID': trackID});
    }

    function playMusicSample(trackID){
        playTheMusic('/playlist/getPlayTrackSample', {'trackID': trackID});
    }

    function playTheMusic(link, data){
        $('.warp-player-music.sticky-position-player').css('display', 'block');
        $.ajax({
            type: 'post',
            data: data,
            url: link,
            success: function(responseText){
                // console.log(responseText); return;
                playJWplayerSingle(responseText);
            },
            error : function(jqXHR, textStatus, errorThrown){
                console.log(jqXHR); return;
            }
        });
    }

    function playPlaylist(playlistID){
        // console.log(playlistID); return;
        $('.warp-player-music.sticky-position-player').css('display', 'block');
        $.ajax({
            type: 'post',
            data: {'playlistID': playlistID},
            url: '/playlist/getPlaylist',
            success: function(responseText){
                playJWplayerPlaylist(responseText);
            },
            error : function(jqXHR, textStatus, errorThrown){
                console.log(jqXHR); return;
            }
        });
    }

    function playPlaylistTrack(trackID, playlistID){
        $('.warp-player-music.sticky-position-player').css('display', 'block');
        var data = {'trackID': trackID, 'playlistID': playlistID};
        $.ajax({
            type: 'post',
            data: data,
            url: '/playlist/getPlaylist',
            success: function(responseText){
                playJWplayerPlaylist(responseText);
            },
            error : function(jqXHR, textStatus, errorThrown){
                console.log(jqXHR); return;
            }
        });
    }

    function playJWplayerPlaylist(data){
        // console.log(data); return;
        $('#playerJWplayer').css('opacity','1');
        var countShuffle = 0;
        var countRepeat = 0;
        var countPlaylist = 0;
        //setup jwplayer
        jwplayer("gjaudioplayer").setup({
            skin: {
                url: "{{asset('js/jwplayer/skins/gjaudioplayer.css')}}",
                name: "gjaudioplayer"
            },
            controls: true,
            width: "100%",
            height: 80,
            repeat: 'always',
            shuffle: 'false',
            primary: 'html5',
            hlshtml: true,

            plugins: {
                "{{asset('js/jwplayer/jwplayer.shuffle.js')}}": {
                    autostart: false,
                    repeatplaylist: false,
                    shuffle: false
                }
            },
            //flashplayer: "{{asset('js/jwplayer/jwplayer.flash.swf')}}",
            playlist: data,
            // playlist: "playlist.json",
        });

        jwplayer().addButton(
            "{{asset('js/jwplayer/list.png')}}",
            "",
            function() {
                if (countPlaylist % 2 != 0) {
                    $(".playlist-wrap .jw-menu.jw-playlist-container.jw-background-color.jw-reset").css("margin-right", "-250px");
                    $('[button="jw-playlist"] .jw-icon.jw-dock-image.jw-reset').css('background-image', 'url(/js/jwplayer/list.png)');
                } else {
                    $(".playlist-wrap .jw-menu.jw-playlist-container.jw-background-color.jw-reset").css("margin-right", "0");
                    $('[button="jw-playlist"] .jw-icon.jw-dock-image.jw-reset').css('background-image', 'url(/js/jwplayer/list-active.png)');
                }
                countPlaylist += 1;
            },
            "jw-playlist"
        );
        jwplayer().addButton(
            "{{asset('js/jwplayer/repeat.png')}}",
            "",
            function() {
                shuffle_setRepeatPlaylist();
                if (countRepeat % 2 != 0) {
                    $('[button="jwplayer-repeat"] .jw-icon.jw-dock-image.jw-reset').css('background-image', 'url(/js/jwplayer/repeat.png)');
                } else {

                    $('[button="jwplayer-repeat"] .jw-icon.jw-dock-image.jw-reset').css('background-image', 'url(/js/jwplayer/repeat-active.png)');
                }
                countRepeat += 1;
            },
            "jwplayer-repeat"
        );
        jwplayer().addButton(
            "{{asset('js/jwplayer/shuffle.png')}}",
            "",
            function() {
                shuffle_setShuffle();
                if (countShuffle % 2 != 0) {
                    $('[button="jwplayer-shuffle"] .jw-icon.jw-dock-image.jw-reset').css('background-image', 'url(/js/jwplayer/shuffle.png)');
                } else {

                    $('[button="jwplayer-shuffle"] .jw-icon.jw-dock-image.jw-reset').css('background-image', 'url(/js/jwplayer/shuffle-active.png)');
                }
                countShuffle += 1;
            },
            "jwplayer-shuffle"
        );

        jwplayer().on('ready', function() {
            $('.jw-icon.jw-icon-tooltip.jw-icon-playlist.jw-button-color.jw-reset').hide();
            $('.jw-icon.jw-icon-tooltip.jw-icon-volume.jw-button-color.jw-reset').hide();
            $('.jw-icon.jw-icon-inline.jw-button-color.jw-reset.jw-icon-playback').insertAfter('.jw-icon.jw-icon-inline.jw-button-color.jw-reset.jw-icon-prev');
            // move overlay outside the player bar
            $(".jw-overlay.jw-reset").insertAfter("#playlistOverlay");
            $(".jw-tooltip-title.jw-reset").append("<span><i class='fa fa-close fa-2x' style='float:right; margin:2px 10px;'></i></span>");
            
            $(".fa.fa-close.fa-2x").click(
                function() {
                    $(".playlist-wrap .jw-menu.jw-playlist-container.jw-background-color.jw-reset").css("margin-right", "-250px");
                }
            );

            // move time-tooltip and show relative to the cursor position

            $(".jw-slider-time.jw-background-color.jw-reset.jw-slider-horizontal.jw-reset").mouseover(
                function() {
                    $(".playlist-wrap .jw-time-tip.jw-background-color.jw-reset").css("opacity", "1");
                }
            );
            
            $(".jw-slider-time.jw-background-color.jw-reset.jw-slider-horizontal.jw-reset").mouseout(
                function() {
                    $(".playlist-wrap .jw-time-tip.jw-background-color.jw-reset").css("opacity", "0");
                }
            );

            $(".jw-slider-time.jw-background-color.jw-reset.jw-slider-horizontal.jw-reset").mousemove(
                function(event) {
                    var xPos = "" + event.clientX - 18 + "px";
                    $(".playlist-wrap .jw-time-tip.jw-background-color.jw-reset").css("left", xPos);
                }
            );
            
            $(".playlist-wrap .jw-menu.jw-playlist-container.jw-background-color.jw-reset").css("margin-right", "-250px");
            $(".playlist-wrap .jw-slider-volume.jw-volume-tip.jw-background-color.jw-reset.jw-slider-vertical.jw-reset").css("margin-bottom", "-170px");

            $("#gjaudioplayer").removeClass(".jw-flag-user-inactive");
            $("#gjaudioplayer.jw-flag-user-inactive").css("display", "block");
            // $(".jw-preview.jw-reset").one('click', function (event) {  
            //    event.preventDefault();
            // });
        });

        jwplayer().on('play', function() {
            $(".jw-icon.jw-icon-inline.jw-button-color.jw-reset.jw-icon-playback").addClass("pause").removeClass("play");
            $(".jwplayer.jw-reset.jw-skin-gjaudioplayer.jw-stretch-uniform.jw-state-playing").removeClass(".jw-flag-user-inactive");
        });

        jwplayer().on('pause', function() {
            $(".jw-icon.jw-icon-inline.jw-button-color.jw-reset.jw-icon-playback").addClass("play").removeClass("pause");
            $(".jwplayer.jw-reset.jw-skin-gjaudioplayer.jw-stretch-uniform.jw-state-playing").removeClass(".jw-flag-user-inactive");
        });

        jwplayer().on('idle', function() {
            $(".jwplayer.jw-reset.jw-skin-gjaudioplayer.jw-stretch-uniform.jw-state-playing").removeClass(".jw-flag-user-inactive");
        });

        jwplayer().on('buffer', function() {
            $(".jwplayer.jw-reset.jw-skin-gjaudioplayer.jw-stretch-uniform.jw-state-playing").removeClass(".jw-flag-user-inactive");
        });
    }

    function playJWplayerSingle(data){
        // console.log(data); return;
        $('#playerJWplayer').css('opacity','1');
        var countShuffle = 0;
        var countRepeat = 0;
        var countPlaylist = 0;
        //setup jwplayer
        jwplayer("gjaudioplayer").setup({
            skin: {
                url: "{{asset('js/jwplayer/skins/gjaudioplayer.css')}}",
                name: "gjaudioplayer"
            },
            controls: true,
            width: "100%",
            height: 80,
            repeat: false,
            shuffle: false,
            // primary: 'flash',
            primary: 'html5',
            hlshtml: true,

            autostart: true,
            plugins: {
                "{{asset('js/jwplayer/jwplayer.shuffle.js')}}": {
                    autostart: false,
                    repeatplaylist: false,
                    shuffle: false
                }
            },
            // flashplayer: "{{asset('js/jwplayer/jwplayer.flash.swf')}}",
            playlist: [data],
            // playlist: "playlist.json",
        });

        jwplayer().addButton(
            "{{asset('js/jwplayer/spice.png')}}",
            "",
            function() {
                $('#spiceModal').modal('show');
                $('[button="jwplayer-spice"] .jw-icon.jw-dock-image.jw-reset').hover(
                  function() {
                    $(this).css('background-image', 'url(/js/jwplayer/spice-active.png)');
                  }, 
                  function() {
                    $(this).css('background-image', 'url(/js/jwplayer/spice.png)');
                  }
                );
            },
            "jwplayer-spice"
        );
        jwplayer().addButton(
            "{{asset('js/jwplayer/forward.png')}}",
            "",
            function() {
                $('#forwardModal').modal('show');
                $('[button="jwplayer-forward"] .jw-icon.jw-dock-image.jw-reset').hover(
                  function() {
                    $(this).css('background-image', 'url(/js/jwplayer/forward-active.png)');
                  }, 
                  function() {
                    $(this).css('background-image', 'url(/js/jwplayer/forward.png)');
                  }
                );

            },
            "jwplayer-forward"
        );
        jwplayer().addButton(
            "{{asset('js/jwplayer/heart.png')}}",
            "",
            function() {
                $('#likeModal').modal('show');
                $('[button="jwplayer-heart"] .jw-icon.jw-dock-image.jw-reset').hover(
                  function() {
                    $(this).css('background-image', 'url(/js/jwplayer/heart-active.png)');
                  }, 
                  function() {
                    $(this).css('background-image', 'url(/js/jwplayer/heart.png)');
                  }
                );
            },
            "jwplayer-heart"
        );

        jwplayer().on('ready', function() {

            $('.jw-icon.jw-icon-tooltip.jw-icon-playlist.jw-button-color.jw-reset').hide();
            $('.jw-icon.jw-icon-tooltip.jw-icon-volume.jw-button-color.jw-reset').hide();
            $('.jw-icon.jw-icon-inline.jw-button-color.jw-reset.jw-icon-next').hide();
            $('.jw-icon.jw-icon-inline.jw-button-color.jw-reset.jw-icon-prev').hide();
            $('.jw-title.jw-reset').css("margin-left", "145px");
            
            $('[button="jwplayer-spice"] .jw-icon.jw-dock-image.jw-reset').hover(
              function() {
                $(this).css('background-image', 'url(/js/jwplayer/spice-active.png)');
              }, 
              function() {
                  $(this).css('background-image', 'url(/js/jwplayer/spice.png)');
              }
            );
            
            $('[button="jwplayer-forward"] .jw-icon.jw-dock-image.jw-reset').hover(
              function() {
                $(this).css('background-image', 'url(/js/jwplayer/forward-active.png)');
              }, 
              function() {
                $(this).css('background-image', 'url(/js/jwplayer/forward.png)');
              }
            );
            
            $('[button="jwplayer-heart"] .jw-icon.jw-dock-image.jw-reset').hover(
              function() {
                $(this).css('background-image', 'url(/js/jwplayer/heart-active.png)');
              }, 
              function() {
                $(this).css('background-image', 'url(/js/jwplayer/heart.png)');
              }
            );
            
            // $('.jw-icon.jw-icon-inline.jw-button-color.jw-reset.jw-icon-playback').insertAfter('.jw-icon.jw-icon-inline.jw-button-color.jw-reset.jw-icon-prev');
            // move overlay outside the player bar
            // $(".jw-overlay.jw-reset").insertAfter("#playlistOverlay");
            $(".jw-tooltip-title.jw-reset").append("<span><i class='fa fa-close fa-2x' style='float:right; margin:2px 10px;'></i></span>");
            
            $(".fa.fa-close.fa-2x").click(
              function() {
                $(".playlist-wrap .jw-menu.jw-playlist-container.jw-background-color.jw-reset").css("margin-right", "-250px");
              }
            );

            // move time-tooltip and show relative to the cursor position

            $(".jw-slider-time.jw-background-color.jw-reset.jw-slider-horizontal.jw-reset").mouseover(
              function() {
                $(".playlist-wrap .jw-time-tip.jw-background-color.jw-reset").css("opacity", "1");
              }
            );
            
            $(".jw-slider-time.jw-background-color.jw-reset.jw-slider-horizontal.jw-reset").mouseout(
              function() {
                $(".playlist-wrap .jw-time-tip.jw-background-color.jw-reset").css("opacity", "0");
              }
            );

            $(".jw-slider-time.jw-background-color.jw-reset.jw-slider-horizontal.jw-reset").mousemove(
              function(event) {
                var xPos = "" + event.clientX - 18 + "px";
                $(".playlist-wrap .jw-time-tip.jw-background-color.jw-reset").css("left", xPos);
              }
            );
            $(".playlist-wrap .jw-menu.jw-playlist-container.jw-background-color.jw-reset").css("margin-right", "-250px");
            $(".playlist-wrap .jw-slider-volume.jw-volume-tip.jw-background-color.jw-reset.jw-slider-vertical.jw-reset").css("margin-bottom", "-170px");

            $("#gjaudioplayer").removeClass(".jw-flag-user-inactive");
            $("#gjaudioplayer.jw-flag-user-inactive").css("display", "block");


            // $(".jw-preview.jw-reset").one('click', function (event) {  
            //    event.preventDefault();

            // });

        });

        jwplayer().on('play', function() {
            $(".jw-icon.jw-icon-inline.jw-button-color.jw-reset.jw-icon-playback").addClass("pause").removeClass("play");
            $(".jwplayer.jw-reset.jw-skin-gjaudioplayer.jw-stretch-uniform.jw-state-playing").removeClass(".jw-flag-user-inactive");

        });

        jwplayer().on('pause', function() {
            $(".jw-icon.jw-icon-inline.jw-button-color.jw-reset.jw-icon-playback").addClass("play").removeClass("pause");
            $(".jwplayer.jw-reset.jw-skin-gjaudioplayer.jw-stretch-uniform.jw-state-playing").removeClass(".jw-flag-user-inactive");

        });

        jwplayer().on('idle', function() {
            $(".jwplayer.jw-reset.jw-skin-gjaudioplayer.jw-stretch-uniform.jw-state-playing").removeClass(".jw-flag-user-inactive");
        });

        jwplayer().on('buffer', function() {
            $(".jwplayer.jw-reset.jw-skin-gjaudioplayer.jw-stretch-uniform.jw-state-playing").removeClass(".jw-flag-user-inactive");
        });
    }
</script>