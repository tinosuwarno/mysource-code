<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="{{asset('js/jquery-2.2.0.js')}}"></script>
<script src="{{asset('js/jquery-1.12.4.js')}}"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<script src="{{asset('js/jquery.stellar.js')}}"></script>
<script src="{{asset('js/jquery.nicescroll.js')}}"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script src='{{asset('js/jquery.pjax.js')}}'></script>
<script src="{{asset('js/bootstrap.js')}}" type="text/javascript"></script>

<script src="{{asset('js/star-rating.js')}}" type="text/javascript"></script>
<script src="{{asset('js/jquery.sidr.js')}}" type="text/javascript"></script>
<script src="{{asset('js/isotope.pkgd.min.js')}}" type="text/javascript"></script>

<script src="{{asset('/js/owl.carousel.js')}}"></script>
<script src="{{asset('/js/moment.js')}}"></script>
<script src="{{asset('/js/Chart.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.2.20/angular.min.js"></script> -->
<script src="{{asset('/js/bootstrap-tagsinput.min.js')}}"></script>
<!-- <script src="{{asset('/js/bootstrap-tagsinput-angular.js')}}"></script> -->
<script src="{{asset('/js/chosen.jquery.js')}}"></script>