@extends('layouts.default-base')
{{-- overwrite title --}}
@section('title')
Gritjam - Event
@endsection
@section('styles')
@endsection
{{-- content --}}
@section('contents')
<?php 
  include app_path() . '/library/timeline.php'; 
  $genreDiv = getGenre();

   $myApp = App::make('baseUrl'); 
   $baseUrl = $myApp->baseUrl ; 
  ?>
<!-- ---------- SECTION MARKET MUSIC ----------------- --- -->
<div class="block-white"></div>
<div class="block-white"></div>
<div class="block-white visible-sm hidden-lg hidden-xs hidden-md"></div>
<div class="block-white visible-sm hidden-lg hidden-xs hidden-md"></div>
<div class="container-fluid">
  <div class="row">
    <div class="col-lg-12">
      <div class="menu text-center">
        <h3>EVENT</h3>
        <div class="hr-sm"></div>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="warp-src-market-gj-2">
      <div class="row">
        <div class="menu text-center">
          <ul class="action-menu-profile">
            <li <?php if($category == 1){echo "class='active'";} ?>>
              <a data-pjax='yes' id='pjax' href="{{ url('events/new-event') }}">New Event</a>
            </li>
            <li <?php if($category == 2){echo "class='active'";} ?>>
              <a data-pjax='yes' id='pjax' href="{{ url('events/most-popular') }}">Most Popular</a>
            </li>
            <li <?php if($category == 3){echo "class='active'";} ?>>
              <a data-pjax='yes' id='pjax' href="{{ url('events/running-event') }}">Running Event</a>
            </li>
          </ul>
        </div>
        <div class="hr"></div>
        <!-- <form class="navbar-form" role="search">
          <div class="input-group warp-input-src-market-gj">
              <div class="col-lg-2 col-md-2 col-sm-2 "></div>
              <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12 nopad-right"><span class="txt-sub-src">Search</span></div>
              <div class="col-lg-6 col-md-6 col-sm-6 ">
                  <input type="text" class="input-src-market-gj-2" placeholder="Title" name="">
              </div>
              <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12 nopad-left"><button type="submit" class="btn btn-primary btn-sub-src-market"><i class="fa fa-search"></i></button></div>
              <div class="col-lg-2 col-md-2 col-sm-2 "></div>
          </div>
          </form>
          <div class="col-xs-12 text-center"><a href="advance-search.html" class="text-center btn-advance-search">Advance Search</a></div> -->
      </div>
    </div>
  </div>
</div>
<div class="container">
  <div class="row">
    <div class="warp-content-song-track">
      <div class="bg-grey-col pos-rel">
        <div class="warp-title-menu-list-track">
          <div class="warp-title-list-track">
          @if($category == 1)
             <h4 style='margin-top:16px;' id='newEvents'>NEW EVENT</h4> 
          @elseif($category == 2)
             <h4 style='margin-top:16px;' id='newEvents'>MOST POPULAR EVENT</h4>
          @elseif($category == 3)
             <h4 style='margin-top:16px;' id='newEvents'>RUNNING EVENT</h4>
          @endif
          </div>
          <!-- <div class="warp-menu-list-track">
            <div class="warp-control-track">
            <div class="wrap-track-cat">
                <div class="owl-track-cat owl-theme"> -->
          <?php 
            //echo $genreDiv;
            ?>
          <!-- </div>
            </div>
              
              <a class="btn next next-track-cat"><i class="fa fa-chevron-right"></i></a>
                <a class="btn prev prev-track-cat"><i class="fa fa-chevron-left"></i></a>
            </div>
            </div> -->
        </div>
      </div>
      <div class="warp-all-music-market">
        <?php if ($return[0]['count']==0) {?>
        <!-- ------------ JIKA MASIH KOSONG ----------- -->
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padten">
          <div class="nf-item-wall-track">
            <p>No Event Available Yet</p>
          </div>
        </div>
        <div class="clearfix"></div>
        <!-- ------------ END JIKA MASIH KOSONG ----------- --> 
        <?php }else{ ?>
        <!-- ================== SONG CARDS ====================== -->
        <?php foreach ($return[0]['data'] as $key => $value){?>
        <?php
          $statusMemberEventParticipatePrint = "<div class='btn-in-stat' id='groupEventParticipant".$value['event_id']."'>".
                                          "<input type='button' onclick='sudahParticipate();' class='btn btn-line-stat col-btn-ex' value='participated'>".
                                          "</div>";
          $statusMemberEventNotParticipatePrint = "<div class='btn-in-stat' id='groupEventParticipant".$value['event_id']."'>".
                                  "<button type='button' onclick='groupEventParticipant(".'"'.$value['groupID'].'"'.", ".'"'.$value['event_id'].'"'.", ".'"1"'.");' class='btn btn-line-stat col-btn-ex'>Participate</button>".
                                  "</div>";
          $statusMemberGroupJoin = "<div class='btn-in-stat' id='groupEventParticipant".$value['event_id']."'>".
                                              "<button type='button' onclick='harusJoinGroup(".'"'.$value['groupID'].'"'.", ".'"'.$value['event_id'].'"'.");' class='btn btn-line-stat col-btn-ex'>Participate</button>".
                                              "</div>";        
          $statusOther = "<div class='btn-in-stat'></div>";                                                               
          ?>
        <div class="col-md-6 gj-song-wrap">
          <div class="gj-song-card">
          <?php 
               $time = time();
               $tanggal = (date("Y-m-d"." "."H-m-s",$time));
               if($tanggal > $value['end_time']){
               echo "<div class='warp-pinned-evt-close'><img src='".$baseUrl."img/evt-close.png' class='img-responsive'></div>";
                }
          ?>
          
            <div class="gj-song-cover">
              @if($value['picture_event']=='' || $value['picture_event']==null)
              <img src="{{asset('img/album/album-song_475x475.jpg')}}" class="img-responsive">
              @else
              <img src="{{asset($value['picture_event'])}}" class="img-responsive">
              @endif
            </div>
            <div class="gj-song-desc">
              <div class="gj-song-story">
                <h1 class="gj-song-title"><a href="{{URL::to('event/'.$value['event_id'])}}">{{$value['event_title']}}</a></h1>
              </div>
              <div class="gj-text-desc">
                <p class="revs">{{$value['description_event']}}</p>
                <p>participant(s): {{$value['peserta']}}</p>
              </div>
            </div>
            <div class="warp-stat-card top-stat-card">
              <div class="stat-song-track" id="groupEventLiked{{$value['event_id']}}">
                <div class="warp-ico-stat">
                  <div class="ico-stat">
                    <a href="javascript:void(0)" <?php if($value['likedGroupEvent']==1){ echo "onclick='udahLikePost();'"; } else { echo "onclick='clickLikeGroupEvent(".$value['event_id'].",".$value['liked'].");'"; } ?>>
                    <i class="fa fa-heart like-single-list-song" <?php if($value['likedGroupEvent']==1){echo "style='color:red;'";} ?>></i>
                    </a>
                  </div>
                  <div class="num-stat">{{$value['liked']}}</div>
                </div>
              </div>
              <div class="stat-song-track">
                <div id="sharedEvent{{$value['event_id']}}" class="warp-ico-stat">
                  <div class="ico-stat"><a onclick="sharedEventToSocial({{$value['event_id']}},{{$value['shared']}} );" style='cursor:pointer;'><i class="fa fa-mail-forward forward-single-list-song"></i></a></div>
                  <div class="num-stat">{{$value['shared']}}</div>
                </div>
              </div>

              <span id="eventTitle{{$value['event_id']}}" style='display:none;'>{{$value['event_title']}}</span>
              <span id="eventDescription{{$value['event_id']}}" style='display:none;'>{{$value['description_event']}}</span>
              <span id="eventPicture{{$value['event_id']}}" style='display:none;'>{{$value['picture_event']}}</span>

              <div class="btn-in-stat" id="groupEventParticipant{{$value['event_id']}}">
                <span id="trackID-groupEvent{{$value['event_id']}}" style='display:none;'>{{$value['trackID']}}</span>
                <span id="judulTrack-groupEvent{{$value['event_id']}}" style='display:none;'>{{$value['judulTrack']}}</span>
                <span id="instrumentTrack-groupEvent{{$value['event_id']}}" style='display:none;'>{{$value['instrument']}}</span>
                <span id="creatorTrack-groupEvent{{$value['event_id']}}" style='display:none;'>{{$value['creatorTrack']}}</span>
                <span id="priceTrack-groupEvent{{$value['event_id']}}" style='display:none;'>{{$value['price']}}</span>
                <span id="eventTitle-groupEvent{{$value['event_id']}}" style='display:none;'>{{$value['event_title']}}</span>
                <span id="groupName-groupEvent{{$value['event_id']}}" style='display:none;'>{{$value['group_name']}}</span>
                <?php 
                $time = time();
                $tanggal = (date("Y-m-d"." "."H-m-s",$time));

                  if($value['checkadmin'] != 'admin'){
                    if($tanggal < $value['end_time']){
                          if($value['group_type'] === 1 || $value['group_type'] === '1'){  // check group public
                            if($value['checkStatusMember'] === 'member'){
                                if($value['event_type'] === 1 || $value['event_type'] === '1'){    //check event public
                                     if($value['statusMemberEventParticipate'] === 1 || $value['statusMemberEventParticipate'] === '1'){
                                          echo $statusMemberEventParticipatePrint;
                                     }else{
                                          echo $statusMemberEventNotParticipatePrint;
                                     }
                                }else{
                                     if($value['statusMemberEventParticipate'] === 1 || $value['statusMemberEventParticipate'] === '1'){
                                          echo $statusMemberEventParticipatePrint;
                                     }else{
                                         echo $statusMemberEventNotParticipatePrint;
                                     }
                                }
                            }else{
                                if($value['event_type'] === 1 || $value['event_type'] === '1'){
                                     if($value['statusMemberEventParticipate'] === 1 || $value['statusMemberEventParticipate'] === '1'){
                                         
                                          echo $statusMemberEventParticipatePrint;
                                     }else{
                                          echo $statusMemberEventNotParticipatePrint;
                                     }
                                }else{
                                     if($value['statusMemberEventParticipate'] === 1 || $value['statusMemberEventParticipate'] === '1'){
                                          echo $statusMemberEventParticipatePrint;
                                     }else{
                                          echo $statusOther;
                                     }
                                }
                            }
                       }else{
                            if($value['checkStatusMember'] === 'member'){
                                 if($value['event_type'] === 1 || $value['event_type'] === '1'){
                                      if($value['statusMemberEventParticipate'] === 1 || $value['statusMemberEventParticipate'] === '1'){
                                           echo $statusMemberEventParticipatePrint;
                                      }else{
                                           echo $statusMemberEventNotParticipatePrint;
                                      } 
                                 }else{
                                      if($value['statusMemberEventParticipate'] === 1 || $value['statusMemberEventParticipate'] === '1'){
                                           echo $statusMemberEventParticipatePrint;
                                      }else{
                                           echo $statusMemberEventNotParticipatePrint;
                                      }
                                 }
                            }else{
                                if($value['event_type'] === 1 || $value['event_type'] === '1'){
                                     if($value['statusMemberEventParticipate'] === 1 || $value['statusMemberEventParticipate'] === '1'){
                                          echo $statusMemberEventParticipatePrint;
                                     }else{
                                          echo $statusMemberGroupJoin;
                                     }
                                }else{
                                     if($value['statusMemberEventParticipate'] === 1 || $value['statusMemberEventParticipate'] === '1'){
                                          echo $statusMemberEventParticipatePrint;
                                     }else{
                                          echo $statusOther;
                                     }
                                }
                            }
                  
                       }
                    }else{
                      echo $statusOther;
                    }    
                  }else{
                     echo $statusOther;
                  } 
                  
                  ?>    
              </div>
            </div>
            <div class="gj-song-revision">
              <div class="name-group-post">
                <i class="fa fa-group"></i><a href="" class="pru"><strong>{{$value['group_name']}}</strong></a>
              </div>
            </div>
          </div>
          <hr>
        </div>
        <?php }} ?>

        @if($return[0]['count'] > 6)
        <!-- ================== END SONG CARDS ====================== -->
        <!-- ----------- BTN LOAD MORE -------------------- -->
        <div class="warp-btn-loadmore-list" id='loadEventMore1'>
          <a class="btn-loadmore-list" onclick='loadMoreEvent(1,{{$category}})'>
          <i class="fa fa-plus-circle"></i>
          <span class="text-load-more-list">LOAD MORE</span>
          </a>
          <!--
            <div class="col-xs-push-5 col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 lazyloader-list">
                <img src="img/loader-more.gif" class="img-full-responsive e-centering width-loader">
            </div> 
            -->
        </div>
        @endif
        <!-- ------- END BTN LOAD MORE -------------------- -->
      </div>
    </div>
    <div class="warp-content-song-track">
      <div class="bg-grey-col pos-rel">
        <div class="warp-title-menu-list-track">
          <div class="warp-title-list-track">
            <h4 style="margin-top:16px;" id='eventWinner'>EVENT WINNER</h4>
          </div>
          <!-- <div class="warp-menu-list-track">
            <div class="warp-control-track">
            <div class="wrap-track-cat">
                <div class="owl-track-cat owl-theme"> -->
          <?php 
            //echo $genreDiv;
            ?>
          <!-- </div>
            </div>
              
              <a class="btn next next-track-cat"><i class="fa fa-chevron-right"></i></a>
                <a class="btn prev prev-track-cat"><i class="fa fa-chevron-left"></i></a>
            </div>
            </div> -->
        </div>
      </div>
      <div class="warp-all-music-market">
        @if(sizeof($winner)==0)
        <!-- ------------ JIKA MASIH KOSONG ----------- -->
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padten">
          <div class="nf-item-wall-track">
            <p>No Event Winner Yet</p>
          </div>
        </div>
        <div class="clearfix"></div>
        <!-- ------------ END JIKA MASIH KOSONG ----------- --> 
        @else
        <!-- ================== SONG CARDS ====================== -->
        @foreach($winner['data'] as $key => $value)
        <div class="col-md-6 gj-song-wrap">
          <div class="gj-song-card">
            <div class="gj-song-cover">
              <img src="{{asset($value['trackPic'])}}" class="img-responsive">
              <!-- <div class="gj-spiceit-label">
                <img src="img/spiceit.png" class="img-responsive">
                </div> -->
              <div class="gj-song-play"><i onclick="playMusic('{{$value['trackID']}}');" class="fa fa-play-circle-o fa-3x"></i></div>
              <div class="gj-song-btn text-center"><i class="fa fa-magic"></i><i class="gritjam-icons gritjam-icon-add-playlist" data-trackid='{{$value['trackID']}}'></i></div>
            </div>
            <div class="gj-song-desc">
              <div class="gj-song-story">
                <h1 class="gj-song-title">{{$value['trackTitle']}}</h1>
                <h5 class="gj-song-username"><i class="fa fa-user"></i> {{$value['username']}}</h5>
                <p class="gj-song-contributor">Event: <a href="{{URL::to('event/'.$value['eventID'])}}">{{$value['eventTitle']}}</a></p>
                <ul class="gj-song-author">
                  <li>Original Song Title: {{$value['originalTrack']['name']}}</li>
                  <li>Genre: {{$value['originalTrack']['genre']}}</li>
                  <li>Original From: {{$value['originalTrack']['username']}}</li>
                </ul>
              </div>
              <div class="gj-song-action">
                <!-- ===<p><i class="fa fa-exclamation-circle" style="opacity:.5;"></i> <a href="">Report/feedback</a></p>=== -->
                <div class="btn-report-track-song"><a class="btn-report-ts"><i class="fa fa-exclamation-triangle btn"></i></a><span>report</span></div>
                <div class="gj-played">
                  <p>{{$value['trackPlay']}} plays</p>
                </div>
              </div>
              <div class="warp-stat-card">
                <div class="stat-song-track">
                  <div class="warp-ico-stat">
                    <div class="ico-stat"><a href="javascript:void(0)" onclick="clickLike();"><i class="fa fa-heart like-single-list-song"></i></a></div>
                    <div class="num-stat">{{$value['trackLiked']}}</div>
                  </div>
                </div>
                <div class="stat-song-track">
                  <div class="warp-ico-stat">
                    <div class="ico-stat"><a class="btn-share-ts"><i class="fa fa-mail-forward forward-single-list-song"></i></a></div>
                    <div class="num-stat">{{$value['trackShared']}}</div>
                  </div>
                </div>
                <div class="stat-song-track">
                  <div class="warp-ico-stat">
                    <div class="ico-stat"><a href=""><i class="fa fa-magic spice-single-list-song"></i></a></div>
                    <div class="num-stat">{{$value['trackSpice']}}</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <hr>
        </div>
        @endforeach
        @endif

        @if($winner['count'] > 6)
        <!-- ================== END SONG CARDS ====================== -->
        <!-- ----------- BTN LOAD MORE -------------------- -->
        <div class="warp-btn-loadmore-list" id='loadEventMore2'>
          <a class="btn-loadmore-list" onclick='loadMoreEvent(2,{{$category}})'>
          <i class="fa fa-plus-circle"></i>
          <span class="text-load-more-list">LOAD MORE</span>
          </a>
          <!--
            <div class="col-xs-push-5 col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 lazyloader-list">
                <img src="img/loader-more.gif" class="img-full-responsive e-centering width-loader">
            </div> 
            -->
        </div>
        @endif
        <!-- ------- END BTN LOAD MORE -------------------- -->
      </div>
    </div>
  </div>
</div>
<div class="block-white"></div>
<!-- ---------- END DEMO MUSIC ------------------ -->
@endsection