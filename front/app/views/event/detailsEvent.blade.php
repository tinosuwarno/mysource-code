@extends('layouts.default-base')
{{-- overwrite title --}}
@section('title')
Gritjam - Details Group Events
@overwrite
@section('styles')
<meta property="og:title" content="..."/>
<meta property="og:image" content="..."/>
<meta property="og:description" content="..."/>
@stop
{{-- content --}}
@section('contents')
<?php
  $session = Session::get('data');
  $id = $session['id'];
  ?> 
<!-- ---------- SECTION MARKET MUSIC ----------------- --- -->
<div class="block-white"></div>
<div class="block-white"></div>
<div class="block-white visible-sm hidden-lg hidden-xs hidden-md"></div>
<div class="block-white visible-sm hidden-lg hidden-xs hidden-md"></div>
<div class="container-fluid">
  <div class="row">
    <div class="md-w-warp">
      <div class="warp-all-wall">
        <!-- ================================== DETAIL EVENT ================================== -->    
        <div class="col-lg-4 col-md-5 col-sm-12 padten-sxs">
        <?php 
          $time = time();
          $tanggal = (date("Y-m-d"." "."H:m:s",$time));
          $to_time = strtotime($tanggal);
  
          $from_time = strtotime($end_time);
        
          $minutes = round(abs($to_time - $from_time)/ (60),0);
          $hours = round(abs($to_time - $from_time)/ (60*60),0);
          $days = round(abs($to_time - $from_time)/(24*60*60),0);
          if($tanggal > $end_time){
            echo "<div class='warp-close-evt'><span>Event Closed</span></div>";
          }else{
             if($days != 0){
                echo "<div class='warp-running-evt'><span>Event will finish in ".$days." days</span></div>";
             }else if($hours != 0){
                echo "<div class='warp-running-evt'><span>Event will finish in ".$hours." hours</span></div>";
             }else if($minutes != 0){
                echo "<div class='warp-running-evt'><span>Event will finish in ".$minutes." minutes</span></div>";
             }
          }
          
           ?>
          <div class="timeline-box my-event">
            <div class='col-xs-push-5 col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 lazyloader-list' id='loadingDetailsEvent1'>
              <img src="{{asset('img/loader-more.gif')}}" class='img-full-responsive e-centering width-loader'>
            </div>
          </div>
        </div>
        <!-- ================================== END DETAIL EVENT ================================== -->   
        <!-- ==================================  WINNER LAGU DI EVENT ================================== --> 
        <div class="col-lg-8 col-md-7 col-sm-12 col-xs-12">
          <div class="row">
            <div class="col-lg-12">
              <div class="warp-title-side-right-wall bc-winner-evt">
                <h4>Event's Winners</h4>
              </div>
              <div class="warp-winner-event">
                <div class="row">
                  <div class="col-md-12 gj-song-wrap" id="sectionSongWinnerEvent">
                    <div class='col-xs-push-5 col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 lazyloader-list' id='loadingDetailsEvent3'>
                      <img src="{{asset('img/loader-more.gif')}}" class='img-full-responsive e-centering width-loader'>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- =============================== END WINNER LAGU DI EVENT ================================== -->   
          <!-- ==================================  LIST USER PARTICIPATED ================================== -->          
          <div class="row">
            <div class="col-lg-12">
              <div class="warp-title-side-right-wall">
                <h4>Event's Song</h4>
              </div>
              <div class="warp-all-music-event" id="listSongEventRelease">
                <div class='col-xs-push-5 col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 lazyloader-list' id='loadingDetailsEvent2'>
                  <img src="{{asset('img/loader-more.gif')}}" class='img-full-responsive e-centering width-loader'>
                </div>
              </div>
            </div>
          </div>
          <!-- =============================== END LIST USER PARTICIPATED ================================== -->           
          <!-- ==================================  LIST LAGU DI EVENT ================================== -->   
          <div class="row">
            <div class="col-lg-12">
              <div class="warp-title-side-right-wall">
                <h4>Participants</h4>
              </div>
              <div class="warp-all-music-event" id="listParticipant">
                <div class="col-md-12 col-sm-12">
                  <div class="row">
                    <div class="scrollable-content sos-wrapper" id="listParticipantUserNotReleaseSong">
                      <div class='col-xs-push-5 col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 lazyloader-list' id='loadingDetailsEvent4'>
                        <img src="{{asset('img/loader-more.gif')}}" class='img-full-responsive e-centering width-loader'>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- ================================ END LIST LAGU DI EVENT ================================= -->                    
        </div>
      </div>
    </div>
  </div>
</div>
<div class="block-white"></div>
@stop