<div id='playlistModal'>
  <div id='addPlaylist' tabindex='-1' role='dialog' aria-hidden='true' class='vh-center modal fade'>
    <div class='modal-dialog modal-sm'>
      <div class='modal-content'>
        <div class='modal-body nopadding'>
          <div class='row'>
            <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sxs-12'>
              <div class='warp-crt-add-playlist'>
                <div class='modal-header color-gj-popup-report'>
                  <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                  <h4 class='modal-title'>Add to Playlist</h4>
                </div>
                <div class='warp-addplaylist'>
                  <!-- =============================== HAS ADDED TO PLAYLIST ===================================== -->
                  <div class='added--pl' id='addedStatusPlaylist' style='display:none'>
                    This Song has been added to 
                    <div class='name--pl'><span id='addedPlaylist'></span></div>
                  </div>
                  <!-- =========================== END HAS ADDED TO PLAYLIST ===================================== -->
                  <!-- ============================ SECTION EXISTING PLAYLIST ===================================== -->
                  <div class='sec-playlist'>
                    <strong class='title-secplaylist'>Playlist</strong>
                    
                    <div class='warp-ext-pl'>
                      <div class="warp-loader-m" id='clearThisPlaylistName'>
                          <div id='loadingPlaylistModal' class="col-xs-push-5 col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 lazyloader-list">
                              <img src="{{asset('img/loader-more.gif')}}" class="img-full-responsive e-centering width-loader">
                          </div>
                      </div>
                    </div>
                  </div>
                  <!-- ======================== END SECTION EXISTING PLAYLIST ===================================== -->
                  <div class='warp-btn-add-crt'>
                    <a class='btn btn-add-crt-pl newPlaylist' id='newPlay' data-dismiss='modal'>Create New Playlist</a>
                    <b></b>
                    <a class='btn btn-add-crt-pl col-btn-red' data-dismiss='modal'>Cancel</a>                        
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id='newPlaylist' class='vh-center modal fade'>
    <div class='modal-dialog modal-sm'>
      <div class='modal-content'>
        <div class='modal-body nopadding'>
          <div class='row'>
            <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sxs-12'>
              <div class='warp-crt-add-playlist'>
                <div class='modal-header color-gj-popup-report'>
                  <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                  <h4 class='modal-title'>New Playlist </h4>
                </div>
                <div class='warp-btn-add-crt'>
                  <form id='createNewPlaylist' onsubmit='return submitNewPlaylist();'>
                    <div class='box-input'>
                      <input type='text' class='input-src-market-gj-2' placeholder='Name Playlist' name='playlistName'>
                      <input type='hidden' id='trackID_playlist' name='trackID' value=''>
                    </div>
                    <button type='submit' class='btn btn-primary text-center'>Submit</button>
                    <button type='button' class='btn btn-primary text-center cancel' data-dismiss='modal'>Cancel</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id='extPlaylist' class='vh-center modal fade'>
    <div class='modal-dialog modal-sm'>
      <div class='modal-content'>
        <div class='modal-header color-gj-popup-report'>
          <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
          <h4 class='modal-title'>Select Playlist</h4>
        </div>
        <div class='modal-body'>
          <div class='row'>
            <div class='col-sm-12'>
              <form id='addToPlaylist' onsubmit='return submitTrackToPlaylist();'>
                <div class='warp-opsi-report' id='clearThisPlaylistName'>
                  <div id='loadingPlaylistModal' class='col-xs-push-5 col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 lazyloader-list'>
                      <img src="{{asset('/img/loader-more.gif')}}" class='img-full-responsive e-centering width-loader'>
                  </div> 
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id='deletePlaylist' tabindex='-1' role='dialog' aria-hidden='true' class='modal vh-center fade'>
    <div class='modal-dialog modal-sm'>
      <div class='modal-content'>
        <div class='modal-body nopadding'>
          <div class='row'>
            <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sxs-12'>
              <div class='warp-crt-add-playlist'>
                <div class='modal-header color-gj-popup-report'>
                  <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>
                  <h4 class='modal-title'>Delete Playlist</h4>
                </div>
                <div class='warp-15'>
                  <form id='deleteMyPlaylist' onsubmit='return deleteMyPlaylist();'>
                    <input type='hidden' id='playlistID_delete' name='playlistID' value=''>
                    <div class='acc-req'>Are You Sure want to Delete This Playlist ?</div>
                    <div class='clearfix'></div>
                    <div class='col-lg-6 text-center'>
                      <button class='btn bgc-btn c-f-btn cancel' data-dismiss='modal'>Cancel</button>
                    </div>
                    <div class='col-lg-6 text-center'>
                      <button class='btn c-f-btn bgc-btn-dec'>Delete</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id='addSongToPlaylist' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true' class='modal fade' >
    <div class='modal-dialog modal-lg'>
      <div class='modal-content'>
        <div class='modal-body nopadding'>
          <div class='row'>
            <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sxs-12'>
              <div class='warp-list-song-popup-player tittle-left-top-round'>
                <div class='title-playlist tittle-left-top-round tittle-right-top-round'>
                  <form class='navbar-form' role='search'>
                    <div class='input-group warp-input-src-market-gj'>
                      <div class='col-lg-2 col-md-2 col-sm-2 '></div>
                      <div class='col-lg-1 col-md-1 col-sm-1 col-xs-12 nopad-right'><span class='txt-sub-src'>Search</span></div>
                      <div class='col-lg-6 col-md-6 col-sm-6 '>
                        <input id='tags' type='text' class='input-src-market-gj-2' placeholder='Search Song (min character: 3)' name=''>
                      </div>
                      <div class='col-lg-1 col-md-1 col-sm-1 col-xs-12 nopad-left'><button type='submit' class='btn btn-primary btn-sub-src-market'><i class='fa fa-search'></i></button></div>
                      <div class='col-lg-2 col-md-2 col-sm-2 '>
                      </div>
                      <a href='' class='btn-close-popup-list' data-dismiss='modal'><i class='fa fa-close adds-single-list-song'></i></a>
                    </div>
                  </form>
                </div>
                <div class='warp-song-pl qtr-pl half-c'>
                  <div class='col-xs-push-5 col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 lazyloader-list' id='getAllSong'>
                    <img src='{{asset('img/loader-more.gif')}}' class='img-full-responsive e-centering width-loader'>
                  </div>
                  <div class='list-song-striped'>
                    <div class='li-ac-src-track'>
                      <div class='btn-li-ac-src-track'></div>
                      <div class='tittle-song-track'><span>Bullet for My Valentine</span></div>
                      <div class='author-song-track'><a href=''>Tinau R</a></div>
                      <div class='harga-li-ac-src-track'><span>04:32</span></div>
                      <div class='harga-li-ac-src-track'><span>Rp. 1000,-</span></div>
                      <div class='stat-song-track'>
                        <div class='warp-ico-stat'>
                          <div class='ico-stat'><a href='javascript:void(0)' onclick='clickLike();'><i class='fa fa-heart like-single-list-song'></i></a></div>
                          <div class='num-stat'>31</div>
                        </div>
                      </div>
                      <div class='stat-song-track'>
                        <div class='warp-ico-stat'>
                          <div class='ico-stat'><a class='btn-share-ts'><i class='fa fa-mail-forward forward-single-list-song'></i></a></div>
                          <div class='num-stat'>98</div>
                        </div>
                      </div>
                      <div class='stat-song-track'>
                        <div class='warp-ico-stat'>
                          <div class='ico-stat'><a href=''><i class='fa fa-magic spice-single-list-song'></i></a></div>
                          <div class='num-stat'>17</div>
                        </div>
                      </div>
                      <div class='add-pl-track-song'>
                        <a class='btn-add-pl-track-song' title='add this song to playlist'><i class='gritjam-icons gritjam-icon-add-playlist'></i></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id='trackModal'>
  <div id='addFeaturedSong' class='vh-center modal fade'>
      <div class='modal-dialog modal-lg'>
        <div class='modal-content'>
          <div class='modal-header color-gj-popup-report'>
            <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
            <h4 class='modal-title'>Select Your Project Song</h4>
          </div>
          <div class='modal-body'>
            <div class='row'>
              <div class='col-sm-12'>
                <div class='tittle-result-search'>
                  <div class='li-ac-src-ft'>
                    <div class='num-li-ac-src'><span>#</span></div>
                    <div class='tittle-li-ac-src-ft'><span class='pull-left'>Song Tittle</span></div>
                    <div class='artist-li-ac-src-ft'><span class='pull-left text-left'>Contributor</span></div>
                    <div class='time-li-ac-src-ft'><span>Length</span></div>
                    <div class='forward-li-ac-src-ft'><span></span></div>
                    <div class='play-li-ac-src-ft'><span></span></div>
                  </div>
                </div>
                <div class='overflow-li-ac-src-karoke'>
                  <div id='clearThisFeaturedSong'>
                    <div id='loaderFeaturedSong' class='col-xs-push-5 col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 lazyloader-list'>
                        <img src='/img/loader-more.gif' class='img-full-responsive e-centering width-loader'>
                    </div> 
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
<!-- ============================= MODAL SHARE TRACK / SONG ======================================== -->
<div id='shareSongTrack' class=' vh-center modal fade' >
  <div class='modal-dialog' style ='display: inline-block;text-align:left; vertical-align:middle;'>
    <div class='modal-content'>
      <div class='modal-header color-gj-popup-report'>
        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
        <h4 class='modal-title'>Share Song / Track <span id='data-judul' style='display: none;'></span></h4>
      </div>
      <div class='modal-body'>
        <div class='row'>
          <div class='col-sm-12'>
            <span id='data-trackid' style='display: none;'></span>
            <div class='warp-opsi-report'>
              <div class='share-med-soc-gj'>
                <ul>
                 <!--  <li><a href='javascript:void(0);' onclick='fb_callout();' class='facebook-f-color'><i class='fa fa-facebook-square'></i></a></li>
                  <li><a href='javascript:void(0);' onclick='twitter_share();' class='twitter-f-color'><i class='fa fa-twitter-square'></i></a></li>
                  <li><a href='javascript:void(0);' onclick='gplus_share();' class='gplus-f-color'><i class='fa fa-google-plus-square'></i></a></li> -->
                  <!-- <li><a href='javascript:void(0);' onclick='tumblr_share();' class='tumblr-f-color'><i class='fa fa-tumblr-square'></i></a></li> -->
                  <!-- <li><a href='javascript:void(0);' onclick='pinterest_share();' class='pinterest-f-color'><i class='fa fa-pinterest-square'></i></a></li> -->

                    <li><a href='#' class='facebook-f-color'><i class='fa fa-facebook-square'></i></a></li>
                    <li><a href='#' class='twitter-f-color'><i class='fa fa-twitter-square'></i></a></li>
                    <li><a href='#' class='gplus-f-color'><i class='fa fa-google-plus-square'></i></a></li>
                </ul>
              </div>
              <!-- <div class='label-url'>URL : </div> -->
              <!-- <div class='warp-url-share'>
                <input type='text' class='input-src-market-gj-2' placeholder='www.gritjam.com/nananananaBatman' name=''>
              </div> -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- ============================= MODAL SHARE EVENT ======================================== -->
<div id='sharedEventModal' class='vh-center modal fade' >
  <div class='modal-dialog' style ='display: inline-block;text-align:left; vertical-align:middle;'>
    <div class='modal-content'>
      <div class='modal-header color-gj-popup-report'>
        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
        <h4 class='modal-title'>Share Event </h4>
      </div>
      <div class='modal-body'>
        <div class='row'>
          <div class='col-sm-12'>
            <span id="event_title_shared" style='display: none;'></span>
            <span id="event_description_shared" style='display: none;'></span>
            <span id="event_picture_shared" style='display: none;'></span>
            <span id="eventID_shared" style='display: none;'></span>
            <span id="totalShared_shared" style='display: none;'></span>
            <div class='warp-opsi-report'>
              <div class='share-med-soc-gj'>
                <ul>
                    <li><a href='#' class='facebook-f-color'><i class='fa fa-facebook-square'></i></a></li>
                    <li><a href='#' class='twitter-f-color'><i class='fa fa-twitter-square'></i></a></li>
                    <li><a href='#' class='gplus-f-color'><i class='fa fa-google-plus-square'></i></a></li>

                    <!-- <li><a href='#' onclick="fb_shared_event();" class='facebook-f-color'><i class='fa fa-facebook-square'></i></a></li>
                    <li><a href='#' onclick="twitter_shared_event();" class='twitter-f-color'><i class='fa fa-twitter-square'></i></a></li>
                    <li><a href='#' onclick="gplus_shared_event();" class='gplus-f-color'><i class='fa fa-google-plus-square'></i></a></li> -->
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- ============================= MODAL SHARE POST GROUP ======================================== -->
<div id='sharedPostGroupModal' class='vh-center modal fade' >
  <div class='modal-dialog' style ='display: inline-block;text-align:left; vertical-align:middle;'>
    <div class='modal-content'>
      <div class='modal-header color-gj-popup-report'>
        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
        <h4 class='modal-title'>Share Single Post Group </h4>
      </div>
      <div class='modal-body'>
        <div class='row'>
          <div class='col-sm-12'>
            <div class='warp-opsi-report'>
              <div class='share-med-soc-gj'>
                <ul>
                    <li><a href='#' class='facebook-f-color'><i class='fa fa-facebook-square'></i></a></li>
                    <li><a href='#' class='twitter-f-color'><i class='fa fa-twitter-square'></i></a></li>
                    <li><a href='#' class='gplus-f-color'><i class='fa fa-google-plus-square'></i></a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- ===================== MODAL JOIN GROUP EVENT  ===================  -->
<div id='modalJoin-groupEvent' class='vh-center modal fade'>
    <div class='modal-dialog modal-sm'>
        <div class='modal-content'>
            <div class='modal-header color-gj-popup-report'>
                    <button type='button' class='close closeModalJoin-groupEvent' data-dismiss='modal' aria-hidden='true'>&times;</button>
                    <h4 class='modal-title'>Join event</h4>
            </div>

            <div class='modal-body'>
                <div class='row'>
                    <div class='col-sm-12'>
                         <span id='groupID-groupEvent' style='display:none;'></span>
                <span id='groupEventID-groupEvent' style='display:none;'></span>
                <span id='trackID-groupEvent' style='display: none;'></span>
                    
                   <p>To join this event <strong>"<span id='eventTitle-groupEvent'></span>"</strong>
                   you should buy the assigned backtrack below :</p>
                   <div class='row'>
                        <div class='col-sm-12'> 
                            <p>Title : <span id='judulTrack-groupEvent'></span></p>
                            <p>Instrument : <span id='instrumentTrack-groupEvent'></span></p>
                            <p>Artist : <span id='creatorTrack-groupEvent'></span></p>
                            <p>Price : <span id='priceTrack-groupEvent'></span></p>
                            <p>Are you sure to join event ?</p>
                        </div>
                     </div>   
                    </div>
                 </div>   
            </div>

            <div class='modal-footer'>
                <span class='loader-process' style='display: none;'><img src='{{asset('img/loader-24.gif')}}' /></span>
                <a href='#' class='btn-s-gj bgc-btn closeModalJoin-groupEvent' data-dismiss='modal'>Cancel</a>
                <a id='buttonProsesJoinEvent' href='javascript:void(0);' onclick='prosesBuyTrackGroupEvent();' class='btn-s-gj bgc-btn'>Process</a>
            </div>

        </div>
    </div>
</div>
<!-- ==================== MODAL JOIN PROJECT INVITATION ================== -->
<div id='modalJoin-Invitation' class='vh-center modal fade'>
  <div class='modal-dialog modal-sm'>
    <div class='modal-content'>
      <div class='modal-header color-gj-popup-report'>
        <button type='button' class='close closeModalJoin-ProjectInvitation' data-dismiss='modal' aria-hidden='true'>&times;</button>
        <h4 class='modal-title'>Join project invitation</h4>
      </div>
      <div class='modal-body'>
        <div class='row'>
          <div class='col-sm-12'>
            <span id='invitation-explore-projectID' style='display:none;'></span>
            <p>Apakah anda yakin untuk join pada project invite ini</p>
          </div>
        </div>
      </div>
      <div class='modal-footer'>
        <span class='loader-process' style='display: none;'><img src='{{asset('img/loader-24.gif')}}' /></span>
        <a href='#' class='btn-s-gj bgc-btn closeModalJoin-ProjectInvitation' data-dismiss='modal'>No</a>
        <a id='buttonProsesJoinProjectInvitation' href='javascript:void(0);' onclick='prosesJoinProjectInvitation();' class='btn-s-gj bgc-btn'>Yes</a>
      </div>
    </div>
  </div>
</div>
<!-- ============================================================= MODAL EMAIL EVENT WINNER =================================================================== -->
<div id='emailTheWinner' class='modal vh-center fade'>
  <div class='modal-dialog'>
    <div class='modal-content'>
      <div class='modal-header color-gj-popup-report'>
        <button type='button' class='close modal-emailTheWinner' data-dismiss='modal' aria-hidden='true'>&times;</button>
        <h4 class='modal-title'>Send Email</h4>
      </div>
      <div class='modal-body'>
        <div class='row'>
          <div class='col-sm-12'>
            <!-- <form> -->
            <span id='input_username_winner' style='display:none;'></span>
            <span id='input_username_admin' style='display:none;'></span>
            <span id='input_event_title' style='display:none;'></span>
              <div class='form-group'>
                <label>Email address</label>
                <input type='email' class='form-control' placeholder='Enter email' id='input_email_winner' name='input_email_winner' disabled='disabled'>
              </div>
              <div class='input-group warp-input-src-market-gj'>
                <p>Text</p>
                <textarea class='txt-area-off' id='input_text_area'></textarea>
                <button id='buttonSendEmail' type='submit' class='btn btn-primary' onclick='sendEmailToEventWinner();'>Send</button>
                 <span class='loader-process' style='display: none;'><img src='{{asset('img/loader-24.gif')}}' /></span>
              </div>
            <!-- </form> -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>   
<!-- ============================================================= MODAL VOTE EVENT WINNER =================================================================== -->
<div id='voteTheWinner' class='vh-center modal fade'>
  <div class='modal-dialog modal-sm'>
    <div class='modal-content'>
      <div class='modal-header color-gj-popup-report'>
        <button type='button' class='close closevoteTheWinner' data-dismiss='modal' aria-hidden='true'>&times;</button>
        <h4 class='modal-title'>Vote the winner</h4>
      </div>
      <div class='modal-body'>
        <div class='row'>
          <div class='col-sm-12'>
            <span id='eventID_voteWinner' style='display:none;'></span>
            <span id='trackID_voteWinner' style='display:none;'></span>
            <p>Are you sure vote this song as winner event</p>
          </div>
        </div>
      </div>
      <div class='modal-footer'>
        <span class='loader-process' style='display: none;'><img src='{{asset('img/loader-24.gif')}}' /></span>
        <a href='#' class='btn-s-gj bgc-btn closevoteTheWinner' data-dismiss='modal'>No</a>
        <a id='buttonProsesVoteWinner' href='javascript:void(0);' onclick='prosesVoteWinnerEvent();' class='btn-s-gj bgc-btn'>Yes</a>
      </div>
    </div>
  </div>
</div>
<!-- ================== MODAL ADMIN LEAVE GROUP JIKA TIDAK ADA MEMBER LAIN =============== -->
<div id="modalAdminLeaveGroupTidakAdaMemberLain" tabindex="-1" role="dialog" aria-hidden="true" class="modal vh-center fade">
  <div class="modal-dialog modal-sm" style="width: 400px">
    <div class="modal-content">
      <div class="modal-body nopadding">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sxs-12">
            <div class="warp-crt-add-playlist">
              <div class="modal-header color-gj-popup-report">
                <button type="button" class="close close-button" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Leave Group </h4>
              </div>
              <div class="warp-15">
                <div>Not found other member, if you are leave group, this group will deleted</div>
                <div>Are you sure to leave group : <strong><span id="data-groupname-adminLeave2"></span></strong> ?</div>
                <span id="data-groupid-adminLeave2" style="display: none;"></span>
                <span id="data-picture-adminLeave2" style="display: none;"></span>
                <span id="data-bg-picture-adminLeave2" style="display: none;"></span>
                <br>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class='modal-footer'>
        <span class='loader-process' style='display: none;'><img src='{{asset('img/loader-24.gif')}}' /></span>
        <a href="#" class="btn-s-gj bgc-btn close-button" data-dismiss="modal">Cancel</a>
        <a href="#" class="btn-s-gj bgc-btn process-admin-leave-group-memberNull" id="admin-leave-group-memberNull">YES</a>
      </div>
    </div>
  </div>
</div>
<!-- =================== MODAL MEMBER LEAVE GROUP ==================== -->
<div id="modalMemberLeaveGroup" tabindex="-1" role="dialog" aria-hidden="true" class="modal vh-center fade">
  <div class="modal-dialog modal-sm" style="width: 400px">
    <div class="modal-content">
      <div class="modal-body nopadding">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sxs-12">
            <div class="warp-crt-add-playlist">
              <div class="modal-header color-gj-popup-report">
                <button type="button" class="close close-button" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Leave Group </h4>
              </div>
              <div class="warp-15">
                <div>Are you sure to leave group : <strong><span id="data-groupname-memberLeave"></span></strong> ?</div>
                <span id="data-groupid-memberLeave" style="display: none;"></span>
                <span id="data-picture-memberLeave" style="display: none;"></span>
                <span id="data-bg-picture-memberLeave" style="display: none;"></span>
                <span id="data-username-memberLeave" style="display: none;"></span>
                <br>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class='modal-footer'>
        <span class='loader-process' style='display: none;'><img src='{{asset('img/loader-24.gif')}}' /></span>
        <a href="#" class="btn-s-gj bgc-btn close-button" data-dismiss="modal">Cansel</a>
        <a href="#" class="btn-s-gj bgc-btn process-member-leave-group" id="member-leaveGroup">YES</a>
      </div>
    </div>
  </div>
</div>
<!-- ====================== MODAL REMOVE PENDING PROJECT===================== -->
<div id="modalRemovePendingProject" class="vh-center modal fade">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header color-gj-popup-report">
       <button type="button" class="close project-pending-confirmation-close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Delete pending project confirmation</h4>
      </div>
      <div class="modal-body">
        <span id="removePendingProject-projectID" style="display:none; margin-right:5px;"></span>
        <div class="row">
          <div class="col-sm-12">
            <p>Are you sure want to remove your pending project</p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <span class="loader-process" style="display: none;margin-right: 5px;"><img src="{{asset('img/loader-24.gif')}}" /></span>
        <a href="#" class="btn-s-gj bgc-btn project-pending-confirmation-close" data-dismiss="modal" style="margin-right: 5px;">Cancel</a>
        <a href="#" class="btn-s-gj bgc-btn process-remove-pending-project" id="button-remove-project">Yes</a>
      </div>
    </div>
  </div>
</div>
<!-- ====================== MODAL MODAL SHARED GROUP PROFILE ===================== -->
<div id="modalmyShareGroupProfile" class="vh-center modal fade">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header color-gj-popup-report">
       <button type="button" class="close close-button-shared-group-profile" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Share my group profile</h4>
      </div>
      <div class="modal-body">
        <span id='group_name-shared-GroupProfile' style='display:none;'></span>
        <span id='group_picture-shared-GroupProfile' style='display:none;'></span>
        <span id='group_description-shared-GroupProfile' style='display:none;'></span>
        <span id= 'group_id-shared-GroupProfile' style='display:none;'></span>
        <div class="row">
          <div class="col-sm-12">
            <p>Are you sure want to shared group profile ?</p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <span class="loader-process" style="display: none;margin-right: 5px;"><img src="{{asset('img/loader-24.gif')}}" /></span>
        <a href="#" class="btn-s-gj bgc-btn close-button-shared-group-profile" data-dismiss="modal" style="margin-right: 5px;">Cancel</a>
        <a href="#" class="btn-s-gj bgc-btn process-shared-group-profile" id="button-shared-group-profile">Yes</a>
      </div>
    </div>
  </div>
</div>
<!-- ====================== MODAL JOIN REQUEST GROUP ===================== -->
<div id="modalJoinRequstGroup" class="vh-center modal fade">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header color-gj-popup-report">
       <button type="button" class="close close-button-join-request-group" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Join Request Group</h4>
      </div>
      <div class="modal-body">
        <span id='group_id-join-request-group' style='display:none;'></span>
        <span id='group_username-request-group' style='display:none;'></span>
        <div class="row">
          <div class="col-sm-12">
            <p>Are you sure want to join in group <strong>"<span id="group_name-join-request-group"></span>"</strong> ?</p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <span class="loader-process" style="display: none;margin-right: 5px;"><img src="{{asset('img/loader-24.gif')}}" /></span>
        <a href="#" class="btn-s-gj bgc-btn close-button-join-request-group" data-dismiss="modal" style="margin-right: 5px;">Cancel</a>
        <a href="#" class="btn-s-gj bgc-btn process-join-request-group" id="button-join-request-group">Yes</a>
      </div>
    </div>
  </div>
</div>
<!-- ============================= MODAL CONFIRM GROUP MEMBER Pending=========================================== -->   
<div id="modalConfirmGroupMemberPending" class="vh-center modal fade">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header color-gj-popup-report">
       <button type="button" class="close close-button-confirm-group" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Confirm Group</h4>
      </div>
      <div class="modal-body">
         <span id="data-groupid-pending-confirm" style="display: none;"></span>
         <span id="data-groupname-pending-confirm" style="display: none;"></span> 
         <span id='data-username-pending-confirm' style='display: none;'></span>
         <span id="data-userid-pending-confirm" style='display: none;'></span>
        <div class="row">
          <div class="col-sm-12">
            <p>Are you sure to confirm group <strong>"<span id="data-groupname-pending-confirm-1"></span>"</strong> ?</p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <span class="loader-process" style="display: none;margin-right: 5px;"><img src="{{asset('img/loader-24.gif')}}" /></span>
        <a href="#" class="btn-s-gj bgc-btn close-button-confirm-group" data-dismiss="modal" style="margin-right: 5px;">Cancel</a>
        <a href="#" class="btn-s-gj bgc-btn process-confirm-group-member-pending" id="button-confirm-group-member-pending">Yes</a>
      </div>
    </div>
  </div>
</div>