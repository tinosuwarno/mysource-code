<nav class="navbar navbar-bootsnipp motion-animate navbar-gritjam">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#gritjam-navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="warp-logo">
                    <a class="logo-gritjam motion-animate" href="{{URL::to('/')}}"><img src="{{asset('/img/logoweb1.png')}}"></a>
                </div>
            </div>
            
            <!-- Collect the nav links, forms, and other content for toggling -->
            <!-- <div class="collapse navbar-collapse" id="gritjam-navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="visible-xs">
                        <!-- search form saat display apda HANDPHONE -->
                       <!--  <form action="" method="GET" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" name="q" placeholder="Search for Jam">
                                <span class="input-group-btn">
                                    <button class="btn btn-primary" type="submit"><span class="glyphicon glyphicon-search"></span></button>
                                    <button class="btn btn-danger" type="reset"><span class="glyphicon glyphicon-remove"></span></button>
                                </span>
                            </div>
                        </form>
                        <!-- search form saat display apda HANDPHONE -->
                    <!-- </li>
                    <li><a href="{{URL::to('/')}}" class="motion-animate">Home</a></li>
                    <li><a href="#" class="motion-animate">Explore</a></li>
                    <li><a href="#" class="motion-animate">Track Bank</a></li>
                    <li><a class="motion-animate" href="#">Market</a></li>
                    <li><a class="motion-animate" href="#">Playground</a></li>
                    <!-- <li class="hidden-xs"><a href="#toggle-search" class="motion-animate search-menu"><span class="glyphicon glyphicon-search"></span></a></li> -->
                   <!--  <li>
                        <div class="hidden-xs hidden-sm search-gj">
                          <!-- search form saat display apda DESKTOP -->
                         <!--  <form action="" method="" role="search">
                             <input type="search" placeholder="">
                             <button type="submit" class="btn btn-src-gj"><i class="fa fa-search"></i></button>
                          </form>
                          <!-- End search form saat display apda DESKTOP -->
                       <!--  </div>
                    </li>
                    <li><a class="motion-animate" href="#" data-toggle="modal" data-target="#reg-signModal">Sign In</a></li>
                </ul>
            </div> -->
            
            
            <div class="col-sm-12 pull-right hidden-xs hidden-md hidden-lg">
                <!-- form saat display pada TABLET -->
                <form class="navbar-form" role="search" action="">
                <div class="input-group warp-src-gj-sm">
                    <input type="text"  class=" input-src-sm-gj" placeholder="Search" name="" >
                    <button type="submit" class="btn btn-src-gj-sm"><i class="fa fa-search"></i></button>
                </div>
                </form>
                <!-- End form saat display pada TABLET -->
             </div>
        </div> 
        <div class="bootsnipp-search motion-animate">
            <div class="container">
                <form action="" method="GET" role="search">
                    <div class="input-group gritjam-search-h">
                        <input type="text" class="form-control" name="q" placeholder="Search for Jam">
                        <span class="input-group-btn">
                            <button class="btn btn-danger" type="reset"><span class="glyphicon glyphicon-remove"></span></button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
    </nav>