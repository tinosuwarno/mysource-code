<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>
    @yield('title')
</title>

@include('includes.css')

@include('includes.js-karaoke')

<link href="{{asset('img/favicon.ico')}}" rel="icon" type="image/x-icon">

@yield('styles')
<link href="{{asset('css/karoke.css')}}" rel="stylesheet" type="text/css">
</head>

<body>
<div class="warp-header">

@include('layouts.navbar-top')
</div>

<div class="pjaxContent">
    @yield('contents')
    <!-- <div id='loadingPage'>
        <div class='col-xs-push-5 col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 lazyloader-list' id='loadingMarket3'>
            <img src='img/loader-more.gif' class='img-full-responsive e-centering width-loader'>
        </div>
    </div> -->
</div>



<!-- Modal -->
@include('layouts.modal')
<!-- ========================================= LOADER WAITING ==================================================== -->
       <div id="loader-wrapper" style="display: none;">
           <div class="warp-c-go">
                <div id="loader-block"></div>
                <div class="go-loader"><div class="logo-loader"><img src="{{asset('img/ico-gj.png')}}"></div></div>
            </div>
       </div>
<!-- ===================================== END LOADER WAITING ==================================================== -->
<script>
    $(document).on('ready pjax:success', function(){
        if($.support.pjax){
            $(document).pjax('#pjax[data-pjax]', '.pjaxContent', {timeout: 100000});
            $(document).pjax('#pjaxDemo[data-pjax]', '.pjaxDemoContent', {timeout: 100000});
        }
    });
    $(document).on('pjax:send', function() {
      $('#loader-wrapper').show()
    })
    $(document).on('pjax:complete', function() {
      $('#loader-wrapper').hide()
    })
</script>
<section class='warp-player-music sticky-position-player' style='display:none;' id='playerJWplayer'>
    <div class='container'>
        <div class='row'>
            <div class='player-interface'>
                <div class='warp-smaller'>
                    <div class='warp-player-ctrl'>

                        <div id='gjaudioplayer' style='color:#fff;'>Loading the player…</div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('layouts.footer')

@include('includes.js-bottom')

<?php 
    $server = $_SERVER['REQUEST_URI'];
    $path = explode('/', $server);

   if($path[1] == 'studio' && !isset($path[2])){
?>

@include('includes.js-jwplayer')

<?php } ?>

@yield('scripts')
<!-- Store -->
<script type="text/javascript" src='{{asset('js/countFunction.js')}}'></script>
<script type='text/javascript' src='{{asset('js/marketJS/demo.js')}}'></script>

<!-- Explore -->
<script type='text/javascript' src='{{asset('js/explore/friendExplore.js')}}'></script>

<!-- Friend Wall (username) -->
<script type="text/javascript" src='{{asset('js/friendTimeline/friendProject.js')}}'></script>
<script type="text/javascript" src='{{asset('js/friendTimeline/friendProfile.js')}}'></script>
<script type="text/javascript" src='{{asset('js/friendTimeline/getTrack.js')}}'></script>
<script type="text/javascript" src='{{asset('js/friendTimeline/friendWall.js')}}'></script>

<!-- TimeLine -->
<script type="text/javascript" src='{{asset('js/timeline/newGetPost.js')}}'></script>
<script type="text/javascript" src='{{asset('js/jquery.sidr.js')}}'></script>
<script type="text/javascript" src='{{asset('js/config-sidr-sticky-wall.js')}}' type='text/javascript'></script>
<script type="text/javascript" src='{{asset('js/timeline/getGroup.js')}}'></script>
<script type="text/javascript" src='{{asset('js/timeline/getPost.js')}}'></script>
<script type="text/javascript" src='{{asset('js/timeline/getComment.js')}}'></script>
<script type="text/javascript" src='{{asset('js/timeline/getTrack.js')}}'></script>
<script type="text/javascript" src='{{asset('js/timeline/getMybio.js')}}'></script>
<script src="{{asset('js/timeline/SinglePostShare.js')}}"></script>

<script src="{{asset('js/TrackBank/trackbank.js')}}"></script>
<script src="{{asset('js/TrackBank/searchtrackbank.js')}}"></script>
<script src="{{asset('js/TrackBank/trackbank_new.js')}}"></script>
<script type="text/javascript" src="{{asset('js/TrackBank/allTrackVersion.js')}}"></script>
<script type="text/javascript" src="{{asset('js/group/group.js')}}"></script>
<script type="text/javascript" src="{{asset('js/group/setCoverGroup.js')}}"></script>
<script type="text/javascript" src="{{asset('js/group/singlepostGroup.js')}}"></script>
<script type="text/javascript" src="{{asset('js/group/event/event.js')}}"></script>


<script src="{{asset('js/timeline.js')}}"></script>
<!-- <script src="{{asset('js/timeline-friend.js')}}"></script> -->
<script src="{{asset('js/setprofile.js')}}"></script>
<script src="{{asset('js/setting.js')}}"></script>
<script src='{{asset('js/aboutactivity.js')}}'></script>

<!-- Friend Timeline -->
<script type="text/javascript" src='{{asset('js/friend/postcontent.js')}}'></script>
<script type="text/javascript" src='{{asset('js/friend/content.js')}}'></script>
<script type="text/javascript" src='{{asset('js/friend/search.js')}}'></script>
<script type="text/javascript" src='{{asset('js/friend/status.js')}}'></script>
<!-- End Friend Timeline -->
</body>
</html>
