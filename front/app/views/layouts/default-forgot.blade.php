<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>GritJam</title>
@include('includes.css')

@include('includes.js-karaoke')
<link href="{{asset('img/favicon.ico')}}" rel="icon" type="image/x-icon">
</head>

<body>
<div class="warp-header">

@include('layouts.navbar-top')
</div>
@yield('contents')
<div class="block-white"></div>
@include('layouts.footer')

@include('includes.js-bottom')
<!-- <script src="{{asset('js/bootstrap.js')}}" type="text/javascript"></script> -->
<!-- <script src="{{asset('js/all-functions.js')}}" type="text/javascript"></script>
<script type="text/javascript" src='https://cdn.jsdelivr.net/sweetalert2/4.2.6/sweetalert2.min.js'></script> -->


</body>
</html>
