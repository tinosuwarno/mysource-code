<style type="text/css">
    .btn-warning {
        background-repeat: repeat-x;
        border-color: #e38d13;
        float: right;
    }

    .input-group .form-control {
    position: relative;
    z-index: 2;
    float: left;
    width: 100%;
    margin-bottom: 0;
}
</style>
<footer class="footer">
	<div class="warp-footer">
    	<div class="container">
        	<div class="row">
            	<div class="col-lg-8 col-md-7 col-sm-6 col-xs-12 col-sxs-12">
                   <div class="warp-med-soc">
                       <h4>Connect with Us</h4>
                        <div class="med-soc-gj">
                           <ul>
                               <li><a href="https://www.facebook.com/Gritjam-620209208118029/" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                                <li><a href="https://www.twitter.com/gritjam" target="_blank"><i class="fa fa-twitter-square"></i></a></li>
                                <li><a href="https://www.youtube.com/channel/UCj-lU7JS1bLmFyyfVA4nSzg" target="_blank"><i class="fa fa-youtube-square"></i></a></li>
                                <li><a href="https://plus.google.com/111831019452125155049" target="_blank"><i class="fa fa-google-plus-square"></i></a></li>
                                <li><a href="https://www.instagram.com/gritjam" target="_blank"><i class="fa fa-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-5 col-sm-6 col-xs-12- col-sxs-12">
                   <div class="warp-subscribe-form">
                    <?php
                        $return = Session::get('data');
                    ?>
                    @if($return) 
                    @else   
                       <h4>Be the First to know when we launch</h4>


                <div class="subscribe-form">
                            {{ Form::open( array(
                                'url' => 'subscribe',
                                'method' => 'post',
                                'id' => 'contact_form',
                                'class' => 'form-horizontal'
                            ) ) }}
                                    <fieldset>
                                        <!-- Text input-->
                                        <div class="form-group">

                                            <div class="col-md-12">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                                    <input id="emailku" name="email" placeholder="E-Mail Address" class="form-control" type="text">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">

                                            <div class="col-md-12">
                                                <span class="loader-process" style="display: none;"><img src="{{asset('img/loader-24.gif')}}" /></span>
                                                <a href="#" class="btn bgc-btn c-f-btn process-subscribe">Send <span class="glyphicon glyphicon-send"></span></a>
                                            </div>
                                        </div>

                                    </fieldset>
                                {{ Form::close() }}
                </div>
                @endif
                    </div>
            </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="warp-copyright">
    	<div class="container">
        	<div class="row">
            	<div class="col-lg-12">
                <div class="footer-menu">
                       <ul>
                            <li><a href="#">Features</a></li>
                            <li><a href="#">Help & Contact</a></li>
                            <li><a href="#">Term and Condition of Use</a></li>
                            <li><a href="#">Personal Data & Cookies</a></li>
                        </ul>
                        <p>Gritjam All Rights Reserved © 2016</p>
                    </div>
                </div>
        	</div>
        </div>
    </div>
</footer>


<!-- ============================== MODAL SHARE SINGLE POST ========================== -->
<div id="shareSinglePost" class="vh-center modal fade" >
  <div class="modal-dialog" style ="display: inline-block;text-align:left; vertical-align:middle;">
    <div class="modal-content">
      <div class="modal-header color-gj-popup-report">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Share Single Post</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-12">
            <span id="data-postID-fromWall" style="display: none;"></span>
            <div class="warp-opsi-report">
              <div class="share-med-soc-gj">
                <ul>
                   <li><a href='#' class='facebook-f-color'><i class='fa fa-facebook-square'></i></a></li>
                    <li><a href='#' class='twitter-f-color'><i class='fa fa-twitter-square'></i></a></li>
                    <li><a href='#' class='gplus-f-color'><i class='fa fa-google-plus-square'></i></a></li>
                    
                  <!-- <li><a href="javascript:void(0);" onclick="SinglePostSharedFB();" class="facebook-f-color"><i class="fa fa-facebook-square"></i></a></li> -->
                  <!-- <li><a href="javascript:void(0);" onclick="SinglePostSharedTwitter();" class="twitter-f-color"><i class="fa fa-twitter-square"></i></a></li> -->
                  <!-- <li><a href="javascript:void(0);" onclick="SinglePostSharedTumblr();" class="tumblr-f-color"><i class="fa fa-tumblr-square"></i></a></li> -->
                  <!-- <li><a href="javascript:void(0);" onclick="SinglePostSharedGPlus();" class="gplus-f-color"><i class="fa fa-google-plus-square"></i></a></li> -->
                  <!-- <li><a href="javascript:void(0);" onclick="SinglePostSharedPinterest();" class="pinterest-f-color"><i class="fa fa-pinterest-square"></i></a></li> -->
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- ============================= MODAL CONFIRM GROUP MEMBER =========================================== -->   
<div id="modalConfirmGroupMember" class="vh-center modal fade">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header color-gj-popup-report">
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Confirm Group <span id="data-groupname-confirm"></span> </h4>
      </div>
      <div class="modal-body">
         <span id="data-groupid-confirm" style="display: none;"></span>
         <span id="data-username-confirm" style="display: none;"></span>
        <div class="row">
          <div class="col-sm-12">
            <p>Are you sure to confirm group <strong>"<span id="data-groupname-confirm-1"></span> "</strong> ?</p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <span class="loader-process" style="display: none;margin-right: 5px;"><img src="{{asset('img/loader-24.gif')}}" /></span>
        <a href="#" class="btn-s-gj bgc-btn process-not-interest-group-member" id='button-reject-group-member' style="margin-right: 5px; background-color: #B22222;">Reject</a>
        <a href="#" class="btn-s-gj bgc-btn process-confirm-group-member" id="button-confirm-group-member">Accept</a>
      </div>
    </div>
  </div>
</div>
<!-- ============================= CANCEL REQUEST JOIN GROUP ============================================= --> 
<div id="userCancelJoinGroup" class="vh-center modal fade">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header color-gj-popup-report">
       <button type="button" class="close close-button-cancel-request-group" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Cancel Your Request Join to Group</h4>
      </div>
      <div class="modal-body">
         <span id="data-groupid-request-cancel" style="display: none;"></span>
         <span id="data-memberid-request-cancel" style="display: none;"></span>
         <span id="data-myusername-request-cancel" style="display: none"></span>
        <div class="row">
          <div class="col-sm-12">
            <p>Are you sure to cancel your request join in group <strong>"<span id="data-groupname-request-cancel"></span>"</strong> ?</p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <span class="loader-process" style="display: none;margin-right: 5px;"><img src="{{asset('img/loader-24.gif')}}" /></span>
        <a href="#" class="btn-s-gj bgc-btn close-button-cancel-request-group" data-dismiss="modal" style="margin-right: 5px;">Cancel</a>
        <a href="#" class="btn-s-gj bgc-btn process-cancel-request-join" id="button-cancel-request-group">Yes</a>
      </div>
    </div>
  </div>
</div>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-73233137-1', 'auto');
  ga('send', 'pageview');

</script>
<script type="text/javascript">
    $('.process-subscribe').click(function(){
        $(".loader-process").show();

        var emailku = $('#emailku').val();
        console.log(emailku);

        $.ajax({
            url: 'subscribe',
            type: 'POST',
            data: { email: emailku},
            dataType: 'json',
            success: function(result){
              $(".loader-process").hide();
              if (result == 1) {
                swal({ 
                  title: "Thank you!",
                   text: "We will contact you when we finally launch the app :D",
                    type: "success",
                    allowOutsideClick: false,
                  });

                $('.swal2-confirm').click(function(){
                    window.location.href = window.location.origin;
                });
              } else{
                swal('Oops...','You have already subscribed, thanks','error');
              }
            },
            error: function(jqXHR, textStatus, errorThrown){
              console.log(jqXHR);
              $(".loader-process").hide();
              swal('Oops...','Something went wrong!','error');
          }

        });
    });
</script>

