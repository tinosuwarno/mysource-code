<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>
    @yield('title')
</title>

@include('includes.css')

@include('includes.js-karaoke')

<script src="{{asset('js/jwplayer/jwplayer7.js')}}"></script>
<script> jwplayer.key="1I4Td5GO5Vv3qqMfm954PkUhpgbS8bxwlmO20N0Pcx4="; </script>

<link href="{{asset('img/favicon.ico')}}" rel="icon" type="image/x-icon">

@yield('styles')
<!-- <link rel="stylesheet" type="text/css" href="{{asset("css/jwplayerSkin.css")}}"> </link> -->
</head>

<body>
<div class="warp-header">

@include('layouts.navbar-top')
</div>

<div class="pjaxContent">
    @yield('contents')
    <!-- <div id='loadingPage'>
        <div class='col-xs-push-5 col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 lazyloader-list' id='loadingMarket3'>
            <img src='img/loader-more.gif' class='img-full-responsive e-centering width-loader'>
        </div>
    </div> -->
</div>

<div class='clearfix'></div>
<div class='block-white'></div>


<script>
    $(document).on('ready pjax:success', function(){
        if($.support.pjax){
            $(document).pjax('#pjax[data-pjax]', '.pjaxContent', {timeout: 100000});
            $(document).pjax('#pjaxDemo[data-pjax]', '.pjaxDemoContent', {timeout: 100000});
        }
    });
    $(document).on('pjax:send', function() {
      $('#loader-wrapper').show()
    });
    $(document).on('pjax:complete', function() {
      $('#loader-wrapper').hide()
    });
    // $(document.body).on('submit', '.navbar-form', function(event){
    //     console.log('here');
    //     $.pjax.submit(event, '.pjaxContent');
    //     event.preventDefault();
    //     event.stopImmediatePropagation();
    // });
    $(document).on('submit', '#formNavbar', function(event) {
        console.log('here');
        event.preventDefault();
        event.stopImmediatePropagation();
        $.pjax.submit(event, '.pjaxContent');
    });
</script>

@include('layouts.footer')
    
@include('includes.js-bottom')



@yield('scripts')
<!-- Explore -->
<script type='text/javascript' src='{{asset('js/explore/friendExplore.js')}}'></script>

<!-- Store -->
<script type="text/javascript" src='{{asset('js/countFunction.js')}}'></script>

<!-- TimeLine -->
<script type="text/javascript" src='{{asset('js/jquery.sidr.js')}}'></script>
<script type="text/javascript" src='{{asset('js/config-sidr-sticky-wall.js')}}' type='text/javascript'></script>
</body>
</html>
