<nav class="navbar navbar-fixed-top navbar-bootsnipp motion-animate navbar-gritjam" role="navigation">
    	<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#gritjam-navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<div class="warp-logo">
					<a class="logo-gritjam motion-animate" href="/"><img src="/img/logoweb1.png"></a>
				</div>
			</div>
            
			<!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="gritjam-navbar-collapse">
            <ul class="nav navbar-nav navbar-right list-menu-gj">
                <li class="visible-xs">
                    <!-- search form saat display apda HANDPHONE -->
                    <form action="" method="GET" role="search">
                        <div class="input-group">
                            <input type="text" class="form-control" name="q" placeholder="Search for Jam">
                            <span class="input-group-btn">
                                    <button class="btn btn-primary" type="submit"><span class="glyphicon glyphicon-search"></span></button>
                            <button class="btn btn-danger" type="reset"><span class="glyphicon glyphicon-remove"></span></button>
                            </span>
                        </div>
                    </form>
                    <!-- search form saat display apda HANDPHONE -->
                </li>
                <?php
                    $return = Session::get('data');
                ?>
                @if($return) 
                @else 
                <!-- <li><a data-pjax='yes' id='pjax' href="{{ url('home') }}" class="motion-animate">Home</a></li> -->
                @endif
                @if($return)
                <li><a data-pjax='yes' id='pjax' href="{{ url('explore') }}" class="motion-animate">Explore</a></li>
                <li><a data-pjax='yes' id='pjax' href="{{ url('TrackBank') }}" class="motion-animate">Track Bank</a></li>
                <li><a data-pjax='yes' id='pjax' class="motion-animate" href="{{ url('demo') }}">Demo</a></li>
                <!-- <li><a data-pjax='yes' id='pjax' class="motion-animate" href="{{ url('myWall') }}">Timeline</a></li> -->
                <!-- <li><a data-pjax='yes' id='pjax' class="motion-animate" href="{{ url('uploadFiles') }}">Upload</a></li> -->
                <li><a class="motion-animate" href="#">Studio</a></li>
                <!-- <li class="hidden-xs"><a href="#toggle-search" class="motion-animate search-menu"><span class="glyphicon glyphicon-search"></span></a></li> -->
                @endif
                <li>
                    <div class="hidden-xs hidden-sm search-gj">
                        <!-- search form saat display apda DESKTOP -->
                        <form action="" method="" role="search">
                            <input type="search" placeholder="">
                            <button type="submit" class="btn btn-src-gj"><i class="fa fa-search"></i></button>
                        </form>
                        <!-- End search form saat display apda DESKTOP -->
                    </div>

                    <!-- get username / name -->
                    <?php
                        // $return = Session::get('data');
                        $id = $return['id'];
                        $apiKey = $return['apiKey'];
                    ?>
                    @if($return)
                    <li><a data-pjax='yes' id='pjax' class="motion-animate" href="{{ url('myWall') }}"><?php
                        if(!empty($return['username'])){
                            echo $return['username'];
                        }else if(!empty($return['name'])){
                            echo $return['name'];
                        }else if(!empty($return['nickname'])){
                            echo $return['nickname'];
                        }else if(!empty($return['first_name'])){
                            echo $return['first_name'];
                        }
                        ?>
                           <span class="thumb-sm avatar pull-right m-t-n-sm m-b-n-sm m-l-sm">

                            <!-- profile image -->
                            <img src=<?php
                            if(!empty($return['profile_image'])){
                                $url = asset($return['profile_image']);
                                echo "\"" . $url . "\"";
                            }
                            else{
                                $url = asset('/img/avatar/avatar-300x300.png');
                                echo $url;
                            }
                            ?> class="img-circle img-responsive"></span> 
                    </a>

                    </li>
                    <li class="dropdown">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                        
                            <span class="caret"></span>
                            
                        </a> 
                        <span class="glyphicon glyphicon-chevron-down dropdown-toggle visible-xs sub-menu-mobile" data-toggle="dropdown"></span>
                        <ul class="dropdown-menu">
                            <li><a data-pjax='yes' id='pjax' href="{{ url('profile/following', $parameters = [$id], $secure = null); }}">My Profile</a></li>
                            <li><a data-pjax='yes' id='pjax' href="{{ url('myprofile', $parameters = [$id], $secure = null); }}">Edit Profile</a></li>
                            <li><a data-pjax='yes' id='pjax' href="{{ url('settings', $parameters = [$id], $secure = null); }}">Settings</a></li>
                            <li><a href="#">Sub Menu 3</a></li>
                            <li><a data-pjax='yes' id='pjax' href="#">Wishlist (<b>jumlah</b>)</a></li>
                            <li><a data-pjax='yes' id='pjax' href="#">Shopping Cart (<b><?php echo $return['credit'];?></b>)</a></li>
                            <li class="log-out-gj">
                            <a>
                            {{Form::open(['url' => '/logout-proses'])}}
                            <input type='hidden' name='apiKey' value=<?php echo $apiKey; ?>>
                            <input type='hidden' name='id' value=<?php echo $id; ?>>
                            <button type="submit" class="btn coba">Logout</button>
                            </form>
                            </a>
                            </li>
                        </ul>
                     </li>
                     @else
                      <li><a class="motion-animate" href="#" data-toggle="modal" data-target="#reg-signModal">Sign In</a></li>
                     @endif
            </ul>
        </div>
            
            
            <div class="col-sm-12 pull-right hidden-xs hidden-md hidden-lg">
            	<!-- form saat display pada TABLET -->
                <form class="navbar-form" role="search" action="">
                <div class="input-group warp-src-gj-sm">
                    <input type="text"  class=" input-src-sm-gj" placeholder="Search" name="" >
                    <button type="submit" class="btn btn-src-gj-sm"><i class="fa fa-search"></i></button>
                </div>
                </form>
                <!-- End form saat display pada TABLET -->
            </div>
		</div>
		<!-- <div class="bootsnipp-search motion-animate">
			<div class="container">
				<form action="" method="GET" role="search">
					<div class="input-group gritjam-search-h">
						<input type="text" class="form-control" name="q" placeholder="Search for Jam">
						<span class="input-group-btn">
							<button class="btn btn-danger" type="reset"><span class="glyphicon glyphicon-remove"></span></button>
						</span>
					</div>
				</form>
			</div>
		</div> -->
	</nav>
