<?php require_once(app_path() . '/library/timeline.php'); ?>
<!-- <nav class="navbar navbar-bootsnipp motion-animate navbar-gritjam" role="navigation"> -->
<nav class="navbar navbar-bootsnipp motion-animate navbar-gritjam">
<!-- <nav class="navbar navbar-fixed-top navbar-bootsnipp motion-animate navbar-gritjam" role="navigation"> -->
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#gritjam-navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            <div class="warp-logo">
                <a data-pjax='yes' id='pjax' class="logo-gritjam motion-animate" href="/"><img src="/img/logoweb1.png"></a>
            </div>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="gritjam-navbar-collapse">
            <ul class="nav navbar-nav navbar-right list-menu-gj">
                <li class="visible-xs">
                    <!-- search form saat display apda HANDPHONE -->
                    <form id='formNavbar' action="/searchAll" method="GET" role="search" data-pjax='yes'>
                        <div class="input-group">
                            <input type="text" class="form-control" name="queryTopBar" placeholder="Search for Jam">
                            <span class="input-group-btn">
                                    <button class="btn btn-primary" type="submit"><span class="glyphicon glyphicon-search"></span></button>
                            <button class="btn btn-danger" type="reset"><span class="glyphicon glyphicon-remove"></span></button>
                            </span>
                        </div>
                    </form>
                    <!-- search form saat display apda HANDPHONE -->
                </li>
                <?php 
                $return = Session::get('data');
                ?>
                @if($return)
                <?php
                    $id = $return['id'];
                    $apiKey = $return['apiKey'];

                    $data = [
                        'id' => $id,
                        'apiKey' => $apiKey
                    ];

                    $myGuzzle = new myGuzzle('myprofile/$id');
                    $myGuzzle->setHeader($apiKey);
                    $data = $myGuzzle->formParams($data, 'post');
                ?>
                @else 
                <!-- <li><a data-pjax='yes' id='pjax' href="{{ url('home') }}" class="motion-animate">Home</a></li> -->
                @endif
                
                <li><a data-pjax='yes' id='pjax' href="{{ url('explore') }}" class="motion-animate">Explore</a></li>
                <li><a data-pjax='yes' id='pjax' href="{{ url('track-bank') }}" class="motion-animate">Track Bank</a></li>
                <li><a data-pjax='yes' id='pjax' class="motion-animate" href="{{ url('demo') }}">Demo</a></li>
                <li><a data-pjax='yes' id='pjax' class="motion-animate" href="{{ url('events/new-event') }}">Event</a></li>
                <!-- <li><a data-pjax='yes' id='pjax' class="motion-animate" href="{{ url('myWall') }}">Timeline</a></li> -->
                <!-- <li><a data-pjax='yes' id='pjax' class="motion-animate" href="{{ url('uploadFiles') }}">Upload</a></li> -->
                <li><a class="motion-animate studio-ec" href="{{ url('studio') }}"><i class="fa fa-microphone"></i>Studio</a></li>
                <!-- <li class="hidden-xs"><a href="#toggle-search" class="motion-animate search-menu"><span class="glyphicon glyphicon-search"></span></a></li> -->
                
                <li>
                    <div class="hidden-xs hidden-sm search-gj">
                        <!-- search form saat display apda DESKTOP -->
                        <form id='formNavbar' action="/searchAll" method="GET" role="search" data-pjax='yes'>
                            <input type="search" name="queryTopBar" placeholder="">
                            <button type="submit" class="btn btn-src-gj"><i class="fa fa-search"></i></button>
                        </form>
                        <!-- End search form saat display apda DESKTOP -->
                    </div>

                    <!-- get username / name -->
                    
                    @if($return)
                    <li class="dropdown"><a data-pjax='yes' id='pjax' class="motion-animate" href="{{ url('myWall') }}"><?php
                        if(!empty($data['username'])){
                            echo $data['username'];
                        }else if(!empty($data['name'])){
                            echo $data['name'];
                        }else if(!empty($data['nickname'])){
                            echo $data['nickname'];
                        }else if(!empty($data['first_name'])){
                            echo $data['first_name'];
                        }
                        ?>
                           <span class="thumb-sm avatar pull-right m-t-n-sm m-b-n-sm m-l-sm">

                            <!-- profile image -->
                            <img src=<?php
                            if(!empty($data['profile_image'])){
                                $url = asset($data["profile_image"]);
                                echo "\"" . $url . "\"";
                            }
                            else{
                                $url = asset('/img/avatar/avatar-300x300.png');
                                echo $url;
                            }
                            ?> class="img-circle img-responsive"></span> 
                    </a>

                    </li>
                    <li class="dropdown">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                        
                            <span class="glyphicon glyphicon-menu-down"></span>
                            
                        </a> 
                        <span class="glyphicon glyphicon-chevron-down dropdown-toggle visible-xs sub-menu-mobile" data-toggle="dropdown"></span>
                        <ul class="dropdown-menu">
                            <li><a data-pjax='yes' id='pjax' href="{{ url('profile/following', $parameters = [$id], $secure = null); }}">My Profile</a></li>
                            <li><a href="{{ url('settings', $parameters = [$id], $secure = null); }}">Account Setting</a></li>
                            <li><a href="/message">Message</a></li>
                            <!-- <li><a href="#">Sub Menu 3</a></li> -->
                            <li><a data-pjax='yes' id='pjax' href="#">Wishlist (<b>0</b>)</a></li>
                            <li><a data-pjax='yes' id='pjax' href="{{url('playlist/myPlaylist')}}">My Playlist</a></li>
                            <li><a data-pjax='yes' id='pjax' href="#" class="current-balance">Balance (<b><?php echo 'IDR '.number_format($return['credit'],0,'','.').',-';?></b>)</a></li>
                            <li><a data-pjax='yes' id='pjax' href="#" class="current-balance">Storage (<b><?php echo formatBytes($return['storage_left'], 2).' of '.formatBytes($return['storage_limit'], 2);?></b>)</a></li>
                            <li class="log-out-gj">
                            <a>
                            {{Form::open(['url' => '/logout-proses'])}}
                            <input type='hidden' name='apiKey' value=<?php echo $apiKey; ?>>
                            <input type='hidden' name='id' value=<?php echo $id; ?>>
                            <button type="submit" class="btn coba">Logout</button>
                            </form>
                            </a>
                            </li>
                        </ul>
                     </li>

                     <?php
                     
                        // Get Notifications

                        $notif = new NotificationsController();
                        $notifdata = $notif->getNotifications($apiKey, $id);

                        $countnotif = $notif->countUnreadNotif($notifdata, 'status');

                     ?>
                     <li class="dropdown">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                            <i class="fa fa-bell fa-notif"></i>
                                <span id="count-notif" class="badge badge-sm up bg-danger count" style="display: inline-block;"><?php echo $countnotif; ?></span>
                        </a>
                        <div class="dropdown-menu ">
                            <div class="list-notif-alt">
                            <?php if(sizeof($notifdata) == 0){ ?>
                                <div class="warp-no-notif">There is no Notif</div>
                             <?php } else{

                                // $countdatnotif = 0; 
                                $myApp = App::make('baseUrl'); 
                                $server = $myApp->baseUrl ;
                                foreach ($notifdata as $datnotif) { 
                                    // if ($countdatnotif < 5){
                                        if(strpos($datnotif['imageNotif'], 'img/') !== false){
                                            //$userimageurl =  $server.$datnotif['imageNotif'];
                                            $userimageurl =  $server.$datnotif['imageNotif'];
                                        } else if($datnotif['imageNotif'] == '') {
                                            $userimageurl = $server.'img/avatar/avatar-300x300.png';
                                        } else{
                                            $userimageurl = $datnotif['imageNotif'];
                                        }
                                    ?>

                                    <a id="<?php echo $datnotif['notificationID'].'-'.$datnotif['status']; ?>" href="<?php if(isset($datnotif['link_toID'])){echo url('/'.$datnotif['link_toID']);}else{echo '#';} ?>" class="list-notif" <?php if($datnotif['status'] == 0) echo 'style="background: #e9ebee;"' ?>>
                                        <span class="pull-left thumb-sm img-notif-pp">
                                            <img src="<?php echo $userimageurl; ?>" alt="..." class="img-circle img-responsive img-pp-notif">
                                        </span>
                                        <span class="notif-body block m-b-none">
                                            <?php echo $datnotif['content']; ?>
                                            <small class="text-muted"><?php echo time_elapsed_string_myWall($datnotif['notifCreated']) ?></small>
                                        </span>
                                    </a>
                             <?php 
                                    // }
                                    // $countdatnotif++;
                                }
                             } 
                             ?> 
                            </div>
                            <div class="warp-all-notif log-out-gj"><a href="#">View all Notification</a></div>
                        </div>

                        <!-- ========================== NOTIFIKASI BALOOOONN ========================================= -->
                             <div class="ballon-notif close-smt animated fadeIn" style="display:none;">
                                <div class="caret-notif-up"></div>
                                <i class="fa fa-close close-btn-rt" style="float:right;"></i>
                                <div class="list-notif-alt">
                                </div>
                             </div>
                        <!-- ========================== END NOTIFIKASI BALOOOONN ========================================= -->

                     </li>
                     @else
                      <li><a class="motion-animate" href="#" data-toggle="modal" data-target="#reg-signModal">Sign In</a></li>
                     @endif
            </ul>
        </div>


        <div class="col-sm-12 pull-right hidden-xs hidden-md hidden-lg">
            <!-- form saat display pada TABLET -->
            <form id='formNavbar' action="/searchAll" method="GET" role="search" data-pjax='yes' class="navbar-form">
                <div class="input-group warp-src-gj-sm">
                    <input type="text" class=" input-src-sm-gj" placeholder="Search" name="queryTopBar">
                    <button type="submit" class="btn btn-src-gj-sm"><i class="fa fa-search"></i></button>
                </div>
            </form>
            <!-- End form saat display pada TABLET -->
        </div>
    </div>
    <!-- <div class="bootsnipp-search motion-animate">
            <div class="container">
                <form action="" method="GET" role="search">
                    <div class="input-group gritjam-search-h">
                        <input type="text" class="form-control" name="q" placeholder="Search for Jam">
                        <span class="input-group-btn">
                            <button class="btn btn-danger" type="reset"><span class="glyphicon glyphicon-remove"></span></button>
                        </span>
                    </div>
                </form>
            </div>
        </div> -->



</nav>

<!-- ===================================================== MODAL REGISTER / SIGN IN ==================================================================== -->      
    <div  class="modal fade" id="reg-signModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header color-gj-popup-sign-in">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><img src="{{asset('/img/logoweb1.png')}}" class="img-responsive e-centering"></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                            <!-- Notifikasi ketika Error Emial atau Password--- -->
                            <!-- <div class="col-lg-12">
                            <div class="warp-error-sign-in-gj error-w">
                                <button id="closeWarning" type="button" class="close-warning" aria-hidden="true">&times;</button>
                                <div class="warning-error-sign-in">
                                    <span class="glyphicon glyphicon-remove-circle"></span>
                                    <p class="text-error-sign"> Your Email or Password are Invalid</p>
                                </div>
                            </div>
                            </div> 
                            <!-- eEND Notifikasi--- -->
                        <div class="col-lg-6 col-sm-12 col-md-6 col-xs-12 b-r">
                        <div class="warpper-login">
                              <h3 class="m-t-none m-b">Log in</h3>
                              {{ Form::open(array('url' => 'login-proses', 'class' => 'form-signin')) }}
                                <div class="form-group">
                                  {{ Form::text('inputEmail', Input::old('inputEmail'), array('id' => 'inputEmail', 'class' => 'form-control input-login', 'placeholder' => 'Email Address', 'required')) }}
                                  <!-- <input type="email" class="form-control input-login" placeholder="Enter Username"> -->
                                </div>
                                <div class="form-group">
                                  <input id="inputPassword" type="password" class="form-control input-login" name="inputPassword" placeholder="Password">
                                </div>
                                <div class="checkbox m-t-lg">
                                    <a href="{{URL::to('forgot-password')}}" class="forgot-pass-sign">Forgot Password</a>
                                </div>
                                <input type="submit" class="btn btn-sm col-btn-ex btn-n-f"></button>              
                              </form>
                        </div>
                        </div>
                        <div class="col-lg-6 col-sm-12 col-md-6 col-xs-12">
                        <div class="warpper-login">
                                <p class="crt-user">If you haven't had any account, please click the button below</p>
                                 <button class="btn btn-sm col-btn-ex btn-n-f btnCrt">Create Account</button>                                    
                                <h3 class="or">OR</h3>
                                <div class="warp-login-social">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <a class="btn-sign-in-mod-gj facebook-color" href="{{URL::to('login_oauth_fb')}}"><i class="fa fa-facebook"></i>Sign in with Facebook</a>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <a class="btn-sign-in-mod-gj gplus-color" href="{{URL::to('login_oauth_google')}}"><i class="fa fa-google-plus"></i>Sign in with Google +</a>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        </div>
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div  class="modal fade" id="crt-signModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header color-gj-popup-sign-in">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><img src="{{asset('/img/logoweb1.png')}}" class="img-responsive e-centering"></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <!-- Jika Field tidak terisi atau tidak valid -->
                        @if(Session::has('message_error'))
                           <div class="col-lg-12">
                            <div class="warp-error-sign-in-gj error-w">
                                <button id="closeWarning" type="button" class="close-warning" aria-hidden="true">&times;</button>
                                <div class="warning-error-sign-in">
                                    <span class="glyphicon glyphicon-remove-circle"></span>
                                    <p class="text-error-sign"> This field is not completed</p>
                                </div>
                            </div>
                           </div>
                         @endif
                        <!-- end tidak valid -->
                        <!-- Jika Field terisi lengkap dan Benar -->
                        @if(Session::has('message_success'))
                           <div class="col-lg-12">
                            <div class="warp-sucses-sign-in-gj sucses-w">
                                <button id="closeWarning" type="button" class="close-warning" aria-hidden="true">&times;</button>
                                <div class="warning-sucses-sign-in">
                                    <span class="glyphicon glyphicon-ok-circle"></span>
                                    <p class="text-sucses-sign"> Registration is completed. please check your email for activating your account.</p>
                                </div>
                            </div> 
                            </div>
                        @endif 
                        <!-- end valid -->
                        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 b-r">
                        <div class="warpper-login">
                              <h3 class="m-t-none m-b">Create New Account</h3>
                              <form action="user/store" method="post" role="form">
                                <div class="form-group">
                                    {{ Form::text('name', Input::old('name'), array('class' => 'form-control input-login', 'placeholder' => 'Type your name here', 'required')) }}<span class="req-w">*</span>
                                </div>
                                <div class="form-group">
                                  {{ Form::text('email', Input::old('email'), array('class' => 'form-control input-login', 'placeholder' => 'Type your email here', 'required', 'id' => 'email')) }}<span class="req-w">*</span>
                                  <div class="result2" id="result2"></div>
                                </div>
                                <div class="form-group">
                                  <input type="password" class="form-control input-login" placeholder="Password" name="password" required><span class="req-w">*</span>
                                </div>
                                <div class="form-group">
                                  <input type="password" class="form-control input-login" placeholder="Re-type password" name="conpassword" required><span class="req-w">*</span>
                                </div>
                                <div class="form-group">
                                  {{ Form::text('username', Input::old('username'), array('title'=>'Panjang username maksimum 23 karakter', 'maxlength'=>'23', 'class' => 'form-control input-login', 'placeholder' => 'Type your username here', 'required', 'id' => 'username')) }}<span class="req-w">*</span>
                                
                                                   
                                  <div class="result" id="result"></div>
                                </div>
                                <!-- <div class="form-group">
                                  <select class="form-control gender" >
                                      <option value="" disabled="" selected="" class="nano">Gender</option>
                                      <option>Male</option>
                                      <option>Female</option>
                                  </select>
                                </div> -->
                                <!-- <div class="form-group">
                                  <select id="day"class="form-control date" >
                                      <option value="" disabled="" selected="" class="nano">Day</option>
                                      <option>1</option>
                                      <option>2</option>
                                      <option>3</option>
                                      <option>4</option>
                                      <option>5</option>
                                      <option>6</option>
                                      <option>7</option>
                                      <option>8</option>
                                      <option>9</option>
                                      <option>10</option>
                                      <option>11</option>
                                      <option>12</option>
                                      <option>13</option>
                                      <option>14</option>
                                      <option>15</option>
                                      <option>16</option>
                                      <option>17</option>
                                      <option>18</option>
                                      <option>19</option>
                                      <option>20</option>
                                      <option>21</option>
                                      <option>22</option>
                                      <option>23</option>
                                      <option>24</option>
                                      <option>25</option>
                                      <option>26</option>
                                      <option>27</option>
                                      <option>28</option>
                                      <option>29</option>
                                      <option>30</option>
                                      <option>31</option>
                                  </select>
                                  
                                  <select id="month" class="form-control date" >
                                      <option value="" disabled="" selected="" class="nano">Month</option>
                                      <option>January</option>
                                      <option>February</option>
                                      <option>March</option>
                                      <option>April</option>
                                      <option>May</option>
                                      <option>June</option>
                                      <option>July</option>
                                      <option>Augustus</option>
                                      <option>September</option>
                                      <option>October</option>
                                      <option>November</option>
                                      <option>December</option>
                                  </select>
                                  
                                  <input type="text" class="form-control date date-year" maxlength="4" placeholder="Year">                                  
                                </div> -->
                                <input type="hidden" name="inputRequired">

                                <!-- Input Captcha Here -->
                                <div class="field-captcha">
                                    <div class="g-recaptcha" data-sitekey="6LdwVxkTAAAAAJijr3cRIzBFSomBH4y9kjP940eW"></div>
                                </div>
                                
                                <div class="checkbox m-t-lg">
                                    <div class="btn-group btn-group-vertical term-of-use-sign" data-toggle="buttons">
                                    <label class="btn">
                                        <input type="checkbox" name="checkbox" id="agree" value="1"><i class="fa fa-square-o"></i><i class="fa fa-check-square-o"></i>
                                    </label>
                                    </div>
                                    <span> I agree to the <a href="#">Terms of Use and Privacy policy</a> of Gritjam *</span>
                                 </div>
                                 <p class="req-w">*  Required</p>
                                 <input id="btnsubmit1" name="btnsubmit1" type="submit" class="btn btn-sm col-btn-ex btn-n-f" value="Sign Up">             
                              </form>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- ================================================= END MODAL REGISTER / SIGN IN ==================================================================== -->   

<script type="text/javascript" src="{{asset('/js/notifications/notification.js')}}" ></script>

<script type="text/javascript">  
    $(document).on('submit', '#formNavbar', function(event) {
        event.preventDefault();
        $.pjax.submit(event, '.pjaxContent');
    })
</script>
<script type="text/javascript">
        var delay = (function(){
          var timer = 0;
          return function(callback, ms){
            clearTimeout (timer);
            timer = setTimeout(callback, ms);
          };
        })();

$(document).ready(function(){
    $('#username').on('keypress', function(e) {
        if (e.which == 32)
            return false;
    });

    $('#email').on('keypress', function(e) {
        if (e.which == 32)
            return false;
    });

    $('#password').on('keypress', function(e) {
        if (e.which == 32)
            return false;
    });

    $('#conpassword').on('keypress', function(e) {
        if (e.which == 32)
            return false;
    });
});

$(document).on('keyup', '#username', function() {
    
    // alert('key up');

    // $('#username').keyup(function(){

        delay(function(){
                UsernameChecking();
                EmailChecking();
                    }, 2000 );
        });

function UsernameChecking(){
    $('#btnsubmit1').attr('disabled', 'disabled');
                var username = $('#username').val(); // Get username textbox using $(this)
                var Result = $('#result'); // Get ID of the result DIV where we display the results
                if(username.length > 2) { // if greater than 2 (minimum 3)
                    Result.html('Loading...'); // you can use loading animation here
                    //var dataPass = 'username='+username;
                    $.ajax({ // Send the username val to available.php
                    type : 'POST',
                    data : {
                        'username': username
                    },
                    url  : 'cekUserAvailability',
                    success: function(responseText){ // Get the result
                        console.log("tot");
                        if(responseText == '0'){
                            $('#btnsubmit1').removeAttr('disabled');
                            Result.html('<span class="sukses">Username Available</span>');
                        }
                        else if(responseText != '0'){
                            $('#btnsubmit1').attr('disabled', 'disabled');
                            Result.html('<span class="gagal">Username Taken</span>');
                        }
                        else{
                            alert('Problem with sql query');
                        }
                    }
                    });
                }else{
                    Result.html('Enter atleast 3 characters');
                }
                if(username.length == 0) {
                    Result.html('');
                }
}
        
    // });


$(document).on('keyup', '#email', function() {
    // alert('key up');

    // $('#username').keyup(function(){
        delay(function(){
                EmailChecking();
                UsernameChecking();
                    }, 2000 );
        });

function EmailChecking(){
    $('#btnsubmit1').attr('disabled', 'disabled');
                var email = $('#email').val(); // Get username textbox using $(this)
                var Result = $('#result2'); // Get ID of the result DIV where we display the results
                if(email.length > 2) { // if greater than 2 (minimum 3)
                    Result.html('Loading...'); // you can use loading animation here
                    //var dataPass = 'username='+username;
                    $.ajax({ // Send the username val to available.php
                    type : 'POST',
                    data : {
                        'email': email
                    },
                    url  : 'cekEmailAvailability',
                    success: function(responseText){ // Get the result
                        if(responseText == '0'){
                            $('#btnsubmit1').removeAttr('disabled');
                            Result.html('<span class="sukses">Email Available</span>');
                        }
                        else if(responseText != '0'){
                            $('#btnsubmit1').attr('disabled', 'disabled');
                            Result.html('<span class="gagal">Email Taken</span>');
                        }
                        else{
                            alert('Problem with sql query');
                        }
                    }
                    });
                }else{
                    Result.html('Enter atleast 3 characters');
                }
                if(email.length == 0) {
                    Result.html('');
                }

}
</script>

<style type="text/css">
    .sukses
    {
        color: green;
    }
    .gagal
    {
        color: red;
    }
    /*.content
    {
        width:900px;
        margin:0 auto;
    }
    #username
    {
        width:500px;
        border:solid 1px #000;
        padding:10px;
        font-size:14px;
    }*/
</style>