<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>
    @section('title')
            Gritjam
    @show
</title>

@include('includes.css')

@include('includes.js-top')

<link href="{{asset('img/favicon.ico')}}" rel="icon" type="image/x-icon">

@yield('styles')

</head>

<body>
<div class="warp-header">
@include('layouts.navbar-login')
</div>

@yield('contents')

@include('layouts.footer')
    
@include('includes.js-bottom')

@yield('scripts')

</body>
</html>
