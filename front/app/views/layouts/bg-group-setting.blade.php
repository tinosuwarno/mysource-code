<div id="draggable-element-group" class='warp-bg-profile-cover' style='background-image:url(
    <?php 
        if (empty($data['bg_picture']) || $data['bg_picture'] == null) { 
            echo ' "' . asset('img/bg-profile/bg.jpg') . '" ';
        }
        else { 
            echo ' "' . asset($data['bg_picture']) . '" ';
        } 
    ?>
    ); cursor: -webkit-grab; background-position-y:
    <?php
         if(!empty($data['y_axisbg'])) {
            if($data['y_axisbg'] <= 0) {
              echo '0' . '%'; 
            }
            elseif($data['y_axisbg'] >= 100) {
              echo '100' . '%';
            }
            else{
              echo $data['y_axisbg'] . '%';
            }
            
         }
         else{
           echo '50%' . '%';
         }
    ?>
    ; position:"relative";'>
  <div class="container">
    <div class="bg-profile-gj">
    <div class="profile-component-bg bg-gradient-profil-gj"></div>
    <div class="row">
      <div class="col-lg-3"></div>
      <div class="col-lg-6">
        <div class="warp-init-profile">
          <div class="profil-pic-gj">
                  <img src=<?php
                            if(!empty($data['picture'])){
                                $url = asset($data["picture"]);
                                echo "\"" . $url . "\"";
                            }
                            else{
                                $url = asset('/img/avatar/avatar-group-300x300.png');
                                echo $url;
                            }
                            ?> class='img-circle img-responsive e-centering img-profile-gj'>
              <!-- <img src="../img/avatar/avatar-group-300x300.png" class="img-circle img-responsive e-centering img-profile-gj"> -->
            </div>
            <h1 class="username-gj">{{$data['name']}}</h1>
            <!-- <div class="user-location">
              <i class="fa fa-map-marker"></i>
                <div class="city-gj">Bogor</div><span class="pad-separator">,</span>
                <div class="country-gj">Indonesia</div>
            </div> -->
            <div class="user-info-gj">
              <p class="desc-info">
                  {{$data['description']}}
                </p>
            </div>
            <div class="warp-btn-add-friend">
             <?php 
              if($group[0]['checkJoined'] == 'joined'){
                    echo "<span class='warp-btn-f-uf' id='btn-joined-group'>";
                    echo "<i class='fa fa-check'></i>";
                    if($group[0]['checkAdmin'] == 'admin'){
                        if($group[0]['checkingEventAktiveAndEventWinner'] == 1 || $group[0]['checkingEventAktiveAndEventWinner'] == '1'){
                            echo "<input type='button' class='btn btn-u-uf' onclick='cantLeaveTheGroup();'  value='Joined' data-value-hover='Leave Group'>";
                        }else if($group[0]['countMember'] > 1){
                            echo "<input type='button' class='btn btn-u-uf' id='adminLeaveGroupAdaMemberLain' groupName='{$group[0]['group_name']}' groupID='{$group[0]['groupID']}' picture='{$group[0]['picture']}' bg_picture='{$group[0]['bg_picture']}' username='{$group[0]['myUsername']}' value='Joined' data-value-hover='Leave Group'>";   
                        }else{
                            echo "<input type='button' class='btn btn-u-uf' id='adminLeaveGroupTidakAdaMemberLain' groupName='{$group[0]['group_name']}' groupID='{$group[0]['groupID']}' picture='{$group[0]['picture']}' bg='{$group[0]['bg_picture']}' username='{$group[0]['myUsername']}' value='Joined' data-value-hover='Leave Group'>";
                        }
                    }else{
                        echo "<input type='button' class='btn btn-u-uf' id='memberleaveGroup' groupName ='{$group[0]['group_name']}' groupID='{$group[0]['groupID']}' picture='{$group[0]['picture']}' bg_picture='{$group[0]['bg_picture']}' username='{$group[0]['myUsername']}' value='Joined' data-value-hover='Leave Group'>";
                    }
                    echo "</span>";
                }
               else if($group[0]['pending'] == 'no' && $group[0]['request'] == 'no' && $group[0]['public'] == '1'){
                    echo "<button type='button' id='UserJoinRequestGroupMember'  groupName ='{$group[0]['group_name']}' groupID='{$group[0]['groupID']}' username='{$group[0]['myUsername']}' class='btn btn-add-f'><i class='fa fa-user-plus'></i>Join Group</button>";
                }
                else if($group[0]['pending'] == 'yes' && $group[0]['request'] == 'no'){
                     echo "<button type='button' id='confirmGroupMember' groupName ='{$group[0]['group_name']}' groupID='{$group[0]['groupID']}' username='{$group[0]['myUsername']}' class='btn btn-add-f'><i class='fa fa-user-plus'></i>confirm</button>";
                }
                else if($group[0]['request'] == 'yes'){
                    echo "<button type='button' id='cancelRequestSendMember' groupName='{$group[0]['group_name']}' groupID='{$group[0]['groupID']}' memberID='{$group[0]['myuserID']}'  username='{$group[0]['myUsername']}' class='btn btn-add-f'><i class='fa fa-user-plus'></i>Request Sent</button>";
                }
             ?>   
                <!-- ------------- APABILA SUDAH MENJADI FRIEND ------------------------------
              ----------- <button type="button" class="btn btn-udh-f">Friend</button> --------- -->
            </div>
        </div>
        </div>
        <div class="col-lg-3"></div>
    </div>
    @if($group[0]['checkAdmin'] == 'admin')
        <div class="warp-geser">
          <a class="btn-geser" onclick="changedivBgGroup()" id="geserBgGroup" style="display: block">
              <i class="fa fa-arrows"></i>
                <div class="btn-geser-det">
                  <span>Reposition Cover Group</span>
                </div>
            </a>
            <a class="helper-geser" id="helpGroup" style="display:none">
              <i class="fa fa-arrows"></i>
                <div class="helper-geser-det">
                  <span>Drag Your Cover Group</span>
                </div>
            </a>
            <span id="data-groupid-drag" style="display: none;">{{$groupID}}</span>
            <a class="btn-fix-geser" id="subGroup" style="display:none" >
              Submit
            </a>
            <a class="btn-ga-geser" onclick="cancel()" id="cancelGroup" style="display:none">
              Cancel
            </a>
        </div>
     @endif   
    </div>
    </div>
</div>

<!-- ============== MODAL ADMIN LEAVE GROUP JIKA ADA MEMBER LAIN =====================-->
<div id="modalAdminLeaveGroupAdaMemberLain" tabindex="-1" role="dialog" aria-hidden="true" class="modal vh-center fade">
  <div class="modal-dialog modal-sm" style="width: 400px">
    <div class="modal-content">
      <div class="modal-body nopadding">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sxs-12">
            <div class="warp-crt-add-playlist">
              <div class="modal-header color-gj-popup-report">
                <button type="button" class="close close-button" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Leave Group </h4>
              </div>
              <div class="warp-15">
                <div>Are you sure to leave group : <strong><span id="data-groupname-adminLeave"></span></strong> ?</div>
                <div>Please,select new admin</div>
                <br>
                <div>
                  <select data-placeholder="Select New Admin..." class="chosen-select" tabindex="4" style="display:block;" name="new_admin_member_id" id="new_admin_member_id">
                    @if(!empty($member))
                    <option value="">-- select new admin --</option>
                    @foreach ($member as $key => $value)
                    <option value="{{$value['memberID']}}" id="{{$value['memberID']}}" name="{{$value['memberID']}}">{{$value['username']}}
                    </option>
                    @endforeach
                    @else
                    <option value=""></option>
                    <option> Sorry, not Found other member in Group </option>
                    @endif 
                    <option value=""></option>
                  </select>
                </div>
                <span id="data-groupid-adminLeave" style="display: none;"></span>
                <span id="data-picture-adminLeave" style="display: none;"></span>
                <span id="data-bg-picture-adminLeave" style="display: none;"></span>
                <br>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class='modal-footer'>
        <span class='loader-process' style='display: none;'><img src='{{asset('img/loader-24.gif')}}' /></span>
        <a href="#" class="btn-s-gj bgc-btn close-button" data-dismiss="modal">Cancel</a>
        <a href="#" class="btn-s-gj bgc-btn process-admin-leave-group-select-newadmin" id="admin-leave-select-newAdmin">YES</a>
      </div>
    </div>
  </div>
</div>