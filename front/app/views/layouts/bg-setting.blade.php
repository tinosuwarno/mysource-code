<?php
    $session = Session::get('data');

    $dataPost = [
        'userID' => $session['id'],
        'apiKey' => $session['apiKey']
    ];

    if(!empty($username))
        $dataPost['username'] = $username;
    $getDetails = new myGuzzle('getSettingProfile');
    $getDetails->setHeader($session['apiKey']);
    $data = $getDetails->formParams($dataPost, 'post');
    
    if(empty($username)){ // user profile
?>

<div id="draggable-element1" class='warp-bg-profile-cover' style='background-image:url(
    <?php 
        if (empty($data['bg_image']) || $data['bg_image'] == null) { 
            echo ' "' . asset('img/bg-profile/bg.jpg') . '" ';
        }
        else { 
            echo ' "' . asset($data['bg_image']) . '" ';
        } 
    ?>
    ); cursor: -webkit-grab; background-position-y:   
    <?php 
        if (!empty($data['y_axisbg'])) {
            if ($data['y_axisbg'] <= 0) {
                echo '0' . '%';
            }
            elseif ($data['y_axisbg'] >= 100) {
                echo '100' . '%';
            }
            else
               echo $data['y_axisbg'] . '%';
        }
        else 
            echo '50%' . '%';
    ?>
    ; position:"relative";'>
    <div class='container'>
    <div class='bg-profile-gj'>
        <div class='profile-component-bg bg-gradient-profil-gj'></div>
        <div>
            <div class='col-lg-4'></div>
            <div class='col-lg-4'>
                <div class='warp-init-profile'>
                    <div class='profil-pic-gj'>
                    <div class="hov-edit"><a href="#" class="warp-btn-edit-p edit-pp"><span>Edit Picture</span><i class="fa fa-pencil"></i></a></div>
                        <img src=<?php
                            if(!empty($data['profile_image'])){
                                $url = asset($data["profile_image"]);
                                echo "\"" . $url . "\"";
                            }
                            else{
                                $url = asset('/img/avatar/avatar-300x300.png');
                                echo $url;
                            }
                            ?> class='img-circle img-responsive e-centering img-profile-gj'>
                    </div>
                    <h1 id='userUsernameViewProfile' data-username="{{$data['username']}}" class='username-gj'><?php 
                                        echo $data['username'];
                                    ?></h1>
                    <div class='user-location'>
                        <?php  
                            if (empty($data['city']) && empty($data['country'])) {
                                echo "";
                            }else{echo "<i class='fa fa-map-marker'></i>";}
                        ?>
                        
                        <!-- get city and country live -->
                        <div class='city-gj'><?php echo $data['city']; ?></div>
                        <?php  
                            if (!empty($data['country'])) {
                                echo "<span class='pad-separator'>,</span>";
                            }
                        ?>
                        
                        <div class='country-gj'><?php echo $data['country']; ?></div>
                    </div>
                    <div class='user-info-gj'>
                        <p class='desc-info'>
                            <!-- description -->
                           <?php echo $data['biography']; ?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- ============================= MODAL EDIT PICTURE ============================================= --> 
    <div id="editPicture" tabindex="-1" role="dialog" aria-hidden="true" class="modal fade" >
      <div class="modal-dialog">
        <div class="modal-content">
          
          <div class="modal-body nopadding">
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sxs-12">
                <div class="warp-crt-add-playlist"> 
                    <div class="modal-header color-gj-popup-report">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Edit Picture</h4>
                    </div>
                    <div class="warp-15">
                        <form method="post" action="/profile/submit/<?php echo $session['id']; ?>" enctype="multipart/form-data">
                            <div class="warp-edit-form-up">
                            <div class="single-form-up">
                            <div class="row">
                                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12"><label class="label-edit-form">Photo</label></div>
                                <div class="col-lg-6 col-md-7 col-sm-8">
                                  <img id="preview" src="<?php if ($data['profile_image']==null) {
                                         echo asset('img/avatar/avatar-300x300.png');
                                      }else{
                                         echo asset($data['profile_image']);} ?>" class="img-responsive img-edit-up img-border-gj img-profile-user">
                                   <input type="file" onchange="document.getElementById('preview').src = window.URL.createObjectURL(this.files[0])" name="image" id="image" class="inputfile" >
                                    <label for="image"> <span>Choose a file</span></label>
                                </div>
                            </div>
                            </div>
                            <div class="single-form-up">
                            <div class="row">
                                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12"><label class="label-edit-form">Background</label></div>
                                <div class="col-lg-6 col-md-7 col-sm-8">
                                      <img id="preview2" src="<?php if ($data['bg_image']==null) {
                                        echo asset('img/bg-profile/bg-profile.jpg');
                                      }else{
                                       echo asset($data['bg_image']);} ?>" class="img-responsive img-edit-up img-border-gj">
                                      <input type="file" onchange="document.getElementById('preview2').src = window.URL.createObjectURL(this.files[0])" name="bg_image" id="bg_image" class="inputfile" data-multiple-caption="{count} files selected" multiple>
                                        <label for="bg_image"> <span>Choose a file</span></label>
                                    </div>
                            </div>
                            </div>
                            
                            <div class="l-hr"></div>
                            <div class="form-group">
                              <div class="col-lg-12">
                                <button class="btn bgc-btn c-f-btn crt-group">Save</button>
                              </div>
                            </div>
                            
                            </div>
                        </form>
                    </div>
                </div>
            </div>
          </div>
          </div>
        </div>
      </div>
  </div>
<!-- ============================= END MODAL EDIT PICTURE =========================================== --> 
            <div class='col-lg-4'></div>
        </div>
        <div class='warp-geser'>
            <a class="btn-geser" onclick="changediv()" id="geser" style="display: block">
                <i class='fa fa-arrows'></i>
                <div class='btn-geser-det'>
                    <span>Reposition Cover Photo</span>
                </div>
            </a>
            <a class="helper-geser" id="help" style="display: none">
                <i class='fa fa-arrows'></i>
                <div class='helper-geser-det'>
                    <span>Drag Your Cover Photo</span>
                </div>
            </a>
            <a class="btn-fix-geser" id="sub" style="display: none">
                Submit
            </a>
            <a class="btn-ga-geser" onclick="cancel()" id="cancel" style="display: none">
                Cancel
            </a>
        </div>
    </div>
</div>
</div>

<?php
    }
    else { //friend profile
?>
    <div id="draggable-element1" class='warp-bg-profile-cover' style='background-image:url(
    <?php 
        if (empty($data['bg_image']) || $data['bg_image'] == null) { 
            echo ' "' . asset('img/bg-profile/bg.jpg') . '" ';
        }
        else { 
            echo ' "' . asset($data['bg_image']) . '" ';
        } 
    ?>
    ); cursor: -webkit-grab; background-position-y:   
    <?php 
        if (!empty($data['y_axis'])) {
            if ($data['y_axis'] <= 0) {
                echo '0' . '%';
            }
            elseif ($data['y_axis'] >= 100) {
                echo '100' . '%';
            }
            else
               echo $data['y_axis'] . '%';
        }
        else 
            echo '50%' . '%';
    ?>
    ; position:"relative";'>
    <div class='container'>
    <div class='bg-profile-gj'>
        <div class='profile-component-bg bg-gradient-profil-gj'></div>
        <div>
            <div class='col-lg-4'></div>
            <div class='col-lg-4'>
                <div class='warp-init-profile'>
                    <div class='profil-pic-gj'>
                        <img src='{{asset($data["profile_image"])}}' class='img-circle img-responsive e-centering img-profile-gj'>
                    </div>

                    <h1 id='userUsernameViewProfile' data-username="{{$data['username']}}" class='username-gj'><?php echo $data['username']; ?></h1>
                    <div class='user-location'>
                        <?php  
                            if (empty($data['city']) && empty($data['country'])) {
                                echo "";
                            }else{echo "<i class='fa fa-map-marker'></i>";}
                        ?>
                        
                        <!-- get city and country live -->
                        <div class='city-gj'><?php echo $data['city']; ?></div>
                        <?php  
                            if (!empty($data['country'])) {
                                echo "<span class='pad-separator'>,</span>";
                            }
                        ?>
                        
                        <div class='country-gj'><?php echo $data['country']; ?></div>
                    </div>
                    <div class='user-info-gj'>
                        <p class='desc-info'>
                           <?php echo $data['biography']; ?>
                        </p>
                    </div>
                    <div class='warp-btn-add-friend'>
                    <?php
                        if($data['is_friend'] == 1)
                            echo "<span class='warp-btn-f-uf' id='bg-div-friend'>
                                    <i class='fa fa-check'></i>
                                    <input onclick='unFriend(\"".$data['id']."\",\"bg-div-friend\")' type='button' class='btn btn-u-uf' value='Friend' data-value-hover='Unfriend'>
                                </span>";
                        elseif($data['is_friend'] == 0)
                            echo "<span class='warp-btn-f-uf' id='bg-div-friend'>
                                    <i class='fa fa-check'></i>
                                    <input onclick='cancelRequest(\"".$data['id']."\",\"bg-div-friend\")' type='button' class='btn btn-u-uf' value='Request Sent' data-value-hover='Cancel Request'>
                                </span>";
                        else
                            echo "<button id='bg-div-friend' onclick='addFriend(\"".$data['id']."\",\"bg-div-friend\")' type='button' class='btn btn-add-f'><i class='fa fa-user-plus'></i>Add Friend</button>";

                        echo " ";

                        if($data['is_following'] == 1)
                            echo "<span class='warp-btn-f-uf' id='bg-div-follow'>
                                    <i class='fa fa-check'></i>
                                    <input onclick='unFollow(\"".$data['id']."\",\"bg-div-follow\")' type='button' class='btn btn-u-uf' value='Following' data-value-hover='UnFollow'>
                                </span>";
                        else
                            echo "<button id='bg-div-follow' onclick='following(\"".$data['id']."\",\"bg-div-follow\")' type='button' class='btn btn-add-f'><i class='fa fa-user-plus'></i>Follow</button>";
                    ?>
                    </div>
                </div>
            </div>
            <div class='col-lg-4'></div>
        </div>
    </div>
</div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="md-w-warp">
            <div class="col-lg-12">
                <div class="menu text-center menu-wall motion-animate">
                    <div class="hr"></div>
                    <ul class="action-menu-profile">
                        <li id='aboutFriendMyWall'>
                            <a data-pjax='yes' id='pjax' href="/<?php echo $username ?>/about">About</a>
                        </li>
                        <li id='userUsernameViewProfile' data-username="{{$data['username']}}">
                            <a data-pjax='yes' id='pjax' href="/{{$data['username']}}"><?php echo ucfirst($username) ?>'s Wall</a>
                        </li>
                        <li id='projectFriendMyWall'>
                            <a data-pjax='yes' id='pjax' href="/<?php echo $username ?>/project">Project</a>
                        </li>
                    </ul>
                    <div class="warp-btn-event-noaffix hidden-lg hidden-md">
                        <a id="btnEvent-affix2" class="btn-event-affix" href="#sidr"><i class="fa fa-calendar"></i></a>
                    </div>
                    <div class="hr"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>

<?php
    }
?>