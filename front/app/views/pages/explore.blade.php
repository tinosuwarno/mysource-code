@extends('layouts.default-base')
{{-- overwrite title --}}
@section('title')
Gritjam - Explore
@overwrite

@section('styles')

@stop

{{-- content --}}
@section('contents')
<?php
 $myApp = App::make('baseUrl'); 
 $baseUrl = $myApp->baseUrl ; 
?>
<div class="block-white"></div>
    <div class="block-white"></div>
    <div class="block-white visible-sm hidden-lg hidden-xs hidden-md"></div>
    <div class="block-white visible-sm hidden-lg hidden-xs hidden-md"></div>
    <div class="container-fluid nopadding">
      <div class="warp-carousel-track-market">
        <div class="row">
          <div class="owl-market owl-theme">
            <div class="item item-slide-market"><img src="img/album/album-06.jpg" alt=""></div>
            <div class="item item-slide-market"><img src="img/album/track-01.jpg" alt=""></div>
            <div class="item item-slide-market"><img src="img/album/track-03.jpg" alt=""></div>
            <div class="item item-slide-market"><img src="img/album/track-04.jpg" alt=""></div>
            <div class="item item-slide-market"><img src="img/album/track-05.jpg" alt=""></div>
            <div class="item item-slide-market"><img src="img/album/track-06.jpg" alt=""></div>
          </div>
          <div class="prev-nav-wraper">
            <a class=" prev-market"><i class="fa fa-4x fa-chevron-left"></i></a>
          </div>
          <div class="next-nav-wraper">
            <a class=" next-market"><i class="fa fa-4x fa-chevron-right"></i></a>
          </div>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="warp-section-border-t">
          <div class="col-lg-12">
            <div class="about-gj">
              <h3>Find your Friends</h3>
             <!-- wait content -->
              <!--<p class="desc-p">
                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
              </p>-->
              <div class="warp-btn-ex">
                <a class="btn-geo-ex col-btn-ex" id='proximityExplore'><i class="fa fa-map-marker"></i>Find Nearby</a>
                <a class='btn-ref-ex col-btn-ex' id='refreshExplore'><i class='fa fa-refresh'></i>Refresh</a>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="warp-owl-card" id='refreshThis'>
            <div id='loadingFriendExplore'>
                <div class='col-xs-push-5 col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 lazyloader-list'>
                    <img src='img/loader-more.gif' class='img-full-responsive e-centering width-loader'>
                </div>
            </div>
          </div>
        </div>
        <div class="warp-section-border-t">
          <div class="col-lg-12">
            <div class="about-gj">
              <h3>Shout Box</h3>
              <!-- wait content -->
              <!-- <p class="desc-p">
                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
              </p> -->
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-sxs-12">
            <div class="warp-disc-user">
              <div class="disc-user-header">
                <div class="disc-header-bg" style="background-image:url(img/bg-cd-overlay.jpg);">
                </div>
                <div class="tile-card-bg"></div>
                <div class="spot-header-ex">
                  <h3>Event</h3>
                  <p>Show your talent and win the prize!</p>
                </div>
              </div>
              <div class="warp-track-ex">
              
                <div class='col-xs-push-5 col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 lazyloader-list' id='loadingEventExplore1'>
                  <img src="{{asset('img/loader-more.gif')}}" class='img-full-responsive e-centering width-loader'>
                </div>
              
              </div>
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-sxs-12">
            <div class="warp-disc-user">
              <div class="disc-user-header">
                <div class="disc-header-bg" style="background-image:url(img/bg-overlay-2.jpg);">
                </div>
                <div class="tile-card-bg"></div>
                <div class="spot-header-ex">
                  <h3>Invitation</h3>
                  <p>Jump in to cool music collaborations with your friends and get discovered!</p>
                </div>
              </div>
              <div class="warp-track-ex">
                  <div class='col-xs-push-5 col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 lazyloader-list' id='loadingEventExplore2'>
                       <img src="{{asset('img/loader-more.gif')}}" class='img-full-responsive e-centering width-loader'>
                  </div>
              </div>
            </div>
          </div>
        </div>
        <div class="warp-section-border-t">
          <div class="about-gj">
            <h3>Find Out More</h3>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="disc-gj-header-link">
              <div class="disc-header-bg-link" style="background-image:url(img/bg-overlay-1.jpg);">
              </div>
              <div class="tile-card-bg"></div>
              <a href="{{$baseUrl}}track-bank#new-release" id="pjax" data-pjax="yes" class="explore-link">
                <div class="link-header-ex text-center">
                  <h3>New Release</h3>
                </div>
              </a>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="disc-gj-header-link">
              <div class="disc-header-bg-link" style="background-image:url(img/bg-overlay-2.jpg);">
              </div>
              <div class="tile-card-bg"></div>
              <a href="{{$baseUrl}}track-bank#most-popular" class="explore-link">
                <div class="link-header-ex text-center">
                  <h3>Most Popular</h3>
                </div>
              </a>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="disc-gj-header-link">
              <div class="disc-header-bg-link" style="background-image:url(img/bg-overlay-3.jpg);">
              </div>
              <div class="tile-card-bg"></div>
              <a href="{{$baseUrl}}track-bank#most-downloaded" class="explore-link">
                <div class="link-header-ex text-center">
                  <h3>Most Download</h3>
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="block-white"></div>
<!-- ---------- END MARKET MUSIC ------------------ -->

@stop