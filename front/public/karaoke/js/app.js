function checkUserBrowser(){
  var appname = navigator.appName;
  return appname;
}

function setJWPlayer(container, play, pause){
  var playerInstance = jwplayer(container);
        playerInstance.setup({
                file: lokasiStreaming,
                height: 40,
                width: "100%",
                title: title,
                primary: "html5",
                hlshtml: true,
                preload: "auto",
                skin: {
                  url: window.location.origin+"/karaoke/jwkaraoke/skins/gjskin.css",
                  name: container,
                },
                events : {
                    onPlay : function(){
                        $(play).hide();
                        $(pause).show();
                        // $('#pause-1').prop('disabled', true);
                    },
                    /*onPause :function(){
                        $('#play-1').show();
                        $('#pause-1').hide();
                    },*/
                    onIdle : function(){
                        $(play).show();
                        $(pause).hide();
                        // $('#pause-1').prop('disabled', true);
                    }
                }
        });

        jwplayer(container).on('displayClick', function(){
            return false;
        });
}

function setJWPlayer2(container, play, pause){
  var playerInstance = jwplayer(container);
        playerInstance.setup({
                file: lokasiStreaming2,
                height: 40,
                width: "100%",
                title: title,
                primary: "html5",
                hlshtml: true,
                preload: "auto",
                skin: {
                  url: window.location.origin+"/karaoke/jwkaraoke/skins/gjskin.css",
                  name: container,
                },
                events : {
                    onPlay : function(){
                        $(play).hide();
                        $(pause).show();
                        // $('#pause-1').prop('disabled', true);
                    },
                    /*onPause :function(){
                        $('#play-1').show();
                        $('#pause-1').hide();
                    },*/
                    onIdle : function(){
                        $(play).show();
                        $(pause).hide();
                        // $('#pause-1').prop('disabled', true);
                    }
                }
        });

        jwplayer(container).on('displayClick', function(){
            return false;
        });
}



function getJwplayerstate(container){
  var state_player = jwplayer(container).getState();

  return state_player;
}

function Buffered50(container){
            
            var buffer_percent = jwplayer(container).getBuffer();
            var state_of_player = jwplayer(container).getState();
         
            if (buffered_enough == 1){
               return;
            }
            
            if(buffered_enough == 0 && state_of_player == 'playing'){
               jwplayer(container).pause(true);
            }
            
               myVar = setInterval(function(){ 
                  var buffer_percent2 = jwplayer(container).getBuffer();
                    // Set the number 75 below to the percent you want to buffer before playing - remember to do it in the html, too.
                  if (buffered_enough == 0 && buffer_percent2 > 8){
                     buffered_enough = '1';
                     jwplayer(container).play();
                     clearInterval(myVar);
                     return;
                  }
               },1000);
         
         }

function drawCanvasRecorder(level){
        /**
       * The Waveform canvas
       */
      analyser = Fr.voice.context.createAnalyser();
      analyser.fftSize = 2048;
      analyser.minDecibels = -90;
      analyser.maxDecibels = -10;
      analyser.smoothingTimeConstant = 0.85;
      Fr.voice.input.connect(analyser);
      
      var bufferLength = analyser.frequencyBinCount;
      var dataArray = new Uint8Array(bufferLength);
      
      WIDTH = 1200, HEIGHT = 150;
      canvasCtx = $("#level-"+level)[0].getContext("2d");
      canvasCtx.clearRect(0, 0, WIDTH, HEIGHT);
      
      function draw() {
        drawVisual = requestAnimationFrame(draw);
        analyser.getByteTimeDomainData(dataArray);
        canvasCtx.fillStyle = 'rgb(200, 200, 200)';
        canvasCtx.fillRect(0, 0, WIDTH, HEIGHT);
        canvasCtx.lineWidth = 2;
        canvasCtx.strokeStyle = 'rgb(51,122,183)';
  
        canvasCtx.beginPath();
        var sliceWidth = WIDTH * 1.0 / bufferLength;
        var x = 0;
        for(var i = 0; i < bufferLength; i++) {
          var v = dataArray[i] / 128.0;
          var y = v * HEIGHT/2;
  
          if(i === 0) {
            canvasCtx.moveTo(x, y);
          } else {
            canvasCtx.lineTo(x, y);
          }
  
          x += sliceWidth;
        }
        canvasCtx.lineTo(WIDTH, HEIGHT/2);
        canvasCtx.stroke();
      };
      draw();
}



function startRecord(container, finish, rec, stop, canvas, monitor, play, pause, startlvl, lvl, pauserec, resumerec, audio){
  $(finish).prop('disabled', true);
  console.log('recording started');
  playrecord(canvas, monitor);
  myIntervalRecord = setInterval(function(){
    var stateJw = jwplayer(container).getState();
    console.log('stateJw :'+stateJw);
    if (stateJw == 'buffering') {
      Fr.voice.pause();
      clearInterval(myIntervalRecord);
      return;
    } else if (stateJw == 'complete'){
      stoprecord(container, play, pause, finish, rec, stop, startlvl, lvl, pauserec, resumerec, audio);
      clearInterval(myIntervalRecord);
      return;
    } else if (stateJw == 'paused') {
      stateJWOnPauseRec = stateJw;
      console.log('State onPause: '+stateJWOnPauseRec);
      Fr.voice.pause();
       //clearInterval(myIntervalRecord);
      return;
    }
  }, 100);
  
}

function timeoutFunc(timeout, container, finish, rec, stop){
      console.log('recording started');

      var new_timeout = timeout*1000;
      console.log(new_timeout);
      playrecord('1', '#monitor-1');
      var recordStop = setTimeout(stoprecord, new_timeout);

      $('#stop-1').click(function(){
              clearTimeout(recordStop);
              Fr.voice.stop();
              jwplayer(container).stop();
              $(this).hide();
              $('#rec-1').show();
              $('#play-1').prop('disabled', false);
              $('#pause-1').prop('disabled', false);
      });
}

function stoprecordandupload(){
  console.log('record done');
  Fr.voice.export(function(blob){
      var formData = new FormData();
      formData.append('file', blob);
      formData.append('trackID', trackName);
  
      $.ajax({
        url: "uploadKaraoke",
        type: 'POST',
        data: formData,
        contentType: false,
        processData: false,
        success: function(url) {
          alert(url['message']);
          console.log('upload done');
        }
      });
    }, "blob");
  Fr.voice.stop();
}

function finishAndUpload(finish){

    console.log('start finishing and uploading');
    console.log(trackName+' '+projectIDzz);



    Fr.voice.export(function(blob){
    var formData = new FormData();
    formData.append('file', blob);
    formData.append('trackID', trackName);
    formData.append('projectID', projectIDzz);

    console.log('BLOB: '+blob);
    console.log('Track ID: '+trackName);
    console.log('ProjectID: '+projectIDzz);

    console.log(formData);

    $(".loader-process").show();
    $.ajax({
      url: window.location.origin+"/studio/karaoke/uploadKaraoke",
      type: 'POST',
      data: formData,
      timeout:500000,
      contentType: false,
      processData: false,
      success: function(url) {
        /*alert(url);
        console.log('upload done');*/
        if(url.status == 'success'){
           $('#modalSaveConfirmation').modal('hide');
          swal({ 
            title: "Success save your voice!",
            text: "Please refersh this page by clicked OK button!",
            type: "success",
            allowOutsideClick: false,
          });
          $('.swal2-confirm').click(function(){
             location.reload();
          });
          $(finish).prop('disabled', true);
        } else {
          swal('Oops...', url.message,'error');
        }
       
      },
      error: function(jqXHR, textStatus, errorThrown){
              console.log(jqXHR);
              $(".loader-process").hide();
              swal('Oops...','Something went wrong!','error');
          }
    });
  }, "blob");
  Fr.voice.stop();
}

function releaseProject(){
  console.log('trackID & ProjectID : '+trackName+'-'+projectIDzz);

  var is_timeline = 0;
  var is_demo  = 0;
  // var vocal_only = 0;
  var previllages = $("#select-previllages").val();
  var project_desc = $("#project-description").val();
  var post_caption = '';

  if ($('#post-timeline').is(":checked"))
  {
    is_timeline = $("#post-timeline").val();
    post_caption = $("#post-caption").val();

  }

  if ($('#post-demo').is(":checked"))
  {
    is_demo = $("#post-demo").val();
  }

  // if ($('#vocal-only').is(":checked"))
  // {
  //   vocal_only = $("#vocal-only").val();
  // }




  console.log('timeline :'+is_timeline+' - demo :'+is_demo+' - previllages :'+previllages);
  console.log('project Description : '+project_desc+' - post_caption :'+post_caption);
  

  

      var formData = new FormData();
      $.each($('#artwork')[0].files, function(i, artwork) {
          formData.append('artwork-'+i, artwork);
      });
      console.log('file artwork :'+artwork);
      if (wrapped == 3 && invited == 1) {
          formData.append('trackID', track_IDV2);
      } else {
          formData.append('trackID', trackName);
      };
      
      formData.append('projectID', projectIDzz);
      formData.append('project_desc', project_desc);
      formData.append('postTimeline', is_timeline);
      formData.append('post_caption', post_caption);
      formData.append('promoteToDemo', is_demo);
      formData.append('isPublic', previllages);
      // formData.append('vocal_only', vocal_only);


      $(".loader-process").show();

      $.ajax({
        url: window.location.origin+"/studio/karaoke/releaseProject",
        type: 'POST',
        data: formData,
        timeout:500000,
        contentType: false,
        processData: false,
        success: function(url) {
          /*alert(url);
          console.log('upload done');*/
          if(url.status == 1){
             $('#modalReleaseProject').modal('hide');
             console.log(url);
            swal({ 
              title: "Success Release your project!",
              text: "Please refersh this page by clicked OK button!",
              type: "success",
              allowOutsideClick: false,
            });
            $(".loader-process").hide();
            $('.swal2-confirm').click(function(){
                window.location.href = window.location.origin+"/studio";
            });
            // $(finish).prop('disabled', true);
          } else {
            swal('Oops...', url.message ,'error');
          }
         
        },
        error: function(jqXHR, textStatus, errorThrown){
                console.log(jqXHR);
                $(".loader-process").hide();
                swal('Oops...','Something went wrong!','error');
            }
      });
      
}

function stoprecord(container, play, pause, finish, rec, stop, startlvl, lvl, pauserec, resumerec, audio){
  if ( lokasiStreaming != '') {
    jwplayer(container).stop();
  }
  
  console.log('record done');
  Fr.voice.export(function(url){
     $(audio).attr("src", url);
   }, "URL");
   // restore();

  $(pauserec).hide();
  $(resumerec).hide();

  if($(play).is(":hidden")){
    console.log('cek visible');
    $(play).show();
    $(pause).hide();
  }

  //$(play).prop('disabled', false);
  $(finish).prop('disabled', false);
  $(rec).show();
  $(stop).hide();
  // Fr.voice.stop();
  Fr.voice.pause();
  $(startlvl).show();
  $(lvl).hide();

  if(sumCheck == 0 && wrapped == 0){
    $(play).prop('disabled', false);
    $(finish).prop('disabled', true);
    $('#release-backsong').prop('disabled', false)
  }
}

function playrecord(canvas, monitor){
  Fr.voice.record($(monitor).is(":checked"), function(){

      drawCanvasRecorder(canvas); 
      
    });
}

function removeVocal() {
    if (wrapped == 3){
      var trackIDRemove = track_IDV2;
    } else {
      var trackIDRemove = trackName;
    }
    var formData = new FormData();
    formData.append('trackID', trackIDRemove);
    formData.append('projectID', projectIDzz);
    formData.append('wrapped', wrapped);

    console.log('trackID : '+trackName+'- projectID: '+projectIDzz+'- wrapped: '+wrapped);

    $(".loader-process").show();
    $.ajax({
      url: window.location.origin+"/studio/karaoke/removeVocal",
      type: 'POST',
      data: formData,
      timeout:500000,
      contentType: false,
      processData: false,
      success: function(url) {
        $(".loader-process").hide();
        console.log(url);
        /*alert(url);
        console.log('upload done');*/
        if(url.status == 1){
           $('#modalRemoveConfirmation').modal('hide');
          swal({ 
            title: url.message,
            text: "Please refersh this page by clicked OK button!",
            type: "success",
            allowOutsideClick: false,
          });
          $('.swal2-confirm').click(function(){
             location.reload();
          });
        } else {
          $(".loader-process").hide();
          swal('Oops...', url.message,'error');
        }
       
      },
      error: function(jqXHR, textStatus, errorThrown){
              console.log(jqXHR);
              $(".loader-process").hide();
              swal('Oops...','Something went wrong!','error');
          }
    });

}

//-------------------------------- LOAD PAGE -------------------------------- //
$(function(){
  console.log('wrapped: '+wrapped+' & invited: '+invited);
  console.log('Browser :'+ browser);

  

  if (sumCheck != '') {

  if(wrapped != 3 && sumCheck!=0){
    setJWPlayer(container, play, pause);
  } else if (wrapped == 3){
    setJWPlayer2(container, play, pause);
  }

  if (sumCheck != 2) {
    $('#advHeadset').html(
        '<div class="modal-dialog modal-sm">'+
            '<div class="modal-content">'+
                '<div class="modal-header color-gj-popup-report">'+
                    '<h4 class="modal-title">Recommended</h4>'+
                '</div>'+
                '<div class="modal-body">'+
                    '<div class="row">'+
                        '<div class="col-sm-12">'+
                            '<div class="warp-15 text-center">'+
                                '<img src="'+window.location.origin+'/img/headset-n.png" class="img-responsive">'+
                            '</div>'+
                            '<div class="warp-15 text-center">'+
                                '<strong>Headphones Recommended</strong>'+
                                '<p class="p-mar-tb">Stop terrible feedback and static noise. Use headphones to get the best recording sound.</p>'+
                            '</div>'+
                            '<div class="l-hr"></div>'+
                            '<div class="text-center">'+
                                '<button class="btn bgc-btn c-f-btn" data-dismiss="modal">OK</button>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
        '</div>'
      );

    $('#advHeadset').modal({
         backdrop: 'static',
         keyboard: true, 
         show: true
      });

  }

    
    if(wrapped == 0 ){
      // $('.voice-2').hide();
      // $('.voice-1').show();
      $('#add-vocal').prop('disabled', true);
      $('#release-project').prop('disabled', true);
      $('#remove-vocal').prop('disabled', true);
      $('#rec-1').prop('disabled', false);

      if (sumCheck == 0) {
        $('#play-1').prop('disabled', true);
        $('#release-project').hide();
        $('#release-backsong').show();
        $('#release-backsong').prop('disabled', true);
      }

    } else if(wrapped == 1 && invited == 1){
      console.log('you have no power here');
      $('#release-project').prop('disabled', true);
      $('#remove-vocal').prop('disabled', true);
      $('#add-vocal').prop('disabled', false);
      $('#rec-1').hide();
      //$('#level-1').hide();
      //$('#start-level-1').hide();
      $('.voice-1 > .warp-wave-cnv').hide();

      if (sumCheck == 0) {
        $('#play-1').prop('disabled', true);
      }

    }else if (wrapped == 1) {
      console.log('why am i here');
      $('#add-vocal').prop('disabled', false);
      $('#release-project').prop('disabled', false);
      //$('#remove-vocal').prop('disabled', false);
      $('#rec-1').prop('disabled', true);

    } else{
      console.log('you shall not pass');
      // $('.voice-1').hide();
      // $('.voice-2').show();
      $('#add-vocal').prop('disabled', true);
      $('#release-project').prop('disabled', false);
      //$('#remove-vocal').prop('disabled', false);
      $('#rec-1').prop('disabled', true);
      $('#monitor-1').hide();
      $('#audio-1').hide();
      $('.warp-dflt-voice').hide();
    }

    $('.voice-2').hide();
    

    //pause resume action
    $('#pauserecord-1').hide();
    $('#resrecord-1').hide();
    $('#pauserecord-2').hide();
    $('#resrecord-2').hide();

     $('#finish-1').prop('disabled', true);
     $('#finish-2').prop('disabled', true);

     
    //$('.jw-controlbar').hide();

    //--------------------------- VOICE 1 ---------------------------//
    $('#rec-1').click(function(){
      if ( lokasiStreaming != '') {
        jwplayer(container).stop();
      }
          
      if($("#audio-1").attr("src")!=""){
        $("#audio-1")[0].pause();
        $("#audio-1")[0].currentTime = 0;

      }

          $('#pauserecord-1').show();

          $('#play-1').prop('disabled', true);
          $('#pause-1').prop('disabled', true);
          $('#rec-1').hide();
          $('#start-level-1').hide();
          $('#level-1').show();
          $('#stop-1').show();

          if (lokasiStreaming != '') {
            jwplayer(container).play();
            myInterval = setInterval(function(){
              myState = jwplayer(container).getState();
              if (myState == 'playing' && stateJWOnPauseRec == '') {

                startRecord(container, '#finish-1', '#rec-1', '#stop-1', '1', '#monitor-1', '#play-1', '#pause-1', '#start-level-1', '#level-1', '#pauserecord-1', '#resrecord-1', "#audio-1");
                //timeoutFunc(durasi, container, '#finish-1', '#rec-1', '#stop-1');
                clearInterval(myInterval);
                return;
              } 
            }, 100);
          } else{
            playrecord('1', '#monitor-1');

          }
          
    });
  
     $('#stop-1').click(function(){
          stoprecord(container, '#play-1', '#pause-1', '#finish-1', '#rec-1', '#stop-1', '#start-level-1', '#level-1', '#pauserecord-1', '#resrecord-1', "#audio-1");
      });
  

  console.log("val:"+ $("#audio-1").attr("src"));

  //listen, pause, retake, finish
  $('#play-1').click(function(){
    console.log($("#audio-1").attr("src"));

    if (lokasiStreaming != '') {

      jwplayer(container).play();
      /*if (buffered_enough == 0){
           jwplayer(container).onPlay(function(){
              Buffered50(container);
           });
        }*/

      if($("#audio-1").attr("src")!=""){
        myInterval = setInterval(function(){
          myState = jwplayer(container).getState();
          if (myState=='playing') {
            $("#audio-1")[0].play();
            clearInterval(myInterval);
            return;
          };
        }, 100);
      }

        $(this).hide();
        $('#pause-1').show();
        $('#pause-1').prop('disabled', false);
      } else if($("#audio-1").attr("src") != ''){
        $("#audio-1")[0].play();
        $(this).hide();
        $('#pause-1').show();
        $('#pause-1').prop('disabled', false);
      } else {
      swal('Oops...',"You don't have source",'error');
    }
    
  });
  $('#pause-1').click(function(){
    if ( lokasiStreaming != '') {
      jwplayer(container).stop();
    }
    

    if($("#audio-1").attr("src")!=""){
            $("#audio-1")[0].pause();
            $("#audio-1")[0].currentTime = 0;
      }

    $(this).hide();
    $('#play-1').show();
  });
  // $('#retake-1').click(function(){
  //   jwplayer(container).stop();
  //   Fr.voice.stop();
  //   $('rec-1').prop('disabled', false);
  //   $('#play-1').show();
  //   $('#pause-1').hide();
  // });


  $('#add-vocal').click(function(){

    if (invited != 1) {
      $('#addVocalCon').html(
            '<div class="modal-dialog modal-sm">'+
              '<div class="modal-content">'+
                  '<div class="modal-header color-gj-popup-report">'+
                          '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'+
                          '<h4 class="modal-title">Add vocal confirmation</h4>'+
                  '</div>'+
                  '<div class="modal-body">'+
                      '<div class="row">'+
                          '<div class="col-sm-12">'+
                              '<p>Make your choice!</p>'+
                          '</div>'+
                       '</div> '+  
                  '</div>'+
                  '<div class="modal-footer">'+
                      '<a href="#" class="btn-s-gj bgc-btn add-own"style="margin-right: 5px;">Record your own</a>'+
                      '<a href="#" class="btn-s-gj bgc-btn invite-friend">Invite friend</a>'+
                  '</div>'+

              '</div>'+
          '</div>'
        );

    $('#addVocalCon').modal({
         backdrop: 'static',
         keyboard: true, 
         show: true
      });

    $(".add-own").click(function(){
      $('.voice-1').remove();
      setJWPlayer(container2, play2, pause2);
      $('.voice-2').show();
      $('#addVocalCon').modal('hide');
      $('#release-project').prop('disabled',true);
      $('#remove-vocal').prop('disabled', true);
    });

     // ---------- Invite Friend ----------------- //
    $('.invite-friend').click(function(){

      // return false;
      $('#addVocalCon').modal('hide');
      $('#inviteFriend').modal({
         backdrop: 'static',
         keyboard: true, 
         show: true
      });
    });
      // ---------- END Invite Friend ----------------- //

     $('.process-invite').click(function(){
        $(".loader-process").show();
        var tipe_invite = document.querySelector('input[name="invite_type"]:checked').value;
        
        var url = window.location.pathname; 

        var spliturl = url.split('/studio/karaoke/');
        var projectID = spliturl[1];

        if (tipe_invite == 2) {

          var friendID = $( "#friend" ).val();
          var data = new FormData();
          data.append('friendID', friendID);
          data.append('projectID', projectID);
          data.append('tipeInvite', tipe_invite);
          // var data = 'friendID=' + friendID + '&projectID=' + projectID + '&tipeInvite=' + tipe_invite;

        };

        var path = window.location.origin+'/studio/karaoke/inviteFriendTokaraoke';
        

        if (tipe_invite == 1) {
            var descProject = $('textarea#projectDescInvite').val();
            //var data = '&projectID=' + projectID + '&tipeInvite=' + tipe_invite + '&projectDescInvite=' + descProject;

            var data = new FormData();
            $.each($('#projectArtInvite')[0].files, function(i, projectArtInvite) {
              //image = 'image-'+i;
                data.append('image-'+i, projectArtInvite);
            });

            data.append('projectID', projectID);
            data.append('tipeInvite', tipe_invite);
            data.append('projectDescInvite', descProject);
        };

        console.log(data);

        $.ajax({
            url: path,
            type: 'POST',
            data: data,
            contentType: false,
            processData: false,
            success: function(result){
              $(".loader-process").hide();
              if (result.success == 1) {
                $('#inviteFriend').modal('hide');
                swal({ 
                  title: "Thank you!",
                   text: "You can redirected to studio page by clicked ok button!",
                    type: "success",
                    allowOutsideClick: false,
                  });

                $('.swal2-confirm').click(function(){
                    window.location.href = window.location.origin+"/studio";
                });
              } else{
                swal('Oops...','Something went wrong!','error');
              }
            },
            error: function(jqXHR, textStatus, errorThrown){
              console.log(jqXHR);
              $(".loader-process").hide();
              swal('Oops...','Something went wrong!','error');
          }

        });
     }) 
    } else{
        
        $('.voice-1').remove();
        setJWPlayer(container2, play2, pause2);
        $('.voice-2').show();
        $('#addVocalCon').modal('hide');
    }
    

  });

  


  //----------------- Pause Resume Record ----------------------//
  $('#pauserecord-1').click(function(){

    if (lokasiStreaming != '') {
      jwplayer(container).pause();
      console.log('State JW Pause :'+jwplayer(container).getState());
    } else {
      Fr.voice.pause();
      stateJWOnPauseRec = 'paused'
    }
    
    $(this).hide();
    $('#resrecord-1').show();
    $('#stop-1').prop('disabled', true);
  });

  $('#resrecord-1').click(function(){
    if (lokasiStreaming != '') {
      jwplayer(container).play();
      console.log('State JW Resume :'+jwplayer(container).getState());
      if (myState == 'playing' && stateJWOnPauseRec == 'paused') {
        Fr.voice.resume();
        stateJWOnPauseRec = '';
      }
    }

    if (stateJWOnPauseRec == 'paused') {
      Fr.voice.resume();
      stateJWOnPauseRec = '';
    }

    $(this).hide();
    $('#pauserecord-1').show();
    $('#stop-1').prop('disabled', false);
  });

  //----------------- End Pause Resume Record ----------------------//

  $('#finish-1').click(function(){

    $('#modalSaveConfirmation').html(
            '<div class="modal-dialog modal-sm">'+
              '<div class="modal-content">'+
                  '<div class="modal-header color-gj-popup-report">'+
                          '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'+
                          '<h4 class="modal-title">Save confirmation</h4>'+
                  '</div>'+
                  '<div class="modal-body">'+
                      '<div class="row">'+
                          '<div class="col-sm-12">'+
                              '<p>Are you sure to save this voice?</p>'+
                          '</div>'+
                       '</div> '+  
                  '</div>'+
                  '<div class="modal-footer">'+
                      '<span class="loader-process" style="display: none;margin-right: 5px;"><img src="'+window.location.origin+'/img/loader-24.gif" /></span>'+
                      '<a href="#" class="btn-s-gj bgc-btn" data-dismiss="modal" style="margin-right: 5px;">Cancel</a>'+
                      '<a href="#" class="btn-s-gj bgc-btn process-save">Yes</a>'+
                  '</div>'+

              '</div>'+
          '</div>'
        );

    $('#modalSaveConfirmation').modal({
         backdrop: 'static',
         keyboard: true, 
         show: true
      });
    $(".process-save").click(function(){
      finishAndUpload('#finish-1');
    });
    
  });

//---------------------------- END VOICE 1 --------------------------------//

//---------------------------- VOICE 2 ------------------------------------//
  $('#rec-2').click(function(){

    jwplayer(container2).stop();

    if($("#audio-2").attr("src")!=""){
        $("#audio-2")[0].pause();
        $("#audio-2")[0].currentTime = 0;
      }

          $('#pauserecord-2').show();

          $('#play-2').prop('disabled', true);
          $('#pause-2').prop('disabled', true);
          $('#rec-2').hide();
          $('#start-level-2').hide();
          $('#level-2').show();
          $('#stop-2').show();

          if (lokasiStreaming != '') {
            jwplayer(container2).play();
            myInterval = setInterval(function(){
              myState = jwplayer(container2).getState();
              if (myState == 'playing' && stateJWOnPauseRec == '') {
                startRecord(container2, '#finish-2', '#rec-2', '#stop-2', '2', '#monitor-2', '#play-2', '#pause-2', '#start-level-2', '#level-2', '#pauserecord-2', '#resrecord-2', "#audio-2");
                //timeoutFunc(durasi, container, '#finish-1', '#rec-1', '#stop-1');
                clearInterval(myInterval);
                return;
              } 
            }, 100);
          } else{
            playrecord('2', '#monitor-2');

          }

  });

  $('#stop-1').click(function(){
          stoprecord(container2, '#play-2', '#pause-2', '#finish-2', '#rec-2', '#stop-2', '#start-level-2', '#level-2', '#pauserecord-2', '#resrecord-2', "audio-2");
      });

  //----------------- Pause Resume Record ----------------------//
  $('#pauserecord-2').click(function(){

    if ( lokasiStreaming != '') {
      jwplayer(container2).pause();
      console.log('State JW Pause :'+jwplayer(container2).getState());
    } else {
      Fr.voice.pause();
      stateJWOnPauseRec = 'paused'
    }
    
    $(this).hide();
    $('#resrecord-2').show();
    $('#stop-2').prop('disabled', true);
  });

  $('#resrecord-2').click(function(){
    if ( lokasiStreaming != '') {
      jwplayer(container2).play();
      console.log('State JW Resume :'+jwplayer(container2).getState());
      if (myState == 'playing' && stateJWOnPauseRec == 'paused') {
        Fr.voice.resume();
        stateJWOnPauseRec = '';
      }
    }

    if (stateJWOnPauseRec == 'paused') {
      Fr.voice.resume();
      stateJWOnPauseRec = '';
    }

    $(this).hide();
    $('#pauserecord-2').show();
    $('#stop-2').prop('disabled', false);
  });

  //----------------- End Pause Resume Record ----------------------//

  $('#play-2').click(function(){
    console.log($("#audio-2").attr("src"));

    if ( lokasiStreaming != '') {

      jwplayer(container2).play();
      /*if (buffered_enough == 0){
           jwplayer(container).onPlay(function(){
              Buffered50(container);
           });
        }*/

      if($("#audio-2").attr("src")!=""){
        myInterval = setInterval(function(){
          myState = jwplayer(container2).getState();
          if (myState == 'playing') {
            $("#audio-2")[0].play();
            clearInterval(myInterval);
            return;
          };
        }, 100);
      }

        $(this).hide();
        $('#pause-2').show();
        $('#pause-2').prop('disabled', false);
      } else if($("#audio-2").attr("src") != ''){
        $("#audio-2")[0].play();
        $(this).hide();
        $('#pause-2').show();
        $('#pause-2').prop('disabled', false);
      } else {
      swal('Oops...',"You don't have source",'error');
    }
    
  });

  $('#pause-2').click(function(){
    
    if ( lokasiStreaming != '') {
      jwplayer(container).stop();
    }

    if($("#audio-2").attr("src")!=""){
            $("#audio-2")[0].pause();
            $("#audio-2")[0].currentTime = 0;
      }

    $(this).hide();
    $('#play-2').show();
  });

  $('#finish-2').click(function(){

    $('#modalSaveConfirmation').html(
            '<div class="modal-dialog modal-sm">'+
              '<div class="modal-content">'+
                  '<div class="modal-header color-gj-popup-report">'+
                          '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'+
                          '<h4 class="modal-title">Save confirmation</h4>'+
                  '</div>'+
                  '<div class="modal-body">'+
                      '<div class="row">'+
                          '<div class="col-sm-12">'+
                              '<p>Are you sure to save this voice?</p>'+
                          '</div>'+
                       '</div> '+  
                  '</div>'+
                  '<div class="modal-footer">'+
                      '<span class="loader-process" style="display: none;margin-right: 5px;"><img src="'+window.location.origin+'/img/loader-24.gif" /></span>'+
                      '<a href="#" class="btn-s-gj bgc-btn" data-dismiss="modal" style="margin-right: 5px;">Cancel</a>'+
                      '<a href="#" class="btn-s-gj bgc-btn process-save">Yes</a>'+
                  '</div>'+

              '</div>'+
          '</div>'
        );

    $('#modalSaveConfirmation').modal({
         backdrop: 'static',
         keyboard: true, 
         show: true
      });
    $(".process-save").click(function(){
      finishAndUpload('#finish-2');
    });
    
  });

//---------------------------- END VOICE 2 --------------------------------//
  }
});

$("#release-project").click(function(){
    
    $('#modalReleaseProject').modal({
         backdrop: 'static',
         keyboard: true, 
         show: true
      });

    $('#post-timeline').click(function() {

      if ($(this).is(":checked"))
        {
          $('#pcapt').show();
        } else{
          $('#pcapt').hide();
        }

    });
    $(".process-release").click(function(){
      releaseProject();
    });
});

$('#remove-vocal').click(function(){
  $('#modalRemoveConfirmation').html(
            '<div class="modal-dialog modal-sm">'+
              '<div class="modal-content">'+
                  '<div class="modal-header color-gj-popup-report">'+
                          '<h4 class="modal-title">Save confirmation</h4>'+
                  '</div>'+
                  '<div class="modal-body">'+
                      '<div class="row">'+
                          '<div class="col-sm-12">'+
                              '<p>Are you sure to remove your last vocal?</p>'+
                          '</div>'+
                       '</div> '+  
                  '</div>'+
                  '<div class="modal-footer">'+
                      '<span class="loader-process" style="display: none;margin-right: 5px;"><img src="'+window.location.origin+'/img/loader-24.gif" /></span>'+
                      '<a href="#" class="btn-s-gj bgc-btn" data-dismiss="modal" style="margin-right: 5px;">Cancel</a>'+
                      '<a href="#" class="btn-s-gj bgc-btn process-remove-vocal">Yes</a>'+
                  '</div>'+

              '</div>'+
          '</div>'
        );
  $('#modalRemoveConfirmation').modal({
         backdrop: 'static',
         keyboard: true, 
         show: true
      });
    $(".process-remove-vocal").click(function(){
      removeVocal();
    });

});

$('#release-backsong').click(function(){
  $('#modalReleaseBacksong').show();
  $('#modalReleaseBacksong').modal({
         backdrop: 'static',
         keyboard: true, 
         show: true
      });
    $(".process-release-backsong").click(function(){
      uploadAndRelease();
    });

});
//---------------------------- END LOAD PAGE --------------------------------//


//------------UPLOAD AND RELEASE BACKSONG--------------//
function uploadAndRelease(){

    console.log('start finishing, uploading and releasing backsong');
    console.log('projectID: '+projectIDBack);

    var genre = $('#genreBack option:selected').val();
    var price =  $('#priceBack option:selected').val();
    // var is_public = $('#is_publicBack option:selected').val();
    var trackLyric = CKEDITOR.instances['lirikback'].getData();

    // console.log('lirik: '+trackLyric);

    Fr.voice.export(function(blob){
    var formData = new FormData();
    formData.append('file', blob);
    formData.append('projectID', projectIDBack);

    formData.append('genre', genre);
    formData.append('price', price);
    // formData.append('is_public', is_public);
    formData.append('trackLyric', trackLyric);

    $.each($('#artWorkReleaseBackSong')[0].files, function(i, artWorkReleaseBackSong) {
          //image = 'image-'+i;
            formData.append('image-'+i, artWorkReleaseBackSong);
        });

    console.log('BLOB: '+blob);

    console.log(formData);

    $(".loader-process").show();
    $.ajax({
      url: window.location.origin+"/studio/karaoke/uploadBackSong",
      type: 'POST',
      data: formData,
      timeout:500000,
      contentType: false,
      processData: false,
      success: function(url) {
        /*alert(url);
        console.log('upload done');*/
        if(url.status == 1 || url.status == '1'){
           $('#modalReleaseBacksong').modal('hide');
          swal({ 
            title: "Success release Backsong!",
            text: "Please refresh this page by clicked OK button!",
            type: "success",
            allowOutsideClick: false,
          });
          $('.swal2-confirm').click(function(){
             window.location.href = window.location.origin+"/studio";
          });
          $('#release-backsong').prop('disabled', true);
        } else {
          swal('Oops...', url.message,'error');
        }
       
      },
      error: function(jqXHR, textStatus, errorThrown){
              console.log(jqXHR);
              $(".loader-process").hide();
              swal('Oops...','Something went wrong!','error');
          }
    });
  }, "blob");
  Fr.voice.stop();
}
//////////////END UPLOAD AND RELEASE BACKSONG///////////////