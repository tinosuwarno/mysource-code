// JavaScript Document

    $(document).on('ready pjax:success', function () {
        $('.kv-fa').rating({
            theme: 'krajee-fa',
            filledStar: '<i class="fa fa-star"></i>',
            emptyStar: '<i class="fa fa-star-o"></i>',
			showCaption :false,
			showClear : false,
			hoverEnabled : false,
			readOnly :true
        });
        $(document).on('change', '.rating,.kv-fa', function () {
            // console.log('Rating selected: ' + $(this).val());
        });
    });

