// JavaScript Document
$(document).ready(function() {
    $('.navbar').affix({
    offset: {
      top: 125
    }
  });
});
$(document).ready(function() {
    $(window).stellar({
    horizontalScrolling: false,
    parallaxBackgrounds: true,
    //verticalOffset: 20,
    responsive: true
  });
  
});

$(document).ready(

  function() { 

    $("html").niceScroll({
        cursorcolor:"rgba(30,30,30,.5)",
        zindex:999,
        scrollspeed:100,
        mousescrollstep:50,
        cursorborder:"0px solid #fff",
    });
      

  }

);

function clickLike() {
    $(this).toggleClass("clicked-like");
} 


$( function() {
  $('.like-single-list-song').click( function() {
    $(this).toggleClass("clicked-like");
  } );
} );

$(document).ready(function(){
  $(".owl-carousel-header").owlCarousel({
    items : 1,
  lazyLoad:true,
    loop:true,
  autoplay : true,
  autoplayHoverPause : true,
  autoplayTimeout : 5000000
  });
});

$(document).ready(function(){
  $(".owl-carousel").owlCarousel({
    items : 1,
  lazyLoad:true,
    loop:true,
  autoplay : true,
  autoplayHoverPause : true,
  autoplayTimeout : 5000
  });
});

$(document).ready(function(){
  $(".owl-studio").owlCarousel({
    items : 1,
  lazyLoad:true,
    loop:true,
  autoplay : true,
  autoplayHoverPause : true,
  autoplayTimeout : 50000
  });
});

$(document).ready(function(){
  var owlListHome = $(".owl-carousel-list");
    owlListHome.owlCarousel({
    
    items : 1,
  lazyLoad:true,
    loop:true,
    margin:10,
  autoplay : true,
  autoplayHoverPause : true,
  autoplayTimeout : 50000
  });
  
  
  $(".prev-list-home").click(function(){
    owlListHome.trigger('next.owl.carousel');
  })
  $(".next-list-home").click(function(){
    owlListHome.trigger('prev.owl.carousel');
  })
});

$(document).ready(function() {
 
  var owlPartner = $(".owl-partner-cont");
 
  owlPartner.owlCarousel({
      items : 7, 
    loop:true,
    margin:40,
    autoplay : true,
    autoplayHoverPause : true,
    autoplayTimeout : 50000,
      responsiveClass:true,
    responsive:{
        0:{
            items:2,
        },
    481:{
      items:3,
    },
    601:{
      items:4,
    },
        769:{
            items:5,
        },
        1025:{
            items:7,
        }
    }
      /*itemsDesktop : [1199,6],
      itemsDesktopSmall : [979,5], 
      itemsTablet: [768,4],
      itemsMobile : [479,1] */
  });

  $(".next-list-partner-h").click(function(){
    owlPartner.trigger('next.owl.carousel');
  })
  $(".prev-list-partner-h").click(function(){
    owlPartner.trigger('prev.owl.carousel');
  })
});


$(document).ready(function() {
 
  var owl = $(".owl-track-cat");
 
  owl.owlCarousel({
      items : 7, 
      responsiveClass:true,
    responsive:{
        0:{
            items:1,
        },
    481:{
      items:2,
    },
    601:{
      items:4,
    },
        769:{
            items:5,
        },
        1025:{
            items:7,
        }
    },
      /*itemsDesktop : [1199,6],
      itemsDesktopSmall : [979,5], 
      itemsTablet: [768,4],
      itemsMobile : [479,1] */
  /*loop:true,
  autoplay:true,
    autoplayTimeout:1000,
    autoplayHoverPause:true*/
  });

  $(".next").click(function(){
    owl.trigger('next.owl.carousel');
  })
  $(".prev").click(function(){
    owl.trigger('prev.owl.carousel');
  })
});

$(document).ready(function() {
 
  var owlmarket = $(".owl-market");
  owlmarket.owlCarousel({
 
      autoPlay: 3000, //Set AutoPlay to 3 seconds
      center: true,
      loop: true,
      items : 2,
      itemsDesktop : [1199,3],
      itemsDesktopSmall : [979,3]
 
  });

  $(".next-market").click(function(){
    owlmarket.trigger('next.owl.carousel');
  })
  $(".prev-market").click(function(){
    owlmarket.trigger('prev.owl.carousel');
  })
 
});

$(document).ready(function() {
 
  var owlmostdl = $(".owl-most-dl");
  owlmostdl.owlCarousel({
      items : 6,
    margin : 10,
    responsive:{
        0:{
            items:1,
        },
    481:{
      items:2,
    },
    601:{
      items:4,
    },
        769:{
            items:4,
        },
        1024:{
            items:6,
        }
    }
    });
 
});


$(document).ready(function() {
    
    $('.collapse').on('shown.bs.collapse', function(){
  $(this).parent().find(".fa-chevron-right").removeClass("fa-chevron-right").addClass("fa-chevron-down");
  }).on('hidden.bs.collapse', function(){
  $(this).parent().find(".fa-chevron-down").removeClass("fa-chevron-down").addClass("fa-chevron-right");
  });

});

// $(document).ready(function() {
 
//   var owlCard = $(".warp-owl-card");
 
//   owlCard.owlCarousel({
//       items : 3, 
//     loop:false,
//     margin:20,
//     /*autoplay : true,
//     autoplayHoverPause : true,
//     autoplayTimeout : 50000,*/
//       responsiveClass:true,
//     responsive:{
//         0:{
//             items:1,
//       stagePadding:40,
//       loop:true,
//         margin:10,
//         },
//     481:{
//       items:1,
//       stagePadding:40,
//       loop:true,
//         margin:10,
//     },
//     601:{
//       items:2,
//       stagePadding:40,
//       loop:true,
//       margin:20,
//     },
//         769:{
//             items:2,
//       stagePadding:40,
//       loop:true,
//       margin:20,
//         },
//         991:{
//             items:3,
//         }
//     }
//       /*itemsDesktop : [1199,6],
//       itemsDesktopSmall : [979,5], 
//       itemsTablet: [768,4],
//       itemsMobile : [479,1] */
//   });

//  /* $(".next-list-partner-h").click(function(){
//     owlPartner.trigger('next.owl.carousel');
//   })
//   $(".prev-list-partner-h").click(function(){
//     owlPartner.trigger('prev.owl.carousel');
//   })*/
// });

$("#btn-ancr").click(function() {
    $('html, body').animate({
        scrollTop: $("#land-1").offset().top
    }, 1000);
});




/*======================================= MODAL PAGE DEMO / TRACK ============================================== */
$(document).ready(function(){
    $('.btn-report-ts').click(function(){
      $('#reportTrackSongModal').modal('show');
    });
    
    $('.btn-share-ts').click(function(){
      $('#shareSongTrack').modal('show');
    });
    
    $('.subButton').click(function(){
      if($('#alreadyGritjam').is(':checked')) { 
      $('#searchReportTrackSong').modal('show');
      $('body').addClass("tot");
      //$('#reportTrackSongModal').modal('hide'); 
      }
      /*$('#reportTrackSongModal').on('hidden.bs.modal', function () {
        // Load up a new modal...
        $('#searchReportTrackSong').modal('show');
      }); */
      
      if($('#offensiveContent').is(':checked')) { 
      $('#offReportTrackSong').modal('show');
      $('body').addClass("tot");
      }
    });
    
    /*$('.btn-kar-vid').click(function(){
      $('#startKaraoke').modal('show');
    });*/
    
    $('.karoke-v').click(function(){
      $('#selectSongKaroke').modal('show');
      $('#startKaraoke').modal('hide');
      $('body').addClass("tot");
    });
    
    $('.karoke-wv').click(function(){
      $('#selectSongKaroke').modal('show');
      $('#startKaraoke').modal('hide');
      $('body').addClass("tot");
    });
    
    $('.get-featured-track').click(function(){
      $('#addFeaturedTrack').modal('show');
    });
    
    $('.crt-group').click(function(){
      $('#createGroup').modal('show');
      $('body').addClass("tot");
    });
    
    // $('.invite-friend').click(function(){
    //   $('#inviteFriend').modal('show');
    //   $('body').addClass("tot");
    // });
    
    $('.req-group').click(function(){
      $('#reqJoinGroup').modal('show');
      $('body').addClass("tot");
    });
    
  });
$(document).ready(function() {
    $('.modal button.close').on('click', function() {
        $('.modal').modal('hide');
    $('body').removeClass("tot");
    });
});
$(document).ready(function(){
    
    $('.btnCrt').click(function(){ 
      $("#crt-signModal").modal('show');
      $('#reg-signModal').modal('hide');  
      $('body').addClass("tot");
      /*$('#reg-signModal').on('hidden.bs.modal', function () {
        // Load up a new modal...
        $('#crt-signModal').modal('show');
      }); */    
    });
    
  });

$(document).ready(function(){
    /*document.getElementById('closeWarning').onclick = function(){
        this.parentNode.parentNode.parentNode
        .removeChild(this.parentNode.parentNode);
        return false;
      };*/
    $('.close-warning').on('click', function(e) { 
        $('.error-w').remove(); 
    $('.sucses-w').remove(); 
    });
});

$(window).load(function(){
  $('#advHeadset').modal('show');
});
/*======================================= DETAIL PAGE DEMO / TRACK ============================================== */
var countToggle = 0;
  $(document).ready(function(){
    

    $('.show-detail-song').on('click',function(){
      var clickIndex = $(this).attr('id');
      console.log(clickIndex);
      if(clickIndex%2!=0){
        if(countToggle%2==0){
          $('.gj-song-card[data-song-index="'+clickIndex+'"]').addClass('active');
          $('.gj-song-detail.dsong').css('display','block').addClass('on-right');
        }else{
          $('.gj-song-card[data-song-index="'+clickIndex+'"]').removeClass('active');
          $('.gj-song-detail.dsong').removeClass('on-right');
          $('.gj-song-detail.dsong').css('display','none');
        }
        countToggle+=1;
        
      }else if(clickIndex%2==0){
        if(countToggle%2==0){
          $('.gj-song-card[data-song-index="'+clickIndex+'"]').addClass('active');
          $('.gj-song-detail.dsong').css('display','block').addClass('on-left');
        }else{
          $('.gj-song-card[data-song-index="'+clickIndex+'"]').removeClass('active');
          $('.gj-song-detail.dsong').removeClass('on-left');
          $('.gj-song-detail.dsong').css('display','none');
        }
        countToggle+=1;
      }

      $('.btn.fa-close').on('click', function(){
      // var clickIndex = $('.show-detail').attr('id');
        $('.gj-song-card[data-song-index="'+clickIndex+'"]').removeClass('active');
        $('.gj-song-detail.dsong').removeClass('on-left');
        $('.gj-song-detail.dsong').removeClass('on-right');
        $('.gj-song-detail.dsong').css('display','none');
        
        countToggle+=1;
      });
    });
        
        $('.show-detail-track').on('click',function(){
            var clickIndex = $(this).attr('id');
            console.log(clickIndex);
            if(clickIndex%2!=0){
                if(countToggle%2==0){
                    $('.gj-song-card[data-song-index="'+clickIndex+'"]').addClass('active');
                    $('.gj-song-detail.dtrack').css('display','block').addClass('on-right');
                }else{
                    $('.gj-song-card[data-song-index="'+clickIndex+'"]').removeClass('active');
                    $('.gj-song-detail.dtrack').removeClass('on-right');
                    $('.gj-song-detail.dtrack').css('display','none');
                }
                countToggle+=1;
                
            }else if(clickIndex%2==0){
                if(countToggle%2==0){
                    $('.gj-song-card[data-song-index="'+clickIndex+'"]').addClass('active');
                    $('.gj-song-detail.dtrack').css('display','block').addClass('on-left');
                }else{
                    $('.gj-song-card[data-song-index="'+clickIndex+'"]').removeClass('active');
                    $('.gj-song-detail.dtrack').removeClass('on-left');
                    $('.gj-song-detail.dtrack').css('display','none');
                }
                countToggle+=1;
            }

            $('.btn.fa-close').on('click', function(){
            // var clickIndex = $('.show-detail').attr('id');
                $('.gj-song-card[data-song-index="'+clickIndex+'"]').removeClass('active');
                $('.gj-song-detail.dtrack').removeClass('on-left');
                $('.gj-song-detail.dtrack').removeClass('on-right');
                $('.gj-song-detail.dtrack').css('display','none');
                
                countToggle+=1;
            });
        });
        
        $('.show-detail-fcontent').on('click',function(){
            var clickIndex = $(this).attr('id');
            console.log(clickIndex);
            if(clickIndex%2!=0){
                if(countToggle%2==0){
                    $('.gj-song-card[data-song-index="'+clickIndex+'"]').addClass('active');
                    $('.gj-song-detail.dfcontent').css('display','block').addClass('on-right');
                }else{
                    $('.gj-song-card[data-song-index="'+clickIndex+'"]').removeClass('active');
                    $('.gj-song-detail.dfcontent').removeClass('on-right');
                    $('.gj-song-detail.dfcontent').css('display','none');
                }
                countToggle+=1;
                
            }else if(clickIndex%2==0){
                if(countToggle%2==0){
                    $('.gj-song-card[data-song-index="'+clickIndex+'"]').addClass('active');
                    $('.gj-song-detail.dfcontent').css('display','block').addClass('on-left');
                }else{
                    $('.gj-song-card[data-song-index="'+clickIndex+'"]').removeClass('active');
                    $('.gj-song-detail.dfcontent').removeClass('on-left');
                    $('.gj-song-detail.dfcontent').css('display','none');
                }
                countToggle+=1;
            }

            $('.btn.fa-close').on('click', function(){
            // var clickIndex = $('.show-detail').attr('id');
                $('.gj-song-card[data-song-index="'+clickIndex+'"]').removeClass('active');
                $('.gj-song-detail.dfcontent').removeClass('on-left');
                $('.gj-song-detail.dfcontent').removeClass('on-right');
                $('.gj-song-detail.dfcontent').css('display','none');
                
                countToggle+=1;
            });
        });

        $('.show-detail').on('click',function(){
            var clickIndex = $(this).attr('id');
            console.log(clickIndex);
            if(clickIndex%2!=0){
                if(countToggle%2==0){
                    $('.gj-song-card[data-song-index="'+clickIndex+'"]').addClass('active');
                    $('.gj-song-detail.dwish').css('display','block').addClass('on-right');
                }else{
                    $('.gj-song-card[data-song-index="'+clickIndex+'"]').removeClass('active');
                    $('.gj-song-detail.dwish').removeClass('on-right');
                    $('.gj-song-detail.dwish').css('display','none');
                }
                countToggle+=1;
                
            }else if(clickIndex%2==0){
                if(countToggle%2==0){
                    $('.gj-song-card[data-song-index="'+clickIndex+'"]').addClass('active');
                    $('.gj-song-detail.dwish').css('display','block').addClass('on-left');
                }else{
                    $('.gj-song-card[data-song-index="'+clickIndex+'"]').removeClass('active');
                    $('.gj-song-detail.dwish').removeClass('on-left');
                    $('.gj-song-detail.dwish').css('display','none');
                }
                countToggle+=1;
            }

            $('.btn.fa-close').on('click', function(){
            // var clickIndex = $('.show-detail').attr('id');
                $('.gj-song-card[data-song-index="'+clickIndex+'"]').removeClass('active');
                $('.gj-song-detail.dwish').removeClass('on-left');
                $('.gj-song-detail.dwish').removeClass('on-right');
                $('.gj-song-detail.dwish').css('display','none');
                
                countToggle+=1;
            });
        });

  });

    
  
  function displayDetail(){
    $('.gj-track-title').on('click', function(){
        $('.msg-empty').hide();
        $('.gj-track-detail .gj-song-card').css('display', 'block');
      
    });
  }
  
  $(document).ready(function() {
        displayDetail();
    });
//$(document).ready(function() {
  //var elemHTML = "<i class='fa fa-times'>" + "</i>";
  
  //$('button.stat-friend ').hover(function(){
    //var element = document.getElementsByClassName("btn-add-f-list");
     //$button = $(this);
    //if($button.hasClass('stat-friend')){
      //$("button.btn-add-f-list").append("<i></i>");
      //$("button.btn-add-f-list i").addClass('fa fa-times');
      //$button.addClass('stat-unfriend');
      //element.append('<i class="fa fa-times"></i>');
      //$button.text('Unfriend');
      //element.innerHTML = '<i class="fa fa-times"></i> Unfriend';     
    //}
  //}, function(){
    //var element = document.getElementsByClassName("btn-add-f-list");
    //if($button.hasClass('stat-friend')){
      //$button.removeClass('stat-unfriend'); 
      //$button.append('<i class="fa fa-times"></i>');  
      //$button.text('Friend');
      //element.innerHTML = '<i class="fa fa-check"></i> Friend';
    //}
  //});
//});
/*$(document).ready(function(){
  jQuery('a[href^="#"]').click(function(e) {
 
    jQuery('html,body').animate({ scrollTop: jQuery(this.hash).offset().top}, 1000);
 
    return false;
 
    e.preventDefault();
 
});

});*/

   //===============================//
   
/*var txt = $('#comments'),
    hiddenDiv = $(document.createElement('div')),
    content = null;

txt.addClass('txtstuff');
hiddenDiv.addClass('hiddendiv common');

$('body').append(hiddenDiv);

txt.on('keyup', function () {

    content = $(this).val();

    content = content.replace(/\n/g, '<br>');
    hiddenDiv.html(content + '<br class="lbr">');

    $(this).css('height', hiddenDiv.height());

});*/
/*if (window.addEventListener) window.addEventListener('DOMMouseScroll', wheel, false);
window.onmousewheel = document.onmousewheel = wheel;

var time = 1300;
var distance = 270;

function wheel(event) {
    if (event.wheelDelta) delta = event.wheelDelta / 120;
    else if (event.detail) delta = -event.detail / 3;

    handle();
    if (event.preventDefault) event.preventDefault();
    event.returnValue = false;
}

function handle() {

    $('html, body').stop().animate({
        scrollTop: $(window).scrollTop() - (distance * delta)
    }, time);
}


$(document).keydown(function (e) {

    switch (e.which) {
        //up
        case 38:
            $('html, body').stop().animate({
                scrollTop: $(window).scrollTop() - distance
            }, time);
            break;

            //down
        case 40:
            $('html, body').stop().animate({
                scrollTop: $(window).scrollTop() + distance
            }, time);
            break;
    }
});*/

/*$(document).ready(function() { 

    $('a[href="#toggle-search"], .navbar-bootsnipp .bootsnipp-search .input-group-btn > .btn[type="reset"]').on('click', function(event) {
    event.preventDefault();
    $('.navbar-bootsnipp .bootsnipp-search .input-group > input').val('');
    $('.navbar-bootsnipp .bootsnipp-search').toggleClass('open');
    $('a[href="#toggle-search"]').closest('li').toggleClass('active');

    if ($('.navbar-bootsnipp .bootsnipp-search').hasClass('open')) {*/
      /* I think .focus dosen't like css animations, set timeout to make sure input gets focus */
/*      setTimeout(function() { 
        $('.navbar-bootsnipp .bootsnipp-search .form-control').focus();
      }, 100);
    }     
  });

  $(document).on('keyup', function(event) {
    if (event.which == 27 && $('.navbar-bootsnipp .bootsnipp-search').hasClass('open')) {
      $('a[href="#toggle-search"]').trigger('click');
    }
  });
    
});*/