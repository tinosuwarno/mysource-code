var errors;
var countEventExplore; 
var takeEventExplore;
var skipEventExplore;

var countInvitationExplore;
var takeInvitationExplore;
var skipInvitationExplore;

var base_url = window.location.origin;
function setValueEventExplore(){
	this.countEventExplore;
	this.takeEventExplore = 3;
	this.skipEventExplore = 0;

  this.countInvitationExplore;
  this.takeInvitationExplore = 3;
  this.skipInvitationExplore = 0;

}

function checkElementExplore(){
	var elementExists = new Array(
      document.getElementById('loadingFriendExplore'),
      document.getElementById('loadingEventExplore1'),  //untuk event explore
      document.getElementById('loadingEventExplore2')   //untuk invitation explore
    );
    var stop = false;
    for (var i = elementExists.length - 1; i >= 0; i--) {
      if(elementExists[i] == null)
        stop = true;
    };

    if(stop)
      return 0;
    return 1;
}


$(document).on('click','.addFriend',function(){
	var elementAdd = $(this);
	var parent = elementAdd.parent().attr('class');
	var userID = elementAdd.attr('id');

	var url = window.location.origin+'/addToFriend/'+userID;

	$.ajax({
  	url: url,
  	type: 'POST',
  	data: {friendID:userID},
  	success: function(responseText){
      // console.log(responseText); return;
      if(responseText == 0){
        elementAdd.parent().html(
          '<button id="'+userID+'" type="button" class="btn btn-add-f-list col-btn-ex"><i class="fa fa-check"></i>Request sent</button>'
        );

        elementAdd.remove();
        swal({ 
          title: "Error",
          text: "The request friend already sent.",
          type: "error",
        });     
      }
      else if(responseText == 1){
        elementAdd.parent().html(
          '<button id="'+userID+'" type="button" class="btn btn-add-f-list col-btn-ex"><i class="fa fa-check"></i>Request sent</button>'
        );

        elementAdd.remove();
        swal({ 
          title: "Thank you!",
          text: "your request was sent , please waiting for confirmation",
          type: "success",
        });     
      }
      else{
        window.location.href = base_url + '/login';
      }
  	},
  	error: function(jqXHR, textStatus, errorThrown){
      swal('Oops...','Something went wrong!','error');
  	}
	});
});

$(document).on('ready pjax:success', function(){
	var explore = checkElementExplore();
	if(explore == 0) return;
	getFriendExplore(1);

  setValueEventExplore();
	getEventExplore(1);  // untuk event
	getEventExplore(2);  // untuk invitation
});

$(document).on('ready pjax:success', function(){
	$('#refreshExplore').on('click', function(){
		$('#refreshThis').replaceWith("<div class='warp-owl-card' id='refreshThis'>"+
							            "<div id='loadingFriendExplore'>"+
								            "<div class='col-xs-push-5 col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 lazyloader-list'>"+
							                    "<img src='img/loader-more.gif' class='img-full-responsive e-centering width-loader'>"+
							                "</div>"+
							            "</div>"+
							          "</div>");
		getFriendExplore(1);
		return false;
	});
});

$(document).on('ready pjax:success', function(){
	$('#proximityExplore').on('click', function(){
		$('#refreshThis').replaceWith("<div class='warp-owl-card' id='refreshThis'>"+
							            "<div id='loadingFriendExplore'>"+
								            "<div class='col-xs-push-5 col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 lazyloader-list'>"+
							                    "<img src='img/loader-more.gif' class='img-full-responsive e-centering width-loader'>"+
							                "</div>"+
							            "</div>"+
							          "</div>");
		getFriendExplore(2);
		return false;
	});
});
/* Ajax Call */
function getFriendExplore(type){
	$.ajax({
	    type  	: 'POST',
	    data	: {'type':type},
	    url   	: '/explorer/friend',
	    success : function(responseText){
	      	// console.log(responseText); return;

	      	if(responseText == -1){
             swal({ 
                title: "Sorry...!",
                text: "You must login first",
                type: "warning",
                allowOutsideClick: false,
              });

              $('body').on('click','.swal2-confirm',function(){
                  window.location.href = window.location.origin+'/login';
              });  
	      		return;
	      	}
	      	else if(responseText == -2){
	      		swal('Oops...','You must set your city first','error');
	      		return;
	      	}

	      	$('#loadingFriendExplore').replaceWith(responseText);
      		  var owlCard = $(".warp-owl-card");
			  owlCard.owlCarousel({
			      items : 3, 
			    loop:false,
			    margin:20,
			    /*autoplay : true,
			    autoplayHoverPause : true,
			    autoplayTimeout : 50000,*/
			      responsiveClass:true,
			    responsive:{
			        0:{
			            items:1,
			      stagePadding:40,
			      loop:true,
			        margin:10,
			        },
			    481:{
			      items:1,
			      stagePadding:40,
			      loop:true,
			        margin:10,
			    },
			    601:{
			      items:2,
			      stagePadding:40,
			      loop:true,
			      margin:20,
			    },
			        769:{
			            items:2,
			      stagePadding:40,
			      loop:true,
			      margin:20,
			        },
			        991:{
			            items:3,
			        }
			    }
			      /*itemsDesktop : [1199,6],
			      itemsDesktopSmall : [979,5], 
			      itemsTablet: [768,4],
			      itemsMobile : [479,1] */
			  });
	  	},
        error : function(jqXHR, textStatus, errorThrown){
            console.log(jqXHR);
            swal('Oops...','Something went wrong! Please reload this page.','error');
        }
 	});
}


function setTakeTotalEventExplore(total){
	var skip = this.skipEventExplore;
	var take = this.takeEventExplore;

	skip = skip + skip;
	if(skip + take > total){
	    take = total - skip + 1;
		  this.takeEventExplore = take;
		  this.countEventExplore = total;
	} 
}

function setLinkEventExplore(){
	var content;
	if(this.skipEventExplore < this.countEventExplore){
		content = LoadMoreLinkExplore()
		$('#LoadEventExplore').append(content);
	}
}

function LoadMoreLinkExplore(){
    var content = "";
    content += "<div class='warp-btn-loadmore-list' id='moreEventExplore'>";
    content += "<a class='btn-loadmore-list' onclick='getEventExplore();'>";
    content += "<i class='fa fa-plus-circle'></i>";
    content += "<span class='text-load-more-list'>LOAD MORE</span>";
    content += "</a>";
    content += "</div>";
    return content;
}
function getDataEventExplore(type){
	var data = {};
  switch(type) {
      case 1:
         data['skip'] = this.skipEventExplore;
         data['take'] = this.takeEventExplore;
         data['orderBy'] = 'created_at';
         data['optionOrder'] = 'desc';
         break;
      case 2:
         data['skip'] = this.skipInvitationExplore;
         data['take'] = this.takeInvitationExplore;
         data['orderBy'] = 'created_at';
         data['optionOrder'] = 'desc';   
         break;
      default:
         this.errors = true;
         break;    
  }
	
    return data;
}

function getLinkExplorePage(type){
    var link;
    switch(type) {
        case 1:
           link = base_url+"/EventExplore";
           break;
        case 2:
           link = base_url+"/invitationExplore";
           break;
        default:
           this.errors = true;
           break;       
    }
    return link;
}

function getContentExplorePage(type, data){
    var content = "";
    switch(type) {
        case 1:
            content += printEventExplore(data);
            break;
        case 2:
            content += printInvitationExplore(data);
        default:
            this.errors = true;
            break;        
    }
    return content;
}


function printEventExplore(data){
	var content ="";

	  var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
    var firstDate = new Date(data['end_time']);
    var secondDate = new Date;
    var diffDays = Math.round(Math.round((secondDate.getTime() - firstDate.getTime()) ));

    var statusMemberEventParticipatePrint = "<div class='btn-in-stat' id='groupEventParticipant"+data['event_id']+"'>"+
                                            "<input type='button' onclick='sudahParticipate();' class='btn btn-line-stat col-btn-ex' value='participated'>"+
                                            "</div>";
    var statusMemberEventNotParticipatePrint = "<div class='btn-in-stat' id='groupEventParticipant"+data['event_id']+"'>"+
                                               "<button type='button' onclick='groupEventParticipant("+'"'+data['groupID']+'"'+", "+'"'+data['event_id']+'"'+","+'"1"'+");' class='btn btn-line-stat col-btn-ex'>Participate</button>"+
                                               "</div>";                        
    var statusMemberGroupJoin = "<div class='btn-in-stat' id='groupEventParticipant"+data['event_id']+"'>"+
                                "<button type='button' onclick='harusJoinGroup("+'"'+data['groupID']+'"'+", "+'"'+data['event_id']+'"'+");' class='btn btn-line-stat col-btn-ex'>Participate</button>"+
                                "</div>";  
    var statusMemberNotLogin = "<div class='btn-in-stat' id='groupEventParticipant"+data['event_id']+"'>"+
                               "<button type='button' class='btn btn-line-stat col-btn-ex' onclick='harusLogin();'>Participate</button>"+
                               "</div>";                           
     
   
    content += "<div class='gj-song-wrap'>";
    content += "<div class='gj-song-card'>";
    content += "<div class='gj-song-cover'>";
    if(data['picture_event'] != 0){
       content += "<img src='"+base_url+"/"+data['picture_event']+"' class='img-responsive'>";
    }else{
       content += "<img src='"+base_url+"/img/photo-event/default_group_event_475x475.jpg' class='img-responsive'>";
    }
    content += "</div>";
    content += "<div class='gj-song-desc'>";
    content += "<div class='gj-song-story'>";
    content += "<h1 class='gj-song-title'><a id='pjax' data-pjax='yes' href='"+base_url+"/event/"+data['event_id']+"'>"+data['event_title']+"</a></h1>";
    content += "</div>";
    content += "<div class='gj-text-desc'>";
    content += "<p class='revs'>"+data['description_event']+"</p>";
    content += "<p class='revs'>particpants : "+data['peserta']+"</p>";
    content += "</div>";
    content += "</div>";
    content += "<div class='warp-stat-card top-stat-card'>";
    
    
    if(data['likedGroupEvent'] === 1 || data['likedGroupEvent'] === '1'){
      content += "<div class='stat-song-track' id='groupEventLiked"+data['event_id']+"'>";
      content += "<div class='warp-ico-stat'>";
      content += "<div class='ico-stat'><a href='javascript:void(0)'' onclick='udahLikePost();'><i class='fa fa-heart like-single-list-song' style='color:red;'></i></a></div>";
      content += "<div class='num-stat'>"+data['liked']+"</div>";
      content += "</div>";
      content += "</div>";
    }else if(data['likedGroupEvent'] === 0 || data['likedGroupEvent'] === '0'){
      content += "<div class='stat-song-track' id='groupEventLiked"+data['event_id']+"'>";
      content += "<div class='warp-ico-stat'>";
      content += "<div class='ico-stat'><a href='javascript:void(0)'' onclick='clickLikeGroupEvent("+data['event_id']+","+data['liked']+");'><i class='fa fa-heart like-single-list-song'></i></a></div>";
      content += "<div class='num-stat'>"+data['liked']+"</div>";
      content += "</div>";
      content += "</div>";
    }else{
      content += "<div class='stat-song-track' id='groupEventLiked"+data['event_id']+"'>";
      content += "<div class='warp-ico-stat'>";
      content += "<div class='ico-stat'><a href='"+base_url+"/login'><i class='fa fa-heart like-single-list-song'></i></a></div>";
      content += "<div class='num-stat'>"+data['liked']+"</div>";
      content += "</div>";
      content += "</div>";
    }
    if(data['checkStatusMember'] === ''){
      content += "<div class='stat-song-track'>";
      content += "<div id='sharedEvent"+data['event_id']+"' class='warp-ico-stat'>";
      content += "<div class='ico-stat'><a href='"+base_url+"/login' style='cursor:pointer;'><i class='fa fa-mail-forward forward-single-list-song'></i></a></div>";
      content += "<div class='num-stat'>"+data['shared']+"</div>";
      content += "</div>";
      content += "</div>";
    }else{
      content += "<div class='stat-song-track'>";
      content += "<div id='sharedEvent"+data['event_id']+"' class='warp-ico-stat'>";
      content += "<div class='ico-stat'><a onclick='sharedEventToSocial("+data['event_id']+","+data['shared']+");' style='cursor:pointer;'><i class='fa fa-mail-forward forward-single-list-song'></i></a></div>";
      content += "<div class='num-stat'>"+data['shared']+"</div>";
      content += "</div>";
      content += "</div>";
    }
    
    content += "<span id='eventTitle"+data['event_id']+"' style='display:none;'>"+data['event_title']+"</span>";
    content += "<span id='eventDescription"+data['event_id']+"' style='display:none;'>"+data['description_event']+"</span>";
    content += "<span id='eventPicture"+data['event_id']+"' style='display:none;'>"+data['picture_event']+"</span>";

    content += "<span id='trackID-groupEvent"+data['event_id']+"' style='display:none;'>"+data['trackID']+"</span>";
    content += "<span id='judulTrack-groupEvent"+data['event_id']+"' style='display:none;'>"+data['judulTrack']+"</span>";
    content += "<span id='instrumentTrack-groupEvent"+data['event_id']+"' style='display:none;'>"+data['instrument']+"</span>";
    content += "<span id='creatorTrack-groupEvent"+data['event_id']+"' style='display:none;'>"+data['creatorTrack']+"</span>";
    content += "<span id='priceTrack-groupEvent"+data['event_id']+"' style='display:none;'>"+data['price']+"</span>";
    content += "<span id='eventTitle-groupEvent"+data['event_id']+"' style='display:none;'>"+data['event_title']+"</span>";
    content += "<span id='groupName-groupEvent"+data['event_id']+"' style='display:none;'>"+data['group_name']+"</span>";

    if(data['checkadmin'] === 0 || data['checkadmin'] === ''){
        content += statusMemberNotLogin;
    }else{
         if(data['checkadmin'] === 'not admin'){
              if(diffDays < 0) {
                   if(data['group_type'] === 1 || data['group_type'] === '1'){  // check group public
                        if(data['checkStatusMember'] === 'member'){
                             if(data['event_type'] === 1 || data['event_type'] === '1'){    //check event public
                                  if(data['statusMemberEventParticipate'] === 1 || data['statusMemberEventParticipate'] === '1'){
                                       content += statusMemberEventParticipatePrint;
                                  }else{
                                       content += statusMemberEventNotParticipatePrint;
                                  }
                             }else{
                                  if(data['statusMemberEventParticipate'] === 1 || data['statusMemberEventParticipate'] === '1'){
                                       content += statusMemberEventParticipatePrint;
                                  }else{
                                       content += statusMemberEventNotParticipatePrint;
                                  }
                             }
                        }else{
                             if(data['event_type'] === 1 || data['event_type'] === '1'){
                                  if(data['statusMemberEventParticipate'] === 1 || data['statusMemberEventParticipate'] === '1'){
                                       content += statusMemberEventParticipatePrint;
                                  }else{
                                       content += statusMemberEventNotParticipatePrint;
                                  }
                             }else{
                                  if(data['statusMemberEventParticipate'] === 1 || data['statusMemberEventParticipate'] === '1'){
                                       content += statusMemberEventParticipate;
                                  }else{
                                       content += "<div class='btn-in-stat'>";
                                       content += "</div>";
                                  }
                             }
                        }
                   }else{
                        if(data['checkStatusMember'] === 'member'){
                             if(data['event_type'] === 1 || data['event_type'] === '1'){
                                  if(data['statusMemberEventParticipate'] === 1 || data['statusMemberEventParticipate'] === '1'){
                                       content += statusMemberEventParticipatePrint;
                                  }else{
                                       content += statusMemberEventNotParticipatePrint;
                                  }
                             }else{
                                  if(data['statusMemberEventParticipate'] === 1 || data['statusMemberEventParticipate'] === '1'){
                                       content += statusMemberEventParticipatePrint;
                                  }else{
                                       content += statusMemberEventNotParticipatePrint;
                                  }
                             }
                        }else{
                             if(data['event_type'] === 1 || data['event_type'] === '1'){
                                  if(data['statusMemberEventParticipate'] === 1 || data['statusMemberEventParticipate'] === '1'){
                                       content += statusMemberEventParticipatePrint;
                                  }else{
                                       content += statusMemberGroupJoin;
                                  }
                             }else{ 
                                  if(data['statusMemberEventParticipate'] === 1 || data['statusMemberEventParticipate'] === '1'){
                                       content += statusMemberEventParticipatePrint;
                                  }else{
                                        content += "<div class='btn-in-stat'>";
                                        content += "</div>";
                                  }
                             }
                        }
                   }
              }else{
                   content += "<div class='btn-in-stat'>";
                   content += "</div>";
              }
         }else{
              content += "<div class='btn-in-stat'>";
              content += "</div>";
         }
    }

    content += "</div>";
    content += "<div class='gj-song-revision'>";
    content += "<div class='name-group-post'>";
    content += "<i class='fa fa-group'></i><a id='pjax' data-pjax='yes' href='"+base_url+"/group/latestpost/"+data['groupID']+"' class='pru'><strong>"+data['group_name']+"</strong></a>";
    content += "</div>";
    content += "</div>";
    content += "</div>";
    content += "</div>";
    return content;
}

function printInvitationExplore(data){
    var content = "";
   
    content += "<div class='gj-song-wrap'>";
    content += "<div class='gj-song-card'>";
    content += "<div class='gj-song-cover'>";
    if(data['image_project'] != 0){
       content += "<img src='"+base_url+"/"+data['image_project']+"' class='img-responsive'>";
    }else{
       content += "<img src='"+base_url+"/img/album/album-song_475x475.jpg' class='img-responsive'>";
    }
    content += "</div>";
    content += "<div class='gj-song-desc'>";
    content += "<div class='gj-song-story'>";
    content += "<h1 class='gj-song-title'><a id='pjax' data-pjax='yes' href='#'>"+data['project_name']+"</a></h1>";
    content += "</div>";
    content += "<div class='gj-text-desc'>";
    if(data['description'] === null || data['description'] === '' || data['description'] === 0 || data['description'] === 'null'){
        content += "<p class='revs'></p>";
    }else{
        content += "<p class='revs'>"+data['description']+"</p>";
    }
    content += "</div>";
    content += "</div>";
    content += "<div class='warp-stat-card top-stat-card'>";
    
    content += "<div class='btn-in-stat'>";
    content += "</div>"; 

    content += "<div class='btn-in-stat'>";
    content += "</div>"; 

    content += "<div class='btn-in-stat'>";
    if(data['myUserID'] != 0 || data['myUserID'] != ''){
        content += "<button type='button' class='btn btn-line-stat col-btn-ex' onclick='joinProjectInvitation("+'"'+data['projectID']+'"'+");'>Join</button>";
    }else{
        content += "<button type='button' class='btn btn-line-stat col-btn-ex' onclick='harusLogin();'>Join</button>";
    }
    content += "</div>"; 

    content += "</div>";
    content += "<div class='gj-song-revision'>";
    content += "<div class='name-group-post'>";
    content += "<i class='fa fa-user'></i><a id='pjax' data-pjax='yes' href='"+base_url+"/"+data['username_creator']+"' class='pru'><strong>"+data['username_creator']+"</strong></a>";
    content += "</div>";
    content += "</div>";
    content += "</div>";
    content += "</div>";

    return content;
}

function printEventExploreNull(type){
  var content = "";
  switch(type){
      case 1:
        content += "<div class='gj-song-wrap'>";
        content += "<div class='gj-song-card'>";
        content += "<div class='nf-item-wall'>";
        content += "<p>Not latest event</p>";
        content += "</div>";
        content += "</div>";
        content += "</div>";
        break;
      case 2:
        content += "<div class='gj-song-wrap'>";
        content += "<div class='gj-song-card'>";
        content += "<div class='nf-item-wall'>";
        content += "<p>Not latest invitation</p>";
        content += "</div>";
        content += "</div>";
        content += "</div>";  
        break;
      default:
        this.errors = true;
        break;  
  }
  
  return content;
}
function getEventExplore(type){
    var data = getDataEventExplore(type);
    var link = getLinkExplorePage(type);
    console.log(link);
    $.ajax({
        type : "POST",
        data : data,
        url : link,
        success :function(responseText){
        	var response = responseText[0];     	
        	var count = response['count'];
        	console.log(response);
          if(count === 0 || count === '0'){
            var contentNull = printEventExploreNull(type);
            $('#loadingEventExplore'+type).replaceWith(contentNull);
          }
          else{
            var data = response['data'];
          	var content = "";
          	for(var i = 0; i < data.length; i ++){
          		content += getContentExplorePage(type, data[i]);
          	}
          	$('#loadingEventExplore'+type).replaceWith(content);

          	// setTakeTotalEventExplore(count);
          	// setLinkEventExplore();
          }
        },
        errors : function(){
        	console.log('error');
        }
    });
}

function getInvitationExplore(){

}

function joinProjectInvitation(projectID){
    // console.log(projectID);
    $('#modalJoin-Invitation').modal({
        backdrop: 'static',
        keyboard: true,
        show : true });
    $('#invitation-explore-projectID').text(projectID);

}

function prosesJoinProjectInvitation(){
   var button = "<a id='buttonProsesJoinProjectInvitation' class='btn-s-gj bgc-btn'>Yes</a>";
   $('.loader-process').show();
   $('.closeModalJoin-ProjectInvitation').hide();
   $('#buttonProsesJoinProjectInvitation').replaceWith(button);
   var projectID = $('#invitation-explore-projectID').text();
   console.log(projectID);
   var data = '&projectID=' +projectID; 
   var link = base_url+"/studio/karaoke/publicInvite";

   $.ajax({
         type : "POST",
         data : data,
         url : link,
         success : function(responseText){
            console.log('sukses');
            console.log(responseText);
            var data = responseText['message'];
            var status = responseText['status'];
            console.log(data);
            console.log(status);
            if(status === 1 || status === '1'){
               window.location.href = window.location.origin+"/studio/karaoke/"+projectID;
            }else{
                swal({
                    title: data,
                    text: 'I will close in 2 seconds.',
                    timer: 2000
                  });
              var buttonProses = "<a id='buttonProsesJoinProjectInvitation' href='javascript:void(0);' onclick='prosesJoinProjectInvitation();' class='btn-s-gj bgc-btn'>Yes</a>";
              $('.loader-process').hide();
              $('.closeModalJoin-ProjectInvitation').show();
              $('#buttonProsesJoinProjectInvitation').replaceWith(buttonProses);
            }

         },
         error : function(){
            console.log('error');
         }
   });
}

function harusLogin(){
  swal({ 
       title: "Sorry...!",
        text: "You must login",
         type: "warning",
          allowOutsideClick: false,
        });

        $('body').on('click','.swal2-confirm',function(){
             window.location.href = window.location.origin+'/login';
        });  
}
/* End Ajax Call */