function checkElementEventPage(){
	var elementExists = new Array(
		document.getElementById("newEvents"),
		document.getElementById("eventWinner")
	);

	for (var i = elementExists.length - 1; i >= 0; i--) {
		if(elementExists[i] == null) return -1;
	};
	return 1;
}

$(document).on('ready pjax:success', function(){
	var check = checkElementEventPage();
	if(check == -1) return;
	setEventValue();
});

var newEvent_skip, newEvent_take, newEvent_count;
var eventWinner_skip, eventWinner_take, eventWinner_count;

function setEventValue(){
	// console.log('im here');
	this.newEvent_count = 0;
	this.newEvent_skip = 6;
	this.newEvent_take = 6;

	this.eventWinner_count = 0;
	this.eventWinner_skip = 6;
	this.eventWinner_take = 6;
}

function getEventValue(type, category){
	var data = {};

	switch(type){
		case 1:
			data['skip'] = this.newEvent_skip;
			data['take'] = this.newEvent_take;
			break;
		case 2:
			data['skip'] = this.eventWinner_skip;
			data['take'] = this.eventWinner_take;
			break;
		default:
			return -1;
			break
	}

	switch(category){
		case 1:
			data['orderBy'] = 'created_at';
			break;
		case 2:
			data['orderBy'] = 'liked';
			break;
		default:
			return -1;
			break
	}

	data['category'] = category;
	data['optionOrder'] = 'DESC';
	data['type'] = type;
	return data;
}

function changeValueEvent(count, skip, type){
	switch(type){
		case 1:
			this.newEvent_skip = skip;
			this.newEvent_count = count;
			break;
		case 2:
			this.eventWinner_skip = skip;
			this.eventWinner_count = count;
			break;
		default:
			return -1;
			break
	}
}

function loadMoreEvent(type, category){
	var data = getEventValue(type, category);

	$('#loadEventMore'+type).replaceWith("<div class='text-center sos-wrapper' id='loaderEventMore"+type+"'>"+
                            "<i class='fa fa-spinner fa fa-spin fa fa-large'></i>"+
                        "</div>");

	$.ajax({
		type: 'post',
		data: data,
		url: '/loadMoreEvent',
		success: function(responseText){
			console.log(responseText);
			var content = responseText['content'];
			var count = responseText['count'];
			var skip = responseText['skip'];

			$("#loaderEventMore"+type).replaceWith(content);
			changeValueEvent(count, skip, type);
		},
		error : function(jqXHR, textStatus, errorThrown){
			console.log(jqXHR);
			// swal('Oops...','Something went wrong! Please reload this page.','error');
		}
	});
}