// JavaScript Document
// Init Countdown Grit Jam
$(document).on('ready pjax:success', function() {
            var $section = $('section');
            $(window).on("resize", function () {
                var dif = Math.max($(window).height() - $section.height(), 0);
                var padding = Math.floor(dif / 2) + 'px';
                $section.css({ 'padding-top': padding, 'padding-bottom': padding });
            }).trigger("resize");
            $('#myCounter').mbComingsoon({ expiryDate: new Date(2016, 4, 31, 12, 0), speed:100 }); // 31 Mei 2016
            setTimeout(function () {
                $(window).resize();
            }, 200);
        });