var base_url = window.location.origin;
var errors;

var countYourGroup;
var takeYourGroup;
var skipYourGroup;

var countGroupMember;
var takeGroupMember;
var skipGroupMember;

var countGroupMemberPending;
var takeGroupMemberPending;
var skipGroupMemberPending;

var countGroupMemberRequest;
var takeGroupMemberRequest;
var skipGroupMemberRequest;

var countFriendGroupPage;
var takeFriendGroupPage;
var skipFriendGroupPage;

function setValueGroup(){
  this.errors = false;

  this.countYourGroup;
  this.takeYourGroup  = 2;
  this.skipYourGroup = 0;

  this.countGroupMember;
  this.takeGroupMember = 2;
  this.skipGroupMember = 0;

  this.countGroupMemberPending;
  this.takeGroupMemberPending = 4;
  this.skipGroupMemberPending = 0;

  this.countGroupMemberRequest;
  this.takeGroupMemberRequest = 4;
  this.skipGroupMemberRequest = 0;

  this.countFriendGroupPage;
  this.takeFriendGroupPage = 4;
  this.skipFriendGroupPage = 0;
}

function checkElementGroup(){
  var elementExists = new Array(
    document.getElementById('loadingGroup1'),
    document.getElementById('loadingGroup2'),
    document.getElementById('loadingGroup3'),
    document.getElementById('loadingGroup4'),
    document.getElementById('totalCountYourGroup'),
    document.getElementById('totalCountYourGroupPending'),
    document.getElementById('totalCountYourGroupRequest'),

    document.getElementById('loadingGroupLeft1'),
    // document.getElementById('loadingGroupLeft2'),
    document.getElementById('loadingGroupLeft3'),
    document.getElementById("loadingGroupEventawal")
    
    // document.getElementById('loadingGroupMyBio')

    
  );

  var stop = false; 
  for (var i = elementExists.length - 1; i >= 0; i--) {
      if(elementExists[i] == null)
        stop = true;
    };

    if(stop)
      return 0;
    return 1;
}

$(document).on('ready pjax:success', function() {
    var check = checkElementGroup();
    if(check == 0) return;

    setValueGroup();

    getGroupPrint(1);
    getGroupPrint(2);
    getGroupPrint(3);
    getGroupPrint(4);


    setValue_myWall();
    getContentGroupLeft(1);
    // getContentGroupLeft(2);
    
    getContentGroupLeft(3);
    loadEventGroupRightawal();
    // getMyBio();
  });



function printYourGroup(data, i, count){
  console.log
var content = "";
if(data != 0){ 
  content += "<div class='item col-lg-6 col-md-6 col-sm-6 col-xs-12 col-sxs-12' id='dataYourGroup"+data['group_id']+"'>";
  if(data['bg_picture'] == 0){
     content += "<div class='warp-single-friend bg-card-fl' style='background-image:url("+base_url+"/img/bg-profile/bg.jpg);'>";
  }else{
     content += "<div class='warp-single-friend bg-card-fl' style='background-image:url("+base_url+"/"+data['bg_picture']+");'>";
  }
  content += "<div class='tile-card-bg-fl'></div>";
  content += "<div>";
  if(data['picture'] == 0){
     content += "<a href='#'><img src='"+base_url+"/img/avatar/avatar-group-300x300.png' class='img-circle img-border-gj img-responsive'></a>";
  }else{
     content += "<a href='#'><img src='"+base_url+"/"+data['picture']+"' class='img-circle img-border-gj img-responsive'></a>";
  }
  content += "</div>";
  content += "<div class='desc-myfriend'>";
  if(data['checkAdmin'] === 'admin'){
     content += "<div class='disc-user-name' style='color:#fff;'><a id='pjax' data-pjax='yes' href='"+base_url+"/group/latestpost/"+data['group_id']+"'>"+data['group_name']+"</a> (Admin)</div>";
  }else{
     content += "<div class='disc-user-name' ><a id='pjax' data-pjax='yes' href='"+base_url+"/group/latestpost/"+data['group_id']+"'>"+data['group_name']+"</a></div>";
  }
  content += "<div class='disc-user-username'></div>"
  content += "<div class='warp-btn-friend-list'>";
  content += "<span class='warp-btn-f-uf'>";
  content += "<i class='fa fa-check'></i>";
    content += "<span id='hiddenGroupIDYourGroup"+[i]+"' style='display: none;''>"+data['group_id']+"</span>";
    content += "<span id='hiddenGroupNameYourGroup"+[i]+"' style='display: none;''>"+data['group_name']+"</span>";
    content += "<span id='hiddenGroupPictureYourGroup"+[i]+"' style='display: none;''>"+data['picture']+"</span>";
    content += "<span id='hiddenGroupBgPictureYourGroup"+[i]+"' style='display: none;''>"+data['bg_picture']+"</span>";

  if(data['checkAdmin'] === 'admin'){
       if(data['checkingEventAktiveAndEventWinner'] === '1' || data['checkingEventAktiveAndEventWinner'] === 1){
            content += "<input type='button' onclick='cantLeaveTheGroup();' class='btn btn-u-uf' value='Joined' data-value-hover='Leave Group'>";
       }else if(data['member'] > 1 || data['member'] > '1'){  
            content += "<input type='button' onclick='adminLeaveGroupOtherMemberFound("+[i]+");' class='btn btn-u-uf' value='Joined' data-value-hover='Leave Group'>";
         }else{
           content += "<input type='button' onclick='adminLeaveGroupOtherMemberNull("+[i]+");'  class='btn btn-u-uf' value='Joined' data-value-hover='Leave Group'>";
            // content += "<input type='button' onclick='leaveGroup("+[i]+");' id='leaveGroup"+[i]+"' groupName ='"+data['group_name']+"' groupID='"+data['group_id']+"' picture='"+data['picture']+"' bg_picture='"+data['bg_picture']+"' class='btn btn-u-uf' value='Joined' data-value-hover='Leave Group'>";
       }
  }else{
      content += "<input type='button' onclick='memberGroupLeaveGroup("+[i]+");' class='btn btn-u-uf' value='Joined' data-value-hover='Leave Group'>";
      // content += "<input type='button' onclick='leaveGroup("+[i]+");' id='leaveGroup"+[i]+"' groupName ='"+data['group_name']+"' groupID='"+data['group_id']+"' picture='"+data['picture']+"' bg_picture='"+data['bg_picture']+"' class='btn btn-u-uf' value='Joined' data-value-hover='Leave Group'>";
   }
    
  content += "</span>";
  content += "</div>";
  content += "</div>";
  content += "</div>";
  content += "</div>";

 }else{
  content += "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>";
  content += "<div class='nf-item-wall-track'>";
  content += "<p>You haven't Joined any Group</p>";
  content += "</div>";
  content += "</div>"; 
 }
return content;
}

function printYourGroupPending(data, i,count){
var content = "";
if(data != 0){ 
  content += "<div class='item col-lg-6 col-md-6 col-sm-6 col-xs-12 col-sxs-12' id='dataYourGroupPending"+data['group_id']+"'>";
  if(data['bg_picture'] == 0){
     content += "<div class='warp-single-friend bg-card-fl' style='background-image:url("+base_url+"/img/bg-profile/bg.jpg);'>";
  }else{
     content += "<div class='warp-single-friend bg-card-fl' style='background-image:url("+base_url+"/"+data['bg_picture']+");'>";
  }
  content += "<div class='tile-card-bg-fl'></div>";
  content += "<div>";
  if(data['picture'] == 0){
      content += "<a href='#'><img src='"+base_url+"/img/avatar/avatar-group-300x300.png' class='img-circle img-border-gj img-responsive'></a>";
  }else{
     content += "<a href='#'><img src='"+base_url+"/"+data['picture']+"' class='img-circle img-border-gj img-responsive'></a>";
  }
  content += "</div>";
  content += "<div class='desc-myfriend'>";
  content += "<div class='disc-user-name'><a id='pjax' data-pjax='yes' href='"+base_url+"/group/latestpost/"+data['group_id']+"'>"+data['group_name']+"</a></div>"
  content += "<div class='disc-user-username'></div>"
  content += "<span id='hiddenGroupIDYourGroupPending"+[i]+"' style='display: none;''>"+data['group_id']+"</span>";
  content += "<span id='hiddenGroupNameYourGroupPending"+[i]+"' style='display: none;''>"+data['group_name']+"</span>";
  content += "<div class='warp-btn-friend-list'>";
  content += "<button id='buttonConfirmGroupInvitation"+data['group_id']+"' type='button' onclick='groupUserConfirmGroup("+[i]+");' class='btn btn-add-f-list col-btn-ex'><i class='fa fa-check'></i>Confirm Now</button>";
  content += "<span>";
  content += "<button id='buttonRejectGroupInvitation"+data['group_id']+"' type='button' onclick='groupUserRejectInvite("+[i]+");' class='btn btn-add-f-list col-btn-red'><i class='fa fa-times'></i>Reject</button>";
  content += "</span>";
  content += "</div>";

  content += "</div>";
  content += "</div>";
  content += "</div>";
 }else{
  content += "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>";
  content += "<div class='nf-item-wall-track'>";
  content += "<p>There is no Group</p>";
  content += "</div>";
  content += "</div>"; 
 }
return content;
}

function printYourGroupRequest(data, i,count){
var content = "";
if(data != 0){ 
  content += "<div class='item col-lg-6 col-md-6 col-sm-6 col-xs-12 col-sxs-12' id='dataYourGroupRequest"+data['group_id']+"'>";
  if(data['bg_picture'] == 0){
     content += "<div class='warp-single-friend bg-card-fl' style='background-image:url("+base_url+"/img/bg-profile/bg.jpg);'>";
  }else{
     content += "<div class='warp-single-friend bg-card-fl' style='background-image:url("+base_url+"/"+data['bg_picture']+");'>";
  }
  content += "<div class='tile-card-bg-fl'></div>";
  content += "<div>";
  if(data['picture'] == 0){
     content += "<a href='#'><img src='"+base_url+"/img/avatar/avatar-300x300.png' class='img-circle img-border-gj img-responsive'></a>";
  }else{
     content += "<a href='#'><img src='"+base_url+"/"+data['picture']+"' class='img-circle img-border-gj img-responsive'></a>";
  }
  content += "</div>";
  content += "<div class='desc-myfriend'>";
  content += "<div class='disc-user-name'><a id='pjax' data-pjax='yes' href='"+base_url+"/group/latestpost/"+data['group_id']+"'>"+data['group_name']+"</a></div>"
  content += "<div class='disc-user-username'></div>"
  content += "<span id='hiddenGroupIDYourGroupRequest"+[i]+"' style='display: none;''>"+data['group_id']+"</span>";
  content += "<span id='hiddenGroupNameYourGroupRequest"+[i]+"' style='display: none;''>"+data['group_name']+"</span>";

  content += "<div class='warp-btn-friend-list'>";
  content += "<button type='button' class='btn btn-add-f-list col-btn-ex'><i class='fa fa-check'></i>Request Sent</button>";
  content += "<span>";
  content += "<button type='button' onclick='groupUserCancelRequest("+[i]+");' class='btn btn-add-f-list col-btn-red'><i class='fa fa-times'></i>Cancel Request</button>";
  content += "</span>";
  content += "</div>";

  content += "</div>";
  content += "</div>";
  content += "</div>";
 }else{
  content += "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>";
  content += "<div class='nf-item-wall-track'>";
  content += "<p>There is no Group</p>";
  content += "</div>";
  content += "</div>"; 
 }
return content;
}

function printMemberGroup(data, i, count) {
  var content = "";

    var profile_image = data['profile_image'];  
    var name_profile_image = profile_image.substring(profile_image.lastIndexOf('/')+1); // nama profile image
    var folder = 'img/avatar/';  // folder penyimpanan profile image
    var picture = folder+name_profile_image; // link profile image
      content += "<div class='item col-lg-6 col-md-6 col-sm-6 col-xs-12 col-sxs-12' id='dataMemberGroup"+data['username']+"'>";
      if(data['bg_image'] != 0){
         content += "<div class='warp-single-friend bg-card-fl' style='background-image:url("+base_url+"/"+data['bg_image']+");'>";
      }else{
        content += "<div class='warp-single-friend bg-card-fl' style='background-image:url("+base_url+"/img/blur-bg-profile/bg-profile-2.jpg);'>";
      }
      content += "<div class='tile-card-bg-fl'></div>";
      if(data['profile_image'] == 0){
          content += "<div>";
          content += "<a href='#'><img src='"+base_url+"/img/avatar/avatar-300x300.png' class='img-circle img-border-gj img-responsive'></a>";
          content += "</div>";
      }else{
        if(profile_image != picture){
          content += "<div>";
          content += "<a href='#'><img src='"+data['profile_image']+"' class='img-circle img-border-gj img-responsive'></a>";
          content += "</div>";
            }else{
          content += "<div>";
          content += "<a href='#'><img src='"+base_url+"/"+data['profile_image']+"' class='img-circle img-border-gj img-responsive'></a>";
          content += "</div>";
            }
      }    
      content += "<div class='desc-myfriend'>";
      if(data['checkMember'] === 'me'){
         content += "<div class='disc-user-name'><a href='#'>You</a></div>";
      }else{
         content += "<div class='disc-user-name'><a href='#'>"+data['real_name']+"</a></div>";
      }
      content += "<div class='disc-user-username'>";
      
      //admin group
      if(data['admin_group'] === 'admin') {
          content += "<a href='#'>"+data['username']+" (admin) </a></div>";
       }else{
          content += "<a href='#'>"+data['username']+" </a></div>";
       }

      content += "<span id='hiddenGroupID"+[i]+"' style='display: none;''>"+data['group_id']+"</span>";
      content += "<span id='hiddenGroupName"+[i]+"' style='display: none;''>"+data['group_name']+"</span>";
      content += "<span id='hiddenUsername"+[i]+"' style='display: none;''>"+data['username']+"</span>";
      content += "<span id='hiddenMemberID"+[i]+"' style='display: none;''>"+data['userID']+"</span>";
      content += "<div class='warp-btn-friend-list' id='buttonFriendMember"+[i]+"'>";
      if(data['checkMember'] === 'not me'){
        if(data['checkFriend'] === 'not friend'){  
          content += "<button type='button' onclick='addToMyFriendGroup("+[i]+");' class='btn btn-add-f-list col-btn-ex'><i class='fa fa-user-plus'></i>Add Friend</button>"; 
        }else if(data['checkFriend'] === 'send request'){
              content += "<button type='button'  class='btn btn-add-f-list col-btn-ex'><i class='fa fa-user-plus'></i>Friend Request Sent</button>";
         }else if(data['checkFriend'] === 'not confirm'){
              content += "<button type='button' onclick='confirmFriendRequestFromMember("+[i]+");' class='btn btn-add-f-list col-btn-ex'><i class='fa fa-user-plus'></i>Confirm Friend</button>";
         }
      }else{
        
      }
      content += "</div>";
      content += "</div>";
      content += "</div>";
      content += "</div>";
  
  return content;
}

function printMemberGroupPending(data, i){
  var content = "";
 
  var profile_image = data['profile_image'];  
  var name_profile_image = profile_image.substring(profile_image.lastIndexOf('/')+1); // nama profile image
  var folder = 'img/avatar/';  // folder penyimpanan profile image
  var picture = folder+name_profile_image; // link profile image
    content += "<div class='item col-lg-6 col-md-6 col-sm-6 col-xs-12 col-sxs-12' id='dataPending"+data['username']+"'>";
    if(data['bg_image'] != 0){
       content += "<div class='warp-single-friend bg-card-fl' style='background-image:url("+base_url+"/"+data['bg_image']+");'>";
    }else{
      content += "<div class='warp-single-friend bg-card-fl' style='background-image:url("+base_url+"/img/blur-bg-profile/bg-profile-2.jpg);'>";
    }
    content += "<div class='tile-card-bg-fl'></div>";
    if(data['profile_image'] == 0){
        content += "<div>";
        content += "<a href='#'><img src='"+base_url+"/img/avatar/avatar-300x300.png' class='img-circle img-border-gj img-responsive'></a>";
        content += "</div>";
    }else{
      if(profile_image != picture){
        content += "<div>";
        content += "<a href='#'><img src='"+data['profile_image']+"' class='img-circle img-border-gj img-responsive'></a>";
        content += "</div>";
      }else{
        content += "<div>";
        content += "<a href='#'><img src='"+base_url+"/"+data['profile_image']+"' class='img-circle img-border-gj img-responsive'></a>";
        content += "</div>";
      }
    }  
    content += "<div class='desc-myfriend'>";
    if(data['checkMember'] === 'me'){
         content += "<div class='disc-user-name'><a href='#'>You</a></div>";
    }else{
         content += "<div class='disc-user-name'><a href='#'>"+data['real_name']+"</a></div>";
    }     
    content += "<div class='disc-user-username'><a href='#'>"+data['username']+"</a></div>";
   
    content += "<span id='hiddenGroupIDPending"+[i]+"' style='display: none;'>"+data['group_id']+"</span>";
    content += "<span id='hiddenGroupNamePending"+[i]+"' style='display: none;'>"+data['group_name']+"</span>";
    content += "<span id='hiddenMemberIDPending"+[i]+"' style='display: none;'>"+data['userID']+"</span>";
    content += "<span id='hiddenMemberUsernamePending"+[i]+"' style='display: none;'>"+data['username']+"</span>";
    content += "<div class='warp-btn-friend-list' id='buttonFriendMemberPending"+[i]+"'>";
    if(data['checkMember'] === 'me'){
        // content += "<button type='button' onclick='memberGroupPendingConfirm("+[i]+");' id='memberGroupPendingConfirm"+[i]+"' groupID='"+data['group_id']+"' class='btn btn-add-f-list col-btn-ex req-group  memberGroupPendingConfirm'><i class='fa fa-user-plus'></i>Confirm</button>";
        content += "<button id='buttonPendingConfirm"+data['username']+"' type='button' onclick='memberGroupPendingConfirm("+[i]+");' class='btn btn-add-f-list col-btn-ex'><i class='fa fa-check'></i>Confirm</button>";
        content += "<button id='buttonPendingReject"+data['username']+"' type='button' onclick='memberGroupPendingReject("+[i]+");' class='btn btn-add-f-list col-btn-red'><i class='fa fa-times'></i>Reject</button>";
    }
    if(data['checkAdmin'] === 'admin'){
       content += "<button id='buttonPendingAdminCancelInvitation"+data['username']+"' type='button' onclick='memberGroupPendingAdminCancelInvite("+[i]+");' class='btn btn-add-f-list col-btn-red'><i class='fa fa-times'></i>Cancel invite</button>";
    }
    if(data['checkMember'] === 'not me'){
        if(data['checkFriend'] === 'not friend'){
             content += "<button type='button' onclick='addToMyFriendGroupPending("+[i]+");' class='btn btn-add-f-list col-btn-ex'><i class='fa fa-user-plus'></i>Add Friend</button>";
         }else if(data['checkFriend'] === 'send request'){
              content += "<button type='button'  class='btn btn-add-f-list col-btn-ex'><i class='fa fa-user-plus'></i>Friend Request Sent</button>";
         }else if(data['checkFriend'] === 'not confirm'){
              content += "<button type='button' onclick='confirmFriendRequestFromMemberPending("+[i]+");' class='btn btn-add-f-list col-btn-ex'><i class='fa fa-user-plus'></i>Confirm Friend</button>";
         }
      }else{
      
      }
    content += "</div>";
    content += "</div>";
    content += "</div>";
    content += "</div>";
  
  return content;
}

function printMemberGroupRequest(data,i){
  var content = "";
 
  var profile_image = data['profile_image'];  
  var name_profile_image = profile_image.substring(profile_image.lastIndexOf('/')+1); // nama profile image
  var folder = 'img/avatar/';  // folder penyimpanan profile image
  var picture = folder+name_profile_image; // link profile image
    content += "<div class='item col-lg-6 col-md-6 col-sm-6 col-xs-12 col-sxs-12' id='dataMemberRequest"+data['username']+"'>";
    if(data['bg_image'] != 0){
       content += "<div class='warp-single-friend bg-card-fl' style='background-image:url("+base_url+"/"+data['bg_image']+");'>";
    }else{
      content += "<div class='warp-single-friend bg-card-fl' style='background-image:url("+base_url+"/img/blur-bg-profile/bg-profile-2.jpg);'>";
    }
    content += "<div class='tile-card-bg-fl'></div>";
    if(data['profile_image'] == 0){
        content += "<div>";
        content += "<a href='#'><img src='"+base_url+"/img/avatar/avatar-300x300.png' class='img-circle img-border-gj img-responsive'></a>";
        content += "</div>";
    }else{
      if(profile_image != picture){
        content += "<div>";
        content += "<a href='#'><img src='"+data['profile_image']+"' class='img-circle img-border-gj img-responsive'></a>";
        content += "</div>";
      }else{
        content += "<div>";
        content += "<a href='#'><img src='"+base_url+"/"+data['profile_image']+"' class='img-circle img-border-gj img-responsive'></a>";
        content += "</div>";
      }
    }  
    content += "<div class='desc-myfriend'>";
    if(data['checkMember'] === 'me'){
        content += "<div class='disc-user-name'><a href='#'>You</a></div>";
    }else{
    content += "<div class='disc-user-name'><a href='#'>"+data['real_name']+"</a></div>";
    }
    content += "<div class='disc-user-username'><a href='#'>"+data['username']+"</a></div>";
    
    content += "<span id='hiddenGroupIDRequest"+[i]+"' style='display: none;''>"+data['group_id']+"</span>";
    content += "<span id='hiddenGroupNameRequest"+[i]+"' style='display: none;''>"+data['group_name']+"</span>";
    content += "<span id='hiddenUsernameRequest"+[i]+"' style='display: none;''>"+data['username']+"</span>";
    content += "<span id='hiddenMemberIDRequest"+[i]+"' style='display: none;''>"+data['userID']+"</span>";
    content += "<div class='warp-btn-friend-list' id='buttonFriendMemberRequest"+[i]+"'>";
    if(data['checkAdmin'] === 'admin'){
        // content += "<button type='button' onclick='memberGroupRequestAdminConfirm("+[i]+")' class='btn btn-add-f-list col-btn-ex req-group'><i class='fa fa-user-plus'></i>Request Join</button>";  
    content += "<button id='buttonAdminConfirmRequestGroupMember"+data['username']+"' type='button' onclick='memberGroupRequestAdminConfirm("+[i]+");' class='btn btn-add-f-list col-btn-ex'><i class='fa fa-check'></i>Confirm</button>";
    content += "<button id='buttonAdminRejectRequestGroupMember"+data['username']+"' type='button' onclick='memberGroupRequestAdminReject("+[i]+");' class='btn btn-add-f-list col-btn-red'><i class='fa fa-times'></i>Reject</button>";
  
    }
     
    if(data['checkMember'] === 'me'){
        content += "<span>";
        content += "<button type='button' class='btn btn-add-f-list col-btn-ex req-group '><i class='fa fa-check'></i>Request Sent</button>";
        content += "</span>";
         content += "<button type='button' onclick='memberGroupRequestSend("+[i]+");' class='btn btn-add-f-list col-btn-red'><i class='fa fa-times'></i>Cancel Request</button>";
    }
     
     content += "<span>";
    if(data['checkMember'] === 'not me'){
        if(data['checkFriend'] === 'not friend'){
             content += "<button type='button' onclick='addToMyFriendGroupRequest("+[i]+");' class='btn btn-add-f-list col-btn-ex'><i class='fa fa-user-plus'></i>Add Friend</button>";
         }else if(data['checkFriend'] === 'send request'){
              content += "<button type='button'  class='btn btn-add-f-list col-btn-ex'><i class='fa fa-user-plus'></i>Friend Request Sent</button>";
         }else if(data['checkFriend'] === 'not confirm'){
              content += "<button type='button' onclick='confirmFriendRequestFromMemberRequest("+[i]+");'  class='btn btn-add-f-list col-btn-ex'><i class='fa fa-user-plus'></i>Confirm Friend</button>";
         }
      }else{

      }
       content += "</span>";
    content += "</div>";
    content += "</div>";
    content += "</div>";
    content += "</div>";

  return content;
}

function memberGroupKosong(type){
  var content = "";
      if(type === 5){
         content += "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12' id='idMemberInvitation'>";
      }else if(type === 7){
         content += "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12' id='idMemberRequest'>";
      }
      
      content += "<div class='nf-item-wall-track'>";
      if(type === 5){
        content += "<p> No new member invited</p>";
      }else if(type === 7){
         content += "<p>No new member requests</p>";
      }
      content += "</div>";
      content += "</div>"; 
      return content;
}

function PrintCountGroupMember(data,i , count){
  var content ="";
  content =" <span class='pull-right sum-member' id='totalCountMember'>"+data['countMember']+" Members</span>";
  return content;
}

function PrintCountGroupMemberPending(data,i , count){
  var content ="";
  content =" <span class='pull-right sum-member' id='totalCountPending'>"+data['countPending']+" Members</span>";
  return content;
}

function PrintCountGroupMemberRequest(data,i , count){
  var content ="";
  content =" <span class='pull-right sum-member' id='totalCountRequest'>"+data['countRequest']+" Members</span>";
  return content;
}

function PrintNameGroupMember(data,i , count){
  var content ="";
  content += data['group_name'];
  return content;
}

function PrintSelectFriend(data){
  var content = "";
  content += "<option value="+data['friend_id']+">"
  content += data['friend_username'];
  content += "</option>";
  return content;
}

function LoadMoreGroup(type){
    var content = "";
    if(type != 10){
      content += "<div class='warp-btn-loadmore-list' id='moreGroup"+type+"'>";
      content += "<a class='btn-loadmore-list' onclick='getGroup("+type+")'>";
      content += "<i class='fa fa-plus-circle'></i>";
      content += "<span class='text-load-more-list'>LOAD MORE</span>";
      content += "</a>";
      content += "</div>";
    }
    else if(type == 10){
      content += "<div class='col-lg-2 col-md-2 col-sm-4 col-xs-6 col-sxs-12' id='moreGroup"+type+"'>";
      content += "<div class='warp-album-music'>";
      content += "<a class='warp-single-most-dl-thumb-loadmore' onclick='getGroup("+type+")'>";
      content += "<img src='"+base_url+"/img/album/blank_album-00_475x475.jpg' class='img-full-responsive img-border-gj e-centering'>";
      content += "<div class='btn-loadmore-thumb'>";
      content += "<i class='fa fa-plus-circle'></i>";
      content += "<p class='text-loadmore-thumb'>LOAD MORE</p>";
      content += "</div>";
      content += "</a>";
      content += "</div>";
      content += "</div>";
    }
    return content;
  }


function loadingGifFileGroup(type){
  var content = "";
  content += "<div class='col-xs-push-5 col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 lazyloader-list' id='loadingGroup"+type+"'>";
  content += "<img src='img/loader-more.gif' class='img-full-responsive e-centering width-loader'>";
  content += "</div>";
  return content;
}

function setTakeTotalGroup(total, type){
  switch(type){
    case 1:
      var skip = this.skipYourGroup;
      var take = this.takeYourGroup;
      break;
    case 2:
      var skip = this.skipYourGroup;
      var take = this.takeYourGroup;
      break;  
    case 3:
      var skip = this.skipYourGroup;
      var take = this.takeYourGroup;
      break;  
    case 4:
      var skip = this.skipFriendGroupPage;
      var take = this.takeFriendGroupPage; 
      break; 
    default:
      this.errors = true;
      break;
  }

  if(this.errors == true){
    return 0;
  }

  skip = skip + take;
  if(skip + take > total){
    take = total - skip + 1;
  }

  switch(type){
    case 1:
          this.skipYourGroup = skip;
          this.takeYourGroup = take;
          this.countYourGroup = total;
          break;
    case 2:
          this.skipYourGroup = skip;
          this.takeYourGroup = take;
          this.countYourGroup = total;  
         break;
    case 3:
          this.skipYourGroup = skip;
          this.takeYourGroup = take;
          this.countYourGroup = total;  
         break;
    case 4:
          this.skipFriendGroupPage = skip;
          this.takeFriendGroupPage = take;
          this.countFriendGroupPage = total;
          break;
        default:
        this.errors = true;
        break;
  }

  if(this.errors == true)
    return 0;
}

function setLinkGroup(type){
  var content;
  switch(type){
    case 1:
    if(this.skipYourGroup < this.countYourGroup){
      content = LoadMoreGroup(1);
      $('#').append(content);
    }
    break;
      case 2:
    if(this.skipYourGroup < this.countYourGroup){
      content = LoadMoreGroup(2);
      $('#').append(content);
    }
    break;  
    case 3:
    if(this.skipYourGroup < this.countYourGroup){
      content = LoadMoreGroup(3);
      $('#').append(content);
    }
    break;
    case 4:
    if(this.skipFriendGroupPage < this.countFriendGroupPage){
      content = LoadMoreGroup(4);
      $('#').append(content);
    }
    break;
    default:
      this.errors = true;
      break;
  }
}

function getdataGroup(type){
   var data = {};
   switch(type) {
    case 1:
       data['skip'] = this.skipYourGroup;
       data['take'] = this.takeYourGroup;
       data['orderBy'] = 'create_at';
       data['optionOrder'] = 'desc';
       data['invite_status'] = '0';
       break;
    case 2:
       data['skip'] = this.skipYourGroup;
       data['take'] = this.takeYourGroup;
       data['orderBy'] = 'created_at';
       data['optionOrder'] = 'desc';
       data['invite_status'] = '1';
       break;
    case 3:
       data['skip'] = this.skipYourGroup;
       data['take'] = this.takeYourGroup;
       data['orderBy'] = 'created_at';
       data['optionOrder'] = 'desc';
       data['invite_status'] = '2';
       break;
    case 4:
       data['skip'] = this.skipFriendGroupPage;
       data['take'] = this.takeFriendGroupPage;
       break;   
    default:
       this.errors = true;
       break;   
   }

   return data;
}

function getLinkGroup(type){
  var link;
  var id= document.getElementById("sessiongroup").value;
  switch(type) {
    case 1:
    link = " "+base_url+"/group/yourGroup/"+id;
    break;
    case 2:
    link = " "+base_url+"/group/yourGroup/"+id;
    break;  
    case 3:
    link = " "+base_url+"/group/yourGroup/"+id;
    break;  
    case 4:
    link = " "+base_url+"/friendlistGroup/"+id;
    default:
      this.errors = true;
      break;
  }

  return link;
}

function getContentGroup(type, data, i,count){
  var content="";
  switch(type) {
    case 1:
       content += printYourGroup(data, i,count);

       break;
    case 2:
       content += printYourGroupPending(data, i,count);
       break;
    case 3:
       content += printYourGroupRequest(data, i,count);
       break;
    case 4:
       content += getContentFriendListGroupLeft(data, i);
       break;
    default:
       this.errors = true;
       break;   
  }
  return content;
}

function getGroupPrint(type){
   var data = getdataGroup(type);
    var link = getLinkGroup(type);
    $.ajax({
      type  : "POST",
      data : data,
      url   : link,
      success : function(responseText){
        console.log(responseText);
        var count = responseText['count'];
        console.log(count);
        if(type === 4){
           if(responseText === '-1' || responseText === -1){
            var replaceContent = getContentFriendListGroupLeftNull();
              $('#loadingGroup4').replaceWith(replaceContent);
           }else{
               var data = responseText['data'];
               var replaceContent = "";
              for (var i = 0; i < data.length; i++) {
                replaceContent += getContentGroup(4, data[i], i, count);
              };
              $('#loadingGroup4').replaceWith(replaceContent);
           }
        }
        else{
        var data = responseText['data'];
        
        var content = "";
        for (var i = 0; i < data.length; i++) {
          content += getContentGroup(type, data[i], i,count);
        };
        $('#loadingGroup'+type).replaceWith(content);
        
        
        
        if(type === 1){
          var content1 = "<span id='totalCountYourGroup' style='display:none'>"+count+"</span>";
           $('#totalCountYourGroup').replaceWith(content1);
        }else if(type === 2){
          var content2 = "<span id='totalCountYourGroupPending' style='display:none'>"+count+"</span>";
           $('#totalCountYourGroupPending').replaceWith(content2);
        }else if(type === 3){
          var content3 = "<span id='totalCountYourGroupRequest' style='display:none'>"+count+"</span>";
           $('#totalCountYourGroupRequest').replaceWith(content3);
        }
        
        // setTakeTotalGroup(count, type);
        // setLinkGroup(type);
       }
      }
    });
}

//=================== GROUP MEMBER =========================//
function checkElementGroupMember(){
  var elementExists = new Array(

    // document.getElementById('loadingGroupMember1'),
    document.getElementById('loadingGroupMember2'),
    document.getElementById('loadingGroupMember3')

    // document.getElementById('loadevent')

    
  );

  var stop = false; 
  for (var i = elementExists.length - 1; i >= 0; i--) {
      if(elementExists[i] == null)
        stop = true;
    };

    if(stop)
      return 0;
    return 1;
}

$(document).on('ready pjax:success', function() {
    var check = checkElementGroupMember();
    if(check == 0) return;

    setValueGroup();
    
    // loadevent();
    getGroupMemberPrint(1);
    getGroupMemberPrint(2);
    getGroupMemberPrint(3);
    getGroupMemberPrint(4);
    getGroupMemberPrint(5);
    getGroupMemberPrint(6);
    getGroupMemberPrint(7);
    

  });

function LoadMoreGroupMember(type){
    var content = "";
    if(type != 10){
      content += "<div class='warp-btn-loadmore-list' id='moreGroupMember"+type+"'>";
      content += "<a class='btn-loadmore-list' onclick='getGroupMember("+type+")'>";
      content += "<i class='fa fa-plus-circle'></i>";
      content += "<span class='text-load-more-list'>LOAD MORE</span>";
      content += "</a>";
      content += "</div>";
    }
    else if(type == 10){
      content += "<div class='col-lg-2 col-md-2 col-sm-4 col-xs-6 col-sxs-12' id='moreGroupMember"+type+"'>";
      content += "<div class='warp-album-music'>";
      content += "<a class='warp-single-most-dl-thumb-loadmore' onclick='getGroupMember("+type+")'>";
      content += "<img src='"+base_url+"/img/album/blank_album-00_475x475.jpg' class='img-full-responsive img-border-gj e-centering'>";
      content += "<div class='btn-loadmore-thumb'>";
      content += "<i class='fa fa-plus-circle'></i>";
      content += "<p class='text-loadmore-thumb'>LOAD MORE</p>";
      content += "</div>";
      content += "</a>";
      content += "</div>";
      content += "</div>";
    }
    return content;
  }


function loadingGifFileGroupMember(type){
  var content = "";
  content += "<div class='col-xs-push-5 col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 lazyloader-list' id='loadingGroupMember"+type+"'>";
  content += "<img src='"+base_url+"/img/loader-more.gif' class='img-full-responsive e-centering width-loader'>";
  content += "</div>";
  return content;
}

function setTakeTotalGroupMember(total, type){
  switch(type){
    case 1:
      // var skip = this.skipGroupMember;
      // var take = this.takeGroupMember;
      break;
    case 2:
      // var skip = this.skipGroupMember;
      // var take = this.takeGroupMember;
      break;
    case 3:
      var skip = this.skipGroupMember;
      var take = this.takeGroupMember;
      break;
    case 4:
      // var skip = this.skipGroupMember;
      // var take = this.takeGroupMember;
      break;
    case 5:
      var skip = this.skipGroupMemberPending;
      var take = this.takeGroupMemberPending;
      break;
    case 6:
      // var skip = this.skipGroupMember;
      // var take = this.takeGroupMember;
      break;
    case 7:
      var skip = this.skipGroupMemberRequest;
      var take = this.takeGroupMemberRequest;
      break;              
    default:
      this.errors = true;
      break;
  }

  if(this.errors == true){
    return 0;
  }

  skip = skip + take;
  if(skip + take > total){
    take = total - skip + 1;
  }

  switch(type){
    case 1:
          // this.skipGroupMember = skip;
          // this.takeGroupMember = take;
          // this.countGroupMember = total;    
          break;
    case 2:
         //  this.skipGroupMember = skip;
         //  this.takeGroupMember = take;
         //  this.countGroupMember = total; 
         break;
    case 3:
          this.skipGroupMember = skip;
          this.takeGroupMember= take;
          this.countGroupMember = total; 
         break;     
    case 4:
         //  this.skipGroupMember = skip;
         //  this.takeGroupMember = take;
         //  this.countGroupMember = total; 
         break;  
    case 5:
          this.skipGroupMemberPending = skip;
          this.takeGroupMemberPending = take;
          this.countGroupMemberPending = total; 
         break; 
    case 6:
         //  this.skipGroupMember = skip;
         //  this.takeGroupMember = take;
         //  this.countGroupMember = total; 
         break; 
    case 7:
          this.skipGroupMemberRequest = skip;
          this.takeGroupMemberRequest = take;
          this.countGroupMemberRequest = total; 
         break;                                    
        default:
        this.errors = true;
        break;
  }

  if(this.errors == true)
    return 0;
}

function setLinkGroupMember(type){
  var content;
  switch(type){
    case 1:
    // if(this.skipGroupMember < this.countGroupMember){
    //   content = LoadMoreGroupMember(1);
    //   $('#').append(content);
    // }
    break;
      case 2:
    // if(this.skipGroupMember < this.countGroupMember){
    //   content = LoadMoreGroupMember(2);
    //   $('#').append(content);
    // }
    break;
     case 3:
    if(this.skipGroupMember < this.countGroupMember){
      content = LoadMoreGroupMember(3);
      $('#groupmember3').append(content);
    }
    break;    
     case 4:
    // if(this.skipGroupMember < this.countGroupMember){
    //   content = LoadMoreGroupMember(4);
    //   $('#').append(content);
    // }
    break;    
     case 5:
    if(this.skipGroupMemberPending < this.countGroupMemberPending){
      content = LoadMoreGroupMember(5);
      $('#groupmember5').append(content);
    }
    break;    
     case 6:
    // if(this.skipGroupMember < this.countGroupMember){
    //   content = LoadMoreGroupMember(6);
    //   $('#').append(content);
    // }
    break;    
     case 7:
    if(this.skipGroupMemberRequest < this.countGroupMemberRequest){
      content = LoadMoreGroupMember(7);
      $('#groupmember7').append(content);
    }
    break;    
    default:
      this.errors = true;
      break;
  }
}

// https://www.sitepoint.com/url-parameters-jquery/
$.urlParam = function(name){
  var results = new RegExp('[/\?&]' + name + '=([^&#]*)').exec(window.location.href);
  return results[1] || 0;
}
function getdataGroupMember(type){
   var data = {};
   switch(type) {
    case 1:
       // data['skip'] = this.skipGroupMember;
       // data['take'] = this.takeGroupMember;
       data['orderBy'] = 'created_at';
       data['optionOrder'] = 'desc';
       break;
    case 2:
       // data['skip'] = this.skipGroupMember;
       // data['take'] = this.takeGroupMember;
       data['orderBy'] = 'created_at';
       data['optionOrder'] = 'desc';
       break;
    case 3:
       data['skip'] = this.skipGroupMember;
       data['take'] = this.takeGroupMember;
       data['orderBy'] = 'created_at';
       data['optionOrder'] = 'desc';
       data['invite_status'] = '0';
       break;  
    case 4:
       // data['skip'] = this.skipGroupMember;
       // data['take'] = this.takeGroupMember;
       data['orderBy'] = 'created_at';
       data['optionOrder'] = 'desc';
       break; 
     case 5:
       data['skip'] = this.skipGroupMember;
       data['take'] = this.takeGroupMember;
       data['orderBy'] = 'created_at';
       data['optionOrder'] = 'desc';
       data['invite_status'] = '1';
       break;
    case 6:
       // data['skip'] = this.skipGroupMember;
       // data['take'] = this.takeGroupMember;
       data['orderBy'] = 'created_at';
       data['optionOrder'] = 'desc';
       break; 
     case 7:
       data['skip'] = this.skipGroupMember;
       data['take'] = this.takeGroupMember;
       data['orderBy'] = 'created_at';
       data['optionOrder'] = 'desc';
       data['invite_status'] = '2';
       break;                    
    default:
       this.errors = true;
       break;   
   }

   return data;
}


function getLinkGroupMember(type){
  var link;
   var url = window.location.pathname;
   // var groupID = url.substring(url.lastIndexOf('/')+1);
   var split = url.split('/');
   var groupID= split[3];

  switch(type) {
     case 1:
    link = " "+base_url+"/dataGroup/"+groupID+"";
    break;
    case 2:
    link = " "+base_url+"/dataGroup/"+groupID+"";
    break;  
    case 3:
    link = " "+base_url+"/group/memberGroup/"+groupID+"";
    break;  
    case 4:
    link = " "+base_url+"/dataGroup/"+groupID+"";
    break;
    case 5:
    link = " "+base_url+"/group/memberGroup/"+groupID+"";
    break; 
    case 6:
    link = " "+base_url+"/dataGroup/"+groupID+"";
    break;
    case 7:
    link = " "+base_url+"/group/memberGroup/"+groupID+"";
    break;       
    default:
      this.errors = true;
      break;
  }

  return link;
}


function getContentGroupMember(type, data, i,count){
  var content="";
  switch(type) {
    case 1:
       content += PrintNameGroupMember(data, i, count);

       break;
    case 2:
       content += PrintCountGroupMember(data, i, count);
       break;
    case 3:
       content += printMemberGroup(data, i);
       break;
    case 4:
       content += PrintCountGroupMemberPending(data, i, count);
       break; 
    case 5:
       content += printMemberGroupPending(data, i);
       break;
    case 6:
       content += PrintCountGroupMemberRequest(data, i, count);
       break; 
    case 7:
       content += printMemberGroupRequest(data, i);
       break;                  
    default:
       this.errors = true;
       break;   
  }
  return content;
}

function getGroupMemberPrint(type){
   var data = getdataGroupMember(type);
    var link = getLinkGroupMember(type);
    $.ajax({
      type  : "POST",
      data : data,
      url   : link,
      success : function(responseText){
        console.log(responseText);
        var count = responseText['count'];
       
        if(count === 0){
          var content = memberGroupKosong(type);
          $('#loadingGroupMember'+type).replaceWith(content);
        }
        else{
          var count = responseText['count'];
          var data = responseText['data'];
          var content = "";
          for (var i = 0; i < data.length; i++) {
            content += getContentGroupMember(type, data[i], i,count);
          };
          $('#loadingGroupMember'+type).replaceWith(content);
  
          // setTakeTotalGroupMember(count, type);
          // setLinkGroupMember(type);
        }
      }
    });
}


function resetLoadingGifGroupMember(type){
  var content = loadingGifFileGroupMember(type);
  $('#moreGroupMember'+type).replaceWith(content);
}

// mencetak data load more group member
function getGroupMember(type){
  resetLoadingGifGroupMember(type);
  getGroupMemberPrint(type);
}

function toJSDate (dateTime) {

    var dateTime = dateTime.split(" ");//dateTime[0] = date, dateTime[1] = time

    var date = dateTime[0].split("-");
    var time = dateTime[1].split(":");

    //(year, month, day, hours, minutes, seconds, milliseconds)
    return new Date(date[0], date[1]-1, date[2], time[0], time[1], time[2], 0);
    
}

//=====checkelement event=======//
function checkElementGroupEvent(){
  var elementExists = new Array(

    // document.getElementById('loadingGroupMember1'),
    // document.getElementById('loadingGroupMember2'),
    // document.getElementById('loadingGroupMember3')
    document.getElementById('loadevent'),
    document.getElementById('loadingGroupEventGroup')    

    
  );

  var stop = false; 
  for (var i = elementExists.length - 1; i >= 0; i--) {
      if(elementExists[i] == null)
        stop = true;
    };

    if(stop)
      return 0;
    return 1;
}

$(document).on('ready pjax:success', function() {
    var check = checkElementGroupEvent();
    if(check == 0) return;

    setValueGroup();
    loadevent();
    loadEventGroupRightGroup();
    
  
  });

function loadevent(){
  var group_id = document.getElementById("group_id").value
  // console.log(group_id);
   $.ajax({
      type  : "POST",
      data : {group_id:group_id},
      url   : "/loadevent",
      success : function(response){
        if (response.length == 0) {
                document.getElementById("loadevent").style.display = "none";
                $( '#printevent').append( "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 padten'>"+
                                  "<div class='nf-item-wall'>"+
                                  "<p>"+
                                  "This Group Doesn't Have Any Event Yet"+
                                  "</p>"+
                                  "</div>"+
                                  "</div>"

                  );
              }else{
                var x = -1
                $.each(response, function( index, value ) {
                  x=x+1
                var jsDate = toJSDate(response[x]['start_time']);
                // var start = jsDate.toLocaleDateString() + " " + jsDate.toLocaleTimeString();

                var jsDate1 = toJSDate(response[x]['end_time']);
                // var end = jsDate1.toLocaleDateString() + " " + jsDate1.toLocaleTimeString();

                var monthNames = ["January", "February", "March","April", "May", "June", "July",
                    "August", "September", "October","November", "December"];

               var numberIndex = ["00","01","02","03","04","05","06","07","08","09"];

               var day = jsDate.getDate();
               var monthIndex = jsDate.getMonth();
               var year = jsDate.getFullYear();
               var hours = jsDate.getHours();
               if(hours < 10){
                  dataHours = numberIndex[hours];
               }else{
                  dataHours = hours;
               }
               var minute = jsDate.getMinutes();
               if(minute < 10){
                  dataMinutes = numberIndex[minute];
               }else{
                  dataMinutes = minute;
               }
               var seconds = jsDate.getSeconds();
               if(seconds < 10){
                   dataSeconds = numberIndex[seconds];
               }else{
                   dataSeconds = seconds;
               }

               var start = day+' '+monthNames[monthIndex]+ ' ' +year+ ' '  +datahours+':'+dataMinutes+':'+dataSeconds;
               

               var dayEnd = jsDate1.getDate();
               var monthIndexEnd = jsDate1.getMonth();
               var yearEnd = jsDate1.getFullYear();
               var hoursEnd = jsDate1.getHours();
               if(hoursEnd < 10){
                  dataHoursEnd = numberIndex[hoursEnd];
               }else{
                  dataHoursEnd = hoursEnd;
               }
               var minuteEnd = jsDate1.getMinutes();
               if(minuteEnd < 10){
                  dataMinutesEnd = numberIndex[minuteEnd];
               }else{
                  dataMinutesEnd = minuteEnd;
               }
               var secondsEnd = jsDate1.getSeconds();
               if(secondsEnd < 10){
                   dataSecondsEnd = numberIndex[secondsEnd];
               }else{
                   dataSecondsEnd = secondsEnd;
               }

               var end = dayEnd+' '+monthNames[monthIndexEnd]+ ' ' +yearEnd+ ' '  +datahoursEnd+':'+dataMinutesEnd+':'+dataSecondsEnd;
                   var base_url = window.location.origin;
                   var str = response[x]['profile_image']
                   var myarr = str.split(":");
                   var myvar = myarr[0];
                   // console.log(myvar)
                   if (myvar=="https") {
                    var url = "<a id='pjax' data-pjax='yes' href='"+base_url+"/"+response[x]['username']+"'><img src='" + response[x]['profile_image'] + "' class='img-responsive img-rounded-full'></a>"
                  }else{
                    var url = "<a id='pjax' data-pjax='yes' href='"+base_url+"/"+response[x]['username']+"'><img src='/" + response[x]['profile_image'] + "' class='img-responsive img-rounded-full'></a>"
                  }

                  if(response[x]['tags'] != 0){
                    var tags_genre = response[x]['tags'].split(',');
                    // console.log(tags_genre.length);
                    var tags=[];
                    var genre=[];
                    for(var i= 0; i < tags_genre.length; i++){
                       // genre.push(tags_genre[i].replace(/\s+/g, ''));
                       // tags.push("<a href='#'>#"+genre+"</a>");
                       // tags.push(" #"+tags_genre[i].replace(/\s+/g, ''));
                        tags.push(" <a href='#' style='text-decoration:none'>#"+tags_genre[i].replace(/\s+/g, '')+"</a>");
                     }
                   }else{
                    var tags = " ";
                   }
           
                   if(response[x]['picture_event'] != 0){
                     var img_event =  "<img src=' "+base_url+"/"+response[x]['picture_event']+"' class='img-responsive img-full-responsive'>"
                   }else{
                     var img_event =  "<img src='"+base_url+"/img/photo-event/default_group_event_475x475.jpg' class='img-responsive img-full-responsive'>";
                   }

                   if(response[x]['likedGroupEvent'] === 1){
                     var contentLiked =  "<div class='stat-song-track'  id='groupEventLiked"+response[x]['group_event_id']+"'>"+
                     "<div class='warp-ico-stat'>"+
                       "<div class='ico-stat'><a href='javascript:void(0)' onclick='udahLikePost();'><i class='fa fa-heart like-single-list-song' style ='color:red;'></i></a></div>"+
                         "<div class='num-stat'>"+response[x]['liked']+"</div>"+
                          "</div>"+
                         "</div>";
                   }else{
                      var contentLiked =  "<div class='stat-song-track'  id='groupEventLiked"+response[x]['group_event_id']+"'>"+
                      "<div class='warp-ico-stat' >"+
                       "<div class='ico-stat'><a href='javascript:void(0)' onclick='clickLikeGroupEvent("+response[x]['group_event_id']+","+response[x]['liked']+");'><i class='fa fa-heart like-single-list-song'></i></a></div>"+
                         "<div class='num-stat'>"+response[x]['liked']+"</div>"+
                         "</div>"+
                         "</div>";
                   }
                   
                   var create = response[x]['event_create'];
                   var t = create.split(/[- :]/);
                   var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]); 

                    
                    var statusMemberEventParticipatePrint = "<div class='btn-in-stat' id='groupEventParticipant"+response[x]['group_event_id']+"'>"+
                                            "<input type='button' onclick='sudahParticipate();' class='btn btn-line-stat col-btn-ex' value='participated'>"+
                                            "</div>";
                    var statusMemberEventNotParticipatePrint = "<div class='btn-in-stat' id='groupEventParticipant"+response[x]['group_event_id']+"'>"+
                                                               "<button type='button' onclick='groupEventParticipant("+'"'+response[x]['group_id']+'"'+", "+'"'+response[x]['group_event_id']+'"'+","+'"1"'+");' class='btn btn-line-stat col-btn-ex'>Participate</button>"+
                                                               "</div>";                                       
                    var statusMemberGroupJoin = "<div class='btn-in-stat' id='groupEventParticipant"+response[x]['group_event_id']+"'>"+
                                                "<button type='button' onclick='harusJoinGroup("+'"'+response[x]['group_id']+'"'+", "+'"'+response[x]['group_event_id']+'"'+");' class='btn btn-line-stat col-btn-ex'>Participate</button>"+
                                                "</div>";              
            
                   var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
                   var firstDate = new Date(response[x]['end_time']);
                   var secondDate = new Date;
                   var diffDays = Math.round(Math.round((secondDate.getTime() - firstDate.getTime()) ));
                   
                    if(response[x]['checkAdmin'] === 'not admin'){   
                        if(diffDays < 0) {  // check jika event belum selesai
                            if(response[x]['group_type'] === 1 || response[x]['group_type'] === '1'){  // check group public
                                  if(response[x]['checkStatusMember'] === 'member'){
                                      if(response[x]['event_type'] === 1 || response[x]['event_type'] === '1'){    //check event public
                                           if(response[x]['statusMemberEventParticipate'] === 1 || response[x]['statusMemberEventParticipate'] === '1'){
                                                var printButtonJoin = statusMemberEventParticipatePrint;
                                           }else{
                                                var printButtonJoin = statusMemberEventNotParticipatePrint;
                                           }
                                      }else{
                                           if(response[x]['statusMemberEventParticipate'] === 1 || response[x]['statusMemberEventParticipate'] === '1'){
                                                var printButtonJoin = statusMemberEventParticipatePrint;
                                           }else{
                                               var printButtonJoin = statusMemberEventNotParticipatePrint;
                                           }
                                      }
                                  }else{
                                      if(response[x]['event_type'] === 1 || response[x]['event_type'] === '1'){
                                           if(response[x]['statusMemberEventParticipate'] === 1 || response[x]['statusMemberEventParticipate'] === '1'){
                                                var printButtonJoin = statusMemberEventParticipatePrint;
                                           }else{
                                                var printButtonJoin = statusMemberEventNotParticipatePrint;
                                           }
                                      }else{
                                           if(response[x]['statusMemberEventParticipate'] === 1 || response[x]['statusMemberEventParticipate'] === '1'){
                                                var printButtonJoin = statusMemberEventParticipatePrint;
                                           }else{
                                                var printButtonJoin = '';
                                           }
                                      }
                                  }
                             }else{
                                  if(response[x]['checkStatusMember'] === 'member'){
                                       if(response[x]['event_type'] === 1 || response[x]['event_type'] === '1'){
                                            if(response[x]['statusMemberEventParticipate'] === 1 || response[x]['statusMemberEventParticipate'] === '1'){
                                                 var printButtonJoin = statusMemberEventParticipatePrint;
                                            }else{
                                                 var printButtonJoin = statusMemberEventNotParticipatePrint;
                                            } 
                                       }else{
                                            if(response[x]['statusMemberEventParticipate'] === 1 || response[x]['statusMemberEventParticipate'] === '1'){
                                                 var printButtonJoin = statusMemberEventParticipatePrint;
                                            }else{
                                                 var printButtonJoin = statusMemberEventNotParticipatePrint;
                                            }
                                       }
                                  }else{
                                      if(response[x]['event_type'] === 1 || response[x]['event_type'] === '1'){
                                           if(response[x]['statusMemberEventParticipate'] === 1 || response[x]['statusMemberEventParticipate'] === '1'){
                                                var printButtonJoin = statusMemberEventParticipatePrint;
                                           }else{
                                                var printButtonJoin = statusMemberGroupJoin;
                                           }
                                      }else{
                                           if(response[x]['statusMemberEventParticipate'] === 1 || response[x]['statusMemberEventParticipate'] === '1'){
                                                var printButtonJoin = statusMemberEventParticipatePrint;
                                           }else{
                                                var printButtonJoin = '';
                                           }
                                      }
                                  }

                             }    
                            
                        }else{
                             var printButtonJoin = '';
                        }
                    }
                    else{
                        var printButtonJoin = '';
                    }

                    
                     if(response[x]['countAnggotaParticipant'] === 0){
                        var user_participant = "not Participated";
                     }else{
                        var participant = response[x]['anggotaParticipant'];
                        var user_participant=[];
                        for(var i = 0; i <  participant.length; i++){
                           user_participant.push(participant[i]['username']);
                        }
                     }
                     

                  if(response[x]['setting_type'] === 0 || response[x]['setting_type'] === '0'){
                      var dataTrack =  "<span id='trackID-groupEvent"+response[x]['group_event_id']+"' style='display:none;'>"+response[x]['trackID']+"</span>"+
                                                "<span id='judulTrack-groupEvent"+response[x]['group_event_id']+"' style='display:none;'>"+response[x]['judulTrack']+"</span>"+
                                                "<span id='instrumentTrack-groupEvent"+response[x]['group_event_id']+"' style='display:none;'>"+response[x]['instrumentTrack']+"</span>"+
                                                "<span id='creatorTrack-groupEvent"+response[x]['group_event_id']+"' style='display:none;'>"+response[x]['creatorTrack']+"</span>"+
                                                "<span id='priceTrack-groupEvent"+response[x]['group_event_id']+"' style='display:none;'>"+response[x]['priceTrack']+"</span>"+
                                                "<span id='eventTitle-groupEvent"+response[x]['group_event_id']+"' style='display:none;'>"+response[x]['title']+"</span>"+
                                                "<span id='groupName-groupEvent"+response[x]['group_event_id']+"' style='display:none;'>"+response[x]['group_name']+"</span>";
                      var printCaptionSetting = "Back Song:";
                      var printTrackName = "<a onclick='playMusic("+'"'+response[x]['trackID']+'"'+")' style='cursor:pointer; text-decoration:none'> "+response[x]['judulTrack']+"</a>";
                  }else if(response[x]['setting_type'] === 1 || response[x]['setting_type'] === '1'){
                      var dataTrack =  "<span id='trackID-groupEvent"+response[x]['group_event_id']+"' style='display:none;'>"+response[x]['trackID']+"</span>"+
                                                "<span id='judulTrack-groupEvent"+response[x]['group_event_id']+"' style='display:none;'>"+response[x]['judulTrack']+"</span>"+
                                                "<span id='instrumentTrack-groupEvent"+response[x]['group_event_id']+"' style='display:none;'>"+response[x]['instrumentTrack']+"</span>"+
                                                "<span id='creatorTrack-groupEvent"+response[x]['group_event_id']+"' style='display:none;'>"+response[x]['creatorTrack']+"</span>"+
                                                "<span id='priceTrack-groupEvent"+response[x]['group_event_id']+"' style='display:none;'>"+response[x]['priceTrack']+"</span>"+
                                                "<span id='eventTitle-groupEvent"+response[x]['group_event_id']+"' style='display:none;'>"+response[x]['title']+"</span>"+
                                                "<span id='groupName-groupEvent"+response[x]['group_event_id']+"' style='display:none;'>"+response[x]['group_name']+"</span>";    
                      var printCaptionSetting = "Event dengan Song: ";
                      var printTrackName = "<a onclick='playMusic("+'"'+response[x]['trackID']+'"'+")' style='cursor:pointer; text-decoration:none'> "+response[x]['judulTrack']+"</a>";
                      // response[x]['judulTrack'];
                  }else{
                      var dataTrack = ''; 
                      var printCaptionSetting = "Event dengan Back Song & Song Bebas";
                      var printTrackName = "";
                  }  
                  
                 console.log(response)
                 document.getElementById("loadevent").style.display = "none";
                $('#printevent' ).append("<div class='timeline-box'>"+
                                     "<div class='warp-evt-group'>"+
                                     "<div class='tb-header'>"+
                                     "<div class='tb-pp'>"+
                                     url+
                                     "</div>"+
                                     "<div class='tb-info'>"+
                                     "<h3><a id='pjax' data-pjax='yes' href='"+base_url+"/"+response[x]['username']+"'>" + response[x]['username'] + "</a></h3>"+
                                     "</div>"+
                                     "<div class='tb-time'>"+
                                     "<p>"+ getDateTimeSince(d, false) +" ago </p>"+
                                     "</div>"+"</div>"+
                                     "<div class='tb-content'>"+
                                      "<div class='tb-img evt-g'>"+
                                          img_event +
                                      "</div>"+
                                      "<div class='tb-bname evt-t'>"+
                                        "<h2><a id='pjax' data-pjax='yes' style='text-decoration:none; cursor:pointer;' href='"+base_url+"/event/"+response[x]['group_event_id']+"'>" + response[x]['title'] + "</a></h2>"+
                                        "<h5>Start: " + start + "</h5>"+
                                        "<h5>End: " + end + "</h5>"+
                                        "<h5>"+printCaptionSetting+"</h5>"+
                                        "<h5> "+printTrackName+"</h5>"+
                                        "<div class='tag-evt'>"+
                                            tags+
                                        "</div>"+
                                                dataTrack+
                                            "<br>"+
                                            printButtonJoin+
                                            "<br><br>"+
                                                "<span id='totalParticipate"+response[x]['group_event_id']+"' >Participant:"+response[x]['countAnggotaParticipant']+"</span>"+
                                      "</div>"+
                                      "<div class='tb-desc evt-d'>"+
                                          "<div class='desc-evt-g'>"+
                                              "<p>" + response[x]['description'] + ""+
                                          "</div>"+
                                          
                                     "</div>"+
                                      "</div>"+
                                      "<div class='warp-stat-card in-tl'>"+
                                        "<div class='add-pl-track-song'></div>"+
                                        "<div class='stat-song-track'></div>"+
                                           contentLiked + 
                                        "<div class='stat-song-track'>"+
                                          "<div id='sharedEvent"+response[x]['group_event_id']+"' class='warp-ico-stat'>"+
                                              "<div class='ico-stat'><a onclick='sharedEventToSocial("+response[x]['group_event_id']+","+response[x]['shared']+");' style='cursor:pointer;'><i class='fa fa-mail-forward forward-single-list-song'></i></a></div>"+
                                              "<div class='num-stat'>"+response[x]['shared']+"</div>"+
                                          "</div>"+
                                        "</div>"+
                                           "<span id='eventTitle"+response[x]['group_event_id']+"' style='display:none;'>"+response[x]['title']+"</span>"+
                                           "<span id='eventDescription"+response[x]['group_event_id']+"' style='display:none;'>"+response[x]['description']+"</span>"+
                                           "<span id='eventPicture"+response[x]['group_event_id']+"' style='display:none;'>"+response[x]['picture_event']+"</span>"+
                                      "</div>"+
                                      
                                      "</div>"+
                                "</div>"+"</div>"
                                      );
               
                });

      }
    }
    });
}

function clickLikeGroupEvent(eventID,likeTotal){
  console.log(eventID);
  var a= 1;
  var totalLike = parseInt(likeTotal)+parseInt(a);
  var content = ReplaceGroupEventLike(eventID,totalLike);
  $('#groupEventLiked'+eventID).replaceWith(content);
    var url = window.location.origin+'/clickLikeGroupEventSave';

   $.ajax({
          url: url,
          type: 'POST',
          data: {eventID:eventID},
         
          success: function(responseText){
            var data = responseText['data'];
            console.log(data);
              console.log('sukses like group event');
          },
          error: function(){
              console.log('error');
              
          }

    });
}

function ReplaceGroupEventLike(eventID,totalLike){
  var content = "";
  content += "<div class='stat-song-track'  id='groupEventLiked"+eventID+"'>";
  content += "<div class='warp-ico-stat' >";
  content += "<div class='ico-stat'><a href='javascript:void(0)' onclick='udahLikePost();'><i class='fa fa-heart like-single-list-song' style ='color:red;'></i></a></div>";
  content += "<div class='num-stat'>"+totalLike+"</div>";
  content += "</div>";
  content += "</div>";

  return content;
}

function loadEventGroupRightGroup(){
  // console.log("totjong")loadingGroupEventGroup
    var id= document.getElementById("session").value;
    $.ajax({
          type : 'POST',
          url  : '/loadeventright/'+id,
          data : {id : id, skip : 0},
          success :  function(response){
            console.log(response)
            if (response.length == 0) {
                  $('#loadingGroupEventGroup').hide();
                  $('#printloadingGroupEvent').append("<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 padten'>"+
                                                       "<div class='nf-item-wall'>"+
                                                       "<p>You haven't Joined any event</p>"+
                                                       "</div>"+
                                                       "</div>");
            }else{
                var x = -1
                $.each(response, function( index, value ) {
                  x=x+1
                  var profile_image = response[x]['picture_event'];
                  var name_profile_image = profile_image.substring(profile_image.lastIndexOf('/')+1); // nama profile image
                  var folder = 'img/photo-event/';  // folder penyimpanan profile image
                  var picture = folder+name_profile_image; // link profile image

                  if(profile_image == 0){
                    profile_image_url = base_url+'/img/album/track-01.jpg';
                      }else{
                        if(profile_image != picture){
                              profile_image_url = profile_image;
                            }else{
                               profile_image_url = base_url+'/'+profile_image;
                            }
                      } 
                   var t = response[x]['created_at'].split(/[- :]/);
                   var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);   
                  // console.log(base_url)
                  // console.log(response)
                  // console.log(x)
                  if (response[x]['type']==1) {
                     var text = ""+response[x]['username']+" had joined event <a href='/event/"+response[x]['event_id']+"'>"+response[x]['title']+"</a></p>"
                  }else if (response[x]['type']==2) {
                    var text = ""+response[x]['username']+" created event "+response[x]['title']+" in group <a href='/group/latestpost/"+response[x]['group_id_enc']+"'>"+response[x]['group_name']+"</a></p>"
                  }
                  $('#loadingGroupEventGroup').hide(); 
                  $('#printloadingGroupEvent').prepend("<div class='gj-song-wrap'>"+
                                                 "<span href='#' class='list-notif'>"+
                                                 "<span class='pull-left thumb-sm img-notif-pp'>"+
                                                 "<img src='"+ profile_image_url +"' class='img-circle img-responsive img-pp-notif'>"+
                                                 // "<div class='gj-song-play'><i class='fa fa-play-circle-o fa-2x'></i></div>"+
                                                 "</span>"+
                                                 "<span class='notif-body block m-b-none'>"+
                                                 text+
                                                 "<small class='text-muted'>"+ getDateTimeSince(d, false) +" ago</small>"+
                                                 "</span>"+
                                                 "</span>"+
                                                 "</div>");
                });
                $('#printloadingGroupEvent').append("<div id='buttonload' class='col-xs-12'>"+
                                                 "<div class='warp-btn-loadmore-list'>"+
                                                 "<a onclick='loadMoreEventRightGroup()' style='cursor:pointer;' class='btn-loadmore-list'>"+
                                                 "<i class='fa fa-plus-circle'></i>"+
                                                 // "<div class='gj-song-play'><i class='fa fa-play-circle-o fa-2x'></i></div>"+
                                                 "<span class='text-load-more-list'>LOAD MORE</span>"+
                                                 "</a> "+
                                                 
                                                 "</div>"+
                                                 "</div>");
            }
          }
            
        })
  }   


function loadMoreEventRightGroup(){
  var id= document.getElementById("session").value;
  $("#loadingGroupEventGroup").css("display", "block");
  var skip2 = $("#skipevent").val()
  var skip = parseInt(skip2) + 2
  // $('#loadingMyWallEvent').show();
  console.log(skip)
    $.ajax({
          type : 'POST',
          url  : '/loadeventright/'+id,
          data : {id : id, skip : skip},
          success :  function(response){
            $("#skipevent").val(skip)
            console.log(response)
            if (response.length == 0) {
                  $('#loadingGroupEventGroup').hide();
                  $('#buttonload').hide();
                  $('#printloadingGroupEvent').append("<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 padten'>"+
                                                       "<div class='nf-item-wall'>"+
                                                       "<p>You Don't have any event more</p>"+
                                                       "</div>"+
                                                       "</div>");
            }else{
                var x = -1
                $.each(response, function( index, value ) {
                  x=x+1
                  var profile_image = response[x]['picture_event'];
                  var name_profile_image = profile_image.substring(profile_image.lastIndexOf('/')+1); // nama profile image
                  var folder = 'img/photo-event/';  // folder penyimpanan profile image
                  var picture = folder+name_profile_image; // link profile image

                  if(profile_image == 0){
                    profile_image_url = base_url+'/img/album/track-01.jpg';
                      }else{
                        if(profile_image != picture){
                              profile_image_url = profile_image;
                            }else{
                               profile_image_url = base_url+'/'+profile_image;
                            }
                      } 
                   var t = response[x]['created_at'].split(/[- :]/);
                   var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);   
                  // console.log(base_url)
                  // console.log(response)
                  // console.log(x)
                  if (response[x]['type']==1) {
                     var text = ""+response[x]['username']+" had joined event <a href='/event/"+response[x]['event_id']+"'>"+response[x]['title']+"</a></p>"
                  }else if (response[x]['type']==2) {
                    var text = ""+response[x]['username']+" created event "+response[x]['title']+" in group <a href='/group/latestpost/"+response[x]['group_id_enc']+"'>"+response[x]['group_name']+"</a></p>"
                  }
                  $('#loadingGroupEventGroup').hide(); 
                  $( "#loadingGroupEventGroup" ).before( "<div class='gj-song-wrap'>"+
                                                 "<span href='#' class='list-notif'>"+
                                                 "<span class='pull-left thumb-sm img-notif-pp'>"+
                                                 "<img src='"+ profile_image_url +"' class='img-circle img-responsive img-pp-notif'>"+
                                                 // "<div class='gj-song-play'><i class='fa fa-play-circle-o fa-2x'></i></div>"+
                                                 "</span>"+
                                                 "<span class='notif-body block m-b-none'>"+
                                                 text+
                                                 "<small class='text-muted'>"+ getDateTimeSince(d, false) +" ago</small>"+
                                                 "</span>"+
                                                 "</span>"+
                                                 "</div>" );
                  // $('#printloadingMyWallEvent').append("<div class='gj-song-wrap'>"+
                  //                                "<span href='#' class='list-notif'>"+
                  //                                "<span class='pull-left thumb-sm img-notif-pp'>"+
                  //                                "<img src='"+ profile_image_url +"' class='img-circle img-responsive img-pp-notif'>"+
                  //                                // "<div class='gj-song-play'><i class='fa fa-play-circle-o fa-2x'></i></div>"+
                  //                                "</span>"+
                  //                                "<span class='notif-body block m-b-none'>"+
                  //                                text+
                  //                                "<small class='text-muted'>"+ getDateTimeSince(d, false) +" ago</small>"+
                  //                                "</span>"+
                  //                                "</span>"+
                  //                                "</div>");
                });
                
            }
          }
            
        })
}

function getDaysInMonthgroup(month,year) {     
    if( typeof year == "undefined") year = 1999; // any non-leap-year works as default     
    var currmon = new Date(year,month),     
        nextmon = new Date(year,month+1);
    return Math.floor((nextmon.getTime()-currmon.getTime())/(24*3600*1000));
} 
function getDateTimeSincegroup(target, full) { // target should be a Date object
    var now = new Date(), diff, yd, md, dd, hd, nd, sd, out = [];
    diff = Math.floor(now.getTime()-target.getTime()/1000);
    
    yd = now.getFullYear() - target.getFullYear();
    md = now.getMonth() - target.getMonth();
    dd = now.getDate() - target.getDate();
    hd = now.getHours() - target.getHours();
    nd = now.getMinutes() - target.getMinutes();
    sd = now.getSeconds() - target.getSeconds();
    
    if( md < 0) {yd--; md += 12;}
    if( dd < 0) {
        md--;
        dd += getDaysInMonthgroup(now.getMonth()-1,now.getFullYear());
    }

    if( hd < 0) {dd--; hd += 24;}
    if( md < 0) {hd--; md += 60;}
    if( sd < 0) {md--; sd += 60;}

    var selesai = false;

    if(!full){
        if( yd > 0 && !selesai) {out.push( yd+" year"+(yd == 1 ? "" : "s")); selesai = true;}
        if( md > 0 && !selesai) {out.push( md+" month"+(md == 1 ? "" : "s")); selesai = true;}
        if( dd > 0 && !selesai) {out.push( dd+" day"+(dd == 1 ? "" : "s")); selesai = true;}
        if( hd > 0 && !selesai) {out.push( hd+" hour"+(hd == 1 ? "" : "s")); selesai = true;}
        if( nd > 0 && !selesai) {out.push( nd+" minute"+(nd == 1 ? "" : "s")); selesai = true;}
        if( sd > 0 && !selesai) {out.push( sd+" second"+(sd == 1 ? "" : "s")); selesai = true;}
    }
    else{
        if( yd > 0 ) out.push( yd+" year"+(yd == 1 ? "" : "s"));
        if( md > 0 ) out.push( md+" month"+(md == 1 ? "" : "s"));
        if( dd > 0 ) out.push( dd+" day"+(dd == 1 ? "" : "s")); 
        if( hd > 0 ) out.push( hd+" hour"+(hd == 1 ? "" : "s"));
        if( nd > 0 ) out.push( nd+" minute"+(nd == 1 ? "" : "s")); 
        if( sd > 0 ) out.push( sd+" second"+(sd == 1 ? "" : "s")); 
    }
    return out.join(", ");
}    

function submitpostgroup(){
  if (!$.trim($("#fieldpost").val())) {
     swal('Oops...','Please write description','error');
  }else{
  document.getElementById("loaderpost").style.display = "block";

  var user_id = document.getElementById("user_id").value;
  // var profile_image = document.getElementById("profile_image").value;
   var form = $('#postgroup');
   var data = form.serialize();
   console.log(data)
    $.ajax({
      type  : "POST",
      data : data,
      url   : "/submitpostgroup",
      success : function(response){

          document.getElementById("loaderpost").style.display = "none";
          console.log(response)
          document.getElementById("track_id").value = null;
                  var base_url = window.location.origin;
                   var str = response['profile_image']
                   var myarr = str.split(":");
                   var myvar = myarr[0];
                   // console.log(myvar)
                   if (myvar=="https") {
                    var url = "<a href=''><img class='img-responsive img-rounded-full' src='" + response['profile_image'] + "'></a>"
                  }else{
                    var url = "<a href=''><img class='img-responsive img-rounded-full' src='/" + response['profile_image'] + "'></a>"
                  }

            if (response['track_id']==0) {
              var t = response['created_at']['created_at'].split(/[- :]/);
                    var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
            $('#printpostgrup' ).prepend("<div class='timeline-box'>"+
                                        "<hr class='tb-hr'>"+
                                        "<div class='tb-header'>"+
                                        "<div class='tb-pp'>"+
                                        url+
                                        "</div>"+
                                        "<div class='tb-info'>"+
                                        "<h1><a href='' class='btn-single-post-tl'>" + response['username'] + "</a></h1>"+
                                        "</div>"+
                                        "<div class='tb-time'>"+
                                        "<p>a few second ago</p>"+
                                        "</div>"+"</div>"+
                                        "<div class='tb-content'>"+
                                        "<div class='warp-entry-com'>"+
                                        "<div class='tb-caption'>"+
                                        "<p>" + response['text'] + "</p>"+
                                        "</div>"+
                                         
                                        "</div>"+
                                        "<div class='warp-stat-card in-tl'>"+
                                        // "<div class='add-pl-track-song'>"+
                                        // "<a class='btn-add-pl-track-song'><i class='gritjam-icons gritjam-icon-add-playlist'></i></a>"+
                                        // "</div>"+
                                        
                                        
                                        "<div class='stat-song-track'>"+
                                          // "<div class='warp-ico-stat'>"+
                                          //     "<div class='ico-stat'><a href='javascript:void(0)'><i class='fa fa-magic spice-single-list-song'></i></a></div>"+
                                          //     "<div class='num-stat'>0</div>"+
                                          // "</div>"+
                                          "</div>"+
                                          "<div class='stat-song-track'>"+
                                          "<div class='warp-ico-stat'>"+
                                              "<div class='ico-stat'><a href='javascript:void(0);' onclick='shareSinglePost("+ response['post_id'] +")' class='btn-share-ts'><i class='fa fa-mail-forward forward-single-list-song'></i></a></div>"+
                                              "<div class='num-stat'>0</div>"+
                                          "</div>"+
                                        "</div>"+
                                          "<div class='stat-song-track'>"+
                                          "<div class='warp-ico-stat'>"+
                                              "<div class='ico-stat'><a id='likePostGroup-"+ response['post_id'] +"' style='cursor: pointer' onclick='likePostGroup("+ response['post_id'] +",1 , "+ response['post_id'] +");'><i class='fa fa-heart like-single-list-song' style='color: #e7e7e7;'></i></a></div>"+
                                              "<div id='num-stat-"+response['post_id']+ "' class='num-stat'>0</div>"+
                                          "</div>"+
                                        "</div>"+
                                          "</div>"+"</div>"+ 
                                          "<div class='warp-all-comment-post'>"+
                                          "<div id='loaderkomenall-" + response['post_id'] + "' style='display: none;'' class='col-xs-push-5 col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 lazyloader-list'>"+
                                          "<img src='/img/loader-more.gif' class='img-full-responsive e-centering width-loader'>"+
                                          "</div>" +
                                          "<div id='printallcomments-" + response['post_id'] + "'></div>"+
                                          "</div>"+
                                          "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sxs-12'>"+
                                          "<div class='row'>"+  
                                          "<div class='hr'></div>"+
                                          "<form id='komengrup-" + response['post_id'] + "' onsubmit='return komengrup(" + response['post_id'] + ")' class='warp-comment-post'>"+
                                          "<input name='user_id' type='hidden' value='"+ user_id +"'></input>"+
                                          "<input name='post_id' type='hidden' value='" + response['post_id'] + "'></input>"+
                                          "<div class='col-lg-1 col-md-1 col-sm-1 col-xs-1 col-sxs-2 padten'>"+
                                          "<div class='warp-user-comment-post'>"+
                                          "<img src='/"+ response['profile_image'] +"' class='img-responsive img-rounded-full'>"+
                                          "</div>"+
                                          "</div>"+
                                          "<div id='append-" + response['post_id'] + "' class='col-lg-9 col-md-9 col-sm-9 col-xs-9 col-sxs-7 pad-xs-phone'>"+
                                          "<textarea id='fieldkomen-" + response['post_id'] + "' name='komen' form='komengrup-" + response['post_id'] + "' class='form-control' placeholder='Write Comments ...''></textarea>"+
                                          "</div>"+
                                          "<div class='col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 related-md-desktop'>"+
                                          "<input type='submit' value='Comment' class='btn-compose-wall'>"+
                                          "<input type='reset' value='cancel' style='display: none;' id='buttoncancelcomment-" + response['post_id'] + "' onclick='cancelcommentreplygrup(" + response['post_id'] + ")' class='btn btn-comment-tl'>"+
                                          "</div></form></div></div></div>"
                      );
                document.getElementById("fieldpost").value=''
                document.getElementById("dropdownMenuLink").style.display="block"
        }else{
          var track_id = response['track_id']
                    var t = response['created_at']['created_at'].split(/[- :]/);
                    var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
                console.log(response)

                if (response['picture']!=0) {
                    var image = "<img class='img-responsive img-full-responsive' src='/" + response['picture'] + "'>"
                  }else{
                    var image = "<img class='img-responsive img-full-responsive' src='/img/album/album-song_475x475.jpg'>"
                  }

                // console.log(responseText)
                $('#printpostgrup' ).prepend("<div class='timeline-box'>"+
                                        "<hr class='tb-hr'>"+
                                        "<div class='tb-header'>"+
                                        "<div class='tb-pp'>"+
                                        url+
                                        "</div>"+
                                        "<div class='tb-info'>"+
                                        // "<h3><a href=''>" + response['username'] + "</a></h3>"+
                                        "<h1><a href='' class='btn-single-post-tl'>" + response['username'] + "</a></h1>"+
                                        "</div>"+
                                        "<div class='tb-time'>"+
                                        "<p>"+ getDateTimeSincegroup(d, false) +"</p>"+
                                        "</div>"+"</div>"+
                                        "<div class='tb-content'>"+
                                        "<div class='tb-img-post-song'>"+
                                          image+
                                          "<a style='cursor: pointer;' class='gj-btn-play'><i onclick='playMusic("+'"'+response['track_id_enc']+'"'+");' class='fa fa-play-circle-o fa-4x'></i></a>"+
                                        "</div>"+
                                        "<div class='warp-info-post-song'>"+
                                            "<div class='gj-post-song-title'><a href=''>" + response['judul'] + "</a></div>"+
                                              "<div class='gj-post-song-author'>" + response['artist'] + "</div>"+
                                              "<div class='gj-post-song-genre'>" + response['genre'] + "</div>"+
                                              "<div class='gj-post-song-contribut'>"+
                                                  "<span><a href='#'></a></span>"+
                                              "</div>"+
                                              "<div class='gj-post-song-desc'>"+
                                                    "<p id='more'>" + response['text'] + "</p>"+
                                           " </div>"+
                                          "</div>"+
                                        "<div class='warp-stat-card in-tl'>"+
                                        // "<div class='add-pl-track-song'>"+
                                        // "<a class='btn-add-pl-track-song'><i class='gritjam-icons gritjam-icon-add-playlist'></i></a>"+
                                        // "</div>"+
                                        
                                        
                                        "<div class='stat-song-track'>"+
                                          // "<div class='warp-ico-stat'>"+
                                          //     "<div class='ico-stat'><a href='javascript:void(0)'><i class='fa fa-magic spice-single-list-song'></i></a></div>"+
                                          //     "<div class='num-stat'>0</div>"+
                                          // "</div>"+
                                          "</div>"+
                                          "<div class='stat-song-track'>"+
                                          "<div class='warp-ico-stat'>"+
                                              "<div class='ico-stat'><a href='javascript:void(0);' onclick='shareSinglePost("+ response['post_id'] +")' class='btn-share-ts'><i class='fa fa-mail-forward forward-single-list-song'></i></a></div>"+
                                              "<div class='num-stat'>0</div>"+
                                          "</div>"+
                                        "</div>"+
                                          "<div class='stat-song-track'>"+
                                          "<div class='warp-ico-stat'>"+
                                              "<div class='ico-stat'><a id='likePostGroup-"+ response['post_id'] +"' style='cursor: pointer' onclick='likePostGroup("+ response['post_id'] +",1 , "+ response['post_id'] +");'><i class='fa fa-heart like-single-list-song' style='color: #e7e7e7;'></i></a></div>"+
                                              "<div id='num-stat-"+response['post_id']+ "' class='num-stat'>0</div>"+
                                          "</div>"+
                                        "</div>"+
                                          "</div>"+"</div>"+ 
                                          "<div class='warp-all-comment-post'>"+
                                          "<div id='loaderkomenall-" + response['post_id'] + "' style='display: none;'' class='col-xs-push-5 col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 lazyloader-list'>"+
                                          "<img src='/img/loader-more.gif' class='img-full-responsive e-centering width-loader'>"+
                                          "</div>" +
                                          "<div id='printallcomments-" + response['post_id'] + "'></div>"+
                                          "</div>"+
                                          "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sxs-12'>"+
                                          "<div class='row'>"+  
                                          "<div class='hr'></div>"+
                                          "<form id='komengrup-" + response['post_id'] + "' onsubmit='return komengrup(" + response['post_id'] + ")' class='warp-comment-post'>"+
                                          "<input name='user_id' type='hidden' value='"+ user_id +"'></input>"+
                                          "<input name='post_id' type='hidden' value='" + response['post_id'] + "'></input>"+
                                          "<div class='col-lg-1 col-md-1 col-sm-1 col-xs-1 col-sxs-2 padten'>"+
                                          "<div class='warp-user-comment-post'>"+
                                          "<img src='/"+ response['profile_image'] +"' class='img-responsive img-rounded-full'>"+
                                          "</div>"+
                                          "</div>"+
                                          "<div id='append-" + response['post_id'] + "' class='col-lg-9 col-md-9 col-sm-9 col-xs-9 col-sxs-7 pad-xs-phone'>"+
                                          "<textarea id='fieldkomen-" + response['post_id'] + "' name='komen' form='komengrup-" + response['post_id'] + "' class='form-control' placeholder='Write Comments ...''></textarea>"+
                                          "</div>"+
                                          "<div class='col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 related-md-desktop'>"+
                                          "<input type='submit' value='Comment' class='btn-compose-wall'>"+
                                          "<input type='reset' value='cancel' style='display: none;' id='buttoncancelcomment-" + response['post_id'] + "' onclick='cancelcommentreplygrup(" + response['post_id'] + ")' class='btn btn-comment-tl'>"+
                                          "</div></form></div></div></div>"
                      );
                $('#printselectsong').empty();
                document.getElementById("fieldpost").value=''
                document.getElementById("dropdownMenuLink").style.display="block"
            
        }
      }
    });
   return false;
 }
}

function loadmoregrup(){
  document.getElementById("loadermorepost").style.display = "block";
  document.getElementById("buttonloadmore").style.display = "none";
  var group_id = document.getElementById("group_id").value;
  var user_id = document.getElementById("user_id").value;
  var profile_image = document.getElementById("profile_image").value;
  var val = document.getElementById("skip").value;
  var skip = Number(val)+5;
  // console.log(val);
  var name_profile_image = profile_image.substring(profile_image.lastIndexOf('/')+1); // nama profile image
var folder = 'img/avatar/';  // folder penyimpanan profile image
var picture = folder+name_profile_image; // link profile image

 if(profile_image == 0){
    profile_image_url = base_url+'/img/avatar/avatar-300x300.png';
      }else{
        if(profile_image != picture){
              profile_image_url = profile_image;
            }else{
               profile_image_url = base_url+'/'+profile_image;
            }
      }    
 

  $.ajax({
      type  : "POST",
      data: {
       skip:skip,
       user_id:user_id,
       group_id:group_id
      },
      url   : "/loadmoregruppost",
      success : function(response){
        console.log(response);
        if (response['data'].length == 0) {
                document.getElementById("loadermorepost").style.display = "none";
                document.getElementById("buttonloadmore").style.display = "none";
                $( '#printpostgrupmore').append( "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 padten'>"+
                                  "<div class='nf-item-wall'>"+
                                  "<p>"+
                                  "This Group Doesn't Have More Post"+
                                  "</p>"+
                                  "</div>"+
                                  "</div>"

                  );
              }else{
                document.getElementById("loadermorepost").style.display = "none";
                document.getElementById("buttonloadmore").style.display = "inline-block";
                  var x = -1
                $.each(response['data'], function( index, value ) {
                  x=x+1
                  // console.log(response['list'][response['data'][x]['id']])
                  var t = response['data'][x]['created_at'].split(/[- :]/);
                    var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
                  var base_url = window.location.origin;
                   var str = response['data'][x]['profile_image']
                   var myarr = str.split(":");
                   var myvar = myarr[0];
                   // console.log(myvar)
                   if (myvar=="https") {
                    var url = "<a href=''><img class='img-responsive img-rounded-full' src='" + response['data'][x]['profile_image'] + "'></a>"
                  }else{
                    var url = "<a href=''><img class='img-responsive img-rounded-full' src='/" + response['data'][x]['profile_image'] + "'></a>"
                  }

                  if (response['list'][response['data'][x]['id']]==0) {
                      var read = "<p></p>"
                  }else{
                      // var read = "<a id='readallcomments-" + response['data'][x]['id'] + "' onclick='readallcomments(" + response['data'][x]['id'] + ")' class='more-txt'>read more comments</a>"+"<a id='hideallcomments-" + response['data'][x]['id'] + "' onclick='hideallcomments(" + response['data'][x]['id'] + ")' class='more-txt' style='display: none;'>Hide comments</a>"
                      var read = "<div class='tot-comment'>"+
                                  "<a style='cursor:pointer' id='readallcomments-" + response['data'][x]['id'] + "' onclick='readallcommentsNew(" + response['data'][x]['id'] + ","+response['list'][response['data'][x]['id']]+")'>"+
                                    "<i class='fa fa-comment'></i>Read "+response['list'][response['data'][x]['id']]+" Comments"+
                                  "</a>"+
                                    "<a id='hideallcomments-" + response['data'][x]['id'] + "' onclick='hideallcommentsNew(" + response['data'][x]['id'] + ","+response['list'][response['data'][x]['id']]+")' class='more-txt' style='display: none;'><i class='fa fa-comment'></i>Hide comments</a>"+  
                                  "</div>"
                  }

                  var xml;
                      $.ajax({
                        async: false,
                        type  : "POST",
                        data: {
                                post_id:response['data'][x]['id']
                              },
                        url   : "/checklikepost",
                        success : function(response2){
                          // console.log(response2)
                          window.xml = response2
                        }
                      })
                    if(window.xml == false){
                      console.log("like")
                      var content = "<a id='likePostGroup-"+ response['data'][x]['id'] +"' style='cursor: pointer' onclick='likePostGroup("+ response['data'][x]['id'] +",1 , "+ response['data'][x]['id'] +");'><i class='fa fa-heart like-single-list-song' style='color: #e7e7e7;'></i></a>"; 

                    }else{
                      console.log("unlike")
                      var content = "<a id='unlikePostGroup-"+ response['data'][x]['id'] +"' style='cursor: pointer' onclick='likePostGroup("+ response['data'][x]['id'] +",2 , "+ response['data'][x]['id'] +");'><i class='fa fa-heart like-single-list-song' style='color: red;'></i></a>";
                    }

                                          

                  if (response['data'][x]['track_id']==null || response['data'][x]['track_id']==0 || response['data'][x]['track_id']=="null" || response['data'][x]['track_id']=="0" || response['data'][x]['track_id']==="null" || response['data'][x]['track_id']==="0" || response['data'][x]['track_id']===null || response['data'][x]['track_id']===0) {

                                            
                     $('#printpostgrupmore' ).append("<div class='timeline-box'>"+
                                        "<hr class='tb-hr'>"+
                                        "<div class='tb-header'>"+
                                        "<div class='tb-pp'>"+
                                        url+
                                        "</div>"+
                                        "<div class='tb-info'>"+
                                        "<h1><a href='/" + response['data'][x]['username'] + "' class='btn-single-post-tl'>" + response['data'][x]['username'] + "</a></h1>"+
                                        "</div>"+
                                        "<div class='tb-time'>"+
                                        "<p>"+ getDateTimeSincegroup(d, false) +"</p>"+
                                        "</div>"+"</div>"+
                                        "<div class='tb-content'>"+
                                        "<div class='warp-entry-com'>"+
                                        "<div class='tb-caption'>"+
                                        "<p>" + response['data'][x]['caption'] + "<a id='pjax' data-pjax='yes' href='"+base_url+"/group/singlepost/"+response['data'][x]['id']+"/"+group_id+"' class='more-txt'>more</a></p>"+
                                        // "<a href='#' class='more-txt'>more</a>"+
                                        "</div>"+
                                        
                                        "</div>"+
                                        "<div class='warp-stat-card in-tl'>"+
                                         
                                        "<div class='stat-song-track'>"+
                                          // "<div class='warp-ico-stat'>"+
                                          //     "<div class='ico-stat'><a href='javascript:void(0)' onclick='clickLike();'><i class='fa fa-heart like-single-list-song'></i></a></div>"+
                                          //     "<div class='num-stat'>31</div>"+
                                          // "</div>"+
                                        "</div>"+
                                        "<div class='stat-song-track'>"+
                                          "<div class='warp-ico-stat'>"+
                                              "<div class='ico-stat'><a href='javascript:void(0);' onclick='shareSinglePost("+ response['data'][x]['id'] +")' class='btn-share-ts'><i class='fa fa-mail-forward forward-single-list-song'></i></a></div>"+
                                              "<div class='num-stat'>0</div>"+
                                          "</div>"+
                                        "</div>"+
                                        "<div class='stat-song-track'>"+
                                          "<div class='warp-ico-stat'>"+
                                              "<div class='ico-stat'>"+
                                              content+
                                              "</div>"+
                                              "<div id='num-stat-" + response['data'][x]['id'] + "' class='num-stat'>" + response['data'][x]['liked'] + "</div>"+
                                          "</div>"+"</div>"+"</div>"+"</div>"+
                                          "<div class='warp-all-comment-post'>"+
                                          "<div class='tb-more-comment ext-left'>"+
                                          read+
                                          "</div>"+
                                          "<div id='loaderkomenall-" + response['data'][x]['id'] + "' style='display: none;'' class='col-xs-push-5 col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 lazyloader-list'>"+
                                          "<img src='/img/loader-more.gif' class='img-full-responsive e-centering width-loader'>"+
                                          "</div>" +
                                          "<div id='printallcomments-" + response['data'][x]['id'] + "'></div>"+
                                        
                                        
                                          "</div>"+
                                          "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sxs-12'>"+
                                          "<div class='row'>"+  
                                          "<div class='hr'></div>"+
                                          "<form id='komengrup-" + response['data'][x]['id'] + "' onsubmit='return komengrup(" + response['data'][x]['id'] + ")' class='warp-comment-post'>"+
                                          "<input name='user_id' type='hidden' value='"+ user_id +"'></input>"+
                                          "<input name='post_id' type='hidden' value='" + response['data'][x]['id'] + "'></input>"+
                                          "<div class='col-lg-1 col-md-1 col-sm-1 col-xs-1 col-sxs-2 padten'>"+
                                          "<div class='warp-user-comment-post'>"+
                                          "<img src='"+ profile_image_url +"' class='img-responsive img-rounded-full'>"+
                                          "</div>"+
                                          "</div>"+
                                          "<div id='append-" + response['data'][x]['id'] + "' class='col-lg-8 col-md-8 col-sm-8 col-xs-8 col-sxs-6 pad-xs-phone'>"+
                                          "<textarea id='fieldkomen-" + response['data'][x]['id'] + "' name='komen' form='komengrup-" + response['data'][x]['id'] + "' class='form-control' placeholder='Write Comments ...''></textarea>"+
                                          "</div>"+
                                          "<div class='col-lg-3 col-md-3 col-sm-3 col-xs-3 col-sxs-3 tl-related-dev'>"+
                                          "<input style='margin-right: 5px;' type='submit' value='Comment' class='btn btn-comment-tl'>"+
                                          "<input type='reset' value='cancel' style='display: none;' id='buttoncancelcomment-" + response['data'][x]['id'] + "' onclick='cancelcommentreplygrup(" + response['data'][x]['id'] + ")' class='btn btn-comment-tl'>"+
                                          "</div></form></div></div></div>"
                      );
                                            var y = -1
                                            var komen_id = response['data'][x]['id']
                                            var itung = 1
                                            $.each(response['data123'], function( index, value ) {
                                            y=y+1
                                            var length = response['replycomment'][y].length
                                            if (length!=0) {
                                              var teks = "<div class='comment-in-comment' id='grup-comment-in-comment-"+response['data123'][y]['comment_id']+"'><a id='seeSubKomenGroup-"+response['data123'][y]['comment_id']+"' href='javascript:toggleAndChangeReplyGroup("+response['data123'][y]['comment_id']+");' class='btn-expand-comment'>See all comment</a><div class='num-comment'><span>"+length+"</span> Comment hidden</div>"
                                            }else{
                                              var teks = "<div id='grup-comment-in-comment-"+response['data123'][y]['comment_id']+"'>"
                                            }
                                            if (komen_id == response['data123'][y]['group_post_id']) {
                                              if (response['list']['group_post_id']<=3) {
                                                var tbkomen = "<div name=comment-"+response['data123'][y]['comment_id']+" class='tb-comment totkomengrup-"+response['data'][x]['id']+"'>"
                                              }else{
                                                if (itung <= 3) {
                                                  var tbkomen = "<div name=comment-"+response['data123'][y]['comment_id']+" class='tb-comment totkomengrup-"+response['data'][x]['id']+"'>"
                                                }else{
                                                  var tbkomen = "<div style='display: none;' name=comment-"+response['data123'][y]['comment_id']+" class='tb-comment totkomengrup-"+response['data'][x]['id']+"'>"
                                                }
                                              }
                                              itung = itung+1

                                               var base_url = window.location.origin;
                                               var str = response['data123'][y]['profile_image']
                                               var myarr = str.split(":");
                                               var myvar = myarr[0];
                                               // console.log(myvar)
                                               if (myvar=="https") {
                                                var url = "<a href=''><img src='" + response['data123'][y]['profile_image'] + "'></a>"
                                              }else{
                                                var url = "<a href=''><img src='/" + response['data123'][y]['profile_image'] + "'></a>"
                                              }

                                              if (response['data123'][y]['liked']==1) {
                                                var liketext = "<a id='like"+response['data123'][y]['comment_id']+"' style='cursor: pointer' onclick='likeCommentGroup("+response['data123'][y]['comment_id']+",2 , "+response['data123'][y]['comment_id']+");'>Unlike</a>"
                                              }else{
                                                var liketext = "<a id='like"+response['data123'][y]['comment_id']+"' style='cursor: pointer' onclick='likeCommentGroup("+response['data123'][y]['comment_id']+",1 , "+response['data123'][y]['comment_id']+");'>Like</a>"
                                              }

                                              $('#printallcomments-'+response['data'][x]['id']).append(
                                              tbkomen+
                                              "<div class='hr-comment'></div>"+
                                              "<div class='tb-pp'>"+
                                              url+
                                              "</div>"+
                                              "<div class='tb-text-comment'>"+
                                              "<div class='tb-info'>"+
                                              "<h3><a href='/"+response['data123'][y]['username']+"'>"+response['data123'][y]['username']+"</a></h3>"+
                                              "</div>"+
                                              "<div class='tb-time'>"+
                                              "<p>"+response['data123'][y]['created_at']+"</p>"+
                                              "</div>"+
                                              "<div class='tb-caption'>"+
                                              "<p>"+response['data123'][y]['comment']+"</p>"+
                                              "<div class='tb-like text-right'>"+
                                              liketext+
                                              "<a class='replycommentgrup' id='replycommentgrup-"+response['data123'][y]['comment_id']+"' onclick='replycommentgrup("+response['data'][x]['id']+","+response['data123'][y]['comment_id']+")'>Reply</a>"+
                                              "</div>"+"</div>"+"</div>"+
                                                teks+
                                                "<div style='display: none;' id='subKomen_1-"+response['data123'][y]['comment_id']+"' class='warp-sub-comment sub-comment'></div>"+
                                                "<div name='textareagrup-"+response['data123'][y]['comment_id']+"' id='textareagrup-"+response['data123'][y]['comment_id']+"'></div>"+
                                              "</div>"+
                                              "</div>"
                                              )

                                              var z = -1
                                            
                                              $.each(response['replycomment'][y], function( index, value ) {
                                              z=z+1
                                            
                                              if (response['data123'][y]['comment_id'] == response['replycomment'][y][z]['comment_id']) {

                                                var base_url = window.location.origin;
                                               var str = response['replycomment'][y][z]['profile_image']
                                               var myarr = str.split(":");
                                               var myvar = myarr[0];
                                               // console.log(myvar)
                                               if (myvar=="https") {
                                                var url5 = "<a href=''><img src='" + response['replycomment'][y][z]['profile_image'] + "'></a>"
                                              }else{
                                                var url5 = "<a href=''><img src='/" + response['replycomment'][y][z]['profile_image'] + "'></a>"
                                              }
                                                // console.log("mek")
                                                $('#subKomen_1-'+response['replycomment'][y][z]['comment_id']).append(
                                                "<div class='tb-comment'>"+
                                                "<div class='hr-comment'></div>"+
                                                "<div class='tb-pp'>"+
                                                url5+
                                                "</div>"+
                                                "<div class='tb-text-comment'>"+
                                                "<div class='tb-info'>"+
                                                "<h3><a href='/"+response['replycomment'][y][z]['username']+"'>"+response['replycomment'][y][z]['username']+"</a></h3>"+
                                                "</div>"+
                                                "<div class='tb-time'>"+
                                                "<p style='margin-bottom: 0; font-size: 11px; color: #ccc; line-height: 9px;'>29 days</p>"+
                                                "</div>"+
                                                "<div class='tb-caption'> <p>"+response['replycomment'][y][z]['contents']+"<a href='#' class='more-txt'></a></p>"+
                                                "<div class='tb-like text-right'>"+
                                                "<a href=''></a>"+
                                                "<a href=''></a>"+
                                                "</div>"+"</div>"+"</div>"+"</div>"+"</div>"
                                                )
                                            }else {var tot = "<p>memek</p>";}
                                          })

                                          }else {var tot = "<p>memek</p>";}
                                        })  

                                            

                      document.getElementById("skip").value = Number(val)+3;
                    }else{
                     if (response['data'][x]['hashPicture']!=0) {
                        var url1 = "<img src='/"+response['data'][x]['hashPicture']+"' class='img-responsive img-full-responsive'>"
                      }else{
                        var url1 = "<img src='/img/album/album-song_475x475.jpg' class='img-responsive img-full-responsive'>"
                      }
                                            
                      $('#printpostgrupmore' ).append("<div class='timeline-box'>"+
                                        "<hr class='tb-hr'>"+
                                        "<div class='tb-header'>"+
                                        "<div class='tb-pp'>"+
                                        url+
                                        "</div>"+
                                        "<div class='tb-info'>"+
                                        // "<h3><a href=''>" + response['data'][x]['username'] + "</a></h3>"+
                                        "<h1><a href='/" + response['data'][x]['username'] + "' class='btn-single-post-tl'>" + response['data'][x]['username'] + "</a></h1>"+
                                        "</div>"+
                                        "<div class='tb-time'>"+
                                        "<p>"+ getDateTimeSincegroup(d, false) +"</p>"+
                                        "</div>"+"</div>"+
                                        "<div class='tb-content'>"+
                                        "<div class='tb-img-post-song'>"+
                                          url1+
                                          "<a style='cursor: pointer;' class='gj-btn-play'><i onclick='playMusic("+'"'+response['data'][x]['track_details_id']+'"'+");' class='fa fa-play-circle-o fa-4x'></i></a>"+
                                        "</div>"+
                                        "<div class='warp-info-post-song'>"+
                                            "<div class='gj-post-song-title'><a href=''>" + response['data'][x]['name'] + "</a></div>"+
                                              "<div class='gj-post-song-author'>" + response['data'][x]['artist'] + "</div>"+
                                              "<div class='gj-post-song-genre'>" + response['data'][x]['genre'] + "</div>"+
                                              "<div class='gj-post-song-contribut'>"+
                                                  "<span><a href='#'></a></span>"+
                                              "</div>"+
                                              "<div class='gj-post-song-desc'>"+
                                                    "<p id='more " + response['data'][x]['id'] + "'>" + response['data'][x]['caption'] + "<a id='pjax' data-pjax='yes' href='"+base_url+"/group/singlepost/"+response['data'][x]['id']+"/"+group_id+"' class='more-txt'>more</a></p>"+
                                                    // "<a href='#' class='more-txt'>more</a>"+
                                           " </div>"+
                                          "</div>"+
                                        // "<div class='warp-entry-com'>"+
                                        // "<div class='tb-bname'>"+
                                        //       "<h2>" + response['data'][x]['artist'] + "</h2>"+
                                        //       "<h5>" + response['data'][x]['genre'] + "</h5>"+
                                        //   "</div>"+
                                        // "<div class='tb-caption'>"+
                                        // "<p>" + response['data'][x]['caption'] + "</p>"+
                                        // "</div>"+
                                         
                                        // "</div>"+
                                        "<div class='warp-stat-card in-tl'>"+
                                        // "<div class='add-pl-track-song'>"+
                                        // "<a class='btn-add-pl-track-song'><i class='gritjam-icons gritjam-icon-add-playlist'></i></a>"+
                                        // "</div>"+
                                        "<div class='stat-song-track'>"+
                                          // "<div class='warp-ico-stat'>"+
                                              // "<div class='ico-stat'><a href='javascript:void(0)' onclick='clickLike();'><i class='fa fa-heart like-single-list-song'></i></a></div>"+
                                              // "<div class='num-stat'>31</div>"+readallcomme
                                          // "</div>"+
                                        "</div>"+
                                        "<div class='stat-song-track'>"+
                                          "<div class='warp-ico-stat'>"+
                                              "<div class='ico-stat'><a href='javascript:void(0);' onclick='shareSinglePost("+ response['data'][x]['id'] +")' class='btn-share-ts'><i class='fa fa-mail-forward forward-single-list-song'></i></a></div>"+
                                              "<div class='num-stat'>0</div>"+
                                          "</div>"+
                                        "</div>"+
                                        "<div class='stat-song-track'>"+
                                          "<div class='warp-ico-stat'>"+
                                              "<div class='ico-stat'>"+
                                              content+
                                              "</div>"+
                                              "<div id='num-stat-" + response['data'][x]['id'] + "' class='num-stat'>" + response['data'][x]['liked'] + "</div>"+
                                          "</div>"+"</div>"+"</div>"+"</div>"+ 
                                          "<div class='warp-all-comment-post'>"+
                                          "<div class='tb-more-comment ext-left'>"+
                                          read+
                                          "</div>"+
                                          "<div id='loaderkomenall-" + response['data'][x]['id'] + "' style='display: none;'' class='col-xs-push-5 col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 lazyloader-list'>"+
                                          "<img src='/img/loader-more.gif' class='img-full-responsive e-centering width-loader'>"+
                                          "</div>" +
                                          "<div id='printallcomments-" + response['data'][x]['id'] + "'></div>"+
                                          
                                          "</div>"+
                                          "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sxs-12'>"+
                                          "<div class='row'>"+  
                                          "<div class='hr'></div>"+
                                          "<form id='komengrup-" + response['data'][x]['id'] + "' onsubmit='return komengrup(" + response['data'][x]['id'] + ")' class='warp-comment-post'>"+
                                          "<input name='user_id' type='hidden' value='"+ user_id +"'></input>"+
                                          "<input name='post_id' type='hidden' value='" + response['data'][x]['id'] + "'></input>"+
                                          "<div class='col-lg-1 col-md-1 col-sm-1 col-xs-1 col-sxs-2 padten'>"+
                                          "<div class='warp-user-comment-post'>"+
                                          "<img src='"+ profile_image_url +"' class='img-responsive img-rounded-full'>"+
                                          "</div>"+
                                          "</div>"+
                                          "<div id='append-" + response['data'][x]['id'] + "' class='col-lg-8 col-md-8 col-sm-8 col-xs-8 col-sxs-6 pad-xs-phone'>"+
                                          "<textarea id='fieldkomen-" + response['data'][x]['id'] + "' name='komen' form='komengrup-" + response['data'][x]['id'] + "' class='form-control' placeholder='Write Comments ...''></textarea>"+
                                          "</div>"+
                                          "<div class='col-lg-3 col-md-3 col-sm-3 col-xs-3 col-sxs-3 tl-related-dev'>"+
                                          "<input style='margin-right: 5px;' type='submit' value='Comment' class='btn btn-comment-tl'>"+
                                          "<input type='reset' value='cancel' style='display: none;' id='buttoncancelcomment-" + response['data'][x]['id'] + "' onclick='cancelcommentreplygrup(" + response['data'][x]['id'] + ")' class='btn btn-comment-tl'>"+
                                          "</div></form></div></div></div>"
                      );
                                            var y = -1
                                            var komen_id = response['data'][x]['id']
                                            var itung = 1
                                            $.each(response['data123'], function( index, value ) {
                                            y=y+1
                                            var length = response['replycomment'][y].length
                                            if (length!=0) {
                                              var teks = "<div class='comment-in-comment' id='grup-comment-in-comment-"+response['data123'][y]['comment_id']+"'><a id='seeSubKomenGroup-"+response['data123'][y]['comment_id']+"' href='javascript:toggleAndChangeReplyGroup("+response['data123'][y]['comment_id']+");' class='btn-expand-comment'>See all comment</a><div class='num-comment'><span>"+length+"</span> Comment hidden</div>"
                                            }else{
                                              var teks = "<div id='grup-comment-in-comment-"+response['data123'][y]['comment_id']+"'>"
                                            }
                                            if (komen_id == response['data123'][y]['group_post_id']) {
                                              if (response['list']['group_post_id']<=3) {
                                                var tbkomen = "<div name=comment-"+response['data123'][y]['comment_id']+" class='tb-comment totkomengrup-"+response['data'][x]['id']+"'>"
                                              }else{
                                                if (itung <= 3) {
                                                  var tbkomen = "<div name=comment-"+response['data123'][y]['comment_id']+" class='tb-comment totkomengrup-"+response['data'][x]['id']+"'>"
                                                }else{
                                                  var tbkomen = "<div style='display: none;' name=comment-"+response['data123'][y]['comment_id']+" class='tb-comment totkomengrup-"+response['data'][x]['id']+"'>"
                                                }
                                              }
                                              itung = itung+1
                                              var base_url = window.location.origin;
                                               var str = response['data123'][y]['profile_image']
                                               var myarr = str.split(":");
                                               var myvar = myarr[0];
                                               // console.log(myvar)
                                               if (myvar=="https") {
                                                var url3 = "<a href=''><img src='" + response['data123'][y]['profile_image'] + "'></a>"
                                              }else{
                                                var url3 = "<a href=''><img src='/" + response['data123'][y]['profile_image'] + "'></a>"
                                              }

                                              if (response['data123'][y]['liked']==1) {
                                                var liketext = "<a id='like"+response['data123'][y]['comment_id']+"' style='cursor: pointer' onclick='likeCommentGroup("+response['data123'][y]['comment_id']+",2 , "+response['data123'][y]['comment_id']+");'>Unlike</a>"
                                              }else{
                                                var liketext = "<a id='like"+response['data123'][y]['comment_id']+"' style='cursor: pointer' onclick='likeCommentGroup("+response['data123'][y]['comment_id']+",1 , "+response['data123'][y]['comment_id']+");'>Like</a>"
                                              }

                                              $('#printallcomments-'+response['data'][x]['id']).append(
                                              tbkomen+
                                              "<div class='hr-comment'></div>"+
                                              "<div class='tb-pp'>"+
                                              url3+
                                              "</div>"+
                                              "<div class='tb-text-comment'>"+
                                              "<div class='tb-info'>"+
                                              "<h3><a href='/"+response['data123'][y]['username']+"'>"+response['data123'][y]['username']+"</a></h3>"+
                                              "</div>"+
                                              "<div class='tb-time'>"+
                                              "<p>"+response['data123'][y]['created_at']+"</p>"+
                                              "</div>"+
                                              "<div class='tb-caption'>"+
                                              "<p>"+response['data123'][y]['comment']+"</p>"+
                                              "<div class='tb-like text-right'>"+
                                              liketext+
                                              "<a class='replycommentgrup' id='replycommentgrup-"+response['data123'][y]['comment_id']+"' onclick='replycommentgrup("+response['data'][x]['id']+","+response['data123'][y]['comment_id']+")'>Reply</a>"+
                                              "</div>"+"</div>"+"</div>"+
                                                teks+
                                                "<div style='display: none;' id='subKomen_1-"+response['data123'][y]['comment_id']+"' class='warp-sub-comment sub-comment'></div>"+
                                                "<div name='textareagrup-"+response['data123'][y]['comment_id']+"' id='textareagrup-"+response['data123'][y]['comment_id']+"'></div>"+
                                              "</div>"+
                                              "</div>"
                                              )

                                              var z = -1
                                            
                                              $.each(response['replycomment'][y], function( index, value ) {
                                              z=z+1
                                              
                                              if (response['data123'][y]['comment_id'] == response['replycomment'][y][z]['comment_id']) {

                                               var base_url = window.location.origin;
                                               var str = response['replycomment'][y][z]['profile_image']
                                               var myarr = str.split(":");
                                               var myvar = myarr[0];
                                               // console.log(myvar)
                                               if (myvar=="https") {
                                                var url5 = "<a href=''><img src='" + response['replycomment'][y][z]['profile_image'] + "'></a>"
                                              }else{
                                                var url5 = "<a href=''><img src='/" + response['replycomment'][y][z]['profile_image'] + "'></a>"
                                              }
                                                
                                                $('#subKomen_1-'+response['replycomment'][y][z]['comment_id']).append(
                                                "<div class='tb-comment'>"+
                                                "<div class='hr-comment'></div>"+
                                                "<div class='tb-pp'>"+
                                                url5+
                                                "</div>"+
                                                "<div class='tb-text-comment'>"+
                                                "<div class='tb-info'>"+
                                                "<h3><a href='/"+response['replycomment'][y][z]['username']+"'>"+response['replycomment'][y][z]['username']+"</a></h3>"+
                                                "</div>"+
                                                "<div class='tb-time'>"+
                                                "<p style='margin-bottom: 0; font-size: 11px; color: #ccc; line-height: 9px;'>29 days</p>"+
                                                "</div>"+
                                                "<div class='tb-caption'> <p>"+response['replycomment'][y][z]['contents']+"<a href='#' class='more-txt'></a></p>"+
                                                "<div class='tb-like text-right'>"+
                                                "<a href=''></a>"+
                                                "<a href=''></a>"+
                                                "</div>"+"</div>"+"</div>"+"</div>"+"</div>"
                                                )
                                            }else {var tot = "<p>memek</p>";}
                                          })

                                          }else {var tot = "<p>memek</p>";}
                                        })


                     document.getElementById("skip").value = Number(val)+3;
                    }
                })
              }
        
      }
    });
}

function replycommentgrup(post_id,komen_id){
      // console.log("tot")
      var pic = $('#profpicgrup').attr("src")
      // console.log(pic)
      $('#textareagrup-'+komen_id).append("<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sxs-12'>"+
                                      "<div class='row'>"+
                                      "<div id='loadgifkomengrup2-"+komen_id+"' style='display: none;'>"+
                                      "<img src='/img/loader-more.gif' class='img-full-responsive e-centering width-loader' >"+
                                      "</div>"+
                                      "<div class='hr'>"+
                                      "</div>"+
                                      "<form id='komenreply-"+komen_id+"' onsubmit='return submitkomenreplygrup("+post_id+","+komen_id+")' class='warp-comment-post'>"+
                                      "<div class='col-lg-1 col-md-1 col-sm-1 col-xs-1 col-sxs-2 padten'>"+
                                      "<div class='warp-user-comment-post'>"+
                                      "<img src='"+pic+"' class='img-responsive img-rounded-full'>"+
                                      "</div></div>"+
                                      "<input style='display:none;' id='comment_id' name='comment_id' value='" + komen_id + "'>"+
                                      "<input style='display:none;' id='post_id' name='post_id' value='" + post_id + "'>"+
                                      "<div class='col-lg-8 col-md-8 col-sm-8 col-xs-8 col-sxs-6 pad-xs-phone'>"+
                                      "<textarea form='komenreply-"+komen_id+"' required name='komen' id='commentsreplygrup-"+komen_id+"' class='form-control' placeholder='Write Comments ...'>"+
                                      "</textarea></div><div class='col-lg-3 col-md-3 col-sm-3 col-xs-3 col-sxs-3 tl-related-dev'>"+
                                      "<button style='margin-right: 5px;' type='submit' class='btn btn-comment-tl'>Comment</button>"+
                                      "<input type='reset' value='cancel' id='buttoncancelcommentgrup-"+komen_id+"' onclick='cancelcommentreplygrup("+komen_id+")' class='btn btn-comment-tl'>"+
                                      "</div>"+
                                      "</form>"+
                                      "</div></div>"
                                      );
      // $('#comments-'+post_id).focus();
      // "<div id='loadgifkomengrup2-"+komen_id+"' style='display: none;'>"
      // "<img src='/img/loader-more.gif' class='img-full-responsive e-centering width-loader' >"
      // "</div>"
      $('#commentsreplygrup-'+komen_id).focus()
      //echo "<textarea required form='komen-" . $key['id'] . "' id='comments-" . $key['id'] . "' name='comment' class='form-control' placeholder='Write Comments ...'></textarea>";
      //  $('#komen-'+post_id).attr('onsubmit', 'return submitkomenreply('+post_id+','+komen_id+')');
      $('.replycommentgrup').removeAttr("style").hide();
      // $('#append-'+post_id).append("<input type='hidden' id='comment_id' name='comment_id' value=" + komen_id + ">")
      // document.getElementById("buttoncancelcomment-"+post_id).style.display="inline-block"
    }

// function replycommentgrup(post_id,komen_id){
//       // console.log("tot")
//       $('#fieldkomen-'+post_id).focus();
//        $('#komengrup-'+post_id).attr('onsubmit', 'return submitkomenreplygrup('+post_id+','+komen_id+')');
//       $('.replycommentgrup').removeAttr("style").hide();
//       $('#append-'+post_id).append("<input type='hidden' id='comment_id' name='comment_id' value=" + komen_id + ">")
//       document.getElementById("buttoncancelcomment-"+post_id).style.display="inline-block"
//     }

    function cancelcommentreplygrup(komen_id){
      $('.replycommentgrup').removeAttr("style").show();
      $('#textareagrup-'+komen_id).empty()
    }

function submitkomenreplygrup(post_id,komen_id){
     var form = $('#komenreply-'+komen_id);
      var data = form.serialize();
      console.log(data)
      document.getElementById("loadgifkomengrup2-"+komen_id).style.display = "block";
      $.ajax({
        type: 'post',
        url: '/submitkomenreplygrup',
        data: data,
        success: function (response) {
          document.getElementById("loadgifkomengrup2-"+komen_id).style.display = "none";
          var t = response['newComment']['created_at'].split(/[- :]/);
          var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);

          $('#textareagrup-'+komen_id).empty()
          // document.getElementById("buttoncancelcomment-"+post_id).style.display="none"
          $('.replycommentgrup').removeAttr("style").show();
          // $('#komengrup-'+post_id).attr('onsubmit', 'return komengrup('+post_id+')');
          // document.getElementById("fieldkomen-" + post_id + "").value=''

          console.log(response);

                   var base_url = window.location.origin;
                   var str = response['user']['profile_image']
                   var myarr = str.split(":");
                   var myvar = myarr[0];
                   // console.log(myvar)
                   if (myvar=="https") {
                    var url = "<a href=''><img src='" + response['user']['profile_image'] + "'></a>"
                  }else{
                    var url = "<a href=''><img src='/" + response['user']['profile_image'] + "'></a>"
                  }

          $('#grup-comment-in-comment-'+komen_id).append("<div id='subKomen_1' class='warp-sub-comment sub-comment'>"+
                                                        "<div class='tb-comment'>"+
                                                            "<div class='hr-comment'></div>"+  
                                                          
                                                          "<div class='tb-pp'>"+
                                                              url+
                                                          "</div>"+
                                                          "<div class='tb-text-comment'>"+ 
                                                          "<div class='tb-info'>"+
                                                              "<h3><a href=''>" + response['user']['username'] + "</a></h3>"+
                                                          "</div>"+
                                                          "<div class='tb-time'>"+
                                                              "<p>a few second ago</p>"+
                                                          "</div>"+
                                                          "<div class='tb-caption'>"+
                                                              "<p>" + response['newComment']['contents'] + "</p>"+
                                                          
                                                              
                                                              "<div class='tb-like text-right'>"+
                                                                  // "<a href=''>Like</a>"+
                                                                  // "<a href=''>Reply</a>"+
                                                              "</div>"+
                                                              "</div>"+
                                                          "</div>"+
                                                        "</div>"+
                                              "</div>");
          }
      });

      return false;
}

function komengrup(id){
  document.getElementById("loaderkomenall-"+id).style.display = "block";
    var form = $('#komengrup-'+id);
    var data = form.serialize();
    // console.log(data)
     $.ajax({
      type  : "POST",
      data : data,
      url   : "/komenpostgrup",
      success : function(response){
        var t = response['created_at'].split(/[- :]/);
        var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);

        document.getElementById("loaderkomenall-"+id).style.display = "none";
        console.log(response)
         if (response.length == 0) {
                // document.getElementById("readallcomments-"+id).style.display = "none";
                document.getElementById("loaderkomenall-"+id).style.display = "none";
                $( '#printallcomments-'+id).append( "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 padten'>"+
                                  "<div class='nf-item-wall'>"+
                                  "<p>"+
                                  "try again"+
                                  "</p>"+
                                  "</div>"+
                                  "</div>"

                  );
              }else{
                // document.getElementById("readallcomments-"+id).style.display = "none";
                document.getElementById("loaderkomenall-"+id).style.display = "none";
        // console.log(response)
              
                   var base_url = window.location.origin;
                   var str = response['profile_image']
                   var myarr = str.split(":");
                   var myvar = myarr[0];
                   // console.log(myvar)
                   if (myvar=="https") {
                    var url = "<a href=''><img class='img-responsive img-rounded-full' src='" + response['profile_image'] + "'></a>"
                  }else{
                    var url = "<a href=''><img class='img-responsive img-rounded-full' src='/" + response['profile_image'] + "'></a>"
                  }
                  document.getElementById("fieldkomen-" + id + "").value=''
        $( '#printallcomments-'+id).append("<div class='tb-comment'>"+
                                            "<div class='hr-comment'></div>"+  
                                          "<div class='tb-pp'>"+
                                             url+
                                         "</div>"+
                                         "<div class='tb-text-comment'>"+ 
                                          "<div class='tb-info'>"+
                                              "<h3><a href=''>" + response['username'] +"</a></h3>"+
                                          "</div>"+
                                          "<div class='tb-time'>"+
                                              "<p>a few second ago</p>"+
                                          "</div>"+
                                          "<div class='tb-caption'>"+
                                              "<p>" + response['komen'] +
                                                "<a href='#' class='more-txt'></a>"+
                                              "</p>"+
                                              "<div class='tb-like text-right'>"+
                                                  "<a href=''>Like</a>"+
                                                  "<a class='replycommentgrup' id='replycommentgrup-" + response['comment_id'] + "' onclick='replycommentgrup("+id+","+ response['comment_id'] +")'>Reply</a>"+
                                              
                                              "</div>"+
                                              
                                          "</div>"+
                                          
                                          "</div>"+
                                      "</div>"+
                                      "<div id='grup-comment-in-comment-" + response['comment_id'] + "' class='comment-in-comment'>"+
                                      "<div name='textareagrup-" + response['comment_id'] + "' id='textareagrup-" + response['comment_id'] + "'></div>"+
                                      "</div>"+
                                      "</div>"  
                                      )
     
      }
      }
    });
    return false;
}

function readallcomments(id){
  document.getElementById("loaderkomenall-"+id).style.display = "block";
  $.ajax({
      type  : "POST",
      data: {
              id:id
            },
      url   : "/readallcomments",
      success : function(response){
        console.log(response)
        if (response['data'].length == 0) {

                document.getElementById("readallcomments-"+id).style.display = "none";
                document.getElementById("loaderkomenall-"+id).style.display = "none";
                $( '#printallcomments-'+id).append( "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 padten'>"+
                                  "<div class='nf-item-wall'>"+
                                  "<p>"+
                                  "No comment"+
                                  "</p>"+
                                  "</div>"+
                                  "</div>"

                  );
              }else{
                document.getElementById("printallcomments-"+id).style.display="block";
                document.getElementById("readallcomments-"+id).style.display = "none";

                document.getElementById("loaderkomenall-"+id).style.display = "none";
                document.getElementById("hideallcomments-"+id).style.display = "block";
        // console.log(response)
               var x = -1
        $.each(response['data'], function( index, value ) {

                x=x+1

          if (response['replycomment'][x].length==0) {
            var toggle = "<div id='grup-comment-in-comment-" + response['data'][x]['id'] + "'>"
          }else{
            var toggle = "<div id='grup-comment-in-comment-" + response['data'][x]['id'] + "' class='comment-in-comment'>"+
                                      "<a id='seeSubKomenGroup-" + response['data'][x]['id'] + "' href='javascript:toggleAndChangeReplyGroup(" + response['data'][x]['id'] + ");' class='btn-expand-comment'>See all comment</a>"+
                                             "<div class='num-comment'><span>" + response['replycomment'][x].length + "</span> Comment hidden</div>"
          }

                var xml;
                $.ajax({
                  async: false,
                  type  : "POST",
                  data: {
                          comment_id:response['data'][x]['id']
                        },
                  url   : "/checklike",
                  success : function(response2){
                    // console.log(response2)
                    window.xml = response2
                  }
                })
                   var base_url = window.location.origin;
                   var str = response['data'][x]['profile_image']
                   var myarr = str.split(":");
                   var myvar = myarr[0];
                   // console.log(myvar)
                   if (myvar=="https") {
                    var url = "<a href=''><img class='img-responsive img-rounded-full' src='" + response['data'][x]['profile_image'] + "'></a>"
                  }else{
                    var url = "<a href=''><img class='img-responsive img-rounded-full' src='/" + response['data'][x]['profile_image'] + "'></a>"
                  }
        if(window.xml == false){
          var t = response['data'][x]['created_at'].split(/[- :]/);
          var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
        
        $( '#printallcomments-'+id).append("<div class='tb-comment'>"+
                                            "<div class='hr-comment'></div>"+  
                                          "<div class='tb-pp'>"+
                                             url+
                                         "</div>"+
                                         "<div class='tb-text-comment'>"+
                                          "<div class='tb-info'>"+
                                              "<h3><a href='/" + response['data'][x]['username'] +"'>" + response['data'][x]['username'] +"</a></h3>"+
                                          "</div>"+
                                          "<div class='tb-time'>"+
                                              "<p>"+ getDateTimeSincegroup(d, false) +"</p>"+
                                          "</div>"+
                                          "<div class='tb-caption'>"+
                                              "<p>" + response['data'][x]['comment'] +"</p>"+
                                              "<div class='tb-like text-right'>"+
                                                  "<a id='like"+ response['data'][x]['id'] +"' style='cursor: pointer' onclick='likeCommentGroup("+ response['data'][x]['id'] +",1 , "+ response['data'][x]['id'] +");'>Like</a>"+
                                                  "<a class='replycommentgrup' id='replycommentgrup-" + response['data'][x]['id'] + "' onclick='replycommentgrup("+id+","+ response['data'][x]['id'] +")'>Reply</a>"+
                                              
                                              "</div>"+
                                              
                                          "</div>"+
                                          
                                          
                                      "</div>"+
                                      toggle+
                                              "<div style='display:none;' id='subKomen_1-" + response['data'][x]['id'] + "' class='warp-sub-comment sub-comment'></div>"+
                                              "<div name='textareagrup-"+ response['data'][x]['id'] +"' id='textareagrup-"+ response['data'][x]['id'] +"'></div>"+
                                          "</div>"+
                                      "</div>"  
                                      )
        var replycomment = '';
          if (response['replycomment'][x].length == 0){
            replycomment = " ";
          }else {
            var n =-1
            $.each(response['replycomment'][x], function( index, value ) {
            // $.map( response['replycomment'][x] , function( n ) {
              n = n+1
              var t = response['replycomment'][x][n]['created_at'].split(/[- :]/);
              var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
            $( '#subKomen_1-'+response['data'][x]['id']).append(
                                                      // "<div id='subKomen_1' class='warp-sub-comment sub-comment'>"+
                                                        "<div class='tb-comment'>"+
                                                            "<div class='hr-comment'></div>  "+
                                                          
                                                          "<div class='tb-pp'>"+
                                                              "<a href=''><img src='/" + response['replycomment'][x][n]['profile_image'] + "'></a>"+
                                                          "</div>"+
                                                          "<div class='tb-text-comment'>"+
                                                          "<div class='tb-info'>"+
                                                              "<h3><a href=''>" + response['replycomment'][x][n]['username'] + "</a></h3>"+
                                                          "</div>"+
                                                          "<div class='tb-time'>"+
                                                              "<p style='margin-bottom: 0; font-size: 11px; color: #ccc; line-height: 9px;'>"+ getDateTimeSincegroup(d, false) +"</p>"+
                                                          "</div>"+
                                                          "<div class='tb-caption'>"+
                                                             " <p>" + response['replycomment'][x][n]['contents'] + ""+
                                                                "<a href='#' class='more-txt'></a>"+
                                                              "</p>"+
                                                              
                                                              "<div class='tb-like text-right'>"+
                                                                  "<a href=''></a>"+
                                                                  "<a href=''></a>"+
                                                              "</div>"+
                                                          "</div>"+"</div>"+
                                                        "</div>")
                                                      })
          } 
      }else{

        var t = response['data'][x]['created_at'].split(/[- :]/);
          var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
        $( '#printallcomments-'+id).append("<div class='tb-comment'>"+
                                            "<div class='hr-comment'></div>"+  
                                          "<div class='tb-pp'>"+
                                             url+
                                         "</div>"+
                                         "<div class='tb-text-comment'>"+
                                          "<div class='tb-info'>"+
                                              "<h3><a href='/" + response['data'][x]['username'] +"'>" + response['data'][x]['username'] +"</a></h3>"+
                                          "</div>"+
                                          "<div class='tb-time'>"+
                                              "<p>"+ getDateTimeSincegroup(d, false) +"</p>"+
                                          "</div>"+
                                          "<div class='tb-caption'>"+
                                              "<p>" + response['data'][x]['comment'] + "</p>"+
                                              "<div class='tb-like text-right'>"+
                                                  "<a id='unlike"+ response['data'][x]['id'] +"' style='cursor: pointer' onclick='likeCommentGroup("+ response['data'][x]['id'] +",2 , "+ response['data'][x]['id'] +");'>unLike</a>"+
                                                  "<a class='replycommentgrup' id='replycommentgrup-" + response['data'][x]['id'] + "' onclick='replycommentgrup("+id+","+ response['data'][x]['id'] +")'>Reply</a>"+
                                              
                                              "</div>"+
                                              
                                          "</div>"+
                                          
                                      "</div>"+
                                      toggle+
                                              "<div style='display:none;' id='subKomen_1-" + response['data'][x]['id'] + "' class='warp-sub-comment sub-comment'></div>"+
                                              "<div name='textareagrup-"+ response['data'][x]['id'] +"' id='textareagrup-"+ response['data'][x]['id'] +"'></div>"+
                                          "</div>"+
                                      "</div>"  
                                      )

        var replycomment = '';
          if (response['replycomment'][x].length == 0){
            replycomment = " ";
          }else {
            var n =-1
            $.each(response['replycomment'][x], function( i, v ) {
            
              n=n+1
              var t = response['replycomment'][x][n]['created_at'].split(/[- :]/);
              var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
              // $( '#replycomment-'+response['data'][x]['id']).append("tot");
            $( '#subKomen_1-'+response['data'][x]['id']).append(
                                                      // "<div id='subKomen_1' class='warp-sub-comment sub-comment'>"+
                                                        "<div class='tb-comment'>"+
                                                            "<div class='hr-comment'></div>  "+
                                                          
                                                          "<div class='tb-pp'>"+
                                                              "<a href=''><img src='/" + response['replycomment'][x][n]['profile_image'] + "'></a>"+
                                                          "</div>"+
                                                          "<div class='tb-text-comment'>"+
                                                          "<div class='tb-info'>"+
                                                              "<h3><a href=''>" + response['replycomment'][x][n]['username'] + "</a></h3>"+
                                                          "</div>"+
                                                          "<div class='tb-time'>"+
                                                              "<p style='margin-bottom: 0; font-size: 11px; color: #ccc; line-height: 9px;'>"+ getDateTimeSincegroup(d, false) +"</p>"+
                                                          "</div>"+
                                                          "<div class='tb-caption'>"+
                                                             " <p>" + response['replycomment'][x][n]['contents'] + ""+
                                                                "<a href='#' class='more-txt'></a>"+
                                                              "</p>"+
                                                              
                                                              "<div class='tb-like text-right'>"+
                                                                  "<a href=''></a>"+
                                                                  "<a href=''></a>"+
                                                              "</div>"+
                                                          "</div>"+"</div>"+
                                                        "</div>")
                                                      })
                                                    }
      }
    //     if(comment[0]['liked'] == false){
    //        content += "<a id='like"+likeID+"' style='cursor: pointer' onclick='likeComment("+comment[0]['commentsID']+",1 , "+likeID+");'>Like</a>";
    //        }
    //      else{
    //      content += "<a id='unlike"+likeID+"' style='cursor: pointer' onclick='likeComment("+comment[0]['commentsID']+",2 , "+likeID+");'>unLike</a>";
    //    }
      })
      }
      }
    });
}

function toggleAndChangeReplyGroup(komen_id){
  
  $('#subKomen_1-'+komen_id).toggle();
     if ($('#subKomen_1-'+komen_id).css('display') == 'none') {
      $('#subKomen_1-'+komen_id).css('display') == 'block'
        $('#seeSubKomenGroup-'+komen_id).html('See all comment');
     }
     else {
        $('#seeSubKomenGroup-'+komen_id).html('Hide all comment');
     }
  // $('#subKomen-'+komen_id).removeAttr("style").show();
  // $('#seeSubKomen-'+komen_id).removeAttr("style").hide();
}

function getkomenreply(){
  console.log("tot");
}

function hideallcomments(id,jumlah){
    document.getElementById("printallcomments-"+id).style.display="none";
    // document.getElementById("readallcomments-"+id).style.display = "block";
    document.getElementById("hideallcomments-"+id).style.display = "none";
    //<a style='cursor:pointer' id="readallcomments-{{$key['id']}}" onclick="readallcomments({{$key['id']}})"><i class='fa fa-comment'></i>Read {{$data1['list'][$key['id']]}} Comments</a>
    $( "#readallcomments-"+id ).replaceWith("<a id='readallcomments-" + id + "' onclick='readallcommentshow(" + id + ")'><i class='fa fa-comment'></i>read "+jumlah+" comments</a>");
}

function readallcommentshow(id){
    document.getElementById("printallcomments-"+id).style.display="block";
    document.getElementById("hideallcomments-"+id).style.display = "block";
    document.getElementById("readallcomments-"+id).style.display="none";
    // $( "#readallcomments-"+id ).replaceWith("<a id='readallcomments-" + id + "' onclick='readallcommentshow(" + id + ")' class='more-txt'>read more comments</a>");
}

// function addevent(){
//   $('#createGroup').modal('show');
// }

function readallcommentsNew(data, data1){
      $(".totkomengrup-"+data).css("display", "block");
      $( "#readallcomments-"+data).replaceWith("<a style='cursor:pointer' id='hideallcomments-" + data + "' onclick='hideallcommentsNew(" + data + ","+data1+")'><i class='fa fa-comment'></i>hide all comments</a>");
}

function hideallcommentsNew(data, data1){
    $(".totkomengrup-"+data).css("display", "none");
    $( "#hideallcomments-"+data).replaceWith("<a style='cursor:pointer' id='readallcomments-" + data + "' onclick='readallcommentsNew(" + data + ","+data1+")'><i class='fa fa-comment'></i>Read "+data1+" comments</a>");
}


function likeCommentGroup(commentsID, likes, likeID){
  // console.log(likeID);
  if(likes == 1){
    var content = "<a id='unlike"+likeID+"' style='cursor: pointer' onclick='likeCommentGroup("+commentsID+",2 , "+likeID+");'>unLike</a>";
    $('#like'+likeID).replaceWith(content);
  }
  else{
    var content = "<a id='like"+likeID+"' style='cursor: pointer' onclick='likeCommentGroup("+commentsID+",1 , "+likeID+");'>Like</a>";
    $('#unlike'+likeID).replaceWith(content);
  }
  $.ajax({
    type:'post',
    data: {
            idComment:commentsID,
            likeTypes:likes
          },
    url:'/likeCommentGroup',
    success: function(responseText){
      console.log(responseText);
    }
  });
}

function likePostGroup(PostId, likes, likeID){
  // console.log(PostId);
  // console.log(likes);
  // console.log(likeID);
  if(likes == 1){
    var num = document.getElementById("num-stat-"+PostId).innerHTML
    var num2 = parseInt(num)+1
    // console.log(num2+1);
    document.getElementById("num-stat-"+PostId).innerHTML = num2
    var content = "<a id='unlikePostGroup-"+likeID+"' style='cursor: pointer' onclick='likePostGroup("+PostId+",2 , "+likeID+");'><i class='fa fa-heart like-single-list-song' style='color: red;'></i></a>";
    $('#likePostGroup-'+likeID).replaceWith(content);
    //<a id="likePostGroup-{{$key['id']}}" onclick="likePostGroup({{$key['id']}},1,{{$key['id']}})"><i class="fa fa-heart like-single-list-song"></i></a>
  }
  else{
    var num = document.getElementById("num-stat-"+PostId).innerHTML
    var num2 = parseInt(num)-1
    document.getElementById("num-stat-"+PostId).innerHTML = num2

    var content = "<a id='likePostGroup-"+likeID+"' style='cursor: pointer' onclick='likePostGroup("+PostId+",1 , "+likeID+");'><i class='fa fa-heart like-single-list-song' style='color: #e7e7e7;'></i></a>";
    $('#unlikePostGroup-'+likeID).replaceWith(content);
  }
  $.ajax({
    type:'post',
    data: {
            idPost:PostId,
            likeTypes:likes
          },
    url:'/likePostGroup',
    success: function(responseText){
      console.log(responseText);
    }
  });
}


function selectsong(pic, name, username, id){
  // console.log(id)
   $('.warp-song-selected').css('display', 'block');
      $('.warp-song-selected .gj-song-card.colab').css('margin-top', '25px');
      $('.warp-song-selected .gj-song-card.colab').css('background-color', '#f1f1f1');
      $('#postMusicBank').modal('hide');

                if (pic==1) {
                    var url = "<img src='/img/album/album-song_475x475.jpg' class='img-responsive'>"
                  }else{
                    var url = "<img src='/"+pic+"' class='img-responsive'>"
                  }
  // document.getElementById("toggleshow").style.display="none"
  // document.getElementById("printselectsong").style.display="block"
  $( '#printselectsong').append("<a class='btn-select-post-song hover-select'></a>"+
                                "<div class='sm-list-song'>"+
                                                    "<div class='gj-song-wrap'>"+
                                                        "<div class='gj-song-card colab' style='margin-top: 25px; background-color: rgb(241, 241, 241);'>"+
                                                    "<div class='gj-song-cover'>"+
                                                    url+
                                                        "</div>"+
                                                            "<div class='gj-song-desc'>"+
                                                            "<div class='gj-song-story' style='width: 100%;'>"+
                                                            "<h1 class='gj-song-title'><a href=''>"+name+"</a><i onclick='closeselectsong()' class='fa fa-close' style='float:right;'></i></h1>"+
                                                        "<h5 class='gj-song-username'><i class='fa fa-user'></i>"+ username+"</h5>"+
                                                        "</div>"+
                                                            "</div>"+
                                                                "</div>"+
                                                                "<hr>"+
                                                            "</div>"+"</div>"
    )
  document.getElementById("track_id").value = id;
  document.getElementById("dropdownMenuLink").style.display="none"
  document.getElementById("buttonsubmitpostgrup").removeAttribute("disabled")
}

function closeselectsong(){
  // console.log("tot")
  $('#printselectsong').empty();
  document.getElementById("track_id").removeAttribute("value");
  document.getElementById("dropdownMenuLink").style.display="block"
  document.getElementById("buttonsubmitpostgrup").setAttribute("disabled", "disabled")
}

function showcontent(){
  document.getElementById("toggleshow").style.display="block"
  document.getElementById("printselectsong").style.display="none"
}


function getGroupMemberFriendList(){
   var data = getdataGroupMember(2);
    // var link = getLinkGroupMember(2);
    $.ajax({
      type  : "POST",
      data : data,
      url   : "PrintSelectFriend",
      success : function(responseText){
        console.log(responseText);
        var count = responseText['count'];
        var data = responseText['data'];
        
        if(data.length === 0)
          return 0;
        
        var content = "";
        for (var i = 0; i < data.length; i++) {
          content += PrintSelectFriend(data[i], i,count);
        };
        
        $('#loadingGroupMemberFriendList').html("<select data-placeholder='Choose Your Friends...' class='chosen-select' multiple tabindex='4' style='display:block;'>"+content+"</select>");
        
        
        
        // setTakeTotalGroup(count, type);
        // setLinkGroup(type);
      }
    });
}

function loadEventGroupRightawal(){
  // console.log("totjong")
    var id= document.getElementById("session").value;
    $.ajax({
          type : 'POST',
          url  : '/loadeventright/'+id,
          data : {id : id, skip : 0},
          success :  function(response){
            console.log(response)
            if (response.length == 0) {
                  $('#loadingGroupEventawal').hide();
                  $('#printloadingGroupEventawal').append("<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 padten'>"+
                                                       "<div class='nf-item-wall'>"+
                                                       "<p>You haven't Joined any event</p>"+
                                                       "</div>"+
                                                       "</div>");
            }else{
                var x = -1
                $.each(response, function( index, value ) {
                  x=x+1
                  var profile_image = response[x]['picture_event'];
                  var name_profile_image = profile_image.substring(profile_image.lastIndexOf('/')+1); // nama profile image
                  var folder = 'img/photo-event/';  // folder penyimpanan profile image
                  var picture = folder+name_profile_image; // link profile image

                  if(profile_image == 0){
                    profile_image_url = base_url+'/img/album/track-01.jpg';
                      }else{
                        if(profile_image != picture){
                              profile_image_url = profile_image;
                            }else{
                               profile_image_url = base_url+'/'+profile_image;
                            }
                      } 
                   var t = response[x]['created_at'].split(/[- :]/);
                   var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);   
                  // console.log(base_url)
                  // console.log(response)
                  // console.log(x)
                  if (response[x]['type']==1) {
                     var text = ""+response[x]['username']+" had joined event <a href='/event/"+response[x]['event_id']+"'>"+response[x]['title']+"</a></p>"
                  }else if (response[x]['type']==2) {
                    var text = ""+response[x]['username']+" created event "+response[x]['title']+" in group <a href='/group/latestpost/"+response[x]['group_id_enc']+"'>"+response[x]['group_name']+"</a></p>"
                  }
                  $('#loadingGroupEventawal').hide(); 
                  $('#printloadingGroupEventawal').prepend("<div class='gj-song-wrap'>"+
                                                 "<span href='#' class='list-notif'>"+
                                                 "<span class='pull-left thumb-sm img-notif-pp'>"+
                                                 "<img src='"+ profile_image_url +"' class='img-circle img-responsive img-pp-notif'>"+
                                                 // "<div class='gj-song-play'><i class='fa fa-play-circle-o fa-2x'></i></div>"+
                                                 "</span>"+
                                                 "<span class='notif-body block m-b-none'>"+
                                                 text+
                                                 "<small class='text-muted'>"+ getDateTimeSince(d, false) +" ago</small>"+
                                                 "</span>"+
                                                 "</span>"+
                                                 "</div>");
                });
                $('#printloadingGroupEventawal').append("<div id='buttonload' class='col-xs-12'>"+
                                                 "<div class='warp-btn-loadmore-list'>"+
                                                 "<a onclick='loadMoreEventRightawal()' style='cursor:pointer;' class='btn-loadmore-list'>"+
                                                 "<i class='fa fa-plus-circle'></i>"+
                                                 // "<div class='gj-song-play'><i class='fa fa-play-circle-o fa-2x'></i></div>"+
                                                 "<span class='text-load-more-list'>LOAD MORE</span>"+
                                                 "</a> "+
                                                 
                                                 "</div>"+
                                                 "</div>");
            }
          }
            
        })
  }   


function loadMoreEventRightawal(){
  var id= document.getElementById("session").value;
  $("#loadingGroupEventawal").css("display", "block");
  var skip2 = $("#skipevent").val()
  var skip = parseInt(skip2) + 2
  // $('#loadingMyWallEvent').show();
  console.log(skip)
    $.ajax({
          type : 'POST',
          url  : '/loadeventright/'+id,
          data : {id : id, skip : skip},
          success :  function(response){
            $("#skipevent").val(skip)
            console.log(response)
            if (response.length == 0) {
                  $('#loadingGroupEventawal').hide();
                  $('#buttonload').hide();
                  $('#printloadingGroupEventawal').append("<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 padten'>"+
                                                       "<div class='nf-item-wall'>"+
                                                       "<p>You Don't have any event more</p>"+
                                                       "</div>"+
                                                       "</div>");
            }else{
                var x = -1
                $.each(response, function( index, value ) {
                  x=x+1
                  var profile_image = response[x]['picture_event'];
                  var name_profile_image = profile_image.substring(profile_image.lastIndexOf('/')+1); // nama profile image
                  var folder = 'img/photo-event/';  // folder penyimpanan profile image
                  var picture = folder+name_profile_image; // link profile image

                  if(profile_image == 0){
                    profile_image_url = base_url+'/img/album/track-01.jpg';
                      }else{
                        if(profile_image != picture){
                              profile_image_url = profile_image;
                            }else{
                               profile_image_url = base_url+'/'+profile_image;
                            }
                      } 
                   var t = response[x]['created_at'].split(/[- :]/);
                   var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);   
                  // console.log(base_url)
                  // console.log(response)
                  // console.log(x)
                  if (response[x]['type']==1) {
                     var text = ""+response[x]['username']+" had joined event <a href='/event/"+response[x]['event_id']+"'>"+response[x]['title']+"</a></p>"
                  }else if (response[x]['type']==2) {
                    var text = ""+response[x]['username']+" created event "+response[x]['title']+" in group <a href='/group/latestpost/"+response[x]['group_id_enc']+"'>"+response[x]['group_name']+"</a></p>"
                  }
                  $('#loadingGroupEventawal').hide(); 
                  $( "#loadingGroupEventawal" ).before( "<div class='gj-song-wrap'>"+
                                                 "<span href='#' class='list-notif'>"+
                                                 "<span class='pull-left thumb-sm img-notif-pp'>"+
                                                 "<img src='"+ profile_image_url +"' class='img-circle img-responsive img-pp-notif'>"+
                                                 // "<div class='gj-song-play'><i class='fa fa-play-circle-o fa-2x'></i></div>"+
                                                 "</span>"+
                                                 "<span class='notif-body block m-b-none'>"+
                                                 text+
                                                 "<small class='text-muted'>"+ getDateTimeSince(d, false) +" ago</small>"+
                                                 "</span>"+
                                                 "</span>"+
                                                 "</div>" );
                  // $('#printloadingMyWallEvent').append("<div class='gj-song-wrap'>"+
                  //                                "<span href='#' class='list-notif'>"+
                  //                                "<span class='pull-left thumb-sm img-notif-pp'>"+
                  //                                "<img src='"+ profile_image_url +"' class='img-circle img-responsive img-pp-notif'>"+
                  //                                // "<div class='gj-song-play'><i class='fa fa-play-circle-o fa-2x'></i></div>"+
                  //                                "</span>"+
                  //                                "<span class='notif-body block m-b-none'>"+
                  //                                text+
                  //                                "<small class='text-muted'>"+ getDateTimeSince(d, false) +" ago</small>"+
                  //                                "</span>"+
                  //                                "</span>"+
                  //                                "</div>");
                });
                
            }
          }
            
        })
}

//=====checkelement Latestpost=======//
function checkElementLatestpost(){
  var elementExists = new Array(

    // document.getElementById('loadingGroupMember1'),
    // document.getElementById('loadingGroupMember2'),
    // document.getElementById('loadingGroupMember3')
    document.getElementById('loadingGroupEvent') 

    
  );

  var stop = false; 
  for (var i = elementExists.length - 1; i >= 0; i--) {
      if(elementExists[i] == null)
        stop = true;
    };

    if(stop)
      return 0;
    return 1;
}

$(document).on('ready pjax:success', function() {
    var check = checkElementLatestpost();
    if(check == 0) return;

    loadEventGroupRight();
    
  
  });

function loadEventGroupRight(){
  // console.log("totjong")
    var id= document.getElementById("session").value;
    $.ajax({
          type : 'POST',
          url  : '/loadeventright/'+id,
          data : {id : id, skip : 0},
          success :  function(response){
            console.log(response)
            if (response.length == 0) {
                  $('#loadingGroupEvent').hide();
                  $('#printloadingGroupEvent').append("<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 padten'>"+
                                                       "<div class='nf-item-wall'>"+
                                                       "<p>You haven't Joined any event</p>"+
                                                       "</div>"+
                                                       "</div>");
            }else{
                var x = -1
                $.each(response, function( index, value ) {
                  x=x+1
                  var profile_image = response[x]['picture_event'];
                  var name_profile_image = profile_image.substring(profile_image.lastIndexOf('/')+1); // nama profile image
                  var folder = 'img/photo-event/';  // folder penyimpanan profile image
                  var picture = folder+name_profile_image; // link profile image

                  if(profile_image == 0){
                    profile_image_url = base_url+'/img/album/track-01.jpg';
                      }else{
                        if(profile_image != picture){
                              profile_image_url = profile_image;
                            }else{
                               profile_image_url = base_url+'/'+profile_image;
                            }
                      } 
                   var t = response[x]['created_at'].split(/[- :]/);
                   var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);   
                  // console.log(base_url)
                  // console.log(response)
                  // console.log(x)
                  if (response[x]['type']==1) {
                     var text = ""+response[x]['username']+" had joined event <a href='/event/"+response[x]['event_id']+"'>"+response[x]['title']+"</a></p>"
                  }else if (response[x]['type']==2) {
                    var text = ""+response[x]['username']+" created event "+response[x]['title']+" in group <a href='/group/latestpost/"+response[x]['group_id_enc']+"'>"+response[x]['group_name']+"</a></p>"
                  }
                  $('#loadingGroupEvent').hide(); 
                  $('#printloadingGroupEvent').prepend("<div class='gj-song-wrap'>"+
                                                 "<span href='#' class='list-notif'>"+
                                                 "<span class='pull-left thumb-sm img-notif-pp'>"+
                                                 "<img src='"+ profile_image_url +"' class='img-circle img-responsive img-pp-notif'>"+
                                                 // "<div class='gj-song-play'><i class='fa fa-play-circle-o fa-2x'></i></div>"+
                                                 "</span>"+
                                                 "<span class='notif-body block m-b-none'>"+
                                                 text+
                                                 "<small class='text-muted'>"+ getDateTimeSince(d, false) +" ago</small>"+
                                                 "</span>"+
                                                 "</span>"+
                                                 "</div>");
                });
                $('#printloadingGroupEvent').append("<div id='buttonload' class='col-xs-12'>"+
                                                 "<div class='warp-btn-loadmore-list'>"+
                                                 "<a onclick='loadMoreEventRight()' style='cursor:pointer;' class='btn-loadmore-list'>"+
                                                 "<i class='fa fa-plus-circle'></i>"+
                                                 // "<div class='gj-song-play'><i class='fa fa-play-circle-o fa-2x'></i></div>"+
                                                 "<span class='text-load-more-list'>LOAD MORE</span>"+
                                                 "</a> "+
                                                 
                                                 "</div>"+
                                                 "</div>");
            }
          }
            
        })
  }   


function loadMoreEventRight(){
  var id= document.getElementById("session").value;
  $("#loadingGroupEvent").css("display", "block");
  var skip2 = $("#skipevent").val()
  var skip = parseInt(skip2) + 2
  // $('#loadingMyWallEvent').show();
  console.log(skip)
    $.ajax({
          type : 'POST',
          url  : '/loadeventright/'+id,
          data : {id : id, skip : skip},
          success :  function(response){
            $("#skipevent").val(skip)
            console.log(response)
            if (response.length == 0) {
                  $('#loadingGroupEvent').hide();
                  $('#buttonload').hide();
                  $('#printloadingGroupEvent').append("<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 padten'>"+
                                                       "<div class='nf-item-wall'>"+
                                                       "<p>You Don't have any event more</p>"+
                                                       "</div>"+
                                                       "</div>");
            }else{
                var x = -1
                $.each(response, function( index, value ) {
                  x=x+1
                  var profile_image = response[x]['picture_event'];
                  var name_profile_image = profile_image.substring(profile_image.lastIndexOf('/')+1); // nama profile image
                  var folder = 'img/photo-event/';  // folder penyimpanan profile image
                  var picture = folder+name_profile_image; // link profile image

                  if(profile_image == 0){
                    profile_image_url = base_url+'/img/album/track-01.jpg';
                      }else{
                        if(profile_image != picture){
                              profile_image_url = profile_image;
                            }else{
                               profile_image_url = base_url+'/'+profile_image;
                            }
                      } 
                   var t = response[x]['created_at'].split(/[- :]/);
                   var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);   
                  // console.log(base_url)
                  // console.log(response)
                  // console.log(x)
                  if (response[x]['type']==1) {
                     var text = ""+response[x]['username']+" had joined event <a href='/event/"+response[x]['event_id']+"'>"+response[x]['title']+"</a></p>"
                  }else if (response[x]['type']==2) {
                    var text = ""+response[x]['username']+" created event "+response[x]['title']+" in group <a href='/group/latestpost/"+response[x]['group_id_enc']+"'>"+response[x]['group_name']+"</a></p>"
                  }
                  $('#loadingGroupEvent').hide(); 
                  $( "#loadingGroupEvent" ).before( "<div class='gj-song-wrap'>"+
                                                 "<span href='#' class='list-notif'>"+
                                                 "<span class='pull-left thumb-sm img-notif-pp'>"+
                                                 "<img src='"+ profile_image_url +"' class='img-circle img-responsive img-pp-notif'>"+
                                                 // "<div class='gj-song-play'><i class='fa fa-play-circle-o fa-2x'></i></div>"+
                                                 "</span>"+
                                                 "<span class='notif-body block m-b-none'>"+
                                                 text+
                                                 "<small class='text-muted'>"+ getDateTimeSince(d, false) +" ago</small>"+
                                                 "</span>"+
                                                 "</span>"+
                                                 "</div>" );
                  // $('#printloadingMyWallEvent').append("<div class='gj-song-wrap'>"+
                  //                                "<span href='#' class='list-notif'>"+
                  //                                "<span class='pull-left thumb-sm img-notif-pp'>"+
                  //                                "<img src='"+ profile_image_url +"' class='img-circle img-responsive img-pp-notif'>"+
                  //                                // "<div class='gj-song-play'><i class='fa fa-play-circle-o fa-2x'></i></div>"+
                  //                                "</span>"+
                  //                                "<span class='notif-body block m-b-none'>"+
                  //                                text+
                  //                                "<small class='text-muted'>"+ getDateTimeSince(d, false) +" ago</small>"+
                  //                                "</span>"+
                  //                                "</span>"+
                  //                                "</div>");
                });
                
            }
          }
            
        })
}

function groupUserConfirmGroup(i){
  var groupID = $('#hiddenGroupIDYourGroupPending'+i).text();
  var groupName = $('#hiddenGroupNameYourGroupPending'+i).text();
   
    $('#modalConfirmGroupMemberPending').modal({
          backdrop: 'static',
          keyboard: true,
          show : true });
    $('#data-groupname-pending-confirm').text(groupName);
    $('#data-groupname-pending-confirm-1').text(groupName);
    $('#data-groupid-pending-confirm').text(groupID);
    $('#data-username-pending-confirm').text(username);
}

function groupUserRejectInvite(i){
  var groupID = $('#hiddenGroupIDYourGroupPending'+i).text();
  var groupName = $('#hiddenGroupNameYourGroupPending'+i).text();
     $('#modalRejectGroupInvite').modal({
          backdrop: 'static',
          keyboard: true,
          show : true });
     $('#data-groupname-confirm-reject').text(groupName);
     $('#data-groupname-confirm-1-reject').text(groupName);
     $('#data-groupid-confirm-reject').text(groupID);
}

$(document).on('ready pjax:success', function(){
    $('body').on('click', '.process-not-interest-group-member-from-page-group', function(){
        var group_name = $('#data-groupname-confirm-reject').text();
        var group_id = $('#data-groupid-confirm-reject').text();

        var buttonYes = "<a href='#' class='btn-s-gj bgc-btn process-not-interest-group-member-from-page-group' id='button-cancel-invitation-member-from-page-group'>Yes</a>";
        var buttonYesProcess = "<a href='#' class='btn-s-gj bgc-btn' id='button-cancel-invitation-member-from-page-group'>Yes</a>";
        $('.loader-process').show();
        $('.close-button-not-interest-group-member-from-page-group').hide();
        $('#button-cancel-invitation-member-from-page-group').replaceWith(buttonYesProcess);

        var url = base_url+'/notInterestGroup';
        var invite_status = '1';
        var data = 'groupID=' + group_id + '&invite_status=' + invite_status;
        $.ajax({
            type : 'POST',
            url : url,
            data : data,
            success : function(){
                 $('#modalRejectGroupInvite').modal('hide');
                 $('.loader-process').hide();
                 $('.close-button-not-interest-group-member-from-page-group').show();
                 $('#button-cancel-invitation-member-from-page-group').replaceWith(buttonYes);

                  console.log('sukses confirm not Interest');
                  swal({ 
                    title: "Thank you!",
                     text: "success reject invite ",
                      type: "success",
                    });
                  var totalCountYourGroupPending = $('#totalCountYourGroupPending').text();
                  console.log(totalCountYourGroupPending);
                  var a = 1;
                  var totalCount = parseInt(totalCountYourGroupPending) - parseInt(a);
                  var replacetotalCountYourGroupPending = "<span id='totalCountYourGroupPending' style='display:none;'>"+totalCount+"</span>";
                  console.log(replacetotalCountYourGroupPending);
                  if(totalCountYourGroupPending > 1 || totalCountYourGroupPending > '1'){
                     $('#dataYourGroupPending'+group_id).remove();
                  }else if(totalCountYourGroupPending === 1 || totalCountYourGroupPending === '1'){
                     var contentDataNull = "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>"+
                                       "<div class='nf-item-wall-track'>"+
                                       "<p>There is no Group</p>"+
                                       "</div>"+
                                       "</div>"; 
                     $('#dataYourGroupPending'+group_id).replaceWith(contentDataNull);
                  }
                  $('#totalCountYourGroupPending').replaceWith(replacetotalCountYourGroupPending);
                  
            },
            error : function(){
                 $('#modalRejectGroupInvite').modal('hide');
                 $('.loader-process').hide();
                 $('.close-button-not-interest-group-member-from-page-group').show();
                 $('#button-cancel-invitation-member-from-page-group').replaceWith(buttonYes);
                 swal({ 
                    title: "error!",
                     text: "please try again later... ",
                      type: "warning",
                    });
            }
        });
    });
});

function groupUserCancelRequest(i){
  var groupID = $('#hiddenGroupIDYourGroupRequest'+i).text();
  var groupName = $('#hiddenGroupNameYourGroupRequest'+i).text();
console.log(groupID);
console.log(groupName);
   $('#modalCancelRequest').modal({
          backdrop: 'static',
          keyboard: true,
          show : true });
   $('#data-groupname-cancel').text(groupName);
   $('#data-groupid-cancel').text(groupID);  
}

$(document).on('ready pjax:success', function() {
    confirmGroupMember();
    // joinRequestGroupMember();
    AdminProsesConfirmRequest();
    AdminProsesCancelYourRequest();
    cancelRequestSendMember();
   
 }); 

$(document).on('ready pjax:success', function() {
  $('.crt-group').click(function(){
      $('#createGroup').modal('show');
      $('body').addClass("tot");
  });
});

$(document).on('ready pjax:success', function() {
  $(document).on('click', '.invite-friend', function(){
    $('#inviteFriend').modal('show');
    $('body').addClass("tot");
  });
});


//================== ADMIN LEAVE GROUP ADA MEMBER LAIN ==================//
$(document).on('ready pjax:success', function(){
    $('body').on('click', '#adminLeaveGroupAdaMemberLain', function(){
        var groupID = $(this).attr('groupID');
        var groupName = $(this).attr('groupName');
        var picture = $(this).attr('picture');
        var bg_picture = $(this).attr('bg_picture');

        $('#modalAdminLeaveGroupAdaMemberLain').modal({
             backdrop: 'static',
             keyboard: true, 
             show: true
          });
        $('#data-groupname-adminLeave').text(groupName);
        $('#data-groupid-adminLeave').text(groupID);
        $('#data-picture-adminLeave').text(picture);
        $('#data-bg-picture-adminLeave').text(bg_picture);
    });

    $('body').on('click', '.process-admin-leave-group-select-newadmin',function(){
        var groupID = $('#data-groupid-adminLeave').text();
        // http://stackoverflow.com/questions/1085801/get-selected-value-in-dropdown-list-using-javascript
        var e = document.getElementById("new_admin_member_id");
        var memberID = e.options[e.selectedIndex].value;

        if(memberID != ''){
           var data = 'groupID=' +groupID + '&memberID=' +memberID;
           var buttonYesProcess = "<a href='#' class='btn-s-gj bgc-btn' id='admin-leave-select-newAdmin'>YES</a>";
           var buttonYes = "<a href='#' class='btn-s-gj bgc-btn process-admin-leave-group-select-newadmin' id='admin-leave-select-newAdmin'>YES</a>";
           $('.loader-process').show();
           $('.close-button').hide();
           $('#admin-leave-select-newAdmin').replaceWith(buttonYesProcess);
           $.ajax({
               type : 'POST',
               url : window.location.origin+'/adminLeaveGroup',
               data : data,
               success : function(responseText){
                $('#modalAdminLeaveGroupAdaMemberLain').modal('hide');
                $('.loader-process').hide();
                $('.close-button').show();
                $('#admin-leave-select-newAdmin').replaceWith(buttonYes);

                swal({ 
                title: "Thank you!",
                 text: "You are success leave group",
                  type: "success",
                  allowOutsideClick: false,
                });

                $('body').on('click','.swal2-confirm',function(){
                     window.location.reload();
                });  

               },
               error : function(){
                $('#modalAdminLeaveGroupAdaMemberLain').modal('hide');
                $('.loader-process').hide();
                $('.close-button').show();
                $('#admin-leave-select-newAdmin').replaceWith(buttonYes);
                 swal({
                    title: 'Error... please, try again later',
                    text: 'I will close in 2 seconds.',
                    timer: 2000
                  });
               }
           });
        }else{
           swal({
                title: 'please, select new admin',
                text: 'I will close in 2 seconds.',
                timer: 2000
              });
        }
      
    });
});

//==================== MEMBER LEAVE GROUP =========================//
$(document).on('ready pjax:success', function(){
    $('body').on('click', '#memberleaveGroup', function(){
        var groupID = $(this).attr('groupID');
        var groupName = $(this).attr('groupName');
        var picture = $(this).attr('picture');
        var bg_picture = $(this).attr('bg_picture');
        var username = $(this).attr('username');

        $('#modalMemberLeaveGroup').modal({
             backdrop: 'static',
             keyboard: true, 
             show: true
          });
        $('#data-groupname-memberLeave').text(groupName);
        $('#data-groupid-memberLeave').text(groupID);
        $('#data-picture-memberLeave').text(picture);
        $('#data-bg-picture-memberLeave').text(bg_picture);
        $('#data-username-memberLeave').text(username);
    });

    $('body').on('click', '.process-member-leave-group', function(){
      var groupID = $('#data-groupid-memberLeave').text();
      var groupName = $('#data-groupname-memberLeave').text();
      var username = $('#data-username-memberLeave').text();

      var buttonYesProcess = "<a href='#' class='btn-s-gj bgc-btn' id='member-leaveGroup'>YES</a>";
      var buttonYes = "<a href='#' class='btn-s-gj bgc-btn process-member-leave-group' id='member-leaveGroup'>YES</a>";
      $('.loader-process').show();
      $('.close-button').hide();
      $('#member-leaveGroup').replaceWith(buttonYesProcess);
      var data = 'groupID=' +groupID;

      $.ajax({
           type : 'POST',
           data : data,
           url : window.location.origin+'/memberLeaveGroup',
           success : function(responseText){
              $('#modalMemberLeaveGroup').modal('hide');
              $('.loader-process').hide();
              $('.close-button').show();
              $('#member-leaveGroup').replaceWith(buttonYes);
              
               swal({ 
                title: "Thank you!",
                 text: "You are success leave group",
                  type: "success",
                });

                 var content = "<button type='button' id='UserJoinRequestGroupMember'  groupName ='"+groupName+"' groupID='"+groupID+"' username='"+username+"' class='btn btn-add-f'><i class='fa fa-user-plus'></i>Join Group</button>";
                 $('#btn-joined-group').replaceWith(content);
                 $('#dataMemberGroup'+username).remove();
                 $('#dataYourGroup'+groupID).remove();
                 $('#dataYourGroupPending'+groupID).remove();
           },
           error : function(){
              $('#modalAdminLeaveGroupAdaMemberLain').modal('hide');
              $('.loader-process').hide();
              $('.close-button').show();
              $('#admin-leave-select-newAdmin').replaceWith(buttonYes);
              swal({
                  title: 'Error... please, try again later',
                  text: 'I will close in 2 seconds.',
                  timer: 2000
              });
           }
      });
        
    });
});

// =============== ADMIN LEAVE GROUP TIDAK ADA MEMBER LAIN ====================//
$(document).on('ready pjax:success', function(){
   $('body').on('click', '#adminLeaveGroupTidakAdaMemberLain', function(){
        var groupID = $(this).attr('groupID');
        var groupName = $(this).attr('groupName');
        var picture = $(this).attr('picture');
        var bg_picture = $(this).attr('bg_picture');

        $('#modalAdminLeaveGroupTidakAdaMemberLain').modal({
             backdrop: 'static',
             keyboard: true, 
             show: true
          });
        $('#data-groupname-adminLeave2').text(groupName);
        $('#data-groupid-adminLeave2').text(groupID);
        $('#data-picture-adminLeave2').text(picture);
        $('#data-bg-picture-adminLeave2').text(bg_picture);
   });

   $('body').on('click', '.process-admin-leave-group-memberNull', function(){
       var groupID =  $('#data-groupid-adminLeave2').text();
       var picture = $('#data-picture-adminLeave2').text();
       var bg_picture = $('#data-bg-picture-adminLeave2').text();
       var buttonYesProcess = "<a href='#' class='btn-s-gj bgc-btn' id='admin-leave-group-memberNull'>YES</a>";
       var buttonYes = "<a href='#' class='btn-s-gj bgc-btn process-admin-leave-group-memberNull' id='admin-leave-group-memberNull'>YES</a>";
       $('.loader-process').show();
       $('.close-button').hide();
       $('#admin-leave-group-memberNull').replaceWith(buttonYesProcess);
       var data = 'groupID=' +groupID + '&picture=' +picture + '&bg_picture=' +bg_picture;
       
       $.ajax({
            type : 'POST',
            data : data,
            url : window.location.origin+'/adminLeaveGroupTanpaMember',
            success : function(responseText){
              $('#modalAdminLeaveGroupTidakAdaMemberLain').modal('hide');
              $('.loader-process').hide();
              $('.close-button').show();
              $('#admin-leave-group-memberNull').replaceWith(buttonYes);
              console.log('sukses');
              swal({ 
                title: "Thank you!",
                 text: "You are success leave group",
                  type: "success",
                  allowOutsideClick: false,
                });

                $('body').on('click','.swal2-confirm',function(){
                     window.location.href = window.location.origin+'/group';
                });  
            },
            error : function(){
              $('#modalAdminLeaveGroupTidakAdaMemberLain').modal('hide');
              $('.loader-process').hide();
              $('.close-button').show();
              $('#admin-leave-group-memberNull').replaceWith(buttonYes);
              swal({
                  title: 'Error... please, try again later',
                  text: 'I will close in 2 seconds.',
                  timer: 2000
              });
            }

       });
   });
});

//======================= LEAVE GROUP FROM GROUP PAGE =========================//
function adminLeaveGroupOtherMemberFound(i){
  var groupID = $('#hiddenGroupIDYourGroup'+i).text();
  var groupName = $('#hiddenGroupNameYourGroup'+i).text();
  var picture = $('#hiddenGroupPictureYourGroup'+i).text();
  var bg_picture = $('#hiddenGroupBgPictureYourGroup'+i).text();

  var data = 'groupID=' +groupID;
  $.ajax({
       type : 'POST',
       data : data,
       url : window.location.origin+'/dataSelectNewAdmin',
       success : function(responseText){
         console.log(responseText);
         var data = responseText['data'];
   
         var sel = document.getElementById('new_admin_member_id');
          $("#new_admin_member_id").append("<option value=''>Select New Admin</option>");
         for(var i= 0; i < data.length; i++){
          var opt = document.createElement('option');
          opt.innerHTML = data[i]['username'];
          opt.value = data[i]['memberID'];
          $("#new_admin_member_id").append(opt);
          $('#new_admin_member_id').trigger("chosen:updated");
         }
          
       },
       error : function(){
         console.log('error');
       }
  });

   $('#modalAdminLeaveGroupAdaMemberLain').modal({
         backdrop: 'static',
         keyboard: true, 
         show: true
      });
    $('#data-groupname-adminLeave').text(groupName);
    $('#data-groupid-adminLeave').text(groupID);
    $('#data-picture-adminLeave').text(picture);
    $('#data-bg-picture-adminLeave').text(bg_picture);
}

function adminLeaveGroupOtherMemberNull(i){
  var groupID = $('#hiddenGroupIDYourGroup'+i).text();
  var groupName = $('#hiddenGroupNameYourGroup'+i).text();
  var picture = $('#hiddenGroupPictureYourGroup'+i).text();
  var bg_picture = $('#hiddenGroupBgPictureYourGroup'+i).text();

   $('#modalAdminLeaveGroupTidakAdaMemberLain').modal({
         backdrop: 'static',
         keyboard: true, 
         show: true
      });
   $('#data-groupname-adminLeave2').text(groupName);
   $('#data-groupid-adminLeave2').text(groupID);
   $('#data-picture-adminLeave2').text(picture);
   $('#data-bg-picture-adminLeave2').text(bg_picture);
}

function memberGroupLeaveGroup(i){
   var groupID = $('#hiddenGroupIDYourGroup'+i).text();
   var groupName = $('#hiddenGroupNameYourGroup'+i).text();
   var picture = $('#hiddenGroupPictureYourGroup'+i).text();
   var bg_picture = $('#hiddenGroupBgPictureYourGroup'+i).text();

    $('#modalMemberLeaveGroup').modal({
         backdrop: 'static',
         keyboard: true, 
         show: true
      });
    $('#data-groupname-memberLeave').text(groupName);
    $('#data-groupid-memberLeave').text(groupID);
    $('#data-picture-memberLeave').text(picture);
    $('#data-bg-picture-memberLeave').text(bg_picture);
}

//======================== CONFIRM GROUP MEMBER =======================//
function confirmGroupMember(){
   $('body').on('click','#confirmGroupMember',function(){
      var groupID = $(this).attr('groupID');
      var groupName = $(this).attr('groupName');
      var username = $(this).attr('username');
      console.log(groupName);
      console.log(groupID);

    $('#modalConfirmGroupMember').modal({
          backdrop: 'static',
          keyboard: true,
          show : true });
    $('#data-groupname-confirm').text(groupName);
    $('#data-groupname-confirm-1').text(groupName);
    $('#data-groupid-confirm').text(groupID);
    $('#data-username-confirm').text(username);
    
     });
}

//================== PROSES CONFIRM GROUP =====================//
$(document).on('ready pjax:success', function(){
  $('.process-confirm-group-member').click(function(){

      var id_group = $('#data-groupid-confirm').text();
      var group_name = $('#data-groupname-confirm').text();
      var username = $('#data-username-confirm').text();

      var url = base_url+'/confirmGroup';
      var data = 'groupID=' + id_group;
      var buttonAccept = "<a href='#' class='btn-s-gj bgc-btn process-confirm-group-member' id='button-confirm-group-member'>Accept</a>";
      var buttonAcceptProcess = "<a href='#' class='btn-s-gj bgc-btn' id='button-confirm-group-member'>Accept</a>";
      $('.loader-process').show();
      $('.close').hide();
      $('#button-reject-group-member').hide();
      $('#button-confirm-group-member').replaceWith(buttonAcceptProcess);

        $.ajax({
          url: url,
          type: 'POST',
          data: data,
          success: function(){
              $('#modalConfirmGroupMember').modal('hide');
              $('.loader-process').hide();
              $('.close').show();
              $('#button-reject-group-member').show();
              $('#button-confirm-group-member').replaceWith(buttonAccept);
              console.log('sukses confirm');
              swal({ 
                title: "Thank you!",
                 text: "success Confirm ",
                  type: "success",
                });

               var content = "<span class='warp-btn-f-uf' id='btn-joined-group'>"+
                             "<i class='fa fa-check'></i>"+
                             "<input type='button' class='btn btn-u-uf' id='memberleaveGroup' groupName ='"+group_name+"' groupID='"+id_group+"' username='"+username+"' value='Joined' data-value-hover='Leave Group'>"+
                             "</span>";
                 $('#confirmGroupMember').replaceWith(content);
                 var content2 = "<button class='btn btn-line-stat col-btn-ex'><i class='fa fa-user-plus'></i>Request Send</button>";
                 $('#joinRequestGroupMember').replaceWith(content2);

                 $('#buttonPendingConfirm'+username).remove();
                 $('#buttonPendingReject'+username).remove();
                 $('#dataPending'+username).prependTo('#clearfix-groupmember3');
                 var totalCountMember = $('#totalCountMember').text();
                 var totalCountPending = $('#totalCountPending').text();

                 var a = 1;
                 var memberSplit = totalCountMember.split(" ");
                 var member = memberSplit[0];
                 var totalMemberNow = parseInt(member) + parseInt(a);

                 var pendingSplit = totalCountPending.split(" ");
                 var pending = pendingSplit[0];
                 var totalPendingNow = parseInt(pending) - parseInt(a);

                 var contentCountMember = "<span class='pull-right sum-member' id='totalCountMember'>"+totalMemberNow+" Members</span>";
                 var contentCountPending = "<span class='pull-right sum-member' id='totalCountPending'>"+totalPendingNow+" Members</span>";
                 $('#totalCountMember').replaceWith(contentCountMember);
                 $('#totalCountPending').replaceWith(contentCountPending);
                 var contentData = ""; 
                      contentData += "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>";
                      contentData += "<div class='nf-item-wall-track'>";
                      contentData += "<p>No new member invitation</p>";
                      contentData += "</div>";
                      contentData += "</div>"; 
                 if(pending === '1' || pending === 1){
                    $('#groupmember5').replaceWith(contentData);
                 }  
          },
          error: function(){
             $('#modalConfirmGroupMember').modal('hide');
             $('.loader-process').hide();
             $('.close').show();
             $('#button-reject-group-member').show();
             $('#button-confirm-group-member').replaceWith(buttonAccept);
              console.log('error');
              swal({ 
                title: "error!",
                 text: "please try again later... ",
                  type: "warning",
                });
          }

        });
   });
});



//===================== PROSES REJECT GROUP INVITATION FROM USER =================//
$(document).on('ready pjax:success', function(){
  $('.process-not-interest-group-member').click(function(){
      var id_group = $('#data-groupid-confirm').text();
      var group_name = $('#data-groupname-confirm').text();
      var username = $('#data-username-confirm').text();
  
      var url = base_url+'/notInterestGroup';
      var invite_status = '1';
      var data = 'groupID=' + id_group + '&invite_status=' + invite_status;

      var buttonReject = "<a href='#' class='btn-s-gj bgc-btn process-not-interest-group-member' id='button-reject-group-member' style='margin-right: 5px; background-color: #B22222;'>Reject</a>";
      var buttonRejectProcess = "<a href='#' class='btn-s-gj bgc-btn' id='button-reject-group-member' style='margin-right: 5px; background-color: #B22222;'>Reject</a>";
      $('.loader-process').show();
      $('.close').hide();
      $('#button-reject-group-member').replaceWith(buttonRejectProcess);
      $('#button-confirm-group-member').hide();

        $.ajax({
          url: url,
          type: 'POST',
          data: data,
          success: function(){
             $('#modalConfirmGroupMember').modal('hide');
             $('.loader-process').hide();
             $('.close').show();
             $('#button-reject-group-member').replaceWith(buttonReject);
             $('#button-confirm-group-member').show();

              console.log('sukses confirm not Interest');
              swal({ 
                title: "Thank you!",
                 text: "success reject invite ",
                  type: "success",
                  allowOutsideClick: false,
                });
              $('.swal2-confirm').click(function(){
                var content = "<button type='button' id='UserJoinRequestGroupMember'  groupName ='"+group_name+"' groupID='"+id_group+"' username='"+username+"' class='btn btn-add-f'><i class='fa fa-user-plus'></i>Join Group</button>";
                $('#confirmGroupMember').replaceWith(content);

                 var totalCountPending = $('#totalCountPending').text();
                 var a = 1;
                 var pendingSplit = totalCountPending.split(" ");
                 var pending = pendingSplit[0];
                 var totalPendingNow = parseInt(pending) - parseInt(a);
                 var contentCountPending = "<span class='pull-right sum-member' id='totalCountPending'>"+totalPendingNow+" Members</span>";
                 $('#totalCountPending').replaceWith(contentCountPending);
                 var contentData = ""; 
                      contentData += "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>";
                      contentData += "<div class='nf-item-wall-track'>";
                      contentData += "<p>No new pending invitation</p>";
                      contentData += "</div>";
                      contentData += "</div>"; 
                 if(pending > '1' || pending > 1){
                    $('#dataPending'+username).remove();
                 }      
                 else if(pending === '1' || pending === 1){
                    $('#dataPending'+username).replaceWith(contentData);
                 }     
              });                             
          },
          error: function(){
             $('#modalConfirmGroupMember').modal('hide');
             $('.loader-process').hide();
             $('.close').show();
             $('#button-reject-group-member').replaceWith(buttonReject);
             $('#button-confirm-group-member').show();
              console.log('error');
               swal({ 
                title: "error!",
                 text: "please try again later... ",
                  type: "warning",
                });
          }

        });
   });
});

//====================== PROSES CANCEL REQUEST FROM USER ======================//
$(document).on('ready pjax:success', function(){
  $('body').on('click', '.process-not-interest-group-from-user', function(){

        var id_group = $('#data-groupid-cancel').text();
        console.log(id_group);
        var invite_status = '2';
  
        var url = base_url+'/notInterestGroup';
        var data = 'groupID=' + id_group + '&invite_status=' + invite_status;

        var buttonYes = "<a href='#' class='btn-s-gj bgc-btn process-not-interest-group-from-user' id='button-not-interest-group-from-user'>Yes</a>";
        var buttonYesProcess = "<a href='#' class='btn-s-gj bgc-btn' id='button-not-interest-group-from-user'>Yes</a>";
        $('.loader-process').show();
        $('.close-button-not-interest-group-from-user').hide();
        $('#button-not-interest-group-from-user').replaceWith(buttonYesProcess);
  
        $.ajax({
          url: url,
          type: 'POST',
          data: data,
          success: function(){
              $('#modalCancelRequest').modal('hide');
              $('.loader-process').hide();
              $('.close-button-not-interest-group-from-user').show();
              $('#button-not-interest-group-from-user').replaceWith(buttonYes);

              console.log('sukses cancel request');
               swal({ 
                title: "Thank you!",
                 text: "success reject request",
                  type: "success",
                });
             var totalCountYourGroupRequest = $('#totalCountYourGroupRequest').text();
             console.log(totalCountYourGroupRequest);
             var a = 1;
             var totalCountRequest = parseInt(totalCountYourGroupRequest) - parseInt(a);
             var replaceTotalCountYourGroupRequest = "<span id='totalCountYourGroupRequest' style='display:none;'>"+totalCountRequest+"</span>"; 
             if(totalCountYourGroupRequest > 1 || totalCountYourGroupRequest > '1'){
                $('#dataYourGroupRequest'+id_group).remove();
             }else if(totalCountYourGroupRequest === 1 || totalCountYourGroupRequest === '1'){
                 var contentDataNull = "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>"+
                                       "<div class='nf-item-wall-track'>"+
                                       "<p>There is no Group</p>"+
                                       "</div>"+
                                       "</div>"; 
                $('#dataYourGroupRequest'+id_group).replaceWith(contentDataNull);
             }

             $('#totalCountYourGroupRequest').replaceWith(replaceTotalCountYourGroupRequest);
             
          },

          error: function(){
             $('#modalCancelRequest').modal('hide');
             $('.loader-process').hide();
             $('.close-button-not-interest-group-from-user').show();
             $('#button-not-interest-group-from-user').replaceWith(buttonYes);
              console.log('error');
               swal({ 
                title: "error!",
                 text: "please try again later... ",
                  type: "warning",
                });
          }

        });
   });
});

//=================== JOIN REQUEST FROM SIDE BAR ===============//
$(document).on('ready pjax:success', function(){
  $('body').on('click', '#joinRequestGroupMember',function(){
      var groupID = $(this).attr('groupID');
      var groupName = $(this).attr('groupName');
      
      $('#modalJoinRequstGroup').modal({
          backdrop: 'static',
          keyboard: true,
          show : true });
      $('#group_name-join-request-group').text(groupName);
      $('#group_id-join-request-group').text(groupID);
     
     });
 });  

//=================  JOIN REQUEST ===================//
 $(document).on('ready pjax:success', function(){ 
   $('body').on('click', '#UserJoinRequestGroupMember', function(){
      var groupID = $(this).attr('groupID');
      var groupName = $(this).attr('groupName');
      var username = $(this).attr('username');

      $('#modalJoinRequstGroup').modal({
          backdrop: 'static',
          keyboard: true,
          show : true });
      $('#group_name-join-request-group').text(groupName);
      $('#group_id-join-request-group').text(groupID);
      $('#group_username-request-group').text(username);
  
     });

     $('body').on('click', '.process-join-request-group', function(){
          var groupID = $('#group_id-join-request-group').text();
          var groupName = $('#group_name-join-request-group').text();
          var username = $('#group_username-request-group').text();

          var buttonYes = "<a href='#' class='btn-s-gj bgc-btn process-join-request-group' id='button-join-request-group'>Yes</a>";
          var buttonYesProcess = "<a href='#' class='btn-s-gj bgc-btn' id='button-join-request-group'>Yes</a>";
          
          $('#button-join-request-group').replaceWith(buttonYesProcess);
          $('.close-button-join-request-group').hide();
          $('.loader-process').show();

         var url = base_url+'/joinRequestGroup';
         var data = 'groupID=' + groupID;
        $.ajax({
            type : 'POST',
            data : data,
            url : url,
            success : function(responseText,status){
            console.log(status);
            console.log(responseText);
              $('#modalJoinRequstGroup').modal('hide');
              $('#button-join-request-group').replaceWith(buttonYes);
              $('.close-button-join-request-group').show();
              $('.loader-process').hide();
               swal({ 
                title: "Thank you!",
                 text: "success request sent ",
                  type: "success",
                  allowOutsideClick: false,
                });
              $('.swal2-confirm').click(function(){
                var content = "<button type='button' id='cancelRequestSendMember' groupName='"+groupName+"' groupID='"+groupID+"' memberID='"+responseText['userID']+"' username='"+username+"' class='btn btn-add-f'><i class='fa fa-user-plus'></i>Request Sent</button>";
                $('#UserJoinRequestGroupMember').replaceWith(content);

                var content2 = "<button class='btn btn-line-stat col-btn-ex'><i class='fa fa-user-plus'></i>Request Send</button>";
                $('#joinRequestGroupMember').replaceWith(content2);
              });
          },
          error : function(status){
            console.log(status);
             $('#modalJoinRequstGroup').modal('hide');
             $('#button-join-request-group').replaceWith(buttonYes);
             $('.close-button-join-request-group').show();
             $('.loader-process').hide();
             swal({ 
                title: "error!",
                 text: "please try again later... ",
                  type: "warning",
                });
          }
        });

     });
 });

$(document).on('ready pjax:success', function(){
     
});

//=============== CANCEL REQUEST SEND ====================//
function cancelRequestSendMember(){
  $('body').on('click','#cancelRequestSendMember', function(){
    var groupID = $(this).attr('groupID');
    var groupName = $(this).attr('groupName');
    var memberID = $(this).attr('memberID');
    var myUsername = $(this).attr('username');

    $('#userCancelJoinGroup').modal({
          backdrop: 'static',
          keyboard: true,
          show : true });
    $('#data-groupname-request-cancel').text(groupName);
    $('#data-groupid-request-cancel').text(groupID);
    $('#data-memberid-request-cancel').text(memberID);
    $('#data-myusername-request-cancel').text(myUsername);

  });
}

$(document).on('ready pjax:success', function(){
  $('body').on('click', '.process-cancel-request-join',function(){
       var group_id = $('#data-groupid-request-cancel').text();
       var group_name = $('#data-groupname-request-cancel').text();
       var myusername = $('#data-myusername-request-cancel').text();
   
       var url = base_url+'/notInterestGroup';
       var invite_status = '2';
       var data = 'groupID=' + group_id + '&invite_status=' + invite_status;

       var buttonYes = "<a href='#' class='btn-s-gj bgc-btn process-cancel-request-join' id='button-cancel-request-group'>Yes</a>";
       var buttonYesProcess = "<a href='#' class='btn-s-gj bgc-btn' id='button-cancel-request-group'>Yes</a>";
          
       $('#button-cancel-request-group').replaceWith(buttonYesProcess);
       $('.close-button-cancel-request-group').hide();
       $('.loader-process').show();
  
        $.ajax({
          url: url,
          type: 'POST',
          data: data,
          success: function(){
              $('#userCancelJoinGroup').modal('hide');
              $('#button-cancel-request-group').replaceWith(buttonYes);
              $('.close-button-cancel-request-group').show();
              $('.loader-process').hide();

              console.log('sukses cancel request');
               swal({ 
                title: "Thank you!",
                 text: "success cancel your request ",
                  type: "success",
                  allowOutsideClick: false,
                });
              $('.swal2-confirm').click(function(){
                 var content = "<button type='button' id='UserJoinRequestGroupMember'  groupName ='"+group_name+"' groupID='"+group_id+"' username='"+myusername+"' class='btn btn-add-f'><i class='fa fa-user-plus'></i>Join Group</button>";
                 $('#cancelRequestSendMember').replaceWith(content);
                 var a = 1;
                 var total = $('#totalCountRequest').text();
                 console.log(total);
                 var totalNow = parseInt(total) - parseInt(a);
                 var totalCountRequest = " <span class='pull-right sum-member' id='totalCountRequest'>"+totalNow+" Members</span>";
                 $('#totalCountRequest').replaceWith(totalCountRequest);
                 var jumlah = total.split(" ");
                 var jumlahTotal = jumlah[0];
                 console.log(jumlahTotal);
                  if(jumlahTotal > 1 || jumlahTotal > '1'){
                     $('#dataMemberRequest'+myusername).remove();
                  }else if(jumlahTotal === '1' || jumlahTotal === 1){
                      var contentData = ""; 
                      contentData += "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>";
                      contentData += "<div class='nf-item-wall-track'>";
                      contentData += "<p>No new member requests</p>";
                      contentData += "</div>";
                      contentData += "</div>"; 
                     $('#dataMemberRequest'+myusername).replaceWith(contentData);
                  }
              });
          },
          error: function(){
            $('#userCancelJoinGroup').modal('hide');
              console.log('error');
              $('#userCancelJoinGroup').modal('hide');
              $('#button-cancel-request-group').replaceWith(buttonYes);
              $('.close-button-cancel-request-group').show();
              $('.loader-process').hide();
              swal({ 
                title: "error!",
                 text: "please try again later... ",
                  type: "warning",
                });
          }

        });
    });
});
//============== CONFIRM FROM PAGE MEMBER ===============//
function memberGroupPendingConfirm(i){
     console.log('confirm test');
      var groupID = $('#hiddenGroupIDPending'+i).text();
      var groupName = $('#hiddenGroupNamePending'+i).text();
      var username = $('#hiddenMemberUsernamePending'+i).text();
      var userID = $('#hiddenMemberIDPending'+i).text();

      console.log(groupID);
      
    $('#modalConfirmGroupMemberPending').modal({
          backdrop: 'static',
          keyboard: true,
          show : true });
    $('#data-groupname-pending-confirm').text(groupName);
    $('#data-groupname-pending-confirm-1').text(groupName);
    $('#data-groupid-pending-confirm').text(groupID);
    $('#data-username-pending-confirm').text(username);
    $('#data-userid-pending-confirm').text(userID);
}

$(document).on('ready pjax:success', function(){
  $('body').on('click', '.process-confirm-group-member-pending', function(){
   
      var id_group = $('#data-groupid-pending-confirm').text();
      var group_name = $('#data-groupname-pending-confirm').text();
      var username = $('#data-username-pending-confirm').text();
      var user_id = $('#data-userid-pending-confirm').text();
  
      var url = base_url+'/confirmGroup';
      var data = 'groupID=' + id_group;

       var buttonYes = "<a href='#' class='btn-s-gj bgc-btn process-confirm-group-member-pending' id='button-confirm-group-member-pending'>Yes</a>";
       var buttonYesProcess = "<a href='#' class='btn-s-gj bgc-btn' id='button-confirm-group-member-pending'>Yes</a>";
       $('.close-button-confirm-group').hide();
       $('.loader-process').show();
       $('#button-confirm-group-member-pending').replaceWith(buttonYesProcess);
  
        $.ajax({
          url: url,
          type: 'POST',
          data: data,   
          success: function(){
              $('#modalConfirmGroupMemberPending').modal('hide');
              $('.close-button-confirm-group').show();
              $('.loader-process').hide();
              $('#button-confirm-group-member-pending').replaceWith(buttonYes);
              console.log('sukses confirm');
              swal({ 
                title: "Thank you!",
                 text: "success Confirm ",
                  type: "success",
                });
  
                 var content = "<span class='warp-btn-f-uf' id='btn-joined-group'>"+
                               "<i class='fa fa-check'></i>"+
                               "<input type='button' class='btn btn-u-uf' id='memberleaveGroup' groupName ='"+group_name+"' groupID='"+id_group+"' username='"+username+"' value='Joined' data-value-hover='Leave Group'>"+
                               "</span>";
                 $('#confirmGroupMember').replaceWith(content);
                 var content2 = "<button type='button' class='btn btn-line-stat col-btn-ex'><i class='fa fa-check'></i>Joined</button>";
                 $('#buttonConfirmGroup'+id_group).replaceWith(content2);

                 $('#buttonPendingConfirm'+username).remove();
                 $('#buttonPendingReject'+username).remove();
                 $('#dataPending'+username).prependTo('#clearfix-groupmember3');
                 var totalCountMember = $('#totalCountMember').text();
                 var totalCountPending = $('#totalCountPending').text();

                 var a = 1;
                 var memberSplit = totalCountMember.split(" ");
                 var member = memberSplit[0];
                 var totalMemberNow = parseInt(member) + parseInt(a);

                 var pendingSplit = totalCountPending.split(" ");
                 var pending = pendingSplit[0];
                 var totalPendingNow = parseInt(pending) - parseInt(a);

                 var contentCountMember = "<span class='pull-right sum-member' id='totalCountMember'>"+totalMemberNow+" Members</span>";
                 var contentCountPending = "<span class='pull-right sum-member' id='totalCountPending'>"+totalPendingNow+" Members</span>";
                 $('#totalCountMember').replaceWith(contentCountMember);
                 $('#totalCountPending').replaceWith(contentCountPending);
                 var contentData = ""; 
                      contentData += "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>";
                      contentData += "<div class='nf-item-wall-track'>";
                      contentData += "<p>No new member invitation</p>";
                      contentData += "</div>";
                      contentData += "</div>"; 
                 if(pending === '1' || pending === 1){
                    $('#clearfix-groupmember5').replaceWith(contentData);
                 }

                var totalCountYourGroupPending = $('#totalCountYourGroupPending').text();
                console.log(totalCountYourGroupPending);
                var a = 1;
                var totalCount = parseInt(totalCountYourGroupPending) - parseInt(a);
                var replacetotalCountYourGroupPending = "<span id='totalCountYourGroupPending' style:display:none;>"+totalCount+"</span>";
                if(totalCountYourGroupPending > 1 || totalCountYourGroupPending > '1'){
                     $('#dataYourGroupPending'+id_group).prependTo('#clearfix-myGroup');
                     var contentNewButton = "<span class='warp-btn-f-uf'>"+
                                "<i class='fa fa-check'></i>"+
                                "<span id='hiddenGroupIDYourGroup"+id_group+"' style='display: none;''>"+id_group+"</span>"+
                                "<span id='hiddenGroupNameYourGroup"+id_group+"' style='display: none;''>"+group_name+"</span>"+
                                "<input type='button' onclick='memberGroupLeaveGroup("+'"'+id_group+'"'+");' class='btn btn-u-uf' value='Joined' data-value-hover='Leave Group'>"+     
                                "</span>";
                     $('#buttonConfirmGroupInvitation'+id_group).replaceWith(contentNewButton);
                     $('#buttonRejectGroupInvitation'+id_group).remove();    
                }else if(totalCountYourGroupPending === 1 || totalCountYourGroupPending === '1'){
                     $('#dataYourGroupPending'+id_group).prependTo('#clearfix-myGroup');
                     var contentNewButton = "<span class='warp-btn-f-uf'>"+
                                "<i class='fa fa-check'></i>"+
                                "<span id='hiddenGroupIDYourGroup"+id_group+"' style='display: none;''>"+id_group+"</span>"+
                                "<span id='hiddenGroupNameYourGroup"+id_group+"' style='display: none;''>"+group_name+"</span>"+
                                "<input type='button' onclick='memberGroupLeaveGroup("+'"'+id_group+'"'+");' class='btn btn-u-uf' value='Joined' data-value-hover='Leave Group'>"+     
                                "</span>";
                     $('#buttonConfirmGroupInvitation'+id_group).replaceWith(contentNewButton);
                     $('#buttonRejectGroupInvitation'+id_group).remove();    

                     var contentDataNull = "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>"+
                                       "<div class='nf-item-wall-track'>"+
                                       "<p>There is no Group</p>"+
                                       "</div>"+
                                       "</div>"; 
                     $('#clearfix-groupInvitation').prepend(contentDataNull);

                }
                $('#totalCountYourGroupPending').replaceWith(replacetotalCountYourGroupPending);     
                 
          },
          error: function(){
             $('#modalConfirmGroupMemberPending').modal('hide');
             $('.close-button-confirm-group').show();
             $('.loader-process').hide();
             $('#button-confirm-group-member-pending').replaceWith(buttonYes);
              console.log('error');
              swal({ 
                title: "error!",
                 text: "please try again later... ",
                  type: "warning",
                });
          }

        });
   });
});

//============== USER REJECT INVITE FROM PAGE MEMBER ===============//
function memberGroupPendingReject(i){
     console.log('confirm test');
      var groupID = $('#hiddenGroupIDPending'+i).text();
      var groupName = $('#hiddenGroupNamePending'+i).text();
      var username = $('#hiddenMemberUsernamePending'+i).text();
      var userID = $('#hiddenMemberIDPending'+i).text();
    
      console.log(groupID);
      
    $('#modalRejectGroupMemberPending').modal({
          backdrop: 'static',
          keyboard: true,
          show : true });
    $('#data-groupname-reject-confirm').text(groupName);
    $('#data-groupid-reject-confirm').text(groupID);
    $('#data-username-reject-confirm').text(username);
    $('#data-userid-reject-confirm').text(userID);
    
}

$(document).on('ready pjax:success', function(){
  $('.process-not-interest-group-member-reject').click(function(){

        var id_group = $('#data-groupid-reject-confirm').text();
        var group_name = $('#data-groupname-reject-confirm').text();
        var username = $('#data-username-reject-confirm').text();

        var url = base_url+'/notInterestGroup';
        var invite_status = '1';
        var data = 'groupID=' + id_group + '&invite_status=' + invite_status;

        var buttonYes = "<a href='#' class='btn-s-gj bgc-btn process-not-interest-group-member-reject' id='button-reject-group-member-pending'>Yes</a>";
        var buttonYesProcess = "<a href='#' class='btn-s-gj bgc-btn' id='button-reject-group-member-pending'>Yes</a>";
        $('.loader-process').show();
        $('.close-button-confirm-group').hide();
        $('#button-reject-group-member-pending').replaceWith(buttonYesProcess);
  
        $.ajax({
          url: url,
          type: 'POST',
          data: data,   
          success: function(){
            $('#modalRejectGroupMemberPending').modal('hide');
            $('.loader-process').hide();
            $('.close-button-confirm-group').show();
            $('#button-reject-group-member-pending').replaceWith(buttonYes);
              console.log('sukses reject invite');
              swal({ 
                title: "Thank you!",
                 text: "success reject invite ",
                  type: "success",
                  allowOutsideClick: false,
                });
              $('.swal2-confirm').click(function(){
                var content = "<button type='button' id='UserJoinRequestGroupMember'  groupName ='"+group_name+"' groupID='"+id_group+"' username='"+username+"' class='btn btn-add-f'><i class='fa fa-user-plus'></i>Join Group</button>";
                $('#confirmGroupMember').replaceWith(content);

                 var totalCountPending = $('#totalCountPending').text();
                 var a = 1;
                 var pendingSplit = totalCountPending.split(" ");
                 var pending = pendingSplit[0];
                 console.log(pending);
                 var totalPendingNow = parseInt(pending) - parseInt(a);
                 var contentCountPending = "<span class='pull-right sum-member' id='totalCountPending'>"+totalPendingNow+" Members</span>";
                 $('#totalCountPending').replaceWith(contentCountPending);
                 var contentData = ""; 
                      contentData += "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>";
                      contentData += "<div class='nf-item-wall-track'>";
                      contentData += "<p>No new pending invitation</p>";
                      contentData += "</div>";
                      contentData += "</div>"; 
                 if(pending > '1' || pending > 1){
                    $('#dataPending'+username).remove();
                 }      
                 else if(pending === '1' || pending === 1){
                    $('#dataPending'+username).replaceWith(contentData);
                 }     
              });              
          },
          error: function(){
             $('#modalRejectGroupMemberPending').modal('hide');
             $('.loader-process').hide();
             $('.close-button-confirm-group').show();
             $('#button-reject-group-member-pending').replaceWith(buttonYes);
              console.log('error');
              swal({ 
                title: "error!",
                 text: "please try again later... ",
                  type: "warning",
                });
          }

        });
   });
});

function memberGroupPendingAdminCancelInvite(i){
   var groupID = $('#hiddenGroupIDPending'+i).text();
   var groupName = $('#hiddenGroupNamePending'+i).text();
   var memberID = $('#hiddenMemberIDPending'+i).text();
   var username = $('#hiddenMemberUsernamePending'+i).text();

   $('#AdminCancelInvitation').modal({
          backdrop: 'static',
          keyboard: true,
          show : true });
   $('#data-groupname-cancel-invite').text(groupName);
   $('#data-groupid-cancel-invite').text(groupID);
   $('#data-memberid-cancel-invite').text(memberID);
   $('#data-username-cancel-invite').text(username);

}

// ==================  admin cancel invite ========================//
$(document).on('ready pjax:success', function(){
  $('body').on('click', '.proses-admin-cancel-invite', function(){
       var group_id = $('#data-groupid-cancel-invite').text();
       var member_id = $('#data-memberid-cancel-invite').text();
       var username = $('#data-username-cancel-invite').text();

       var invite_status = '1';
       var data = 'groupID=' + group_id + '&memberID=' + member_id + '&invite_status=' + invite_status;
       var url = base_url+'/adminNotAcceptRequest';

       var buttonYes = "<a href='#' class='btn-s-gj bgc-btn proses-admin-cancel-invite' id='button-admin-cancel-invitation'>Accept</a>";
       var buttonYesProcess = "<a href='#' class='btn-s-gj bgc-btn' id='button-admin-cancel-invitation'>Accept</a>";
       $('.loader-process').show();
       $('.close-button-admin-cancel-invitation').hide();
       $('#proses-admin-cancel-invite').replaceWith(buttonYesProcess);

        $.ajax({
          url: url,
          type: 'POST',
          data: data,
          success: function(){
              $('#AdminCancelInvitation').modal('hide');
              $('.loader-process').hide();
              $('.close-button-admin-cancel-invitation').show();
              $('#proses-admin-cancel-invite').replaceWith(buttonYes);
              console.log('sukses cancel invite');
               swal({ 
                title: "Thank you!",
                 text: "success cancel Invite ",
                  type: "success",
                });

                 var totalCountPending = $('#totalCountPending').text();
                 var a = 1;
                 var pendingSplit = totalCountPending.split(" ");
                 var pending = pendingSplit[0];
                 console.log(pending);
                 var totalPendingNow = parseInt(pending) - parseInt(a);
                 var contentCountPending = "<span class='pull-right sum-member' id='totalCountPending'>"+totalPendingNow+" Members</span>";
                 $('#totalCountPending').replaceWith(contentCountPending);
                 var contentData = ""; 
                      contentData += "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>";
                      contentData += "<div class='nf-item-wall-track'>";
                      contentData += "<p>No new pending invitation</p>";
                      contentData += "</div>";
                      contentData += "</div>"; 
                 if(pending > '1' || pending > 1){
                    $('#dataPending'+username).remove();
                 }      
                 else if(pending === '1' || pending === 1){
                    $('#dataPending'+username).replaceWith(contentData);
                 }     
          },
          error: function(){
            $('#AdminCancelInvitation').modal('hide');
               $('.loader-process').hide();
              $('.close-button-admin-cancel-invitation').show();
              $('#proses-admin-cancel-invite').replaceWith(buttonYes);
              swal({ 
                title: "error!",
                 text: "please try again later... ",
                  type: "warning",
                });
          }

        });
    });
});

function memberGroupRequestAdminConfirm(i){
   var groupID = $('#hiddenGroupIDRequest'+i).text();
   var groupName = $('#hiddenGroupNameRequest'+i).text();
   var username = $('#hiddenUsernameRequest'+i).text();
   var memberID = $('#hiddenMemberIDRequest'+i).text();


    $('#AdminConfirmRequest').modal({
          backdrop: 'static',
          keyboard: true,
          show : true });
    $('#data-groupname-request').text(groupName);
    $('#data-groupid-request').text(groupID);
    $('#data-username-request').text(username);
    $('#data-memberid-request').text(memberID);
    
}

function memberGroupRequestAdminReject(i){
   var groupID = $('#hiddenGroupIDRequest'+i).text();
   var groupName = $('#hiddenGroupNameRequest'+i).text();
   var username = $('#hiddenUsernameRequest'+i).text();
   var memberID = $('#hiddenMemberIDRequest'+i).text();


    $('#AdminRejectRequest').modal({
          backdrop: 'static',
          keyboard: true,
          show : true });
    $('#data-groupname-request-reject').text(groupName);
    $('#data-groupid-request-reject').text(groupID);
    $('#data-username-request-reject').text(username);
    $('#data-memberid-request-reject').text(memberID);
    
}

function AdminProsesConfirmRequest(){
   $('body').on('click', '.proses-admin-confirm-request', function(){
      var group_id = $('#data-groupid-request').text();
      var member_id = $('#data-memberid-request').text();
      var username = $('#data-username-request').text();
   
      var data = 'groupID=' + group_id + '&memberID=' + member_id;
      var url = base_url+'/adminAcceptRequest';

      var buttonAccept = "<a href='#' class='btn-s-gj bgc-btn proses-admin-confirm-request' id='button-admin-confirm-request'>Accept</a>";
      var buttonAcceptProcess = "<a href='#' class='btn-s-gj bgc-btn' id='button-admin-confirm-request'>Accept</a>";
      $('.loader-process').show();
      $('.close-button-admin-confirm-request').hide();
      $('#button-admin-confirm-request').replaceWith(buttonAcceptProcess);

       $.ajax({
          url: url,
          type: 'POST',
          data: data,     
          success: function(){
            $('#AdminConfirmRequest').modal('hide');
            $('.loader-process').hide();
            $('.close-button-admin-confirm-request').show();
            $('#button-admin-confirm-request').replaceWith(buttonAccept);
              console.log('sukses join request');
               swal({ 
                title: "Thank you!",
                 text: "you success confirm request",
                  type: "success",
                });
            
                var a = 1;
                var totalCountMember = $('#totalCountMember').text();
                var memberRequest = totalCountMember.split(" ");
                var member = memberRequest[0];
                var totalMemberNow = parseInt(member) + parseInt(a);

                var totalCountRequest = $('#totalCountRequest').text();
                var requestSplit = totalCountRequest.split(" ");
                var request = requestSplit[0];
                var totalRequestNow = parseInt(request) - parseInt(a);
                console.log(totalRequestNow);

                var contentMember = "<span class='pull-right sum-member' id='totalCountMember'>"+totalMemberNow+" Members</span>";
                var contenRequest = "<span class='pull-right sum-member' id='totalCountRequest'>"+totalRequestNow+" Members</span>";

                $('#dataMemberRequest'+username).prependTo('#clearfix-groupmember3');
                $('#buttonAdminConfirmRequestGroupMember'+username).remove();
                $('#buttonAdminRejectRequestGroupMember'+username).remove();
                $('#totalCountMember').replaceWith(contentMember);
                $('#totalCountRequest').replaceWith(contenRequest);
                if(request === '1' || request === 1){
                   var contentData = ""; 
                      contentData += "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>";
                      contentData += "<div class='nf-item-wall-track'>";
                      contentData += "<p>No new member requests</p>";
                      contentData += "</div>";
                      contentData += "</div>"; 
                    $('#clearfix-groupmember7').prepend(contentData);
                }
          },
          error: function(){
            $('#AdminConfirmRequest').modal('hide');
            $('.loader-process').hide();
            $('.close-button-admin-confirm-request').show();
            $('#button-admin-confirm-request').replaceWith(buttonAccept);
              console.log('error');
              swal({ 
                title: "error!",
                 text: "please try again later... ",
                  type: "warning",
                });
          }

        });

   });
}

function AdminProsesCancelYourRequest(){
   $('body').on('click', '.proses-admin-cancel-request', function(){
      var group_id = $('#data-groupid-request-reject').text();
      var member_id = $('#data-memberid-request-reject').text();
      var username = $('#data-username-request-reject').text();

      var invite_status = '2';
      var data = 'groupID=' + group_id + '&memberID=' + member_id + '&invite_status=' + invite_status;
    
      var url = base_url+'/adminNotAcceptRequest';
      var buttonYes = "<a href='#' class='btn-s-gj bgc-btn proses-admin-cancel-request' id='button-admin-reject-request'>Accept</a>";
      var buttonYesProcess = "<a href='#' class='btn-s-gj bgc-btn' id='button-admin-reject-request'>Accept</a>";

      $('.loader-process').show();
      $('.close-button-admin-reject-request').hide();
      $('#button-admin-reject-request').replaceWith(buttonYesProcess);

       $.ajax({
          url: url,
          type: 'POST',
          data: data,      
          success: function(){
            $('#AdminRejectRequest').modal('hide');
            $('.loader-process').hide();
            $('.close-button-admin-reject-request').show();
            $('#button-admin-reject-request').replaceWith(buttonYes);
              console.log('sukses cancel request');
               swal({ 
                title: "Thank you!",
                 text: "success reject request ",
                  type: "success",
                });

              var a = 1;
              var totalCountRequest = $('#totalCountRequest').text();
              var requestSplit = totalCountRequest.split(" ");
              var request = requestSplit[0];
              var totalRequestNow = parseInt(request) - parseInt(a);
              console.log(totalRequestNow);
              var contenRequest = "<span class='pull-right sum-member' id='totalCountRequest'>"+totalRequestNow+" Members</span>";
              $('#totalCountRequest').replaceWith(contenRequest);
              if(request > 1 || request > '1'){
                  $('#dataMemberRequest'+username).remove();
              }else if(request === 1 || request === '1'){
                var contentData = ""; 
                    contentData += "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>";
                    contentData += "<div class='nf-item-wall-track'>";
                    contentData += "<p>No new member requests</p>";
                    contentData += "</div>";
                    contentData += "</div>"; 
                  $('#dataMemberRequest'+username).replaceWith(contentData);
              }
          },
          error: function(){
            $('#AdminRejectRequest').modal('hide');
            $('.loader-process').hide();
            $('.close-button-admin-reject-request').show();
            $('#button-admin-reject-request').replaceWith(buttonYes);
              console.log('error');
              swal({ 
                title: "error!",
                 text: "please try again later... ",
                  type: "warning",
                });
          }

        });

   });
}

//====================== PROSES CANCEL REQUEST SEND IN GROUP MEMBER REQUEST ========================//
function memberGroupRequestSend(i){
  var groupID = $('#hiddenGroupIDRequest'+i).text();
  var groupName = $('#hiddenGroupNameRequest'+i).text();
  var username = $('#hiddenUsernameRequest'+i).text();
  var memberID = $('#hiddenMemberIDRequest'+i).text();

  console.log(groupID);
  console.log(groupName);
  console.log(username);
  console.log(memberID);

    $('#userCancelJoinGroup').modal({
          backdrop: 'static',
          keyboard: true,
          show : true });
    $('#data-groupname-request-cancel').text(groupName);
    $('#data-groupid-request-cancel').text(groupID);
    $('#data-memberid-request-cancel').text(memberID);
    $('#data-myusername-request-cancel').text(username);
      
}

//======================== ADD FRIEND FROM GROUP MEMBER ===============================//
function addToMyFriendGroup(i){
  var memberID = $('#hiddenMemberID'+i).text();
  console.log(memberID)

  var url = base_url+'/addToFriend/'+memberID;

   $.ajax({
          url: url,
          type: 'POST',
          data: {friendID:memberID},
         
          success: function(){
              console.log('sukses add friend');
            swal({ 
                title: "Thank you!",
                 text: "your request was sent , please waiting confirm...",
                  type: "success",
                });
                var content = "<button type='button'  class='btn btn-add-f-list col-btn-ex'><i class='fa fa-user-plus'></i>Request Send</button>";
                $('#buttonFriendMember'+i).html(content);
          },

          error: function(){
              console.log('error');
              // window.location.reload();
          }

        });

}
//======================== ADD FRIEND FROM GROUP MEMBER PENDING===============================//
function addToMyFriendGroupPending(i){
  var memberID = $('#hiddenMemberIDPending'+i).text();
  console.log(memberID)

 var url = base_url+'/addToFriend/'+memberID;
 $.ajax({
          url: url,
          type: 'POST',
          data: {friendID:memberID},
         
          success: function(){
              console.log('sukses add friend');
              swal({ 
                title: "Thank you!",
                 text: "your request was sent , please waiting confirm...",
                  type: "success",
                });
                var content = "<button type='button'  class='btn btn-add-f-list col-btn-ex'><i class='fa fa-user-plus'></i>Request Send</button>";
                $('#buttonFriendMemberPending'+i).html(content);
          },

          error: function(){
              console.log('error');
              // window.location.reload();
          }

        });

}
//======================== ADD FRIEND FROM GROUP MEMBER REQUEST===============================//
function addToMyFriendGroupRequest(i){
  var memberID = $('#hiddenMemberIDRequest'+i).text();
  console.log(memberID)
 var url = base_url+'/addToFriend/'+memberID;

 $.ajax({
          url: url,
          type: 'POST',
          data: {friendID:memberID},
         
          success: function(){
              console.log('sukses add friend');
              swal({ 
                title: "Thank you!",
                 text: "your request was sent , please waiting confirm...",
                  type: "success",
                });
                var content = "<button type='button'  class='btn btn-add-f-list col-btn-ex'><i class='fa fa-user-plus'></i>Request Send</button>";
                $('#buttonFriendMemberRequest'+i).html(content);
          },

          error: function(){
              console.log('error');
              // window.location.reload();
          }

        });

}

//======================== CONFIRM FRIEND FROM GROUP MEMBER =============================//
function confirmFriendRequestFromMember(i){
  console.log('confirm ........');
    var memberID = $('#hiddenMemberID'+i).text();
    var  url =  base_url+'/ConfrimFriendGroupMember/'+memberID;

    $.ajax({
          url: url,
          type: 'POST',
          data: {friendID:memberID},
         
          success: function(){
              console.log('sukses confirm friend');
              swal({ 
                title: "Thank you!",
                 text: "sukses confirm friend...",
                  type: "success",
                });
                  $('#buttonFriendMember'+i).html('');
          },

          error: function(){
              console.log('error');
              // window.location.reload();
          }

        });

}

//======================== CONFIRM FRIEND FROM GROUP MEMBER PENDING =============================//
function confirmFriendRequestFromMemberPending(i){
  console.log('confirm ........');
    var memberID = $('#hiddenMemberIDPending'+i).text();
    console.log(memberID);
    var  url =  base_url+'/ConfrimFriendGroupMember/'+memberID;
    console.log(url);
    $.ajax({
          url: url,
          type: 'POST',
          data: {friendID:memberID},
         
          success: function(){
              console.log('sukses confirm friend');
              swal({ 
                title: "Thank you!",
                 text: "sukses confirm friend...",
                  type: "success",
                });
                  $('#buttonFriendMemberPending'+i).html('');
          },

          error: function(){
              console.log('error');
              // window.location.reload();
          }

        });

}

//======================== CONFIRM FRIEND FROM GROUP MEMBER REQUEST =============================//
function confirmFriendRequestFromMemberRequest(i){
  console.log('confirm ........');
    var memberID = $('#hiddenMemberIDRequest'+i).text();
    console.log(memberID);
    var  url =  base_url+'/ConfrimFriendGroupMember/'+memberID;
    console.log(url);
    $.ajax({
          url: url,
          type: 'POST',
          data: {friendID:memberID},
         
          success: function(){
              console.log('sukses confirm friend');
              swal({ 
                title: "Thank you!",
                 text: "sukses confirm friend...",
                  type: "success",
                });
                  $('#buttonFriendMemberRequest'+i).html('');
          },

          error: function(){
              console.log('error');
              // window.location.reload();
          }

        });

}

//============= GROUP CONTENT BAGIAN KIRI =================//

function getNoContentGroupLeft(type){
  var content = '';
  content += "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 padten'>";
  content += "<div class='nf-item-wall'>";
  content += "<p>";
    if(type == 1){
      content += "You don't have any song yet";
    }
    else if(type == 2){
      content += "You don't have any cover song yet";
    }
    else if(type == 3){
      content += "You don't have any collaboration song yet";
    }
  content += "</p>";
  content += "</div>";
  content += "</div>";
  $('#loadingGroupLeft'+type).replaceWith(content);
}

function getContentFriendListGroupLeft(data,i){
  // var data = data;
  console.log(data);
  var content = "";
  var screensize = document.documentElement.clientWidth;
  var getLeftSidebar = (25/100) * screensize;

  var friendProfPicWidth = (getLeftSidebar/3).toFixed(1);

  var base_url = window.location.origin;


    var profile_image = data['picture'];

    var name_profile_image = profile_image.substring(profile_image.lastIndexOf('/')+1); // nama profile image
    var folder = 'img/avatar/';  // folder penyimpanan profile image
    var picture = folder+name_profile_image; // link profile image

  content += "<div class='gj-friend-side'>";
  content += "<div class='img-friend-side'>";
  content += "<div class='tb-pp'>";
  if(data['picture'] == 0 ){
        content += "<a href='#'><img src='"+base_url+"/img/avatar/avatar-300x300.png' class='img-responsive img-rounded-full'></a>";
    }else{
      if(profile_image != picture){
        content += "<a href='#'><img src='"+data['picture']+"' class='img-responsive img-rounded-full'></a>";
          }else{
        content += "<a href='#'><img src='"+base_url+'/'+data['picture']+"' class='img-responsive img-rounded-full'></a>";
          }
    }    
  content += "</div>";
  content += "<div class='tb-info'>";
  content += "<span><a id='pjax' data-pjax='yes' href='"+base_url+"/"+data['username']+"' class='btn-single-post-tl'>"+data['username']+"</a></span>";
  content += "</div>";
  content += "<div class='tile-counters'> <span class='tile-counter'>"+data['jumlahFriend']+" Friends</span> </div>";
  // content += "<div class='stat-friend-side'>";
  // content += "<button type='button' class='btn btn-line-stat col-btn-ex'><i class='fa fa-user-plus'></i>Add Friend</button>";
  // content += "</div>";
  content += "</div>";
  content += "</div>";
  
  return content;
}

function getContentFriendListGroupLeftNull(){
   var content = "";
   content += "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 padten'>";
   content += "<div class='nf-item-wall'>";
   content += "<p>";
   content += "You dont Have Any Friends yet";
   content += "</p>";
   content += "</div>";
   content += "</div>";

   return content;
}
function getContentGroupLeft(type){
  var data = getData_myWall(type, 0);
  var link = getLink_myWall(type);


  $.ajax({
    type  : "POST",
    data : data,
    url   : link,
    success : function(responseText) {
   
      var count = responseText['count'];
      var data = responseText['data'];

      if(count === 0){
        
        getNoContentGroupLeft(type);
        
      }
      else{
        var content = "";
        for (var i = 0; i < data.length; i++) {
          content += getContentPrint_myWall(type, data[i], i);
        };
        
        $('#loadingGroupLeft'+type).replaceWith(content);

        setTakeTotal_myWall(count, type);
        setLink_myWall(type);
       }
      },
      error: function(){
        console.log('error');
      }
    
  });
}

function getMyBio(){
    var id = document.getElementById("sessiongroup").value;
        $.ajax({
          type : 'POST',
          url  : '/getmybioinfo',
          data : {id : id},
          success :  function(response)
              {
                // document.getElementById("loadingMyBio").style.display = "none";
                console.log(response)
                // var x = -1
                // $.each(response['group'], function( index, value ) {
                // x=x+1
                // var grup = response['group'][x]['name'] 
                // });
                $('#loadingGroupMyBio').replaceWith("<div class='bio-wrapper'>"+
                                                    "<div class='bio-short'>From<span>"+ response['user'][0]['birth_city'] +"</span></div>"+
                                                    "<div class='bio-short'>Live in<span>"+ response['user'][0]['city'] +"</span></div>"+
                                                    // "<div class='bio-short'>Play<span>Guitar, Bass</span></div>"+
                                                    // "<div class='bio-short'>Music Genre<span>Keroncong, Rock, Pop</span></div>"+
                                                    "<div class='bio-short'>Join with<span>"+ response['namegroup'][0]['name']  +"</span></div>"+
                                                "</div>"
                                                );
              }
            })
}

//event Participant show modal join group event participate
function groupEventParticipant(groupID,groupEventID,status){
    if(status === 1 || status === '1'){
        var alertText = "You are success participate at event <strong>"+eventTitle+"</strong>";
    }else{
        var alertText = "Thank you of your response at event <strong>"+eventTitle+"</strong>";
    }

    var data = '&groupID=' + groupID + '&groupEventID=' + groupEventID + '&statusEventParticipate=' + status;
    console.log(data);
    var url = base_url+"/groupEventParticipate";
    
    var trackID = $('#trackID-groupEvent'+groupEventID).text();
    var judulTrack = $('#judulTrack-groupEvent'+groupEventID).text();
    var instrumentTrack = $('#instrumentTrack-groupEvent'+groupEventID).text();
    var creatorTrack = $('#creatorTrack-groupEvent'+groupEventID).text();
    var priceTrack = $('#priceTrack-groupEvent'+groupEventID).text();
    var eventTitle = $('#eventTitle-groupEvent'+groupEventID).text();

    $('#modalJoin-groupEvent').modal({
        backdrop: 'static',
        keyboard: true,
        show : true });
    $('#groupID-groupEvent').text(groupID);
    $('#groupEventID-groupEvent').text(groupEventID);
    $('#eventTitle-groupEvent').text(eventTitle);
    $('#trackID-groupEvent').text(trackID);
    $('#judulTrack-groupEvent').text(judulTrack);
    if(instrumentTrack != 0 || instrumentTrack != ''){
        $('#instrumentTrack-groupEvent').text(instrumentTrack);
     }else{
        $('#instrumentTrack-groupEvent').text("not set");
     }   
    $('#creatorTrack-groupEvent').text(creatorTrack);
    if(priceTrack == 0){
        $('#priceTrack-groupEvent').text("Free");
    }else{
        $('#priceTrack-groupEvent').text(priceTrack*500);
    }
    
} 

// proses buy track group event 
function prosesBuyTrackGroupEvent(){
  var button = "<a id='buttonProsesJoinEvent' class='btn-s-gj bgc-btn'>Process</a>";
  $('#buttonProsesJoinEvent').replaceWith(button);
  $('.loader-process').show();
  $('.closeModalJoin-groupEvent').hide();

  var groupID = $('#groupID-groupEvent').text();
  var groupEventID = $('#groupEventID-groupEvent').text();
  var eventTitle = $('#eventTitle-groupEvent').text();
  var trackID = $('#trackID-groupEvent').text();
  var judulTrack = $('#judulTrack-groupEvent').text();
  var creatorTrack = $('#creatorTrack-groupEvent').text();
  var instrumentTrack = $('#instrumentTrack-groupEvent').text();
  var priceTrack = $('#priceTrack-groupEvent').text();

  var data = '&groupID=' +groupID + '&groupEventID=' +groupEventID + '&eventTitle=' +eventTitle + '&trackID=' +trackID + '&judulTrack=' +judulTrack + '&creatorTrack=' +creatorTrack;
  var link = window.location.origin+"/prosesJoinGroupEvent";

  var buttonProsess = " <a id='buttonProsesJoinEvent' href='javascript:void(0);' onclick='prosesBuyTrackGroupEvent();' class='btn-s-gj bgc-btn'>Process</a>";

    $.ajax({
       type : "POST",
       url : link,
       data : data,
       success : function(responseText){
          console.log('sukses');
          // console.log(responseText);
          if(responseText['status'] === 1 || responseText['status'] === '1'){
              // console.log('create project');
               window.location.href = window.location.origin+"/studio/karaoke/"+responseText['projectID'];
          }else if(responseText === -1 || responseText === '-1'){
              // console.log('gak cukup duit');
              $('#modalJoin-groupEvent').modal('hide');
              swal('Oops...Your balance is not enough!','Please, refill your wallet!','error');
              $('.loader-process').hide();
              $('.closeModalJoin-groupEvent').show();
              $('#buttonProsesJoinEvent').replaceWith(buttonProsess);
          }else if(responseText === 0 || responseText === '0'){
              // console.log('kebanyakan project');
              $('#modalJoin-groupEvent').modal('hide');
              swal('Oops...sorry check limit your project pending');
              $('.loader-process').hide();
              $('.closeModalJoin-groupEvent').show();
              $('#buttonProsesJoinEvent').replaceWith(buttonProsess); 
          }else{
            // console.log('error123');
              $('#modalJoin-groupEvent').modal('hide');
              swal('Oops...try again!','error');
              $('.loader-process').hide();
              $('.closeModalJoin-groupEvent').show();
              $('#buttonProsesJoinEvent').replaceWith(buttonProsess);
          }
       },
       error : function(){
           console.log('error');
       }

    });
}

function sudahParticipate(){
     swal({
        title: 'You have participate',
        text: 'I will close in 2 seconds.',
        timer: 2000
      })
}

function harusJoinGroup(groupID,eventID){
  var groupName = $('#groupName-groupEvent'+eventID).text();
     swal({
        title: 'Sorry, private event, you must join in group '+groupName,
        text: 'I will close in 2 seconds.',
        timer: 2000
      })
}
  
function replaceButtonEventParticipate(data,groupID,groupEventID,eventTitle){
    var content = "";
     if(data['status'] === '1' || data['status'] === 1){
       content += "<div class='btn-in-stat' id='groupEventParticipant"+groupEventID+"'>";
       content += "<span class='warp-btn-f-uf'>";
       content += "<i class='fa fa-check'></i>";
       content += "<input type='button' onclick='groupEventParticipant("+'"'+groupID+'"'+", "+'"'+groupEventID+'"'+", "+'"'+eventTitle+'"'+","+'"0"'+");' class='btn btn-line-stat btn-u-uf' value='Joined' data-value-hover='Leave'>";
       content += "</span>";
       content += "</div>";
    }else{
       content += "<div class='btn-in-stat' id='groupEventParticipant"+groupEventID+"'>";
       content += "<button type='button' onclick='groupEventParticipant("+'"'+groupID+'"'+", "+'"'+groupEventID+'"'+", "+'"'+eventTitle+'"'+","+'"1"'+");' class='btn btn-line-stat col-btn-ex'>Join</button>";
       content += "</div>";
    }
   
    return content;
}

function replaceTotalParticipate(dataParticipate,groupID,groupEventID,countAnggotaParticipant){
    var user_participant=[];
    for(var i = 0; i < dataParticipate.length; i++){
       user_participant.push(dataParticipate[i]['username']);
    }
    var content = "<span id='totalParticipate"+groupEventID+"' class='hint--right hint--info' data-hint="+user_participant+">Participant:"+countAnggotaParticipant+"</span>";
    return content;                
}

function cantLeaveTheGroup(){
   swal({
        title: 'Sorry...',
        text: 'You can not leave the group there running event or event that no winner',
        timer: 2000
      });
}

function shareMyGroupProfile(){
  // console.log('modal');
 var group_name = $('#group_name-sharedGroupProfile').text();
 var group_picture = $('#group_picture-sharedGroupProfile').text();
 var group_description = $('#group_description-sharedGroupProfile').text();
 var group_id = $('#group_id-sharedGroupProfile').text();
console.log(group_id);
  $('#modalmyShareGroupProfile').modal({
       backdrop: 'static',
       keyboard: true, 
       show: true
    });

  $('#group_name-shared-GroupProfile').text(group_name);
  $('#group_picture-shared-GroupProfile').text(group_picture);
  $('#group_description-shared-GroupProfile').text(group_description);
  $('#group_id-shared-GroupProfile').text(group_id);

}

$(document).on('ready pjax:success', function(){
     $('body').on('click', '.process-shared-group-profile', function(){
        var group_name = $('#group_name-shared-GroupProfile').text();
        var group_picture = $('#group_picture-shared-GroupProfile').text();
        var group_description = $('#group_description-shared-GroupProfile').text();
        var group_id = $('#group_id-shared-GroupProfile').text();

        var data = 'group_name=' +group_name+ '&group_picture=' +group_picture + '&group_description=' +group_description + '&group_id=' +group_id;

        buttonYes = "<a href='#' class='btn-s-gj bgc-btn process-shared-group-profile' id='button-shared-group-profile'>Yes</a>";
        buttonYesProcess = "<a href='#' class='btn-s-gj bgc-btn' id='button-shared-group-profile'>Yes</a>";
        $('#button-shared-group-profile').replaceWith(buttonYesProcess);
        $('.close-button-shared-group-profile').hide();
        $('.loader-process').show();

        $.ajax({
            type : 'POST',
            data : data,
            url : window.location.origin+'/sharedMyGroupProfile',
            success : function(responseText){
               $('#modalmyShareGroupProfile').modal('hide');
               $('#button-shared-group-profile').replaceWith(buttonYes);
               $('.close-button-shared-group-profile').show();
               $('.loader-process').hide();
                console.log(responseText);
              swal({ 
                title: "Thank you!",
                 text: "success share group profile... ",
                  type: "success",
                });
            },
            error : function(){
               $('#modalmyShareGroupProfile').modal('hide');
               $('#button-shared-group-profile').replaceWith(buttonYes);
               $('.close-button-shared-group-profile').show();
               $('.loader-process').hide();
               swal({ 
                title: "error!",
                 text: "please try again later... ",
                  type: "warning",
                });
              console.log('error');
            }
        });
     });
});

  /*=============================================================================================================================================
  ========================================================= CHOSEN JS ===========================================================================
  =============================================================================================================================================*/
   /*var config = {
      '.chosen-select'           : {},
      '.chosen-select-deselect'  : {allow_single_deselect:true},
      '.chosen-select-no-single' : {disable_search_threshold:10},
      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chosen-select-width'     : {width:"95%"},
    //'.result-selected'       : {display_disabled_options:false}
    
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }*/
  /*$(function() {
        $('.chosen-select').chosen({
      display_selected_options: true
    });
        $('.chosen-select-deselect').chosen({ allow_single_deselect: true });
    
      });*/
$(document).on('ready pjax:success', function(){
  $('.postMusic-bank').click(function(){
      $('#postMusicBank').modal('show');
      $('body').addClass("tot");
    });
  }
);
  
$(document).on('ready pjax:success', function(){
  $('#buttonaddevent').click(function(){
      $('#createGroup1').modal('show');
      $('body').addClass("tot");
    });
  }
);

 $(document).on('ready pjax:success', function() {
  $(".chosen-select").chosen({
      disable_search_threshold: 1,
      no_results_text: "Oops, nothing found!",
      width: "100%",
      display_selected_options:false
    });
 }); 
  

 $(document).on('ready pjax:success', function(){
      $("#selectSongEvent").hide();
      // $("#selectBackTrackEvent").hide();
 }); 
 $(document).on('ready pjax:success', function(){
    $("#optionBackTrack").click(function(){
        $("#selectSongEvent").hide();
    });
    $("#optionBackTrack").click(function(){
        $("#selectBackTrackEvent").show();
    });
}); 

$(document).on('ready pjax:success', function(){
    $("#optionSong").click(function(){
        $("#selectBackTrackEvent").hide();
    });
    $("#optionSong").click(function(){
        $("#selectSongEvent").show();
    });
}); 

$(document).on('ready pjax:success', function(){
    $("#optionNoBackTrackSong").click(function(){
        $("#selectBackTrackEvent").hide();
        $("#selectSongEvent").hide();
    });
}); 
  /*=============================================================================================================================================
  ===================================================== END CHOSEN JS ===========================================================================
  =============================================================================================================================================*/

// $(document).on('ready pjax:success', function(){
//   UploadCreate();
// });
// function UploadCreate(){  
//   $('#MyCreateGroup').click(function(){
//    var GroupName = $('#groupname').val();
//    var Description = $('#Description').val();
//    var PhotoGroup = $('#file-1').val();
//    var BackgroundGroup = $('#file-2').val();
   
//   var radios = document.getElementsByName('optionsRadios');

// for (var i = 0, length = radios.length; i < length; i++) {
//     if (radios[i].checked) {
//         // do whatever you want with the checked radio
//         var SettingGroup = radios[i].value
//         // alert(radios[i].value);

//         // only one radio can be logically checked, don't check the rest
//         break;
//     }
// }


// var nBytes = 0,
//       oFiles = document.getElementById('file-1').files,
//       nFiles = oFiles.length;
//   for (var nFileId = 0; nFileId < nFiles; nFileId++) {
//     nBytes += oFiles[nFileId].size;
//   }
//   var sOutput = nBytes + " bytes";
//   // optional code for multiples approximation
//   for (var aMultiples = ["KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB"], nMultiple = 0, nApprox = nBytes / 1024; nApprox > 1; nApprox /= 1024, nMultiple++) {
//     sOutput = nApprox.toFixed(3) + " " + aMultiples[nMultiple] + " (" + nBytes + " bytes)";
//   }
// console.log(oFiles);
// // var PhotoGroup = 
//   // var nBytes = 0,
//   //     // oFiles = document.getElementById('file-1').files,
//   //     oFiles2 = document.getElementById('file-2').files,
//   //     nFiles = oFiles2.length;
//   // for (var nFileId = 0; nFileId < nFiles; nFileId++) {
//   //   nBytes += oFiles2[nFileId].size;
//   // }
//   // var sOutput = nBytes + " bytes";
//   // // optional code for multiples approximation
//   // for (var aMultiples = ["KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB"], nMultiple = 0, nApprox = nBytes / 1024; nApprox > 1; nApprox /= 1024, nMultiple++) {
//   //   sOutput = nApprox.toFixed(3) + " " + aMultiples[nMultiple] + " (" + nBytes + " bytes)";
//   // }
// // console.log(oFiles);
// // console.log(oFiles2);

// alert(GroupName);
// alert(Description);
// alert(SettingGroup);
// alert(PhotoGroup);
// alert(BackgroundGroup);
// // alert(oFiles);
// // alert(sOutput);
// var data = 'GroupName='+GroupName + '&Description=' +Description + '&PhotoGroup=' +PhotoGroup + '&BackgroundGroup=' +BackgroundGroup + '&SettingGroup=' +SettingGroup;

//    $.ajax({
//           url: "createGroupProsess",
//           type: "POST",
//           data:data,

//           success: function(responseText){
//            console.log('sukses');
//            console.log(responseText);
//           },

//           error: function(jqXHR, textStatus, errorThrown){
//               console.log('error');
              
//           }

//         });
//   });
// }


