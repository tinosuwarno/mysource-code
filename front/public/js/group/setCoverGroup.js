var selected = null, // Object of the element to be moved
    y_pos_group = 0, // Stores x & y coordinates of the mouse pointer
    y_elem_group = 0; // Stores top, left values (edge) of the element

    function changedivBgGroup() {
    if (document.getElementById("draggable-element-group")) {
        document.getElementById("geserBgGroup").style.display = "none";
        document.getElementById("subGroup").style.display = "block";          
        document.getElementById("cancelGroup").style.display = "block"; 
        document.getElementById("helpGroup").style.display = "block"; 
        document.getElementById("draggable-element-group").id = "draggable-element-groupcover";
        // Bind the functions...
        document.getElementById('draggable-element-groupcover').onmousedown = function () {
        _drag_init_group(this);
            return false;
        };

        document.onmousemove = _move_elem_group;
        document.onmouseup = _destroy_group;
    } else {
        // document.getElementById("div_top2").innerHTML = "teste";            
        // document.getElementById("div_top2").id = "div_top1";
    }
}

// Will be called when user starts dragging an element
    function _drag_init_group(elem) {
    // Store the object of the element which needs to be moved
    selected = elem;
    y_elem_group = (y_pos_group + selected.offsetTop);
}

// Will be called when user dragging an element
    function _move_elem_group(e) {
    y_pos_group = document.all ? window.event.clientY : e.pageY;
    if (selected !== null) {
      //  selected.style.left = (x_pos - x_elem) + 'px';
      y_axis_group= -(y_pos_group - y_elem_group);
      if (y_axis_group >= 0 && y_axis_group <= 100) {
        selected.style.backgroundPositionY = -(y_pos_group - y_elem_group) + '%';
        }else if (y_axis_group >= 100){
            selected.style.backgroundPositionY = '100%';
        }else{
            selected.style.backgroundPositionY = '0%';
        }
    } 
}

// Destroy the object when we are done
    function _destroy_group() {
    selected = null;
}



 $(document).on('ready pjax:success', function(){
    $('#subGroup').click(function() {
        var group_id = $('#data-groupid-drag').text();
        var base_url = window.location.origin;
        var url = base_url+"/setCoverGroup/"+y_axis_group;
        var data = 'groupID=' + group_id;
        $.ajax({
              url: url,
              type: 'POST',
              data: data,
             
              success: function(){
                  window.location.reload();
              },

              error: function(){
                  window.location.reload();
              }

        });
     });
 });

 function cancel() {
    location.reload();
}
