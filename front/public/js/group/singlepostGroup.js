function checkElelemantSinglePostGroup(){
   var elementExists = new Array(
       document.getElementById('loadingSinglePostGroup'),
       document.getElementById('loadingSingleEvent')
    );

   var stop = false; 
    for (var i = elementExists.length - 1; i >= 0; i--) {
        if(elementExists[i] == null)
          stop = true;
      };

      if(stop)
        return 0;
      return 1;
}

$(document).on('ready pjax:success', function() {
    var check = checkElelemantSinglePostGroup();
    if(check == 0) return;

      getSinglePostGroup();
      getRightBarEvent();

  });

function printSinglePostGroup(data){
   var profile_image_post = data['profile_image_post'];  
   var name_profile_image_post = profile_image_post.substring(profile_image_post.lastIndexOf('/')+1); // nama profile image
   var folder = 'img/avatar/';  // folder penyimpanan profile image
   var picture = folder+name_profile_image_post; // l

   var create_post = data['create'];
   var t = create_post.split(/[- :]/);
   var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]); 

   var base_url = window.location.origin; 
   var content = "";

   content += "<div class='timeline-box'>";
   content += "<hr class='tb-hr'>";
   content += "<div class='tb-header'>";
   content += "<div class='tb-pp'>";
   if(data['profile_image_post'] == 0 ){
        content += "<a href='#'><img src='"+base_url+"/img/avatar/avatar-300x300.png' class='img-responsive img-rounded-full'></a>";
   }else{
       if(profile_image_post != picture){
           content += "<a href='#'><img src='"+data['profile_image_post']+"' class='img-responsive img-rounded-full'></a>";
       }else{
           content += "<a href='#'><img src='"+base_url+"/"+data['profile_image_post']+"' class='img-responsive img-rounded-full'></a>";
       }
   }
   content += "</div>";
   content += "<div class='tb-info'>";
   content += "<h1><a href='#' class='btn-single-post-tl'>"+data['usernamePost']+"</a></h1>";
   content += "</div>";
   content += "<div class='tb-time'>";
   content += "<p>"+ getDateTimeSince(d, false) +" ago</p>";
   content += "</div>";
   content += "</div>";
   content += "<div class='tb-content'>";
   if(data['trackID'] != '' || data['trackID'] != 0){
   content += "<div class='tb-img-post-song'>";
      if(data['pictureTrack'] != '' ){
          content += "<img src='"+base_url+"/"+data['pictureTrack']+"' class='img-responsive img-full-responsive'>";
      }else{
          content += "<img src='"+base_url+"/img/album/album-song_475x475.jpg' class='img-responsive img-full-responsive'>";
      }

   content += "<a href='#' class='gj-btn-play'><i onclick='playMusic("+'"'+data['trackID']+'"'+")' class='fa fa-play-circle-o fa-4x'></i></a>";
   content += "</div>";
   
   content += "<div class='warp-info-post-song'>";

   content += "<div class='gj-post-song-title'><a href='#'>"+data['judulTrack']+"</a></div>";
   content += "<div class='gj-post-song-author'>"+data['creatorTrack']+"</div>";
   content += "<div class='gj-post-song-genre'>"+data['genreTrack']+"</div>";
   content += "<div class='gj-post-song-contribut'>";
   // content += "<span><a href='#'>Contributor 1, </a></span>";
   // content += "<span><a href='#'>Tebo, </a></span>";
   // content += "<span><a href='#'>Kribo, </a></span>";
   // content += "<span><a href='#'>Contributor 4, </a></span>";
   content += "</div>";
   }else{
      content += "<div class='warp-info-post-song'>";
   }
   content += "<div class='gj-post-song-desc'>";
   content += "<p>";
   content += data['caption'];
   // content += "<a href='#' class='more-txt'>more</a>";
   content += "</p>";
   content += "</div>";
   content += "</div>";
   content += "<div class='warp-stat-post-song'>";
   content += "<div class='warp-stat-card in-tl'>";
   content += "<div class='add-pl-track-song'>";
   if(data['trackID'] != '' || data['trackID'] != 0){
        // content += "<a class='btn-add-pl-track-song'><i class='gritjam-icons gritjam-icon-add-playlist'></i></a>";
   }
   content += "</div>";
   if(data['statusLikePostGroup'] === 1 || data['statusLikePostGroup'] === '1'){
       content += "<div class='stat-song-track' id='statusLikePostGroup"+data['group_post_id']+"'>";
       content += "<div class='warp-ico-stat'>";
       content += "<div class='ico-stat'><a href='javascript:void(0)' onclick='udahLikePost();'><i class='fa fa-heart like-single-list-song' style='color:orange;'></i></a></div>";
       content += "<div class='num-stat'>"+data['like']+"</div>";
       content += "</div>";
       content += "</div>";
   }else{
       content += "<div class='stat-song-track' id='statusLikePostGroup"+data['group_post_id']+"'>";
       content += "<div class='warp-ico-stat'>";
       if(data['like'] === 'null' || data['like'] === null || data['like'] === ''){
           content += "<div class='ico-stat'><a href='javascript:void(0)' onclick='groupPostLikeProses("+'"'+data['group_post_id']+'"'+","+'"0"'+");'><i class='fa fa-heart like-single-list-song'></i></a></div>";
           content += "<div class='num-stat'>0</div>";
       }else{
           content += "<div class='ico-stat'><a href='javascript:void(0)' onclick='groupPostLikeProses("+'"'+data['group_post_id']+'"'+","+'"'+data['like']+'"'+");'><i class='fa fa-heart like-single-list-song'></i></a></div>";
           content += "<div class='num-stat'>"+data['like']+"</div>";
       }
       content += "</div>";
       content += "</div>";
   }
   
   content += "<div class='stat-song-track'>";
   content += "<div class='warp-ico-stat'>";
   content += "<div class='ico-stat'><a class='btn-share-ts'><i class='fa fa-mail-forward forward-single-list-song'></i></a></div>";
   content += "<div class='num-stat'>0</div>";
   content += "</div>";
   content += "</div>";
   content += "<div class='stat-song-track'>";
   // content += "<div class='warp-ico-stat'>";
   // content += "<div class='ico-stat'><a href='#'><i class='fa fa-magic spice-single-list-song'></i></a></div>";
   // content += "<div class='num-stat'>9999k</div>";
   // content += "</div>";
   content += "</div>";
   content += "</div>";
   content += "</div>";
   content += "</div>";
   content += "<div class='warp-all-comment-post'>";
   content += "<div class='tot-comment'><i class='fa fa-comment'></i><span>"+data['totalComment']+"</span> Comments</div>";
   content += "<div class='tb-more-comment ext-left'>";
   
   var totalHiddenComment = data['totalComment'] - 5;
   if(totalHiddenComment > 0){
       content += "<a id='readAllCommentGroup"+data['group_post_id']+"' onclick='readAllCommentGroup("+'"'+data['group_post_id']+'"'+","+'"'+data['myUserID']+'"'+","+'"'+data['myUsername']+'"'+","+'"'+data['myImageProfile']+'"'+","+'"'+data['statusGroupMember']+'"'+","+'"'+data['group_type']+'"'+");' class='more-txt'>read more comments</a>";
       content += "<div id='totalComment"+data['group_post_id']+"' class='num-comment'><span>"+totalHiddenComment+"</span> Comment hidden</div>";  
   }
   
   content += "</div>";

   var dataComment = data['dataComment'];
   
   // console.log('hasil: '+dataComment);
   content += "<div id='postGroupComment"+data['group_post_id']+"' style='display:block;'>";
   if(data['totalComment'] != 0 || data['totalComment'] != '0'){     
       var looping_comment = data['dataComment'].length;
     // console.log(looping_comment);
       for(var i= 0; i < looping_comment; i++){
           var dataComment = data['dataComment'][i];
           var profile_image_comment = dataComment['profile_image_comment'];
           var name_profile_image_comment = profile_image_comment.substring(profile_image_comment.lastIndexOf('/')+1);
           var picture_comment = folder+name_profile_image_comment;
           // console.log(profile_image_comment);

          var create_comment = dataComment['create_comment'];
          var time_comment = create_comment.split(/[- :]/);
          var day_comment = new Date(time_comment[0], time_comment[1]-1, time_comment[2], time_comment[3], time_comment[4], time_comment[5]); 

           content += "<div class='tb-comment'>";
           content += "<div class='hr-comment'></div>";
           content += "<div class='tb-pp'>";
           if(data['profile_image_comment'] === 0 ){
              content += "<a href='#'><img src='"+base_url+"/img/avatar/avatar-300x300.png' class='img-responsive img-rounded-full'></a>";
           }else{
               if(profile_image_comment != picture_comment){
                   content += "<a href='#'><img src='"+dataComment['profile_image_comment']+"' class='img-responsive img-rounded-full'></a>";
               }else{
                   content += "<a href='#'><img src='"+base_url+"/"+dataComment['profile_image_comment']+"' class='img-responsive img-rounded-full'></a>";
               }
           }
           content += "</div>";
           content += "<div class='tb-text-comment'>";
           content += "<div class='tb-info'>";
           content += "<h3><a href='#'>"+dataComment['username_comment']+"</a></h3>";
           content += "</div>";
           content += "<div class='tb-time'>";
           content += "<p>"+ getDateTimeSince(day_comment, false) +" ago</p>";
           content += "</div>";
           content += "<div class='tb-caption'>";
           content += "<p>";
           content += dataComment['comment'];
           // content += "<a href='#' class='more-txt'>more</a>";
           content += "</p>";
           content += "<div class='tb-like text-right'>";
           if(dataComment['statusLikeCommentGroupPost'] === 1 || dataComment['statusLikeCommentGroupPost'] === '1'){
                  content += "<a id='likedGroupCommentPost"+dataComment['group_post_comment_id']+"' href='javascript:void(0)' onclick='groupCommentPostUnlikeProses("+'"'+dataComment['group_post_comment_id']+'"'+","+'"'+dataComment['like']+'"'+")'>Unlike</a><span id='totalLikedCommentGroupPost"+dataComment['group_post_comment_id']+"'>"+dataComment['like']+"</span>";
           }else{
               if(dataComment['like'] === 'null' || dataComment['like'] === null || dataComment['like'] === '' || dataComment['like'] === 0 || dataComment['like'] === '0'){
                   content += "<a id='likedGroupCommentPost"+dataComment['group_post_comment_id']+"' href='javascript:void(0)' onclick='groupCommentPostLikeProses("+'"'+dataComment['group_post_comment_id']+'"'+","+'"0"'+")'>Like</a><span id='totalLikedCommentGroupPost"+dataComment['group_post_comment_id']+"'>0</span>";
               }else{
                   content += "<a id='likedGroupCommentPost"+dataComment['group_post_comment_id']+"' href='javascript:void(0)' onclick='groupCommentPostLikeProses("+'"'+dataComment['group_post_comment_id']+'"'+","+'"'+dataComment['like']+'"'+")'>Like</a><span id='totalLikedCommentGroupPost"+dataComment['group_post_comment_id']+"'>"+dataComment['like']+"</span>";
               }
           }
           
           content += "<a onclick='ShowTextAreaCommentReplayGroup("+'"'+dataComment['group_post_comment_id']+'"'+")' class='commentReplyPostGroup'>Reply</a>";
           content += "</div>";
           content += "</div>";
           content += "</div>";
           content += "<div class='comment-in-comment'>";
           if(dataComment['totalCommentReplay'] != 0 || dataComment['totalCommentReplay'] != '0'){
               // content += "<a id='seeSubKomen_1' href='javascript:toggleAndChangeText();' class='btn-expand-comment'>";
               content += "<a id='seeAllCommentReplayGroup"+dataComment['group_post_comment_id']+"' onclick='seeAllCommentReplayGroup("+'"'+data['group_post_id']+'"'+","+'"'+dataComment['group_post_comment_id']+'"'+","+'"'+data['myUserID']+'"'+","+'"'+data['myUsername']+'"'+","+'"'+data['myImageProfile']+'"'+")' class='btn-expand-comment' style='cursor:pointer'>";
               content += "See all comment";
               content += "</a>";
               content += "<div id='totalCommentReplay"+dataComment['group_post_comment_id']+"' class='num-comment'><span>"+dataComment['totalCommentReplay']+"</span> Comment hidden</div>";
           }
           
           content += "<div id='subKomen_1' class='warp-sub-comment sub-comment'>";
           content += "<div id='postGroupCommentReplay"+dataComment['group_post_comment_id']+"'>";

            content += "<div id='replayCommentForm"+dataComment['group_post_comment_id']+"' style='display:none;'>";
            content += "<div class='hr-comment'></div>";
            // content += "<form id='createCommentReply6480' onsubmit='return submitCommentReplyFunction(6480);' class='warp-comment-post'>";
            // content += "<form>";
            content += "<div class='col-lg-1 col-md-1 col-sm-1 col-xs-1 col-sxs-2 padten'>";
            content += "<div class='warp-user-comment-post'>";

            var myprofile_image = data['myImageProfile']
            var name_myprofile_image = myprofile_image.substring(myprofile_image.lastIndexOf('/')+1);
            var mypicture = folder+name_myprofile_image;
            if(data['myImageProfile'] === 0 ){
              content += "<a href='#'><img src='"+base_url+"/img/avatar/avatar-300x300.png' class='img-responsive img-rounded-full'></a>";
             }else{
                 if(myprofile_image != mypicture){
                     content += "<a href='#'><img src='"+data['myImageProfile']+"' class='img-responsive img-rounded-full'></a>";
                 }else{
                     content += "<a href='#'><img src='"+base_url+"/"+data['myImageProfile']+"' class='img-responsive img-rounded-full'></a>";
                 }
             }
            content += "</div>";
            content += "</div>";
            content += "<div class='col-lg-9 col-md-9 col-sm-9 col-xs-9 col-sxs-7 pad-xs-phone'>";
            content += "<textarea name='commentContentGroup' id='textAreaInput"+dataComment['group_post_comment_id']+"' class='form-control' placeholder='Write Comments ...'></textarea>";
            content += "</div>";
            content += "<div class='col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 related-md-desktop'>";

            if(data['group_type'] === 1 || data['group_type'] === '1'){
                 content += "<button onclick='replayCommentGroupProses("+'"'+data['group_post_id']+'"'+","+'"'+dataComment['group_post_comment_id']+'"'+", "+'"'+data['myUserID']+'"'+","+'"'+data['myUsername']+'"'+","+'"'+data['myImageProfile']+'"'+")' type='submit' class='btn btn-comment-post'>Comment</button>";
            }else{
                if(data['statusGroupMember'] === 'member'){
                     content += "<button onclick='replayCommentGroupProses("+'"'+data['group_post_id']+'"'+","+'"'+dataComment['group_post_comment_id']+'"'+", "+'"'+data['myUserID']+'"'+","+'"'+data['myUsername']+'"'+","+'"'+data['myImageProfile']+'"'+")' type='submit' class='btn btn-comment-post'>Comment</button>";
                }else{
                     content += "<button onclick='commentGroupPostHarusMember("+'"'+data['group_post_id']+'"'+");' type='submit' class='btn btn-comment-post'>Comment</button>";
                }
            }

            // content += "<button onclick='replayCommentGroupProses("+'"'+data['group_post_id']+'"'+","+'"'+dataComment['group_post_comment_id']+'"'+", "+'"'+data['myUserID']+'"'+","+'"'+data['myUsername']+'"'+","+'"'+data['myImageProfile']+'"'+")' type='submit' class='btn btn-comment-post'>Comment</button>";
            content += "<button type='button' class='btn btn-comment-post col-btn-red' onclick='deleteReplayCommentForm("+'"'+dataComment['group_post_comment_id']+'"'+")'>Cancel</button>";
            content += "</div>";
            // content += "</form>";
            content += "</div>";

           content += "</div>";     

           content += "</div>";
           content += "</div>";
           content += "</div>";  
      }
      
    }  

    content += "</div>"; 
   content += "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sxs-12'>";
   content += "<div class='row'>";
   content += "<div class='hr-comment'></div>";

   var myProfile_image = data['myImageProfile'];  
   var name_myProfile_image = myProfile_image.substring(myProfile_image.lastIndexOf('/')+1); // nama profile image
   var picture = folder+name_myProfile_image; 

   // content += "<form id='commentGroupPost"+data['group_post_id']+"' onsubmit='return commentGroupPost("+'"'+data['group_post_id']+'"'+", "+'"'+data['myUserID']+'"'+");' class='warp-comment-post'>";
   content += "<div class='warp-comment-post'>";
   
   content += "<div class='col-lg-1 col-md-1 col-sm-1 col-xs-1 col-sxs-2 padten'>";
   content += "<div class='warp-user-comment-post'>";
   if(data['profile_image_post'] == 0 ){
        content += "<img src='"+base_url+"/img/avatar/avatar-300x300.png' class='img-responsive img-rounded-full'>";
   }else{
       if(profile_image_post != picture){
           content += "<img src='"+data['myImageProfile']+"' class='img-responsive img-rounded-full'>";
       }else{
           content += "<img src='"+base_url+"/"+data['myImageProfile']+"' class='img-responsive img-rounded-full'>";
       }
   }
   content += "</div>";
   content += "</div>";
   content += "<div id='append-"+data['group_post_id']+"' class='col-lg-9 col-md-9 col-sm-9 col-xs-9 col-sxs-7 pad-xs-phone'>";
   content += "<textarea  id='fieldkomen-"+data['group_post_id']+"' name='komen' form='komengrup-"+data['group_post_id']+"' class='form-control' placeholder='Write Comments ...''></textarea>";
   content += "</div>";
   content += "<div class='col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 related-md-desktop'>";
   if(data['group_type'] === 1 || data['group_type'] === '1'){
       content += "<button onclick='commentGroupPost("+'"'+data['group_post_id']+'"'+", "+'"'+data['myUserID']+'"'+")' type='submit' class='btn btn-comment-post'>Comment</button>";
   }else{
        if(data['statusGroupMember'] === 'member'){
           content += "<button onclick='commentGroupPost("+'"'+data['group_post_id']+'"'+", "+'"'+data['myUserID']+'"'+")' type='submit' class='btn btn-comment-post'>Comment</button>";
        }else{
            content += "<button onclick='commentGroupPostHarusMember("+'"'+data['group_post_id']+'"'+");' type='submit' class='btn btn-comment-post'>Comment</button>";
        }
   }

   // content += "<button type='submit' class='btn btn-comment-post col-btn-red'>Cancel</button>";
   content += "</div>";
   // content += "</form>";
   content += "</div>";

   content += "</div>";
   content += "</div>";
   content += "</div>";
   content += "<br>";
   content += "<br>";

   return content;
}  

function getSinglePostGroup(){
    var url = window.location.pathname;
    // console.log(url);
    var groupID = url.substring(url.lastIndexOf('/')+1);
    var split = url.split('/');
    var groupPostID = split[3];
    // console.log(groupPostID);
    var data = '&groupID=' + groupID + '&groupPostID=' + groupPostID;
    var link = window.location.origin+"/singlePostGroup";
    // console.log(data);

    $.ajax({
        type : "POST",
        data : data,
        url : link,
        success : function(responseText){
          console.log('sukses');
          console.log(responseText);


           console.log('total comment :'+responseText[0].totalComment);

           var content = printSinglePostGroup(responseText[0]);
           $('#loadingSinglePostGroup').replaceWith(content);

        },
        error : function(){
           console.log('error');
        }
    });
}

function printAllCommentGroupPost(data,myUserID,myUsername,myImageProfile){
    var content = "";
     var looping_comment = data.length;
console.log(looping_comment);
    content += "<div id='postGroupComment"+data[0]['group_post_id']+"' style='display:block;'>";
       for(var i= 0; i < looping_comment; i++){
           var dataComment = data[i];
           console.log(dataComment);
           var profile_image_comment = dataComment['profile_image_comment'];
           var name_profile_image_comment = profile_image_comment.substring(profile_image_comment.lastIndexOf('/')+1);
           var folder = 'img/avatar/'; 
           var picture_comment = folder+name_profile_image_comment;
           console.log(profile_image_comment);

          var create_comment = dataComment['create_comment'];
          var time_comment = create_comment.split(/[- :]/);
          var day_comment = new Date(time_comment[0], time_comment[1]-1, time_comment[2], time_comment[3], time_comment[4], time_comment[5]); 
           
           content += "<div class='tb-comment'>";
           content += "<div class='hr-comment'></div>";
           content += "<div class='tb-pp'>";
           if(data['profile_image_comment'] === 0 ){
              content += "<a href='#'><img src='"+base_url+"/img/avatar/avatar-300x300.png' class='img-responsive img-rounded-full'></a>";
           }else{
               if(profile_image_comment != picture_comment){
                   content += "<a href='#'><img src='"+dataComment['profile_image_comment']+"' class='img-responsive img-rounded-full'></a>";
               }else{
                   content += "<a href='#'><img src='"+base_url+"/"+dataComment['profile_image_comment']+"' class='img-responsive img-rounded-full'></a>";
               }
           }
           content += "</div>";
           content += "<div class='tb-text-comment'>";
           content += "<div class='tb-info'>";
           content += "<h3><a href='#'>"+dataComment['username_comment']+"</a></h3>";
           content += "</div>";
           content += "<div class='tb-time'>";
           content += "<p>"+ getDateTimeSince(day_comment, false) +" ago</p>";
           content += "</div>";
           content += "<div class='tb-caption'>";
           content += "<p>";
           content += dataComment['comment'];
           // content += "<a href='#' class='more-txt'>more</a>";
           content += "</p>";
           content += "<div class='tb-like text-right'>";
           if(dataComment['statusLikeCommentGroupPost'] === 1 || dataComment['statusLikeCommentGroupPost'] === '1'){
                  content += "<a id='likedGroupCommentPost"+dataComment['group_post_comment_id']+"' href='javascript:void(0)' onclick='groupCommentPostUnlikeProses("+'"'+dataComment['group_post_comment_id']+'"'+","+'"'+dataComment['like']+'"'+")'>Unlike</a><span id='totalLikedCommentGroupPost"+dataComment['group_post_comment_id']+"'>"+dataComment['like']+"</span>";
           }else{
               if(dataComment['like'] === 'null' || dataComment['like'] === null || dataComment['like'] === '' || dataComment['like'] === 0 || dataComment['like'] === '0'){
                   content += "<a id='likedGroupCommentPost"+dataComment['group_post_comment_id']+"' href='javascript:void(0)' onclick='groupCommentPostLikeProses("+'"'+dataComment['group_post_comment_id']+'"'+","+'"0"'+")'>Like</a><span id='totalLikedCommentGroupPost"+dataComment['group_post_comment_id']+"'>0</span>";
               }else{
                   content += "<a id='likedGroupCommentPost"+dataComment['group_post_comment_id']+"' href='javascript:void(0)' onclick='groupCommentPostLikeProses("+'"'+dataComment['group_post_comment_id']+'"'+","+'"'+dataComment['like']+'"'+")'>Like</a><span id='totalLikedCommentGroupPost"+dataComment['group_post_comment_id']+"'>"+dataComment['like']+"</span>";
               }
           }
           content += "<a onclick='ShowTextAreaCommentReplayGroup("+'"'+dataComment['group_post_comment_id']+'"'+")' class='commentReplyPostGroup'>Reply</a>";
           content += "</div>";
           content += "</div>";
           content += "</div>";
           content += "<div class='comment-in-comment'>";
           if(dataComment['totalCommentReplay'] != 0 || dataComment['totalCommentReplay'] != '0'){
               // content += "<a id='seeSubKomen_1' href='javascript:toggleAndChangeText();' class='btn-expand-comment'>";
               content += "<a id='seeAllCommentReplayGroup"+dataComment['group_post_comment_id']+"' onclick='seeAllCommentReplayGroup("+'"'+dataComment['group_post_comment_id']+'"'+","+'"'+dataComment['group_post_comment_id']+'"'+","+'"'+myUserID+'"'+","+'"'+myUsername+'"'+","+'"'+myImageProfile+'"'+")' class='btn-expand-comment' style='cursor:pointer'>";
               content += "See all comment";
               content += "</a>";
               content += "<div id='totalCommentReplay"+dataComment['group_post_comment_id']+"' class='num-comment'><span>"+dataComment['totalCommentReplay']+"</span> Comment hidden</div>";
           }
           
           content += "<div id='subKomen_1' class='warp-sub-comment sub-comment'>";
           content += "<div id='postGroupCommentReplay"+dataComment['group_post_comment_id']+"'>";

            content += "<div id='replayCommentForm"+dataComment['group_post_comment_id']+"' style='display:none;'>";
            content += "<div class='hr-comment'></div>";
            // content += "<form id='createCommentReply6480' onsubmit='return submitCommentReplyFunction(6480);' class='warp-comment-post'>";
            // content += "<form>";
            content += "<div class='col-lg-1 col-md-1 col-sm-1 col-xs-1 col-sxs-2 padten'>";
            content += "<div class='warp-user-comment-post'>";

            var myprofile_image = myImageProfile
            var name_myprofile_image = myprofile_image.substring(myprofile_image.lastIndexOf('/')+1);
            var mypicture = folder+name_myprofile_image;
            if(myImageProfile === 0 ){
              content += "<a href='#'><img src='"+base_url+"/img/avatar/avatar-300x300.png' class='img-responsive img-rounded-full'></a>";
             }else{
                 if(myprofile_image != mypicture){
                     content += "<a href='#'><img src='"+myImageProfile+"' class='img-responsive img-rounded-full'></a>";
                 }else{
                     content += "<a href='#'><img src='"+base_url+"/"+myImageProfile+"' class='img-responsive img-rounded-full'></a>";
                 }
             }
            content += "</div>";
            content += "</div>";
            content += "<div class='col-lg-9 col-md-9 col-sm-9 col-xs-9 col-sxs-7 pad-xs-phone'>";
            content += "<textarea name='commentContentGroup' id='textAreaInput"+dataComment['group_post_comment_id']+"' class='form-control' placeholder='Write Comments ...'></textarea>";
            content += "</div>";
            content += "<div class='col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 related-md-desktop'>";
            
            if(data['group_type'] === 1 || data['group_type'] === '1'){
                 content += "<button onclick='replayCommentGroupProses("+'"'+data['group_post_id']+'"'+","+'"'+dataComment['group_post_comment_id']+'"'+", "+'"'+data['myUserID']+'"'+","+'"'+data['myUsername']+'"'+","+'"'+data['myImageProfile']+'"'+")' type='submit' class='btn btn-comment-post'>Comment</button>";
            }else{
                if(data['statusGroupMember'] === 'member'){
                     content += "<button onclick='replayCommentGroupProses("+'"'+data['group_post_id']+'"'+","+'"'+dataComment['group_post_comment_id']+'"'+", "+'"'+data['myUserID']+'"'+","+'"'+data['myUsername']+'"'+","+'"'+data['myImageProfile']+'"'+")' type='submit' class='btn btn-comment-post'>Comment</button>";
                }else{
                     content += "<button onclick='commentGroupPostHarusMember("+'"'+data['group_post_id']+'"'+");' type='submit' class='btn btn-comment-post'>Comment</button>";
                }
            }

            // content += "<button onclick='replayCommentGroupProses("+'"'+dataComment['group_post_id']+'"'+","+'"'+dataComment['group_post_comment_id']+'"'+", "+'"'+myUserID+'"'+","+'"'+myUsername+'"'+","+'"'+myImageProfile+'"'+")' type='submit' class='btn btn-comment-post'>Comment</button>";
            content += "<button type='button' class='btn btn-comment-post col-btn-red' onclick='deleteReplayCommentForm("+'"'+dataComment['group_post_comment_id']+'"'+")'>Cancel</button>";
            content += "</div>";
            // content += "</form>";
            content += "</div>";

           content += "</div>";    

           content += "</div>";
           content += "</div>";
           content += "</div>";  
      }
      content += "</div>";
      return content;
}

function readAllCommentGroup(groupPostID,myUserID,myUsername,myImageProfile){
    var data = '&groupPostID=' + groupPostID;
    var link = window.location.origin+"/readAllCommentGroupPost";
    $.ajax({
        type : "POST",
        url : link,
        data : data,
        success : function(responseText){
          console.log(responseText);     
           var replaceContent = "<a id='hideAllCommentGroup"+groupPostID+"' onclick='hideAllCommentGroup("+'"'+groupPostID+'"'+","+'"'+myUserID+'"'+","+'"'+myUsername+'"'+","+'"'+myImageProfile+'"'+");' class='more-txt'>hide more comments</a>";
           $('#readAllCommentGroup'+groupPostID).replaceWith(replaceContent); 
           $('#totalComment'+groupPostID).hide();       
            var content = printAllCommentGroupPost(responseText,myUserID,myUsername,myImageProfile);
            $('#postGroupComment'+groupPostID).replaceWith(content);
        },

        error : function(){
            console.log('error');
        }
    });

}

function hideAllCommentGroup(groupPostID,myUserID,myUsername,myImageProfile){
  var data = '&groupPostID=' + groupPostID;
  var link = window.location.origin+"/hideAllCommentGroupPost";
  $.ajax({
      type : "POST",
      url : link,
      data : data,
      success : function(responseText){
          console.log(responseText);
          var replaceContent = "<a id='readAllCommentGroup"+groupPostID+"' onclick='readAllCommentGroup("+'"'+groupPostID+'"'+","+'"'+myUserID+'"'+","+'"'+myUsername+'"'+","+'"'+myImageProfile+'"'+");' class='more-txt'>read more comments</a>";
          $('#hideAllCommentGroup'+groupPostID).replaceWith(replaceContent); 
           $('#totalComment'+groupPostID).show();  
          var content = printAllCommentGroupPost(responseText,myUserID,myUsername,myImageProfile);
          $('#postGroupComment'+groupPostID).replaceWith(content);

      },
      error : function(){
        console.log('error');
      }

  });

}

function printShowCommentReplayPostGroup(group_post_id,group_post_comment_id, data,myUserID,myUsername,myImageProfile){
   var base_url = window.location.origin;
   var profile_image_comment_replay = data['profile_image_comment_replay'];
   var name_profile_image_comment_replay = profile_image_comment_replay.substring(profile_image_comment_replay.lastIndexOf('/')+1); // nama profile image
   var folder = 'img/avatar/';  // folder penyimpanan profile image
   var picture = folder+name_profile_image_comment_replay; // l

   var create_post = data['create'];
   var t = create_post.split(/[- :]/);
   var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]); 

     var content = "";

     content += "<div class='tb-comment'>";
     content += "<div class='hr-comment'></div>";
     content += "<div class='tb-pp'>";
     if(data['profile_image_comment_replay'] == 0 ){
        content += "<a href='#'><img src='"+base_url+"/img/avatar/avatar-300x300.png' class='img-responsive img-rounded-full'></a>";
     }else{
         if(profile_image_comment_replay != picture){
             content += "<a href='#'><img src='"+data['profile_image_comment_replay']+"' class='img-responsive img-rounded-full'></a>";
         }else{
             content += "<a href='#'><img src='"+base_url+"/"+data['profile_image_comment_replay']+"' class='img-responsive img-rounded-full'></a>";
         }
     }
     content += "</div>";
     content += "<div class='tb-text-comment'>";
     content += "<div class='tb-info'>";
     content += "<h3><a href='#'>"+data['username_commentReplay']+"</a></h3>";
     content += "</div>";
     content += "<div class='tb-time'>";
     content += "<p>"+ getDateTimeSince(d, false) +" ago</p>";
     content += "</div>";
     content += "<div class='tb-caption'>";
     content += "<p>"+data['contents']+"";
     // content += "<a href='#'' class='more-txt'>more</a>";
     content += "</p>";
     content += "<div class='tb-like text-right'>";
     // content += "<a href='#'>Like</a><span>12</span>";
     // content += "<a href='#'>Reply</a>";
     content += "</div>";
     content += "</div>";
     content += "</div>";
     content += "</div>";

     return content;
}

function printReplaceSeeAllCommentReplayGroup(group_post_id,group_post_comment_id,myUserID,myUsername,myImageProfile,statusGroupMember,group_type){
    var content = "";
    content += "<a id='hideAllCommentReplayGroup"+group_post_comment_id+"' onclick='hideAllCommentReplayGroup("+'"'+group_post_id+'"'+","+'"'+group_post_comment_id+'"'+","+'"'+myUserID+'"'+","+'"'+myUsername+'"'+","+'"'+myImageProfile+'"'+","+'"'+statusGroupMember+'"'+","+'"'+group_type+'"'+")' class='btn-expand-comment' style='cursor:pointer'>";
    content += "Hide all comment";
    content += "</a>";
    return content;
}

function printReplaceHideAllCommentReplayGroup(group_post_id,group_post_comment_id,myUserID,myUsername,myImageProfile,statusGroupMember,group_type){
    var content = "";
    content += "<a id='seeAllCommentReplayGroup"+group_post_comment_id+"' onclick='seeAllCommentReplayGroup("+'"'+group_post_id+'"'+","+'"'+group_post_comment_id+'"'+","+'"'+myUserID+'"'+","+'"'+myUsername+'"'+","+'"'+myImageProfile+'"'+","+'"'+statusGroupMember+'"'+","+'"'+group_type+'"'+")' class='btn-expand-comment' style='cursor:pointer'>";
    content += "See all comment";
    content += "</a>";
    return content;
}

function seeAllCommentReplayGroup(group_post_id,group_post_comment_id,myUserID,myUsername,myImageProfile,statusGroupMember,group_type){
  console.log(group_post_comment_id);

  $('.commentReplyPostGroup').show();
  var data = '&group_post_comment_id=' + group_post_comment_id;
  var link = window.location.origin+"/seeAllCommentReplayGroup";
    $.ajax({
        type : "POST",
        data : {group_post_comment_id:group_post_comment_id},
        url : link,
        success : function(responseText){
           console.log(responseText);
           var contentData = printReplaceSeeAllCommentReplayGroup(group_post_id,group_post_comment_id,myUserID,myUsername,myImageProfile);
           $('#seeAllCommentReplayGroup'+group_post_comment_id).replaceWith(contentData);
           $('#totalCommentReplay'+group_post_comment_id).hide();
           var content = "";
           content += "<div id='postGroupCommentReplay"+group_post_comment_id+"'>";
           for(var i= 0; i < responseText.length; i++ ){
              content += printShowCommentReplayPostGroup(group_post_id,group_post_comment_id,responseText[i],myUserID,myUsername,myImageProfile);
             
           }
           content += "";

            content += "<div id='replayCommentForm"+group_post_comment_id+"' style='display:none;'>";
            content += "<div class='tb-comment'>";
            content += "<div class='hr-comment'></div>";
            content += "</div>";
            // content += "<form id='createCommentReply6480' onsubmit='return submitCommentReplyFunction(6480);' class='warp-comment-post'>";
            // content += "<form>";
            content += "<div class='col-lg-1 col-md-1 col-sm-1 col-xs-1 col-sxs-2 padten'>";
            content += "<div class='warp-user-comment-post'>";

            var myprofile_image = myImageProfile;
            var name_myprofile_image = myprofile_image.substring(myprofile_image.lastIndexOf('/')+1);
            var folder = 'img/avatar/';
            var mypicture = folder+name_myprofile_image;
            if(myImageProfile === 0 ){
              content += "<a href='#'><img src='"+base_url+"/img/avatar/avatar-300x300.png' class='img-responsive img-rounded-full'></a>";
             }else{
                 if(myprofile_image != mypicture){
                     content += "<a href='#'><img src='"+myImageProfile+"' class='img-responsive img-rounded-full'></a>";
                 }else{
                     content += "<a href='#'><img src='"+base_url+"/"+myImageProfile+"' class='img-responsive img-rounded-full'></a>";
                 }
             }

            content += "</div>";
            content += "</div>";
            content += "<div class='col-lg-9 col-md-9 col-sm-9 col-xs-9 col-sxs-7 pad-xs-phone'>";
            content += "<textarea name='commentContentGroup' id='textAreaInput"+group_post_comment_id+"' class='form-control' placeholder='Write Comments ...'></textarea>";
            content += "</div>";
            content += "<div class='col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 related-md-desktop'>";

            if(group_type === 1 || group_type === '1'){
                 content += "<button onclick='replayCommentGroupProses("+'"'+group_post_id+'"'+","+'"'+group_post_comment_id+'"'+", "+'"'+myUserID+'"'+","+'"'+myUsername+'"'+","+'"'+myImageProfile+'"'+")' type='submit' class='btn btn-comment-post'>Comment</button>";
            }else{
                if(statusGroupMember === 'member'){
                     content += "<button onclick='replayCommentGroupProses("+'"'+group_post_id+'"'+","+'"'+group_post_comment_id+'"'+", "+'"'+myUserID+'"'+","+'"'+myUsername+'"'+","+'"'+myImageProfile+'"'+")' type='submit' class='btn btn-comment-post'>Comment</button>";
                }else{
                     content += "<button onclick='commentGroupPostHarusMember("+'"'+group_post_id+'"'+");' type='submit' class='btn btn-comment-post'>Comment</button>";
                }
            }

            content += "<button type='button' class='btn btn-comment-post col-btn-red' onclick='deleteReplayCommentForm("+'"'+group_post_comment_id+'"'+")'>Cancel</button>";
            content += "</div>";
            // content += "</form>";
            content += "</div>";
           $('#postGroupCommentReplay'+group_post_comment_id).replaceWith(content);
        },
        error : function(){
          console.log('error');
        }

    });
}

function hideAllCommentReplayGroup(group_post_id,group_post_comment_id,myUserID,myUsername,myImageProfile,statusGroupMember,group_type){
    console.log(group_post_comment_id);

    $('.commentReplyPostGroup').show();
    var contentData = printReplaceHideAllCommentReplayGroup(group_post_id,group_post_comment_id,myUserID,myUsername,myImageProfile);
    $('#hideAllCommentReplayGroup'+group_post_comment_id).replaceWith(contentData);
    $('#totalCommentReplay'+group_post_comment_id).show();
    
    var content = "";
    content += "<div id='postGroupCommentReplay"+group_post_comment_id+"'>";
    
    content += "<div id='replayCommentForm"+group_post_comment_id+"' style='display:none;'>";
    content += "<div class='hr-comment'></div>";
    content += "<div class='col-lg-1 col-md-1 col-sm-1 col-xs-1 col-sxs-2 padten'>";
    content += "<div class='warp-user-comment-post'>";

    var myprofile_image = myImageProfile;
    var name_myprofile_image = myprofile_image.substring(myprofile_image.lastIndexOf('/')+1);
    var folder = 'img/avatar/';
    var mypicture = folder+name_myprofile_image;
    if(myImageProfile === 0 ){
      content += "<a href='#'><img src='"+base_url+"/img/avatar/avatar-300x300.png' class='img-responsive img-rounded-full'></a>";
     }else{
         if(myprofile_image != mypicture){
             content += "<a href='#'><img src='"+myImageProfile+"' class='img-responsive img-rounded-full'></a>";
         }else{
             content += "<a href='#'><img src='"+base_url+"/"+myImageProfile+"' class='img-responsive img-rounded-full'></a>";
         }
     }

    content += "</div>";
    content += "</div>";
    content += "<div class='col-lg-9 col-md-9 col-sm-9 col-xs-9 col-sxs-7 pad-xs-phone'>";
    content += "<textarea name='commentContentGroup' id='textAreaInput"+group_post_comment_id+"' class='form-control' placeholder='Write Comments ...'></textarea>";
    content += "</div>";
    content += "<div class='col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 related-md-desktop'>";
    
    if(group_type === 1 || group_type === '1'){
        content += "<button onclick='replayCommentGroupProses("+'"'+group_post_id+'"'+","+'"'+group_post_comment_id+'"'+", "+'"'+myUserID+'"'+","+'"'+myUsername+'"'+","+'"'+myImageProfile+'"'+")' type='submit' class='btn btn-comment-post'>Comment</button>";
    }else{
        if(statusGroupMember === 'member'){
            content += "<button onclick='replayCommentGroupProses("+'"'+group_post_id+'"'+","+'"'+group_post_comment_id+'"'+", "+'"'+myUserID+'"'+","+'"'+myUsername+'"'+","+'"'+myImageProfile+'"'+")' type='submit' class='btn btn-comment-post'>Comment</button>";
        }else{
            content += "<button onclick='commentGroupPostHarusMember("+'"'+group_post_id+'"'+");' type='submit' class='btn btn-comment-post'>Comment</button>";
        }
    }

    content += "<button type='button' class='btn btn-comment-post col-btn-red' onclick='deleteReplayCommentForm("+'"'+group_post_comment_id+'"'+")'>Cancel</button>";
    content += "</div>";
    content += "</div>";
    $('#postGroupCommentReplay'+group_post_comment_id).replaceWith(content);

}  

function commentGroupPost(group_post_id,myUserID){
   console.log(group_post_id);
   console.log(myUserID);
   var comment = $('#fieldkomen-'+group_post_id).val();
   if(comment.length === 0 || comment === ''){
      swal({
          title: 'Sorry...',
          text: 'write comments, please...',
          timer: 2000
      });
   }else{
     console.log(comment);
     var data =  '&post_id=' +group_post_id + '&user_id=' +myUserID + '&komen=' +comment;
     var link = window.location.origin+"/komenpostgrup";

     $.ajax({
         type : "POST",
         data : data,
         url : link,
         success : function(responseText){
            console.log(responseText);
            var content = printNewCommentGroupPost(responseText,group_post_id);
            $('#postGroupComment'+group_post_id).append(content);
            $('#fieldkomen-'+group_post_id).val('');
         },
         error : function(){
          console.log('error');
         }

     });   
     $('#fieldkomen-'+group_post_id).val('');
  }
  
}

function printNewCommentGroupPost(dataComment,group_post_id){
  var base_url = window.location.origin;
  
   var content = "";

       var profile_image_comment = dataComment['profile_image'];
       var name_profile_image_comment = profile_image_comment.substring(profile_image_comment.lastIndexOf('/')+1);
       var folder = 'img/avatar/'; 
       var picture_comment = folder+name_profile_image_comment;
       console.log(profile_image_comment);

      var create_comment = dataComment['created_at'];
      var time_comment = create_comment.split(/[- :]/);
      var day_comment = new Date(time_comment[0], time_comment[1]-1, time_comment[2], time_comment[3], time_comment[4], time_comment[5]); 
       
       content += "<div class='tb-comment'>";
       content += "<div class='hr-comment'></div>";
       content += "<div class='tb-pp'>";
       if(dataComment['profile_image'] === 0 ){
          content += "<a href='#'><img src='"+base_url+"/img/avatar/avatar-300x300.png' class='img-responsive img-rounded-full'></a>";
       }else{
           if(profile_image_comment != picture_comment){
               content += "<a href='#'><img src='"+dataComment['profile_image']+"' class='img-responsive img-rounded-full'></a>";
           }else{
               content += "<a href='#'><img src='"+base_url+"/"+dataComment['profile_image']+"' class='img-responsive img-rounded-full'></a>";
           }
       }
       content += "</div>";
       content += "<div class='tb-text-comment'>";
       content += "<div class='tb-info'>";
       content += "<h3><a href='#'>"+dataComment['username']+"</a></h3>";
       content += "</div>";
       content += "<div class='tb-time'>";
       content += "<p>"+ getDateTimeSince(day_comment, false) +" ago</p>";
       content += "</div>";
       content += "<div class='tb-caption'>";
       content += "<p>";
       content += dataComment['komen'];
       // content += "<a href='#' class='more-txt'>more</a>";
       content += "</p>";
       content += "<div class='tb-like text-right'>";   
       content += "<a id='likedGroupCommentPost"+dataComment['comment_id']+"' href='javascript:void(0)' onclick='groupCommentPostLikeProses("+'"'+dataComment['comment_id']+'"'+","+'"0"'+")'>Like</a><span id='totalLikedCommentGroupPost"+dataComment['comment_id']+"'>0</span>";
       
       content += "<a onclick='ShowTextAreaCommentReplayGroup("+'"'+dataComment['comment_id']+'"'+")' class='commentReplyPostGroup'>Reply</a>";
       content += "</div>";
       content += "</div>";
       content += "</div>";
       content += "<div class='comment-in-comment'>"; 

        content += "<div id='subKomen_1' class='warp-sub-comment sub-comment'>";
        content += "<div id='postGroupCommentReplay"+dataComment['comment_id']+"'>";
        content += "<div id='replayCommentForm"+dataComment['comment_id']+"' style='display:none;'>";

        content += "<div class='tb-comment'>";
        content += "<div class='hr-comment'></div>";
        content += "</div>";
        content += "<div class='col-lg-1 col-md-1 col-sm-1 col-xs-1 col-sxs-2 padten'>";
        content += "<div class='warp-user-comment-post'>";

        if(dataComment['profile_image'] === 0 ){
            content += "<a href='#'><img src='"+base_url+"/img/avatar/avatar-300x300.png' class='img-responsive img-rounded-full'></a>";
        }else{
            if(profile_image_comment != picture_comment){
                content += "<a href='#'><img src='"+dataComment['profile_image']+"' class='img-responsive img-rounded-full'></a>";
            }else{
                content += "<a href='#'><img src='"+base_url+"/"+dataComment['profile_image']+"' class='img-responsive img-rounded-full'></a>";
            }
        }
            
        content += "</div>";
        content += "</div>";
        content += "<div class='col-lg-9 col-md-9 col-sm-9 col-xs-9 col-sxs-7 pad-xs-phone'>";
        content += "<textarea name='commentContentGroup' id='textAreaInput"+dataComment['comment_id']+"' class='form-control' placeholder='Write Comments ...'></textarea>";
        content += "</div>";
        content += "<div class='col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 related-md-desktop'>";
        content += "<button onclick='replayCommentGroupProses("+'"'+group_post_id+'"'+","+'"'+dataComment['comment_id']+'"'+", "+'"'+dataComment['myUserID']+'"'+","+'"'+dataComment['username']+'"'+","+'"'+dataComment['profile_image']+'"'+")' type='submit' class='btn btn-comment-post'>Comment</button>";
        content += "<button type='button' class='btn btn-comment-post col-btn-red' onclick='deleteReplayCommentForm("+'"'+dataComment['comment_id']+'"'+")'>Cancel</button>";
        content += "</div>";
        content += "</div>";
        content += "</div>";     
        content += "</div>";
       content += "</div>";
       content += "</div>";
       content += "</div>";  
       return content;
}


function ShowTextAreaCommentReplayGroup(group_post_comment_id){
  console.log(group_post_comment_id);
  $('#textAreaInput'+group_post_comment_id).val('');
  $('.commentReplyPostGroup').hide();
  $('#replayCommentForm'+group_post_comment_id).show();
  $('#textAreaInput'+group_post_comment_id).focus();
}

function deleteReplayCommentForm(group_post_comment_id){
  $('.commentReplyPostGroup').show();
  $('#replayCommentForm'+group_post_comment_id).hide();
  $('#textAreaInput'+group_post_comment_id).val('');
}

function replayCommentGroupProses(group_post_id,group_post_comment_id,myUserID,myUsername,myImageProfile){
  console.log(group_post_comment_id);
  console.log(myUserID);
  var replayComment = $('#textAreaInput'+group_post_comment_id).val();
  console.log(replayComment);
  var data =  '&post_id=' + group_post_id + '&comment_id=' + group_post_comment_id + '&komen=' +replayComment;
  console.log(data);
  
  var link = window.location.origin+"/submitkomenreplygrup";
  if(replayComment.length === 0 || replayComment === ''){
      swal({
          title: 'Sorry...',
          text: 'write comments, please...',
          timer: 2000
      });
  }else{
      $.ajax({
           type : "POST",
           data : data,
           url : link,
           success : function(responseText){
               console.log(responseText);
               var data = responseText['newComment'];
               $('#replayCommentForm'+group_post_comment_id).hide();
               var content = printNewCommentReplayPostGroup(data,group_post_comment_id,myUserID,myUsername,myImageProfile);
               // $('#postGroupCommentReplay'+group_post_comment_id).append(content);
               $('#textAreaInput'+group_post_comment_id).val('');
               $('#replayCommentForm'+group_post_comment_id).before(content);
           },
           error : function(){
              console.log('error');
           }
      });
      
      $('.commentReplyPostGroup').show();
  }
}

function printNewCommentReplayPostGroup(data,group_post_comment_id,myUserID,myUsername,myImageProfile){
   var base_url = window.location.origin;
   var profile_image_comment_replay = myImageProfile;
   var name_profile_image_comment_replay = profile_image_comment_replay.substring(profile_image_comment_replay.lastIndexOf('/')+1); // nama profile image
   var folder = 'img/avatar/';  // folder penyimpanan profile image
   var picture = folder+name_profile_image_comment_replay; // l

   var create = data['created_at'];
   var t = create.split(/[- :]/);
   var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]); 

     var content = "";
     content += "<div id='postGroupCommentReplay"+group_post_comment_id+"'>";

     content += "<div class='tb-comment'>";
     content += "<div class='hr-comment'></div>";
     content += "<div class='tb-pp'>";
     if(myImageProfile == 0 ){
        content += "<a href='#'><img src='"+base_url+"/img/avatar/avatar-300x300.png' class='img-responsive img-rounded-full'></a>";
     }else{
         if(profile_image_comment_replay != picture){
             content += "<a href='#'><img src='"+myImageProfile+"' class='img-responsive img-rounded-full'></a>";
         }else{
             content += "<a href='#'><img src='"+base_url+"/"+myImageProfile+"' class='img-responsive img-rounded-full'></a>";
         }
     }
     content += "</div>";
     content += "<div class='tb-text-comment'>";
     content += "<div class='tb-info'>";
     content += "<h3><a href='#'>"+myUsername+"</a></h3>";
     content += "</div>";
     content += "<div class='tb-time'>";
     content += "<p>"+ getDateTimeSince(d, false) +" ago</p>";
     content += "</div>";
     content += "<div class='tb-caption'>";
     content += "<p>"+data['contents']+"";
     // content += "<a href='#'' class='more-txt'>more</a>";
     content += "</p>";
     content += "<div class='tb-like text-right'>";
     
     content += "</div>";
     content += "</div>";
     content += "</div>";
     content += "</div>";

     content += "</div>"; 

     return content;
}

function groupPostLikeProses(group_post_id, totalLike){
  console.log(totalLike);
  var a = 1;
  var likeTotal = parseInt(totalLike)+parseInt(a);

  var content = replaceGroupPostLikeProses(group_post_id, likeTotal)
  $('#statusLikePostGroup'+group_post_id).replaceWith(content);
  var link = window.location.origin+"/groupPostLikeProses";
  var data = '&group_post_id=' + group_post_id + '&like_total=' + likeTotal;
console.log(data);
console.log(link);
    $.ajax({
       type : "POST",
       data : data,
       url : link,
        success : function(responseText){
           console.log(responseText)
        },
        error : function(){
          console.log('error');
        }

    });
}

function replaceGroupPostLikeProses(group_post_id,likeTotal)     {
     var content = "";
     content += "<div class='stat-song-track' id='statusLikePostGroup"+group_post_id+"'>";
     content += "<div class='warp-ico-stat'>";
     content += "<div class='ico-stat'><a href='javascript:void(0)' onclick='udahLikePost();'><i class='fa fa-heart like-single-list-song' style='color:orange;'></i></a></div>";
     content += "<div class='num-stat'>"+likeTotal+"</div>";
     content += "</div>";
     content += "</div>";
     return content;
}

function groupCommentPostLikeProses(group_post_comment_id,totalLike){
   console.log(totalLike);
   var a = 1;
   var likeTotal = parseInt(totalLike)+parseInt(a);
   var content = repalceGroupCommentPostLikeProses(group_post_comment_id,likeTotal);
   $('#likedGroupCommentPost'+group_post_comment_id).replaceWith(content);
   var dataContent = "<span id='totalLikedCommentGroupPost"+group_post_comment_id+"'>"+likeTotal+"</span>";
   $('#totalLikedCommentGroupPost'+group_post_comment_id).replaceWith(dataContent);

   var link = window.location.origin+"/groupCommentPostLikeProses";
   var data = '&group_post_comment_id=' + group_post_comment_id;
   console.log(data);
   $.ajax({
        type : "POST",
        data : data,
        url : link,
        success : function(responseText){
            console.log(responseText);
        },
        error : function(){
           console.log('error');
        }
   });
}

function repalceGroupCommentPostLikeProses(group_post_comment_id,likeTotal){
   var content = "";
    content += "<a id='likedGroupCommentPost"+group_post_comment_id+"' href='javascript:void(0)' onclick='groupCommentPostUnlikeProses("+'"'+group_post_comment_id+'"'+","+'"'+likeTotal+'"'+")'>Unlike</a>";
    return content;
}

function groupCommentPostUnlikeProses(group_post_comment_id,totalLike){
    console.log(totalLike);
    var a = 1;
    var likeTotal = parseInt(totalLike)-parseInt(a);
    var content = repalceGroupCommentPostUnlikeProses(group_post_comment_id,likeTotal);
    $('#likedGroupCommentPost'+group_post_comment_id).replaceWith(content);
    var dataContent = "<span id='totalLikedCommentGroupPost"+group_post_comment_id+"'>"+likeTotal+"</span>";
    $('#totalLikedCommentGroupPost'+group_post_comment_id).replaceWith(dataContent);

    var link = window.location.origin+"/groupCommentPostUnlikeProses";
    var data = '&group_post_comment_id=' + group_post_comment_id;
     console.log(data);
     $.ajax({
          type : "POST",
          data : data,
          url : link,
          success : function(responseText){
              console.log(responseText);
          },
          error : function(){
             console.log('error');
          }
     });
}

function repalceGroupCommentPostUnlikeProses(group_post_comment_id,likeTotal){
  var content = "";
  content += "<a id='likedGroupCommentPost"+group_post_comment_id+"' href='javascript:void(0)' onclick='groupCommentPostLikeProses("+'"'+group_post_comment_id+'"'+","+'"'+likeTotal+'"'+")'>Like</a>";
  return content;
}

function commentGroupPostHarusMember(group_post_id){
  $('#fieldkomen-'+group_post_id).val('');
  swal({
    title: 'Sorry, this is private group',
    text: 'you must join group if you are post comment',
    timer: 2000
  });
}