var errors;

var countSongEvent;
var skipSongEvent;
var takeSongEvent;

var countParticipateNotReleaseSong;
var skipParticipateNotReleaseSong;
var takeParticipateNotReleaseSong;

var base_url = window.location.origin;
function setValueDetailsEvent(){
	this.countSongEvent;
	this.takeSongEvent = 12;
	this.skipSongEvent = 0;

  this.countParticipateNotReleaseSong;
  this.takeParticipateNotReleaseSong = 12;
  this.skipParticipateNotReleaseSong = 0;
}

function cekElementDetailsEvent(){
	var elementExists = new Array(
		 document.getElementById('loadingDetailsEvent1'),
     document.getElementById('loadingDetailsEvent2'),
     document.getElementById('loadingDetailsEvent3'),
     document.getElementById('loadingDetailsEvent4')
	 );
	var stop = false;
	for (var i = elementExists.length - 1; i >= 0; i--) {
      if(elementExists[i] == null)
        stop = true;
    };

    if(stop)
      return 0;
    return 1;
}

$(document).on('ready pjax:success', function(){
	var detailsEvent = cekElementDetailsEvent();
	if(detailsEvent == 0) return;

    setValueDetailsEvent();
	  getDetailsEventPrint(1);
	  getDetailsEventPrint(2);
    getDetailsEventPrint(3);
    getDetailsEventPrint(4);
});

function setTakeTotalDetailsEvent(total,type){
    switch(type){
      case 1:
        break;
      case 2:
        var skip = this.skipSongEvent;
        var take = this.takeSongEvent;
        break;
      case 3:
        break;
      case 4:
        var skip = this.skipParticipateNotReleaseSong;
        var take = this.takeParticipateNotReleaseSong;   
        break;
      default:
        this.errors = true;
        break;  
    }
    if(this.errors == true){
      return 0;
    }

    skip = skip + take;
    if(skip + take > total){
       take = total - skip + 1;
    }

    switch(type){
      case 1: 
         break;
      case 2:
         this.skipSongEvent = skip;
         this.takeSongEvent = take;
         this.countSongEvent = total;
         break;
      case 3:
         break;
      case 4:
         this.skipParticipateNotReleaseSong = skip;
         this.takeParticipateNotReleaseSong = take;
         this.countParticipateNotReleaseSong = total;    
         break;  
      default:
         this.errors = true; 
         break;     
    }

    if(this.errors == true)
    return 0;
}

function setLinkDetailsEvent(type){
	var content;
  switch(type){
      case 1: 
         break;
      case 2:
         if(this.skipSongEvent < this.countSongEvent){
           content = LoadMoreDetailsEvent(2);
           $('#listSongEventRelease').append(content);
         }
         break;
      case 3:
        break;
      case 4:
        if(this.skipParticipateNotReleaseSong < this.countParticipateNotReleaseSong){
          content = LoadMoreDetailsEvent(4);
          $('#listParticipantUserNotReleaseSong').append(content);
        }
        break;     
      default:
        this.errors = true;
        break;      
  }

}

function LoadMoreDetailsEvent(type){
    var content = "";
    if(type != 1){
      content += "<div class='warp-btn-loadmore-list' id='moreSongEventRelease"+type+"'>";
      content += "<a class='btn-loadmore-list' onclick='getDetailsEvent("+type+")'>";
      content += "<i class='fa fa-plus-circle'></i>";
      content += "<span class='text-load-more-list'>LOAD MORE</span>";
      content += "</a>";
      content += "</div>";
    }
    return content;
}

function loadingGifFileDetailsEvent(type){
  var content = "";
  content += "<div class='col-xs-push-5 col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sxs-2 lazyloader-list' id='loadingDetailsEvent"+type+"'>";
  content += "<img src='"+base_url+"/img/loader-more.gif' class='img-full-responsive e-centering width-loader'>";
  content += "</div>";
  return content;
}

 function resetLoadingGifDetailsEvent(type){
    var content = loadingGifFileDetailsEvent(type);
    $("#moreSongEventRelease"+type).replaceWith(content);
  }

// mencetak data load more
  function getDetailsEvent(type){
    resetLoadingGifDetailsEvent(type);
    getDetailsEventPrint(type);
  }

function getDataDetailsEvent(type){
	var pathname = window.location.pathname;
	var event_id = pathname.substring(pathname.lastIndexOf('/')+1);
	var data = {};
	switch(type) {
		case 1:
		  data['orderBy'] = 'created_at';
		  data['optionOrder'] = 'desc';
		  data['event_id'] = event_id;
		  break;
		case 2:
		  data['skip'] = this.skipSongEvent;
		  data['take'] = this.takeSongEvent;
		  data['orderBy'] = 'created_at';
		  data['optionOrder'] = 'desc';
		  data['event_id'] = event_id;
		  break;
    case 3:
      data['event_id'] = event_id;
      break;
    case 4:
      data['skip'] = this.skipParticipateNotReleaseSong;
      data['take'] = this.takeParticipateNotReleaseSong;
      data['orderBy'] = 'created_at';
      data['optionOrder'] = 'desc';
      data['event_id'] = event_id;
      break;    
		default:
		  this.errors = true;
		  break;    
	}
	return data;
}

function getLinkDetailsEvent(type){
	
	var link;
	switch(type) {
		case 1:
		 link = base_url+"/eventDetailsDescription";
		 break;
		case 2:
		 link = base_url+"/SongEventRelease";
      break;
    case 3: 
      link = base_url+"/winnerGroupEvent";
      break;  
    case 4:
      link = base_url+"/participatedEventNotReleaseSong";
      break;  
    default:
      this.errors = true;
      break; 
	}
	return link;
}

function getContentDetailsEvent(type, data,i){
	var content = "";
	switch(type) {
		case 1:
		  content += printEventDetailsDescription(data);
		  break;
		case 2:
		  content += printEventDetailsSongEvent(data);
		  break;
		default:
    case 3:
      content += printSongEventWinner(data); 
      break; 
    case 4:
      content += printParticipatedEventNotReleaseSong(data,i);
      break;  
		  this.errors = true;
		  break;
	}
	return content;
}

function printEventDetailsDescription(data){
   var content = "";

   var create = data['create'];
   var t = create.split(/[- :]/);
   var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]); 

   var jsDate = toJSDate(data['start_time']);
   // var start = jsDate.toLocaleDateString() + " " + jsDate.toLocaleTimeString();

   var jsDate1 = toJSDate(data['end_time']);
   // var end = jsDate1.toLocaleDateString() + " " + jsDate1.toLocaleTimeString();

   var monthNames = [
    "January", "February", "March","April", "May", "June", "July",
    "August", "September", "October","November", "December"
   ];

   var numberIndex = ["00","01","02","03","04","05","06","07","08","09"];

   var day = jsDate.getDate();
   var monthIndex = jsDate.getMonth();
   var year = jsDate.getFullYear();
   var hours = jsDate.getHours();
   if(hours < 10){
      dataHours = numberIndex[hours];
   }else{
      dataHours = hours;
   }
   var minute = jsDate.getMinutes();
   if(minute < 10){
      dataMinutes = numberIndex[minute];
   }else{
      dataMinutes = minute;
   }
   var seconds = jsDate.getSeconds();
   if(seconds < 10){
       dataSeconds = numberIndex[seconds];
   }else{
       dataSeconds = seconds;
   }

   var start = day+' '+monthNames[monthIndex]+ ' ' +year+ ' '  +dataHours+':'+dataMinutes+':'+dataSeconds;
   

   var dayEnd = jsDate1.getDate();
   var monthIndexEnd = jsDate1.getMonth();
   var yearEnd = jsDate1.getFullYear();
   var hoursEnd = jsDate1.getHours();
   if(hoursEnd < 10){
      dataHoursEnd = numberIndex[hoursEnd];
   }else{
      dataHoursEnd = hoursEnd;
   }
   var minuteEnd = jsDate1.getMinutes();
   if(minuteEnd < 10){
      dataMinutesEnd = numberIndex[minuteEnd];
   }else{
      dataMinutesEnd = minuteEnd;
   }
   var secondsEnd = jsDate1.getSeconds();
   if(secondsEnd < 10){
       dataSecondsEnd = numberIndex[secondsEnd];
   }else{
       dataSecondsEnd = secondsEnd;
   }

   var end = dayEnd+' '+monthNames[monthIndexEnd]+ ' ' +yearEnd+ ' '  +dataHoursEnd+':'+dataMinutesEnd+':'+dataSecondsEnd;


   var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
   var firstDate = new Date(data['end_time']);
   var secondDate = new Date;
   // var diffDays = Math.round(Math.round((secondDate.getTime() - firstDate.getTime()) / (oneDay))); 
   var diffDays = Math.round(Math.round((secondDate.getTime() - firstDate.getTime()) ));
   
    var statusMemberEventParticipatePrint = "<div class='btn-in-stat' id='groupEventParticipant"+data['event_id']+"'>"+
                                            "<input type='button' onclick='sudahParticipate();' class='btn btn-line-stat col-btn-ex' value='participated'>"+
                                            "</div>";
    var statusMemberEventNotParticipatePrint = "<div class='btn-in-stat' id='groupEventParticipant"+data['event_id']+"'>"+
                                               "<button type='button' onclick='groupEventParticipant("+'"'+data['groupID']+'"'+", "+'"'+data['event_id']+'"'+", "+'"1"'+");' class='btn btn-line-stat col-btn-ex'>Participate</button>"+
                                               "</div>";                                       
    var statusMemberGroupJoin = "<div class='btn-in-stat' id='groupEventParticipant"+data['event_id']+"'>"+
                                "<button type='button' onclick='harusJoinGroup("+'"'+data['groupID']+'"'+", "+'"'+data['event_id']+'"'+");' class='btn btn-line-stat col-btn-ex'>Participate</button>"+
                                "</div>";                            

   content += "<hr class='tb-hr'>";
   content += "<div class='tb-header'>";
   content += "<div class='tb-pp'>";
   if(data['picture_group'] != 0 || data['picture_group'] != ''){
       content += "<a id='pjax' data-pjax='yes' href='"+base_url+"/group/latestpost/"+data['groupID']+"'><img src='"+base_url+"/"+data['picture_group']+"' class='img-responsive img-rounded-full'></a>";
   }else{
       content += "<a id='pjax' data-pjax='yes' href='"+base_url+"/group/latestpost/"+data['groupID']+"'><img src='"+base_url+"/img/avatar/avatar-group-300x300.png' class='img-responsive img-rounded-full'></a>";
   }
   
   content += "</div>";
   content += "<div class='tb-info'>";
   content += "<h1><a id='pjax' data-pjax='yes' href='"+base_url+"/group/latestpost/"+data['groupID']+"' class='btn-single-post-tl'>"+data['group_name']+"</a></h1>";
   content += "</div>";
   content += "<div class='tb-time'>";
   content += "<p>"+ getDateTimeSince(d, false) +" ago </p>";
   content += "</div>";
   content += "</div>";
   content += "<div class='warp-info-post-event'>";
   content += "<div class='gj-post-song-title'><a href='#'>"+data['event_title']+"</a></div>";
   content += "</div>";
   content += "<div class='tb-content'>";
   content += "<div class='sp-image'>";
   if(data['picture_event'] !=0 || data['picture_event'] != ''){
        content += "<img src='"+base_url+"/"+data['picture_event']+"' width='448px' height='300px' class='img-responsive img-full-responsive'>";
   }else{
        content += "<img src='"+base_url+"/img/photo-event/default_group_event_475x475.jpg' width='448px' height='300px' class='img-responsive img-full-responsive'>";
        // content += "<img src='"+base_url+"/img/photo-event/default_group_event_475x475.jpg' width='475px' height='300px' class='img-responsive img-full-responsive'>";
   }
   // content += "<a href='#' class='gj-btn-play>'<i class='fa fa-play-circle-o fa-4x'></i></a>";
   content += "</div>";
   content += "<div class='warp-info-post-song'>";
   content += "<div class='gj-post-song-author' style='font-size:14px;'><p>Creator : "+data['creator_event']+"</p></div>";
   content += "<div class='gj-post-song-genre' style='padding-left:10px; font-size:14px;'><p>Genre : "+data['genre']+"</p></div>";
   content += "<div class='gj-post-song-genre' style='padding-left:10px; font-size:14px;'>Event Start: " + start + "</div>";
   content += "<div class='gj-post-song-genre' style='padding-left:10px; font-size:14px;'>Event End: " + end + "</div>";
   if(data['trackID'] !=0 || data['trackID'] != ''){
       content += "<div class='gj-post-song-genre' style='padding-left:10px; font-size:14px;'>Back Song: <a onclick='playMusic("+'"'+data['trackID']+'"'+")' style='cursor:pointer; text-decoration:none'> "+data['judulTrack']+"</a></div>";
   }
   content += "<div class='gj-post-song-contribut'>";
   // content += "<span><a href='#'>Contributor 1, </a></span>";
   // content += "<span><a href='#'>Tebo, </a></span>";
   // content += "<span><a href='#'>Kribo, </a></span>";
   // content += "<span><a href='#'>Contributor 4, </a></span>";
   content += "</div>";
   content += "<div class='gj-post-song-desc'>";
   content += "<p style='font-size:14px;'>";
   content += "Description : "+data['description_event']+" ";
               
    // content += "<a href='#' class='more-txt'>more</a>"
    content += "</p>";
    content += "</div>";
    content += "</div>";
    content += "<div class='warp-stat-post-song'>";
    content += "<div class='warp-stat-card in-tl'>";
    content += "<div class='add-pl-track-song'>";
    
    content += "</div>";
    if(data['likedGroupEvent'] === 1 || data['likedGroupEvent'] === '1'){
        content += "<div class='stat-song-track' id='groupEventLiked"+data['event_id']+"'>";
        content += "<div class='warp-ico-stat'>";
        content += "<div class='ico-stat'><a href='javascript:void(0)' onclick='udahLikePost();'><i class='fa fa-heart like-single-list-song' style='color:red;'></i></a></div>";
        content += "<div class='num-stat'>"+data['liked']+"</div>";
        content += "</div>";
        content += "</div>";
    }else{
        content += "<div class='stat-song-track' id='groupEventLiked"+data['event_id']+"'>";
        content += "<div class='warp-ico-stat'>";
        content += "<div class='ico-stat'><a href='javascript:void(0)' onclick='clickLikeGroupEvent("+'"'+data['event_id']+'"'+","+'"'+data['liked']+'"'+");'><i class='fa fa-heart like-single-list-song'></i></a></div>";
        content += "<div class='num-stat'>"+data['liked']+"</div>";
        content += "</div>";
        content += "</div>";
    }
   
    content += "<div class='stat-song-track'>";
    content += "<div id='sharedEvent"+data['event_id']+"' class='warp-ico-stat'>";
    content += "<div class='ico-stat'><a onclick='sharedEventToSocial("+data['event_id']+","+data['shared']+");' style='cursor:pointer;'><i class='fa fa-mail-forward forward-single-list-song'></i></a></div>";
    content += "<div class='num-stat'>"+data['shared']+"</div>";
    content += "</div>";
    content += "</div>";
    
    content += "<span id='eventTitle"+data['event_id']+"' style='display:none;'>"+data['event_title']+"</span>";
    content += "<span id='eventDescription"+data['event_id']+"' style='display:none;'>"+data['description_event']+"</span>";
    content += "<span id='eventPicture"+data['event_id']+"' style='display:none;'>"+data['picture_event']+"</span>";

    content += "<span id='trackID-groupEvent"+data['event_id']+"' style='display:none;'>"+data['trackID']+"</span>";
    content += "<span id='judulTrack-groupEvent"+data['event_id']+"' style='display:none;'>"+data['judulTrack']+"</span>";
    content += "<span id='instrumentTrack-groupEvent"+data['event_id']+"' style='display:none;'>"+data['instrumentTrack']+"</span>";
    content += "<span id='creatorTrack-groupEvent"+data['event_id']+"' style='display:none;'>"+data['creatorTrack']+"</span>";
    content += "<span id='priceTrack-groupEvent"+data['event_id']+"' style='display:none;'>"+data['priceTrack']+"</span>";
    content += "<span id='eventTitle-groupEvent"+data['event_id']+"' style='display:none;'>"+data['event_title']+"</span>";
    content += "<span id='groupName-groupEvent"+data['event_id']+"' style='display:none;'>"+data['group_name']+"</span>";
  
    if(data['checkadmin'] === 'not admin'){   
        if(diffDays < 0) {  // check jika event belum selesai
            if(data['group_type'] === 1 || data['group_type'] === '1'){  // check group public
                  if(data['checkStatusMember'] === 'member'){
                      if(data['event_type'] === 1 || data['event_type'] === '1'){    //check event public
                           if(data['statusMemberEventParticipate'] === 1 || data['statusMemberEventParticipate'] === '1'){
                                console.log('sudah join event 1-1-1-1 , show' );  //group public  member event public  joined event
                                content += statusMemberEventParticipatePrint;
                           }else{
                                console.log('belum join event 1-1-1-0 , show');   // group public member event public not join event
                                content += statusMemberEventNotParticipatePrint;
                           }
                      }else{
                           if(data['statusMemberEventParticipate'] === 1 || data['statusMemberEventParticipate'] === '1'){
                                console.log('sudah join 1-1-0-1 , show');  //group public member event privete joined
                                content += statusMemberEventParticipatePrint;
                           }else{
                               console.log('belum join 1-1-0-0 , show');  // group public member event private not join
                               content += statusMemberEventNotParticipatePrint;
                           }
                      }
                  }else{
                      if(data['event_type'] === 1 || data['event_type'] === '1'){
                           if(data['statusMemberEventParticipate'] === 1 || data['statusMemberEventParticipate'] === '1'){
                                console.log('sudah join 1-0-1-1 , show');  //group public  not member event public joined
                                content += statusMemberEventParticipatePrint;
                           }else{
                                console.log('sudah join 1-0-1-0 , show');  //group public  not member event public  not join
                                content += statusMemberEventNotParticipatePrint;
                           }
                      }else{
                           if(data['statusMemberEventParticipate'] === 1 || data['statusMemberEventParticipate'] === '1'){
                                console.log('sudah join 1-0-0-1 , hide');  //group public  not member event private joined
                                content += statusMemberEventParticipatePrint;
                           }else{
                                console.log('sudah join 1-0-0-1 , hide');  //group public  not member event private joined
                                content += "<div class='btn-in-stat'>";
                                content += "</div>";
                           }
                      }
                  }
             }else{
                  if(data['checkStatusMember'] === 'member'){
                       if(data['event_type'] === 1 || data['event_type'] === '1'){
                            if(data['statusMemberEventParticipate'] === 1 || data['statusMemberEventParticipate'] === '1'){
                                 console.log('sudah join 0-1-1-1 , show'); // group privete member event public joined
                                 content += statusMemberEventParticipatePrint;
                            }else{
                                 console.log('belum join 0-1-1-0 , show'); // group private member event public not join
                                 content += statusMemberEventNotParticipatePrint;
                            } 
                       }else{
                            if(data['statusMemberEventParticipate'] === 1 || data['statusMemberEventParticipate'] === '1'){
                                 console.log('sudah join 0-1-0-1 , show');  // group privete member  event private joined
                                 content += statusMemberEventParticipatePrint;
                            }else{
                                 console.log('belum join 0-1-0-0 , show'); // group private member event private not join
                                 content += statusMemberEventNotParticipatePrint;
                            }
                       }
                  }else{
                      if(data['event_type'] === 1 || data['event_type'] === '1'){
                           if(data['statusMemberEventParticipate'] === 1 || data['statusMemberEventParticipate'] === '1'){
                                console.log('sudah join 0-0-1-1 , show harus join group'); // group private not member event public joined
                                content += statusMemberEventParticipatePrint;
                           }else{
                                console.log('belum join 0-0-1-0 , show harus join group'); // group private not ember event public not join
                                content += statusMemberGroupJoin;
                           }
                      }else{
                           if(data['statusMemberEventParticipate'] === 1 || data['statusMemberEventParticipate'] === '1'){
                                console.log('sudah join 0-0-0-1 , hide');  //group private  not member event private joined
                                content += statusMemberEventParticipatePrint;
                           }else{
                                console.log('sudah join 0-0-0-0 , hide');  //group private  not member event private not join
                                content += "<div class='btn-in-stat'>";
                                content += "</div>";
                           }
                      }
                  }

             }    
            
        }else{
            content += "<div class='btn-in-stat'>";
            content += "</div>";
        }
    }
    else{
       content += "<div class='btn-in-stat'>";
       content += "</div>";
    }
       
    content += "</div>";
    content += "</div>";
    content += "</div>";
    
    return content;
}

function printEventDetailsSongEvent(data){

   var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
   var firstDate = new Date(data['end_event']);
   var secondDate = new Date;
   var diffDays = Math.round(Math.round((secondDate.getTime() - firstDate.getTime()) ));

	 var content = "";
	 content += "<div class='col-lg-6 col-md-12 col-sm-12 col-xs-12 nopadding'>";
    content += "<div class='gj-song-wrap'>";
    content += "<div class='gj-song-card my-event'>";
    content += "<div class='gj-song-cover'>";
    if(data['pictureTrack'] != 0 || data['pictureTrack'] != ''){
        content += "<img src='"+base_url+"/"+data['pictureTrack']+"' class='img-responsive'>";
    }else{
        content += "<img src='"+base_url+"/img/album/album-song_475x475.jpg' class='img-responsive'>";
    }
  
    content += "<div class='gj-song-play'><i onclick='playMusic("+'"'+data['trackID']+'"'+")' class='fa fa-play-circle-o fa-2x'></i></div>";
    content += "</div>";
    content += "<div class='gj-song-desc'>";
    content += "<div class='gj-song-story'>";
    content += "<h1 class='gj-song-title'>"+data['song_event_title']+"</h1>";
    content += "<div class='gj-song-username'><i class='fa fa-user'></i> <a id='pjax' data-pjax='yes' href='"+base_url+"/"+data['creatorSong']+"'>"+data['creatorSong']+"</a></div>";
    content += "<div class='gj-event-contributor'><span>"+data['play']+" plays</span></div>";
    content += "</div>";

    content += "<span id='eventID_songVote' style='display:none'>"+data['event_id']+"</span>";
    content += "<span id='trackID_songVote' style='display:none'>"+data['trackID']+"</span>";

    if(data['checkAdminGroup'] === 'admin'){  //untuk cek admin group
       if(diffDays > 0){      // untuk cek event suadah selesai
           if(data['checkingWinnerEvent'] === '0' || data['checkingWinnerEvent'] === 0){  // unbtuk cek event sudah ada winner atau belum
              content += "<div class='gj-song-act-evt button_VoteWinner'>";
              content += "<div class='btn-report-track-song'><a href='javascript:void(0);' onclick='clickLike();' class='btn-vote-ts'><i class='fa fa-thumbs-o-up vote-ts-evt btn'></i></a><span>Vote This</span></div>";
              content += "</div>";
          }
       }
    }

    content += "</div>";
    content += "<div class='warp-stat-card'>";
    content += "<div class='stat-song-track'>";
    if(data['statusLike'] === 1 || data['statusLike'] === '1'){
        content += "<div class='warp-ico-stat' id='statusLikeTrackAndSong"+data['trackID']+"'>";
        content += "<div class='ico-stat'><a href='javascript:void(0)' onclick='YouHaveLikeTrack();'><i class='fa fa-heart like-single-list-song' style='color:red;'></i></a></div>";
        content += "<div class='num-stat'>"+data['like']+"</div>";
        content += "</div>";
    }else{
        content += "<div class='warp-ico-stat' id='statusLikeTrackAndSong"+data['trackID']+"'>";
        content += "<div class='ico-stat'><a href='javascript:void(0)' onclick='LikeTrackAndSong("+'"'+data['trackID']+'"'+","+'"'+data['like']+'"'+");'><i class='fa fa-heart like-single-list-song'></i></a></div>";
        content += "<div class='num-stat'>"+data['like']+"</div>";
        content += "</div>";
    }

    content += "</div>";
    content += "<div class='stat-song-track'>";
    content += "<div class='warp-ico-stat'>";
    content += "<div class='ico-stat'><a class='btn-share-ts'><i class='fa fa-mail-forward forward-single-list-song'></i></a></div>";
    content += "<div class='num-stat'>"+data['shared']+"</div>";
    content += "</div>";
    content += "</div>";
    content += "<div class='stat-song-track'>";
    content += "<div class='warp-ico-stat'>";
    content += "<div class='ico-stat'><a href='#'><i class='fa fa-magic spice-single-list-song'></i></a></div>";
    content += "<div class='num-stat'>"+data['spice']+"</div>";
    content += "</div>";
    content += "</div>";
    content += "</div>";
    content += "</div>";
    content += "<hr>";
    content += "</div>";
    content += "</div>";
    return content;

}

function printEventDetailsSongEventNull(type){
    var content = "";
    content += "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 padten'>";
    content += "<div class='nf-item-wall-track'>";
    if(type === 2){
        content += "<p>No songs available</p>";
    }else if(type === 3){
        content += "<p>No winners yet</p>";
    }else if(type === 4){
        content += "<p>No participants yet</p>";
    }    
    content += "</div>";
    content += "</div>";

    return content;
}

function printSongEventWinner(data){

   var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
   var firstDate = new Date(data['end_event']);
   var secondDate = new Date;
   var diffDays = Math.round(Math.round((secondDate.getTime() - firstDate.getTime()) ));
  
   var content = "";
   if(diffDays > 0){
       content += "<div class='gj-song-card gj-song-list' id='1' data-song-index='1'>";
       content += "<div class='gj-song-cover'>";
       if(data['pictureSong'] != 0 || data['pictureSong'] != ''){
           content += "<img src='"+base_url+"/"+data['pictureSong']+"' class='img-responsive'>";
       }else{
           content += "<img src='"+base_url+"/img/album/track-02.jpg' class='img-responsive'>";
       }
       content += "<div class='gj-song-play'><i onclick='playMusic("+'"'+data['trackID']+'"'+");' class='fa fa-play-circle-o fa-3x'></i></div>";
       content += "</div>";
       content += "<div class='gj-song-desc'>";
       content += "<div class='gj-song-story'>";
       content += "<h1 class='gj-song-title'>"+data['judulSong']+"</h1>";
       content += "<div class='gj-song-username'><i class='fa fa-user'></i> <a href='"+base_url+"/"+data['username_winner']+"'>"+data['username_winner']+"</a></div>";
       // content += "<p class='gj-song-contributor'>Contributor1, contributor 2, contributor3</p>";
       content += "<ul class='gj-song-author'>";
       content += "<li>Original Song Title: "+data['original_track']+"</li>";
       content += "<li>Genre: "+data['genre']+"</li>";
       content += "<li>Original From: "+data['original_username']+"</li>";
       content += "</ul>";
       content += "</div>";
       content += "<div class='gj-song-action'>";
       content += "<div class='btn-report-track-song'><a class='btn-report-ts'><i class='fa fa-exclamation-triangle btn'></i></a><span>report</span></div>";
       content += "<div class='gj-played'><p>"+data['play']+" plays</p></div>";
       content += "</div>";
       content += "<div class='warp-stat-card winner-evt-bc'>";
       content += "<div class='email-winner-evt'>";

       content += "<span id='email_winner' style='display:none;'>"+data['email_winner']+"</span>";
       content += "<span id='username_winner' style='display:none;'>"+data['username_winner']+"</span>";
       content += "<span id='username_admin' style='display:none;'>"+data['admin_name']+"</span>";
       content += "<span id='event_title' style='display:none;'>"+data['event_title']+"</span>";
       if(data['checkAdminGroup'] === 'admin'){
           content += "<a class='btn-emailto-winner'><i class='fa fa-envelope-o'></i></a>";
       }
       content += "</div>";
       content += "<div class='stat-song-track'>";

       if(data['statusLike'] === 1 || data['statusLike'] === '1'){
            content += "<div class='warp-ico-stat' id='statusLikeTrackAndSong"+data['trackID']+"'>";
            content += "<div class='ico-stat'><a href='javascript:void(0)' onclick='YouHaveLikeTrack();'><i class='fa fa-heart like-single-list-song' style='color:red;'></i></a></div>";
            content += "<div class='num-stat'>"+data['like']+"</div>";
       
        }else{
            content += "<div class='warp-ico-stat' id='statusLikeTrackAndSong"+data['trackID']+"'>";
            content += "<div class='ico-stat'><a href='javascript:void(0)' onclick='LikeTrackAndSong("+'"'+data['trackID']+'"'+","+'"'+data['like']+'"'+");'><i class='fa fa-heart like-single-list-song'></i></a></div>";
            content += "<div class='num-stat'>"+data['like']+"</div>";
           
        }
       content += "</div>";
       content += "</div>";
       content += "<div class='stat-song-track'>";
       content += "<div class='warp-ico-stat'>";
       content += "<div class='ico-stat'><a class='btn-share-ts'><i class='fa fa-mail-forward forward-single-list-song'></i></a></div>";
       content += "<div class='num-stat'>"+data['shared']+"</div>";
       content += "</div>";
       content += "</div>";
       content += "<div class='stat-song-track'>";
       content += "<div class='warp-ico-stat'>";
       content += "<div class='ico-stat'><a href='#'><i class='fa fa-magic spice-single-list-song'></i></a></div>";
       content += "<div class='num-stat'>"+data['spice']+"</div>";
       content += "</div>";
       content += "</div>";
       content += "</div>";
       content += "</div>";
       content += "</div>";
    }else{
        content += "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 padten' id='sectionSongWinnerEvent'>";
        content += "<div class='nf-item-wall-track'>";
        content += "<p>No winners yet</p>";
        content += "</div>";
        content += "</div>";
    }   

   return content; 
}

function printParticipatedEventNotReleaseSong(data,i){
   var profile_image = data['profile_image'];  
   var name_profile_image = profile_image.substring(profile_image.lastIndexOf('/')+1); // nama profile image
   var folder = 'img/avatar/';  // folder penyimpanan profile image
   var picture = folder+name_profile_image; // link profile image

   content = "";
   content += "<div class='item col-lg-4 col-md-4 col-sm-6 col-xs-12 col-sxs-12 padten'>";
   if(data['bg_image'] != 0){
      content += "<div class='warp-single-friend bg-card-fl' style='background-image:url("+base_url+"/"+data['bg_image']+");'>";
   }else{
      content += "<div class='warp-single-friend bg-card-fl' style='background-image:url("+base_url+"/img/blur-bg-profile/bg-profile-2.jpg);'>";
   }
   
   content += "<div class='tile-card-bg-fl'></div>";
   content += "<div>";
   if(data['profile_image'] == 0){
          content += "<a href='"+base_url+"/"+data['username']+"'><img src='"+base_url+"/img/avatar/avatar-300x300.png' class='img-circle img-border-gj img-responsive'></a>";
    }else{
        if(profile_image != picture){
            content += "<a href='"+base_url+"/"+data['username']+"'><img src='"+data['profile_image']+"' class='img-circle img-border-gj img-responsive'></a>";
        }else{
           content += "<a href='"+base_url+"/"+data['username']+"'><img src='"+base_url+"/"+data['profile_image']+"' class='img-circle img-border-gj img-responsive'></a>";
        }
   }    
   content += "</div>";
   content += "<div class='desc-myfriend'>";
   content += "<div class='disc-user-name'><a href='"+base_url+"/"+data['username']+"'>"+data['real_name']+"</a></div>";
   content += "<div class='disc-user-username'><a href='"+base_url+"/"+data['username']+"'>"+data['username']+"</a></div>";
   if(data['statusRelease'] === 1 || data['statusRelease'] === '1'){
        content += "<h5 style='color:green;'>submitted</></h5>";
   }else{
        content += "<h5>unsubmitted</h5>";
   }
   content += "<div class='warp-btn-friend-list' id='buttonFriendMember"+data['userID']+"'>";  
    
   if(data['checkFriend'] === 'not friend'){  
      content += "<button type='button' onclick='addToNewMyFriend("+'"'+data['userID']+'"'+");' class='btn btn-add-f-list col-btn-ex'><i class='fa fa-user-plus'></i>Add Friend</button>"; 
    }else if(data['checkFriend'] === 'send request'){
          content += "<button type='button'  class='btn btn-add-f-list col-btn-ex'><i class='fa fa-user-plus'></i>Friend Request Sent</button>";
     }else if(data['checkFriend'] === 'not confirm'){
          content += "<button type='button' onclick='confirmFriendRequestMember("+'"'+data['userID']+'"'+");' class='btn btn-add-f-list col-btn-ex'><i class='fa fa-user-plus'></i>Confirm Friend</button>";
     }else{
        content += "<button type='button' class='btn btn-add-f-list col-btn-ex'><i class='fa fa-check'></i> Friend</button>";
   }      
   content += "</div>";
   content += "</div>";
   content += "</div>";
   content += "</div>";

   return content;
}

function getDetailsEventPrint(type){
	var data = getDataDetailsEvent(type);
	var link = getLinkDetailsEvent(type);
	$.ajax({
        type : "POST",
        data : data,
        url : link,
        success : function(responseText){
        	console.log(responseText);
        	var response = responseText[0];
        	var count = response['count'];
        	// console.log(response);
        	// console.log(count);
        	// var count = responseText[0]['count'];
        	if(count === 0){
        		var contentNull = printEventDetailsSongEventNull(type);
            $('#loadingDetailsEvent'+type).replaceWith(contentNull);
        	}else{
        		var data = response['data'];
        		var p = data.length;
        		var content = "";
        	    for(var i = 0; i < data.length; i ++){
        	    	content += getContentDetailsEvent(type, data[i], i);
        	    }
        	    $('#loadingDetailsEvent'+type).replaceWith(content);
        	    if(type == 2){
                setTakeTotalDetailsEvent(count,2);
                setLinkDetailsEvent(2);
              }
              if(type == 4){
                 setTakeTotalDetailsEvent(count,4);
                 setLinkDetailsEvent(4);
              }
        	}
        	

        },
        error : function(){
        	console.log('error');
        }
	});
}

$(document).on('ready pjax:success', function(){
    $('body').on('click','.vote-ts-evt', function() {
       // $(this).toggleClass("clicked-like");
       var eventID = $('#eventID_songVote').text();
       var trackID = $('#trackID_songVote').text();
       // console.log(eventID+','+trackID);
       $('#voteTheWinner').modal({
            backdrop: 'static',
            keyboard: true,
            show : true }); 
       $('#eventID_voteWinner').text(eventID);
       $('#trackID_voteWinner').text(trackID);
           
    });

    $('body').on('click','.btn-emailto-winner',function(){
       var email = $('#email_winner').text();
       var username_winner = $('#username_winner').text();
       var username_admin = $('#username_admin').text();
       var event_title = $('#event_title').text();
       $('#emailTheWinner').modal({
            backdrop: 'static',
            keyboard: true,
            show : true }); 
       $('#input_email_winner').val(email);
       $('#input_username_winner').text(username_winner);
       $('#input_username_admin').text(username_admin);
       $('#input_event_title').text(event_title);
    });
});

function prosesVoteWinnerEvent(){
    var event_id = $('#eventID_voteWinner').text();
    var trackID = $('#trackID_voteWinner').text();
    // console.log(event_id+','+trackID);
    $('.closevoteTheWinner').hide();
    $('.loader-process').show();
    var button = "<a id='buttonProsesVoteWinner' class='btn-s-gj bgc-btn'>Yes</a>";
    $('#buttonProsesVoteWinner').replaceWith(button);
    var buttonProsess = "<a id='buttonProsesVoteWinner' href='javascript:void(0);' onclick='prosesVoteWinnerEvent();' class='btn-s-gj bgc-btn'>Yes</a>";

    var data = '&event_id=' +event_id + '&trackID=' +trackID;
    var link = base_url+"/submitEventWinner";

    $.ajax({
         type : "POST",
         url : link,
         data : data,
         success : function(responseText){
          console.log(responseText);
            $('.button_VoteWinner').hide();
            $('#voteTheWinner').modal('hide');
            var status = responseText['status'];
            console.log(status);
            if(status === 1 || status === '1' ){
                var data = responseText['data'][0]['data'];
                console.log(data);
                var content = printSongEventWinner(data[0]);
                $('#sectionSongWinnerEvent').html(content);
            }else if(status === 0 || status === '0'){
                swal('Oops...Try Again!','error');
                $('.closevoteTheWinner').show();
                $('.loader-process').hide();
                $('#buttonProsesVoteWinner').replaceWith(buttonProsess);
            }else{
                var data = responseText['data'][0]['data'];
                console.log(data);
                var content = printSongEventWinner(data[0]);
                $('#sectionSongWinnerEvent').replaceWith(content);
            }
            console.log('suksses');
         },
         error : function(){
           console.log('error');
            $('.closevoteTheWinner').show();
            $('.loader-process').hide();
            $('#buttonProsesVoteWinner').replaceWith(buttonProsess);
         }
    });
}

function sendEmailToEventWinner(){
   var email =  $('#input_email_winner').val();
   var username_winner = $('#input_username_winner').text();
   var username_admin = $('#input_username_admin').text();
   var text_area = $('#input_text_area').val();
   var event_title = $('#input_event_title').text();

   var data = '&email=' +email + '&username_winner=' +username_winner + '&username_admin=' +username_admin + '&text_area=' +text_area + '&event_title=' +event_title;
  
   var link = base_url+"/sendEmailToEventWinner";
   
   $('.modal-emailTheWinner').hide();
   var button = "<button id='buttonSendEmail' type='submit' class='btn btn-primary'>Send</button>";
   $('#buttonSendEmail').replaceWith(button);
   $('.loader-process').show();
   var buttonSend = "<button id='buttonSendEmail' type='submit' class='btn btn-primary' onclick='sendEmailToEventWinner();'>Send</button>";
   $.ajax({
        type : "POST",
        url : link,
        data : data,
        success : function(responseText){
           console.log(responseText);
            console.log('suksses');
            $('#emailTheWinner').modal('hide');
            $('.loader-process').hide();
            $('.modal-emailTheWinner').show();
            $('#buttonSendEmail').replaceWith(buttonSend);
            $('#input_text_area').val('');
            swal({
              title: 'Success send email to '+username_winner,
              text: 'I will close in 2 seconds.',
              timer: 2000
            });
        },
        error : function(){
            console.log('errors');
            $('#emailTheWinner').modal('hide');
            $('.loader-process').hide();
            $('.modal-emailTheWinner').show();
            $('#buttonSendEmail').replaceWith(buttonSend);
            $('#input_text_area').val('');
        }
   });
}

function addToNewMyFriend(memberID){
  console.log(memberID)
  var url = base_url+'/addToFriend/'+memberID;

    $.ajax({
          url: url,
          type: 'POST',
          data: {friendID:memberID},
         
          success: function(){
              console.log('sukses add friend');
            swal({ 
                title: "Thank you!",
                 text: "your request was sent , please waiting confirm...",
                  type: "success",
                });
                var content = "<button type='button'  class='btn btn-add-f-list col-btn-ex'><i class='fa fa-user-plus'></i>Request Send</button>";
                $('#buttonFriendMember'+memberID).html(content);
          },
          error: function(){
              console.log('error');
          }
    });
} 

function confirmFriendRequestMember(memberID){
    console.log(memberID);
    var  url =  base_url+'/ConfrimFriendGroupMember/'+memberID;

    $.ajax({
          url: url,
          type: 'POST',
          data: {friendID:memberID},
         
          success: function(){
              console.log('sukses confirm friend');
              swal({ 
                title: "Thank you!",
                 text: "sukses confirm friend...",
                  type: "success",
                });
                 var content = "<button type='button' class='btn btn-add-f-list col-btn-ex'><i class='fa fa-check'></i> Friend</button>";
                  $('#buttonFriendMember'+memberID).html(content);
          },
          error: function(){
              console.log('error');
          }
    });
}

function sharedEventToSocial(eventID,totalShared){
   var event_title = $('#eventTitle'+eventID).text();
   var event_description = $('#eventDescription'+eventID).text();
   var event_picture = $('#eventPicture'+eventID).text();
   $('#sharedEventModal').modal('show');

   $('#event_title_shared').text(event_title);
   $('#event_description_shared').text(event_description);
   $('#event_picture_shared').text(event_picture);
   $('#eventID_shared').text(eventID);
   $('#totalShared_shared').text(totalShared);

}

//twitter event share
function twitter_shared_event(){
    var event_title_shared = $('#event_title_shared').text();
    var event_description_shared = $('#event_description_shared').text();
    var event_picture_shared = $('#event_picture_shared').text();
    var eventID_shared = $('#eventID_shared').text();
    var totalShared_shared = $('#totalShared_shared').text();

    var a = 1;
    var sharedTotal = parseInt(totalShared_shared)+parseInt(a);

    var leftPosition = (window.screen.width / 2) - ((450 / 2) + 10);
    var topPosition = (window.screen.height / 2) - ((250 / 2) + 50);

    window.open('https://twitter.com/intent/tweet?text=Im Sharing on Twitter&url='+window.location.origin+'/event/'+eventID_shared,'New Window 1','width=450,height=250,status=no,resizable=no,scrollbars=no,screenX='+leftPosition+',screenY='+topPosition);
    $('#sharedEventModal').modal('hide');

    var content = "<div id='sharedEvent"+eventID_shared+"' class='warp-ico-stat'>"+
                  "<div class='ico-stat'><a onclick='sharedEventToSocial("+eventID_shared+","+sharedTotal+");' style='cursor:pointer;'><i class='fa fa-mail-forward forward-single-list-song'></i></a></div>"+
                  "<div class='num-stat'>"+sharedTotal+"</div>"+
                  "</div>";
    $('#sharedEvent'+eventID_shared).replaceWith(content);
     hitungSharedEvent(eventID_shared)
}

// fb share event
function fb_shared_event(){
    var event_title_shared = $('#event_title_shared').text();
    var event_description_shared = $('#event_description_shared').text();
    var event_picture_shared = $('#event_picture_shared').text();
    var eventID_shared = $('#eventID_shared').text();
    var totalShared_shared = $('#totalShared_shared').text();
    
    var a = 1;
    var sharedTotal = parseInt(totalShared_shared)+parseInt(a);

    var leftPosition = (window.screen.width / 2) - ((450 / 2) + 10);
    var topPosition = (window.screen.height / 2) - ((250 / 2) + 50);

    window.open('http://www.facebook.com/dialog/share?app_id=783798388413863&display=popup&ref=plugin&src=like&href='+window.location.origin+'/event/'+eventID_shared,'New Window 1','width=450,height=250,status=no,resizable=no,scrollbars=no,screenX='+leftPosition+',screenY='+topPosition);
    $('#sharedEventModal').modal('hide');

    var content = "<div id='sharedEvent"+eventID_shared+"' class='warp-ico-stat'>"+
                  "<div class='ico-stat'><a onclick='sharedEventToSocial("+eventID_shared+","+sharedTotal+");' style='cursor:pointer;'><i class='fa fa-mail-forward forward-single-list-song'></i></a></div>"+
                  "<div class='num-stat'>"+sharedTotal+"</div>"+
                  "</div>";
    $('#sharedEvent'+eventID_shared).replaceWith(content);
     hitungSharedEvent(eventID_shared)
}

// gplus share event
function gplus_shared_event(){
    var event_title_shared = $('#event_title_shared').text();
    var event_description_shared = $('#event_description_shared').text();
    var event_picture_shared = $('#event_picture_shared').text();
    var eventID_shared = $('#eventID_shared').text();
    var totalShared_shared = $('#totalShared_shared').text();

    var a = 1;
    var sharedTotal = parseInt(totalShared_shared)+parseInt(a);

    var leftPosition = (window.screen.width / 2) - ((450 / 2) + 10);
    var topPosition = (window.screen.height / 2) - ((250 / 2) + 50);

    window.open('https://plus.google.com/share?url='+window.location.origin+'/event/'+eventID_shared,'New Window 1','width=450,height=250,status=no,resizable=no,scrollbars=no,screenX='+leftPosition+',screenY='+topPosition);
    $('#sharedEventModal').modal('hide');

    var content = "<div id='sharedEvent"+eventID_shared+"' class='warp-ico-stat'>"+
                  "<div class='ico-stat'><a onclick='sharedEventToSocial("+eventID_shared+","+sharedTotal+");' style='cursor:pointer;'><i class='fa fa-mail-forward forward-single-list-song'></i></a></div>"+
                  "<div class='num-stat'>"+sharedTotal+"</div>"+
                  "</div>";
    $('#sharedEvent'+eventID_shared).replaceWith(content);
    hitungSharedEvent(eventID_shared)
}

function hitungSharedEvent(eventID){
   var base_url = window.location.origin;

   $.ajax({
       type : "POST",
       url : base_url+"/hitungSharedEvent",
       data : {eventID : eventID},
       success : function(){
          console.log('sukses');
       },
       error : function(){
          console.log('error');
       }
   });
}