function formatTime(duration) //as hh:mm:ss
  {
    var sec_num = parseInt(duration,10); // don't forget the second param
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    var time    = minutes+':'+seconds;
    return time;
  }

function changeCount(count){
  if (count > 1000000) {
    count = Math.floor(count/1000000);
    count += "M";
  }
  else if (count > 1000) {
    count = Math.floor(count/1000);
    count += "K";
  }

  return count;
}

function checkPicture(pic){
  var number = Math.floor((Math.random() * 6) + 1);
  if(pic === "")
    pic = "img/album/album-0"+number+".jpg";
  else
    pic = "img/album/album-0"+number+".jpg";
  return pic;
}
