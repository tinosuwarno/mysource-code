// JavaScript Document
function initialize() {
		var myLatlng = new google.maps.LatLng(-6.612098, 106.822014);
        var mapCanvas = document.getElementById('map-canvas');
        var mapOptions = {
          center: new google.maps.LatLng(-6.612098, 106.822014),
          zoom: 16,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
		  
        }
        var map = new google.maps.Map(mapCanvas, mapOptions)
		var marker = new google.maps.Marker({
			position: myLatlng,
			map: map,
			title:"GritJam"
		});
      }
      google.maps.event.addDomListener(window, 'load', initialize);