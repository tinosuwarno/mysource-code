//function edited for explore card friend [Fadhlan Rizal]
//please send notice if you want to rewrite this script
//1. Card Friend in Explore Page
//2. Click Button in Friend List Page
//3. Detail Button in Demo Page

/* Note for using this shit LOL */
/*
  kalo pake 
    $(document).ready(function(){
      //put your script here
    });
  ganti jadi 
    $(document).on('ready pjax:success', function(){
      //put your script here
    });
*/

function picCheck(img){
  if (img.indexOf('img/') != -1 ) {
    userimageurl = window.location.origin+'/'+img;
  } else if(img == ''){
    userimageurl = window.location.origin+'/img/avatar/avatar-300x300.png';
  } else{
    userimageurl = img;
  }
  return userimageurl;
}

function getTimes(time){
  var t = time.split(/[- :]/);
  var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
  return d;
} 

// --------------- Featured modal -by fadhlan- --------------//
$(document).on('ready pjax:success', function(){
  $(document).on('click', '.get-featured-song', function(){    
    $('#addFeaturedSong').modal('show');
    getFeaturedSong();
  });
});


// --------------- Playlist Modal -by Fadhlan- --------------//

$(document).on('ready pjax:success', function(){
  $(document).on('click','.gritjam-icon-add-playlist',function(){
    var trackID = $(this).attr('data-trackid');
    document.getElementById('newPlay').setAttribute('data-trackid', trackID);
    getDataPlaylist(trackID);
    $('#addPlaylist').modal('show');
  });

  $('.newPlaylist').on('click',function(){
    var trackID = $(this).attr('data-trackid');
    if(trackID === undefined || trackID === '' || trackID === null){
      document.getElementById("trackID_playlist").value = -1;
    }
    else{
      document.getElementById("trackID_playlist").value = trackID;
    }
    $('#newPlaylist').modal('show');
  });

  $('.extPlaylist').on('click',function(){
    var trackID = $(this).attr('data-trackid');
    
    $('#extPlaylist').modal('show');
  });

  $('.delPlaylist').on('click',function(){
    // console.log($(this).attr('data-playlistid')); return;
    var playlistID = $(this).attr('data-playlistid');
    document.getElementById("playlistID_delete").value = playlistID;
    $('#deletePlaylist').modal('show');
  });

  $('.btn-delete-ts').on('click', function(){
     $('#deleteProject').modal('show');
   });

  $('.addSongToPlaylist').on('click',function(){
    $('#addSongToPlaylist').modal('show');
  });

  $(document).on('click','.name-playlist',function(){
    var playlistID = $(this).attr('data-playlist-index');
    var playlistName = $(this).attr('data-playlist-name');
    $('#judulPlaylist').html(playlistName);

    document.getElementById('deletePlaylistDiv').setAttribute('data-playlistid', playlistID);
    document.getElementById('addNewSongPlaylistDiv').setAttribute('data-playlistid', playlistID);

    $('#clearThisPlaylist').replaceWith(clearPlaylistTrack());

    $('.name-playlist').removeClass('active');
    $(this).addClass('active');
    $('.gj-playlist-detail.plsong').css('display','block').addClass('on-right');

    getPlaylistTrackDetails(playlistID);
  });
    
  $('.btn.fa-close').on('click', function(){
    $('.name-playlist').removeClass('active');
    $('.gj-playlist-detail.plsong').removeClass('on-right');
    $('.gj-playlist-detail.plsong').css('display','none');
  });


});

// --------------- landing page karaoke ------------ //
    $(document).ready(function(){
        /*document.getElementById('closeWarning').onclick = function(){
            this.parentNode.parentNode.parentNode
            .removeChild(this.parentNode.parentNode);
            return false;
          };*/
        $('.close-warning').on('click', function(e) { 
            $('.error-w').remove(); 
        $('.sucses-w').remove(); 
        });

        $('.close-btn-rt').on('click', function(e) { 
            $('.close-smt').remove();  
        });
    });

    $('.start-karaoke').click(function(){
      $('#selectSongKaroke').modal('show');
      $('#startKaraoke').modal('hide');
      $('body').addClass("tot");
    });

    // --------------- Payment Modal ---------------- //

      $('.sel-song').click(function(){
        var id_track = $(this).attr('id');
        console.log(id_track);
        var judul_back = $('.judul-back-'+id_track).text();
        var artist_back = $('.artist-back-'+id_track).text();
        var price_back = $('.price-back-'+id_track).text();

        console.log('price: '+price_back);

        $('#selectSongKaroke').modal('hide');
        $('#modalPayment').modal('show');

        $('#dat-id').text(id_track);
        $('#dat-title').text(judul_back);
        $('#dat-artist').text(artist_back);
        $('#dat-price').text(price_back);

        $('body').addClass("tot");
      });

      $('.process-payment').click(function(){
        $(".loader-process").show();

        var tipe_track;
        var id_track = $('#dat-id').text();
        var judul_back = $('#dat-title').text();
        var artist_back = $('#dat-artist').text();
        var price_back = $('#dat-price').text();
        var type_back = $('#dat-type').text();

        var url = window.location.origin+'/studio/paymentprocess';

        var data = 'trackID=' + id_track+'&price='+ price_back;

        if (type_back == 'karaoke') {
          tipe_track = 'karaoke';
          data += '&trackTitle=' + judul_back + '&trackArtist=' + artist_back + '&trackType=' + tipe_track;
        }

        console.log(data);

        $.ajax({
          url: url,
          type: 'POST',
          data: data,
          dataType: 'json',

          success: function(result){
            $(".loader-process").hide();
            console.log(result.status+' '+result.projectID);
            if (result.status == 1) {
              $('#modalPayment').modal('hide');
              
              swal({ 
                title: "Thank you!",
                 text: "You can redirected to karaoke page by clicked ok button!",
                  type: "success",
                  allowOutsideClick: false,
                });

              $('.swal2-confirm').click(function(){
                if(type_back == 'karaoke')
                  window.location.href = window.location.origin+"/studio/karaoke/"+result.projectID;
              });
            } else if(result.status == -1){
                $('#modalPayment').modal('hide');
                swal('Oops...Your balance is not enough!','Please, refill your wallet!','error');
            } else{
              swal('Oops...','Something went wrong!','error');
            }
          },

          error: function(jqXHR, textStatus, errorThrown){
              console.log(jqXHR);
              $(".loader-process").hide();
              swal('Oops...','Something went wrong!','error');
          }

        });
          

      });

    //-------------- end ------------ //

    //-------------- Delete Project -------------- //

      $('.btn-delete-ts').click(function(){
          var projectID = this.id;
          var url = window.location.origin+'/studio/removeReleaseProject';

          var data = 'projectID= '+projectID;
          // var formData = new FormData();
          // formData.append('projectID', projectID);

          $('#modalRemoveProjectConfirmation').html(
            '<div class="modal-dialog modal-sm">'+
              '<div class="modal-content">'+
                  '<div class="modal-header color-gj-popup-report">'+
                          '<h4 class="modal-title">Delete project confirmation</h4>'+
                  '</div>'+
                  '<div class="modal-body">'+
                      '<div class="row">'+
                          '<div class="col-sm-12">'+
                              '<p>Are you sure want to remove your release project?</p>'+
                          '</div>'+
                       '</div> '+  
                  '</div>'+
                  '<div class="modal-footer">'+
                      '<span class="loader-process" style="display: none;margin-right: 5px;"><img src="'+window.location.origin+'/img/loader-24.gif" /></span>'+
                      '<a href="#" class="btn-s-gj bgc-btn" data-dismiss="modal" style="margin-right: 5px;">Cancel</a>'+
                      '<a href="#" class="btn-s-gj bgc-btn process-remove-project">Yes</a>'+
                  '</div>'+

              '</div>'+
          '</div>'
        );

        $('#modalRemoveProjectConfirmation').modal({
             backdrop: 'static',
             keyboard: true, 
             show: true
          });

        $(".process-remove-project").click(function(){
            $(".loader-process").show();
            
            jQuery.ajax({
              url: url,
              type: 'POST',
              data: data,
              dataType: 'json',

              success: function(result){

                $(".loader-process").hide();
                if (result.status == 1) {
                    $('#modalRemoveProjectConfirmation').modal('hide');
                    swal({ 
                        title: "Success",
                        text: result.message,
                        type: "success",
                        allowOutsideClick: false,
                    });
                    $('.swal2-confirm').click(function(){
                       location.reload();
                    });
                    
                } else{

                  $('#modalRemoveProjectConfirmation').modal('hide');
                  swal('Oops...', result.message, 'error');
                }
              },

              error: function(jqXHR, textStatus, errorThrown){
                  console.log(jqXHR);
                  $(".loader-process").hide();
                  swal('Oops...','Something went wrong!','error');
              }


            });
        });  

      });

    //-------------- End ----------------- //

    


    //-------------- create Project Name -------------- //

      $('.new-back').click(function(){
          $("#selectSongKaroke").modal('hide');
          $('#modalOptionBacksong').modal('show');
          // $('#modalProjectName').modal('show');

          $('.process-next').click(function(){
            var radioVal = $('input[name=optionback]:checked').val();

            console.log('radio value = '+radioVal);

            if (radioVal == 0) {
              $('#modalOptionBacksong').modal('hide');
              $('#modalProjectName').modal('show');
            } else{
              $('#modalOptionBacksong').modal('hide');
              $('#modalUploadBacksong').modal('show');
            }
        });

      });

      $('.process-upload-back').click(function(){
        var judul = $('#judul').val();
        var genre = $('#select-tipe option:selected').val();
        var price =  $('#price option:selected').val();
        var is_public = $('#is_public option:selected').val();
        // var trackLyric = $('#trackLyric').val();
        var trackLyric = CKEDITOR.instances['trackLyric'].getData();
        var artist = $('#artist').val();
        

        var formData = new FormData();
        $.each($('#artWorkBackSong')[0].files, function(i, artWorkBackSong) {
          //image = 'image-'+i;
            formData.append('image-'+i, artWorkBackSong);
        });

        $.each($('#lagu')[0].files, function(i, lagu) {
          //lagu = 'lagu-'+i;
            formData.append('lagu-'+i, lagu);
        });

        formData.append('artist', artist);
        formData.append('judul', judul);
        formData.append('genre', genre);
        formData.append('price', price);
        formData.append('is_public', is_public);
        formData.append('trackLyric', trackLyric);

        $(".loader-process").show();

        $.ajax({
          url: window.location.origin+"/studio/uploadBacksong",
          type: 'POST',
          data: formData,
          timeout:900000,
          contentType: false,
          processData: false,
          success: function(result){
            // console.log(result);return;
            $(".loader-process").hide();
            if(result.status == 1 || result.status == '1'){
              $('#modalUploadBacksong').modal('hide');
              swal({ 
                title: "Success Upload your backsong!",
                text: "Please refresh this page by clicked OK button!",
                type: "success",
                allowOutsideClick: false,
              });
              $('.swal2-confirm').click(function(){
                $(".loader-process").hide();
                  location.reload();
              });
            } else {
              swal({
                title: "Error",
                text: result.message,
                type: "error",
              });
            }
            console.log(result);
          },
          error: function(jqXHR, textStatus, errorThrown){
                console.log(jqXHR);
                $(".loader-process").hide();
                swal('Oops...','Something went wrong!','error');
            }
        });

        //console.log(judul+'-'+genre+'-'+image+'-'+price+'-'+is_public+'-'+trackLyric+'-'+lagu);
      });


      $('.process-project').click(function(){
        $(".loader-process").show();

          var ptitle = $('#ptitle').val();
          var pdescription = $('#pdescription').val();
          var ptitleext = $('#ptitle-ext').text();
          var ptipe = $('#select-tipe option:selected').val();
          var pstatus = 0;

          ptitle += ' '+ptitleext;

          var url = window.location.origin+'/studio/createproject';
          var data = 'ptitle=' + ptitle + '&ptipe=' + ptipe + '&pstatus=' + pstatus + '&pdescription=' + pdescription;

          var type_back = 'karaoke';

          //console.log(ptitle+' '+ptipe);

          //console.log(url+data);

          $.ajax({
          url: url,
          type: 'POST',
          data: data,
          dataType: 'json',

          success: function(result){
            $(".loader-process").hide();
            //console.log(result.status+' '+result.projectID);
            if (result.status == 1) {
              $('#modalProjectName').modal('hide');
              swal({ 
                title: "Thank you!",
                 text: "You can redirected to karaoke page by clicked ok button!",
                  type: "success",
                  allowOutsideClick: false,
                });

              $('.swal2-confirm').click(function(){
                if(type_back == 'karaoke')
                  window.location.href = window.location.origin+"/studio/karaoke/"+result.projectID;
              });
            } else{
                $('#modalProjectName').modal('hide');
                swal('Oops...','Something went wrong!','error');
            }
          },

          error: function(jqXHR, textStatus, errorThrown){
              console.log(jqXHR);
              $(".loader-process").hide();
              swal('Oops...','Something went wrong!','error');
          }

        });

      });


    //-------------- End ------------------------- //


// JavaScript Document
$(document).on('ready pjax:success', function(){
    $('.navbar').affix({
    offset: {
      top: 125
    }
  });
});
$(document).on('ready pjax:success', function(){
    $(window).stellar({
    horizontalScrolling: false,
    parallaxBackgrounds: true,
    //verticalOffset: 20,
    responsive: true
  });
  
});

$(document).on('ready pjax:success', function(){
    $("html").niceScroll({
        cursorcolor:"rgba(30,30,30,.5)",
        zindex:999,
        scrollspeed:100,
        mousescrollstep:50,
        cursorborder:"0px solid #fff",
    });
  }
);

function clickLike() {
    $(this).toggleClass("clicked-like");
} 


$( function() {
  $('.like-single-list-song').click( function() {
    $(this).toggleClass("clicked-like");
  } );
} );

$(document).on('ready pjax:success', function(){
  $(".owl-carousel-header").owlCarousel({
    items : 1,
  lazyLoad:true,
    loop:true,
  autoplay : true,
  autoplayHoverPause : true,
  autoplayTimeout : 5000000
  });
});

$(document).on('ready pjax:success', function(){
  $(".owl-carousel").owlCarousel({
    items : 1,
  lazyLoad:true,
    loop:true,
  autoplay : true,
  autoplayHoverPause : true,
  autoplayTimeout : 5000
  });
});

$(document).on('ready pjax:success', function(){
  var owlListHome = $(".owl-carousel-list");
    owlListHome.owlCarousel({
    
    items : 1,
  lazyLoad:true,
    loop:true,
    margin:10,
  autoplay : true,
  autoplayHoverPause : true,
  autoplayTimeout : 50000
  });
  
  
  $(".prev-list-home").click(function(){
    owlListHome.trigger('next.owl.carousel');
  })
  $(".next-list-home").click(function(){
    owlListHome.trigger('prev.owl.carousel');
  })
});

$(document).on('ready pjax:success', function(){
 
  var owlPartner = $(".owl-partner-cont");
 
  owlPartner.owlCarousel({
      items : 7, 
    loop:true,
    margin:40,
    autoplay : true,
    autoplayHoverPause : true,
    autoplayTimeout : 50000,
      responsiveClass:true,
    responsive:{
        0:{
            items:2,
        },
    481:{
      items:3,
    },
    601:{
      items:4,
    },
        769:{
            items:5,
        },
        1025:{
            items:7,
        }
    }
      /*itemsDesktop : [1199,6],
      itemsDesktopSmall : [979,5], 
      itemsTablet: [768,4],
      itemsMobile : [479,1] */
  });

  $(".next-list-partner-h").click(function(){
    owlPartner.trigger('next.owl.carousel');
  })
  $(".prev-list-partner-h").click(function(){
    owlPartner.trigger('prev.owl.carousel');
  })
});


$(document).on('ready pjax:success', function(){
 
  var owl = $(".owl-track-cat");
 
  owl.owlCarousel({
      items : 7, 
      responsiveClass:true,
    responsive:{
        0:{
            items:1,
        },
    481:{
      items:2,
    },
    601:{
      items:4,
    },
        769:{
            items:5,
        },
        1025:{
            items:7,
        }
    },
      /*itemsDesktop : [1199,6],
      itemsDesktopSmall : [979,5], 
      itemsTablet: [768,4],
      itemsMobile : [479,1] */
  /*loop:true,
  autoplay:true,
    autoplayTimeout:1000,
    autoplayHoverPause:true*/
  });

  $(".next").click(function(){
    owl.trigger('next.owl.carousel');
  })
  $(".prev").click(function(){
    owl.trigger('prev.owl.carousel');
  })
});

$(document).on('ready pjax:success', function(){
 
  var owlmarket = $(".owl-market");
  owlmarket.owlCarousel({
 
      autoPlay: 3000, //Set AutoPlay to 3 seconds
      center: true,
      loop: true,
      items : 2,
      itemsDesktop : [1199,3],
      itemsDesktopSmall : [979,3]
 
  });

  $(".next-market").click(function(){
    owlmarket.trigger('next.owl.carousel');
  })
  $(".prev-market").click(function(){
    owlmarket.trigger('prev.owl.carousel');
  })
 
});

$(document).on('ready pjax:success', function(){
 
  var owlmostdl = $(".owl-most-dl");
  owlmostdl.owlCarousel({
      items : 6,
    margin : 10,
    responsive:{
        0:{
            items:1,
        },
    481:{
      items:2,
    },
    601:{
      items:4,
    },
        769:{
            items:4,
        },
        1024:{
            items:6,
        }
    }
    });
 
});


$(document).on('ready pjax:success', function(){
    
    $('.collapse').on('shown.bs.collapse', function(){
  $(this).parent().find(".fa-chevron-right").removeClass("fa-chevron-right").addClass("fa-chevron-down");
  }).on('hidden.bs.collapse', function(){
  $(this).parent().find(".fa-chevron-down").removeClass("fa-chevron-down").addClass("fa-chevron-right");
  });

});


/* edited by Fadhlan Rizal */
/* Card Friend in Explore Page */
/* Deleted and put in explore/friendExplore.js */
/* end edited by Fadhlan Rizal */

$("#btn-ancr").click(function() {
    $('html, body').animate({
        scrollTop: $("#land-1").offset().top
    }, 1000);
});

/* edited by Fadhlan Rizal */
/* Click Button in Friend List Page */
  $(document).on('mouseenter mouseleave', 'input[value][data-value-hover]', function() {
      var $this = $(this);  
      $this.toggleClass('hovered');
      var temp = $this.prop('value');
      $this.prop('value', $this.data('value-hover'))
      $this.data('value-hover', temp);
  });
  $(document).on('mouseenter mouseleave', 'span.warp-btn-f-uf',function () {
      if($(this).find($(".fa")).hasClass('fa-check')){
          $(this).find($(".fa")).removeClass('fa-check').addClass('fa-times');
      }
      else if($(this).find($(".fa")).hasClass('fa-times')){                     
          $(this).find($(".fa")).removeClass('fa-times').addClass('fa-check'); 
      } 
  });
/* end edited by fadhlan rizal 


/*======================================= MODAL PAGE DEMO / TRACK ============================================== */
$(document).on('ready pjax:success', function(){
  $('.edit-pp').click(function(){
      $('#editPicture').modal('show');
      $('body').addClass("tot");
    });
  
  $(".btn-report-ts").click(function(){
    $("#reportTrackSongModal").modal('show');
  });
  
  $(".btn-share-ts").click(function(){
    $("#shareSongTrack").modal('show');
  });
  
  $(".subButton").click(function(){
    if($('#alreadyGritjam').is(':checked')) { 
      $("#searchReportTrackSong").modal('show');
      $("#reportTrackSongModal").modal('hide');
    }
  });
});
  

/*======================================= DETAIL PAGE DEMO / TRACK ============================================== */
  /* Edited by Fadhlan Rizal */
  var countToggle = 0;
  var clickIndex = -1;
  $(document).on('ready pjax:success', function(){
    $(document).on('click', '.show-detail-song', function(){
      var clickIndex = $(this).attr('id');
      // var countToggle = this.countToggle;
      if(clickIndex%2!=0){
        if(countToggle%2==0){
          $('.gj-song-card').removeClass('active');
          $('.gj-song-card[data-song-index="'+clickIndex+'"]').addClass('active');
          $('.gj-song-detail.dsong').css('display','block').addClass('on-right');
        }else{
          $('.gj-song-card').removeClass('active');
          $('.gj-song-detail.dsong').removeClass('on-right');
          $('.gj-song-detail.dsong').css('display','none');
        }
        countToggle+=1;
      }else if(clickIndex%2==0){
        if(countToggle%2==0){
          $('.gj-song-card').removeClass('active');
          $('.gj-song-card[data-song-index="'+clickIndex+'"]').addClass('active');
          $('.gj-song-detail.dsong').css('display','block').addClass('on-left');
        }else{
          $('.gj-song-card').removeClass('active');
          $('.gj-song-detail.dsong').removeClass('on-left');
          $('.gj-song-detail.dsong').css('display','none');
        }
        // this.countToggle+=1;
        // this.clickIndex = clickIndex;
      }

      $('.btn.fa-close').on('click', function(){
      // var clickIndex = $('.show-detail').attr('id');
        $('.gj-song-card').removeClass('active');
        $('.gj-song-detail.dsong').removeClass('on-left');
        $('.gj-song-detail.dsong').removeClass('on-right');
        $('.gj-song-detail.dsong').css('display','none');
        
        countToggle+=1;
      });
    });
    
    $(document).on('click', '.show-detail-project', function(){
      var clickIndex = $(this).attr('id');
      console.log(clickIndex);
      if(clickIndex%2!=0){
        if(countToggle%2==0){
          $('.gj-song-card[data-song-index="'+clickIndex+'"]').addClass('active');
          $('.gj-song-detail.myproject').css('display','block').addClass('on-right');
        }else{
          $('.gj-song-card[data-song-index="'+clickIndex+'"]').removeClass('active');
          $('.gj-song-detail.myproject').removeClass('on-right');
          $('.gj-song-detail.myproject').css('display','none');
        }
        countToggle+=1;
        
      }else if(clickIndex%2==0){
        if(countToggle%2==0){
          $('.gj-song-card[data-song-index="'+clickIndex+'"]').addClass('active');
          $('.gj-song-detail.myproject').css('display','block').addClass('on-left');
        }else{
          $('.gj-song-card[data-song-index="'+clickIndex+'"]').removeClass('active');
          $('.gj-song-detail.myproject').removeClass('on-left');
          $('.gj-song-detail.myproject').css('display','none');
        }
        countToggle+=1;
      }

      $('.btn.fa-close').on('click', function(){
      // var clickIndex = $('.show-detail').attr('id');
        $('.gj-song-card[data-song-index="'+clickIndex+'"]').removeClass('active');
        $('.gj-song-detail.myproject').removeClass('on-left');
        $('.gj-song-detail.myproject').removeClass('on-right');
        $('.gj-song-detail.myproject').css('display','none');
        
        countToggle+=1;
      });
    });
    
    $(document).on('click', '.show-detail-ownproject', function(){
      var clickIndex = $(this).attr('id');
      console.log(clickIndex);
      if(clickIndex%2!=0){
        if(countToggle%2==0){
          $('.gj-song-card[data-song-index="'+clickIndex+'"]').addClass('active');
          $('.gj-song-detail.own-project').css('display','block').addClass('on-right');
        }else{
          $('.gj-song-card[data-song-index="'+clickIndex+'"]').removeClass('active');
          $('.gj-song-detail.own-project').removeClass('on-right');
          $('.gj-song-detail.own-project').css('display','none');
        }
        countToggle+=1;
        
      }else if(clickIndex%2==0){
        if(countToggle%2==0){
          $('.gj-song-card[data-song-index="'+clickIndex+'"]').addClass('active');
          $('.gj-song-detail.own-project').css('display','block').addClass('on-left');
        }else{
          $('.gj-song-card[data-song-index="'+clickIndex+'"]').removeClass('active');
          $('.gj-song-detail.own-project').removeClass('on-left');
          $('.gj-song-detail.own-project').css('display','none');
        }
        countToggle+=1;
      }

      $('.btn.fa-close').on('click', function(){
      // var clickIndex = $('.show-detail').attr('id');
        $('.gj-song-card[data-song-index="'+clickIndex+'"]').removeClass('active');
        $('.gj-song-detail.own-project').removeClass('on-left');
        $('.gj-song-detail.own-project').removeClass('on-right');
        $('.gj-song-detail.own-project').css('display','none');
        
        countToggle+=1;
      });
    });
    
    $(document).on('click', '.show-detail-invitproject', function(){
      var clickIndex = $(this).attr('id');
      console.log(clickIndex);
      if(clickIndex%2!=0){
        if(countToggle%2==0){
          $('.gj-song-card[data-song-index="'+clickIndex+'"]').addClass('active');
          $('.gj-song-detail.invited-project').css('display','block').addClass('on-right');
        }else{
          $('.gj-song-card[data-song-index="'+clickIndex+'"]').removeClass('active');
          $('.gj-song-detail.invited-project').removeClass('on-right');
          $('.gj-song-detail.invited-project').css('display','none');
        }
        countToggle+=1;
        
      }else if(clickIndex%2==0){
        if(countToggle%2==0){
          $('.gj-song-card[data-song-index="'+clickIndex+'"]').addClass('active');
          $('.gj-song-detail.invited-project').css('display','block').addClass('on-left');
        }else{
          $('.gj-song-card[data-song-index="'+clickIndex+'"]').removeClass('active');
          $('.gj-song-detail.invited-project').removeClass('on-left');
          $('.gj-song-detail.invited-project').css('display','none');
        }
        countToggle+=1;
      }

      $('.btn.fa-close').on('click', function(){
      // var clickIndex = $('.show-detail').attr('id');
        $('.gj-song-card[data-song-index="'+clickIndex+'"]').removeClass('active');
        $('.gj-song-detail.invited-project').removeClass('on-left');
        $('.gj-song-detail.invited-project').removeClass('on-right');
        $('.gj-song-detail.invited-project').css('display','none');
        
        countToggle+=1;
      });
    });
        
    $(document).on('click','.show-detail-fcontent',function(){
      var clickIndex = $(this).attr('id');
      if(clickIndex%2!=0){
        if(countToggle%2==0){
            $('.gj-song-card[data-song-index="'+clickIndex+'"]').addClass('active');
            $('.gj-song-detail.dfcontent').css('display','block').addClass('on-right');
        }else{
            $('.gj-song-card[data-song-index="'+clickIndex+'"]').removeClass('active');
            $('.gj-song-detail.dfcontent').removeClass('on-right');
            $('.gj-song-detail.dfcontent').css('display','none');
        }
        countToggle+=1;
      }
      else if(clickIndex%2==0){
        if(countToggle%2==0){
          $('.gj-song-card[data-song-index="'+clickIndex+'"]').addClass('active');
          $('.gj-song-detail.dfcontent').css('display','block').addClass('on-left');
        }else{
          $('.gj-song-card[data-song-index="'+clickIndex+'"]').removeClass('active');
          $('.gj-song-detail.dfcontent').removeClass('on-left');
          $('.gj-song-detail.dfcontent').css('display','none');
        }
        countToggle+=1;
      }

      $(document).on('click','.btn.fa-close', function(){
      // var clickIndex = $('.show-detail').attr('id');
          $('.gj-song-card[data-song-index="'+clickIndex+'"]').removeClass('active');
          $('.gj-song-detail.dfcontent').removeClass('on-left');
          $('.gj-song-detail.dfcontent').removeClass('on-right');
          $('.gj-song-detail.dfcontent').css('display','none');
          
          countToggle+=1;
      });
    });
  });
  /* End Edited by Fadhlan Rizal */

  //=============== Delete Pending Project ==============//
  $(document).ready(function(){
      // $('.btn-delete-ts-pending-project').click(function(){
      $('body').on('click', '.btn-delete-ts-pending-project', function(){  
        var projectID = $(this).attr('id');
        // console.log(projectID);
          $('#modalRemovePendingProject').modal({
            backdrop: 'static',
            keyboard: true,
            show : true });
          $('#removePendingProject-projectID').text(projectID);
      });

      $('body').on('click', '.process-remove-pending-project', function(){
          var projectID = $('#removePendingProject-projectID').text();
          var buttonYes = "<a href='#' class='btn-s-gj bgc-btn process-remove-pending-project' id='button-remove-project'>Yes</a>";
          var buttonYesProcess = "<a href='#' class='btn-s-gj bgc-btn' id='button-remove-project'>Yes</a>";
          var count = $('#pendingProject').attr('data-count');

          $('.loader-process').show();
          $('.project-pending-confirmation-close').hide();
          $('#button-remove-project').replaceWith(buttonYesProcess);
          console.log(count);
          var base_url = window.location.origin;
          
          $.ajax({
              type : 'POST',
              url : base_url+'/deletePendingProject',
              data : {projectID : projectID},
              success : function(responseText){
                // console.log(responseText);return;
                $('#modalRemovePendingProject').modal('hide');
                $('.loader-process').hide();
                $('.project-pending-confirmation-close').show();
                $('#button-remove-project').replaceWith(buttonYes);
                
                if(responseText != 1){
                  swal('Oops... An error occurred, please try again later','error');
                  return;
                }
                
                $('#myPendingProject'+projectID).remove();
                count -= 1; console.log(count);
                
                if(count === 0 || count === '0'){
                  var content = "<div class='gj-song-wrap'>"+
                                  "<div class='nf-item-wall-track'>"+
                                      "<p>You don't have pending project</p>"+
                                  "</div>"+
                                "</div>";
                  $('#myPendingProject'+projectID).replaceWith(content);
                }

                $('#pendingProject').attr('data-count',count);

                swal({ 
                  title: "Congratulation!",
                  text: "You just deleted your pending project",
                  type: "success", 
                    allowOutsideClick: true,
                });
              },
              error : function(jqXHR, textStatus, errorThrown){
                console.log(jqXHR); return;
                $('#modalRemovePendingProject').modal('hide');
                $('.loader-process').hide();
                $('.project-pending-confirmation-close').show();
                $('#button-remove-project').replaceWith(buttonYes);

                // console.log('error');
              }
          });
      });
  });
 //=============== Delete Pending Project End ==============//

 //=============== Messaging=================//
 /*-------------------------------------- PAGE DETAIL MESSAGE ------------------------------------*/
  $(document).on('ready pjax:success', function(){
   

   $('.msg-friend').on('click',function(){
     $('.msg-friend').removeClass('active');
       $(this).addClass('active');
       $('.msg-content').css('display','block');
     

     $('.btn.fa-close').on('click', function(){
     // var clickIndex = $('.show-detail').attr('id');
       $('.gj-song-card[data-song-index="'+clickIndex+'"]').removeClass('active');
       $('.gj-song-detail.dsong').removeClass('on-left');
       $('.gj-song-detail.dsong').removeClass('on-right');
       $('.gj-song-detail.dsong').css('display','none');
       
       countToggle+=1;
     });
     
     $('.btn-back-side-right-msg').click(function () {
        $('.msg-friend').removeClass('active');
        $('.msg-content').css('display','none');
     });
   });
   
   
  });