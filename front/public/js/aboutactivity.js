function checkElementAboutme(){
	var elementExists = new Array(
		document.getElementById("loadingAboutMe")
	);

	for (var i = elementExists.length - 1; i >= 0; i--) {
		if(elementExists[i] == null) return -1;
	};
	return 1;
}

$(document).on('ready pjax:success', function(){
	var check = checkElementAboutme();
	if(check == -1) return;

	var user = document.getElementById('userUsername').value;
	
	getViewFriendProfileAbout(user, 0);
});

function getViewFriendProfileAbout(username, type){
	$.ajax({
		type: 'post',
		data: {'username': username, 'skip': 0, 'take': 10, 'getType': type},
		url: '/getViewFriendProfile',
		success: function(responseText){
			// console.log(responseText); return;
			var content = responseText['content'];
			// var count = responseText['count'];

			$("#loadingAboutMe").replaceWith(content);
		},
		error : function(jqXHR, textStatus, errorThrown){
			console.log(jqXHR);
			console.log(textStatus);
		}
	});
}