var selected = null, // Object of the element to be moved
    y_pos = 0, // Stores x & y coordinates of the mouse pointer
    y_elem = 0; // Stores top, left values (edge) of the element

    function changediv() {
    if (document.getElementById("draggable-element1")) {
        document.getElementById("geser").style.display = "none";
        document.getElementById("sub").style.display = "block";          
        document.getElementById("cancel").style.display = "block"; 
        document.getElementById("help").style.display = "block"; 
        document.getElementById("draggable-element1").id = "draggable-element";
        // Bind the functions...
        document.getElementById('draggable-element').onmousedown = function () {
        _drag_init(this);
            return false;
        };

        document.onmousemove = _move_elem;
        document.onmouseup = _destroy;
    } else {
        // document.getElementById("div_top2").innerHTML = "teste";            
        // document.getElementById("div_top2").id = "div_top1";
    }
}

// Will be called when user starts dragging an element
    function _drag_init(elem) {
    // Store the object of the element which needs to be moved
    selected = elem;
    y_elem = (y_pos + selected.offsetTop);
}

// Will be called when user dragging an element
    function _move_elem(e) {
    y_pos = document.all ? window.event.clientY : e.pageY;
    if (selected !== null) {
      //  selected.style.left = (x_pos - x_elem) + 'px';
      y_axis= -(y_pos - y_elem);
      if (y_axis >= 0 && y_axis <= 100) {
        selected.style.backgroundPositionY = -(y_pos - y_elem) + '%';
        }else if (y_axis >= 100){
            selected.style.backgroundPositionY = '100%';
        }else{
            selected.style.backgroundPositionY = '0%';
        }
    } 
}

// Destroy the object when we are done
    function _destroy() {
    selected = null;
}



 $(document).on('ready pjax:success', function(){
                $('#sub').click(function() {
                    window.location.href = "/profile/save/" + y_axis;
                 });
             });

 function cancel() {
    location.reload();
}
