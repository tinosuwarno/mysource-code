  function viewconvo(message_id){
  	document.getElementById("loadermessage").style.display="block";
    $('#newmessage').remove();
  	var data = document.getElementById('data-'+message_id);

	var sender = data.getAttribute('data-me');
	var rcvr = data.getAttribute('data-you');
	// console.log(rcvr);
	 $.ajax({
	      type  : "POST",
	      data: {
	       sent_to:rcvr,
	       sent_by:sender
	      },
	      url   : "/viewconversation",
	      success : function(response){
	      	document.getElementById("loadermessage").style.display="none";
	      	console.log(response)
	      	$('#conversation').empty();
	      		var x = -1
                $.each(response, function( index, value ) {
                  x=x+1
                  	var t = response[x]['created_at'].split(/[- :]/);
                    var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
                  var str = response[x]['profile_image']
                   var myarr = str.split(":");
                   var myvar = myarr[0];
                   // console.log(myvar)
                   if (myvar=="https") {
                    var url = "<a href=''><img class='img-responsive img-rounded-full' src='" + response[x]['profile_image'] + "'></a>"
                  }else{
                    var url = "<a href=''><img class='img-responsive img-rounded-full' src='/" + response[x]['profile_image'] + "'></a>"
                  }

                  $('#conversation').append(
		  			"<div class='tb-friend-msg'>"+
		  			"<div class='tb-pp'>"+
		  			url+
		  			"</div>"+
		  			"<div class='tb-text-friend-msg'>"+
		  			"<div class='tb-info'>"+
		  			"<h3><a href='/"+response[x]['username']+"'>"+response[x]['username']+"</a></h3>"+
		  			"</div>"+
		  			"<div class='tb-time'>"+
		  			"<p>"+getDateTimeSincegroup(d, false)+"</p>"+
		  			"</div>"+
		  			"<div class='tb-caption'>"+
		  			"<p>"+response[x]['message']+"</p>"+
		  			"</div>"+
		  			"</div>"+
		  			"</div>"
  		)
              })
                // $('#textareablock').append("<img style='display: none;' id='loadermessage' src='/img/loader-more.gif' class='img-full-responsive e-centering width-loader'>")
                $('#textareablock').append(
                	"<div class='warp-post-msg' id='replymessage'>"+
		  			"<div class='warp-form-wall'>"+
		  			"<form id='createmessage' onsubmit='return createmessage()' class='tb-form-input-2'>"+
		  			"<input type='hidden' name='send_to' value='"+rcvr+"'></input>"+
		  			"<input type='hidden' name='user_id' value='"+sender+"'></input>"+
		  			// "<textarea form='createmessage' name='message' id='message'></textarea>"
		  			"<textarea form='createmessage' name='message' id='message' class='text-msg form-control'></textarea>"+
		  			"<div class='warp-btn-compose-wall'>"+
		  			"<input type='submit' class='btn-compose-wall'></input>"+
		  			"</div>"+
		  			"</form>"+
		  			"</div>"+
		  			"</div>")
	      }
	  })
  	}

function createmessage(){
	var form = $('#createmessage');
    var data = form.serialize();
    console.log(data);

    $.ajax({
	      type  : "POST",
	      data: data,
	      url   : "/create-message",
	      success : function(response){
	      	console.log(response)
	      	var str = response['user'][0]['profile_image']
                   var myarr = str.split(":");
                   var myvar = myarr[0];
                   // console.log(myvar)
                   if (myvar=="https") {
                    var url = "<a href=''><img class='img-responsive img-rounded-full' src='" + response['user'][0]['profile_image'] + "'></a>"
                  }else{
                    var url = "<a href=''><img class='img-responsive img-rounded-full' src='/" + response['user'][0]['profile_image'] + "'></a>"
                  }
	      	$('#conversation').append(
		  			"<div class='tb-friend-msg'>"+
		  			"<div class='tb-pp'>"+
		  			url+
		  			"</div>"+
		  			"<div class='tb-text-friend-msg'>"+
		  			"<div class='tb-info'>"+
		  			"<h3><a href='/"+response['user'][0]['username']+"'>"+response['user'][0]['username']+"</a></h3>"+
		  			"</div>"+
		  			"<div class='tb-time'>"+
		  			"<p>a moment ago</p>"+
		  			"</div>"+
		  			"<div class='tb-caption'>"+
		  			"<p>"+response['Message']['message']+"</p>"+
		  			"</div>"+
		  			"</div>"+
		  			"</div>")
	      }
	  })
    return false;
}

function createnew(send_to){
  console.log("tot")
  $('#friend').empty()
  $('#conversation').empty();
  $('#newmessage').empty();
  $('#replymessage').remove();
  var user_id = $('#user_id').val()

  $('#textareablock').append(
            "<div class='warp-post-msg' id='newmessage'>"+
            "<div class='warp-form-wall'>"+
            "<form id='createmessage' onsubmit='return createmessage()' class='tb-form-input-2'>"+
            "<input type='hidden' name='send_to' value='"+send_to+"'></input>"+
            "<input type='hidden' name='user_id' value='"+user_id+"'></input>"+
            // "<textarea form='createmessage' name='message' id='message'></textarea>"
            "<textarea form='createmessage' name='message' id='message' class='text-msg form-control'></textarea>"+
            "<div class='warp-btn-compose-wall'>"+
            "<input type='submit' class='btn-compose-wall'></input>"+
            "</div>"+
            "</form>"+
            "</div>"+
            "</div>")
}

function getDaysInMonthgroup(month,year) {     
    if( typeof year == "undefined") year = 1999; // any non-leap-year works as default     
    var currmon = new Date(year,month),     
        nextmon = new Date(year,month+1);
    return Math.floor((nextmon.getTime()-currmon.getTime())/(24*3600*1000));
} 
function getDateTimeSincegroup(target, full) { // target should be a Date object
    var now = new Date(), diff, yd, md, dd, hd, nd, sd, out = [];
    diff = Math.floor(now.getTime()-target.getTime()/1000);
    
    yd = now.getFullYear() - target.getFullYear();
    md = now.getMonth() - target.getMonth();
    dd = now.getDate() - target.getDate();
    hd = now.getHours() - target.getHours();
    nd = now.getMinutes() - target.getMinutes();
    sd = now.getSeconds() - target.getSeconds();
    
    if( md < 0) {yd--; md += 12;}
    if( dd < 0) {
        md--;
        dd += getDaysInMonthgroup(now.getMonth()-1,now.getFullYear());
    }

    if( hd < 0) {dd--; hd += 24;}
    if( md < 0) {hd--; md += 60;}
    if( sd < 0) {md--; sd += 60;}

    var selesai = false;

    if(!full){
        if( yd > 0 && !selesai) {out.push( yd+" year"+(yd == 1 ? "" : "s")); selesai = true;}
        if( md > 0 && !selesai) {out.push( md+" month"+(md == 1 ? "" : "s")); selesai = true;}
        if( dd > 0 && !selesai) {out.push( dd+" day"+(dd == 1 ? "" : "s")); selesai = true;}
        if( hd > 0 && !selesai) {out.push( hd+" hour"+(hd == 1 ? "" : "s")); selesai = true;}
        if( nd > 0 && !selesai) {out.push( nd+" minute"+(nd == 1 ? "" : "s")); selesai = true;}
        if( sd > 0 && !selesai) {out.push( sd+" second"+(sd == 1 ? "" : "s")); selesai = true;}
    }
    else{
        if( yd > 0 ) out.push( yd+" year"+(yd == 1 ? "" : "s"));
        if( md > 0 ) out.push( md+" month"+(md == 1 ? "" : "s"));
        if( dd > 0 ) out.push( dd+" day"+(dd == 1 ? "" : "s")); 
        if( hd > 0 ) out.push( hd+" hour"+(hd == 1 ? "" : "s"));
        if( nd > 0 ) out.push( nd+" minute"+(nd == 1 ? "" : "s")); 
        if( sd > 0 ) out.push( sd+" second"+(sd == 1 ? "" : "s")); 
    }
    return out.join(", ");
}    