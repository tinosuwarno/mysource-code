function checkElementProjectSetting(){
	var elementExists = new Array(
		document.getElementById("loadingProjectSetting0"),
		document.getElementById("loadingProjectSetting1"),
		document.getElementById("loadingProjectSetting2")
	);

	for (var i = elementExists.length - 1; i >= 0; i--) {
		if(elementExists[i] == null) return -1;
	};
	return 1;
}

$(document).on('ready pjax:success', function(){
  var check = checkElementProjectSetting();
	if(check == -1) return;

	var user = document.getElementById('userUsernameViewProfile');
	var username = user.getAttribute('data-username');

  // $('#aboutFriendMyWall').removeClass('active');
  // $('#userUsernameViewProfile').removeClass('active');
  // $('#projectFriendMyWall').addClass('active');

	getProjectSetting(username, 0);
	getProjectSetting(username, 1);
	getProjectSetting(username, 2);
});

function getProjectSetting(username, type){
	$.ajax({
		type: 'post',
		data: {'username': username, 'skip': 0, 'take': 10, 'getType': type},
		url: '/getViewFriendProjectSetting',
		success: function(responseText){
			
			// var content = responseText['content'];
			// var count = responseText['count'];
      var cek = ""
      if (type==0) {
        cek = "Current Project"
      }else if (type==1) {
        cek = "Invited Project"
      }else{
        cek = "My Project"
      }
      if (responseText['data'] == null || responseText['data'] == 0) {
        var content =                       "<div class='row'>"+
                                                       "<div class='col-lg-10 col-md-12'>"+
                                                            "<div class='security-box'>"+
                                                                 "<div class='panel panel-default'>"+
                                                                      "<div class='panel-default' >"+
                                                                        "<div class='col-lg-12' style='background-color:#f1f1f1;'><h5><strong>"+cek+"</strong></h5></div>"+
                                                                      "</div>"+
                                                                      "<table class='table table-striped'>"+
                                                                          "<thead>"+
                                                                            "<tr>"+
                                                                              "No Project"
                                                                            "</tr>"+
                                                                          "</thead>"+
                                                                          
                                                                        "</table>"+
                                                                 "</div>"+
                                                            "</div>"+
                                                       "</div>"+
                                                  "</div>"
        $("#loadingProjectSetting"+type).replaceWith(content);
      }else{    
                $("#spinner"+type).hide();
                var content =                       "<div class='row'>"+
                                                       "<div class='col-lg-10 col-md-12'>"+
                                                            "<div class='security-box'>"+
                                                                 "<div class='panel panel-default'>"+
                                                                      "<div class='panel-default' >"+
                                                                        "<div class='col-lg-12' style='background-color:#f1f1f1;'><h5><strong>"+cek+"</strong></h5></div>"+
                                                                      "</div>"+
                                                                      "<table class='table table-striped'>"+
                                                                          "<thead>"+
                                                                            "<tr>"+
                                                                              "<th>#</th>"+
                                                                              "<th>Project Name</th>"+
                                                                              "<th>Creator</th>"+
                                                                              "<th>Original Title</th>"+
                                                                              "<th>Original Genre</th>"+
                                                                              "<th>Original Owner</th>"+
                                                                            "</tr>"+
                                                                          "</thead>"+
                                                                          "<tbody id='append-"+type+"'>"+
                                                                            
                                                                          "</tbody>"+
                                                                        "</table>"+
                                                                 "</div>"+
                                                            "</div>"+
                                                       "</div>"+
                                                  "</div>"
                $("#loadingProjectSetting"+type).append(content);
                var x = -1
                $.each(responseText['data'], function( index, value ) {
                x=x+1
                var num = x+1
                // console.log(responseText['data'][x]);
                $("#append-"+type).append("<tr>"+
                                            "<td>"+num+"</td>"+
                                            "<td>"+responseText['data'][x]['projectName']+"</td>"+
                                            "<td>"+responseText['data'][x]['projectCreator']+"</td>"+
                                            "<td>"+responseText['data'][x]['originalTitle']+"</td>"+
                                            "<td>"+responseText['data'][x]['originalGenre']+"</td>"+
                                            "<td>"+responseText['data'][x]['originalUsername']+"</td>"+
                                          "</tr>");
		})
      }
    },
		error : function(jqXHR, textStatus, errorThrown){
			// console.log(jqXHR);
			// console.log(textStatus);
      window.location = "/error404";
		}
	});
}

