#Adding User Activity or Buy Activity for (logging database purpose)
1. add Activity Controller
2. use save activity function to save activity
3. enjoy.

example code
$activityController = new ActivityController;
$state = $activityController->saveActivity(1, 1, $id); // see details

###details
saveActivity($activityCode, $activityType, $userID, $keterangan = null)

**$activityType** : 1 or 2 (1 for user and 2 for buy activity)

**$activityCode** terdiriri dari :

1. user login (without keterangan) example : saveActivity(1, 1, $userID, $keterangan = null).
2. user updated his/her profile (without keterangan) example : saveActivity(2, 1, $userID, 'password').
3. user upload something (without keterangan) example : saveActivity(3, 1, $userID, 'track').
4. user join something (without keterangan) example : saveActivity(4, 1, $userID, 'group').
5. user doing something (without keterangan) example : saveActivity(5, 1, $userID, 'like').

**$userID** : user id

**$keterangan** : misalkan user mengupload track, maka $keterangan = 'track'

#Accessing Global Variable

$myApp = App::make('serverLocation');
echo $myApp->serverLocation;

##Changing serverLocation
1. go to app/filters.php
2. change ``` $app->serverLocation = 'yourServerLocation';``` in App::before()
3. refresh the page

#You can master PJAX too!! LOL
##   1. set your URL 
```
#!php
before::
<a href="{{ url('home') }}" class="motion-animate">Home</a>

after::
<a data-pjax='yes' id='pjax' href="{{ url('home') }}" class="motion-animate">Home</a>
```

##   2. set your JavaScript too

```
#!JavaScript
before::
$(document).ready() { 
   yourfunction1();
   yourfunction2();
   yourfunction3();
});

after::
$(document).on('ready pjax:success', function() {
   setValue(); //jika ada variable global set value di fungsi! bukan di set saat deklarasi global
   checkElement(); // dibuat agar fungsi tidak di load setiap ganti halaman
   yourfunction1();
   yourfunction2();
   yourfunction3();
});

function checkElement(){
   // jika element ini tidak ada, maka fungsi2 tidak akan ke load
   var elementExists = new Array(
      document.getElementById("loadingMarket1"), //ganti dengan element yang ada di halaman anda
      document.getElementById("loadingMarket2"), //ganti dengan element yang ada di halaman anda
      document.getElementById("loadingMarket3"), //ganti dengan element yang ada di halaman anda
      document.getElementById("loadingMarket4"), //ganti dengan element yang ada di halaman anda
      document.getElementById("loadingMarket5"), //ganti dengan element yang ada di halaman anda
      document.getElementById("loadingMarket6") //ganti dengan element yang ada di halaman anda
   );
   var stop = false;
   for (var i = elementExists.length - 1; i >= 0; i--) {
      if(elementExists[i] == null)
         stop = true;
   };

   if(stop)
   return;
}
```
##   3. put your JavaScript location in default-base.blade.php 

##   4. want example? see in this file
```
#!php
   1. view/layouts/default-base.blade.php //layout default, tempat naro jsnya
   2. view/layouts/navbar-top.blade.php //url yang sudah disetting -> market, home, trackbank
   3. view/store/store.blade.php //page yang akan diambil oleh pjax (TIDAK BOLEH ADA SCRIPT DI SINI!!! G BAKAL KE LOAD!!)
   4. public/js/marketJS/getTrack.js //contoh setting javascript
```

##   5. See it in action (klik market, home and trackbank)!! 
###Got a problem? 
###Can't Load?
Search in google broo! don't ask me. LOL (just kidding. LOL again).




###PAYMENT PROCESS & MODAL
```
#!JavaScript
$('.process-payment').click(function(){
        $(".loader-process").show();

        var id_track = $('#dat-id').text();
        var judul_back = $('#dat-title').text();
        var artist_back = $('#dat-artist').text();
        var price_back = $('#dat-price').text();

        var url = 'studio/paymentprocess';

        var data = 'trackID=' + id_track;
        $.ajax({
          url: url,
          type: 'POST',
          data: data,
          dataType: 'json',

          success: function(result){
            $(".loader-process").hide();
            console.log(result);
            if (result == 1) {
              $('#modalPayment').modal('hide');
              swal({ 
                title: "Thank you!",
                 text: "You can redirected to karaoke page by clicked ok button!",
                  type: "success",
                });

              $('.swal2-confirm').click(function(){
                window.location.href = "studio/karaoke/"+id_track;
              });
            } else if(result == -1){
                $('#modalPayment').modal('hide');
                swal('Oops...Your balance is not enough!','Please, refill your wallet!','error');
            } else{
              swal('Oops...','Something went wrong!','error');
            }
          },

          error: function(jqXHR, textStatus, errorThrown){
              console.log(jqXHR);
              swal('Oops...','Something went wrong!','error');
          }

        });
          

      });
```

```
#!php
<-- ====================== MODAL PAYMENT ============================ -->

<div id="modalPayment" class="vh-center modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header color-gj-popup-report">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Payment Process</h4>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <span id="dat-id" style="display: none;"></span>
                        <p>Song Title : <span id="dat-title"></span></p>
                        <p>Artist : <span id="dat-artist"></span></p>
                        <p>Price : <span id="dat-price"></span></p>
                        <p>Are you sure to use this song?</p>
                    </div>
                 </div>   
            </div>

            <div class="modal-footer">
                <span class="loader-process" style="display: none;"><img src="img/loader-24.gif" /></span>
                <a href="#" class="btn-s-gj bgc-btn" data-dismiss="modal">Cancel</a>
                <a href="#" class="btn-s-gj bgc-btn process-payment">Process</a>
            </div>

        </div>
    </div>
</div>
```