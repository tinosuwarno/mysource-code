-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 09, 2015 at 04:10 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gritjam_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `contributors`
--

CREATE TABLE IF NOT EXISTS `contributors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `contributor_tracks`
--

CREATE TABLE IF NOT EXISTS `contributor_tracks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_contributor` int(11) NOT NULL,
  `id_track` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fungsi`
--

CREATE TABLE IF NOT EXISTS `fungsi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_menu` int(11) NOT NULL,
  `nama` varchar(250) NOT NULL,
  `url_akses` varchar(250) NOT NULL,
  `is_submenu` int(11) NOT NULL,
  `enable` int(11) NOT NULL,
  `keterangan` varchar(250) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=41 ;

--
-- Dumping data for table `fungsi`
--

INSERT INTO `fungsi` (`id`, `id_menu`, `nama`, `url_akses`, `is_submenu`, `enable`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 1, 'Index User', 'user/index', 1, 1, 'Halaman Index User', '2015-11-25 20:55:12', '2015-11-26 07:19:05'),
(2, 1, 'Add User', 'user/add', 1, 1, 'Tambah User', '2015-11-25 20:56:02', '2015-11-26 07:19:15'),
(3, 1, 'View User', 'user/show/', 0, 1, 'Melihat Detail User', '2015-11-25 20:56:57', '2015-11-25 20:58:18'),
(4, 1, 'Edit User', 'user/edit/', 0, 1, 'Mengedit Detail User', '2015-11-25 20:57:54', '2015-11-25 20:57:54'),
(5, 1, 'Delete User', 'user/trash/', 0, 1, 'Menghapus Data User', '2015-11-25 20:58:57', '2015-11-25 20:58:57'),
(6, 1, 'Update User', 'user/update', 0, 1, 'Update Data User', '2015-11-25 21:09:03', '2015-11-25 21:09:03'),
(7, 1, 'Store User', 'user/store', 0, 1, 'Menyimpan Data User', '2015-11-25 21:09:28', '2015-11-25 21:09:28'),
(8, 3, 'Index Group Menu', 'group_menu/index', 1, 1, 'Halaman Index Group Menu', '2015-11-25 21:10:14', '2015-11-26 07:19:30'),
(9, 3, 'Add Group Menu', 'group_menu/add', 1, 1, 'Tambah Group Menu', '2015-11-25 21:11:12', '2015-11-26 00:17:58'),
(10, 3, 'View Group Menu', 'group_menu/show/', 0, 1, 'Melihat Detail Group Menu', '2015-11-25 21:13:22', '2015-11-25 21:13:22'),
(11, 3, 'Edit Group Menu', 'group_menu/edit/', 0, 1, 'Mengedit Detail Group Menu', '2015-11-25 21:13:58', '2015-11-25 21:13:58'),
(12, 3, 'Delete Group Menu', 'group_menu/trash/', 0, 1, 'Menghapus Data Group Menu', '2015-11-25 21:14:41', '2015-11-25 21:14:41'),
(13, 3, 'Remove User From Group Menu', 'group_menu/removeUserGroupMenu/', 0, 1, 'Menghapus User Dari Group Menu', '2015-11-25 21:16:24', '2015-11-25 21:16:24'),
(14, 3, 'Disable Group Menu', 'group_menu/disableGroupMenu/', 0, 1, 'Menonaktifkan Group Menu', '2015-11-25 21:17:57', '2015-11-25 21:17:57'),
(15, 3, 'Enable Group Menu', 'group_menu/enableGroupMenu/', 0, 1, 'Mengaktifkan Group Menu', '2015-11-25 21:18:37', '2015-11-25 21:18:37'),
(16, 3, 'Update Group Menu', 'group_menu/update', 0, 1, 'Update Data Group Menu', '2015-11-25 21:19:30', '2015-11-25 21:19:30'),
(17, 3, 'Store Group Menu', 'group_menu/store', 0, 1, 'Menyimpan Data Group Menu', '2015-11-25 21:20:03', '2015-11-25 21:20:03'),
(18, 3, 'Add User To Group Menu', 'group_menu/addUserGroupMenu', 0, 1, 'Tambah User ke Group Menu', '2015-11-25 21:20:46', '2015-11-25 21:20:46'),
(19, 2, 'Index Menu', 'menu/index', 1, 1, 'Halaman Index Menu', '2015-11-25 21:22:28', '2015-11-26 07:19:38'),
(20, 2, 'Add Menu', 'menu/add', 1, 1, 'Tambah Menu', '2015-11-25 21:23:19', '2015-11-26 00:49:24'),
(21, 2, 'View Menu', 'menu/show/', 0, 1, 'Melihat Detail Menu', '2015-11-25 21:23:43', '2015-11-25 21:23:43'),
(22, 2, 'Edit Menu', 'menu/edit/', 0, 1, 'Mengedit Detail Menu', '2015-11-25 21:24:09', '2015-11-25 21:24:09'),
(23, 2, 'Delete Menu', 'menu/trash/', 0, 1, 'Menghapus Data Menu', '2015-11-25 21:24:39', '2015-11-25 21:24:39'),
(24, 2, 'Remove Group Menu From Menu', 'menu/removeMenuGroupMenu/', 0, 1, 'Menghapus Group Menu Dari Menu', '2015-11-25 21:27:49', '2015-11-25 21:27:49'),
(25, 2, 'Disable Menu', 'menu/disableMenu/', 0, 1, 'Menonaktifkan Menu', '2015-11-25 21:28:26', '2015-11-25 21:28:26'),
(26, 2, 'Enable Menu', 'menu/enableMenu/', 0, 1, 'Mengaktifkan Menu', '2015-11-25 21:29:03', '2015-11-25 21:29:03'),
(27, 2, 'Update Menu', 'menu/update', 0, 1, 'Update Data Menu', '2015-11-25 21:30:32', '2015-11-25 21:30:32'),
(28, 2, 'Store menu', 'menu/store', 0, 1, 'Menyimpan Data Menu', '2015-11-25 21:31:29', '2015-11-25 21:31:29'),
(29, 2, 'Add Group Menu to Menu', 'menu/addMenuGroupMenu', 0, 1, 'Tambah Group Menu ke Menu', '2015-11-25 21:32:35', '2015-11-25 21:32:35'),
(30, 4, 'Index Fungsi', 'fungsi/index', 1, 1, 'Halaman Index Fungsi', '2015-11-25 21:33:08', '2015-11-26 07:19:51'),
(31, 4, 'Add Fungsi', 'fungsi/add', 1, 1, 'Tambah Fungsi', '2015-11-25 21:33:27', '2015-12-01 01:10:31'),
(32, 4, 'View Fungsi', 'fungsi/show/', 0, 1, 'Melihat Detail Fungsi', '2015-11-25 21:35:37', '2015-11-25 21:35:37'),
(33, 4, 'Edit Fungsi', 'fungsi/edit/', 0, 1, 'Mengedit Detail Fungsi', '2015-11-25 21:36:06', '2015-12-01 01:10:39'),
(34, 4, 'Delete Fungsi', 'fungsi/trash/', 0, 1, 'Menghapus Data Fungsi', '2015-11-25 21:36:53', '2015-11-25 21:36:53'),
(35, 4, 'Add Fungsi of Menu for Group Menu', 'fungsi/addFungsi/', 0, 1, 'Menambah Fungsi Untuk Group Menu', '2015-11-25 21:44:44', '2015-11-25 21:44:44'),
(36, 4, 'Disable Fungsi', 'fungsi/disableFungsi/', 0, 1, 'Menonaktifkan Fungsi', '2015-11-25 21:45:47', '2015-11-25 21:45:47'),
(37, 4, 'Enable Fungsi', 'fungsi/enableFungsi/', 0, 1, 'Mengaktifkan Fungsi', '2015-11-25 21:46:48', '2015-11-25 21:46:48'),
(38, 4, 'Update Fungsi', 'fungsi/update', 0, 1, 'Update Data Fungsi', '2015-11-25 21:47:45', '2015-11-25 21:47:45'),
(39, 4, 'Store Fungsi', 'fungsi/store', 0, 1, 'Menyimpan Data Fungsi', '2015-11-25 21:48:06', '2015-11-25 21:48:06'),
(40, 4, 'Add Fungsi for Menu of Group Menu', 'fungsi/addMenuGroupMenuFungsi', 0, 1, 'Add Fungsi for Menu of Group Menu', '2015-11-25 21:48:57', '2015-11-25 21:48:57');

-- --------------------------------------------------------

--
-- Table structure for table `genres`
--

CREATE TABLE IF NOT EXISTS `genres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `genre` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `group_menu`
--

CREATE TABLE IF NOT EXISTS `group_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `enable` int(11) NOT NULL,
  `keterangan` varchar(250) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `group_menu`
--

INSERT INTO `group_menu` (`id`, `nama`, `enable`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 'root', 1, 'Super Admin', '2015-11-25 20:24:15', '2015-11-26 07:38:14'),
(2, 'administrator', 0, 'Administrator', '2015-11-25 20:30:44', '2015-12-01 03:04:10'),
(3, 'user', 1, 'Web User', '2015-11-25 20:31:43', '2015-11-25 20:32:05');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `enable` int(11) NOT NULL,
  `keterangan` varchar(250) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `nama`, `enable`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 'User', 1, 'User Management', '2015-11-25 20:32:32', '2015-11-25 20:33:21'),
(2, 'Menu', 1, 'Menu Management', '2015-11-25 20:32:49', '2015-11-25 20:33:16'),
(3, 'Group Menu', 1, 'Group Menu Management', '2015-11-25 20:33:06', '2015-11-25 20:33:06'),
(4, 'Fungsi', 1, 'Fungsi Management', '2015-11-25 20:33:34', '2015-12-01 01:14:07');

-- --------------------------------------------------------

--
-- Table structure for table `menu_group_menu`
--

CREATE TABLE IF NOT EXISTS `menu_group_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_menu` int(11) NOT NULL,
  `id_groupmenu` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `menu_group_menu`
--

INSERT INTO `menu_group_menu` (`id`, `id_menu`, `id_groupmenu`, `created_at`, `updated_at`) VALUES
(1, 4, 1, '2015-11-25 20:33:50', '2015-11-25 20:33:50'),
(2, 3, 1, '2015-11-25 20:33:56', '2015-11-25 20:33:56'),
(3, 2, 1, '2015-11-25 20:34:01', '2015-11-25 20:34:01'),
(4, 1, 1, '2015-11-25 20:34:07', '2015-11-25 20:34:07'),
(5, 4, 2, '2015-11-29 23:23:18', '2015-11-29 23:23:18'),
(6, 2, 2, '2015-11-30 00:54:02', '2015-11-30 00:54:02');

-- --------------------------------------------------------

--
-- Table structure for table `menu_group_menu_fungsi`
--

CREATE TABLE IF NOT EXISTS `menu_group_menu_fungsi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_menugroupmenu` int(11) NOT NULL,
  `id_fungsi` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=67 ;

--
-- Dumping data for table `menu_group_menu_fungsi`
--

INSERT INTO `menu_group_menu_fungsi` (`id`, `id_menugroupmenu`, `id_fungsi`, `created_at`, `updated_at`) VALUES
(12, 2, 8, '2015-11-25 23:07:34', '2015-11-25 23:07:34'),
(13, 2, 9, '2015-11-25 23:07:34', '2015-11-25 23:07:34'),
(14, 2, 10, '2015-11-25 23:07:34', '2015-11-25 23:07:34'),
(15, 2, 11, '2015-11-25 23:07:34', '2015-11-25 23:07:34'),
(16, 2, 12, '2015-11-25 23:07:34', '2015-11-25 23:07:34'),
(17, 2, 13, '2015-11-25 23:07:34', '2015-11-25 23:07:34'),
(18, 2, 14, '2015-11-25 23:07:34', '2015-11-25 23:07:34'),
(19, 2, 15, '2015-11-25 23:07:34', '2015-11-25 23:07:34'),
(20, 2, 16, '2015-11-25 23:07:34', '2015-11-25 23:07:34'),
(21, 2, 17, '2015-11-25 23:07:34', '2015-11-25 23:07:34'),
(22, 2, 18, '2015-11-25 23:07:34', '2015-11-25 23:07:34'),
(23, 3, 19, '2015-11-25 23:07:50', '2015-11-25 23:07:50'),
(24, 3, 20, '2015-11-25 23:07:50', '2015-11-25 23:07:50'),
(25, 3, 21, '2015-11-25 23:07:50', '2015-11-25 23:07:50'),
(26, 3, 22, '2015-11-25 23:07:50', '2015-11-25 23:07:50'),
(27, 3, 23, '2015-11-25 23:07:50', '2015-11-25 23:07:50'),
(28, 3, 24, '2015-11-25 23:07:50', '2015-11-25 23:07:50'),
(29, 3, 25, '2015-11-25 23:07:50', '2015-11-25 23:07:50'),
(30, 3, 26, '2015-11-25 23:07:50', '2015-11-25 23:07:50'),
(31, 3, 27, '2015-11-25 23:07:50', '2015-11-25 23:07:50'),
(32, 3, 28, '2015-11-25 23:07:50', '2015-11-25 23:07:50'),
(33, 3, 29, '2015-11-25 23:07:50', '2015-11-25 23:07:50'),
(34, 4, 1, '2015-11-25 23:08:02', '2015-11-25 23:08:02'),
(35, 4, 2, '2015-11-25 23:08:02', '2015-11-25 23:08:02'),
(36, 4, 3, '2015-11-25 23:08:02', '2015-11-25 23:08:02'),
(37, 4, 4, '2015-11-25 23:08:02', '2015-11-25 23:08:02'),
(38, 4, 5, '2015-11-25 23:08:02', '2015-11-25 23:08:02'),
(39, 4, 6, '2015-11-25 23:08:02', '2015-11-25 23:08:02'),
(40, 4, 7, '2015-11-25 23:08:02', '2015-11-25 23:08:02'),
(51, 1, 30, '2015-11-26 23:55:32', '2015-11-26 23:55:32'),
(52, 1, 31, '2015-11-26 23:55:32', '2015-11-26 23:55:32'),
(53, 1, 32, '2015-11-26 23:55:32', '2015-11-26 23:55:32'),
(54, 1, 33, '2015-11-26 23:55:32', '2015-11-26 23:55:32'),
(55, 1, 34, '2015-11-26 23:55:32', '2015-11-26 23:55:32'),
(56, 1, 35, '2015-11-26 23:55:32', '2015-11-26 23:55:32'),
(57, 1, 36, '2015-11-26 23:55:32', '2015-11-26 23:55:32'),
(58, 1, 37, '2015-11-26 23:55:32', '2015-11-26 23:55:32'),
(59, 1, 38, '2015-11-26 23:55:32', '2015-11-26 23:55:32'),
(60, 1, 39, '2015-11-26 23:55:32', '2015-11-26 23:55:32'),
(61, 1, 40, '2015-11-26 23:55:32', '2015-11-26 23:55:32'),
(62, 5, 30, '2015-11-29 23:23:45', '2015-11-29 23:23:45'),
(63, 5, 31, '2015-11-29 23:23:45', '2015-11-29 23:23:45'),
(64, 5, 33, '2015-11-29 23:23:45', '2015-11-29 23:23:45'),
(65, 5, 38, '2015-11-29 23:23:45', '2015-11-29 23:23:45'),
(66, 5, 39, '2015-11-29 23:23:45', '2015-11-29 23:23:45');

-- --------------------------------------------------------

--
-- Table structure for table `performers`
--

CREATE TABLE IF NOT EXISTS `performers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `performer_tracks`
--

CREATE TABLE IF NOT EXISTS `performer_tracks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_performer` int(11) NOT NULL,
  `id_track` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tracks`
--

CREATE TABLE IF NOT EXISTS `tracks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `length` time NOT NULL,
  `type` int(11) NOT NULL,
  `year` varchar(50) NOT NULL,
  `genre` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `is_free` int(11) NOT NULL,
  `is_song` int(11) NOT NULL,
  `is_public` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `track_type`
--

CREATE TABLE IF NOT EXISTS `track_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_data`
--

CREATE TABLE IF NOT EXISTS `user_data` (
  `id` bigint(150) NOT NULL AUTO_INCREMENT,
  `password` varchar(255) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `email` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `username` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `first_name` varchar(100) CHARACTER SET latin1 DEFAULT '',
  `last_name` varchar(25) CHARACTER SET latin1 DEFAULT NULL,
  `surename` varchar(100) CHARACTER SET latin1 DEFAULT '',
  `country` varchar(100) CHARACTER SET latin1 DEFAULT '',
  `province` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `village` varchar(30) DEFAULT NULL,
  `RT` varchar(5) DEFAULT NULL,
  `RW` int(5) DEFAULT NULL,
  `birthday` date DEFAULT '0000-00-00',
  `city` varchar(150) CHARACTER SET latin1 DEFAULT '',
  `sex` char(1) CHARACTER SET latin1 DEFAULT '',
  `biography` text CHARACTER SET latin1,
  `picture` varchar(200) CHARACTER SET latin1 DEFAULT '',
  `picture_id` varchar(255) DEFAULT NULL,
  `picture_guid` varchar(255) DEFAULT NULL,
  `lastlogin` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_update` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `activation_status` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `activation_id` varchar(50) CHARACTER SET latin1 DEFAULT '',
  `activation_forget` varchar(50) CHARACTER SET latin1 DEFAULT '',
  `address` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `homeNo` varchar(4) NOT NULL,
  `addHomeNo` varchar(4) NOT NULL,
  `contact` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `home_phone` varchar(15) NOT NULL,
  `area_code` varchar(7) NOT NULL,
  `facebook` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `twitter` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `user_point` int(11) DEFAULT NULL,
  `join_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `blocked` varchar(3) CHARACTER SET latin1 DEFAULT 'N',
  `is_global_administrator` tinyint(2) DEFAULT NULL,
  `is_hidden` tinyint(2) DEFAULT NULL,
  `user_timezone` varchar(20) DEFAULT NULL,
  `window_live_Id` varchar(25) DEFAULT NULL,
  `skype_id` varchar(25) DEFAULT NULL,
  `ym_id` varchar(25) DEFAULT NULL,
  `im_id` varchar(25) DEFAULT NULL,
  `profesi` varchar(30) DEFAULT NULL,
  `facebook_url` varchar(50) DEFAULT NULL,
  `website` varchar(50) DEFAULT NULL,
  `hobby` varchar(100) DEFAULT NULL,
  `rubrik_favourite` text,
  `cardIdType` varchar(15) NOT NULL,
  `cardId` varchar(50) NOT NULL,
  `remember_token` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_index` (`email`,`username`,`activation_status`,`activation_id`,`blocked`),
  KEY `user_point` (`user_point`),
  FULLTEXT KEY `username` (`username`),
  FULLTEXT KEY `email` (`email`),
  FULLTEXT KEY `first_name` (`first_name`),
  FULLTEXT KEY `last_name` (`last_name`),
  FULLTEXT KEY `surename` (`surename`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user_data`
--

INSERT INTO `user_data` (`id`, `password`, `email`, `username`, `first_name`, `last_name`, `surename`, `country`, `province`, `village`, `RT`, `RW`, `birthday`, `city`, `sex`, `biography`, `picture`, `picture_id`, `picture_guid`, `lastlogin`, `last_update`, `activation_status`, `activation_id`, `activation_forget`, `address`, `homeNo`, `addHomeNo`, `contact`, `home_phone`, `area_code`, `facebook`, `twitter`, `user_point`, `join_date`, `blocked`, `is_global_administrator`, `is_hidden`, `user_timezone`, `window_live_Id`, `skype_id`, `ym_id`, `im_id`, `profesi`, `facebook_url`, `website`, `hobby`, `rubrik_favourite`, `cardIdType`, `cardId`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, '$2y$10$0pw5euzOqXYcp.8JoxD2IeV4MGuDOCoFAckS5V5Wqve/MG4cTw9dW', 'gritjam_root@gritjam.com', 'gritjam_root', 'GritJam', 'GritJam', 'GritJam', 'Indonesia', 'Jawa Barat', NULL, NULL, NULL, '0000-00-00', 'Bogor', '', NULL, '', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0', '', '', NULL, '', '', NULL, '', '', NULL, NULL, NULL, '2015-11-26 03:16:26', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'rX7kdUFuK7DNFVqIOrWUkoOZPby2sSsdOKBOgm8HWrEHhZ51SpuPvBdp5tzC', '2015-11-26 03:16:26', '2015-12-01 03:04:12'),
(2, '$2y$10$292/uzyuvOPPGzq.V.rsYu5vsKB/lCuBxxR4xm0b.BRCDbNlL/6LS', NULL, 'gritjam1', 'GritJam', 'GritJam', '', '', 'Jawa Barat', NULL, NULL, NULL, '0000-00-00', 'Bogor', '', NULL, '', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0', '', '', 'BSI C4 20', '', '', NULL, '', '', NULL, NULL, NULL, '2015-11-30 06:20:10', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'CSa2JguZhgDokLyohx4YQCEJuzGJIkJu77pkkfixiKjARTH69erD41z4eLbL', '2015-11-29 23:20:10', '2015-12-01 04:35:25');

-- --------------------------------------------------------

--
-- Table structure for table `user_group_menu`
--

CREATE TABLE IF NOT EXISTS `user_group_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_groupmenu` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user_group_menu`
--

INSERT INTO `user_group_menu` (`id`, `id_user`, `id_groupmenu`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2015-11-25 20:25:17', '2015-11-25 20:25:17'),
(2, 2, 2, '2015-11-29 23:20:48', '2015-11-29 23:20:48');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
