/*
 * MWS Admin v2.1 - Form Elements Demo JS
 * This file is part of MWS Admin, an Admin template build for sale at ThemeForest.
 * All copyright to this file is hold by Mairel Theafila <maimairel@yahoo.com> a.k.a nagaemas on ThemeForest.
 * Last Updated:
 * December 08, 2012
 *
 */

;(function( $, window, document, undefined ) {

    $(document).ready(function() {

        // PickList
        $.fn.pickList && $('#pickList').pickList();

        // CLEditor
        $.fn.cleditor && $( '#cleditor').cleditor({ width: '100%' });

        // AutoSize
        $.fn.autosize && $( '.autosize' ).autosize();

        // jQuery-UI Autocomplete
        if( $.fn.autocomplete ) {
            var availableTags = ["Alabama","Alaska","Arizona","Arkansas","California","Colorado","Connecticut","Delaware","Florida","Georgia","Hawaii","Idaho","Illinois","Indiana","Iowa","Kansas","Kentucky","Louisiana","Maine","Maryland","Massachusetts","Michigan","Minnesota","Mississippi","Missouri","Montana","Nebraska","Nevada","New Hampshire","New Jersey","New Mexico","New York","North Dakota","North Carolina","Ohio","Oklahoma","Oregon","Pennsylvania","Rhode Island","South Carolina","South Dakota","Tennessee","Texas","Utah","Vermont","Virginia","Washington","West Virginia","Wisconsin","Wyoming"];
            $(".mws-autocomplete").autocomplete({
                source: availableTags
            });
        }

        // ColorPicker
        if( $.fn.ColorPicker ) {
            $(".mws-colorpicker").ColorPicker({
                onSubmit: function (hsb, hex, rgb, el) {
                    $(el).val(hex);
                    $(el).ColorPickerHide();
                },
                onBeforeShow: function () {
                    $(this).ColorPickerSetColor(this.value);
                }
            });
        }

        if( $.fn.spinner ) {

            $('.mws-spinner').spinner();

            $('.mws-spinner-decimal').spinner({
                step: 0.01,
                numberFormat: "n"
            });

            $.widget( "ui.timespinner", $.ui.spinner, {
                options: {
                    // seconds
                    step: 60 * 1000,
                    // hours
                    page: 60
                },
         
                _parse: function( value ) {
                    if ( typeof value === "string" ) {
                        // already a timestamp
                        if ( Number( value ) == value ) {
                            return Number( value );
                        }
                        return +Globalize.parseDate( value );
                    }
                    return value;
                },
         
                _format: function( value ) {
                    return Globalize.format( new Date(value), "t" );
                }
            });

            $( ".mws-spinner-time" ).timespinner({
                value: new Date().getTime()
            });
        }

        /* Chosen Select Box Plugin */
        if( $.fn.select2 ) {
            $("select.mws-select2").select2();
        }

		$("#tagging").select2({tags:[]});

// Returns [{id: 123, text: 'Item 1'}, 
//          {id: 456, text: 'Item 2'},
//          {id: 789, text: 'Item 3'}]		
//var tagsdata = [];

//tagsdata = $('ul#tagsdata li').map(function() {
  // $(this) is used more than once; cache it for performance.
//  var $item = $(this);
 
 // return { 
   // id: $item.data('id'), 
    //text: $item.text()
  //};
//}).get();
		
//$("#tagging-edit").select2({
	//tags: tagsdata
	//,tokenSeparators	: [",", " "]
//})

        $.fn.iButton && $('.ibutton').iButton();

        // Validation
        if( $.validator ) {
            $("#mws-validate").validate({
                rules: {
                    spinner: {
                        required: true,
                        max: 5
                    }
                },
                invalidHandler: function (form, validator) {
                    var errors = validator.numberOfInvalids();
                    if (errors) {
                        var message = errors == 1 ? 'You missed 1 field. It has been highlighted' : 'You missed ' + errors + ' fields. They have been highlighted';
                        $("#mws-validate-error").html(message).show();
                    } else {
                        $("#mws-validate-error").hide();
                    }
                }
            });

            /* Form Wizard */
            var v = $("#mws-wizard-form").validate({
                onsubmit: false
            });
            if ($.fn.mwsWizard) {
                $("#mws-wizard-form").mwsWizard({
                    forwardOnly: false,
                    onLeaveStep: function (index, elem) {
                        return v.form();
                    },
                    onBeforeSubmit: function () {
                        return v.form();
                    }
                });
            }
        }
    });

   //Initialize the validation object which will be called on form submit.
    var validobj = $("#mws-validate").validate({
        ignore: [],
        onkeyup: false,
        errorClass: "error",


        //put error message behind each form element
        errorPlacement: function (error, element) {
            var elem = $(element);

            error.insertAfter(element);
        },

        //When there is an error normally you just add the class to the element.
        // But in the case of select2s you must add it to a UL to make it visible.
        // The select element, which would otherwise get the class, is hidden from
        // view.
        highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            if (elem.hasClass("select2-offscreen")) {
                $("#s2id_" + elem.attr("id") + " a").addClass(errorClass);
            } else {
                elem.addClass(errorClass);
            }
        },

        //When removing make the same adjustments as when adding
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
            if (elem.hasClass("select2-offscreen")) {
                $("#s2id_" + elem.attr("id") + " a").removeClass(errorClass);
            } else {
                elem.removeClass(errorClass);
            }
        }
    });

    //If the change event fires we want to see if the form validates.
    //But we don't want to check before the form has been submitted by the user
    //initially.
    $(document).on("change", ".select2-offscreen", function () {
        if (!$.isEmptyObject(validobj.submitted)) {
            validobj.form();
        }
    });
    
    
    //A select2 visually resembles a textbox and a dropdown.  A textbox when
    //unselected (or searching) and a dropdown when selecting. This code makes
    //the dropdown portion reflect an error if the textbox portion has the
    //error class. If no error then it cleans itself up.
    $(document).on("select2-opening", function (arg) {
        var elem = $(arg.target);
        if ($("#s2id_" + elem.attr("id") + " a").hasClass("error")) {
            //jquery checks if the class exists before adding.
            $(".select2-drop ul").addClass("error");
            //$(".select2-input").addClass("myErrorClass");
        } else {
            $(".select2-drop ul").removeClass("error");
            //$(".select2-input").removeClass("myErrorClass");
        }
    });	
	
}) (jQuery, window, document);