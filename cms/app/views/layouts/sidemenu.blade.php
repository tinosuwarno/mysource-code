<?php 
    $id_user = Auth::user()->id;
    $user_group_menu = UserGroupMenu::where('id_user', '=', $id_user)->get();
    $cek_group_menu = GroupMenu::find($user_group_menu[0]->id_groupmenu);
    $menu_group_menu = MenuGroupMenu::where('id_groupmenu', '=', $user_group_menu[0]->id_groupmenu)->get();
 ?>
<!-- Sidebar Wrapper -->
        <div id="mws-sidebar">
        
            <!-- Hidden Nav Collapse Button -->
            <div id="mws-nav-collapse">
                <span></span>
                <span></span>
                <span></span>
            </div>
            
            <!-- Main Navigation -->
            <div id="mws-navigation">
                <ul>
                    @if($user_group_menu[0]->id_groupmenu != 1)
                        @if($cek_group_menu->enable == 1)
                        @foreach($menu_group_menu as $value)
                            @if(Menu::find($value->id_menu)->enable == 1)
                            <li>
                                <a href="#"><i class="icon-table"></i> {{Menu::find($value->id_menu)->nama}}</a>

                                <?php 
                                    $submenu = MenuGroupMenuFungsi::where('id_menugroupmenu', '=', $value->id)->get();
                                 ?>

                                
                                <ul class="closed">
                                    @foreach($submenu as $value2)
                                    <?php 
                                        $fungsi = Fungsi::find($value2->id_fungsi);
                                     ?>
                                     @if(($fungsi->is_submenu == 1) and ($fungsi->enable == 1))
                                    <li>
                                        <a href="{{ URL::to($fungsi->url_akses) }}" title="{{$fungsi->keterangan}}"><i class="icon-table"></i> {{$fungsi->nama}}</a>
                                    </li>
                                    @endif
                                    @endforeach
                                </ul>
                                
                                
                            </li>
                            @endif
                        @endforeach
                    @endif
                        
                    @endif
                    @if($user_group_menu[0]->id_groupmenu == 1)
                        <?php
                            $menu_root = Menu::all();
                        ?>
                        @foreach($menu_root as $value_root)
                            @if(Menu::find($value_root->id)->enable == 1)
                                <li>
                                    <a href="#"><i class="icon-table"></i> {{$value_root->nama}}</a>

                                    
                                    <?php 
                                        $submenu = Fungsi::where('id_menu', '=', $value_root->id)->get();
                                     ?>
                                    
                                    <ul class="closed">
                                        @foreach($submenu as $value5)
                                        <?php 
                                            $fungsi2 = Fungsi::find($value5->id);
                                         ?>
                                         @if(($fungsi2->is_submenu == 1) and ($fungsi2->enable == 1))
                                        <li>
                                            <a href="{{ URL::to($fungsi2->url_akses) }}" title="{{$fungsi2->keterangan}}"><i class="icon-table"></i> {{$fungsi2->nama}}</a>
                                        </li>
                                        @endif
                                        @endforeach
                                    </ul>
                                    
                                    
                                </li>
                            @endif
                        @endforeach

                    @endif
                    
                    <!-- <li>
                        <a href="#"><i class="icon-users"></i> Users</a>
                        <ul class="closed">
                            <li>
                                <a href="{{ URL::to('user/index') }}"><i class="icon-table"></i> List of user</a>
                            </li>
                            <li>
                                <a href="{{ URL::to('user/add') }}"><i class="icon-plus"></i> Add User</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="icon-users"></i> List Test</a>
                        <ul class="closed">
                            <li>
                                <a href="{{ URL::to('group_menu/index') }}"><i class="icon-table"></i> Group Menu</a>
                            </li>
                            <li>
                                <a href="{{ URL::to('menu/index') }}"><i class="icon-table"></i> Menu</a>
                            </li>
                            <li>
                                <a href="{{ URL::to('fungsi/index') }}"><i class="icon-table"></i> Fungsi</a>
                            </li>
                        </ul>
                    </li> -->
                </ul>
            </div>
        </div>