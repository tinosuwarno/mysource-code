@extends('layouts.default')

@section('title')
    Homepage Section 2 
@overwrite

@section('css')

@stop

@section('content')
    <div class="mws-panel grid_8">
                    <div class="mws-panel-header">
                        <span><i class="icon-home"></i> Section 2 (Update Data)</span>
                    </div>

                   <!-- <br>
                    <a class="btn btn-success" href="{{ URL::to('homepage/section1/edit') }}">
                                <span>Edit</span>
                    </a>
                    <br>-->
                    

                    <div class="mws-panel-body no-padding">
                        @if(Session::has('message_success'))
                            <div class="mws-form-message success">{{ Session::get('message_success') }}</div>
                        @endif
                        @if(Session::has('message_error'))
                            <div class="mws-form-message error">{{ Session::get('message_error') }}</div>
                        @endif
                    </div>

                    <div class="mws-panel-body no-padding">
                         {{ Form::model($data, array('id' => 'formEdit', 'class' => 'mws-form','url' => 'homepage/section2/update', 'enctype' => 'multipart/form-data')) }}
                        <input id="id" name="id" type="hidden" value="{{$data->id}}">
                        <div class="mws-form-inline">

                            <div class="mws-form-row">
                                <label class="mws-form-label">Slider Content</label>
                                <div class="mws-form-item">
                                    {{ Form::textarea('teks_content', null, array('class' => 'large', 'id' => 'cleditor')) }}
                                </div>
                            </div>

                            <div class="mws-form-row">
                                <label class="mws-form-label">Image for Section 2 Slider</label>
                                <div class="mws-form-item">
                                <div class="mws-form-cols">
                                    <div class="mws-form-col-2-8">
                                        {{ Form::file('image', array('id'=>'img','class'=>'')) }}
                                    </div>
                                    <div class="mws-form-col-2-8">
                                        <img src="{{asset('images/homepage/section2/'.$data->image)}}">
                                    </div>
                                </div>
                                </div>
                            </div>

                            
                                
                                    <div class="mws-form-row">
                                        <label class="mws-form-label">Slider Order</label>
                                        <div class="mws-form-item">
                                        <div class="mws-form-cols">
                                            <div class="mws-form-col-1-8">
                                                {{ Form::text('order_number', null, array('id' => 'ord','class' => 'small', 'placeholder' => 'Order')) }}
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                
                           

                            
                                
                                    <div class="mws-form-row">
                                        <label class="mws-form-label">Status</label>
                                        <div class="mws-form-item">
                                        <div class="mws-form-cols">
                                            <div class="mws-form-col-2-8">
                                                {{ Form::select('status', array('1' => 'Active', '0' => 'Not Active'), $data->status)}}
                                                <!-- <select id="status" class="small" name="status">
                                                        <option value="1">Active</option>
                                                        <option value="0">Not Active</option>
                                                </select> -->
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                
                            

                        </div>
                            <div class="mws-button-row">
                                <input type="submit" value="Update" class="btn btn-danger">
                            </div>
                        {{ Form::close() }}
                    </div>
                </div>
@stop

@section('scripts')
<script>
    $(document).ready(function() {


        // Data Tables
        if( $.fn.dataTable ) {
            $(".mws-datatable").dataTable();
            $(".mws-datatable-fn").dataTable({
                sPaginationType: "full_numbers"
            });
        }

    });
</script>
@stop