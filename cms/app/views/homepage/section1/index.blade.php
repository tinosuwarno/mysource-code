@extends('layouts.default')

@section('title')
    Homepage Section 1
@overwrite

@section('css')

@stop

@section('content')
    <div class="mws-panel grid_8">
                    <div class="mws-panel-header">
                        <span><i class="icon-home"></i> Section 1</span>
                    </div>

                   <!-- <br>
                    <a class="btn btn-success" href="{{ URL::to('homepage/section1/edit') }}">
                                <span>Edit</span>
                    </a>
                    <br>-->
                    

                    <div class="mws-panel-body no-padding">
                        @if(Session::has('message_success'))
                            <div class="mws-form-message success">{{ Session::get('message_success') }}</div>
                        @endif
                        @if(Session::has('message_error'))
                            <div class="mws-form-message error">{{ Session::get('message_error') }}</div>
                        @endif
                    </div>

                    <div class="mws-panel-body no-padding">
                         {{ Form::open(array('id' => 'formUpdate','url' => 'homepage/section1/update', 'class' => 'mws-form', 'enctype' => 'multipart/form-data', 'method' => 'post')) }}
                        
                            @foreach($section1 as $key => $value)
                            <div class="mws-form-row">
                                <label class="mws-form-label">Section 1 Content</label>
                                <div class="mws-form-item">
                                    <textarea name="content_1" id="cleditor" class="large">{{$value->text_content}}</textarea>
                                </div>
                            </div>

                            <div class="mws-form-row">
                                <label class="mws-form-label">Image/Video for Section 1 Background</label>
                                <div class="mws-form-item">
                                    {{ Form::file('background', array('id'=>'','class'=>'')) }}
                                </div>
                                <label class="mws-form-label">Current Image Background : <a target="_blank" href="{{$value->img_background}}">{{$value->img_background}}</a></label>
                            </div>
                            @endforeach   
                            <div class="mws-button-row">
                                <input type="submit" value="Update" class="btn btn-danger">
                            </div>
                        {{ Form::close() }}
                    </div>
                </div>
@stop

@section('scripts')
<script>
    $(document).ready(function() {


        // Data Tables
        if( $.fn.dataTable ) {
            $(".mws-datatable").dataTable();
            $(".mws-datatable-fn").dataTable({
                sPaginationType: "full_numbers"
            });
        }

    });
</script>
@stop