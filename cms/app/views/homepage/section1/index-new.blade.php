@extends('layouts.default')

@section('title')
    Homepage Section 1 (New Slider)
@overwrite

@section('css')

@stop

@section('content')
<div class="mws-panel grid_8">
                    <div class="mws-panel-header">
                        <span><i class="icon-home"></i> Section 1 (New Slider)</span>
                    </div>

                    
                    <div class="mws-panel-body no-padding">
                        @if(Session::has('message_success'))
                            <div class="mws-form-message success">{{ Session::get('message_success') }}</div>
                        @endif
                        @if(Session::has('message_error'))
                            <div class="mws-form-message error">{{ Session::get('message_error') }}</div>
                        @endif
                    </div>
                    <a class="btn btn-success" href="{{ URL::to('homepage/section1/add-new') }}" style="margin-bottom: 5px;margin-top: 5px;">
                                <span>Add New Content</span>
                    </a>

                    <div class="mws-panel-body no-padding">
                         <table class="mws-datatable-fn mws-table">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Content</th>
                                    <th>Image</th>
                                    <th>Ordering</th>
                                    <th>Status</th> 
                                    <th>Action</th>

                                </tr>
                            </thead>
                            <tbody>
                                <?php $count = 1; ?>
                                @foreach($section_data as $dat)
                                <tr>
                                    <td>{{$count}}</td>
                                    <td>{{$dat->teks_content}}</td>
                                    <td><img src="{{asset('/images/homepage/section1/'.$dat->image)}}" width="100" height="30"></td>
                                    <td>{{$dat->order_number}}</td>
                                    <td>
                                        <?php
                                            if ($dat->status == 0) {
                                                echo "Not Active";
                                            } else{
                                                echo "Active";
                                            }
                                        ?>

                                    </td>   
                                    <td>
                                        <center>
                                        <span class="btn-group">
                                            <a href="{{ URL::to('homepage/section1/edit-new/' . $dat->id) }}" class="btn btn-small"><i class="icon-pencil"></i></a>
                                            <a href="{{ URL::to('homepage/section1/trash-new/' . $dat->id) }}" onclick="return confirm('Are you sure want to delete this data?'); " class="btn btn-small"><i class="icon-trash"></i></a>
                                        </span>
                                    </center>
                                    </td>
                                </tr>
                                    <?php $count++; ?>
                                    @endforeach                  
                                    
                            </tbody>
                        </table>
                    </div>
                </div>
@stop

@section('scripts')
<script>
    $(document).ready(function() {


        // Data Tables
        if( $.fn.dataTable ) {
            $(".mws-datatable").dataTable();
            $(".mws-datatable-fn").dataTable({
                sPaginationType: "full_numbers"
            });
        }

    });
</script>
@stop