@extends('layouts.default')

@section('title')
    Homepage Section 4 
@overwrite

@section('css')

@stop

@section('content')
    <div class="mws-panel grid_8">
                    <div class="mws-panel-header">
                        <span><i class="icon-home"></i> Section 4 (Store Data)</span>
                    </div>

                   <!-- <br>
                    <a class="btn btn-success" href="{{ URL::to('homepage/section1/edit') }}">
                                <span>Edit</span>
                    </a>
                    <br>-->
                    

                    <div class="mws-panel-body no-padding">
                        @if(Session::has('message_success'))
                            <div class="mws-form-message success">{{ Session::get('message_success') }}</div>
                        @endif
                        @if(Session::has('message_error'))
                            <div class="mws-form-message error">{{ Session::get('message_error') }}</div>
                        @endif
                    </div>

                    <div class="mws-panel-body no-padding">
                         {{ Form::open(array('id' => 'formAdd','url' => 'homepage/section4/store', 'class' => 'mws-form', 'enctype' => 'multipart/form-data', 'method' => 'post')) }}
                        
                        <div class="mws-form-inline">

                            <div class="mws-form-row">
                                        <label class="mws-form-label">Title</label>
                                        <div class="mws-form-item">
                                        <div class="mws-form-cols">
                                            <div class="mws-form-col-2-8">
                                                {{ Form::text('title', Input::old('title'), array('id' => 'ord','class' => 'large required', 'placeholder' => 'Title')) }}
                                            </div>
                                        </div>
                                        </div>
                            </div>

                            <div class="mws-form-row">
                                <label class="mws-form-label">Tab Content</label>
                                <div class="mws-form-item">
                                    <textarea name="content" id="cleditor" class="large"></textarea>
                                </div>
                            </div>

                            <div class="mws-form-row">
                                <label class="mws-form-label">Image for Section 4 Tab</label>
                                <div class="mws-form-item">
                                <div class="mws-form-cols">
                                    <div class="mws-form-col-2-8">
                                        {{ Form::file('image', array('id'=>'img','class'=>'')) }}
                                    </div>
                                </div>
                                </div>
                            </div>

                            
                                
                                    <div class="mws-form-row">
                                        <label class="mws-form-label">Tab Order</label>
                                        <div class="mws-form-item">
                                        <div class="mws-form-cols">
                                            <div class="mws-form-col-1-8">
                                                {{ Form::text('order_number', Input::old('order_number'), array('id' => 'ord','class' => 'small required', 'placeholder' => 'Order')) }}
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                
                           

                            
                                
                                    <div class="mws-form-row">
                                        <label class="mws-form-label">Status</label>
                                        <div class="mws-form-item">
                                        <div class="mws-form-cols">
                                            <div class="mws-form-col-2-8">
                                                <select id="status" class="small" name="status">
                                                        <option value="1">Active</option>
                                                        <option value="0">Not Active</option>
                                                </select>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                
                            

                        </div>
                            <div class="mws-button-row">
                                <input type="submit" value="Add" class="btn btn-danger">
                            </div>
                        {{ Form::close() }}
                    </div>
                </div>
@stop

@section('scripts')
<script>
    $(document).ready(function() {


        // Data Tables
        if( $.fn.dataTable ) {
            $(".mws-datatable").dataTable();
            $(".mws-datatable-fn").dataTable({
                sPaginationType: "full_numbers"
            });
        }

    });
</script>
@stop