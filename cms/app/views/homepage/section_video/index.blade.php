@extends('layouts.default')

@section('title')
    Video Section
@overwrite

@section('css')
<link rel="stylesheet" type="text/css" href="{{asset("css/jwplayerSkin.css")}}"> </link>
@stop

@section('content')
<script src='{{asset('js/jwplayer/jwplayer.js')}}'></script>
    <div class="mws-panel grid_8">
                    <div class="mws-panel-header">
                        <span><i class="icon-home"></i> Video Section</span>
                    </div>

                   <!-- <br>
                    <a class="btn btn-success" href="{{ URL::to('homepage/section1/edit') }}">
                                <span>Edit</span>
                    </a>
                    <br>-->
                    

                    <div class="mws-panel-body no-padding">
                        @if(Session::has('message_success'))
                            <div class="mws-form-message success">{{ Session::get('message_success') }}</div>
                        @endif
                        @if(Session::has('message_error'))
                            <div class="mws-form-message error">{{ Session::get('message_error') }}</div>
                        @endif
                    </div>

                    <div class="mws-panel-body no-padding">
                         {{ Form::open(array('id' => 'formUpdate','url' => 'homepage/section_video/upload', 'class' => 'mws-form', 'enctype' => 'multipart/form-data', 'method' => 'post')) }}
                        
                            @foreach($section_video as $key => $value)
                            <div class="mws-form-row">
                                <label class="mws-form-label">Title</label>
                                <div class="mws-form-item">
                                <div class="mws-form-cols">
                                    <div class="mws-form-col-1-8">
                                        {{ Form::text('title', $value->title, array('id' => 'title','class' => 'required', 'placeholder' => 'Title')) }}
                                    </div>
                                </div>
                                </div>
                            </div>

                            <div class="mws-form-row">
                                <label class="mws-form-label">Section Video Caption</label>
                                <div class="mws-form-item">
                                    <textarea name="caption" id="cleditor" class="large">{{$value->caption}}</textarea>
                                </div>
                            </div>

                            <div class="mws-form-row">
                                <label class="mws-form-label">Video for Section Video</label>
                                <div class="mws-form-item">
                                    {{ Form::file('video', array('id'=>'','class'=>'')) }}
                                </div>
                                <label class="mws-form-label">Current Video :{{$value->embedded_location}} </label>
                                <div id="myElement"></div>

                            </div>
                            @endforeach   
                            <div class="mws-button-row">
                                <input type="submit" value="Update" class="btn btn-danger">
                            </div>
                        {{ Form::close() }}
                    </div>
                </div>
                <script type="text/JavaScript">
                    var playerInstance = jwplayer("myElement");
                    playerInstance.setup({
                        file: "{{asset($value->embedded_location)}}"
                    });
                </script>
@stop

@section('scripts')


<script>
    $(document).ready(function() {


        // Data Tables
        if( $.fn.dataTable ) {
            $(".mws-datatable").dataTable();
            $(".mws-datatable-fn").dataTable({
                sPaginationType: "full_numbers"
            });
        }

    });
</script>
@stop