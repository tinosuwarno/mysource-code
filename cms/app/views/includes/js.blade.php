    <!-- JavaScript Plugins -->
    <script src="{{asset('js/libs/jquery-1.8.3.min.js')}}"></script>
    <script src="{{asset('js/libs/jquery.mousewheel.min.js')}}"></script>
    <script src="{{asset('js/libs/jquery.placeholder.min.js')}}"></script>
    <script src="{{asset('custom-plugins/fileinput.js')}}"></script>
    
    <!-- jQuery-UI Dependent Scripts -->
    <script src="{{asset('jui/js/jquery-ui-1.9.2.min.js')}}"></script>
    <script src="{{asset('jui/jquery-ui.custom.min.js')}}"></script>
    <script src="{{asset('jui/js/jquery.ui.touch-punch.js')}}"></script>

    <!-- Plugin Scripts -->
    <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('plugins/colorpicker/colorpicker-min.js')}}"></script>

    <!-- Core Script -->
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/core/mws.js')}}"></script>

    <!-- Themer Script (Remove if not needed) -->
    <script src="{{asset('js/core/themer.js')}}"></script>

    <script src="{{asset('jui/js/timepicker/jquery-ui-timepicker.min.js')}}"></script>
    <script src="{{asset('jui/js/globalize/globalize.js')}}"></script>
    <script src="{{asset('jui/js/globalize/cultures/globalize.culture.en-US.js')}}"></script>
    <script src="{{asset('custom-plugins/picklist/picklist.min.js')}}"></script>
    <script src="{{asset('plugins/autosize/jquery.autosize.min.js')}}"></script>
    <script src="{{asset('plugins/select2/select2.min.js')}}"></script>
    <script src="{{asset('plugins/cleditor/jquery.cleditor.min.js')}}"></script>
    <script src="{{asset('plugins/cleditor/jquery.cleditor.table.min.js')}}"></script>
    <script src="{{asset('plugins/cleditor/jquery.cleditor.xhtml.min.js')}}"></script>
    <script src="{{asset('plugins/cleditor/jquery.cleditor.icon.min.js')}}"></script>
    <script src="{{asset('custom-plugins/wizard/wizard.min.js')}}"></script>
    <script src="{{asset('custom-plugins/wizard/jquery.form.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('plugins/validate/jquery.validate-min.js')}}"></script>
    <script src="{{asset('js/demo/demo.wizard.js')}}"></script>
    <script src="{{asset('js/demo/demo.formelements.js')}}"></script>
    <script src="{{asset('js/demo/demo.widget.js')}}"></script>

