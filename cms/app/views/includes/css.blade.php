<!-- Plugin Stylesheets first to ease overrides -->

<link rel="stylesheet" type="text/css" href="{{asset('plugins/colorpicker/colorpicker.css')}}" media="screen">

<!-- Required Stylesheets -->
<link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}" media="screen">
<link rel="stylesheet" type="text/css" href="{{asset('css/fonts/ptsans/stylesheet.css')}}" media="screen">
<link rel="stylesheet" type="text/css" href="{{asset('css/fonts/icomoon/style.css')}}" media="screen">

<link rel="stylesheet" type="text/css" href="{{asset('css/mws-style.css')}}" media="screen">
<link rel="stylesheet" type="text/css" href="{{asset('css/icons/icol16.css')}}" media="screen">
<link rel="stylesheet" type="text/css" href="{{asset('css/icons/icol32.css')}}" media="screen">

<!-- Demo Stylesheet -->
<link rel="stylesheet" type="text/css" href="{{asset('css/demo.css')}}" media="screen">

<!-- jQuery-UI Stylesheet -->
<link rel="stylesheet" type="text/css" href="{{asset('jui/css/jquery.ui.all.css')}}" media="screen">
<link rel="stylesheet" type="text/css" href="{{asset('jui/jquery-ui.custom.css')}}" media="screen">

<!-- Theme Stylesheet -->
<link rel="stylesheet" type="text/css" href="{{asset('css/mws-theme.css')}}" media="screen">
<link rel="stylesheet" type="text/css" href="{{asset('css/themer.css')}}" media="screen">

<link rel="stylesheet" type="text/css" href="{{asset('custom-plugins/wizard/wizard.css')}}" media="screen">
<link rel="stylesheet" type="text/css" href="{{asset('jui/css/jquery.ui.timepicker.css')}}" media="screen">
<link rel="stylesheet" type="text/css" href="{{asset('custom-plugins/picklist/picklist.css')}}" media="screen">
<link rel="stylesheet" type="text/css" href="{{asset('plugins/select2/select2.css')}}" media="screen">
<link rel="stylesheet" type="text/css" href="{{asset('plugins/ibutton/jquery.ibutton.css')}}" media="screen">
<link rel="stylesheet" type="text/css" href="{{asset('plugins/cleditor/jquery.cleditor.css')}}" media="screen">