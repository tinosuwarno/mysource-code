<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Your Friend Liked Your Post</h2>

		<?php 
			$myApp = App::make('frontLocation'); 
            $front = $myApp->frontLocation ;
		 ?>

		<div>
			<a href="{{URL::to($front.$username)}}">@<?php echo $username; ?></a> has liked your post.
			<br>
			Click <a href="{{URL::to($front.'singlepost/'.$post_id)}}">here</a> to view your post.
		</div>
	</body>
</html>
