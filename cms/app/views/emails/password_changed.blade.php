<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Password Changed</h2>

		<div>
			Your password has been changed on {{Carbon\Carbon::now()->toDayDateTimeString()}}<br/>
		</div>
	</body>
</html>
