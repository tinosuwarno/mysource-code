<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Request Accepted</h2>
		<?php 
			$myApp = App::make('frontLocation'); 
            $front = $myApp->frontLocation ;
		 ?>

		<div>
			<a href="{{URL::to($front.$username)}}">@<?php echo $username; ?></a> has accepted you to be his/her friend.
			<br>
			Click <a href="{{URL::to($front.'friend')}}">here</a> to view your friend list.
		</div>
	</body>
</html>
