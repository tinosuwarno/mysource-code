<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Project Invitation</h2>

		<?php 
			$myApp = App::make('frontLocation'); 
            $front = $myApp->frontLocation ;
		 ?>

		<div>
			You have been invited by <a href="{{URL::to($front.$username)}}">@<?php echo $username; ?></a> to collaborate on project "<strong>{{$project_name}}</strong>".
			<br>
			Click <a href="{{URL::to($front.'studio/karaoke/'.$project_id)}}">here</a>.
		</div>
	</body>
</html>
