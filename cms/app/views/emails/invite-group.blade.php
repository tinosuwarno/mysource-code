<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Group Invitation</h2>

		<?php 
			$myApp = App::make('frontLocation'); 
            $front = $myApp->frontLocation ;
		 ?>

		<div>
			You have been invited by <a href="{{URL::to($front.$admin_username)}}"> @<?php echo $admin_username; ?> </a> to join in group <b> "{{$group_name}}"</b>.
			<br>
			Click <a href="{{URL::to($front.'group/member/'.$group_id)}}">here</a>.
		</div>
	</body>
</html>
