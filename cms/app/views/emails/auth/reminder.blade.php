<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Password Reset</h2>

		<?php 
			$myApp = App::make('frontLocation'); 
            $front = $myApp->frontLocation ;
		 ?>

		<div>
			To reset your password, click here: <a href="{{URL::to($front.'password/reset/'.$token)}}">LINK</a>.<br/>
			This link will expire in 60 minutes.
		</div>
	</body>
</html>
