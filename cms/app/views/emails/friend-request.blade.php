<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Friend Request</h2>

		<?php 
			$myApp = App::make('frontLocation'); 
            $front = $myApp->frontLocation ;
		 ?>

		<div>
			<a href="{{URL::to($front.$username)}}">@<?php echo $username; ?></a> has requested to be your friend.
			<br>
			Please click <a href="{{URL::to($front.'friendRequest')}}">here</a> to accept or reject this request.
		</div>
	</body>
</html>
