<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Join Request Group </h2>
		<div>
			You have been messages from <a href="{{URL::to($front.$username)}}"> @<?php echo $username; ?> </a> to join request in group <b> "{{$group_name}}"</b>.
			<br>
			Click <a href="{{URL::to($front.'group/member/'.$group_id)}}">here</a>.
		</div>
	</body>
</html>
