<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{asset('favicon.png')}}">

    <title>Login</title>

    <!-- Bootstrap core CSS -->
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{asset('css/signin.css')}}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="container">

      <form class="form-signin">
        <table>
        <tr>
          <td colspan="3"><h2 class="form-signin-heading form-control">Please sign in</h2></td>
        </tr>
        <tr>
          <td><label for="inputEmail" class="sr-only form-control">Email</label></td>
          <td>:</td>
          <td><input type="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus></td>
        </tr>
        <tr>
          <td><label for="inputPassword" class="sr-only form-control">Password</label></td>
          <td>:</td>
          <td><input type="password" id="inputPassword" class="form-control" placeholder="Password" required></td>
        </tr>
        <tr>
          <td colspan="3">
            <div class="checkbox form-control">
              <label>
                <input type="checkbox" value="remember-me"> Remember me
              </label>
            </div>
          </td>
        </tr>
        <tr>
          <td colspan="3"><button class="btn btn-lg btn-success btn-block" type="submit">Sign in</button></td>
        </tr>
      </table>
      </form>

      <center><p><a href="#">Register here</a></p></center>
      <center><p>Login with: </p></center>
      <center><a class="btn btn-lg btn-danger" href="{{URL::to('login_oauth_gplus')}}">Google+</a>&nbsp;<a class="btn btn-lg btn-primary" href="{{URL::to('login_oauth_fb')}}">Facebook</a></p></center>

    </div> <!-- /container -->

  </body>
</html>