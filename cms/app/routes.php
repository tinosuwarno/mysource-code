<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


//Explore
Route::group(['prefix' => 'explore'], function() {
	Route::post('getFriendExplore1', 'ExploreController@getFriendExploreGet');
	Route::post('getFriendExplore2', 'ExploreController@getFriendExplorePost');
	Route::post('EventExplore', 'ExploreController@EventExplore');
	Route::post('EventExplore2', 'ExploreController@EventExplore2');
	Route::post('invitationExplore', 'ExploreController@invitationExplore');
	Route::post('invitationExplore2', 'ExploreController@invitationExplore2');
	Route::post('invitationExploreAbout', 'ExploreController@invitationExploreAbout');
});

//Event
Route::group(['prefix' => 'event'], function() {
	Route::post('winner', 'EventController@eventWinner');
	Route::post('checkMyProject', 'EventController@checkMyProject');
	Route::post('checkEventID', 'EventController@checkEventID');
	Route::post('eventDetailsDescription', 'EventController@eventDetailsDescription');
	Route::post('SongEventRelease', 'EventController@SongEventRelease');
	Route::post('winnerGroupEvent', 'EventController@winnerGroupEvent');
	Route::post('submitEventWinner', 'EventController@submitEventWinner');
	Route::post('sendEmailToEventWinner', 'EventController@sendEmailToEventWinner');
	Route::post('participatedEventNotReleaseSong', 'EventController@participatedEventNotReleaseSong');
	Route::post('hitungSharedEvent', 'EventController@hitungSharedEvent');
	Route::post('checkStatusGroupEventID', 'EventController@checkStatusGroupEventID');
	Route::post('LandingEvent', 'EventController@LandingEvent');
	Route::post('LandingNewEvent', 'EventController@LandingNewEvent');
	Route::post('getDataEvent', 'EventController@getDataEvent');
});

