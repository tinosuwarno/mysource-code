<?php
/**
 * Created by PhpStorm.
 * User: chrisbjr
 * Date: 6/26/14
 * Time: 7:52 PM
 */

namespace Chrisbjr\ApiGuard;

use Chrisbjr\ApiGuard\Transformers\ApiKeyTransformer;

class ApiKeyController extends ApiGuardController
{

    protected $apiMethods = [
        'doLogin_front_user' => [
            'keyAuthentication' => false
        ],
        'doLogout_front_user' => [
            'keyAuthentication' => true
        ],
        'sosialLoginAuth' => [
            'keyAuthentication' => false
        ],
        'getUserDataMamen' => [
            'keyAuthentication' => true
        ],
        'loginDesktop' => [
            'keyAuthentication' => false
        ]
    ];

    // public function sosialLoginAuth_old(){
    //     // return \Input::all();
    //     $email              = \Input::get('email');
    //     $nickname           = \Input::get('nickname');
    //     $name               = \Input::get('name');
    //     $profile_image      = \Input::get('profile_image');

    //     $data = \User::where('email', '=', $email)->first();
    //     // return json_encode($data);
    //     if(!isset($data)){
    //         $user = new \User;
    //         $user->email            = $email;
    //         $user->nickname         = $nickname;
    //         $user->profile_image    = $profile_image;
    //         $user->name             = $name;
    //         $user->level_user       = 0;
    //         $user->save();

    //         $id = $user->id;
    //     }
    //     else{
    //         $id = $data->id;
    //     }
    //     // return $id;

    //     $apiKey = ApiKey::where('user_id', '=', $id)->first();
    //     if(!isset($apiKey)){ //kalo apiKey kosong atau belum diset

    //         //add level user to api
    //         $userController = new \UserController();
    //         $level = $userController->getMyColoumn($id, 'level_user');

    //         $apiKey                 = new ApiKey;
    //         $apiKey->key            = $apiKey->generateKey();
    //         $apiKey->user_id        = \Input::json('user_id', $id);
    //         $apiKey->level          = \Input::json('level', $level);
    //         $apiKey->ignore_limits  = \Input::json('ignore_limits', 0);
    //     }
    //     else{
    //         $apiKey->generateKey();
    //     }

    //     if(!$apiKey->save()){ //cek save auth
    //         return 0;
    //     }

    //     return $this->response->withArray([
    //         'apiKey'    => $apiKey['key']
    //     ]);
    // }

    public function sosialLoginAuth(){
        // return \Input::all();
        $email              = \Input::get('email');
        $nickname           = \Input::get('nickname');
        $name               = \Input::get('name');
        $profile_image      = \Input::get('profile_image');

        $data = \User::where('email', '=', $email)->first();
        // return json_encode($data);
        if(!isset($data)){
            $parts = explode("@", $email);
            $username = $parts[0];
            $usernameCek = str_replace('.', '_', $username);

            while(sizeof(\User::where('username', '=', $usernameCek)->get())>0) {
                # kalau username udah ada
                $usernameCek = $usernameCek.rand(0,99);
            }

            $user = new \User;
            $user->email            = $email;
            $user->nickname         = $nickname;
            $user->profile_image    = $profile_image;
            $user->name             = $name;
            $user->level_user       = 0;
            $user->username = $usernameCek;
            $user->activation_status = 1;
            // $user->credit = 50000;
            $user->save();

            $id = $user->id;
        }
        else{
            $id = $data->id;
        }
        // return $id;

        $apiKey = ApiKey::where('user_id', '=', $id)->first();
        if(!isset($apiKey)){ //kalo apiKey kosong atau belum diset

            //add level user to api
            $userController = new \UserController();
            $level = $userController->getMyColoumn($id, 'level_user');

            $apiKey                 = new ApiKey;
            $apiKey->key            = $apiKey->generateKey();
            $apiKey->user_id        = \Input::json('user_id', $id);
            $apiKey->level          = \Input::json('level', $level);
            $apiKey->ignore_limits  = \Input::json('ignore_limits', 0);
        }
        else{
            $apiKey->generateKey();
        }

        if(!$apiKey->save()){ //cek save auth
            return 0;
        }

        //cek email notif setting
        $checkEmailSetting = \EmailNotifSetting::where('user_id', $id)->first();
        if (sizeof($checkEmailSetting)==0) {
            $emailSetting = new \EmailNotifSetting;
            $emailSetting->user_id = $id;
            $emailSetting->save();
        }

        $checkAppNotif = \AppNotif::where('user_id', $id)->first();
            if (sizeof($checkAppNotif)==0) {
                $appNotif = new \AppNotif;
                $appNotif->user_id = $id;
                $appNotif->save();
            }

        $checkUserGroup = \UserGroupMenu::where('id_user', $id)->first();
        if (sizeof($checkUserGroup)==0) {
            $user_group_menu = new \UserGroupMenu;
            $user_group_menu->id_user = $id;
            $user_group_menu->id_groupmenu = 3;
            $user_group_menu->save();
        }

        return $this->response->withArray([
            'apiKey'    => $apiKey['key']
        ]);
    }

    public function doLogin_front_user(){
        // return \Input::all();
        $password = \Input::get('inputPassword');
        $email = \Input::get('inputEmail');

        // $encryptController = new \EncryptController();
        // $passwordDecrypt = $encryptController->encryptValue($password, 2, 3);
        // $emailDecrypt = $encryptController->encryptValue($email, 2, 2);
        // return [$passwordDecrypt, $emailDecrypt];

        $userdata = array(
            'email' => $email,
            'password' => $email . $password . $email
        );
        // return $userdata;
        // $hash = \Hash::make($emailDecrypt . $passwordDecrypt . $emailDecrypt);
        // return [$hash];

        if(\Auth::attempt($userdata)){
            $id = \Auth::user()->id;
            $apiKey = ApiKey::where('user_id', '=', $id)->first();
            // return [$apiKey];

            if(!isset($apiKey)){ //kalo apiKey kosong atau belum diset

                //add level user to api
                $userController = new \UserController();
                $level = $userController->getMyColoumn($id, 'level_user');

                $apiKey                 = new ApiKey;
                $apiKey->key            = $apiKey->generateKey();
                $apiKey->user_id        = \Input::json('user_id', $id);
                $apiKey->level          = \Input::json('level', $level);
                $apiKey->ignore_limits  = \Input::json('ignore_limits', 0);
            }
            else{
                $apiKey->generateKey();
            }

            if(!$apiKey->save()){ //cek save auth
                return -1;
            }
            $user = \Auth::user();
            // return $user;

            $activityController = new \ActivityController();
            $state = $activityController->saveActivity(1, 1, $id);

            $encryptController = new \EncryptController();

            $method = [
                'method' => 'encrypt',
                'prime' => 1
            ];

            $userData = [
                'userIDSession' => $id,
                'apiKeySession' => $apiKey['key'],
                'value' => $id
            ];

            $id = $encryptController->encryptValue($user['id'], 1, 1);
            $newID = $encryptController->newEncryptValue($method, $userData);

            $checkEmailSetting = \EmailNotifSetting::where('user_id', $user['id'])->first();
            if (sizeof($checkEmailSetting)==0) {
                $emailSetting = new \EmailNotifSetting;
                $emailSetting->user_id = $user['id'];
                $emailSetting->save();
            }

            $checkAppNotif = \AppNotif::where('user_id', $user['id'])->first();
            if (sizeof($checkAppNotif)==0) {
                $appNotif = new \AppNotif;
                $appNotif->user_id = $user['id'];
                $appNotif->save();
            }

            $checkUserGroup = \UserGroupMenu::where('id_user', $user['id'])->first();
            if (sizeof($checkUserGroup)==0) {
                $user_group_menu = new \UserGroupMenu;
                $user_group_menu->id_user = $user['id'];
                $user_group_menu->id_groupmenu = 3;
                $user_group_menu->save();
            }

            return $this->response->withArray([
                'id'            => $id,
                'newID'         => $newID,
                'username'      => $user['username'],
                'nickname'      => $user['nickname'],
                'first_name'    => $user['first_name'],
                'profile_image' => $user['profile_image'],
                'name'          => $user['name'],
                'credit'        => $user['credit'],
                'apiKey'        => $apiKey['key'],
                'storage_left'  => $user['storage_left'],
                'storage_limit' => $user['storage_limit']
            ]);
            // return $this->response->withItem($apiKey, new ApiKeyTransformer);
        }
        else{
            // login gagal
            return 0;
        }
    }

    public function doLogout_front_user(){

        $apiKey = \Input::get('apiKey');
        $dataApi = ApiKey::where('key', '=', $apiKey)->first();
        $data = ApiKey::where('user_id', '=', $dataApi['user_id']);
         //print_r($data);

        if($data->delete()){

            return $this->response->withArray([
                'response' => [
                    'code'      => 'SUCCESSFUL',
                    'http_code' => 200,
                    'message'   => 'User was successfuly deauthenticated'
                ]
            ]);
        }
        else{
            return $this->response->withArray([
                'response' => [
                    'code'      => 'Failed',
                    'http_code' => 201,
                    'message'   => 'User not delete'
                ]
            ]);
        }
    }

    public function getUserDataMamen(){
        // return \Input::all();
        $apiKey = \Input::get('apiKey');
        // echo $apiKey;
        $dataApi = ApiKey::where('key', '=', $apiKey)->first();
        $user_id = $dataApi['user_id'];
        $user = \User::find($user_id);

        $activityController = new \ActivityController();
        $state = $activityController->saveActivity(1, 1, $user['id']);

        $encryptController = new \EncryptController();
        $id = $encryptController->encryptValue($user['id'], 1, 1);

        $storageController = new \StorageController();
        $storage = $storageController->myStorage($user['id']);

        $data = [
            'id'            => $id,
            'username'      => $user['username'],
            'nickname'      => $user['nickname'],
            'first_name'    => $user['first_name'],
            'profile_image' => $user['profile_image'],
            'name'          => $user['name'],
            'credit'        => $user['credit'],
            'apiKey'        => $apiKey,
            'storage_left'  => $storage['left'],
            'storage_limit' => $user['storage_limit']
        ];
        return $data;
    }

    public function loginDesktop(){
        // return \Input::all();
        $inputs = \Input::all();

        $email = $inputs['email'];
        $password = $inputs['password'];


        $userdata = array(
                    'email' => $email,
                    'password' => $email.$password.$email
            );

        if(\Auth::attempt($userdata)){
            $id = \Auth::user()->id;
            $apiKey = ApiKey::where('user_id', '=', $id)->first();
            // return [$apiKey];
            $status  = array(
                            'response' => 200,
                            'message' => 'success',
                        );

            if(!isset($apiKey)){ //kalo apiKey kosong atau belum diset

                //add level user to api
                $userController = new \UserController();
                $level = $userController->getMyColoumn($id, 'level_user');

                $apiKey                 = new ApiKey;
                $apiKey->key            = $apiKey->generateKey();
                $apiKey->user_id        = \Input::json('user_id', $id);
                $apiKey->level          = \Input::json('level', $level);
                $apiKey->ignore_limits  = \Input::json('ignore_limits', 0);
            }
            else{
                $apiKey->generateKey();
            }

            if(!$apiKey->save()){ //cek save auth
                return -1;
            }
            $user = \Auth::user();
            // return $user;
            $myApp = \App::make('frontLocation'); 
            $front = $myApp->frontLocation ;

            $gambarprofile = $user['profile_image'];
            if ($gambarprofile != null || $gambarprofile != '') {
                if ($gambarprofile[0].$gambarprofile[1].$gambarprofile[2].$gambarprofile[3]=='http') {
                    $imagefromurl = file_get_contents($gambarprofile);
                    $gambar64 = base64_encode($imagefromurl);
                }
                else {
                    $gambar64 = base64_encode(file_get_contents($front.$gambarprofile));
                }
            }
            else {
                $gambar64 = NULL;
            }

            $encryptController = new \EncryptController;

            $returns = array([
                'id'            => $encryptController->encryptThis('encrypt', 1, $user['id'], $apiKey['key'], $user['id']),
                'username'      => $user['username'],
                'nickname'      => $user['nickname'],
                'first_name'    => $user['first_name'],
                'profile_image' => $gambar64,
                'name'          => $user['name'],
                'apiKey'        => $apiKey['key']
            ]);
        }
        else{
            // login gagal
            $returns = "NULL";
            $status  = array(
                            'response' => 400,
                            'message' => 'email or password error',
                        );
        }

        $hasil = array(
                        'status' => $status,
                        'data' => $returns,
        );

        return \Response::json($hasil);
    }

    public function logoutDesktop(){
        // return \Input::all();
        $input = \Input::all();
        $apiKey = $input['apiKey'];
        $userID = $input['userID'];

        $encryptController = new \EncryptController;
        $userID = $encryptController->encryptThis('decrypt', 1, false, $apiKey, $userID);

        $dataApi = ApiKey::where('key', '=', $apiKey)->first();
        $data = ApiKey::where('user_id', '=', $dataApi['user_id']);
         //print_r($data);

        if($data->delete()){
            $response = [
                'code'      => 'SUCCESSFUL',
                'http_code' => 200,
                'message'   => 'User was successfuly deauthenticated'
            ];
        }
        else{
            $response = [
                'code'      => 'Failed',
                'http_code' => 201,
                'message'   => 'User not delete'
            ];
        }
        return \Response::json($response);
    }
}