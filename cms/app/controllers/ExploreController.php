<?php

use Chrisbjr\ApiGuard\ApiGuardController;
class ExploreController extends ApiGuardController {

	protected $apiMethods = [
        'getFriendExploreGet' => [
            'keyAuthentication' => false
        ],
        'getFriendExplorePost' => [
            'keyAuthentication' => true
        ],
        'EventExplore' => [
            'keyAuthentication' => true
        ],
        'EventExplore2' => [
            'keyAuthentication' => false
        ],
        'invitationExplore' => [
            'keyAuthentication' => true
        ],
        'invitationExplore2' => [
            'keyAuthentication' => false
        ]
    ];

    public function checkCity($userID){
        $userData = User::where('id', '=', $userID)->first();
        return $userData['city'];
    }

	public function getRandomUser($userID, $type){
		$query = DB::table('user_data');
        $query->join('user_group_menu', function($join){
            $join->on('user_data.id', '=', 'user_group_menu.id_user');
        });

        $query->where('user_group_menu.id_groupmenu', '=', 3);
        $query->orderBy(DB::raw('RAND()'));
        
        if(!empty($userID) || $userID != 0)
            $query->where('user_data.id', '!=', $userID);

        if($type == 2){
            if(empty($userID) || $userID == 0) return -2;

            $city = $this->checkCity($userID);
            if(!empty($city) || sizeof($city) != 0){
                $query->where('user_data.city', '=', $city);
            }
        }

        $query->take(3);
        $query->addSelect('user_data.id as userID');
        $query->addSelect('user_data.username as username');
        $query->addSelect('user_data.name as name');
        $query->addSelect('user_data.instrument_main as instrument');
        $query->addSelect('user_data.genre_main as genre');
        // $query->addSelect('user_data.description as description');
        $query->addSelect('user_data.profile_image as profile_image');
        $query->addSelect('user_data.bg_image as bg_image');
        $query->addSelect('user_data.biography as biography');

        $data = $query->get();
        $data = json_decode(json_encode($data), true);

        $encryptController = new EncryptController;
        foreach ($data as $key => $value) {
        	$friendID = $value['userID'];
        	if(!empty($userID) || $userID != 0){
        		$friendCombineController = new FriendCombineController('friend', $userID);
        		$data[$key]['isFriend'] = $friendCombineController->isFriend($userID, $friendID);
        	}
        	$data[$key]['userID'] = $encryptController->encryptValue($friendID, 1,1);
        }
        return $data;
	}

	public function getFriendExploreGet(){
        $type = Input::get('type');
       	$data = $this->getRandomUser(0, $type);
        return $data;
    }

    public function getFriendExplorePost(){
    	$input = Input::all();
    	$userID = $input['userID'];
        $type = $input['type'];

    	$encryptController = new EncryptController;
    	$userID = $encryptController->encryptValue($userID, 2, 1);

       	$data = $this->getRandomUser($userID, $type);
        return $data;
    }

    public function EventExplore(){
        $userID = Input::get('userID');
        $skip = Input::get('skip');
        $take = Input::get('take');
        $orderBy = Input::get('orderBy');
        $optionOrder = Input::get('optionOrder');

        $encryptController = new EncryptController;
        $user_id = $encryptController->encryptValue($userID, 2, 1);

        $time = time();
        $tanggal = (date("Y-m-d"." "."H-m-s",$time));

        $data = $this->dataEventExplore($orderBy,$optionOrder,$tanggal);
        $count = count($data->get());
        
        if($count != 0){
            $dataEvent = $data->skip($skip)->take($take)->get();
            $decode =  json_decode(json_encode($dataEvent), true);
            foreach ($decode as $key => $value) {
              $trackID = $encryptController->encryptValue($value['track_details_id'], 1, 3);
              $groupID = $encryptController->encryptValue($value['group_id'], 1, 5);
              $groupController = new GroupController();
              $likedGroupEvent = $groupController->checkDataLikeEvent($user_id, $value['event_id']);
              $statusMemberEventParticipate = $groupController->statusMemberEventParticipate($user_id,$value['group_id'],$value['event_id']);
              $checkadmin = $groupController->checkAdminGroup($value['group_id'],$user_id);
              $checkStatusMember = $groupController-> checkStatusMember($value['group_id'], $user_id);
              $peserta = sizeof(Project::where('event_id', '=', $value['event_id'])->get());
              $creatorTrack = $groupController->getFriendName($value['creatorTrack']); 

                 $return[] = [
                     'event_title' => $value['event_title'],
                     'event_id' => $value['event_id'],
                     'picture_event' => $value['picture_event'],
                     'groupID' => $groupID,
                     'user_id' => $userID,
                     'description_event' => $value['description_event'],
                     'setting_type' => $value['setting_type'],
                     'event_type' => $value['event_type'],
                     'trackID' => $trackID,
                     'genre_tags' => $value['genre_tags'],
                     'liked' => $value['liked'],
                     'start_time' => $value['start_time'],
                     'end_time' => $value['end_time'],
                     'created_at' => $value['created_at'],
                     'group_name' => $value['group_name'],
                     'picture_group' => $value['picture_group'],
                     'username_creator' => $value['username'],
                     'likedGroupEvent' => $likedGroupEvent,
                     'statusMemberEventParticipate' => $statusMemberEventParticipate,
                     'checkadmin' => $checkadmin,
                     'checkStatusMember' => $checkStatusMember,
                     'shared' => $value['shared'],
                     'peserta' => $peserta,
                     'judulTrack' => $value['judulTrack'],
                     'instrument' => $value['instrument'],
                     'price' => $value['price'],
                     'creatorTrack' => $creatorTrack,
                     'group_type' => $value['group_type'],
                 ];
            }
            $dataReturn[] = [
               'data' => $return,
               'count' => $count,
             ];
           
        }else{
           $dataReturn[] = [
              'count' => $count,
           ]; 
        }

       return $dataReturn;
    }

    public function EventExplore2(){
        $skip = Input::get('skip');
        $take = Input::get('take');
        $orderBy = Input::get('orderBy');
        $optionOrder = Input::get('optionOrder');

        $time = time();
        $tanggal = (date("Y-m-d"." "."H-m-s",$time));

        $data = $this->dataEventExplore($orderBy,$optionOrder,$tanggal);
        $count = count($data->get());
        
        if($count != 0){
            $dataEvent = $data->skip($skip)->take($take)->get();
            $decode =  json_decode(json_encode($dataEvent), true);
            foreach ($decode as $key => $value) {
              $encryptController = new EncryptController;
              $trackID = $encryptController->encryptValue($value['track_details_id'], 1, 3);
              $groupID = $encryptController->encryptValue($value['group_id'], 1, 5);
              $userID = $encryptController->encryptValue($value['user_id'], 1, 1);
              $peserta = sizeof(EventParticipant::where('event_id', '=', $value['event_id'])->where('status', '=', 1)->get());
              $groupController = new GroupController();
              $creatorTrack = $groupController->getFriendName($value['creatorTrack']); 
                 $return[] = [
                     'event_title' => $value['event_title'],
                     'event_id' => $value['event_id'],
                     'picture_event' => $value['picture_event'],
                     'groupID' => $groupID,
                     'user_id' => $userID,
                     'description_event' => $value['description_event'],
                     'setting_type' => $value['setting_type'],
                     'event_type' => $value['event_type'],
                     'trackID' => $trackID,
                     'genre_tags' => $value['genre_tags'],
                     'liked' => $value['liked'],
                     'start_time' => $value['start_time'],
                     'end_time' => $value['end_time'],
                     'created_at' => $value['created_at'],
                     'group_name' => $value['group_name'],
                     'picture_group' => $value['picture_group'],
                     'username_creator' => $value['username'],
                     'likedGroupEvent' => '',
                     'statusMemberEventParticipate' => '',
                     'checkadmin' => '',
                     'checkStatusMember' => '',
                     'peserta' => $peserta,
                     'judulTrack' => $value['judulTrack'],
                     'instrument' => $value['instrument'],
                     'price' => $value['price'],
                     'creatorTrack' => $creatorTrack,
                     'group_type' => $value['group_type'],
                     'shared' => $value['shared'],
                 ];
            }
            
            $dataReturn[] = [
               'data' => $return,
               'count' => $count,
             ];
           
        }else{
           $dataReturn[] = [
              'count' => $count,
           ];      
        }
       return $dataReturn;  
    }

    public function dataEventExplore($orderBy,$optionOrder,$tanggal){
        $event = DB::table('group_event')
            ->join('groups', function($join)
              {
                  $join->on('group_event.group_id', '=', 'groups.id');        
              })
            ->join('user_data', function($join)
              {
                  $join->on('group_event.creator_id', '=', 'user_data.id');        
              })
            ->join('track_details', function($join)
              {
                 $join->on('group_event.track_details_id', '=', 'track_details.id');
              })
            ->join('track', function($join)
              {
                  $join->on('group_event.track_details_id', '=', 'track.track_details_id');
              })
            ->where('end_time', '>', $tanggal)
            ->orderBy('group_event.'.$orderBy, '=', $optionOrder)
            ->addSelect('group_event.title as event_title',
                        'group_event.id as event_id',
                        'group_event.picture_event as picture_event',
                        'group_event.group_id as group_id',
                        'group_event.creator_id as user_id',
                        'group_event.description as description_event',
                        'group_event.setting_type as setting_type',
                        'group_event.event_type as event_type',
                        'group_event.track_details_id as track_details_id',
                        'group_event.tags as genre_tags',
                        'group_event.liked as liked',
                        'group_event.start_time',
                        'group_event.end_time',
                        'group_event.created_at',
                        'group_event.shared',
                        'groups.name as group_name',
                        'groups.picture as picture_group',
                        'user_data.username as username',
                        'track_details.name as judulTrack',
                        'track_details.instrument_name as instrument',
                        'track_details.is_free as price',
                        'track.track_id_create as creatorTrack',
                        'groups.public as group_type');
        return $event;
    }

    public function invitationExplore(){
        $userID = trim(Input::get('userID'));
        $orderBy = trim(Input::get('orderBy'));
        $optionOrder = trim(Input::get('optionOrder'));
        $skip = trim(Input::get('skip'));
        $take = trim(Input::get('take'));

        $encryptController = new EncryptController;
        $groupController = new GroupController();

        $data = $this->dataInvitationExplore($orderBy,$optionOrder);
        $count = count($data->get());
        if($count != 0){
             $dataInvitation =  $data->skip($skip)->take($take)->get();
             $decode = json_decode(json_encode($dataInvitation), true);
             foreach ($decode as $key => $value) {
                  $return[] = [
                      'projectID' => $encryptController->encryptValue($value['project_id'], 1, 9),
                      'userID_creator' => $encryptController->encryptValue($value['project_creator'], 1, 1),
                      'username_creator' => $groupController->getFriendName($value['project_creator']),
                      'project_name' => $value['project_name'],
                      'invited_id' => $value['invited_id'],
                      'create' => $value['create'],
                      'image_project' => $value['image_project'],
                      'description' => $value['description'],
                      'status_project_invite' => $value['status_project_invite'],
                      'myUserID' => $userID,

                  ];
             }
             $dataReturn[] = [
               'data' => $return,
               'count' => $count,
             ];
        }else{
             $dataReturn[] = [
              'count' => $count,
           ];      
        }
        return $dataReturn;
    }

    public function invitationExploreAbout(){
        $userID = trim(Input::get('userID'));
          $encryptController = new EncryptController;
          $id = $encryptController->encryptValue($userID, 2, 1);
        $orderBy = trim(Input::get('orderBy'));
        $optionOrder = trim(Input::get('optionOrder'));
        $skip = trim(Input::get('skip'));
        $take = trim(Input::get('take'));

        $encryptController = new EncryptController;
        $groupController = new GroupController();

        $data = $this->dataInvitationExploreAbout($orderBy,$optionOrder,$id);
        $count = count($data->get());
        if($count != 0){
             $dataInvitation =  $data->skip($skip)->take($take)->get();
             $decode = json_decode(json_encode($dataInvitation), true);
             foreach ($decode as $key => $value) {
                  $return[] = [
                      'projectID' => $encryptController->encryptValue($value['project_id'], 1, 9),
                      'userID_creator' => $encryptController->encryptValue($value['project_creator'], 1, 1),
                      'username_creator' => $groupController->getFriendName($value['project_creator']),
                      'project_name' => $value['project_name'],
                      'invited_id' => $value['invited_id'],
                      'create' => $value['create'],
                      'image_project' => $value['image_project'],
                      'description' => $value['description'],
                      'status_project_invite' => $value['status_project_invite'],
                      'myUserID' => $userID,

                  ];
             }
             $dataReturn[] = [
               'data' => $return,
               'count' => $count,
             ];
        }else{
             $dataReturn[] = [
              'count' => $count,
           ];      
        }
        return $dataReturn;
    }

    public function invitationExplore2(){
        $orderBy = trim(Input::get('orderBy'));
        $optionOrder = trim(Input::get('optionOrder'));
        $skip = trim(Input::get('skip'));
        $take = trim(Input::get('take'));

        $encryptController = new EncryptController;
        $groupController = new GroupController();

        $data = $this->dataInvitationExplore($orderBy,$optionOrder);
        $count = count($data->get());
        if($count != 0){
             $dataInvitation =  $data->skip($skip)->take($take)->get();
             $decode = json_decode(json_encode($dataInvitation), true);
             foreach ($decode as $key => $value) {
                $myUserID = '';
                  $return[] = [
                      'projectID' => $encryptController->encryptValue($value['project_id'], 1, 9),
                      'userID_creator' => $encryptController->encryptValue($value['project_creator'], 1, 1),
                      'username_creator' => $groupController->getFriendName($value['project_creator']),
                      'project_name' => $value['project_name'],
                      'invited_id' => $value['invited_id'],
                      'create' => $value['create'],
                      'image_project' => $value['image_project'],
                      'description' => $value['description'],
                      'status_project_invite' => $value['status_project_invite'],
                      'myUserID' => $myUserID,
                  ];
             }
             $dataReturn[] = [
               'data' => $return,
               'count' => $count,
             ];
        }else{
             $dataReturn[] = [
              'count' => $count,
           ];      
        }
        return $dataReturn;
    }

    public function dataInvitationExplore($orderBy,$optionOrder){
         $invitation = DB::table('project_invite')
            ->join('project', function($join)
              {
                  $join->on('project_invite.project_id', '=', 'project.id');
              })
            ->where('project_invite.status', '=', 0)
            ->where('project_invite.invited_id', '=', 0)
            ->orderBy('project_invite.'.$orderBy, '=', $optionOrder)
            ->addSelect('project_invite.project_id as project_id',
                        'project_invite.creator_id as project_creator',
                        'project_invite.status as status_project_invite',
                        'project_invite.invited_id as invited_id',
                        'project_invite.created_at as create',
                        'project.project_name as project_name',
                        'project.image_project as image_project',
                        'project.description as description'
                        );
        
        return $invitation;
    }

    public function dataInvitationExploreAbout($orderBy,$optionOrder,$id){
         $invitation = DB::table('project_invite')
            ->join('project', function($join)
              {
                  $join->on('project_invite.project_id', '=', 'project.id');
              })
            ->where('project_invite.creator_id', '=', $id)
            ->where('project_invite.status', '=', 0)
            ->where('project_invite.invited_id', '=', 0)
            ->orderBy('project_invite.'.$orderBy, '=', $optionOrder)
            ->addSelect('project_invite.project_id as project_id',
                        'project_invite.creator_id as project_creator',
                        'project_invite.status as status_project_invite',
                        'project_invite.invited_id as invited_id',
                        'project_invite.created_at as create',
                        'project.project_name as project_name',
                        'project.image_project as image_project',
                        'project.description as description'
                        );
        
        return $invitation;
    }
   
}
