<?php

use Chrisbjr\ApiGuard\ApiGuardController;
use Carbon\Carbon;
class EventController extends ApiGuardController {

	protected $apiMethods = [
        'eventWinner' => [
            'keyAuthentication' => true
        ],
        'checkMyProject' => [
            'keyAuthentication' => true
        ],
        'eventDetailsDescription' => [
            'keyAuthentication' => true
        ],
        'participatedEventNotReleaseSong' => [
           'keyAuthentication' => true
        ],
        'LandingEvent' => [
           'keyAuthentication' => true
        ],
        'LandingNewEvent' => [
            'keyAuthentication' => true
        ]

    ];

    public function eventWinner(){
      // return Input::all();
      $input = Input::all();
      $userID = $input['userID'];
      $apiKey = $input['apiKey'];
      $skip = $input['skip'];
      $take = $input['take'];
      $optionOrder = $input['optionOrder'];
      $orderBy = $input['orderBy'];

      $query = DB::table('group_event_winner');
      $query->join('group_event', 'group_event.id', '=', 'group_event_winner.group_event_id');
      $query->join('groups', 'groups.id', '=', 'group_event.group_id');

      $query->join('track_details', 'track_details.id', '=', 'group_event_winner.track_details_id');
      $query->join('track', 'track.track_details_id', '=', 'track_details.id');

      $query->join('user_data', 'user_data.id', '=', 'track.track_id_create');

      $query->addSelect('track_details.id as trackID');
      $query->addSelect('track_details.name as trackTitle');
      $query->addSelect('track_details.hashPicture as trackPic');
      $query->addSelect('track_details.liked as trackLiked');
      $query->addSelect('track_details.shared as trackShared');
      $query->addSelect('track_details.buy as trackBuy');
      $query->addSelect('track_details.play as trackPlay');
      $query->addSelect('track_details.totalSpice as trackSpice');

      $query->addSelect('user_data.username as username');

      $query->addSelect('group_event.id as eventID');
      $query->addSelect('group_event.title as eventTitle');

      $query->addSelect('groups.id as groupID');
      $query->addSelect('groups.name as groupName');

      $query->addSelect('group_event.track_details_id as originalTrack');

      $query->orderBy('group_event_winner.'.$orderBy, $optionOrder);

      $count = $query->count();

      if($count == 0)
        return $data = ['data' => [], 'count' => 0];

      if($take != false && $take != 0 && $take > 0)
        $query->skip($skip)->take($take);

      $winner = $query->get();
      $winner = json_decode(json_encode($winner), true);

      $encryptController = new EncryptController;
      
      foreach ($winner as $key => $value) {
        $originalTrackDetails = TrackDetails::where('id', '=', $value['originalTrack'])->first();
        $originalTrack = Track::where('track_details_id', '=', $value['originalTrack'])->first();
        $originalUser = User::where('id', '=', $originalTrack['track_id_create'])->first();
        $winner[$key]['originalTrack'] = false;
        $winner[$key]['originalTrack']['name'] = $originalTrackDetails['name'];
        $winner[$key]['originalTrack']['genre'] = Genre::find($originalTrackDetails['genre'])->genre;
        $winner[$key]['originalTrack']['username'] = $originalUser['username'];

        $winner[$key]['trackID'] = $encryptController->encryptValue($value['trackID'], 1, 3);
        $winner[$key]['groupID'] = $encryptController->encryptValue($value['groupID'], 1, 5);
        // $data['eventID'] = $encryptController->encryptValue($value['eventID'], 1, 3);
      }

      $return = [
        'data' => $winner,
        'count' => $count
      ];

      return $return;
    }

    public function checkMyProject(){
    	$userID = Input::get('userID');
    	$encryptController = new EncryptController;
        $user_id = $encryptController->encryptValue($userID, 2, 1);

    	$data = DB::table('project')->where('user_id', '=', $user_id)
    	     ->where('status', '=', 0)->get();
    	 if(count($data) < 5 ){
    	 	return 1;  // bisa join event
    	 }else{
    	 	return 0;  // tidak bisa join event
    	 }    
    }

    public function checkEventID(){
    	$userID = trim(Input::get('userID'));
    	$event_id = trim(Input::get('event_id'));
    	$encryptController = new EncryptController;
        $user_id = $encryptController->encryptValue($userID, 2, 1);

        $data = GroupsEvent::where('id', '=', $event_id)->first();
        if(count($data) != 0){
        	return 1;  // jika event_id ada
        }else{
        	return 0;  // jika event id tidak ada
        }
    }

    public function checkStatusGroupEventID(){
        $userID = trim(Input::get('userID'));
        $event_id = trim(Input::get('event_id'));
        $encryptController = new EncryptController;
        $user_id = $encryptController->encryptValue($userID, 2, 1);
        $data = GroupsEvent::where('id', '=', $event_id)->select('group_id')->first();

        $group = Groups::where('id', '=', $data->group_id)->first();
        if(count($group) != 0){
            return 1;  // jika group ditemukan
        }else{
            return 0;  // jika group tidak ditemukan
        }
    }

    public function eventDetailsDescription(){
    	$userID = trim(Input::get('userID'));
    	$event_id = trim(Input::get('event_id'));
    	$encryptController = new EncryptController;
        $user_id = $encryptController->encryptValue($userID, 2, 1);

        $data =  DB::table('group_event')
            ->join('groups', function($join)
              {
                  $join->on('group_event.group_id', '=', 'groups.id');        
              })
            ->join('user_data', function($join)
              {
                  $join->on('group_event.creator_id', '=', 'user_data.id');        
              })
            ->where('group_event.id', '=', $event_id)
            ->addSelect('group_event.title as event_title',
                        'group_event.id as event_id',
                        'group_event.picture_event as picture_event',
                        'group_event.group_id as group_id',
                        'group_event.creator_id as user_id',
                        'group_event.description as description_event',
                        'group_event.setting_type as setting_type',
                        'group_event.event_type as event_type',
                        'group_event.track_details_id as track_details_id',
                        'group_event.tags as genre_tags',
                        'group_event.liked as liked',
                        'group_event.start_time',
                        'group_event.end_time',
                        'group_event.created_at',
                        'group_event.shared',
                        'groups.name as group_name',
                        'groups.picture as picture_group',
                        'user_data.username as username',
                        'groups.public as group_type')
            ->get();
            // return $data;
        
        	$decode = json_decode(json_encode($data), true);
        	foreach ($decode as $key => $value) {
        	    $groupID = $encryptController->encryptValue($value['group_id'], 1, 5);
        	    $IDcreator_event = $encryptController->encryptValue($value['user_id'], 1, 1);
              $groupController = new GroupController();
              $likedGroupEvent = $groupController->checkDataLikeEvent($user_id, $value['event_id']);
              $statusMemberEventParticipate = $groupController->statusMemberEventParticipate($user_id,$value['group_id'],$value['event_id']);
              $checkadmin = $groupController->checkAdminGroup($value['group_id'],$user_id);
              $checkStatusMember = $groupController-> checkStatusMember($value['group_id'], $user_id);

              if(!empty($value['track_details_id'] || $value['track_details_id'] != 0 || $value['track_details_id'] != '' )){
                   $trackID = $encryptController->encryptValue($value['track_details_id'], 1, 3);
                   $instrumentTrack = $groupController->DataTrackDetails($value['track_details_id'])['instrument_name'];
  		             $userIDCreatorTrack = $groupController->userIDCreatorTrack($value['track_details_id']);
  		             $creatorTrack = $groupController->getFriendName($userIDCreatorTrack);
  		             $priceTrack = $groupController->DataTrackDetails($value['track_details_id'])['is_free'];
  		             $judulTrack = $groupController->DataTrackDetails($value['track_details_id'])['name'];
  		             $genreCode = $groupController->DataTrackDetails($value['track_details_id'])['genre'];
  		             $genre = $groupController->getGenreTrack($genreCode);
  		             $buy = $groupController->DataTrackDetails($value['track_details_id'])['buy'];
  		        }else{
                   $trackID = '';
                   $instrumentTrack = '';
  		             $userIDCreatorTrack = '';
  		             $creatorTrack = '';
  		             $priceTrack = '';
  		             $judulTrack = '';
  		             $genre = '';
  		             $buy = '';
                  }	
	        		 $data_return[] = [
	        		     'event_title' => $value['event_title'],
	        		     'event_id' => $value['event_id'],
	        		     'picture_event' => $value['picture_event'],
	        		     'groupID' => $groupID,
	        		     'IDcreator_event' => $IDcreator_event,
	        		     'description_event' => $value['description_event'],
	        		     'setting_type' => $value['setting_type'],
	        		     'event_type' => $value['event_type'],
	        		     'trackID' => $trackID,
                   'judulTrack' => $judulTrack,
	                 'instrumentTrack' => $instrumentTrack,
	                 'creatorTrack' => $creatorTrack,
	                 'priceTrack' =>$priceTrack,
	                 'genre_tags' => $value['genre_tags'],
	                 'liked' => $value['liked'],
	                 'start_time' => $value['start_time'],
	                 'create' => $value['created_at'],
	                 'end_time' => $value['end_time'],
	                 'shared' => $value['shared'],
	                 'group_name' => $value['group_name'],
	                 'picture_group' => $value['picture_group'],
	                 'creator_event' => $value['username'],
	                 'genre' => $genre,
	                 'likedGroupEvent' => $likedGroupEvent,
                   'buy' => $buy,
                   'statusMemberEventParticipate' => $statusMemberEventParticipate,
                   'checkadmin' => $checkadmin,
                   'checkStatusMember' => $checkStatusMember,
                   'group_type' => $value['group_type'],

	        		 ];
        	}
        	
         $return[] =[
           'data' => $data_return,
           'count' => 1,
         ];
         return $return;
    }

    public function SongEventRelease(){
    	$userID = trim(Input::get('userID'));
    	$event_id = trim(Input::get('event_id'));
    	$skip = trim(Input::get('skip'));
    	$take = trim(Input::get('take'));

      $event = GroupsEvent::where('id', '=', $event_id)->first();
      $end_time = $event->end_time;

    	$encryptController = new EncryptController;
        $user_id = $encryptController->encryptValue($userID, 2, 1);

        $data_song = DB::table('project_tracks')
              ->join('project', function($join)
	              {
	                  $join->on('project_tracks.project_id', '=', 'project.id');        
	              })
              ->join('track_details', function($join)
	              {
	                  $join->on('project_tracks.track_details_id', '=', 'track_details.id');        
	              })
              ->where('project.event_id', '=', $event_id)
              ->where('project.status', '=', 1)
              ->where('project_tracks.is_buy_track', '=', 0)
              ->where('project_tracks.wrapped', '=', 1)
              ->where('project_tracks.created_at', '<', $end_time)
              ->addSelect('project_tracks.track_details_id as track_details_id',
                          'track_details.liked as like',
                          'track_details.shared as shared',
                          'track_details.buy as buy',
                          'track_details.play as play',
                          'track_details.name as song_event_title',
                          'project_tracks.user_id as userID_creatorSongEvent',
                          'track_details.hashPicture as pictureSong',
                          'track_details.instrument_name as instrument',
                          'track_details.is_free as price',
                          'track_details.totalSpice as spice');

        $count = count($data_song->get());
        $data = $data_song->skip($skip)->take($take)->get();
                   
        if(!empty($data)){
        	$decode = json_decode(json_encode($data), true);
        	foreach ($decode as $key => $value) {
        		 $trackID = $encryptController->encryptValue($value['track_details_id'], 1, 3);
        		 $groupController = new GroupController();        		
	           $creatorSong = $groupController->getFriendName($value['userID_creatorSongEvent']);
	           $userID_creatorSong = $encryptController->encryptValue($value['userID_creatorSongEvent'], 1, 1);
	           $trackBankController = new TrackBankController();
	           $statusLike = $trackBankController->getCheckDataLike($user_id,$value['track_details_id']);
             $group_id = $groupController->getGroupID($event_id);
	        		 $data_return[] = [
	        		       'song_event_title' => $value['song_event_title'],
	        		       'trackID' => $trackID,
	        		       'like' => $value['like'],
	        		       'shared' => $value['shared'],
	        		       'buy' => $value['buy'],
	        		       'play' => $value['play'],
	        		       'pictureTrack' => $value['pictureSong'],
	        		       'instrumentTrack' => $value['instrument'],
	        		       'price' => $value['price'],
	        		       'creatorSong' => $creatorSong,
	        		       'userID_creatorSong' => $userID_creatorSong,
	        		       'statusLike' => $statusLike,
                     'event_id' => $event_id,
                     'spice' => $value['spice'],
                     'checkingWinnerEvent' => $this->checkingWinnerEvent($event_id),
                     'start_event' => $this->getDataGroupEvent($event_id)['start_time'],
                     'end_event' => $this->getDataGroupEvent($event_id)['end_time'],
                     'checkAdminGroup' => $groupController->checkAdminGroup($group_id,$user_id),
	        		 ];
        	}
            
        }else{
        	$data_return = array();
        }
        $return[] = [
        	     'data' => $data_return,
        	     'count' => $count,
        	   ];  
        return $return;   
    }

    public function checkingWinnerEvent($event_id){
        $data = DB::table('group_event_winner')->where('group_event_id', '=', $event_id)->first();
        if(count($data) != 0){
            return 1;   //sudah ada pemenang
        }else{
            return 0;   //belum ada pemenang
        }
    }

    public function getDataGroupEvent($event_id){
        $data = GroupsEvent::where('id', '=', $event_id)->first();
        return $data;
    }

    public function winnerGroupEvent(){
    	// return Input::all();
    	$userID = trim(Input::get('userID'));
    	$event_id = trim(Input::get('event_id'));
      
      $encryptController = new EncryptController;
      $user_id = $encryptController->encryptValue($userID, 2, 1);

      $data = $this->datawinnerGroupEvent($user_id,$event_id);
      return $data;
        
    }

    public function datawinnerGroupEvent($user_id,$event_id){
        $check_group = GroupsEvent::where('id', '=', $event_id)->first();
        $group_id = $check_group->group_id;
        $end_time_event = $check_group->end_time;
       
        $end = strtotime($end_time_event);
        $end_event = date("Y-m-d", $end);
      // return $end_event;

        $time = time();
        $tanggal = (date("Y-m-d"));
        // return $tanggal;
        $date_now = date_create($tanggal);
        $date_end_event = date_create($end_event);
        $diff = date_diff($date_now,$date_end_event);
        $check_event_end = $diff->format("%R%a");
        
        $dataGroup = Groups::where('id', '=', $group_id)->first();
        $group_name = $dataGroup->name;

        $encryptController = new EncryptController;
        // $user_id = $encryptController->encryptValue($userID, 2, 1);
        $groupID = $encryptController->encryptValue($group_id, 1, 5);

        $data = EventWinner::where('group_event_id', '=', $event_id)->first();
        // return $data;
        if(!empty($data)){
          $track_winner = $data->track_details_id;
          $trackBankController = new TrackBankController();
          $groupController = new GroupController();

          $data_winner = DB::table('track')
                ->join('track_details', function($join)
                  {
                      $join->on('track.track_details_id', '=', 'track_details.id');        
                  })
                ->join('user_data', function($join)
                  {
                      $join->on('track.track_id_create', '=', 'user_data.id');        
                  })
                ->where('track.track_details_id', '=', $track_winner)
                ->addSelect('user_data.username as username_winner',
                            'user_data.id as userID_winner',
                            'user_data.email as email',
                            'track.track_details_id as track_details_id',
                            'track_details.name as judulSong',
                            'track_details.liked as like',
                            'track_details.shared as shared',
                            'track_details.buy as buy',
                            'track_details.play as play',
                            'track_details.totalSpice as spice',
                            'track_details.genre as genre',
                            'track_details.hashPicture as pictureSong',
                            'track_details.instrument_name as instrument',
                            'track_details.is_free as price')
            ->get();
            $count = count($data_winner);   

             $decode = json_decode(json_encode($data_winner),true);
              foreach ($decode as $key => $value) {
                $userID_winner = $encryptController->encryptValue($value['userID_winner'], 1, 1);
                $trackID = $encryptController->encryptValue($value['track_details_id'], 1, 3);
                $track_original = $trackBankController->checkingOriginalTrack($value['track_details_id']);
                $original_userID = $groupController->userIDCreatorTrack($track_original);
                $UserData = $groupController->myUserData($value['userID_winner']);
                $UserData_encode = json_decode(json_encode($UserData), true);
                $group_id = $groupController->getGroupID($event_id);
                $group = DB::table('groups')->where('id', '=', $group_id)->first();
                $admin_id = $group->admin_id;
                
                $check_group = GroupsEvent::where('id', '=', $event_id)->first();
                $event_title = $check_group->title;

                   $data_return[] = [
                       'username_winner' => $value['username_winner'],
                       'userID_winner' => $userID_winner,
                       'email' => $value['email'],
                       'trackID' => $trackID,
                       'judulSong' => $value['judulSong'],
                       'like' => $value['like'],
                       'shared' => $value['shared'],
                       'buy' => $value['buy'],
                       'play' => $value['play'],
                       'pictureSong' => $value['pictureSong'],
                       'instrument' => $value['instrument'],
                       'price' => $value['price'],
                       'group_name' => $group_name,
                       'groupID' => $groupID,
                       'event_id' => $event_id,
                       'spice' => $value['spice'],
                       'genre' => $groupController->getGenreTrack($value['genre']),
                       'original_track' => $groupController->DataTrackDetails($track_original)['name'],
                       'original_username' => $groupController->getFriendName($original_userID),
                       'email_winner' => $UserData_encode[0]['email'],
                       'statusLike' => $trackBankController->getCheckDataLike($user_id,$value['track_details_id']),
                       'checkAdminGroup' => $groupController->checkAdminGroup($group_id,$user_id),
                       'start_event' => $this->getDataGroupEvent($event_id)['start_time'],
                       'end_event' => $this->getDataGroupEvent($event_id)['end_time'],
                       'admin_name' => $groupController->getFriendName($admin_id),
                       'event_title' => $event_title,
                   ];
              }

        }else{
           $data_return = array();
           $count = 0;
        }

        $return[] = [
               'data' => $data_return,
               'count' => $count,
             ];  
        return $return;      
        
    }

    function submitEventWinner(){

    	$userID = trim(Input::get('userID'));
    	$event_id = trim(Input::get('event_id'));
    	$trackID = trim(Input::get('trackID'));

    	$encryptController = new EncryptController;
        $user_id = $encryptController->encryptValue($userID, 2, 1);
        $track_details_id = $encryptController->encryptValue($trackID, 2, 3);
        // $track_details_id = 222;

        $check_group = GroupsEvent::where('id', '=', $event_id)->first();
        $group_id = $check_group->group_id;
        $event_title = $check_group->title;

        $dataGroup = Groups::where('id', '=', $group_id)->first();
        $admin_group = $dataGroup->admin_id;

        $track = Track::where('track_details_id', '=', $track_details_id)->first();
        $track_id = $track->id;
        $userID_winner = $track->track_id_create;

        $groupController = new GroupController();
        $creatorSong = $groupController->getFriendName($userID_winner);
        $judulSong = $groupController->DataTrackDetails($track_details_id)['name'];
        
        $creator_event = $groupController->getFriendName($check_group->creator_id);    
       
        $string = "<a id='pjax' data-pjax='yes' style='text-decoration:none;' href='/".$creator_event."'>".$creator_event."</a> &nbsp ".
                  "announce the winners at the event with the title <strong><a id='pjax' data-pjax='yes' style='text-decoration:none;' href='/event/".$event_id."'>".$event_title."</a></strong>\n".
                  "winner : <a id='pjax' data-pjax='yes' style='text-decoration:none;' href='/".$creatorSong."'>".$creatorSong."</a>\n".
                  "song title : <a id='pjax' data-pjax='yes' style='text-decoration:none;' href='javascript:void();' onclick='playMusic(".'"'.$trackID.'"'.");' style='cursor:pointer; text-decoration:none'> ".$judulSong." </a>";
        $caption = nl2br($string);

        $data = new EventWinner;
        $data->track_details_id = $track_details_id;
        $data->group_event_id = $event_id;
        if($data->save()){
          
         $participantReleaseSong = Project::where('event_id', '=', $event_id)->where('status', '=', 1)->get();
         $decode = json_decode(json_encode($participantReleaseSong), true);
         foreach ($decode as $key => $value) {
            $notificationController = new NotificationController();
            $stateAdd[] = $notificationController->addNotiffication($user_id,$value['user_id'], $event_id, 621);
         }

          $postGroup = new GroupPost;
          $postGroup->group_id = $group_id;
          $postGroup->user_id = $user_id;
          $postGroup->track_id = $track_id;
          $postGroup->caption = $caption;
          $postGroup->liked = 0;
          $postGroup->group_event_id = $event_id;
          $postGroup->save();

          $postWall = new Post;
          $postWall->user_id = $user_id;
          $postWall->liked = 0;
          $postWall->track_id = $track_details_id;
          $postWall->post_content = $caption;
          $postWall->is_event = $event_id;
          $postWall->is_public = 0;
          $postWall->shared = 0;
          $postWall->save();

          $data =  $data = $this->datawinnerGroupEvent($user_id,$event_id);
          $status = 1;

        }else{
          $data = array();
        	$status = 0;
        }
       $return = [
           'status' => $status,
           'data' => $data,
       ];
       return $return;
    }

    public function sendEmailToEventWinner(){
         $email = trim(Input::get('email'));
         $username_winner = trim(Input::get('username_winner'));
         $username_admin = trim(Input::get('username_admin'));
         $text_area = trim(Input::get('text_area'));
         $event_title = trim(Input::get('event_title'));

         $emailController = new EmailController();
         $emailController->sendEmailToEventWinner($email,$username_winner,$username_admin,$text_area,$event_title);
         return 1; // jika sukses kirim email
    }

    public function participatedEventNotReleaseSong(){
        $event_id = Input::get('event_id');
        $userID = Input::get('userID');
        $skip = Input::get('skip');
        $take = Input::get('take');

        $event = GroupsEvent::where('id', '=', $event_id)->first();
        $end_time = $event->end_time;

        $encryptController = new EncryptController;
        $user_id = $encryptController->encryptValue($userID, 2, 1);

        $groupController = new GroupController();
        
        $dataParticipant = $this->dataParticipatedEventNotReleaseSong($event_id);
        $dataCount = $dataParticipant->get();
        $data = $dataParticipant->skip($skip)->take($take)->get();
        
        if(count($data) != 0){
            $decode = json_decode(json_encode($data), true);
            foreach ($decode as $key => $value) {
              $user = $groupController->myUserData($value['user_id']);
              $user_decode = json_decode(json_encode($user),true);
                $return[] = [
                    'userID' => $encryptController->encryptValue($value['user_id'], 1, 1),
                    'checkFriend' => $groupController->checkFriend($user_id, $value['user_id']),
                    'username' => $groupController->getFriendName($value['user_id']),
                    'real_name' => $user_decode[0]['name'],
                    'profile_image' => $user_decode[0]['profile_image'],
                    'bg_image' => $user_decode[0]['bg_image'],
                    'statusRelease' => $this->statusReleaseEventSong($event_id,$end_time,$value['id']),
                ];
            }
            $count = count($dataCount);
        }else{
          $count = 0;
          $return = array();
        }
        
        $data_return[] = [
            'data' => $return,
            'count' => $count,
        ];
        return $data_return;                 
    }

    public function dataParticipatedEventNotReleaseSong($event_id){
        $data = Project::where('event_id', '=', $event_id);

        return $data;                 
                         
    }

    public function statusReleaseEventSong($event_id,$end_time,$project_id){
        $release = Project::where('id', '=', $project_id)
              ->where('event_id', '=', $event_id)
              ->where('status', '=', 1)
              ->where('updated_at', '<', $end_time)
              ->first();
      
        if(count($release) != 0){
            return 1; 
        }else{
            return 0;
        }
    }

    public function hitungSharedEvent(){
        $userID = Input::get('userID');
        $event_id = Input::get('event_id');
        $encryptController = new EncryptController;
        $user_id = $encryptController->encryptValue($userID, 2, 1);

       $data = GroupsEventActivity::where('group_event_id', '=', $event_id)
             ->where('user_id', '=', $user_id)->first();
       if(count($data) != 0){
           $sharedTotal = $data->shared;
           $data->shared = $sharedTotal + 1;
           if($data->save()){
               $saveEvent = GroupsEvent::where('id', '=', $event_id)->first();
               $totalShared = $saveEvent->shared;
               $saveEvent->shared = $totalShared + 1;
               if($saveEvent->save()){
                   return 'save';
               }else{
                  return 'gagal event';
               }
           }else{
               return 'gagal';
           }
           
       }else{
           $saveShared = new GroupsEventActivity;
           $saveShared->group_event_id = $event_id;
           $saveShared->user_id = $user_id;
           $saveShared->liked = 0;
           $saveShared->shared = 1;
           if($saveShared->save()){
              $saveEvent = GroupsEvent::where('id', '=', $event_id)->first();
               $totalShared = $saveEvent->shared;
               $saveEvent->shared = $totalShared + 1;
               if($saveEvent->save()){
                   return 'save';
               }else{
                  return 'gagal event';
               }
           }else{
              return 0;
           }
       }
    }

    
    public function LandingEvent(){
        $userID = Input::get('userID');
        $skip = Input::get('skip');
        $take = Input::get('take');
        $orderBy = Input::get('orderBy');
        $optionOrder = Input::get('optionOrder');

        $encryptController = new EncryptController;
        $user_id = $encryptController->encryptValue($userID, 2, 1);

        $time = time();
        $tanggal = (date("Y-m-d"." "."H-m-s",$time));

        $data = $this->dataLandingEvent($orderBy,$optionOrder);
        $count = count($data->get());
        
        if($count != 0){
            $dataEvent = $data->skip($skip)->take($take)->get();
            $decode =  json_decode(json_encode($dataEvent), true);
            foreach ($decode as $key => $value) {
              $trackID = $encryptController->encryptValue($value['track_details_id'], 1, 3);
              $groupID = $encryptController->encryptValue($value['group_id'], 1, 5);
              $groupController = new GroupController();
              $likedGroupEvent = $groupController->checkDataLikeEvent($user_id, $value['event_id']);
              $statusMemberEventParticipate = $groupController->statusMemberEventParticipate($user_id,$value['group_id'],$value['event_id']);
              $checkadmin = $groupController->checkAdminGroup($value['group_id'],$user_id);
              $checkStatusMember = $groupController-> checkStatusMember($value['group_id'], $user_id);
              $peserta = sizeof(Project::where('event_id', '=', $value['event_id'])->get());
              $creatorTrack = $groupController->getFriendName($value['creatorTrack']); 

                 $return[] = [
                     'event_title' => $value['event_title'],
                     'event_id' => $value['event_id'],
                     'picture_event' => $value['picture_event'],
                     'groupID' => $groupID,
                     'user_id' => $userID,
                     'description_event' => $value['description_event'],
                     'setting_type' => $value['setting_type'],
                     'event_type' => $value['event_type'],
                     'trackID' => $trackID,
                     'genre_tags' => $value['genre_tags'],
                     'liked' => $value['liked'],
                     'start_time' => $value['start_time'],
                     'end_time' => $value['end_time'],
                     'created_at' => $value['created_at'],
                     'group_name' => $value['group_name'],
                     'picture_group' => $value['picture_group'],
                     'username_creator' => $value['username'],
                     'likedGroupEvent' => $likedGroupEvent,
                     'statusMemberEventParticipate' => $statusMemberEventParticipate,
                     'checkadmin' => $checkadmin,
                     'checkStatusMember' => $checkStatusMember,
                     'shared' => $value['shared'],
                     'peserta' => $peserta,
                     'judulTrack' => $value['judulTrack'],
                     'instrument' => $value['instrument'],
                     'price' => $value['price'],
                     'creatorTrack' => $creatorTrack,
                     'group_type' => $value['group_type'],
                 ];
            }
            $dataReturn[] = [
               'data' => $return,
               'count' => $count,
             ];
           
        }else{
           $dataReturn[] = [
              'count' => $count,
           ]; 
        }

       return $dataReturn;
    }

    public function dataLandingEvent($orderBy,$optionOrder){
        $event = DB::table('group_event')
            ->join('groups', function($join)
              {
                  $join->on('group_event.group_id', '=', 'groups.id');        
              })
            ->join('user_data', function($join)
              {
                  $join->on('group_event.creator_id', '=', 'user_data.id');        
              })
            ->join('track_details', function($join)
              {
                 $join->on('group_event.track_details_id', '=', 'track_details.id');
              })
            ->join('track', function($join)
              {
                  $join->on('group_event.track_details_id', '=', 'track.track_details_id');
              })
            ->orderBy('group_event.'.$orderBy, '=', $optionOrder)
            ->addSelect('group_event.title as event_title',
                        'group_event.id as event_id',
                        'group_event.picture_event as picture_event',
                        'group_event.group_id as group_id',
                        'group_event.creator_id as user_id',
                        'group_event.description as description_event',
                        'group_event.setting_type as setting_type',
                        'group_event.event_type as event_type',
                        'group_event.track_details_id as track_details_id',
                        'group_event.tags as genre_tags',
                        'group_event.liked as liked',
                        'group_event.start_time',
                        'group_event.end_time',
                        'group_event.created_at',
                        'group_event.shared',
                        'groups.name as group_name',
                        'groups.picture as picture_group',
                        'user_data.username as username',
                        'track_details.name as judulTrack',
                        'track_details.instrument_name as instrument',
                        'track_details.is_free as price',
                        'track.track_id_create as creatorTrack',
                        'groups.public as group_type');
        return $event;
    }

    public function LandingNewEvent(){
        $userID = Input::get('userID');
        $skip = Input::get('skip');
        $take = Input::get('take');
        $orderBy = Input::get('orderBy');
        $optionOrder = Input::get('optionOrder');

        $encryptController = new EncryptController;
        $user_id = $encryptController->encryptValue($userID, 2, 1);

        $time = time();
        $tanggal = (date("Y-m-d"." "."H-m-s",$time));

        $data = $this->dataLandingNewEvent($orderBy,$optionOrder,$tanggal);
        $count = count($data->get());
        
        if($count != 0){
            $dataEvent = $data->skip($skip)->take($take)->get();
            $decode =  json_decode(json_encode($dataEvent), true);
            foreach ($decode as $key => $value) {
              $trackID = $encryptController->encryptValue($value['track_details_id'], 1, 3);
              $groupID = $encryptController->encryptValue($value['group_id'], 1, 5);
              $groupController = new GroupController();
              $likedGroupEvent = $groupController->checkDataLikeEvent($user_id, $value['event_id']);
              $statusMemberEventParticipate = $groupController->statusMemberEventParticipate($user_id,$value['group_id'],$value['event_id']);
              $checkadmin = $groupController->checkAdminGroup($value['group_id'],$user_id);
              $checkStatusMember = $groupController-> checkStatusMember($value['group_id'], $user_id);
              $peserta = sizeof(Project::where('event_id', '=', $value['event_id'])->get());
              $creatorTrack = $groupController->getFriendName($value['creatorTrack']); 

                 $return[] = [
                     'event_title' => $value['event_title'],
                     'event_id' => $value['event_id'],
                     'picture_event' => $value['picture_event'],
                     'groupID' => $groupID,
                     'user_id' => $userID,
                     'description_event' => $value['description_event'],
                     'setting_type' => $value['setting_type'],
                     'event_type' => $value['event_type'],
                     'trackID' => $trackID,
                     'genre_tags' => $value['genre_tags'],
                     'liked' => $value['liked'],
                     'start_time' => $value['start_time'],
                     'end_time' => $value['end_time'],
                     'created_at' => $value['created_at'],
                     'group_name' => $value['group_name'],
                     'picture_group' => $value['picture_group'],
                     'username_creator' => $value['username'],
                     'likedGroupEvent' => $likedGroupEvent,
                     'statusMemberEventParticipate' => $statusMemberEventParticipate,
                     'checkadmin' => $checkadmin,
                     'checkStatusMember' => $checkStatusMember,
                     'shared' => $value['shared'],
                     'peserta' => $peserta,
                     'judulTrack' => $value['judulTrack'],
                     'instrument' => $value['instrument'],
                     'price' => $value['price'],
                     'creatorTrack' => $creatorTrack,
                     'group_type' => $value['group_type'],
                 ];
            }
            $dataReturn[] = [
               'data' => $return,
               'count' => $count,
             ];
           
        }else{
           $dataReturn[] = [
              'count' => $count,
           ]; 
        }

       return $dataReturn;
    }

    public function dataLandingNewEvent($orderBy,$optionOrder,$tanggal){
      $five_days_ago_date = date('Y-m-d', strtotime('-6 days', strtotime(date('Y-m-d'))));

        $event = DB::table('group_event')
              ->join('groups', function($join)
                {
                    $join->on('group_event.group_id', '=', 'groups.id');        
                })
              ->join('user_data', function($join)
                {
                    $join->on('group_event.creator_id', '=', 'user_data.id');        
                })
              ->join('track_details', function($join)
                {
                   $join->on('group_event.track_details_id', '=', 'track_details.id');
                })
              ->join('track', function($join)
                {
                    $join->on('group_event.track_details_id', '=', 'track.track_details_id');
                })
              ->where('group_event.end_time', '>', $tanggal)
              ->where('group_event.created_at', '>=', Carbon::now()->subDays(5))
              ->orderBy('group_event.'.$orderBy, '=', $optionOrder)
              ->addSelect('group_event.title as event_title',
                          'group_event.id as event_id',
                          'group_event.picture_event as picture_event',
                          'group_event.group_id as group_id',
                          'group_event.creator_id as user_id',
                          'group_event.description as description_event',
                          'group_event.setting_type as setting_type',
                          'group_event.event_type as event_type',
                          'group_event.track_details_id as track_details_id',
                          'group_event.tags as genre_tags',
                          'group_event.liked as liked',
                          'group_event.start_time',
                          'group_event.end_time',
                          'group_event.created_at',
                          'group_event.shared',
                          'groups.name as group_name',
                          'groups.picture as picture_group',
                          'user_data.username as username',
                          'track_details.name as judulTrack',
                          'track_details.instrument_name as instrument',
                          'track_details.is_free as price',
                          'track.track_id_create as creatorTrack',
                          'groups.public as group_type');
          return $event;
    }

    public function getDataEvent(){
        $event_id = Input::get('event_id');
        $data = GroupsEvent::where('id', '=', $event_id)->addSelect('title','description','start_time','end_time')->first();
        return $data;
    }

  
}
