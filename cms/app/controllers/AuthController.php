<?php

class AuthController extends BaseController {

	public function showLogin(){
		$session = Auth::user();

		if(!empty($session) || sizeof($session) != 0){
			return View::make('pages.welcome');

		}

		return View::make('pages.login');
	}

	public function welcome(){
		return View::make('pages.welcome');
	}

	public function naughty(){
		return View::make('pages.naughty');
	}

	public function disabled(){
		return View::make('pages.disabled');
	}

	public function doLogin() {
	    // validasi input, bikin rule
	    $rules = array(
	        'username' => 'required',
	        'password' => 'required'
	    );

	    // jalanin validasi
	    $validator = Validator::make(Input::all(), $rules);

	    // kalo gagal, balik ke login form
	    if ($validator-> fails()) {
	        return Redirect::to('login')-> withErrors($validator) // kirim balik semua error
	            -> withInput(Input::except('password')); // kirim balik input kecuali password
	    } else {

	        // bikin user data buat autentikasi
	        $usernamenya = Input::get('username');
	        $passwordnya = Input::get('password');

	        $userdata = array(
	            'username' => $usernamenya,
	            'password' => $usernamenya.$passwordnya.$usernamenya
	        );

	        // lakukan login
	        if (Auth::attempt($userdata)) {
	        	$id = Auth::user()->id;
	        	$user_group_menu = UserGroupMenu::where('id_user', '=', $id)->get();
	        	$group_menu = GroupMenu::find($user_group_menu[0]->id_groupmenu);
	        	if($group_menu->enable==1){
	            // redirect ke halaman yg dimau
	        		Session::flash('message_success', 'Welcome :)');
	            	return Redirect::to('welcome');
	        	} else {
	        		return Redirect::to('disabled');
	        	}
	        } else {
	            // login gagal
	            Session::flash('message_error', 'Login Failed');
	            return Redirect::to('login');
	        }
	    }
	}

	public function doLogout(){
	    Auth::logout(); 
	    return Redirect::to('login');
	}

	public function checkMyAPIkey(){
        // return Input::all();
        $inputApi = Input::get('apiKey');
        $inputID = Input::get('user_id');

        $encryptController = new EncryptController;
	    $inputID = $encryptController->encryptValue($inputID, 2, 1);
	    // echo $id;
	    //return $id;
        $return = $this->checkAPIandUserID($inputID, $inputApi);
        return $return;
	}

	public function checkAPIandUserID($userID, $apiKey){
		$data = DB::table('api_keys')
	    				->where('key', $apiKey)
	                    ->where('user_id','=', $userID)
	                    ->first();

	    if(sizeof($data) == 0 || empty($data)){
	    	$return = [
	    		'data' => [],
	    		'status' => -1
	    	];
	    }
	    else {
	    	$return = [
		    	'data' => $data,
		    	'status' => 1
		    ];
	    }	    
        return $return; 
	}
     
}
