<?php

class EncryptController extends BaseController {

	private $publicDirEncrypt = 'public/upload_files/encrypt/';
	private $serverDirMP3 = '/usr/local/WowzaStreamingEngine-4.4.1/applications/vod/gr1tj@m/';
	private $secretPrime = '0400C3251F9D1B85CF1DAB211D179F154A103E44629398BF4E1AE0B20212CEF5A3D516DB364A6F5884A305A13CF84C196FB6AD7DBF4794837FE68B04F6EECC4EACF071624D6E31C42159580164CFAA1315609AECF441C381E62550B88ED1716D84D8323406E3913DE4E587B64C2CC80790D3074216680B5A2DC0239FBD0BD817D807';
	private $secretLogin = '040081B30816565A1FD1EB23BDC5FE97BEDF4B581AA21F15AFBD851563C9551EB0DC7D59F6492EE7F3400018C023BF2170A6FB68C015C25641320B6BBE3A48AF1D5B5F8DB48F5559AA100A628005F5EADEC7AEB997773C3A6DE935041C7FEBAAEF2ABFA416975AE09BB2B3BAAE1C0B0AAD7A8D6C75600A1BF84161E8E572C2BB340F';
	private $secretGenerator = 5;

	private $iv; //hex
	private $key; //hex

	private $publicKey;

	public function setPublicKey($publicKey){
		$this->publicKey = $publicKey;
	}

	private function setKey($controller){
		switch ($controller) {
			case 1: //encrypt user ID
				$this->iv = '5006408E49EE00A780E86B0CA8D7026B7';
				$this->key = '50100A4897D535D1662BAE964AEC70FCD61ECFB3C855EA34BA3B6CF4DE43EB9F5F';
				$return = 1;
				break;
			case 2: //encrypt track ID
				$this->iv = '078B97A12C97ADAA48E00A08BC088E4AB';
				$this->key = '0100D090B650B291E5D23AC6325BCCCEF3F615F4BEAA452424E1C6B650FA31FA6F';
				$return = 1;
				break;
			case 3: //encrypt track ID
				$this->iv = '078BA3F3146C611970D601B4615A21BDF';
				$this->key = '0100AF675814612B2632CCE465F6A2AE4C5AD61E7C41093E3BA6038E9E9D9966E3';
				$return = 1;
				break;
			case 4: // notification ID
				$this->iv = '078AC9B4E385DB993FA4EDC6FB3B4F81F';
				$this->key = '0100F7B7EB7CE60E70E9FF86EAF47894512A3BEB1D6820A33E4CA76E274E82F533';
				$return = 1;
				break;
			case 5: //encrypt group ID
				$this->iv = '078B29C0B9D5EFD053ABA78E57085942B';
				$this->key = '01009EB575AF6D2AA7291DC20632AB078C54F3AA001AFEC77C31007AF34921BC67';
				$return = 1;
				break;
			case 6: //encrypt playlist ID
				$this->iv = '078DC4BE382DCC02AFF16CE4A4F05A867';
				$this->key = '0100A0035B69F8A77853D931EDF7BFD5902C0FAFECC98854E23554012ED4A97CAB';
				$return = 1;
				break;
			case 7:
				$this->iv = '078EE27F6B0963D8E5EBDEB2A9EE73E4B';
				$this->key = '0100CEA44366FBF5863F80B64F714AA9DBB3B3757BAA4F2C48EA22E8736ABA6333';
				$return = 1;
				break;
			case 8: //encrypt file
				$this->iv = '078DDBCD86B30FFEBF4B124F2F10E11A7';
				$this->key = '010F69F2A7D3670A9913F50AE8D10055F66F6DC0956ADDC77FF00A3D87A705233F';
				$return = 1;
				break;
			case 9: //encrypt Project ID
				$this->iv = '0789213D33A19C4564F4AC6D5968F85A7';
				$this->key = '100D1EF8CC339BA02C16124F76B032F10580241010E402B76BDDD7B2B941D1BFCB';
				$return = 1;
				break;
			case 10: //encrypt notification ID
				$this->iv = '';
				$this->key = '';
			default:
				$return = 0;
				break;
		}
		return $return;
	}

	public function encryptThis($method, $prime, $userID, $apiKey, $value){
		$method = [
	        'method' => $method,
	        'prime' => $prime
	    ];

	    $userData = [
	        'userIDSession' => $userID,
	        'apiKeySession' => $apiKey,
	        'value' => $value
	    ];

	    $value = $this->newEncryptValue($method, $userData); 
	    return $value;
	}

	public function encryptFileMP3($path, $encryptName, $extension, $secretKey){
		require_once(app_path() . '/library/chilkat_9_5_0.php');
		$crypt = new CkCrypt2();
		$success = $crypt->UnlockComponent('TOIENR.CB10417_3eRa2Tzam46h');
		if($success != true)
			return 0;

		$sessionKey 	= $crypt->hashStringENC($secretKey);
		$ivEncrypt 		= $crypt->hashStringENC($sessionKey);

		$crypt->put_EncodingMode('hex');
		$crypt->put_HashAlgorithm('sha512');
		$crypt->put_CryptAlgorithm('aes');
		$crypt->put_KeyLength(128);
		$crypt->put_CipherMode('ecb');
		$crypt->SetEncodedKey($sessionKey,'hex');
		$crypt->SetEncodedIV($ivEncrypt,'hex');

		$inFile = $path;
		$outFile = $this->publicDirEncrypt . $encryptName . '.' . $extension;

		$success = $crypt -> CkEncryptFile($inFile, $outFile);
		if($success != true)
			return 0;

		return 1;
	}

	public function encryptDH($publicKey){
		require_once(app_path() . '/library/chilkat_9_5_0.php');
		$serverEncrypt = new CkDh();
		$success = $serverEncrypt->UnlockComponent('TOIENR.CB10417_3eRa2Tzam46h');
		if($success != true){
			return $serverEncrypt->lastErrorText();
		}	

		$success = $serverEncrypt->SetPG($this->secretPrime, $this->secretGenerator);
		if($success != true){
			return 'not safe prime';
		}
		$eServerEncrypt = $serverEncrypt->createE(256);
		$kFrontEncrypt = $serverEncrypt->findK($publicKey);

		$data = array(
			'secretKey' => $kFrontEncrypt,
			'publicKey' => $eServerEncrypt,
			'status' => 1
		);

		return $data;
	}

	public function getMP3FTP($folderPath, $filePath, $fileName, $userID){
		// return [$filePath, $name];
		Config::set('remote.connections.runtime.host', '103.43.45.217');
		Config::set('remote.connections.runtime.port', '22');
		Config::set('remote.connections.runtime.username', 'wowzaftp');
		Config::set('remote.connections.runtime.password', '@dm1nftp');

		$serverFile = $filePath;
		$localFile = $this->publicDirEncrypt. "/".$fileName."[".$userID."].mp3";
		// echo $localFile;
		// echo $folderPath;
		// echo $filePath;
		// echo $name;
		SSH::into('runtime')->get($filePath, $localFile);
		return $localFile;
	}

	public function downloadFile(){
		// return Input::all();

		// $sharedKey = Input::get('sharedKey');

		// require_once(app_path() . '/library/chilkat_9_5_0.php');
		// $serverEncrypt = new CkDh();
		// $success = $serverEncrypt->UnlockComponent('Anything for 30-day trial.');
		// if($success != true){
		// 	return $serverEncrypt->lastErrorText();
		// }	

		// $success = $serverEncrypt->SetPG($this->secretPrime, $this->secretGenerator);
		// if($success != true){
		// 	return 'not safe prime';
		// }
		// $eServerEncrypt = $serverEncrypt->createE(256);
		// $kFrontEncrypt = $serverEncrypt->findK($sharedKey);
		// // return [$eServerEncrypt, $kFrontEncrypt];

		$userIDEncrypt = Input::get('userID');
		$trackID = Input::get('trackID');
		$apiKey = Input::get('apiKey');
		
		/* Decrypt user ID */
		$userID = $this->encryptValue($userIDEncrypt, 2, 1);

		/* Decrypt track ID */
		$trackID = $this->encryptValue($trackID, 2, 7);
		// echo $userID . " " . $trackID;

		/* Checking Apikey and UserID */
		$statusApi = Chrisbjr\ApiGuard\ApiKey::where('user_id', '=', $userID)
						->where('key', '=', $apiKey)
						->first();
		
		if(sizeof($statusApi) == 0 || empty($statusApi)){
			return 0;
		}

		/* Get Track Details */
		$combineController = new CombineController('all', 0, false);
		$combineController->setNeed('all');
		$combineController->setUserID($userID, false);
		$combineController->setOrderBy('create', 'asc');
		$combineController->setTrackID($trackID);
		$combineController->setData();
		$data = $combineController->getData();
		$data = $data[0];

		$fileHashName = $data['full'];
		
		/* Get User Folder */
		$fileHash = $data['track_folderHash'];

		/* Download File to Server */

		$filePath = $this->serverDirMP3 . $fileHash . '/' .
				// $fileHashName . '.mp3';
				'2y10T8OC2u0p10IFTn9ZGcKjhOAhOiibcZ3SKeVAdp2ZmDpumccZp2.mp3';
		$folderPath = $this->serverDirMP3 . $fileHash;
		// return $filePath;
		
		$stateGetMP3 = $this->getMP3FTP($folderPath, $filePath, $fileHashName, $userIDEncrypt);
		// return $stateGetMP3;

		/* Check file */
		if(!file_exists($stateGetMP3))
			return 0;

		/* Encrypt Song || Track File */
		$inFile = $stateGetMP3;
		$outFile = $this->publicDirEncrypt . "/". $fileHashName."[".$userID."][encrypt].mp3";
		// echo $inFile;
		// echo $outFile;
		$stateEncrypt = $this->encryptValue(true, 3, 8, $inFile, $outFile);
		
		/* Delete File */
		unlink($inFile);

		/* Send Link to Local for Download */
		App::finish(function($request, $response) use ($outFile)
		{
		   unlink($outFile);
		});
		return Response::download($outFile);
	}

	// ---------------------------- ENCRYPT VALUE HERE ----------------------------- //

	/*
	function encryptValue($value, $method, $controller, $userData = false, $inFile = false, $outFile = false);
		$value 		= value yang mau di encrypt / decrypt
		$method 	= 1, 2, 3 atau 4 
			1 : encrypt value,
			2 : decrypt,
			3 : encrypt file
			4 : decrypt file
		$controller = 1 - 9 (prime yang digunakan)
		$userData	= array ([userID, apiKey])
		$inFile. lokasi file in (default false)
		$outFile. lokasi file out (default false)

		cat: 
			1. saat mengencypt file tidak perlu di isi $value, tp harus isi $inFile dan $outFile
			2. saat encrypt value, $inFile dan $outFile tidak perlu diisi (default false)
	*/
	public function encryptValue($value = false, $method, $controller, $inFile = false, $outFile = false){
		$stateConf = $this->setKey($controller);
		if($stateConf == 0){
			return -1;
		}

		require_once(app_path() . '/library/chilkat_9_5_0.php');
		$cylkat = new CkCrypt2();
		$success = $cylkat->UnlockComponent('TOIENR.CB10417_3eRa2Tzam46h');
		if ($success != true) {
		    $error = $cylkat->lastErrorText() . "\n";
		    return $error;
		}

		$cylkat->put_CryptAlgorithm('aes');
		$cylkat->put_CipherMode('cbc');
		$cylkat->put_KeyLength(256);
		$cylkat->put_PaddingScheme(0);
		$cylkat->put_EncodingMode('hex');
		$cylkat->SetEncodedIV($this->iv,'hex');
		$cylkat->SetEncodedKey($this->key,'hex');

		if($method == 1){
			$value = $value . "/". time();
			$value = $cylkat->encryptStringENC($value);
			return $value;
		}
		else if($method == 2){
			$value = $cylkat->decryptStringENC($value);
			$value = explode('/', $value);
			if(sizeof($value) == 1)
				return -1;
			return $value[0];
		}
		elseif ($method == 3) {
			$success = $cylkat->CkEncryptFile($inFile,$outFile);
			if($success == false){
				return 0;
			}
			return 1;
		}
		elseif ($method == 4) {
			$success = $cylkat->CkDecryptFile($inFile,$outFile);
			if($success == false){
				return 0;
			}
			return 1;
		}
		else{
			return -1; //wrong method
		}
	}

	public function newEncryptValue($method, $userData = false, $file = false){
		// return $userData;
		$optionMethod = $method['method']; 	//1 for encrypt and 2 for decrypt
		$optionPrime = $method['prime'];	//1-9 for prime number to use for encrypt and decrypt

		if($userData !== false){
			$userIDSession 	= $userData['userIDSession']; 	//user id session
			$apiKeySession 	= $userData['apiKeySession']; 	//apikey session
			$value 			= $userData['value']; 			//value to encrypt or decrypt
		} 
		
		if($file !== false){
			$inFile = $file['inFile']; 		//file in location
			$outFile = $file['outFile'];	//file out location
		}

		$stateConf = $this->setKey($optionPrime);
		if($stateConf == 0){
			return -1;
		}

		require_once(app_path() . '/library/chilkat_9_5_0.php');
		$cylkat = new CkCrypt2();
		$success = $cylkat->UnlockComponent('TOIENR.CB10417_3eRa2Tzam46h');
		if ($success != true) {
		    $error = $cylkat->lastErrorText() . "\n";
		    return $error;
		}

		$cylkat->put_CryptAlgorithm('aes');
		$cylkat->put_CipherMode('cbc');
		$cylkat->put_KeyLength(256);
		$cylkat->put_PaddingScheme(0);
		$cylkat->put_EncodingMode('hex');
		$cylkat->SetEncodedIV($this->iv,'hex');
		$cylkat->SetEncodedKey($this->key,'hex');

		if(!$file){
			if($optionMethod == 'encrypt'){
				$apiKeySession = $this->trimApiKey($apiKeySession); // return $apiKeySession;
				// return $value;
				$value = $value . "/" . time() . "/" . $apiKeySession . "/" . $userIDSession;
				// return $value;
				$value = $cylkat->encryptStringENC($value);
				return $value;
			}
			elseif ($optionMethod == 'decrypt') {
				$value = $cylkat->decryptStringENC($value);
				$value = explode('/', $value);
				if(empty($userIDSession) || $userIDSession == false){
					$userID = $value[0];
					$userIDSession_Encrypt = $value[3];
					$apiKeySession_Encrypt = $value[2];
					$state = $this->checkApi($apiKeySession, $apiKeySession_Encrypt, $userID, $userIDSession_Encrypt);
					if($state) return $userID;
					else return -2;
				}
				else{
					$userIDSession_Encrypt = $value[3];
					$apiKeySession_Encrypt = $value[2];
					if($userIDSession_Encrypt == $userIDSession)
						return $value[0];
					else
						return -2;
				}
			}
			else{
				return -2;
			}
		}
		else{
			if($optionMethod == 'encrypt'){
				$success = $cylkat->CkEncryptFile($inFile,$outFile);
				if($success == false){
					return 0;
				}
				return 1;
			}
			elseif ($optionMethod == 'decrypt') {
				$success = $cylkat->CkDecryptFile($inFile,$outFile);
				if($success == false){
					return 0;
				}
				return 1;
			}
			else{
				return -2;
			}
		}
	}

	public function trimApiKey($apiKeySession){
		$lengthApi = strlen($apiKeySession);
		$start = ceil($lengthApi / 2) - 3;
		$newApi = substr($apiKeySession, $start, 5);
		return $newApi;
	}

	public function checkApi($apiKeySession, $apiKeySession_Encrypt, $userIDValue, $userIDSession_Encrypt){
		if($apiKeySession_Encrypt !== $this->trimApiKey($apiKeySession))
			return false;
		if($userIDValue !== $userIDSession_Encrypt)
			return false;

		$authController = new AuthController;
		$state = $authController->checkAPIandUserID($apiKeySession_Encrypt, $userIDSession_Encrypt);
		if(sizeof($state) >= 0)
			return true;
		else
			return false;
	}

	public function testDecrypt(){
		$userID = Input::get('userID'); //encrypted (prime = 1)
		$apiKey = Input::get('apiKey');
		$valueTest = Input::get('valueTest'); //encrypted (prime = 1)

		if(empty($valueTest)) return -1;
		
		$userID = $this->encryptThis('decrypt', 1, false, $apiKey, $userID);
    $valueTest = $this->encryptThis('decrypt', 2, $userID, $apiKey, $valueTest);
    return $valueTest;
	}

	public function testEncrypt(){
		// return Input::all();
		$userID = Input::get('userID'); //encrypted (prime = 1)
		$apiKey = Input::get('apiKey');
		$valueTest = Input::get('valueTest'); //encrypted (prime = 2)

	    $userID = $this->encryptThis('decrypt', 1, $userID, $apiKey, $userID);
	    $valueTest = $this->encryptThis('encrypt', 2, $userID, $apiKey, $valueTest);
	    return [$valueTest];
	}

	public function testHere(){
		$userID = Input::get('userID'); //encrypted (prime = 1)
		$apiKey = Input::get('apiKey');

		$encryptController = new EncryptController;
		return $encryptController->encryptValue($userID, 2, 1);
	}
}
