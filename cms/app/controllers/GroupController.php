<?php

use Chrisbjr\ApiGuard\ApiGuardController;
use Carbon\Carbon;
class GroupController extends ApiGuardController{

  protected $apiMethods = [
     
      'createGroupProsess' => [
         'keyAuthentication' => true
      ],
      'memberGroup' => [
         'keyAuthentication' => true
      ],
      'inviteGroupProses' => [
         'keyAuthentication' => true
      ],
      'friendNotMember' => [
         'keyAuthentication' => true
      ],
      'adminAcceptRequest' => [
         'keyAuthentication' => true
      ],
      'notInterestGroup' => [
         'keyAuthentication' => true
      ],
      'getGroup' => [
         'keyAuthentication' => true
      ],
      'setCoverGroup' => [
         'keyAuthentication' => true
      ],
      'singlePostGroup' => [
         'keyAuthentication' => true
      ],
      'readAllCommentGroupPost' => [
         'keyAuthentication' => true
      ],
      'hideAllCommentGroupPost' => [
         'keyAuthentication' => true
      ],
      'seeAllCommentReplayGroup' => [
         'keyAuthentication' => true
      ],
      'groupPostLikeProses' => [
         'keyAuthentication' => true
      ],
      'adminLeaveGroup' => [
         'keyAuthentication' => true
      ],
      'memberLeaveGroup' => [
         'keyAuthentication' => true
      ],
      'sharedMyGroupProfile' => [
          'keyAuthentication' => true
      ]
  ];

  public function getGroup(){
    // return Input::all();
    $input = Input::all();
    $userID = $input['userID'];
    $apiKey = $input['apiKey'];
    $skip = $input['skip'];
    $take = $input['take'];

    $encryptController = new EncryptController;
    $userID = $encryptController->encryptValue($userID, 2 , 1);
    // $userID = $encryptController->encryptThis('decrypt', 1, false, $apiKey, $userID);

    $groupMember = GroupsMembers::where('user_id', '=', $userID)
                    ->where('invite_status', '=', 0)
                    ->get();;

    $count = $groupMember->count();
    // return $groupMember;

    $groupDetails = array();
    foreach ($groupMember as $key => $value) {
      $detailsGroup = Groups::where('id', '=', $value['group_id'])->first();
      $groupDetails[$key]['id'] = $encryptController->encryptValue($value['group_id'], 1, 5);
      // $groupDetails['id'] = $encryptController->encryptThis('encrypt', 5, $userID, $apiKey, $detailsGroup['id']);
      $groupDetails[$key]['name'] = $detailsGroup['name'];
      $groupDetails[$key]['picture'] = $detailsGroup['picture'];
      $groupDetails[$key]['description'] = $detailsGroup['description'];

      $member = DB::table('group_members')->where('group_id','=',$value['group_id'])->count();
      $groupDetails[$key]['member'] = $member;
    }

    $return = [
      'count' => $count,
      'data' => $groupDetails
    ];

    return $return;
  }

  public function createGroupProsess(){
// return Input::all();

    $userID = Input::get('userID');
  	$GroupName=Input::get('GroupName');
    $Description=Input::get('Description');
    $PhotoGroup=Input::get('PhotoGroup');
    $BackgroundGroup=Input::get('BackgroundGroup');
    $SettingGroup=Input::get('SettingGroup');

  	$encryptController = new EncryptController();
    $user_id = $encryptController->encryptValue($userID, 2, 1);

    $Group = new Groups;
    $Group->creator_id = $user_id;
    $Group->admin_id = $user_id;
    $Group->name= $GroupName;
    $Group->public = $SettingGroup;
    $Group->description = $Description;
    $Group->picture = $PhotoGroup;
    $Group->bg_picture = $BackgroundGroup;
    if($Group->save()){
    	$group_id = $Group->id;
    	$GroupMember = new GroupsMembers;
    	$GroupMember->group_id = $group_id;
    	$GroupMember->user_id = $user_id;
    	if($GroupMember->save()){
    		// return 'suksess';
    		$group_ID = $GroupMember->group_id;
    		$encryptController = new EncryptController();
    		$groupID = $encryptController->encryptValue($group_ID, 1, 5);
    		return $groupID;

    	}else{
    		return 'suksess create group';
    	}
    	
    }else{
    	return 'error';
    }


  }

  public function dataGroup(){
  	// return Input::all();
    $input = Input::all();
  	$userID = $input['userID'];
  	$groupID = $input['groupID'];
    $username = $input['username'];
    $encryptController = new EncryptController();
    $user_id = $encryptController->encryptValue($userID, 2, 1);
    $group_id = $encryptController->encryptValue($groupID, 2, 5);

    if($group_id != null ){
	    $countGroupMember = DB::table('group_members')
	      ->join('user_data', function($join)
	        {
	            $join->on('group_members.user_id', '=', 'user_data.id');   
	        })
	      ->join('groups', function($join)  
	        {
	            $join->on('group_members.group_id', '=', 'groups.id');     
	        })->where('group_members.group_id', '=', $group_id) ->where('group_members.invite_status', '=', '0')->get();
	     $count =  count($countGroupMember);
	     $countGroupMemberPending = DB::table('group_members')
	      ->join('user_data', function($join)
	        {
	            $join->on('group_members.user_id', '=', 'user_data.id'); 
	        })
	         ->join('groups', function($join)  
	        {
	            $join->on('group_members.group_id', '=', 'groups.id');     
	        })->where('group_members.group_id', '=', $group_id) ->where('group_members.invite_status', '=', '1')->get();
	     $countPending = count($countGroupMemberPending);
	     $countGroupMemberRequest = DB::table('group_members')
	      ->join('user_data', function($join)
	        {
	            $join->on('group_members.user_id', '=', 'user_data.id'); 
	        })
	      ->join('groups', function($join)
	        {
	            $join->on('group_members.group_id', '=', 'groups.id');     
	        })->where('group_members.group_id', '=', $group_id) ->where('group_members.invite_status', '=', '2')->get();
	     $countRequest = count($countGroupMemberRequest);

	  	$detailGroups = Groups::where('id', '=', $group_id)->get();
	  	// if(!empty($detailGroups) ){
	  	if(count($detailGroups) != 0 ){
		  	$encode = json_decode(json_encode($detailGroups), true);
		  	foreach ($encode as $key => $value) {
		  	  $checkadmin = $this->checkAdminGroup($value['id'],$user_id);
		  	  $joined = $this->checkJoined($value['id'], $user_id);
		  	  $request = $this->checkMyRequest($value['id'], $user_id);
		  	  $pending = $this->checkJoinedPendingFromUser($value['id'], $user_id);
		  	  $admin = $encryptController->encryptValue($value['admin_id'], 1, 1);

		  		$return[]= [
		           'group_name' => $value['name'],
		           'groupID'  => $groupID,
		           'checkAdmin' => $checkadmin,
		           'countMember' => $count,
		           'countPending' => $countPending,
		           'countRequest' => $countRequest,
		           'public'  =>$value['public'],
		           'picture' => $value['picture'],
		           'bg_picture' => $value['bg_picture'],
		           'checkJoined' => $joined,
		           'request' => $request,
		           'pending' => $pending,
		           'myuserID' => $userID,
               'myUsername' => $username,
		           'description' => $value['description'],
		           'admin' => $admin,
               'checkingEventAktiveAndEventWinner'  => $this->checkingEventAktiveAndEventWinner($group_id),

		  		];
		  	}
		  	$data = [
		  	   'data' => $return,
		  	];
		  	return $data;
	    }else{
	    	return '123';
	    }
    }else{
   	return '123';
    }
  }

  public function yourGroup(){
	// return Input::all();
	$input = Input::all();
	$userID = $input['userID'];
  $skip = $input['skip'];
  $take = $input['take'];
	$invite_status = $input['invite_status'];
	$encryptController = new EncryptController();
    $user_id = $encryptController->encryptValue($userID, 2, 1);

  $user_id_mine = $input['userID_mine'];
  $encryptController = new EncryptController();
    $user_mine = $encryptController->encryptValue($user_id_mine, 2, 1);

    $countGroup =  DB::table('groups')
	      ->join('group_members', function($join)
	        {
	            $join->on('groups.id', '=', 'group_members.group_id');
	          
	        })
	      ->where('group_members.user_id', '=', $user_id)
	      ->where('group_members.invite_status', '=', $invite_status)
	      ->groupBY('group_members.group_id')	    
	      ->addSelect('group_members.group_id as group_id',
	      	          'groups.name as name',
	      	          'groups.description as description',
	      	          'groups.picture as picture',
	      	          'groups.bg_picture as bg_picture',
	      	          'groups.creator_id as creator_id',
	      	          'groups.id as id'
	      	          )
	      ->get();
    $count = count($countGroup);
    if($count != 0){
       $mine = DB::table('group_members')->where('user_id', '=', $user_mine)->where('invite_status','=',0)->select('group_id')->get();
       foreach ($mine as $key) {
         $group_id_cek[] = $key->group_id;
       }
       $a = array_combine($group_id_cek, $group_id_cek);
    	
	     $GroupAdmin = DB::table('groups')
		      ->join('group_members', function($join)
		        {
		            $join->on('groups.id', '=', 'group_members.group_id');
		          
		        })
		      ->where('group_members.user_id', '=', $user_id)
		      ->where('group_members.invite_status', '=', $invite_status)
          // ->take($take)
          // ->skip($skip)
		      ->groupBY('group_members.group_id')
          ->where('groups.admin_id', '=', $user_id)
		      ->addSelect('group_members.group_id as group_id',
		      	          'groups.name as name',
		      	          'groups.description as description',
		      	          'groups.picture as picture',
		      	          'groups.bg_picture as bg_picture',
		      	          'groups.creator_id as creator_id',
		      	          'groups.id as id'
		      	          )
		      ->get();
	    
       $Group = DB::table('groups')
          ->join('group_members', function($join)
            {
                $join->on('groups.id', '=', 'group_members.group_id');        
            })
          ->where('group_members.user_id', '=', $user_id)
          ->where('group_members.invite_status', '=', $invite_status)
          // ->take($take)
          // ->skip($skip)
          ->groupBY('group_members.group_id')
          ->where('groups.admin_id', '!=', $user_id)
          ->addSelect('group_members.group_id as group_id',
                      'groups.name as name',
                      'groups.description as description',
                      'groups.picture as picture',
                      'groups.bg_picture as bg_picture',
                      'groups.creator_id as creator_id',
                      'groups.id as id'
                      )
          ->get();

      if(count($GroupAdmin) != 0 && count($Group) != 0){
          $merge = array_merge($GroupAdmin,$Group);
      }else if(count($GroupAdmin) != 0 && count($Group) == 0){
          $merge = $GroupAdmin;
      }else if(count($GroupAdmin) == 0 && count($Group) != 0){
          $merge = $Group;
      }

	    $encode = json_decode(json_encode($merge), true);
	    foreach ($encode as $key => $value) {
	    	$admin = $this->checkAdminGroup($value['id'],$user_id);
	    	$user_ID = $encryptController->encryptValue($value['creator_id'], 1, 1);
	    	$group_id = $encryptController->encryptValue($value['id'], 1, 5); //encript group id
        $group_id_dec = $value['id'];

        $member = DB::table('group_members')->where('group_id','=',$group_id_dec)->count();

	    	$data[] =[
	    	   'group_id' => $group_id,
	    	   'userID' => $user_ID,
	    	   'group_name' => $value['name'],
	    	   'description' => $value['description'],
	    	   'picture' => $value['picture'],
	    	   'bg_picture' => $value['bg_picture'],
	    	   'checkAdmin' => $admin,
           'group_id_dec' => $group_id_dec,
           'member'=> $member,
           'checkingEventAktiveAndEventWinner' => $this->checkingEventAktiveAndEventWinner($value['id']),
	    	];
	    }
	    $return = [
        'mine' => $a,
	      'data' => $data,
	      'count' => $count,
	    ];
	    return $return;
	} else{
	  $coba = '{"count":0,"data":[{ }] }';
        return $coba;
    }
  }

  public function memberGroup(){
  	// return Input::all();
  	$input = Input::all();
  	$userID = $input['userID'];
  	$group_id = $input['group_id'];
  	$invite_status = $input['invite_status'];
    $take = $input['take'];
    $skip = $input['skip'];
    $encryptController = new EncryptController();
    $user_id = $encryptController->encryptValue($userID, 2, 1);
    $groupID = $encryptController->encryptValue($group_id, 2, 5);
    
    $countGroupMember = DB::table('group_members')
      ->join('user_data', function($join)
        {
            $join->on('group_members.user_id', '=', 'user_data.id');
          
        })
         ->join('groups', function($join)
          
        {
            $join->on('group_members.group_id', '=', 'groups.id');     
        })->where('group_members.group_id', '=', $groupID) ->where('group_members.invite_status', '=', $invite_status)->get();
     $count =  count($countGroupMember);

     if($count != 0){
        if($invite_status == 0){
           $admin = DB::table('group_members')
              ->join('user_data', function($join)
              {
                  $join->on('group_members.user_id', '=', 'user_data.id');
                
              })
               ->join('groups', function($join)
                
              {
                  $join->on('group_members.group_id', '=', 'groups.id');     
              })
               ->where('group_members.group_id', '=', $groupID)
               ->where('user_data.id', '=', $countGroupMember[0]->admin_id)
               ->where('group_members.invite_status', '=', $invite_status)
               ->addSelect('group_members.group_id as group_id',
                         'group_members.user_id as user_id',
                         'groups.creator_id as creatorID',
                         'groups.name as group_name',
                         'user_data.name as real_name',
                         'user_data.username as username',
                         'user_data.profile_image',
                         'user_data.bg_image',
                         'groups.name as group_name')
               ->get();
             $GroupMember= DB::table('group_members')
              ->join('user_data', function($join)
              {
                  $join->on('group_members.user_id', '=', 'user_data.id');
                
              })
               ->join('groups', function($join)
                
              {
                  $join->on('group_members.group_id', '=', 'groups.id');     
              })
               ->where('group_members.group_id', '=', $groupID)
               ->where('user_data.id', '!=', $countGroupMember[0]->admin_id)
               ->where('group_members.invite_status', '=', $invite_status)
               ->addSelect('group_members.group_id as group_id',
                         'group_members.user_id as user_id',
                         'groups.creator_id as creatorID',
                         'groups.name as group_name',
                         'user_data.name as real_name',
                         'user_data.username as username',
                         'user_data.profile_image',
                         'user_data.bg_image',
                         'groups.name as group_name')
               ->get();
               $merge = array_merge($admin, $GroupMember);
               $encode = json_decode(json_encode($merge), true);
        }else{ 
  		     $GroupMember= DB::table('group_members')
  		        ->join('user_data', function($join)
  		        {
  		            $join->on('group_members.user_id', '=', 'user_data.id');
  		          
  		        })
  		         ->join('groups', function($join)
  		          
  		        {
  		            $join->on('group_members.group_id', '=', 'groups.id');     
  		        })
  		         ->where('group_members.group_id', '=', $groupID)
  		         ->where('group_members.invite_status', '=', $invite_status)
               // ->take($take)->skip($skip)
  		         ->addSelect('group_members.group_id as group_id',
  		         	         'group_members.user_id as user_id',
  		         	         'groups.creator_id as creatorID',
  		         	         'groups.name as group_name',
  		         	         'user_data.name as real_name',
  		         	         'user_data.username as username',
  		         	         'user_data.profile_image',
  		         	         'user_data.bg_image',
  		         	         'groups.name as group_name')
  		         ->get();
  		    $encode = json_decode(json_encode($GroupMember), true);
        }
		    foreach($encode as $key => $value){
		    	$user_ID = $encryptController->encryptValue($value['user_id'], 1, 1);
		    	$group_id = $encryptController->encryptValue($value['group_id'], 1, 5); //encript group id
		    	$ceatorID = $encryptController->encryptValue($value['creatorID'], 1, 1);
		    	$checkUserMember = $this->checkUserMember($user_id,$value['user_id']);
		    	$checkFriend = $this->checkFriend($user_id, $value['user_id']);
		    	$admin = $this->checkAdminGroup($groupID,$user_id);     // untuk cek apakah kita admin group atau bukan 
          $admin_group = $this->checkAdminGroup($groupID, $value['user_id']);  // untuk cek siapa yang jadi admin group dari keseluruhan member

		    	$data[] =[
		    	   'group_id' => $group_id,
		    	   'userID' => $user_ID,
		    	   'creatorID' => $ceatorID,
		    	   'group_name' => $value['group_name'],
		    	   'real_name' => $value['real_name'],
		    	   'username' => $value['username'],
		    	   'profile_image' => $value['profile_image'],
		    	   'bg_image' => $value['bg_image'],
		    	   'count' => $count,
		    	   'checkMember' => $checkUserMember,
		    	   'checkFriend' => $checkFriend,
		    	   'checkAdmin' => $admin,
             'admin_group' => $admin_group,
		    	];
		    }
		    $return = [
		      'data' => $data,
		      'count' => $count,
		    ];
		    return $return;
     }else{
        $return = [
           'count' => $count,
        ];
        return $return;
     }
  }

  public function MyFriendList(){
     // return Input::all();
    $input = Input::all();
  	$userID = $input['userID'];
  	$groupID = $input['group_id'];
    $encryptController = new EncryptController();
    $user_id = $encryptController->encryptValue($userID, 2, 1);
    $group_id = $encryptController->encryptValue($groupID, 2, 5);

     $check_member = DB::table('friend')
     ->join('group_members', function($join)
          
        {
            $join->on('friend.friend_id', '=', 'group_members.user_id');     
        })
     ->where('friend.user_id', '=', $user_id)
     ->where('group_members.group_id', '=', $group_id)
     ->where('group_members.user_id', '!=', $user_id)
     ->get();

    $a = DB::table('friend')
     ->join('group_members', function($join)
          
        {
            $join->on('friend.friend_id', '=', 'group_members.user_id');     
        })
     // ->where('group_members.user_id', '!=', 'friend.friend_id')
     ->where('friend.user_id', '=', $user_id)
     ->where('group_members.group_id', '=', $group_id)
     ->where('group_members.user_id', '!=', $user_id)
     ->where('group_members.invite_status', '!=', 0)
     ->where('friend.friend_status', '=', '1')
     ->groupBY('friend.friend_id')
     ->addSelect('friend.friend_id as friend_id')
     ->get();

     if(!empty($check_member)){
        if(!empty($a)){
          $friend = $a;
        }else{
          $friend = '';
        }
     }else{
      $friend = Friend::where('friend_status', '=', '1')
           ->where('user_id', '=', $user_id)
           ->select('friend_id as friend_id')
           ->get()->toArray();
     }

     $check_member2 =  DB::table('friend')
     ->join('group_members', function($join)
          
        {
            $join->on('friend.user_id', '=', 'group_members.user_id');     
        })
     ->where('friend.friend_id', '=', $user_id)
     ->where('group_members.group_id', '=', $group_id)
     ->where('group_members.user_id', '!=', $user_id)
     ->get();

     $b = DB::table('friend')
     ->join('group_members', function($join)
          
        {
            $join->on('friend.user_id', '=', 'group_members.user_id');     
        })
     // ->where('group_members.user_id', '!=', 'friend.user_id')
     ->where('friend.friend_id', '=', $user_id)
     ->where('group_members.group_id', '=', $group_id)
     ->where('group_members.user_id', '!=', $user_id)
     ->where('group_members.invite_status', '!=', 0)
     ->where('friend.friend_status', '=', '1')
     ->groupBY('friend.user_id')
     ->addSelect('friend.user_id as friend_id')
     ->get();

     if(!empty($check_member2) ){
        if(!empty($b)) {
          $friend2 = $b;
        }else{
          $friend2 = '';
        }
     }else{
      $friend2 = Friend::where('friend_status', '=', '1')
           ->Where('friend_id', '=', $user_id)
           ->select('user_id as friend_id')
           ->get()->toArray();
     }

     if($friend == 0  && $friend2 != 0){
        $merge = $friend2;
     }else if($friend != 0 && $friend2 == 0){
        $merge = $friend;
     }else if($friend != 0 && $friend2 != 0){
        $merge = array_merge($friend, $friend2);
     }else{
        $merge = '';
     }

     if(!empty($merge) ){
     	$encode = json_decode(json_encode($merge), true);
     	foreach ($encode as $key => $value) {
     		$username = $this->getFriendName($value['friend_id']);
            $user_ID = $encryptController->encryptValue($value['friend_id'], 1, 1);
            $return[] = [
               'friend_id' => $user_ID,
               'friend_username' => $username,
            ];
     	}
 	     $data =[
           'group_id' => $groupID,
           'data' => $return,
        ];
        return $data;
     }else{
     	$data = [
     	   'group_id' => $groupID,
     	   'data' => '',
     	];
        return $data;
     }
    
  }

  public function getFriendName($user_id){
		$data = User::where('id', '=', $user_id)->select('username')->first();
		return $data['username'];
		
	}

   public function inviteGroupProses(){
   	// return Input::all();
   	$input = Input::all();
   	$userID = $input['userID'];
   	$friendID = $input['friendID'];
   	$groupID = $input['groupID'];

   	$encryptController = new EncryptController();
    $user_id = $encryptController->encryptValue($userID, 2, 1);
    $group_id =  $encryptController->encryptValue($groupID, 2, 5);
   
//table group_member invite status 0=confirm 1=invite 2= request;
    for($i=0; $i<count($friendID); $i++)
        { 
    	     $Member = GroupsMembers::where('group_id', '=', $group_id)
               ->where('user_id', '=', $encryptController->encryptValue($friendID[$i], 2, 1))
               ->where('invite_status', '!=', '0')
               ->addSelect('user_id', 'id')
               ->first();

             $MemberConfirm = GroupsMembers::where('group_id', '=', $group_id)
               ->where('user_id', '=', $encryptController->encryptValue($friendID[$i], 2, 1))
               ->where('invite_status', '=', '0')
               ->addSelect('user_id', 'id')
               ->first();
              
            if($Member['user_id']){
            	$id = $Member['id'];
              $member_friend =  $Member['user_id'];
            	$member = GroupsMembers::find($id);
                $member->invite_status = '1';
                $member->blocked = '0';
                if($member->save() ){
                    // $notificationController = new NotificationController();
                    // $stateAdd = $notificationController->addNotiffication($user_id, $member_friend, $group_id, 341);
                	echo 'save member';
                	echo '<br>';
                }else{
                	echo 'gagal';
                }
            }
            else if($MemberConfirm['user_id']){
                echo 'sudah jadi member';
                echo '<br>';
            }
            else
           {
        	   $GroupsMembers = new GroupsMembers;
                $GroupsMembers->group_id = $group_id;
                $GroupsMembers->user_id = $encryptController->encryptValue($friendID[$i], 2, 1);
                $GroupsMembers->invite_status = '1';
                $GroupsMembers->blocked = '0';
                
                if($GroupsMembers->save()){
	                $user_admin = User::find($user_id);
  	            	$friend_id = $encryptController->encryptValue($friendID[$i], 2, 1);
  	            	$user_friend = User::find($friend_id);
  	            	$group = Groups::find($group_id);

	            	    $notificationController = new NotificationController();
                    $stateAdd = $notificationController->addNotiffication($user_id, $friend_id, $group_id, 341);

			           } 
          
            } 
        }
              
   }

  public function checkJoined($group_id, $user_id){
  	$joined = GroupsMembers::where('group_id', '=', $group_id)
  	   ->where('user_id', '=', $user_id)
  	   ->where('invite_status', '=', '0')
  	   ->where('blocked', '=', '0')
  	   ->first();
  	  if(count($joined) == 0){
  	  	return 'not join';
  	  } else{
  	  	return 'joined';
  	  }
  }

   public function checkJoinedPendingFromUser($group_id, $user_id){
  	$joined = GroupsMembers::where('group_id', '=', $group_id)
  	   ->where('user_id', '=', $user_id)
  	   ->where('invite_status', '=', '1')
  	   ->where('blocked', '=', '0')
  	   ->first();
  	  if(count($joined) == 0){
  	  	return 'no';
  	  } else{
  	  	return 'yes';
  	  }
  }

  public function checkMyRequest($group_id, $user_id){
  	$joined = GroupsMembers::where('group_id', '=', $group_id)
  	   ->where('user_id', '=', $user_id)
  	   ->where('invite_status', '=', '2')
  	   ->where('blocked', '=', '0')
  	   ->first();
  	  if(count($joined) == 0){
  	  	return 'no';
  	  } else{
  	  	return 'yes';
  	  }
  }

  public function checkAdminGroup($group_id,$user_id){
  	 $checkAdmin = Groups::where('id', '=', $group_id)
              ->where('admin_id', '=', $user_id)
              ->get();
          if(count($checkAdmin) == 0){
          	return 'not admin';
          }else{
          	return 'admin';
          }
  }

  public function getGroupID($event_id){
      $data = GroupsEvent::where('id', '=', $event_id)->first();
      return $data->group_id;
  }

  public function checkUserMember($user_id, $user_id_members){
  	 if($user_id == $user_id_members){
  	 	return 'me';
  	 }else{
  	 	return 'not me';
  	 }
  }

  public function checkFriend($user_id, $friend_id){
  	if($user_id != $friend_id){
      $checkfriend = Friend::where('user_id', '=', $user_id)
         ->where('friend_id', '=', $friend_id)
         ->get();

       $checkfriend2 = Friend::where('user_id', '=', $friend_id)
         ->where('friend_id', '=', $user_id)
         ->get();
   
        if(count($checkfriend) != 0){
	          if($checkfriend[0]['friend_status'] == '1'){
	          	return 'friend';
	          }else{
	          	return 'send request';
	          }
        }else if(count($checkfriend2) != 0){
              if($checkfriend2[0]['friend_status'] == '1'){
              	return 'friend';
              }else{
              	return 'not confirm';
              }
         }else{
         	return 'not friend';
         }
     }else{
     	return 'me';
     }    
  }

  public function adminLeaveGroup(){
     // return Input::all();
    $groupID = Input::get('groupID');
    $userID = Input::get('userID');
    $memberID = Input::get('memberID');

    // return $groupID;

    $encryptController = new EncryptController();
    $user_id = $encryptController->encryptValue($userID, 2, 1);
    $group_id =  $encryptController->encryptValue($groupID, 2, 5);
    $member_id =  $encryptController->encryptValue($memberID, 2, 1);
    
    $data = GroupsMembers::where('group_id', '=', $group_id)
             ->where('user_id', '=', $user_id)
             ->first();
    if($data->delete()){
        $admin = Groups::where('id', '=', $group_id)->first();
        $admin->admin_id = $member_id;
        if($admin->save()){
            $notificationController = new NotificationController();
            $stateAdd = $notificationController->addNotiffication($user_id, $member_id, $group_id, 321);
            return 1; 
        }else{
            return 0;
        }
    }else{
       return 2;
    }        

  }

  public function adminLeaveGroupTanpaMember(){
      $input = Input::all();
      $userID = $input['userID'];
      $groupID = $input['groupID'];

      $encryptController = new EncryptController();
      $user_id = $encryptController->encryptValue($userID, 2, 1);
      $group_id =  $encryptController->encryptValue($groupID, 2, 5);

      $group_member = GroupsMembers::where('group_id', '=', $group_id)->get();
      if($group_member->delete()){
          $group = Groups::where('id', '=', $group_id)->first();
          if($group->delete()){
              return 1;
          }else{
             return 0;
          }
      }else{
        return 2;
      }         
  }

  public function memberLeaveGroup(){
      // return Input::all();
      $groupID = Input::get('groupID');
      $userID = Input::get('userID');

      $encryptController = new EncryptController();
      $user_id = $encryptController->encryptValue($userID, 2, 1);
      $group_id =  $encryptController->encryptValue($groupID, 2, 5);

      $data = GroupsMembers::where('group_id', '=', $group_id)
             ->where('user_id', '=', $user_id)
             ->first();
      if($data->delete()){
          return 1;
      }else{
         return 0;
      }       
  }

   public function confirmGroup(){
   	// return Input::all();
   	$input = Input::all();
   	$userID = $input['userID'];
   	$groupID = $input['groupID'];

   	 $encryptController = new EncryptController();
	 $user_id = $encryptController->encryptValue($userID, 2, 1);
	 $group_id =  $encryptController->encryptValue($groupID, 2, 5);

	 $confirm = GroupsMembers::where('group_id', '=', $group_id)
          ->where('user_id', '=', $user_id)
          ->where('invite_status', '=', '1')
          ->where('blocked', '=', '0')
          ->first();
       
       $confirm->invite_status = '0';
       if($confirm->save()){
       	  // return 'sukses confirm';
	     $user = User::find($user_id);  
		 $group = Groups::find($group_id);
		 $admin = User::find($group->admin_id); 

		 $notificationController = new NotificationController();
         $stateAdd = $notificationController->addNotiffication($user_id, $group->admin_id, $group_id, 321);

		 // $emailController = new EmailController();
		 // $emailController->sendConfirmInvitedGroupEmail($admin , $user->username, $group->name, $groupID);     
       }else{
       	  return 'gagal confirm';
       }
   }

   public function notInterestGroup(){
   	// return Input::all();
   	 $input = Input::all();
   	 $userID = $input['userID'];
   	 $groupID = $input['groupID'];
   	 $invite_status = $input['invite_status'];

   	 $encryptController = new EncryptController();
	 $user_id = $encryptController->encryptValue($userID, 2, 1);
	 $group_id =  $encryptController->encryptValue($groupID, 2, 5);
	 
	 $notInterest = GroupsMembers::where('group_id', '=', $group_id)
          ->where('user_id', '=', $user_id)
          ->where('invite_status', '=', $invite_status)
          ->where('blocked', '=', '0')
          ->first();
    
       if($notInterest->delete()){
       	  return 'sukses confirm not Interest';
       }else{
       	  return 'gagal confirm';
       }
   }

   public function joinRequestGroup(){
   	 $input = Input::all();
   	 $userID = $input['userID'];
   	 $groupID = $input['groupID'];

     $encryptController = new EncryptController();
	   $user_id = $encryptController->encryptValue($userID, 2, 1);
	   $group_id =  $encryptController->encryptValue($groupID, 2, 5);
    
     $data = GroupsMembers::where('group_id', '=',  $group_id)->where('user_id', '=', $user_id)->first();
     if(count($data) == 0){
    	 $request = new GroupsMembers;
    	 $request->group_id = $group_id;
    	 $request->user_id = $user_id;
    	 $request->invite_status = '2';
    	 $request->blocked = '0';

  	  if($request->save()){
    	 	$user = User::find($user_id);
    		$group = Groups::find($group_id);
    		$admin = User::find($group->admin_id);

    		$notificationController = new NotificationController();
        $stateAdd = $notificationController->addNotiffication($user_id, $group->admin_id, $group_id, 311);
    		
        $data = [
           'userID' => $userID,
        ];
        return $data;
    		// $emailController = new EmailController();
    		// $emailController->sendJoinRequestGroupEmail($admin , $user->username, $group->name, $groupID);
       }else{
       	  return 2; // jika gagal
       }

     }else{
       return 0;
     }
	 
   }

   public function adminAcceptRequest(){
   
   	 $input = Input::all();
   	 $userID = $input['userID'];
     $groupID = $input['groupID'];
     $memberID = $input['memberID'];

   	 $encryptController = new EncryptController();
	 $user_id = $encryptController->encryptValue($userID, 2, 1);
	 $group_id =  $encryptController->encryptValue($groupID, 2, 5);
	 $member_id = $encryptController->encryptValue($memberID, 2, 1);
	 // echo $user_id; echo '<br>'; echo $group_id;echo '<br>';

	 $adminAccept = GroupsMembers::where('group_id', '=', $group_id)
	        ->where('invite_status', '=', '2')
	        ->where('user_id', '=', $member_id)
	        ->first();
 
	  $adminAccept->invite_status = '0';
	  if($adminAccept->save()){
	  	// return 'accept';
	  	 $user = User::find($user_id);  
		 $group = Groups::find($group_id);
		 $member = User::find($member_id); 

		 $notificationController = new NotificationController();
         $stateAdd = $notificationController->addNotiffication($user_id, $member_id, $group_id, 321);

		 // $emailController = new EmailController();
		 // $emailController->sendAdminAcceptRequestGroupEmail($member , $user->username, $group->name, $groupID);   
	  }else{
	  	return 'gagal';
	  }

   }

   public function adminNotAcceptRequest(){
    // return Input::all();
     $input = Input::all();
   	 $userID = $input['userID'];
     $groupID = $input['groupID'];
     $memberID = $input['memberID'];
     $invite_status = $input['invite_status'];

   	 $encryptController = new EncryptController();
	 $user_id = $encryptController->encryptValue($userID, 2, 1);
	 $group_id =  $encryptController->encryptValue($groupID, 2, 5);
	 $member_id = $encryptController->encryptValue($memberID, 2, 1);
	 // echo $user_id; echo '<br>'; echo $group_id;echo '<br>';echo $member_id;
   
	 $adminNotAccept = GroupsMembers::where('group_id', '=', $group_id)
	        ->where('invite_status', '=', $invite_status)
	        ->where('user_id', '=', $member_id)
	        ->first();
	  
	  if($adminNotAccept->delete()){
	  	return 'sukses delete';
	  }else{
	  	return 'gagal';
	  }
   }

   public function addToFriend(){
     $input = Input::all();
   	 $userID = $input['userID'];
     $friendID = $input['friendID'];

   	 $encryptController = new EncryptController();
	 $user_id = $encryptController->encryptValue($userID, 2, 1);
	 $friend_id = $encryptController->encryptValue($friendID, 2, 1);
     
     $checkfriend = Friend::where('user_id', '=', $user_id)
               ->where('friend_id', '=', $friend_id)->get();
         
     $checkfriend2 = Friend::where('user_id', '=', $friend_id)
               ->where('friend_id', '=', $user_id)->get();
          
     if(count($checkfriend) != 0 || count($checkfriend2) != 0){
     	return 0;
     }else{
     	 $friend = new Friend;
	     $friend->user_id = $user_id;
	     $friend->friend_id = $friend_id;
	     $friend->friend_status = '0';
	     if($friend->save()){
       
         $notificationController = new NotificationController();
         $stateAdd = $notificationController->addNotiffication($user_id, $friend_id, $user_id, 312);
	     	return 1;
	     }else{
	     	return -1;
	     }
     }

   }
   public function friendNotMember(){
   	// return Input::all();
   	$input = Input::all();
  	$userID = $input['userID'];
    // $genre = $input['genreCode'];
    $groupID = 1;
   //  $trackID = "4FF5B08C569DEF732E125DF7EF5D42B3";
    $encryptController = new EncryptController();
    $user_id = $encryptController->encryptValue($userID, 2, 1);
    $groupID = $encryptController->encryptValue($groupID, 1, 5);
    return $groupID;

   }

   public function profile(){
    $id1 = Input::get('id');
    $encryptController = new EncryptController;
    $id = $encryptController->encryptValue($id1, 2, 5);
      
    $data = DB::table('groups')->where('id','=',$id)->get();
    foreach ($data as $key) {
      $id = $key->id;
      $name = $key->name;
      $creator_id = $key->creator_id;
      $description = $key->description;
      $picture = $key->picture;
      $bg_picture = $key->bg_picture;
      $y_axisbg = $key->y_axisbg;
    }
    $array = array('id' => $id1,
                   'name' => $name, 
                   'creator_id' => $creator_id,
                   'description' => $description,
                   'picture' => $picture,
                   'bg_picture' => $bg_picture,
                   'y_axisbg' => $y_axisbg,
                  );
    return $array;
   }

   public function loadevent(){
    $groupID = Input::get('group_id');
    $userID = Input::get('userID');
    $encryptController = new EncryptController;
    $user_id = $encryptController->encryptValue($userID, 2, 1);
    $group_id = $encryptController->encryptValue($groupID, 2, 5);

    $data = DB::table('group_event')->where('group_id', '=', $group_id)
            ->join('user_data', 'group_event.creator_id', '=', 'user_data.id')
            ->select('user_data.username', 'group_event.title', 'group_event.id as group_event_id','group_event.description', 'group_event.start_time', 'group_event.end_time', 'group_event.tags', 'group_event.picture_event', 'group_event.setting_type', 'group_event.track_details_id','group_event.event_type','group_event.liked', 'group_event.shared', 'group_event.created_at as event_create', 'user_data.profile_image')
            ->orderBy('group_event.created_at', 'desc')
            ->get();
    if (!empty($data)) {
       $data_encode = json_decode(json_encode($data), true);
       foreach ($data_encode as $key => $value) {
         $likedGroupEvent = $this->checkDataLikeEvent($user_id, $value['group_event_id']);
         $statusMemberEventParticipate = $this->statusMemberEventParticipate($user_id,$group_id,$value['group_event_id']);
         $checkadmin = $this->checkAdminGroup($group_id,$user_id);
         $checkStatusMember = $this-> checkStatusMember($group_id, $user_id);

        $data_creator = GroupsEvent::where('id', '=', $value['group_event_id'])->first();
        $creator_event =  $data_creator->creator_id;
         $anggotaParticipant = $this->anggotaParticipantGroupEvent($group_id, $value['group_event_id'], $creator_event);
         $countAnggotaParticipant = count($anggotaParticipant);
         if(!empty($value['track_details_id'] || $value['track_details_id'] != 0)){
            $trackID = $encryptController->encryptValue($value['track_details_id'], 1, 3);
            $instrumentTrack = $this->DataTrackDetails($value['track_details_id'])['instrument_name'];
            $userIDCreatorTrack = $this->userIDCreatorTrack($value['track_details_id']);
            $creatorTrack = $this->getFriendName($userIDCreatorTrack);
            $priceTrack = $this->DataTrackDetails($value['track_details_id'])['is_free'];
            $judulTrack = $this->DataTrackDetails($value['track_details_id'])['name'];
         }else{
            $trackID = '';
            $instrumentTrack = '';
            $creatorTrack = '';
            $priceTrack = '';
            $judulTrack = '';
         }
         
              $return[] = [
                'username' => $value['username'],
                'title' => $value['title'],
                'group_event_id' => $value['group_event_id'],
                'description' => $value['description'],
                'start_time' => $value['start_time'],
                'end_time' => $value['end_time'],
                'tags' => $value['tags'],
                'liked' => $value['liked'],
                'shared' => $value['shared'],
                'picture_event' => $value['picture_event'],
                'event_create' => $value['event_create'],
                'profile_image' => $value['profile_image'],
                'likedGroupEvent' => $likedGroupEvent,
                'group_id' => $groupID,
                'statusMemberEventParticipate' => $statusMemberEventParticipate,
                'checkAdmin' => $checkadmin,
                'checkStatusMember' => $checkStatusMember,
                'anggotaParticipant' => $anggotaParticipant,
                'countAnggotaParticipant' => $countAnggotaParticipant,
                'setting_type' => $value['setting_type'],
                'event_type' => $value['event_type'],
                'trackID' => $trackID,
                'judulTrack' => $judulTrack,
                'instrumentTrack' => $instrumentTrack,
                'creatorTrack' => $creatorTrack,
                'priceTrack' =>$priceTrack,
                'group_name' => $this->getNameGroup($group_id)['name'],
                'group_type' => $this->getNameGroup($group_id)['public'],
              ];
            }
            return $return;     
        }else{
          $return = array();
          return $return;
        }
   }

   public function checkDataLikeEvent($user_id, $event_id) {
        $data = DB::table('group_event_activity')
          ->where('user_id', '=', $user_id)
          ->where('group_event_id', '=', $event_id)
          ->first();

        if(sizeof($data) == 0) {
            return 0;
        }else{
          return 1;   
        }
    }

   public function submitpostgroup(){
    $userID1 = Input::get('user_id');
    $encryptController = new EncryptController;
    $userID = $encryptController->encryptValue($userID1, 2, 1);

    $text = Input::get('text');
    $group_id = Input::get('group_id');
    $encryptController = new EncryptController;
    $group_id = $encryptController->encryptValue($group_id, 2, 5);
    // return $text;
    $track_id = Input::get('track_id');

    $post = new GroupPost;
    $post->group_id = $group_id;
    $post->user_id = $userID;
    $post->caption = $text;
    $post->track_id = $track_id;

    if ($post->save()) {

    if ($track_id == 0) {
      $datauser = DB::table('user_data')->where('id', '=', $userID)->get();
      foreach ($datauser as $key) {
        $username = $key->username;
        $profile_image = $key->profile_image;
      }
      $array = array('track_id'=> 0, 'post_id'=> $post->id, 'text'=> $text, 'userID'=> $userID, 'username'=>$username, 'profile_image'=>$profile_image, 'created_at'=>$post );
      return $array;
    }else {
      $track_id = Input::get('track_id');
      $track_id_enc = $encryptController->encryptValue($track_id, 1, 3);
      $track_details_id = DB::table('track')->where('id', '=', $track_id)->pluck('track_details_id');
      $artist = DB::table('track')->where('id', '=', $track_id)->pluck('track_id_create');
      $dataartist = DB::table('user_data')->where('id', '=', $artist)->pluck('username');
      $data = DB::table('track_details')->where('id','=',$track_details_id)->get();
      $datauser = DB::table('user_data')->where('id', '=', $userID)->get();
      foreach ($datauser as $key) {
        $username = $key->username;
        $profile_image = $key->profile_image;
      }
      foreach ($data as $key) {
        $judul = $key->name;
        $picture = $key->hashPicture;
        $genre = $key->genre;
      }

      $namegenre = DB::table('genres')->where('id', '=', $genre)->pluck('genre');
      $array = array('track_id'=>$track_id, 'track_id_enc'=>$track_id_enc, 'judul' => $judul, 'picture' => $picture, 'artist' => $dataartist, 'genre'=> $namegenre, 'post_id'=> $post->id, 'created_at'=>$post , 'text'=> $text, 'userID'=> $userID, 'username'=>$username, 'profile_image'=>$profile_image );
      return $array;
    }
  }
   }

   public function getpostgrup(){
      $group_id = Input::get('group_id');
      $encryptController = new EncryptController;
      $group_id = $encryptController->encryptValue($group_id, 2, 5);
     
     
     $data_group_post = DB::table('group_post')->where('group_id', '=', $group_id)
              ->leftJoin('track', 'group_post.track_id', '=', 'track.id')
              ->leftJoin('track_details', 'track.track_details_id', '=', 'track_details.id')
              ->leftJoin('genres', 'track_details.genre', '=', 'genres.id')
              ->leftJoin('user_data AS name_track', 'track.track_id_create', '=', 'name_track.id')
              ->join('user_data AS name_post', 'group_post.user_id', '=', 'name_post.id')
              ->select('name_post.username as username', 'name_track.username as artist', 'name_post.profile_image as profile_image', 'group_post.caption', 'group_post.created_at', 'group_post.id', 'group_post.track_id', 'group_post.liked', 'track.track_details_id','track_details.name', 'track_details.hashPicture', 'genres.genre')
              ->orderBy('group_post.created_at', 'desc')
              ->skip(0)->take(5)
              ->get();
    if (!empty($data_group_post)) {
      foreach ($data_group_post as $key) {
        $post_id= $key->id;
        $cek[] = GroupKomen::where('group_post_id', $post_id)->get();
        $post_id_array[]=$key->id;
      }
      foreach ($cek as $value) {
        $count[]=sizeof($value);
      }
      $list = array_combine($post_id_array, array_values($count));
      $data2 = json_decode(json_encode($data_group_post), true);
      foreach ($data2 as $key => $value) {
        if($value['track_details_id'] != null || $value['track_details_id'] != 0){
            $trackID = $encryptController->encryptValue($value['track_details_id'], 1, 3);
         }else{
             $trackID = $value['track_details_id'];
         }

        if($value['track_id'] == null || $value['track_id'] == 0 || $value['track_id'] == '0' || empty($value['track_id']) || $value['track_id'] == 'null'){
            $track_id = 0;
        }else{
            $track_id = $encryptController->encryptValue($value['track_id'], 1, 3);
        }
          $data[] = [
              'username' => $value['username'],
              'artist' => $value['artist'],
              'profile_image' => $value['profile_image'],
              'caption' => $value['caption'],
              'created_at' => $value['created_at'],
              'id' => $value['id'],
              'liked' => $value['liked'],
              'track_details_id' => $trackID,
              'track_id' => $track_id,
              'name' => $value['name'],
              'hashPicture' => $value['hashPicture'],
              'genre' => $value['genre']
          ];
      }
      
      // return 1;
      return compact('list', 'data');
    }else {
      $list = array();
       return compact('list', 'data');
    }
     
   }

   public function getmorepostgrup(){
      $group_id = Input::get('group_id');
      $skip = Input::get('skip');
      $encryptController = new EncryptController;
      $group_id = $encryptController->encryptValue($group_id, 2, 5);
      $user_id = Input::get('user_id');
      $encryptController = new EncryptController;
      $user_id = $encryptController->encryptValue($user_id, 2, 1);
      // $track_id = DB::table('group_post')->where('group_id', '=', $group_id)->select('track_id')->orderBy('group_post.created_at', 'desc')->skip($skip)->take(3)->get();
      $data_group_post = DB::table('group_post')->where('group_id', '=', $group_id)
              ->leftJoin('track', 'group_post.track_id', '=', 'track.id')
              ->leftJoin('track_details', 'track.track_details_id', '=', 'track_details.id')
              ->leftJoin('genres', 'track_details.genre', '=', 'genres.id')
              ->leftJoin('user_data AS name_track', 'track.track_id_create', '=', 'name_track.id')
              ->join('user_data AS name_post', 'group_post.user_id', '=', 'name_post.id')
              ->select('name_post.username as username', 'name_track.username as artist', 'name_post.profile_image as profile_image', 'group_post.caption', 'group_post.created_at', 'group_post.id', 'group_post.track_id', 'group_post.liked', 'track.track_details_id','track_details.name', 'track_details.hashPicture', 'genres.genre')
              ->orderBy('group_post.created_at', 'desc')
              ->skip($skip)->take(5)
              ->get();
      if (empty($data_group_post)) {
        $array= array();
        $data= array();
        $list = array();
        return compact('list', 'data');
      }else{
      foreach ($data_group_post as $key) {
        $post_id= $key->id;
        $cek[] = GroupKomen::where('group_post_id', $post_id)->get();
        $post_id_array[]=$key->id;
      }
      foreach ($cek as $value) {
        $count[]=sizeof($value);
      }
      $list = array_combine($post_id_array, array_values($count));
      $data2 = json_decode(json_encode($data_group_post), true);
      foreach ($data2 as $key => $value) {
        if($value['track_details_id'] != null || $value['track_details_id'] != 0){
            $trackID = $encryptController->encryptValue($value['track_details_id'], 1, 3);
         }else{
             $trackID = $value['track_details_id'];
         }
        if($value['track_id'] == null || $value['track_id'] == 0 || $value['track_id'] == '0' || empty($value['track_id']) || $value['track_id'] == 'null'){
            $track_id = 0;
        }else{
            
            $track_id = $encryptController->encryptValue($value['track_id'], 1, 3);
        }
          $data[] = [
              'username' => $value['username'],
              'artist' => $value['artist'],
              'profile_image' => $value['profile_image'],
              'caption' => $value['caption'],
              'created_at' => $value['created_at'],
              'id' => $value['id'],
              'liked' => $value['liked'],
              'track_details_id' => $trackID,
              'track_id' => $track_id,
              'name' => $value['name'],
              'hashPicture' => $value['hashPicture'],
              'genre' => $value['genre']
          ];
      }

      $data3= DB::table('group_post_comments')->whereIn('group_post_id', $post_id_array)
              ->join('user_data', 'group_post_comments.user_id', '=', 'user_data.id')
              ->select('user_data.username', 'user_data.profile_image', 'group_post_comments.comment', 'group_post_comments.group_post_id', 'group_post_comments.id', 'group_post_comments.created_at', 'group_post_comments.id')
              ->orderBy('group_post_comments.created_at', 'asc')
              ->get();

      if (!empty($data3)) {
        foreach ($data3 as $key) {
          $comment_id = $key->id;
          $group_post_id = $key->group_post_id;
          $username = $key->username;
          $profile_image = $key->profile_image;
          $comment = $key->comment;
          $created_at = $key->created_at;

          $time= $created_at;
          $ago= Carbon::createFromTimeStamp(strtotime($created_at))->diffForHumans();
          $liked[$comment_id] = DB::table('comments_activity')->where('user_id', '=', $user_id)->where('comment_id', '=', $comment_id)->pluck('comment_id');

          if ($liked[$comment_id] != null || !empty($liked[$comment_id])) {
            $like = 1;
          }else{
            $like = 0;
          }

          $data123[] =[
                 'comment_id' => $comment_id,
                 'group_post_id' => $group_post_id,
                 'username' => $username,
                 'profile_image' => $profile_image,
                 'comment' => $comment,
                 'created_at' => $ago,
                 'liked' => $like
              ];

           $replycomment[] = GroupKomenReply::where('comment_id', '=' , $comment_id)
            ->join('user_data', 'group_comment_reply.user_id', '=', 'user_data.id')
            ->select('group_comment_reply.created_at','comment_id','contents', 'user_id', 'user_data.username', 'user_data.profile_image','group_comment_reply.id')->get();
        }
      // return compact('data123', 'replycomment');
        // return $data123;
    }else{
      $data123 = array();
      $replycomment = array();
      
    }

      return compact('list', 'data', 'data123', 'replycomment');
    }
   }

   public function komenpostgrup(){
      $user_id = Input::get('user_id');
      $encryptController = new EncryptController;
      $user_id = $encryptController->encryptValue($user_id, 2, 1);

      $post_id = Input::get('post_id');
      $komen = Input::get('komen');
      // return $arrayName = array('komen' => $komen, 'user_id' => $user_id, 'post_id' => $post_id );
        $comment = new GroupKomen;
        $comment->group_post_id = $post_id;
        $comment->user_id = $user_id;
        $comment->comment = $komen;

        $cek = GroupPost::find($post_id);

        if ($comment->save()) {
            $notificationController = new NotificationController;
            $stateAdd = $notificationController->addNotiffication($user_id, $cek->user_id, $post_id, 122);
           return $comment;
        } else {
          return 0;
        }
      // $insert = DB::table('group_post_comments')->insertGetId(
      //             array('group_post_id' => $post_id, 'user_id' => $user_id, 'comment' => $komen)
      //             );

      //   if ($insert) {
      //     return 1;
      //   }else return 0;

       }

    public function readallcomments(){
      $post_id= Input::get('post_id');
      $data= DB::table('group_post_comments')->where('group_post_id', '=', $post_id)
              ->join('user_data', 'group_post_comments.user_id', '=', 'user_data.id')
              ->select('user_data.username', 'user_data.profile_image', 'group_post_comments.comment', 'group_post_comments.group_post_id', 'group_post_comments.id', 'group_post_comments.created_at', 'group_post_comments.id')
              ->orderBy('group_post_comments.created_at', 'asc')
              ->get();
      // return $data;
        foreach ($data as $key) {
          $comment_id = $key->id;
           $replycomment[] = GroupKomenReply::where('comment_id', '=' , $comment_id)
            ->join('user_data', 'group_comment_reply.user_id', '=', 'user_data.id')
            ->select('group_comment_reply.created_at','comment_id','contents', 'user_id', 'user_data.username', 'user_data.profile_image')->get();
        }
      return compact('data', 'replycomment');
    }

    public function readallcommentsNew(){
      $user_id = Input::get('user_id');
      $encryptController = new EncryptController;
      $user_id = $encryptController->encryptValue($user_id, 2, 1);
      $post_id= Input::get('post_id');
      $data= DB::table('group_post_comments')->whereIn('group_post_id', $post_id)
              ->join('user_data', 'group_post_comments.user_id', '=', 'user_data.id')
              ->select('user_data.username', 'user_data.profile_image', 'group_post_comments.comment', 'group_post_comments.group_post_id', 'group_post_comments.id', 'group_post_comments.created_at', 'group_post_comments.id')
              ->orderBy('group_post_comments.created_at', 'asc')
              ->get();
      // return $data;
      if (!empty($data)) {
        foreach ($data as $key) {
          $comment_id = $key->id;
          $group_post_id = $key->group_post_id;
          $username = $key->username;
          $profile_image = $key->profile_image;
          $comment = $key->comment;
          $created_at = $key->created_at;

          $time= $created_at;
          $ago= Carbon::createFromTimeStamp(strtotime($time))->diffForHumans();
          $liked[$comment_id] = DB::table('comments_activity')->where('user_id', '=', $user_id)->where('comment_id', '=', $comment_id)->pluck('comment_id');

          if ($liked[$comment_id] != null || !empty($liked[$comment_id])) {
            $like = 1;
          }else{
            $like = 0;
          }

          $data123[] =[
                 'comment_id' => $comment_id,
                 'group_post_id' => $group_post_id,
                 'username' => $username,
                 'profile_image' => $profile_image,
                 'comment' => $comment,
                 'created_at' => $ago,
                 'liked' => $like
              ];

           $replycomment[] = GroupKomenReply::where('comment_id', '=' , $comment_id)
            ->join('user_data', 'group_comment_reply.user_id', '=', 'user_data.id')
            ->select('group_comment_reply.created_at','comment_id','contents', 'user_id', 'user_data.username', 'user_data.profile_image')->get();
        }
      return compact('data123', 'replycomment');
        // return $data123;
    }else{
      $data123 = array();
      $replycomment = array();
      return compact('data123', 'replycomment');
    }
    }

    public function checklikekomen(){
      $user_id = Input::get('user_id');
      $encryptController = new EncryptController;
      $user_id = $encryptController->encryptValue($user_id, 2, 1);

      $comment_id = Input::get('comment_id');
      $liked = DB::table('comments_activity')->where('user_id', '=', $user_id)->whereIn('comment_id', $comment_id)->select('comment_id')->get();
      return $liked;
    }

    public function addevent(){
      // return Input::all();
      $title = Input::get('title');
      $desc = Input::get('description');
      $start_time = Input::get('start_time');
      $end_time = Input::get('end_time');
      $groupID = Input::get('group_id');
      $picture_event = Input::get('url_photoEvent');
      $tags = Input::get('genres');
      $trackID = Input::get('trackID');
      $eventType = Input::get('eventType');   

      $encryptController = new EncryptController;
      $group_id = $encryptController->encryptValue($groupID, 2, 5);
      $track_details_id = $encryptController->encryptValue($trackID, 2, 3);

      $userID = Input::get('user_id');
      $encryptController = new EncryptController;
      $user_id = $encryptController->encryptValue($userID, 2, 1);

      $group = Groups::where('id', '=', $group_id)->first();
      $group_name = $group->name;

      $track = Track::where('track_details_id', '=', $track_details_id)->first();
      $track_id = $track->id;
      $admin_name = $this->getFriendName($user_id);
      
       $insert = DB::table('group_event')->insertGetId(
        array('group_id' => $group_id, 'creator_id' => $user_id, 'title' => $title, 'start_time' => $start_time, 'end_time' => $end_time, 'description' => $desc, 'picture_event' => $picture_event, 'setting_type' => 0, 'track_details_id' => $track_details_id, 'event_type' => $eventType, 'tags' => $tags)
        );

        if ($insert) {

          $member = GroupsMembers::where('group_id', '=', $group_id)
                   ->where('user_id', '!=', $user_id)
                   ->where('invite_status', '=', 0)
                   ->get();    
          $decode = json_decode(json_encode($member), true);
          foreach ($decode as $key => $value) {   
             $notificationController = new NotificationController();
             $stateAdd[] = $notificationController->addNotiffication($user_id,$value['user_id'], $insert, 610);
          }        
          
          $start_time = new DateTime($start_time);
          $start_time =  date_format($start_time, 'd F Y H:i:s ');

          $end_time = new DateTime($end_time);
          $end_time =  date_format($end_time, 'd F Y H:i:s ');

          $string = "<a id='pjax' data-pjax='yes' style='text-decoration:none;' href='/".$admin_name."'>".$admin_name."</a> create event <a id='pjax' data-pjax='yes' style='text-decoration:none;' href='/event/".$insert."'>".$title."</a> at group <a id='pjax' data-pjax='yes' style='text-decoration:neone; cursor:pointer;' href='/group/latestpost/".$groupID."' >".$group_name."</a>\n".
                "event description : ".$desc." "."\n".
                "start time :"." ".$start_time." "."\n".
                "end time : "." ".$end_time;
          $caption = nl2br($string);
    
           $postGroup = new GroupPost;
           $postGroup->group_id = $group_id;
           $postGroup->user_id = $user_id;
           $postGroup->track_id = $track_id;
           $postGroup->caption = $caption;
           $postGroup->liked = 0;
           $postGroup->group_event_id = $insert;
           $postGroup->save();

           $postWall = new Post;
           $postWall->user_id = $user_id;
           $postWall->liked = 0;
           $postWall->track_id = $track_details_id;
           $postWall->post_content = $caption;
           $postWall->is_event = $insert;
           $postWall->is_public = 0;
           $postWall->shared = 0;
           $postWall->save();

          return 1;
        }else return 0;
    }


    public function groupSettingProses(){
    	// return Input::all();
    	$userID = Input::get('userID');
    	$groupID = Input::get('groupID');
    	$picture = Input::get('PhotoGroup');
    	$bg_picture = Input::get('BackgroundGroup');
    	$description = Input::get('Description');
    	$group_name = Input::get('GroupName');
    	$adminID = Input::get('admin');

    	$encryptController = new EncryptController;
    	$user_id = $encryptController->encryptValue($userID, 2, 1);
        $group_id = $encryptController->encryptValue($groupID, 2, 5);
        $admin_id = $encryptController->encryptValue($adminID, 2, 1);

        $group = Groups::where('id', '=', $group_id)->first(); 
        $group->name = $group_name;
        $group->description = $description;
        $group->picture = $picture;
        $group->bg_picture = $bg_picture;
        $group->admin_id = $admin_id;
        if($group->save()){
        	// return 'sukses setting group';
        	$update_group = Groups::where('id', '=', $group_id)->first();
        	return $update_group;
        }else{
        	return 'gagal setting group';
        }

    }

    public function anggotaMember(){
    	
    	$groupID = Input::get('groupID');
    	$userID = Input::get('userID');
    	$invite_status = Input::get('invite_status');

    	$encryptController = new EncryptController;
    	$user_id = $encryptController->encryptValue($userID, 2, 1);
        $group_id = $encryptController->encryptValue($groupID, 2, 5);

         $GroupMember= DB::table('group_members')
		        ->join('user_data', function($join)
		        {
		            $join->on('group_members.user_id', '=', 'user_data.id');
		          
		        })
		         
		         ->where('group_members.group_id', '=', $group_id)
		         ->where('group_members.invite_status', '=', $invite_status)
		         ->where('group_members.user_id', '!=', $user_id)
		         ->addSelect('group_members.group_id as group_id',
		         	         'group_members.user_id as user_id',
		         	         'user_data.name as real_name',
		         	         'user_data.username as username')     
		         ->get();
		         // return $GroupMember;
        if(count($GroupMember) != 0){
		    $encode = json_decode(json_encode($GroupMember), true);
		    foreach($encode as $key => $value){
		    	$user_ID = $encryptController->encryptValue($value['user_id'], 1, 1);
		    	$group_id = $encryptController->encryptValue($value['group_id'], 1, 5); 

		    	$data[] =[
		    	   'groupID' => $group_id,
		    	   'memberID' => $user_ID,
		    	   'username' => $value['username'],
		    	];
		    }
		    $return = [
		      'data' => $data,
		      
		    ];
		    return $return;

		}else{
			$coba = '{"data": }';
            return $coba;
		}
   
   }

   public function setCoverGroup(){
     // return Input::all();
    $groupID = Input::get("groupID");
    $y_axisbg = Input::get('y_axisbg');
    $userID = Input::get('userID');
    
    $encryptController = new EncryptController;
    $user_id = $encryptController->encryptValue($userID, 2, 1);
    $group_id = $encryptController->encryptValue($groupID, 2, 5);
      
    $group = Groups::find($group_id);
              $group->y_axisbg = Input::get('y_axisbg');
                         
              if ($group->save()) {
                return 1;
              }else return 2;

   }
   
    public function checklike(){
      $comment_id = Input::get('comment_id');
      $user_id = Input::get('user_id');
      $encryptController = new EncryptController;
      $user_id = $encryptController->encryptValue($user_id, 2, 1);

      $data = DB::table('comments_activity')->where('user_id', '=', $user_id)->where('comment_id', '=', $comment_id)->pluck('comment_id');
      return $data;

    }

    public function checklikepost(){
      $post_id = Input::get('post_id');
      $user_id = Input::get('user_id');
      $encryptController = new EncryptController;
      $user_id = $encryptController->encryptValue($user_id, 2, 1);

      $data = DB::table('group_post_activity')->where('user_id', '=', $user_id)->whereIn('group_post_id', $post_id)->select('group_post_id')->get();
      return $data;

    }

    public function checklikepostmore()
    {
      $post_id = Input::get('post_id');
      $user_id = Input::get('user_id');
      $encryptController = new EncryptController;
      $user_id = $encryptController->encryptValue($user_id, 2, 1);

      $data = DB::table('group_post_activity')->where('user_id', '=', $user_id)->where('group_post_id', '=', $post_id)->pluck('group_post_id');
      return $data;
    }

    public function getsongforpost()
    {
       $data = DB::table('track')->where('track_type_id', '=', 1)
               ->join('track_details', 'track.track_details_id', '=', 'track_details.id')
               ->join('user_data', 'track.track_id_create', '=', 'user_data.id')
               ->select('track_details.name', 'track_details.hashPicture', 'user_data.username', 'track.id', 'track.track_details_id','track_details.liked','track_details.shared','track_details.totalSpice')
               ->get();
        return $data;
    }

    public function gettrackproperty(){
      $track_id = Input::get('track_id');
      $track_details_id = DB::table('track')->where('id', '=', $track_id)->pluck('track_details_id');
      $artist = DB::table('track')->where('id', '=', $track_id)->pluck('track_id_create');
      $dataartist = DB::table('user_data')->where('id', '=', $artist)->pluck('username');
      $data = DB::table('track_details')->where('id','=',$track_details_id)->get();
      foreach ($data as $key) {
        $judul = $key->name;
        $picture = $key->hashPicture;
        $genre = $key->genre;
      }
      $namegenre = DB::table('genres')->where('id', '=', $genre)->pluck('genre');
      $array = array('judul' => $judul, 'picture' => $picture, 'username' => $dataartist, 'genre'=> $namegenre );
               // ->join('track_details', 'track.track_details_id', '=', 'track_details.id')
               // ->join('user_data', 'track.track_id_create', '=', 'user_data.id')
               // ->select('track_details.name', 'track_details.hashPicture', 'user_data.username', 'track.id')
               // ->get();
      return $array;
    }

    public function getTotalGroup($userID){
      $groupData = GroupsMembers::where('user_id', '=', $userID)
                    ->where('invite_status', '=', 0)
                    ->count();
      return $groupData;

    }

    public function submitkomenreplygrup(){
      $user_id = Input::get('user_id');
      $encryptController = new EncryptController;
        $user_id = $encryptController->encryptValue($user_id, 2, 1);

      $post_id = Input::get('post_id');
      $comment = Input::get('comment');
      $comment_id = Input::get('comment_id');

        $newComment = new GroupKomenReply;
        $newComment->comment_id = $comment_id;
        $newComment->user_id = $user_id;
        $newComment->contents = $comment;

        $cek = GroupKomen::find($comment_id);
        $notificationController = new NotificationController;
        $stateAdd = $notificationController->addNotiffication($user_id, $cek->user_id, $comment_id, 222);

        if($newComment->save()){
          // return $newComment;
            $user = User::find($user_id);
             // $user = User::where('id', '=', $user_id)->select('username', 'profile_image')->get();
            return compact('user','newComment');
        }

    }

    public function getGenreTag(){
        $data = Genre::select('genre')->get();
        return $data;
    }

    public function clickLikeGroupEventSave(){
        $userID = Input::get('userID');
        $event_id = Input::get('eventID');
        $encryptController = new EncryptController;
        $user_id = $encryptController->encryptValue($userID, 2, 1);
    
        $data = new GroupsEventActivity;
        $data->group_event_id = $event_id;
        $data->user_id = $user_id;
        $data->liked = 1;
        if($data->save()) {  
          $groupEvent = GroupsEvent::find($event_id);
          $liked = $groupEvent->liked;
          $groupEvent->liked = $liked +1;
          if($groupEvent->save()) {
               $data_event = GroupsEvent::find($event_id);

                $data_liked = [
                    'liked' => $data_event->liked,
                    'group_event_id' => $data_event->id,
                        ];
                    
                    $return = [
                        'data' => $data_liked,
                    ];
               return $return;
          }
        }  
    }

    public function groupEventParticipate(){
        // return Input::all();
        $userID = Input::get('userID');
        $groupID = Input::get('groupID');
        $groupEventID = Input::get('groupEventID');
        $status = Input::get('status');
        $encryptController = new EncryptController;
        $user_id = $encryptController->encryptValue($userID, 2, 1);
        $group_id = $encryptController->encryptValue($groupID, 2, 5);

        $data_creator = GroupsEvent::where('id', '=', $groupEventID)->first();
        $creator_event =  $data_creator->creator_id;
        $checkData = EventParticipant::where('group_id', '=', $group_id)
              ->where('event_id', '=', $groupEventID)
              ->where('user_id', '!=', $creator_event)
              ->where('status', '=', $status)
              ->get();
// return $checkData;
        if(count($checkData) != 0 ){
           // return 1;
            $dataParticipate = $this->anggotaParticipantGroupEvent($group_id, $groupEventID, $creator_event);
             $return[] = [
                 'dataParticipate' => $dataParticipate,
                 'countAnggotaParticipant' => count($dataParticipate),
             ];     
            return $return;
        }else{
           $dataParticipate = array();
           $return[] = [
              'dataParticipate' => $dataParticipate,
              'countAnggotaParticipant' => 0,
            ];
            return $return;  
        }      

        // $checkData = EventParticipant::where('group_id', '=', $group_id)
        //       ->where('event_id', '=', $groupEventID)
        //       ->where('user_id', '=', $user_id)
        //       ->first();

        // if(count($checkData) != 0){
        //      $checkData->status = $status;
        //      if($checkData->save()){
        //       $data_return = EventParticipant::where('group_id', '=', $group_id)
        //              ->where('event_id', '=', $groupEventID)
        //              ->where('user_id', '=', $user_id)
        //              ->first();

        //          $dataParticipate = $this->anggotaParticipantGroupEvent($group_id, $groupEventID);
        //          $return[] = [
        //              'data' => $data_return,
        //              'dataParticipate' => $dataParticipate,
        //              'countAnggotaParticipant' => count($dataParticipate),
        //          ];     
        //        return $return;
        //      }else{
        //          return $checkData;
        //      }
        // }else{
        //     $data = new EventParticipant;
        //     $data->group_id = $group_id;
        //     $data->event_id = $groupEventID;
        //     $data->user_id = $user_id;
        //     $data->status = $status;
        //     if($data->save()){
        //       // return 'save';
        //       $data_return = EventParticipant::where('group_id', '=', $group_id)
        //              ->where('event_id', '=', $groupEventID)
        //              ->where('user_id', '=', $user_id)
        //              ->first();

        //          $dataParticipate = $this->anggotaParticipantGroupEvent($group_id, $groupEventID);
        //          $return[] = [
        //              'data' => $data_return,
        //              'dataParticipate' => $dataParticipate,
        //              'countAnggotaParticipant' => count($dataParticipate),
        //          ];     
        //       return $return;
        //     }else{
        //       return 'gagal';
        //     }
        // }
        
    }

    public function statusMemberEventParticipate($user_id,$group_id,$group_event_id){
         $data = Project::where('event_id', '=', $group_event_id)
               ->where('user_id', '=', $user_id)
               ->first();
          if(count($data) != 0){
            return 1;
          }else{
             return 0;
          }      
             
    }

    public function checkStatusMember($group_id, $user_id){
        $data = GroupsMembers::where('group_id', '=', $group_id)
              ->where('user_id', '=', $user_id)
              ->where('invite_status', '=', 0)
              ->where('blocked', '=', 0)
              ->first();
        if(count($data) != 0){
          return 'member';
        }else{
           return 'not member';
        }      
    }

    public function anggotaParticipantGroupEvent($group_id, $group_event_id, $creator_event){
      
       $data = DB::table('project')
          ->join('user_data', function($join)              
            {
                $join->on('project.user_id', '=', 'user_data.id');     
            })
          ->where('project.event_id', '=', $group_event_id)
          ->addSelect('user_data.username as username')
          ->get();

          return $data;
        
    }

    public function DataTrackDetails($track_details_id){
      if(!empty($track_details_id) || $track_details_id != 0 || $track_details_id != null || $track_details_id != '0' || $track_details_id != 'null' || $track_details_id != ''){
        $data = TrackDetails::where('id', '=', $track_details_id)->first();
        return $data;
      }else{
        return '';
      }
    }

    public function singlePostGroup(){
        // return Input::all();
        $userID = trim(Input::get('userID'));
        
        $group_post_id = trim(Input::get('group_post_id'));
        $groupID = trim(Input::get('group_id'));

        $encryptController = new EncryptController;
        $user_id = $encryptController->encryptValue($userID, 2, 1);
        $group_id = $encryptController->encryptValue($groupID, 2, 5);
 
        $dataPost = DB::table('group_post')
            ->join('user_data', function($join)
                {
                    $join->on('group_post.user_id', '=', 'user_data.id');
                })

            ->where('group_post.id', '=', $group_post_id)
            ->addSelect('group_post.id as group_post_id',
                        'group_post.user_id as user_id_post',
                        'group_post.caption as caption',
                        'group_post.track_id as track_id',
                        'user_data.username as username',
                        'user_data.profile_image as profile_image_post',
                        'group_post.created_at as create',
                        'group_post.liked as like')
            ->get();
        $decode = json_decode(json_encode($dataPost), true);
         $dataComment = $this->singlePostGroupComment($group_post_id,5,$user_id);
         $dataCommentTotal = $this->singlePostGroupComment($group_post_id,false,$user_id);
         if($dataCommentTotal != 0){
            $count = count($dataCommentTotal);
         }else{
            $count = 0;
         }       
         // return $dataComment;
        foreach ($decode as $key => $value) {
          $statusLikePostGroup= $this->statusLikePostGroup($group_post_id, $user_id);
          $myUserData = $this->myUserData($user_id);
          $myUserData_encode = json_decode(json_encode($myUserData),true);
          $myUsername = $myUserData_encode[0]['username'];
          $myImageProfile = $myUserData_encode[0]['profile_image'];
           $userID_post = $encryptController->encryptValue($value['user_id_post'], 1, 1);
           if($value['track_id'] != 0 || $value['track_id'] != '0'){
               $dataTrack = DB::table('track')->where('id', '=', $value['track_id'])->first();
               $dataTrack_encode = json_decode(json_encode($dataTrack), true);
               $track_details_id = $dataTrack_encode['track_details_id'];  // mendapatkan track_details_id
               $trackID = $encryptController->encryptValue($track_details_id, 1, 3); // encrypt track_details_id
               $judulTrack = $this->DataTrackDetails($track_details_id)['name'];   // judul track
               $pictureTrack = $this->DataTrackDetails($track_details_id)['hashPicture'];   // picture track
               $genre = $this->DataTrackDetails($track_details_id)['genre'];     // mendapatkan genre id
               $genreTrack = $this->getGenreTrack($genre);                        // nama genre track
               $creator_track_id = $dataTrack_encode['track_id_create'];       // mendapatkan creator id track
               $creatorTrack = $this->getFriendName($creator_track_id);      // mendapatkan nama cretaor track
           }else{
               $trackID = '';
               $judulTrack = '';
               $pictureTrack = '';
               $genreTrack = '';
               $creatorTrack = '';
           } 
          
             $data[] = [
                 'myUserID' => $userID,
                 'statusLikePostGroup' => $statusLikePostGroup,
                 'myUsername' => $myUsername,
                 'myImageProfile' => $myImageProfile,
                 'group_post_id' => $value['group_post_id'],
                 'groupID' => $groupID,
                 'userID_post' => $userID_post,
                 'caption' => $value['caption'],
                 'trackID' => $trackID,
                 'judulTrack' => $judulTrack,
                 'pictureTrack' => $pictureTrack,
                 'genreTrack' => $genreTrack,
                 'creatorTrack' => $creatorTrack,
                 'usernamePost' => $value['username'],
                 'profile_image_post' => $value['profile_image_post'],
                 'create' => $value['create'],
                 'like' => $value['like'],
                 'totalComment' => $count,
                 'dataComment' => $dataComment,
                 'statusGroupMember' => $this->checkStatusMember($group_id, $user_id),
                 'group_name' => $this->getNameGroup($group_id)['name'],
                 'group_type' => $this->getNameGroup($group_id)['public'],
             ];
        }

        return $data;
    }

    public function singlePostGroupComment($group_post_id,$take,$user_id){

        $data = DB::table('group_post_comments')
            ->join('user_data', function($join)
            {
                $join->on('group_post_comments.user_id', '=', 'user_data.id');
            })
              ->where('group_post_comments.group_post_id', '=', $group_post_id)
              ->orderBy('group_post_comments.created_at', '=', 'desc')
              ->addSelect('group_post_comments.group_post_id as group_post_id',
                          'group_post_comments.id as group_post_comment_id',
                          'group_post_comments.user_id as userID_comment',
                          'group_post_comments.comment as comment',
                          'group_post_comments.liked as like',
                          'group_post_comments.created_at as create_comment',
                          'user_data.username as username_comment',
                          'user_data.profile_image as profile_image_comment')
              ->take($take)
              ->get();
              
          if(count($data) != 0){
              $decode = json_decode(json_encode($data),true);
              foreach ($decode as $key => $value) {
                 $encryptController = new EncryptController;
                 $userID_comment = $encryptController->encryptValue($value['userID_comment'], 1, 1);
                 $dataCommentReplay = $this->singlePostGroupCommentReplay($value['group_post_comment_id'],5);
                 $dataCommentReplayTotal = $this->singlePostGroupCommentReplay($value['group_post_comment_id'],false);
                 $statusLikeCommentGroupPost = $this->statusLikeCommentGroupPost($value['group_post_comment_id'],$user_id);
                 if($dataCommentReplayTotal != 0){
                    $count = count($dataCommentReplayTotal);
                 }else{
                    $count = 0;
                 }
                    $return[] = [
                        'group_post_id' => $value['group_post_id'],
                        'group_post_comment_id' => $value['group_post_comment_id'],
                        'comment' => $value['comment'],
                        'like' => $value['like'],                       
                        'statusLikeCommentGroupPost' => $statusLikeCommentGroupPost,
                        'create_comment' => $value['create_comment'],
                        'username_comment' => $value['username_comment'],
                        'profile_image_comment' => $value['profile_image_comment'],
                        'userID_comment' => $userID_comment,
                        'totalCommentReplay' => $count,
                        'dataCommentReplayTotal' => $dataCommentReplay
                    ];
              }    
              return $return;    
          }else{
             return 0;
          }    
    }

    public function singlePostGroupCommentReplay($group_post_comment_id,$take){
        $data = DB::table('group_comment_reply')
            ->join('user_data', function($join)
              {
                  $join->on('group_comment_reply.user_id', '=', 'user_data.id');
              })
            ->where('group_comment_reply.comment_id', '=', $group_post_comment_id)
            ->orderBy('group_comment_reply.created_at', '=', 'desc')
            ->addSelect('group_comment_reply.id as group_comment_reply_id',
                        'group_comment_reply.comment_id as group_post_comment_id',
                        'group_comment_reply.user_id as userID_commentReplay',
                        'group_comment_reply.contents as contents',
                        'group_comment_reply.created_at as create',
                        'group_comment_reply.liked as like',
                        'user_data.username as username_commentReplay',
                        'user_data.profile_image as profile_image_comment_replay')
            ->take($take)
            ->get();
        if(count($data) != 0){
            $decode = json_decode(json_encode($data),true);
            foreach ($decode as $key => $value) {
                 $encryptController = new EncryptController;
                 $userID_commentReplay = $encryptController->encryptValue($value['userID_commentReplay'], 1, 1);
                 $return[] = [
                      'group_comment_reply_id' => $value['group_comment_reply_id'],
                      'group_post_comment_id' => $value['group_post_comment_id'],
                      'contents' => $value['contents'],
                      'create' => $value['create'],
                      'like' => $value['like'],
                      'username_commentReplay' => $value['username_commentReplay'],
                      'profile_image_comment_replay' => $value['profile_image_comment_replay'],
                      'userID_commentReplay' => $userID_commentReplay
                 ];
            }
            return $return;
        }else{
            return 0;
        }
    } 

    public function myUserData($user_id){
         $data = DB::table('user_data')->where('id', '=', $user_id)->get();
         return $data;
    }

    public function getGenreTrack($genre){
        $data = DB::table('genres')->where('id', '=', $genre)->select('genre')->get();
        $decode = json_decode(json_encode($data), true);
        return $decode[0]['genre'];
    }

    public function readAllCommentGroupPost(){
        $group_post_id = Input::get('group_post_id');
        $userID = Input::get('userID');

        $encryptController = new EncryptController;
        $user_id = $encryptController->encryptValue($userID, 2, 1);
        $data = $this->singlePostGroupComment($group_post_id,false,$user_id);
        return $data;
    }

    public function hideAllCommentGroupPost(){
        $group_post_id = trim(Input::get('group_post_id'));
        $userID = trim(Input::get('userID'));

        $encryptController = new EncryptController;
        $user_id = $encryptController->encryptValue($userID, 2, 1);
        $data = $this->singlePostGroupComment($group_post_id,5,$user_id);
        return $data;
    }

    public function seeAllCommentReplayGroup(){
        $group_post_comment_id = trim(Input::get('group_post_comment_id'));
        $userID = trim(Input::get('userID'));

        $encryptController = new EncryptController;
        $user_id = $encryptController->encryptValue($userID, 2, 1);
        $data = $this->singlePostGroupCommentReplay($group_post_comment_id,false);
        return $data; 
    }

    public function statusLikePostGroup($group_post_id, $user_id){
        $data = GroupPostActivity::where('group_post_id', '=', $group_post_id)
              ->where('user_id', '=', $user_id)->first();
         if(count($data) != 0){
             if($data->liked == 1 || $data->liked == '1'){
                 return 1;
             }else{
                  return 0;
             }
         }else{
            return 0;
         }     
    }
   
   public function statusLikeCommentGroupPost($group_post_comment_id,$user_id){
       $data = GroupPostCommentsActivity::where('group_post_comment_id', '=', $group_post_comment_id)
          ->where('user_id', '=', $user_id)->first();
        if(count($data) != 0) {
            if($data->liked == 1 || $data->liked == '1'){
               return 1;
            }else{
              return 0;
            }
        }else{
           return 0;
        }
   }

   public function groupPostLikeProses(){
      $userID = trim(Input::get('userID'));
      $group_post_id = trim(Input::get('group_post_id'));
      $like_total = trim(Input::get('like_total'));

      $encryptController = new EncryptController;
      $user_id = $encryptController->encryptValue($userID, 2, 1);
       $data =  GroupPostActivity::where('group_post_id', '=', $group_post_id)
              ->where('user_id', '=', $user_id)->first();
              // return count($data);
          if(count($data) != 0){
              if($data->liked == 1){
                  return 1;
              } else{
                  $data->liked = 1;
                  if($data->save()){
                     $dataPostGroup = GroupPost::where('id', '=', $group_post_id)->first();
                      $liked = $dataPostGroup->liked + 1;
                      $dataPostGroup->liked = $liked;
                      if($dataPostGroup->save()){
                        return 'save data';
                      }else{
                        return 'gagal';
                      }
                  }
              }
          }else{
              
             $saveData = new GroupPostActivity;
              $saveData->group_post_id = $group_post_id;
              $saveData->user_id = $user_id;
              $saveData->liked = 1;
              if($saveData->save()){
                  $dataPostGroup = GroupPost::where('id', '=', $group_post_id)->first();
                  $liked = $dataPostGroup->liked + 1;
                  $dataPostGroup->liked = $liked;
                  if($dataPostGroup->save()){
                    return 'save new data';
                  }else{
                    return 'gagal';
                  }
                  
              }
              
          }  
   }

   public function groupCommentPostLikeProses(){
       $userID = trim(Input::get('userID'));
       $group_post_comment_id = trim(Input::get('group_post_comment_id'));
       // $like_total = trim(Input::get('like_total'));

       $encryptController = new EncryptController;
       $user_id = $encryptController->encryptValue($userID, 2, 1);
       $data = GroupPostCommentsActivity::where('group_post_comment_id', '=', $group_post_comment_id)
               ->where('user_id', '=', $user_id)->first();

        if(count($data) != 0){
             if($data->liked == 1){
                 return 1;
             }else{
                 $data->liked = 1;
                 if($data->save()){
                     $dataComment = GroupKomen::where('id', '=', $group_post_comment_id)->first();
                     $liked = $dataComment->liked + 1;
                     $dataComment->liked = $liked;
                     if($dataComment->save()){
                         return 'save data';
                     }else{
                        return 'gagal';
                     }
                 }
             }
        }else{
            $saveData = new GroupPostCommentsActivity;
            $saveData->group_post_comment_id = $group_post_comment_id;
            $saveData->user_id = $user_id;
            $saveData->liked = 1;
            if($saveData->save()){
                $dataCommentGroup = GroupKomen::where('id', '=', $group_post_comment_id)->first();
                $liked = $dataCommentGroup->liked + 1;
                $dataCommentGroup->liked = $liked;
                if($dataCommentGroup->save()){
                    return 'save new data';
                }else{
                     return 'gagal';
                }
            }
        }      
   }

   public function groupCommentPostUnlikeProses(){
       $userID = trim(Input::get('userID'));
       $group_post_comment_id = trim(Input::get('group_post_comment_id'));
       $encryptController = new EncryptController;
       $user_id = $encryptController->encryptValue($userID, 2, 1);

       $data = GroupPostCommentsActivity::where('group_post_comment_id', '=', $group_post_comment_id)
               ->where('user_id', '=', $user_id)->first();
          $liked = $data->liked - 1;
          $data->liked = $liked;
          if($data->save()){
              $dataCommentGroup = GroupKomen::where('id', '=', $group_post_comment_id)->first();
              $liked = $dataCommentGroup->liked - 1;
              $dataCommentGroup->liked = $liked;
              if($dataCommentGroup->save()){
                  return 'update';
              }else{
                 return 'gagal save';
              }
              
          }else{
             return 'gagal';
          }     
   }

  public function userIDCreatorTrack($track_details_id){
    $data = Track::where('track_details_id', '=', $track_details_id)->first();
    return $data->track_id_create;
  }

  public function getNameGroup($group_id){
      $data = Groups::where('id', '=', $group_id)->first();
      // $group_name = $data->name;
      return $data;
  }

  public function checkingEventAktiveAndEventWinner($group_id){
      $time = time();
      $tanggal = (date("Y-m-d"." "."H-m-s",$time));

      $event_berjalan = DB::table('group_event')
         ->where('group_id', '=', $group_id)
         ->where('end_time', '>', $tanggal)
         ->get();

       if(count($event_berjalan) != 0){
          return 1;  //ada event yang belum selesai;
        }else{
           return 0;
           $event_selesai = DB::table('group_event')
              ->where('group_id', '=', $group_id)
              ->where('end_time', '<', $tanggal)
              ->get();
              // return count($event_selesai);
              $decode = json_decode(json_encode($event_selesai),true);
              foreach ($decode as $key => $value) {
                  $data_song_participated = DB::table('project_tracks')
                                ->join('project', function($join)
                                  {
                                      $join->on('project_tracks.project_id', '=', 'project.id');        
                                  })
                                ->join('track_details', function($join)
                                  {
                                      $join->on('project_tracks.track_details_id', '=', 'track_details.id');        
                                  })
                                ->where('project.event_id', '=', $value['id'])
                                ->where('project.status', '=', 1)
                                ->where('project_tracks.is_buy_track', '=', 0)
                                ->where('project_tracks.wrapped', '=', 1)
                                ->get();
                       if(count($data_song_participated) != 0){
                            $return = DB::table('group_event_winner')
                                   ->where('group_event_id', '=', $value['id'])
                                   ->first();
                            if(count($return) != 0){
                                $data_return[] = 1;  // event selesai ada pemenang
                            }else{
                               $data_return[] = 0;  // event selesai belum ada pemenang
                            }             
                       }else{
                           $data_return[] = 2;  // event selesai tapi tidak ada participate yang release song
                       }            
                 
              }
              // return count($data_return);
             if (in_array('0', $data_return)) {
                  return 1;  // ada event yang sudah selesai tapi belum ada pemenang
              }else{
                return 0;   // semua event selesai, sudah ada pemenang && event selesai tapi tidak ada participate yang release song
              }
        }
  }

  public function sharedMyGroupProfile(){
      $userID = Input::get('userID');
      $groupID = Input::get('group_id');
      $group_name = Input::get('group_name');
      $group_picture = Input::get('group_picture');
      $myUsername = Input::get('myUsername');

      $encryptController = new EncryptController;
      $user_id = $encryptController->encryptValue($userID, 2, 1);
      $group_id = $encryptController->encryptValue($groupID, 2, 5);

       $string = "<a id='pjax' data-pjax='yes' style='text-decoration:none;' href='/".$myUsername."'>".$myUsername."</a> share a group profile\n".
                "group_name : <a id='pjax' data-pjax='yes' style='text-decoration:none; cursor:pointer;' href='/group/latestpost/".$groupID."'>".$group_name."\n";
               
      $caption = nl2br($string);

      $post = new Post;
      $post->user_id = $user_id;
      $post->track_id = 0;
      $post->pic_content = $group_picture;
      $post->post_content = $caption;
      $post->is_public = 1;
      if($post->save()){
         return 1;
      }else{
        return 0;
      }

  } 
       
}



