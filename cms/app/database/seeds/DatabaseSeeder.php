<?php

class DatabaseSeeder extends Seeder {

    public function run()
    {
        $this->call('GenreSub_Comedy');
        $this->command->info('Genre table seeded!');
    }

}

class GenreTableSeeder extends Seeder {

    public function run(){
        DB::table('genres')->delete();

        $genre = array(
        	array('genre' => 'Alternative', 		'description' => 'http://www.musicgenreslist.com/music-alternative/'),
        	array('genre' => 'Anime', 				'description' => 'http://www.musicgenreslist.com/music-anime/'),
        	array('genre' => 'Blues', 				'description' => 'http://www.musicgenreslist.com/music-blues/'),
        	array('genre' => 'Children’s Music', 	'description' => 'http://www.musicgenreslist.com/music-childrens-music/'),
        	array('genre' => 'Classical Music', 	'description' => 'http://www.musicgenreslist.com/music-classical/'),
        	array('genre' => 'Comedy', 				'description' => 'http://www.musicgenreslist.com/music-comedy/'),
        	array('genre' => 'Commercial', 			'description' => ''),
        	array('genre' => 'Country', 			'description' => 'http://www.musicgenreslist.com/music-country/'),
        	array('genre' => 'Dance', 				'description' => 'http://www.musicgenreslist.com/music-dance/'),
        	array('genre' => 'Disney', 				'description' => 'http://www.musicgenreslist.com/music-disney/'),
        	array('genre' => 'Easy Listening', 		'description' => 'http://www.musicgenreslist.com/music-easy-listening/'),
        	array('genre' => 'Electronic', 			'description' => 'http://www.musicgenreslist.com/music-electronic/'),
        	array('genre' => 'Enka', 				'description' => 'http://www.musicgenreslist.com/music-enka/'),
        	array('genre' => 'French Pop', 			'description' => 'http://www.musicgenreslist.com/music-french-pop/'),
        	array('genre' => 'German Folk', 		'description' => 'http://www.musicgenreslist.com/music-german-folk/'),
        	array('genre' => 'German Pop', 			'description' => 'http://www.musicgenreslist.com/music-german-pop/'),
        	array('genre' => 'Fitness & Workout', 	'description' => 'http://www.musicgenreslist.com/music-health-fitness/'),
        	array('genre' => 'Hip-Hop/Rap', 		'description' => 'http://www.musicgenreslist.com/music-hip-hop-rap/'),
        	array('genre' => 'Holiday', 			'description' => 'http://www.musicgenreslist.com/music-holiday/'),
        	array('genre' => 'Indie Pop', 			'description' => 'http://www.musicgenreslist.com/music-indie-pop/'),
        	array('genre' => 'Industrial', 			'description' => 'http://www.musicgenreslist.com/music-industrial-pop/'),
        	array('genre' => 'Inspirational', 		'description' => 'http://www.musicgenreslist.com/music-inspirational/'),
        	array('genre' => 'Instrumental', 		'description' => 'http://www.musicgenreslist.com/music-instrumental/'),
        	array('genre' => 'J-Pop', 				'description' => 'http://www.musicgenreslist.com/music-j-pop/'),
        	array('genre' => 'Jazz', 				'description' => 'http://www.musicgenreslist.com/music-jazz/'),
        	array('genre' => 'K-Pop', 				'description' => 'http://www.musicgenreslist.com/music-k-pop/'),
        	array('genre' => 'Karaoke', 			'description' => 'http://www.musicgenreslist.com/music-karaoke/'),
        	array('genre' => 'Kayokyoku', 			'description' => 'http://www.musicgenreslist.com/music-kayokyoku/'),
        	array('genre' => 'Latin', 				'description' => 'http://www.musicgenreslist.com/music-latin/'),
        	array('genre' => 'New Edge', 			'description' => 'http://www.musicgenreslist.com/music-new-age/'),
        	array('genre' => 'Opera', 				'description' => 'http://www.musicgenreslist.com/music-opera/'),
        	array('genre' => 'Pop', 				'description' => 'http://www.musicgenreslist.com/music-pop/'),
        	array('genre' => 'R&B / Soul', 			'description' => 'http://www.musicgenreslist.com/music-r-b-soul/'),
        	array('genre' => 'Reggae', 				'description' => 'http://www.musicgenreslist.com/music-reggae/'),
        	array('genre' => 'Rock', 				'description' => 'http://www.musicgenreslist.com/music-rock/'),
        	array('genre' => 'Singer/Songwriter', 	'description' => 'http://www.musicgenreslist.com/music-singer-songwriter/'),
        	array('genre' => 'Soundtrack', 			'description' => 'http://www.musicgenreslist.com/music-soundtrack/'),
        	array('genre' => 'Spoken Word', 		'description' => 'http://www.musicgenreslist.com/music-spoken-word/'),
        	array('genre' => 'Tex-Mex / Tejano', 	'description' => 'http://www.musicgenreslist.com/tex-mex-tejano-music-genre/'),
        	array('genre' => 'Vocal', 				'description' => 'http://www.musicgenreslist.com/music-vocal/'),
        	array('genre' => 'World', 				'description' => 'http://www.musicgenreslist.com/music-world/')
        );
		
		DB::table('genres')->insert($genre);
    }
}

class GenreSub_Alternative extends Seeder{

	public function run(){
		DB::table('genre_sub')->delete();

		$sub_genre = array(
			array('genre_id' => '1', 'genre_name' => 'Art Punk', 		'description' => ''),
			array('genre_id' => '1', 'genre_name' => 'Alternative', 	'description' => ''),
			array('genre_id' => '1', 'genre_name' => 'College', 		'description' => ''),
			array('genre_id' => '1', 'genre_name' => 'Experimental', 	'description' => ''),
			array('genre_id' => '1', 'genre_name' => 'Goth / Gothic', 	'description' => ''),
			array('genre_id' => '1', 'genre_name' => 'Grunge', 			'description' => ''),
			array('genre_id' => '1', 'genre_name' => 'Hardcore Punk', 	'description' => ''),
			array('genre_id' => '1', 'genre_name' => 'Hard', 			'description' => ''),
			array('genre_id' => '1', 'genre_name' => 'Indie', 			'description' => ''),
			array('genre_id' => '1', 'genre_name' => 'Lo-fi', 			'description' => ''),
			array('genre_id' => '1', 'genre_name' => 'New Wave', 		'description' => ''),
			array('genre_id' => '1', 'genre_name' => 'Progressive', 	'description' => ''),
			array('genre_id' => '1', 'genre_name' => 'Punk', 			'description' => ''),
			array('genre_id' => '1', 'genre_name' => 'Shoegaze', 		'description' => ''),
			array('genre_id' => '1', 'genre_name' => 'Steampunk', 		'description' => '')
		);
		
		DB::table('genre_sub')->insert($sub_genre);
	}
}

class GenreSub_Blues extends Seeder{

	public function run(){
		$sub_genre = array(
			array('genre_id' => '3', 'genre_name' => 'Acoustic', 		'description' => ''),
			array('genre_id' => '3', 'genre_name' => 'Chicago', 		'description' => ''),
			array('genre_id' => '3', 'genre_name' => 'Classic', 		'description' => ''),
			array('genre_id' => '3', 'genre_name' => 'Contemporary', 	'description' => ''),
			array('genre_id' => '3', 'genre_name' => 'Country', 		'description' => ''),
			array('genre_id' => '3', 'genre_name' => 'Delta', 			'description' => ''),
			array('genre_id' => '3', 'genre_name' => 'Electric', 		'description' => '')
		);

		DB::table('genre_sub')->insert($sub_genre);
	}
}

class GenreSub_Children extends Seeder{

	public function run(){
		$sub_genre = array(
			array('genre_id' => '4', 'genre_name' => 'Lullabies', 		'description' => ''),
			array('genre_id' => '4', 'genre_name' => 'Sing-Along', 		'description' => ''),
			array('genre_id' => '4', 'genre_name' => 'Stories', 		'description' => '')
		);

		DB::table('genre_sub')->insert($sub_genre);
	}
}

class GenreSub_Classical extends Seeder{

	public function run(){
		$sub_genre = array(
			array('genre_id' => '5', 'genre_name' => 'Avant-Garde', 		'description' => ''),
			array('genre_id' => '5', 'genre_name' => 'Baroque', 		'description' => ''),
			array('genre_id' => '5', 'genre_name' => 'Chamber Music', 		'description' => ''),
			array('genre_id' => '5', 'genre_name' => 'Chant', 		'description' => ''),
			array('genre_id' => '5', 'genre_name' => 'Choral', 		'description' => ''),
			array('genre_id' => '5', 'genre_name' => 'Classical Crossover', 		'description' => ''),
			array('genre_id' => '5', 'genre_name' => 'Early Music', 		'description' => ''),
			array('genre_id' => '5', 'genre_name' => 'High Classical', 		'description' => ''),
			array('genre_id' => '5', 'genre_name' => 'Impressionist', 		'description' => ''),
			array('genre_id' => '5', 'genre_name' => 'Medieval', 		'description' => ''),
			array('genre_id' => '5', 'genre_name' => 'Minimalism', 		'description' => ''),
			array('genre_id' => '5', 'genre_name' => 'Modern Composition', 		'description' => ''),
			array('genre_id' => '5', 'genre_name' => 'Opera', 		'description' => ''),
			array('genre_id' => '5', 'genre_name' => 'Orchestral', 		'description' => ''),
			array('genre_id' => '5', 'genre_name' => 'Renaissance', 		'description' => ''),
			array('genre_id' => '5', 'genre_name' => 'Romantic', 		'description' => ''),
			array('genre_id' => '5', 'genre_name' => 'Wedding Music', 		'description' => '')
		);

		DB::table('genre_sub')->insert($sub_genre);
	}
}

class GenreSub_Comedy extends Seeder{

	public function run(){
		$sub_genre = array(
			array('genre_id' => '6', 'genre_name' => 'Novelty', 		'description' => ''),
			array('genre_id' => '6', 'genre_name' => 'Standup Comedy', 		'description' => ''),
			array('genre_id' => '6', 'genre_name' => 'Vaudeville', 		'description' => '')
		);

		DB::table('genre_sub')->insert($sub_genre);
	}
}