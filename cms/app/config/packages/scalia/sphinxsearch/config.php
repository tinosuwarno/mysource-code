<?php

return array (
	'host'    => '103.43.45.217',
	'port'    => 3306,
     'timeout' => 30,
	'indexes' => array (
		// 'my_index_name' => array ( 'table' => 'keywords', 'column' => 'id' ),
		'user_index' => array ( 'table' => 'user_data', 'column' => 'username', 'modelname' => 'User' ),
	)
);
