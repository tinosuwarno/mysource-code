<?php 

return array( 
	
	/*
	|--------------------------------------------------------------------------
	| oAuth Config
	|--------------------------------------------------------------------------
	*/

	/**
	 * Storage
	 */
	'storage' => 'Session', 

	/**
	 * Consumers
	 */
	'consumers' => array(

		/**
		 * Facebook
		 */
        'Facebook' => array(
            'client_id'     => '783798388413863',
            'client_secret' => '2ab9f8089c7b8d9cb2b1908491c2573f',
            'scope'         => array('email','read_friendlists','user_online_presence'),
        ),		

	)

);