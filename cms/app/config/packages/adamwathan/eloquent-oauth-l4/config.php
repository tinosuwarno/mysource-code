<?php

return array(
	'table' => 'oauth_identities',
	'providers' => array(
		'facebook' => array(
			'client_id' => '783798388413863',
			'client_secret' => '2ab9f8089c7b8d9cb2b1908491c2573f',
			'redirect_uri' => URL::to('facebook/login'),
			'scope' => array('email', 'public_profile'),
		),
		'google' => array(
			'client_id' => '572007800141-q1jaaj56sjjv1go4ehpa3na9jjs4l20s.apps.googleusercontent.com',
			'client_secret' => 'yDICVB7VvKFASTcsXSBOsxKs',
			'redirect_uri' => URL::to('gplus/login'),
			'scope' => array(),
		),
		'github' => array(
			'client_id' => '12345678',
			'client_secret' => 'y0ur53cr374ppk3y',
			'redirect_uri' => URL::to('your/github/redirect'),
			'scope' => array(),
		),
		'linkedin' => array(
			'client_id' => '12345678',
			'client_secret' => 'y0ur53cr374ppk3y',
			'redirect_uri' => URL::to('your/linkedin/redirect'),
			'scope' => array(),
		),
		'instagram' => array(
			'client_id' => '12345678',
			'client_secret' => 'y0ur53cr374ppk3y',
			'redirect_uri' => URL::to('your/instagram/redirect'),
			'scope' => array(),
		),
	)
);
