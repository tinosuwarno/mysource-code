<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class DeleteApiLogs extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'logs:cron';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Delete ApiLogs';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$logsdat = DB::table('api_logs')->get();;
		
		if(count($logsdat > 0)){
			$deletelogs = DB::table('api_logs')->delete();

			if($deletelogs){
				$this->info('Success delete logs');
			} else {
				$this->info('failed delete logs');
			}

		} else {
			$this->info('API Logs table is empty');
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	// protected function getArguments()
	// {
	// 	return array(
	// 		array('example', InputArgument::REQUIRED, 'An example argument.'),
	// 	);
	// }

	protected function getArguments() {
      return array();
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
