<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ProjectCron extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'project:cron';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Delete Pending Karaoke Project After 48 hours';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{

		$today = date('Y-m-d H:i:s');
		$datPenProject = Project::where('status', '=', 0)->get();

		$datProject = '';

		if($datPenProject){
			$countDat = count($datPenProject);
			$countLimitProject = 0;
			if($countDat > 0){
				$this->info('Today : '.$today.'\n');
				$this->info('Total Pending Project : '.$countDat.'\n');
				foreach ($datPenProject as $dat) {

					$createdAtLimit = date('Y-m-d H:i:s', strtotime($dat['created_at'].' +2 day'));

					if($createdAtLimit <= $today){
						$projectID = $dat['id'];
						$userID = $dat['user_id'];

						//$userDataFolder = User::where('id', '=', $userID)->pluck('track_folderHash');

						$getdatProjectTracks = ProjectTracks::where('project_id', '=', $projectID)->where('user_id', '=', $userID)->get();
						$countdatProjectTracks = count($getdatProjectTracks);

						//$this->info($countdatProjectTracks);

						if($countdatProjectTracks > 0){
							//$this->info($projectID);
							foreach ($getdatProjectTracks as $datProjectTracks) {
								$trackDetailsID = $datProjectTracks['track_details_id'];

								if(($datProjectTracks['is_buy_track'] == 1) && ($datProjectTracks['wrapped'] == 0) && ($countdatProjectTracks == 1)){

									$deleteProjectTracks = ProjectTracks::where('project_id', '=', $projectID)->where('user_id', '=', $userID)->delete();

									$deleteProjectPendingLimit = Project::where('id', '=', $projectID)->delete();

									$countLimitProject++;
								}
								
								
							}
							
						}	


					} else{
						$this->info('No Pending Project more than 48 hours at this time');
					}


				} 

				if($countLimitProject > 0){
					$this->info('Sukses Delete Pending Project with Limit : '.$countLimitProject.'\n');	
				}

			} else{
				$this->info('No Pending at this time');
			}
			
		} else {
			$this->info('Cannot get Data');
		}
		

		/*$projectController = new ProjectController;

		$projectController->deletePendingProject();*/
		
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	// protected function getArguments()
	// {
	// 	return array(
	// 		array('example', InputArgument::REQUIRED, 'An example argument.'),
	// 	);
	// }

	protected function getArguments() {
      return array();
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
