<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	// Singleton (global) object
  App::singleton('frontLocation', function(){
    $app = new stdClass;
    $app->frontLocation = 'http://localhost:8000/';
    // $app->serverLocation = 'c420cms.gritjam.com/';
    if (Auth::check()) {
    	// Put your User object in $app->user            
    	// $app->serverLocation = 'c420cms.gritjam.com/';
    	$app->frontLocation = 'http://localhost:8000/';
        // $app->user = Auth::User();
        // $app->isLoggedIn = TRUE;
    }
    else {
        // $app->isLoggedIn = FALSE;
    }
    return $app;
  });

  App::singleton('baseUrl', function(){
    $base = new stdClass;
    $base->baseUrl = 'http://localhost:8080/';
    if (Auth::check()) {
    	$base->baseUrl = 'http://localhost:8080/';
    }
    return $base;
  });
});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest())
	{
		if (Request::ajax())
		{
			return Response::make('Unauthorized', 401);
		}
		else
		{
			return Redirect::guest('login');
		}
	}
});

Route::filter('authDesktop', function(){
	$input = Input::all();
	$userID = $input['userID'];
	$apiKey = $input['apiKey'];

	$authController = new AuthController;
	$encryptController = new EncryptController;
	$userID = $encryptController->encryptThis('decrypt', 1, false, $apiKey, $userID);
	$checkApi = $authController->checkAPIandUserID($userID, $apiKey);
	if($checkApi['status'] != 1){
		$status  = [
			'response' => 400,
            'message' => 'failed'
		];
		return $status;
	}
});

Route::filter('auth.basic', function()
{
	return Auth::basic("username");
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});



//Function Filter
Route::filter('fungsi_permission', function()
	{ 
		$id_user = Auth::user()->id;
		$group_menu_cek = UserGroupMenu::where('id_user', '=', $id_user)->first();

		if ($group_menu_cek['id_groupmenu'] != 1) {
			
			$url = Request::segment(1).'/'.Request::segment(2).'/';
		    // $url = 'menu/index';

		    $result = DB::table('menu')
	        ->leftJoin('fungsi', 'menu.id', '=', 'fungsi.id_menu')
	        ->leftJoin('menu_group_menu_fungsi', 'menu_group_menu_fungsi.id_fungsi', '=', 'fungsi.id')
	        ->leftJoin('menu_group_menu', 'menu_group_menu.id', '=', 'menu_group_menu_fungsi.id_menugroupmenu')
	        ->leftJoin('user_group_menu', 'user_group_menu.id', '=', 'menu_group_menu.id_groupmenu')
	        ->leftJoin('group_menu', 'group_menu.id', '=', 'user_group_menu.id_groupmenu')
	        ->leftJoin('user_data', 'user_data.id', '=', 'user_group_menu.id_user')
	        ->select('fungsi.url_akses')
	        ->where('fungsi.url_akses', '=', $url)
	        ->where('menu_group_menu.id_groupmenu', '=', $group_menu_cek['id_groupmenu'])->get();

		  	if (sizeof($result)==0) {
			     // do something
			     return Redirect::to('/naughty'); 
		   	}
		}

	});

Route::filter('fungsi_permission_2', function()
	{ 
		$id_user = Auth::user()->id;
		$group_menu_cek = UserGroupMenu::where('id_user', '=', $id_user)->first();

		if ($group_menu_cek['id_groupmenu'] != 1) {

			$url = Request::segment(1).'/'.Request::segment(2);
		    // $url = 'menu/index';

		    $result = DB::table('menu')
	        ->leftJoin('fungsi', 'menu.id', '=', 'fungsi.id_menu')
	        ->leftJoin('menu_group_menu_fungsi', 'menu_group_menu_fungsi.id_fungsi', '=', 'fungsi.id')
	        ->leftJoin('menu_group_menu', 'menu_group_menu.id', '=', 'menu_group_menu_fungsi.id_menugroupmenu')
	        ->leftJoin('user_group_menu', 'user_group_menu.id', '=', 'menu_group_menu.id_groupmenu')
	        ->leftJoin('group_menu', 'group_menu.id', '=', 'user_group_menu.id_groupmenu')
	        ->leftJoin('user_data', 'user_data.id', '=', 'user_group_menu.id_user')
	        ->select('fungsi.url_akses')
	        ->where('fungsi.url_akses', '=', $url)
	        ->where('menu_group_menu.id_groupmenu', '=', $group_menu_cek['id_groupmenu'])->get();

		  	if (sizeof($result)==0) {
			     // do something
			     return Redirect::to('/naughty'); 
		   	}
	   }
	});


Route::filter('fungsi_permission_3', function()
	{ 
		$id_user = Auth::user()->id;
		$group_menu_cek = UserGroupMenu::where('id_user', '=', $id_user)->first();

		if ($group_menu_cek['id_groupmenu'] != 1) {
			
			$url = Request::segment(1).'/'.Request::segment(2).'/'.Request::segment(3);
		    // $url = 'menu/index';

		    $result = DB::table('menu')
	        ->leftJoin('fungsi', 'menu.id', '=', 'fungsi.id_menu')
	        ->leftJoin('menu_group_menu_fungsi', 'menu_group_menu_fungsi.id_fungsi', '=', 'fungsi.id')
	        ->leftJoin('menu_group_menu', 'menu_group_menu.id', '=', 'menu_group_menu_fungsi.id_menugroupmenu')
	        ->leftJoin('user_group_menu', 'user_group_menu.id', '=', 'menu_group_menu.id_groupmenu')
	        ->leftJoin('group_menu', 'group_menu.id', '=', 'user_group_menu.id_groupmenu')
	        ->leftJoin('user_data', 'user_data.id', '=', 'user_group_menu.id_user')
	        ->select('fungsi.url_akses')
	        ->where('fungsi.url_akses', '=', $url)
	        ->where('menu_group_menu.id_groupmenu', '=', $group_menu_cek['id_groupmenu'])->get();

		  	if (sizeof($result)==0) {
			     // do something
			     return Redirect::to('/naughty'); 
		   	}
		}

	});

Route::filter('fungsi_permission_4', function()
	{ 
		$id_user = Auth::user()->id;
		$group_menu_cek = UserGroupMenu::where('id_user', '=', $id_user)->first();

		if ($group_menu_cek['id_groupmenu'] != 1) {
		$url = Request::segment(1).'/'.Request::segment(2).'/'.Request::segment(3).'/';
	    // $url = 'menu/index';

		
		    $result = DB::table('menu')
	        ->leftJoin('fungsi', 'menu.id', '=', 'fungsi.id_menu')
	        ->leftJoin('menu_group_menu_fungsi', 'menu_group_menu_fungsi.id_fungsi', '=', 'fungsi.id')
	        ->leftJoin('menu_group_menu', 'menu_group_menu.id', '=', 'menu_group_menu_fungsi.id_menugroupmenu')
	        ->leftJoin('user_group_menu', 'user_group_menu.id', '=', 'menu_group_menu.id_groupmenu')
	        ->leftJoin('group_menu', 'group_menu.id', '=', 'user_group_menu.id_groupmenu')
	        ->leftJoin('user_data', 'user_data.id', '=', 'user_group_menu.id_user')
	        ->select('fungsi.url_akses')
	        ->where('fungsi.url_akses', '=', $url)
	        ->where('menu_group_menu.id_groupmenu', '=', $group_menu_cek['id_groupmenu'])->get();

		  	if (sizeof($result)==0) {
			     // do something
			     return Redirect::to('/naughty'); 
		   	}
	   }
	});

