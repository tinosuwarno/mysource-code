<?php

$fileDir = '/Applications/XAMPP/xamppfiles/htdocs/gritjam_cms/testEncrypt/';

$p = '0400C3251F9D1B85CF1DAB211D179F154A103E44629398BF4E1AE0B20212CEF5A3D516DB364A6F5884A305A13CF84C196FB6AD7DBF4794837FE68B04F6EECC4EACF071624D6E31C42159580164CFAA1315609AECF441C381E62550B88ED1716D84D8323406E3913DE4E587B64C2CC80790D3074216680B5A2DC0239FBD0BD817D807';
$g = 5;

include("chilkat_9_5_0.php");

// ------------------------------------- ENKRIPSI ---------------------------------- //
// CREATE INSTANCE USER
$dhFrontEncrypt = new CkDh();

$dhFrontEncrypt->UnlockComponent('Anything for 30-day trial');

// SET PRIME DAN GENERATOR
$dhFrontEncrypt->SetPG($p, $g);

// CREATE-E USER
$eFrontEncrypt = $dhFrontEncrypt->createE(256);

// SEND E TO SERVER

// CREATE INSTANCE SERVER
$dhServerEncrypt = new CkDh();

// SET PRIME DAN GENERATOR
$success = $dhServerEncrypt->SetPG($p,$g);

// CREATE-E SERVER
$eServerEncrypt = $dhServerEncrypt->createE(256);

// GET-K USER
$kFrontEncrypt = $dhServerEncrypt->findK($eFrontEncrypt);

// SET INSTANCE FOR ENCRYPT IN SERVER
$crypt = new CkCrypt2();
$success = $crypt->UnlockComponent('Anything for 30-day trial.');
if ($success != true) {
    print $crypt->lastErrorText() . "<br>";
    exit;
}

$secretKey 	= $crypt->hashStringENC($kFrontEncrypt); //simpan secret key ke database
$ivEncrypt 	= $crypt->hashStringENC($secretKey);

$crypt->put_EncodingMode('hex');
$crypt->put_HashAlgorithm('md5');
$crypt->put_CryptAlgorithm('aes');
$crypt->put_KeyLength(128);
$crypt->put_CipherMode('cbc');
$crypt->SetEncodedKey($secretKey,'hex');
$crypt->SetEncodedIV($ivEncrypt,'hex');

$inFile = $fileDir . 'test.mp3';
$outFile = $fileDir . 'encrypt.mp3';
$success = $crypt->CkEncryptFile($inFile,$outFile);

print 'HASIL ENKRIPSI' . "<br>";
print 'Secret Key:' . "<br>";
print $kFrontEncrypt . "<br>";
print '128-bit Session Key:' . "<br>";
print $secretKey . "<br>";
print 'Initialization Vector:' . "<br>";
print $ivEncrypt . "<br>";
print 'State Encrypt:' . "<br>";
print $success . "<br><br>";

// ------------------------------------- DEKRIPSI ---------------------------------- //
// CREATE INSTANCE USER
$dhFrontDecrypt = new CkDh();

// SET PRIME DAN GENERATOR
$dhFrontDecrypt->SetPG($p, $g);

// CREATE-E USER
$eFrontDecrypt = $dhFrontDecrypt->createE(256);

$dhServerDecrypt = new CkDh();

// SET PRIME DAN GENERATOR
$dhServerDecrypt->SetPG($p,$g);

//CREATE-E SERVER
$eServerDecrypt = $dhServerDecrypt->createE(256);

// GET-K USER
$kFrontDecrypt = $dhServerDecrypt->findK($eFrontDecrypt);

// SET INSTANCE FOR ENCRYPT SECRET KEY IN SERVER
$encryptSecret = new CkCrypt2();
$secretKeyEncrypt 	= $encryptSecret->hashStringENC($kFrontDecrypt);
$ivSecret 			= $encryptSecret->hashStringENC($secretKeyEncrypt);

$encryptSecret->put_EncodingMode('hex');
$encryptSecret->put_HashAlgorithm('md5');
$encryptSecret->put_CryptAlgorithm('aes');
$encryptSecret->put_KeyLength(128);
$encryptSecret->put_CipherMode('cbc');
$encryptSecret->SetEncodedKey($secretKeyEncrypt,'hex');
$encryptSecret->SetEncodedIV($ivSecret,'hex');

$encripsiSecretKey = $encryptSecret->encryptStringENC($secretKey); //secretKey adalah 128 session key yang akan disimpen di database
print 'True Secret Key:' . "<br>";
print $secretKey . "<br>";
print 'Encripsi Secret Key:' . "<br>";
print $encripsiSecretKey . "<br><br>";

// GET TEXT ENCRIPSI FROM SERVER
// $textEncripsi = $encripsiServer;

// GET K FROM SERVER
$kServerDecrypt = $dhFrontDecrypt->findK($eServerDecrypt);
// if($kServerDecrypt != $kFrontDecrypt)
// 	echo 'not';
// else
// 	echo 'same';

// SET INSTANCE DECRYPT SECRET KEY
$decrypt1 = new CkCrypt2();
$sessionKeyDecrypt 	= $decrypt1->hashStringENC($kServerDecrypt);
$ivSessionDecrypt 	= $decrypt1->hashStringENC($sessionKeyDecrypt);

$decrypt1->put_EncodingMode('hex');
$decrypt1->put_HashAlgorithm('md5');
$decrypt1->put_CryptAlgorithm('aes');
$decrypt1->put_KeyLength(128);
$decrypt1->put_CipherMode('cbc');
$decrypt1->SetEncodedKey($sessionKeyDecrypt,'hex');
$decrypt1->SetEncodedIV($ivSessionDecrypt,'hex');

// DECRYPT THE SECRET KEY
$sessionKey = $decrypt1->decryptStringENC($encripsiSecretKey);

$trueDecrypt = new CkCrypt2();
$secretKeyDecrypt 	= $sessionKey;
$ivDecrypt 			= $trueDecrypt->hashStringENC($secretKeyDecrypt);

$trueDecrypt->put_EncodingMode('hex');
$trueDecrypt->put_HashAlgorithm('md5');
$trueDecrypt->put_CryptAlgorithm('aes');
$trueDecrypt->put_KeyLength(128);
$trueDecrypt->put_CipherMode('cbc');
$trueDecrypt->SetEncodedKey($secretKeyDecrypt,'hex');
$trueDecrypt->SetEncodedIV($ivDecrypt,'hex');
print 'Secret Key (Session Key) Decrypt:' . "<br>";
print $secretKeyDecrypt . "<br>";
print 'Initialization Vector:' . "<br>";
print $ivDecrypt . "<br>";
print 'State Decrypt:' . "<br>";

$inFile = $fileDir . 'encrypt.mp3';
$outFile = $fileDir . 'recover.mp3';
$success = $trueDecrypt->CkDecryptFile($inFile,$outFile);
print $success;
?>
